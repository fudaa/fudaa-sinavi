package org.fudaa.dodico.sinavi3;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.fudaa.dodico.corba.sinavi3.Sinavi3ResultatConsoEau;
import org.fudaa.fudaa.sinavi3.Sinavi3AlgorithmeBassinees;
import org.fudaa.fudaa.sinavi3.Sinavi3DataSimulation;
import org.fudaa.fudaa.sinavi3.Sinavi3Ecluse;

import junit.framework.TestCase;

public class UnitLoadConsoEau extends TestCase{

	
	
//	public void feedFileConsoEau(File f, int size) throws IOException {
//		PrintWriter writer = new PrintWriter(f);
//		FortranWriter fortranwriter = new FortranWriter(writer);
//		for(int i=0;i<size;i++) {
//			fortranwriter.writeln((float)(i*25.6)+"      coefficient d'�pargne du bassin pour l'ecluse "+i);
//		}
//		fortranwriter.close();
//	}
	
	public void testLoadConsoEau() throws IOException {
		File f = File.createTempFile("testSinavi", "consoEau");
		int size= 200;
		//feedFileConsoEau(f, size);
		
		Map<Integer, Integer> map = new HashMap<Integer, Integer>();
		map.put(0,15);
		
		Sinavi3DataSimulation donnees = new Sinavi3DataSimulation(null);
		Sinavi3Ecluse ecluse = new Sinavi3Ecluse();
		ecluse.setCoefficientBassinEpargne_(0.5d);
		ecluse.setLargeur_(12);
		ecluse.setLongueur_(9);
		ecluse.setProfondeur_(42);
		
		donnees.getListeEcluse().ajout(ecluse);
		
		 List<Sinavi3ResultatConsoEau> res = Sinavi3AlgorithmeBassinees.computeConsommationEauSinavi3(map,donnees);
		assertNotNull(res);
		assertEquals(res.size(), map.keySet().size());
		assertNotNull(map.get(res.get(0).indiceEcluse));
		
		assertEquals(Sinavi3AlgorithmeBassinees.computeConsommationEauPourEcluse(15, ecluse), res.get(0).consommationEau);
		
	}
	
}
