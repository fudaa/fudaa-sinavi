

package org.fudaa.dodico.sinavi3;

import java.io.File;
import junit.framework.TestCase;
import org.fudaa.fudaa.sinavi3.Sinavi3DataSimulation;
import org.fudaa.fudaa.sinavi3.Sinavi3Implementation;
import org.fudaa.fudaa.sinavi3.mock.Sinavi3Mock;
import org.fudaa.fudaa.sinavi3.ui.results.reports.Sinavi3SheetProducer;

/**
 * Unit test to validate the format of the post processing reports.
 * @author Adrien Hadoux
 */
public class Sinavi3PostProcessingReportTest extends TestCase{
	
	
	public void testGeneratePostProcessingReport() throws Exception{
            File temp = File.createTempFile("sinavi",".sinavi");
            Sinavi3Mock mock = new Sinavi3Mock();
            Sinavi3DataSimulation data = new Sinavi3DataSimulation(null);
            mock.mockGeneral(data, temp.getAbsolutePath());
            
           Sinavi3Implementation.MOCK_ENABLE = true;
            
            Sinavi3Implementation.lectureResultats(null, temp.getAbsolutePath(), data);
            Sinavi3Implementation.calculExploitationResultats(null, null, data);
            
            assertNotNull(data.getParams_());
            
           //Sinavi3SheetProducer.producePostProcessingReport(new File("C:\\toto.xls"), data, true);
            
            
        }
    
}
