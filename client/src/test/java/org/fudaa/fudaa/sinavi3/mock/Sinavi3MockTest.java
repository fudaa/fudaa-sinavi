package org.fudaa.fudaa.sinavi3.mock;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.fudaa.dodico.corba.sinavi3.Sinavi3ResultatConsoEau;
import org.fudaa.dodico.corba.sinavi3.Sinavi3ResultatGarage;
import org.fudaa.dodico.corba.sinavi3.Sinavi3ResultatListeBassinees;
import org.fudaa.dodico.corba.sinavi3.Sinavi3ResultatListeOccupations;
import org.fudaa.dodico.corba.sinavi3.Sinavi3ResultatListeevenementsSimu;
import org.fudaa.dodico.sinavi3.DResultatsSinavi3;
import org.fudaa.fudaa.sinavi3.Sinavi3DataSimulation;
import org.fudaa.fudaa.sinavi3.Sinavi3Implementation;

import junit.framework.TestCase;

public class Sinavi3MockTest extends TestCase{
	
	
	public void testLoadHis() throws IOException {
		Sinavi3Mock mock = new Sinavi3Mock();
		Sinavi3DataSimulation data = new Sinavi3DataSimulation(null);
		File f=File.createTempFile("sinavi","data");
		mock.mockGeneral(data, f.getAbsolutePath());
		
		Sinavi3ResultatListeevenementsSimu res = DResultatsSinavi3.litResultatsSinavi3(f.getAbsolutePath() + ".his");
		assertNotNull(res);
		assertNotNull(res.listeEvenements);
		assertEquals(res.nombreNavires ==Sinavi3Mock.NB_RECORD_BATEAUX_TO_GENERATE, true);
		assertEquals(res.listeEvenements.length>100, true);
	}
	
	
	public void testLoadOccup() throws IOException {
		Sinavi3Mock mock = new Sinavi3Mock();
		Sinavi3DataSimulation data = new Sinavi3DataSimulation(null);
		File f=File.createTempFile("sinavi","data");
		mock.mockGeneral(data, f.getAbsolutePath());
		List<Sinavi3ResultatGarage> resultGarages = new ArrayList<Sinavi3ResultatGarage>();
		Sinavi3ResultatListeOccupations  res = DResultatsSinavi3.litOccupationsSinavi3(f.getAbsolutePath()+".occup",20,resultGarages);
		assertNotNull(res);
		assertNotNull(res.listeOccupations);
		assertEquals(res.nombreEvenements==Sinavi3Mock.NB_RECORD_OCCUP_TO_GENERATE*2, true);
		assertEquals(res.listeOccupations.length>0, true);
	}
	
	public void testLoadConso() throws IOException {
		Sinavi3Mock mock = new Sinavi3Mock();
		Sinavi3DataSimulation data = new Sinavi3DataSimulation(null);
		File f=File.createTempFile("sinavi","data");
		mock.mockGeneral(data, f.getAbsolutePath());
		
		Sinavi3ResultatListeBassinees  res = DResultatsSinavi3.litBassineesSinavi3(f.getAbsolutePath()+".conso",data.mapBassineeEcluses);
		assertNotNull(res);
		assertNotNull(res.listeBassinees);
		assertEquals(res.nombreEvenements==mock.NB_EVENT_CONSO_EAU, true);
		assertEquals(res.listeBassinees.length>1, true);
	}
	
	

}
