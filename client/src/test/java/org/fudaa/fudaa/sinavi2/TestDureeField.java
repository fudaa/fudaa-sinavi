/*
 *  @creation     13 d�c. 2005
 *  @modification $Date: 2007-01-17 10:44:32 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.sinavi2;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuPanel;

import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.gui.CtuluLibDialog;

/**
 * @author Fred Deniger
 * @version $Id: TestDureeField.java,v 1.3 2007-01-17 10:44:32 deniger Exp $
 */
public final class TestDureeField {

  private TestDureeField() {}

  public static void main(final String[] _args) {
    final BuPanel pn = new BuPanel(new BuGridLayout(2, 2, 5));
    final String[] lb = new String[] { "Temps 1", "Temps 2" };
    final DureeField duree1 = new DureeField(true, true, true, true, true);
    final DureeField duree2 = new DureeField(true, true, true, true, true);
    duree1.setDureeValidator(DureeFieldValidator.creeMin(120));
    duree2.setDureeValidator(DureeFieldValidator.creeMax(120));
    pn.add(new BuLabel(lb[0]));
    pn.add(duree1);
    pn.add(new BuLabel(lb[1]));
    pn.add(duree2);
    final DureeField[] fieldsToValid = new DureeField[] { duree1, duree2 };
    final BuButton btValid = new BuButton("Valider");
    pn.add(btValid);
    final JFrame frame = new JFrame("essai pour sinavi");
    btValid.addActionListener(new ActionListener() {

      public void actionPerformed(final ActionEvent _e) {
        final List invalidFields = new ArrayList();
        for (int i = 0; i < fieldsToValid.length; i++) {
          if (!fieldsToValid[i].isValueValid()) {
            invalidFields.add(lb[i]);
          }
        }
        if (invalidFields.size() == 0) {
          CtuluLibDialog.showConfirmation(frame, frame.getTitle(), "Tout il est valide");
        } else {
          CtuluLibDialog.showError(frame, frame.getTitle(), "Les champs suivants sont invalides:\n -"
              + CtuluLibString.arrayToString(CtuluLibString.enTableau(invalidFields), "\n -"));
        }

      }

    });

    frame.setContentPane(pn);
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.pack();
    frame.setVisible(true);

  }
}
