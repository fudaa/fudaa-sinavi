/*
 * @file         SinaviBateauxParametres.java
 * @creation     2001-05-17
 * @modification $Date: 2006-09-19 15:08:59 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.sinavi;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.InputStreamReader;
import java.util.Vector;

import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.EmptyBorder;

import com.memoire.bu.BuAssistant;
import com.memoire.bu.BuButton;
import com.memoire.bu.BuComboBox;
import com.memoire.bu.BuCommonImplementation;
import com.memoire.bu.BuCommonInterface;
import com.memoire.bu.BuDialogMessage;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuTextField;
import com.memoire.xml.XmlParser;

import org.fudaa.dodico.corba.navigation.INavireType;
import org.fudaa.dodico.corba.navigation.INavireTypeHelper;
import org.fudaa.dodico.corba.navigation.LGroupeNavireType;

import org.fudaa.dodico.objet.UsineLib;

/**
 * impl�mentation de l'onglet "Bateaux" pour les param�tres de sinavi
 * 
 * @version $Revision: 1.12 $ $Date: 2006-09-19 15:08:59 $ by $Author: deniger $
 * @author Aline Marechalle , Franck Lejeune
 */
public class SinaviBateauxParametres extends BuPanel implements ActionListener, MouseListener {

  /**
   * tableau de vecteurs contenant les types de navires pr�d�finis dans le fichier sinavi.txt
   */
  private Vector[] naviresPredefinis_;
  /**
   * longueur du bateau en m�tre
   */
  private final LongueurField lfTailleBateauLongueur_ = new LongueurField(false, true, true);
  /**
   * largeur du bateau en m�tre
   */
  private final LongueurField lfTailleBateauLargeur_ = new LongueurField(false, true, true);
  /**
   * tirant d'eau du bateau en m�tre
   */
  private final LongueurField lfTirantEau_ = new LongueurField(false, true, true);
  /**
   * g�ne admissible du bateau en heures:minutes
   */
  private final DureeField dfGeneAdmissible_ = new DureeField(false, false, false, true, false);
  /**
   * bouton pour ajouter un type de navire � l'�tude
   */
  private final BuButton bCreerBateau_ = new BuButton(SinaviResource.SINAVI.getIcon("AJOUTER"), "Cr�er");
  /**
   * bouton pour valider une modification d'un type de navire
   */
  private final BuButton bValiderBateau_ = new BuButton(SinaviResource.SINAVI.getIcon("VALIDER"), "Enregistrer");
  /**
   * bouton pour supprimer un type de navire
   */
  private final BuButton bSupprimerBateau_ = new BuButton(SinaviResource.SINAVI.getIcon("ANNULER"), "Supprimer");
  /**
   * menu d�roulant permettant de choisir la cat�gorie de navire
   */
  private final BuComboBox cbChoixMenuBateaux_ = new BuComboBox();
  /**
   * liste des navires cr��s pour la simulation
   */
  private final JList lBateauxCrees_ = new JList();
  /**
   * nom du type de navire
   */
  private final BuTextField tfBateauChoisi_ = new BuTextField();
  /**
   * vitesse du type de navire en kilom�tres/heure
   */
  private final BuTextField tfVitesseBateau_ = BuTextField.createDoubleField();
  /**
   * liste affichant les types de navires de chaque cat�gorie
   */
  private final JList lCategorie_ = new JList();
  /**
   * ascenseur pour les categories de navires
   */
  private final JScrollPane CategorieScrollPane_ = new JScrollPane(lCategorie_,
      ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
  /**
   * ascenseur pour les types de navire cr��s
   */
  private final JScrollPane BateauxCreesScrollPane_ = new JScrollPane(lBateauxCrees_,
      ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
  /**
   * tableau de type de navire pour la cat�gorie plaisancier
   */
  private INavireType[] categorieplaisancier_;
  /**
   * tableau de types de navire pour la cat�gorie passager
   */
  private INavireType[] categoriepassager_;
  /**
   * tableau de types de navire pour la cat�gorie freycinet
   */
  private INavireType[] categoriefreycinet_;
  /**
   * tableau de types de navire pour la cat�gorie convois pousse
   */
  private INavireType[] categorieconvoispousse_;
  /**
   * tableau de types de navire pour la cat�gorie personnalise
   */
  private INavireType[] categoriepersonnalise_;
  /**
   * panel contenant les boutons, il est plac� en bas � droite
   */
  private final BuPanel pBoutons_ = new BuPanel();
  /**
   * panel contenant les diff�rents champs de l'onglet
   */
  private final BuPanel pDonnees_ = new BuPanel();
  /**
   * instance de SinaviImplementation
   */
  private BuCommonImplementation app_;

  /**
   * Constructeur.
   * 
   * @param _appli : instance de SinaviImplementation
   */
  public SinaviBateauxParametres(final BuCommonInterface _appli) {
    super();
    app_ = _appli.getImplementation();
    // chargement des bateaux pr�d�finis dans le fichier Sinavi.txt
    // utilisation d'un parseur Xml et d'un Listener propre � Sinavi
    naviresPredefinis_ = new Vector[5];
    try {
      final XmlParser xmlp_ = new XmlParser(new InputStreamReader(SinaviResource.class
          .getResourceAsStream("Sinavi.txt")), "Sinavi.txt");
      final SinaviDefinitionXmlListener sdxmllistener = new SinaviDefinitionXmlListener(naviresPredefinis_);
      xmlp_.setXmlListener(sdxmllistener);
      xmlp_.parse();
    } catch (final Exception ex) {
      System.err.println(ex);
    }
    // Cr�ation du layout manager et des contraintes du panel donnees
    final GridBagLayout lm_ = new GridBagLayout();
    final GridBagConstraints c_ = new GridBagConstraints();
    // Cr�ation du layout manager et du bord de l'onglet
    setLayout(new BorderLayout());
    setBorder(new EmptyBorder(0, 0, 0, 0));
    // layout des deux panels, donn�es et boutons
    pDonnees_.setLayout(lm_);
    pBoutons_.setLayout(new FlowLayout(FlowLayout.RIGHT));
    // Ajout d'un listener aux differents elements
    bValiderBateau_.addActionListener(this);
    bCreerBateau_.addActionListener(this);
    bSupprimerBateau_.addActionListener(this);
    cbChoixMenuBateaux_.addActionListener(this);
    lCategorie_.addMouseListener(this);
    lBateauxCrees_.addMouseListener(this);
    // Bouton "sauver" et "supprimer" pas visible par defaut
    bValiderBateau_.setEnabled(false);
    bSupprimerBateau_.setEnabled(false);
    // Definition de la largeur du champ texte
    tfBateauChoisi_.setColumns(24);
    tfVitesseBateau_.setColumns(3);
    // Definition de la largeur maximum des ComboBox
    cbChoixMenuBateaux_.setPreferredSize(new Dimension(150, 33));
    // D�finition des types de bateaux
    cbChoixMenuBateaux_.addItem("Plaisancier");
    cbChoixMenuBateaux_.addItem("Passager");
    cbChoixMenuBateaux_.addItem("Freycinet");
    cbChoixMenuBateaux_.addItem("Convoi Pouss�");
    cbChoixMenuBateaux_.addItem("Personnalis�");
    // D�finition des categories de bateaux
    categorieplaisancier_ = new INavireType[naviresPredefinis_[0].size()];
    categoriepassager_ = new INavireType[naviresPredefinis_[1].size()];
    categoriefreycinet_ = new INavireType[naviresPredefinis_[2].size()];
    categorieconvoispousse_ = new INavireType[naviresPredefinis_[3].size()];
    categoriepersonnalise_ = new INavireType[naviresPredefinis_[4].size()];
    categorieplaisancier_ = (INavireType[]) naviresPredefinis_[0].toArray(categorieplaisancier_);
    categoriepassager_ = (INavireType[]) naviresPredefinis_[1].toArray(categoriepassager_);
    categoriefreycinet_ = (INavireType[]) naviresPredefinis_[2].toArray(categoriefreycinet_);
    categorieconvoispousse_ = (INavireType[]) naviresPredefinis_[3].toArray(categorieconvoispousse_);
    categoriepersonnalise_ = (INavireType[]) naviresPredefinis_[4].toArray(categoriepersonnalise_);
    // Contraintes des listes ( ScrollPane )
    final Dimension dimc_ = new Dimension(150, 150);
    final Dimension dimbc_ = new Dimension(150, 250);
    CategorieScrollPane_.setPreferredSize(dimc_);
    BateauxCreesScrollPane_.setPreferredSize(dimbc_);
    lCategorie_.setCellRenderer(new SinaviBateauCellRenderer());
    lCategorie_.setListData(categorieplaisancier_);
    lBateauxCrees_.setModel(SinaviImplementation.SINAVIBATEAULISTMODEL);
    lBateauxCrees_.setCellRenderer(new SinaviBateauCellRenderer());
    // Contraintes communes � tous les composants
    c_.gridy = GridBagConstraints.RELATIVE;
    c_.anchor = GridBagConstraints.WEST;
    c_.ipadx = 0;
    c_.ipady = 0;
    c_.gridheight = 1; // 1 ligne pour les �tiquettes.
    // Remplissage de la premiere colonne
    c_.gridx = 0; // premiere colonne
    placeComposant(lm_, new BuLabel("Bateaux Cr��s :"), c_);
    c_.insets.top = 10;
    c_.gridheight = GridBagConstraints.REMAINDER;
    placeComposant(lm_, BateauxCreesScrollPane_, c_);
    c_.gridheight = 1;
    // Remplissage de la deuxieme colonne
    c_.gridx = 1; // deuxieme colonne
    c_.insets.left = 25;
    placeComposant(lm_, new BuLabel("Type de bateau :"), c_);
    placeComposant(lm_, new BuLabel("Longueur :"), c_);
    placeComposant(lm_, new BuLabel("Largeur :"), c_);
    placeComposant(lm_, new BuLabel("Tirant d'eau :"), c_);
    placeComposant(lm_, new BuLabel("Vitesse :"), c_);
    placeComposant(lm_, new BuLabel("Gene admissible :"), c_);
    // Remplissage de la troisieme colonne
    c_.gridx = 2; // troisieme colonne
    c_.gridwidth = 2;
    placeComposant(lm_, tfBateauChoisi_, c_);
    c_.gridwidth = 1;
    placeComposant(lm_, lfTailleBateauLongueur_, c_);
    placeComposant(lm_, lfTailleBateauLargeur_, c_);
    placeComposant(lm_, lfTirantEau_, c_);
    placeComposant(lm_, tfVitesseBateau_, c_);
    placeComposant(lm_, dfGeneAdmissible_, c_);
    // Remplissage de la quatrieme colonne
    c_.gridx = 3; // quatrieme colonne
    placeComposant(lm_, new BuLabel("Types pr�d�finis : "), c_);
    placeComposant(lm_, cbChoixMenuBateaux_, c_);
    c_.gridheight = GridBagConstraints.REMAINDER;
    placeComposant(lm_, CategorieScrollPane_, c_);
    // ajout du bouton valider au panel boutons
    pBoutons_.add(bCreerBateau_);
    pBoutons_.add(bSupprimerBateau_);
    pBoutons_.add(bValiderBateau_);
    // ajout des deux panels � l'onglet
    add(pDonnees_, BorderLayout.CENTER);
    add(pBoutons_, BorderLayout.SOUTH);
  }

  /**
   * si clic de souris sur la liste de cat�gorie de navires : chargement des param�tres du type de navires s�lectionn�
   * si clic de souris sur la liste des types de navires cr��s : affichage des param�tres de ce type de navires
   * 
   * @param _e
   */
  public void mouseClicked(final MouseEvent _e) {
    if (_e.getSource() == lCategorie_) {
      getBateauParametres(INavireTypeHelper.narrow((org.omg.CORBA.Object) lCategorie_.getSelectedValue()));
    } else {
      if (lBateauxCrees_.getSelectedValue() == null) {
        // System.out.println("liste vide");
      } else {
        cbChoixMenuBateaux_.setSelectedItem(INavireTypeHelper.narrow(
            (org.omg.CORBA.Object) lBateauxCrees_.getSelectedValue()).groupeEnChaine());
        getBateauParametres(INavireTypeHelper.narrow((org.omg.CORBA.Object) lBateauxCrees_.getSelectedValue()));
        bValiderBateau_.setEnabled(true);
        bSupprimerBateau_.setEnabled(true);
        bCreerBateau_.setEnabled(false);
      }
    }
  }

  /**
   * pas utilis�
   * 
   * @param e
   */
  public void mousePressed(final MouseEvent e) {}

  /**
   * pas utilis�
   * 
   * @param e
   */
  public void mouseReleased(final MouseEvent e) {}

  /**
   * pas utilis�
   * 
   * @param e
   */
  public void mouseEntered(final MouseEvent e) {}

  /**
   * pas utilis�
   * 
   * @param e
   */
  public void mouseExited(final MouseEvent e) {}

  /**
   * si action sur le bouton cr�er bateau, test de la validit� des param�tres et si ok, enregistrement du type de navire
   * si action sur le bouton valider bateau, test de la validit� des param�tres et si ok, enregistrement des
   * modifications sinon, chargement des param�tres pr�c�dents valides si action sur le menu d�roulant, chargement des
   * types de navires selon la cat�gorie s�lectionn�e dans le menu
   * 
   * @param _e
   */
  public void actionPerformed(final ActionEvent _e) {
    if (_e.getSource() == bCreerBateau_) {
      if (validBateauParametres()) {
        final INavireType bateautemp_ = UsineLib.findUsine().creeNavigationNavireType();
        setBateauParametres(bateautemp_);
        SinaviImplementation.ETUDE_SINAVI.ajouteNavireType(bateautemp_);
        SinaviImplementation.SINAVIBATEAULISTMODEL.refresh();
      } else {
        final BuDialogMessage dialog_mess = new BuDialogMessage(app_, SinaviImplementation.informationsSoftware(),
            "Param�tres du type de navire\nincomplets");
        dialog_mess.activate();
        SinaviImplementation.assistant_.changeAttitude(BuAssistant.PAROLE, "Parametres du bateau\nincomplets");
        // System.out.println("Parametres du bateau incomplets");
      }
    } else if (_e.getSource() == bValiderBateau_) {
      if (validBateauParametres()) {
        setBateauParametres(INavireTypeHelper.narrow((org.omg.CORBA.Object) lBateauxCrees_.getSelectedValue()));
        bValiderBateau_.setEnabled(false);
        bCreerBateau_.setEnabled(true);
        SinaviImplementation.SINAVIBATEAULISTMODEL.refresh();
      } else {
        final BuDialogMessage dialog_mess = new BuDialogMessage(app_, SinaviImplementation.informationsSoftware(),
            "Param�tres du type de navire\nincomplets");
        dialog_mess.activate();
        SinaviImplementation.assistant_.changeAttitude(BuAssistant.PAROLE, "Parametres du bateau\nincomplets");
        // getBateauParametres(INavireTypeHelper.narrow((org.omg.CORBA.Object)lBateauxCrees_.getSelectedValue()));
      }
    } else if (_e.getSource() == bSupprimerBateau_) {
      SinaviImplementation.ETUDE_SINAVI.enleveNavireType(INavireTypeHelper.narrow((org.omg.CORBA.Object) lBateauxCrees_
          .getSelectedValue()));
      bSupprimerBateau_.setEnabled(false);
      bValiderBateau_.setEnabled(false);
      bCreerBateau_.setEnabled(true);
      SinaviImplementation.SINAVIBATEAULISTMODEL.refresh();
    } else if (_e.getSource() == cbChoixMenuBateaux_) {
      if (cbChoixMenuBateaux_.getSelectedItem() == "Plaisancier") {
        lCategorie_.setListData(categorieplaisancier_);
      }
      if (cbChoixMenuBateaux_.getSelectedItem() == "Passager") {
        lCategorie_.setListData(categoriepassager_);
      }
      if (cbChoixMenuBateaux_.getSelectedItem() == "Freycinet") {
        lCategorie_.setListData(categoriefreycinet_);
      }
      if (cbChoixMenuBateaux_.getSelectedItem() == "Convoi Pouss�") {
        lCategorie_.setListData(categorieconvoispousse_);
      }
      if (cbChoixMenuBateaux_.getSelectedItem() == "Personnalis�") {
        lCategorie_.setListData(categoriepersonnalise_);
      }
    }
  }

  /**
   * sauvegarde des donnees du navire courant ( s�lectionn� dans la liste )
   * 
   * @param _navire le bateau s�lectionn� dans la liste
   */
  private void setBateauParametres(final INavireType _navire) {
    if (validBateauParametres()) {
      _navire.type(tfBateauChoisi_.getText());
      if (cbChoixMenuBateaux_.getSelectedItem() == "Plaisancier") {
        _navire.groupeNavire(LGroupeNavireType.PLAISANCIER);
      } else if (cbChoixMenuBateaux_.getSelectedItem() == "Passager") {
        _navire.groupeNavire(LGroupeNavireType.PASSAGER);
      } else if (cbChoixMenuBateaux_.getSelectedItem() == "Freycinet") {
        _navire.groupeNavire(LGroupeNavireType.FREYCINET);
      } else if (cbChoixMenuBateaux_.getSelectedItem() == "Convoi Pouss�") {
        _navire.groupeNavire(LGroupeNavireType.CONVOI_POUSSE);
      } else if (cbChoixMenuBateaux_.getSelectedItem() == "Personnalis�") {
        _navire.groupeNavire(LGroupeNavireType.PERSONNALISE);
      }
      _navire.longueur(lfTailleBateauLongueur_.getLongueurField());
      _navire.largeur(lfTailleBateauLargeur_.getLongueurField());
      _navire.tirantMoy(lfTirantEau_.getLongueurField());
      _navire.vitesseMoy(((Double) tfVitesseBateau_.getValue()).doubleValue());
      _navire.dureeAttenteAdmissible(dfGeneAdmissible_.getDureeField());
      tfBateauChoisi_.setText(null);
      lfTailleBateauLongueur_.setLongueurField(0);
      lfTailleBateauLargeur_.setLongueurField(0);
      lfTirantEau_.setLongueurField(0);
      dfGeneAdmissible_.setDureeField(0);
      tfVitesseBateau_.setValue(null);
    } else {
      SinaviImplementation.assistant_.changeAttitude(BuAssistant.PAROLE, "Parametres du bateau\nincomplets");
      // System.out.println("Parametres du bateau incomplets");
    }
  }

  /**
   * M�thode utile pour placer un composant avec ses contraintes dans le panel "donnees"
   * 
   * @param _lm le layout manager
   * @param _composant le composant
   * @param _c les contraintes
   */
  private void placeComposant(final GridBagLayout _lm, final Component _composant, final GridBagConstraints _c) {
    _lm.setConstraints(_composant, _c);
    pDonnees_.add(_composant);
  }

  /**
   * test de validit� de tous les champs de l'onglet
   */
  private boolean validBateauParametres() {
    if (tfBateauChoisi_.getText().equals(null)) {
      return false;
    }
    if (lfTailleBateauLongueur_.getLongueurField() == 0) {
      return false;
    }
    if (lfTailleBateauLargeur_.getLongueurField() == 0) {
      return false;
    }
    if (lfTirantEau_.getLongueurField() == 0) {
      return false;
    }
    if (((Double) tfVitesseBateau_.getValue()).doubleValue() == 0) {
      return false;
    }
    // if (dfGeneAdmissible_.getDureeField()==0)
    // return false;
    return true;
  }

  /**
   * mise � jour des champs du bateau s�lectionn� dans la liste
   * 
   * @param _navire le bateau s�lectionn� dans la liste
   */
  private void getBateauParametres(final INavireType _navire) {
    tfBateauChoisi_.setText(_navire.type());
    lfTailleBateauLongueur_.setLongueurField((int) _navire.longueur());
    lfTailleBateauLargeur_.setLongueurField((int) _navire.largeur());
    lfTirantEau_.setLongueurField((int) _navire.tirantMoy());
    tfVitesseBateau_.setValue(new Double(_navire.vitesseMoy()));
    dfGeneAdmissible_.setDureeField((int) _navire.dureeAttenteAdmissible());
  }
}
