/*
 * @file         SinaviFilleChoixResultats.java
 * @creation     2001-05-17
 * @modification $Date: 2006-09-19 15:09:00 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.sinavi;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.JComponent;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.EmptyBorder;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuComboBox;
import com.memoire.bu.BuCommonImplementation;
import com.memoire.bu.BuInternalFrame;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuPicture;
import com.memoire.bu.BuRadioButton;

import org.fudaa.dodico.corba.navigation.IBiefNavigation;
import org.fudaa.dodico.corba.navigation.IBiefNavigationHelper;
import org.fudaa.dodico.corba.navigation.IEcluseFluviale;
import org.fudaa.dodico.corba.navigation.IEcluseFluvialeHelper;
import org.fudaa.dodico.corba.navigation.IGare;
import org.fudaa.dodico.corba.navigation.IGareHelper;
import org.fudaa.dodico.corba.navigation.INavireType;
import org.fudaa.dodico.corba.navigation.INavireTypeHelper;

/**
 * impl�mentation d'une fen�tre interne permettant de choisir les r�sultats d�sir�s
 * 
 * @version $Revision: 1.8 $ $Date: 2006-09-19 15:09:00 $ by $Author: deniger $
 * @author Aline Marechalle , Franck Lejeune
 */
public class SinaviFilleChoixResultats extends BuInternalFrame implements ActionListener {

  /**
   * bouton valider
   */
  private final BuButton bValider_ = new BuButton(SinaviResource.SINAVI.getIcon("VALIDER"), "Valider");
  /**
   * bouton annuler
   */
  private final BuButton bAnnuler_ = new BuButton(SinaviResource.SINAVI.getIcon("ANNULER"), "Annuler");
  /**
   * menu d�roulant pour choisir le type d'Element
   */
  private final BuComboBox cbChoixElement_ = new BuComboBox();
  /**
   * groupe de boutons relatif aux Elements
   */
  private final ButtonGroup bgElements_ = new ButtonGroup();
  /**
   * bouton radio pour l'affichage des r�sultats concernant tous les Elements
   */
  private final BuRadioButton rbTousElements_ = new BuRadioButton("Tous les Elements");
  /**
   * bouton radio pour l'affichage des r�sultats concernant l'Element s�lectionn� dans la liste
   */
  private final BuRadioButton rbElementChoisi_ = new BuRadioButton("Un Element s�lectionn�");
  /**
   * groupe de boutons relatif au choix du graphique
   */
  private final ButtonGroup bgGraphique_ = new ButtonGroup();
  /**
   * bouton radio pour la cr�ation d'un graphique pour tous les Elements
   */
  private final BuRadioButton rbUnGraphique_ = new BuRadioButton("Un graphique pour tous les Elements");
  /**
   * bouton radio pour la cr�ation d'un graphique par Element
   */
  private final BuRadioButton rbDesGraphiques_ = new BuRadioButton("Un graphique par Element");
  /**
   * liste affichant les Elements cr��s
   */
  private final JList lElementsCrees_ = new JList();
  /**
   * ascenceur pour la liste des biefs cr��s
   */
  private final JScrollPane ElementsCreesScrollPane_ = new JScrollPane(lElementsCrees_,
      ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
  /**
   * liste affichant les r�sultats possibles pour le type d'Element
   */
  private final JList lResultatsPossibles_ = new JList();
  /**
   * ascenceur pour la liste des �cluses cr��es
   */
  private final JScrollPane ResultatsPossiblesScrollPane_ = new JScrollPane(lResultatsPossibles_,
      ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
  /**
   * liste des r�sultats possibles pour un Element de type gare
   */
  private final String[] ResultatsGare_ = { "Nombre de bateaux par jour" };
  /**
   * liste des r�sultats possibles pour un bateau
   */
  private final String[] ResultatsNavire_ = { "Temps de parcours moyen par type" };
  /**
   * liste des r�sultats possibles pour un Element de type bief
   */
  private final String[] ResultatsBief_ = { "Nombre de bateaux montants (tous types confondus)",
      "Nombre de bateaux montants (par type)", "------------------------------------------------",
      "Nombre de bateaux avalants (tous types confondus)", "Nombre de bateaux avalants (par type)",
      "------------------------------------------------", "Temps d'attente des montants",
      "Temps d'attente des avalants" };
  /**
   * liste des r�sultats possibles pour un Element de type �cluse
   */
  private final String[] ResultatsEcluse_ = { "Nombre de bateaux montants (tous types confondus)",
      "Nombre de bateaux montants (par type)", "------------------------------------------------",
      "Nombre de bateaux avalants (tous types confondus)", "Nombre de bateaux avalants par type)",
      "------------------------------------------------", "Nombre de fausse bassin�e montante",
      "Nombre de fausse bassin�e avalante", "------------------------------------------------",
      "Nombre de bassin�e montante", "Nombre de bassin�e avalante", "------------------------------------------------",
      "Temps d'attente des montants", "Temps d'attente des avalants" };
  /**
   * panel contenant les boutons, il est plac� en bas � droite
   */
  private final BuPanel pBoutons_ = new BuPanel();
  /**
   * panel contenant les diff�rents champs de la fen�tre
   */
  private final BuPanel pDonnees_ = new BuPanel();
  /**
   * fen�tre interne affichant les r�sultats sous forme HTML
   */
  // private BuBrowserFrame fResultats_;
  /**
   * une instance de SinaviImplementation
   */
  private BuCommonImplementation app_;
  private final Image imgGare_ = SinaviResource.SINAVI.getImage("sinaviimagegare");
  private final Image imgEcluse_ = SinaviResource.SINAVI.getImage("sinaviimageecluse");
  private final Image imgBief_ = SinaviResource.SINAVI.getImage("sinaviimagebief");
  private final Image imgNavire_ = SinaviResource.SINAVI.getImage("sinaviimagenavire");
  private final BuPicture CadreImage_ = new BuPicture();

  /**
   * Constructeur
   * 
   * @param _app une instance de SinaviImplementation
   */
  public SinaviFilleChoixResultats(final BuCommonImplementation _app) {
    super("Choix des r�sultats", false, false, false, false);
    app_ = _app;
    // Cr�ation du layout manager et des contraintes du panel donnees
    final GridBagLayout lm_ = new GridBagLayout();
    final GridBagConstraints c_ = new GridBagConstraints();
    // Cr�ation du layout manager et du bord de la fenetre
    ((JComponent) getContentPane()).setBorder(new EmptyBorder(25, 25, 25, 25));
    getContentPane().setLayout(new BorderLayout());
    // layout des deux panels, donn�es et boutons
    pDonnees_.setLayout(lm_);
    pBoutons_.setLayout(new FlowLayout(FlowLayout.RIGHT));
    // Ajout des listener aux elements
    bValider_.addActionListener(this);
    bAnnuler_.addActionListener(this);
    rbTousElements_.addActionListener(this);
    rbElementChoisi_.addActionListener(this);
    cbChoixElement_.addActionListener(this);
    // placement des boutons radio dans les groupes de boutons
    bgElements_.add(rbTousElements_);
    bgElements_.add(rbElementChoisi_);
    bgGraphique_.add(rbUnGraphique_);
    bgGraphique_.add(rbDesGraphiques_);
    // boutons selectionnes par defaut
    rbUnGraphique_.setSelected(true);
    rbTousElements_.setSelected(true);
    // cr�ation du menu deroulant pour le choix du type d'Element
    cbChoixElement_.addItem("Gare");
    cbChoixElement_.addItem("Bief");
    cbChoixElement_.addItem("Ecluse");
    cbChoixElement_.addItem("Navire");
    // selection par defaut du type ecluse comme Element
    lResultatsPossibles_.setListData(ResultatsEcluse_);
    cbChoixElement_.setSelectedItem("Ecluse");
    CadreImage_.setImage(imgEcluse_);
    SinaviImplementation.SINAVIELEMENTLISTMODEL.setElement("ecluse");
    // Contrainte des listes
    ElementsCreesScrollPane_.setPreferredSize(new Dimension(160, 200));
    lElementsCrees_.setCellRenderer(new SinaviElementsCellRenderer());
    lElementsCrees_.setModel(SinaviImplementation.SINAVIELEMENTLISTMODEL);
    ResultatsPossiblesScrollPane_.setPreferredSize(new Dimension(340, 200));
    // lElementsCrees_.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    lResultatsPossibles_.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    // Contraintes communes � tous les composants
    c_.gridy = GridBagConstraints.RELATIVE;
    c_.anchor = GridBagConstraints.WEST;
    c_.ipadx = 0;
    c_.ipady = 0;
    c_.gridheight = 1; // 1 ligne pour les �tiquettes.
    // Remplissage de la premiere colonne
    c_.gridx = 0; // premiere colonne
    placeComposant(lm_, new BuLabel("S�lectionner un"), c_);
    placeComposant(lm_, new BuLabel("type d'Element :"), c_);
    placeComposant(lm_, cbChoixElement_, c_);
    placeComposant(lm_, new BuLabel(""), c_);
    placeComposant(lm_, CadreImage_, c_);
    // Remplissage de la deuxieme colonne
    c_.gridx = 1; // deuxieme colonne
    c_.insets.left = 25;
    placeComposant(lm_, rbTousElements_, c_);
    placeComposant(lm_, rbElementChoisi_, c_);
    c_.gridheight = GridBagConstraints.REMAINDER;
    placeComposant(lm_, ElementsCreesScrollPane_, c_);
    c_.gridheight = 1;
    // Remplissage de la troisieme colonne
    c_.gridx = 2; // troisieme colonne
    placeComposant(lm_, rbUnGraphique_, c_);
    placeComposant(lm_, rbDesGraphiques_, c_);
    c_.gridheight = GridBagConstraints.REMAINDER;
    placeComposant(lm_, ResultatsPossiblesScrollPane_, c_);
    // ajout du bouton valider au panel boutons
    pBoutons_.add(bAnnuler_);
    pBoutons_.add(bValider_);
    // ajout des deux panels � l'onglet
    getContentPane().add(pDonnees_, BorderLayout.CENTER);
    getContentPane().add(pBoutons_, BorderLayout.SOUTH);
  }

  /**
   * M�thode utile pour placer un composant avec ses contraintes dans le panel "donnees"
   * 
   * @param _lm le layout manager
   * @param _composant le composant
   * @param _c les contraintes
   */
  private void placeComposant(final GridBagLayout _lm, final Component _composant, final GridBagConstraints _c) {
    _lm.setConstraints(_composant, _c);
    pDonnees_.add(_composant);
  }

  /**
   * methode creant le tableau des elements sur lesquels vont porter les resultats
   */
  private Object[] construitTableauElements() {
    Object[] temp_;
    if (((String) cbChoixElement_.getSelectedItem()).equals("Ecluse")) {
      if (rbTousElements_.isSelected()) {
        return SinaviImplementation.ETUDE_SINAVI.reseau().ecluses();
      }
      temp_ = lElementsCrees_.getSelectedValues();
      // les ecluses s�lectionn�es
      final IEcluseFluviale[] ecluses = new IEcluseFluviale[temp_.length];
      for (int i = 0; i < temp_.length; i++) {
        ecluses[i] = (IEcluseFluvialeHelper.narrow((org.omg.CORBA.Object) temp_[i]));
      }
      return ecluses;
    } else if (((String) cbChoixElement_.getSelectedItem()).equals("Gare")) {
      if (rbTousElements_.isSelected()) {
        return SinaviImplementation.ETUDE_SINAVI.reseau().gares();
      }
      temp_ = lElementsCrees_.getSelectedValues(); // les gares s�lectionn�es
      final IGare[] gares = new IGare[temp_.length];
      for (int i = 0; i < temp_.length; i++) {
        gares[i] = (IGareHelper.narrow((org.omg.CORBA.Object) temp_[i]));
      }
      return gares;
    } else if (((String) cbChoixElement_.getSelectedItem()).equals("Bief")) {
      if (rbTousElements_.isSelected()) {
        return SinaviImplementation.ETUDE_SINAVI.reseau().biefs();
      }
      temp_ = lElementsCrees_.getSelectedValues(); // les biefs s�lectionn�s
      final IBiefNavigation[] biefs = new IBiefNavigation[temp_.length];
      for (int i = 0; i < temp_.length; i++) {
        biefs[i] = (IBiefNavigationHelper.narrow((org.omg.CORBA.Object) temp_[i]));
      }
      return biefs;
    } else // if(((String)cbChoixElement_.getSelectedItem()).equals("Navire"))
    {
      if (rbTousElements_.isSelected()) {
        return SinaviImplementation.ETUDE_SINAVI.naviresType();
      }
      temp_ = lElementsCrees_.getSelectedValues();
      // les types de navire s�lectionn�s
      final INavireType[] navires = new INavireType[temp_.length];
      for (int i = 0; i < temp_.length; i++) {
        navires[i] = (INavireTypeHelper.narrow((org.omg.CORBA.Object) temp_[i]));
      }
      return navires;
    }
  }

  /**
   * gestion des boutons et des autres actions
   * 
   * @param _e
   */
  public void actionPerformed(final ActionEvent _e) {
    if (_e.getSource() == cbChoixElement_) {
      if (((String) cbChoixElement_.getSelectedItem()).equals("Ecluse")) {
        lResultatsPossibles_.setListData(ResultatsEcluse_);
        CadreImage_.setImage(imgEcluse_);
        SinaviImplementation.SINAVIELEMENTLISTMODEL.setElement("ecluse");
      } else if (((String) cbChoixElement_.getSelectedItem()).equals("Gare")) {
        lResultatsPossibles_.setListData(ResultatsGare_);
        CadreImage_.setImage(imgGare_);
        SinaviImplementation.SINAVIELEMENTLISTMODEL.setElement("gare");
      } else if (((String) cbChoixElement_.getSelectedItem()).equals("Bief")) {
        lResultatsPossibles_.setListData(ResultatsBief_);
        CadreImage_.setImage(imgBief_);
        SinaviImplementation.SINAVIELEMENTLISTMODEL.setElement("bief");
      } else if (((String) cbChoixElement_.getSelectedItem()).equals("Navire")) {
        lResultatsPossibles_.setListData(ResultatsNavire_);
        CadreImage_.setImage(imgNavire_);
        SinaviImplementation.SINAVIELEMENTLISTMODEL.setElement("navire");
      }
    } else if (_e.getSource() == rbTousElements_) {
      rbUnGraphique_.setEnabled(true);
      rbUnGraphique_.setSelected(true);
    } else if (_e.getSource() == rbElementChoisi_) {
      rbUnGraphique_.setEnabled(false);
      rbDesGraphiques_.setSelected(true);
    } else if (_e.getSource() == bValider_) {
      if (SinaviImplementation.sinavifilleresultats_ == null) {
        SinaviImplementation.sinavifilleresultats_ = new SinaviFilleResultats(app_, construitTableauElements(),
            rbUnGraphique_.isSelected(), (String) lResultatsPossibles_.getSelectedValue());
        app_.addInternalFrame(SinaviImplementation.sinavifilleresultats_);
      } else {
        SinaviImplementation.sinavifilleresultats_ = null;
        SinaviImplementation.sinavifilleresultats_ = new SinaviFilleResultats(app_, construitTableauElements(),
            rbUnGraphique_.isSelected(), (String) lResultatsPossibles_.getSelectedValue());
        app_.addInternalFrame(SinaviImplementation.sinavifilleresultats_);
      }
    } else if (_e.getSource() == bAnnuler_) {
      try {
        setClosed(true);
      } catch (final Exception _pve) {}
    }
  }
}
