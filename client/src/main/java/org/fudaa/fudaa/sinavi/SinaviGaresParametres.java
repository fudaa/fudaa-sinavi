/*
 * @file         SinaviGaresParametres.java
 * @creation     2001-05-17
 * @modification $Date: 2006-09-19 15:09:00 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.sinavi;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.EmptyBorder;

import com.memoire.bu.*;

import org.fudaa.dodico.corba.navigation.IGare;
import org.fudaa.dodico.corba.navigation.IGareHelper;
import org.fudaa.dodico.corba.navigation.IGenerationDeterministeHelper;
import org.fudaa.dodico.corba.navigation.IGenerationHelper;
import org.fudaa.dodico.corba.navigation.IGenerationJournaliereAleatoire;
import org.fudaa.dodico.corba.navigation.IGenerationJournaliereAleatoireHelper;
import org.fudaa.dodico.corba.navigation.INavireType;
import org.fudaa.dodico.corba.navigation.INavireTypeHelper;
import org.fudaa.dodico.corba.navigation.ITrajetFluvialHelper;

/**
 * impl�mentation de l'onglet "Gare" pour les param�tres de Sinavi.
 * 
 * @version $Revision: 1.8 $ $Date: 2006-09-19 15:09:00 $ by $Author: deniger $
 * @author Aline Marechalle , Franck Lejeune
 */
public class SinaviGaresParametres extends BuPanel implements ActionListener, MouseListener {

  /**
   * panel contenant un label "nom" et un champs pour remplir le nom de la gare
   */
  private final BuPanel pNomGare_ = new BuPanel();
  /**
   * bouton valider
   */
  private final BuButton bValiderGare_ = new BuButton(SinaviResource.SINAVI.getIcon("VALIDER"), "Enregistrer");
  /**
   * champs pour renseigner le nombre maximum de navires
   */
  private final BuTextField tfNombreMaxNavires_ = BuTextField.createIntegerField();
  /**
   * nom de la gare
   */
  private final BuTextField tfNomGare_ = new BuTextField();
  /**
   * liste des gares cr��es
   */
  private final JList lGaresCreees_ = new JList();
  /**
   * ascenseur pour la liste des gares cr��es
   */
  private final JScrollPane GaresCreeesScrollPane_ = new JScrollPane(lGaresCreees_,
      ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
  /**
   * liste des types de bateaux
   */
  private final JList lMenuTypeBateaux_ = new JList();
  /**
   * ascenseur pour la liste des types de bateaux
   */
  private final JScrollPane MenuTypeBateauxScrollPane_ = new JScrollPane(lMenuTypeBateaux_,
      ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
  /**
   * liste des types de bateaux partant de la gare s�lectionn�e
   */
  private final JList lBateauxPartant_ = new JList();
  /**
   * ascenseur pour la liste des types de bateaux partant de la gare s�lectionn�e
   */
  private final JScrollPane BateauxPartantScrollPane_ = new JScrollPane(lBateauxPartant_,
      ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
  /**
   * bouton ajouter le type de bateau s�lectionn� aux types de bateaux partant
   */
  private final BuButton bAjouterBateauPartant_ = new BuButton(SinaviResource.SINAVI.getIcon("AJOUTER"), "Ajouter");
  /**
   * bouton supprimer le type de bateau partant s�lectionn�
   */
  private final BuButton bSupprimerBateauPartant_ = new BuButton(SinaviResource.SINAVI.getIcon("ANNULER"), "Supprimer");
  /**
   * table rappelant les trajets d�finit pour le type de bateau partant s�lectionn�
   */
  private final BuTable tTrajetBateau_ = new BuTable(SinaviImplementation.SINAVITABLETRAJETMODEL);
  /**
   * ascenseur pour la table rappelant les trajets d�finit pour le type de bateau partant s�lectionn�
   */
  private final JScrollPane TrajetBateauScrollPane_ = new JScrollPane(tTrajetBateau_,
      ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
  /**
   * bouton pour cr�er un trajet
   */
  private final BuButton bCreerTrajet_ = new BuButton(SinaviResource.SINAVI.getIcon("VALIDER"), "Cr�er Trajet");
  /**
   * bouton pour supprimer un trajet
   */
  private final BuButton bSupprimerTrajet_ = new BuButton(SinaviResource.SINAVI.getIcon("ANNULER"), "Supprimer Trajet");
  /**
   * table rappelant les loi associ�es � un trajet
   */
  private final BuTable tLoiTrajet_ = new BuTable(SinaviImplementation.SINAVITABLELOIMODEL);
  /**
   * ascenseur pour la table rappelant les loi associ�es � un trajet
   */
  private final JScrollPane LoiTrajetScrollPane_ = new JScrollPane(tLoiTrajet_,
      ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
  /**
   * bouton pour cr�er une loi
   */
  private final BuButton bCreerLoi_ = new BuButton(SinaviResource.SINAVI.getIcon("VALIDER"), "Cr�er Loi");
  /**
   * bouton pour voir une loi
   */
  private final BuButton bVoirLoi_ = new BuButton(SinaviResource.SINAVI.getIcon("VOIR"), "Voir Loi");
  /**
   * panel pour les boutons "cr�er" et "voir" de la loi
   */
  private final BuPanel pCreerVoirLoi_ = new BuPanel();
  /**
   * bouton pour supprimer une loi
   */
  private final BuButton bSupprimerLoi_ = new BuButton(SinaviResource.SINAVI.getIcon("ANNULER"), "Supprimer Loi");
  /**
   * une instance de SinaviImplementation
   */
  private BuCommonImplementation app_;
  /**
   * la gare courante ( s�lectionn�e dans la liste )
   */
  private IGare gareCourante_;
  /**
   * le navire partant courant ( s�lectionn� dans la liste )
   */
  private INavireType navireCourant_;
  /**
   * une fen�tre interne pour renseigner le trajet
   */
  private SinaviFilleTrajetBateau sftb_;
  /**
   * une fen�tre interne pour choisir le type de loi ( d�terministe ou al�atoire )
   */
  private SinaviFilleChoixLoiTrajet sfclt_;
  /**
   * booleen pour savoir si on est en mode �dition
   */
  private boolean mode_edition_;
  /**
   * panel contenant les boutons, il est plac� en bas � droite
   */
  private final BuPanel pBoutons_ = new BuPanel();
  /**
   * panel contenant les diff�rents champs de l'onglet
   */
  private final BuPanel pDonnees_ = new BuPanel();
  /**
   * panel contenant le champs de texte message
   */
  private final BuPanel pMessage_ = new BuPanel();
  /**
   * panel contenant les panels boutons et message
   */
  private final BuPanel pSud_ = new BuPanel();
  /**
   * panel contenant le texte concernant les types de bateaux partant et la liste correspondante
   */
  private final BuPanel pBateauxPartant_ = new BuPanel();
  /**
   * champ de texte pour afficher des messages
   */
  private final BuTextField tfMessage_ = new BuTextField();

  /**
   * Cr�ation de l'onglet.
   * 
   * @param _appli doit etre instance de SinaviImplementation.
   * @param _mode_edition mode �dition � partir du r�seau
   */
  public SinaviGaresParametres(final BuCommonInterface _appli, boolean _mode_edition) {
    super();
    app_ = _appli.getImplementation();
    sftb_ = null;
    mode_edition_ = _mode_edition;
    // Cr�ation du layout manager et des contraintes du panel donnees
    final GridBagLayout lm_ = new GridBagLayout();
    final GridBagConstraints c_ = new GridBagConstraints();
    // Cr�ation du layout manager et du bord de l'onglet
    this.setLayout(new BorderLayout());
    this.setBorder(new EmptyBorder(0, 0, 0, 0));
    // layout des panels
    pDonnees_.setLayout(lm_);
    pMessage_.setLayout(new FlowLayout(FlowLayout.LEFT));
    pBoutons_.setLayout(new FlowLayout(FlowLayout.RIGHT));
    pSud_.setLayout(new BorderLayout());
    // Largeur des zones de texte
    tfNombreMaxNavires_.setColumns(5);
    tfNomGare_.setPreferredSize(new Dimension(100, 28));
    tfMessage_.setColumns(15);
    tfMessage_.setEditable(false);
    // un panel pour les deux champs concernants le nom de la gare
    pNomGare_.setLayout(new BuGridLayout());
    pNomGare_.setBorder(new EmptyBorder(0, 0, 0, 0));
    pNomGare_.add(new BuLabel("Nom :   "));
    pNomGare_.add(tfNomGare_);
    // un panel pour les deux champs concernants le nom de la gare
    pBateauxPartant_.setLayout(new BuGridLayout(1));
    pBateauxPartant_.setBorder(new EmptyBorder(0, 0, 0, 0));
    pBateauxPartant_.add(new BuLabelMultiLine("Type de bateaux\nsusceptible de partir"));
    pBateauxPartant_.add(MenuTypeBateauxScrollPane_);
    // deux boutons rassembles en un seul panel
    pCreerVoirLoi_.setLayout(new BuGridLayout(2, 0, 0, true, true));
    pCreerVoirLoi_.add(bCreerLoi_);
    pCreerVoirLoi_.add(bVoirLoi_);
    // Contrainte des listes et des tables
    final Dimension dimgarescreees_ = new Dimension(150, 200);
    final Dimension dimtypemenubateau_ = new Dimension(150, 85);
    final Dimension dimbateaupartant_ = new Dimension(150, 80);
    final Dimension dimtable_ = new Dimension(250, 80);
    GaresCreeesScrollPane_.setPreferredSize(dimgarescreees_);
    lGaresCreees_.setCellRenderer(new SinaviGareCellRenderer());
    lGaresCreees_.setModel(SinaviImplementation.SINAVIGARELISTMODEL);
    MenuTypeBateauxScrollPane_.setPreferredSize(dimtypemenubateau_);
    lMenuTypeBateaux_.setCellRenderer(new SinaviBateauCellRenderer());
    lMenuTypeBateaux_.setModel(SinaviImplementation.SINAVIBATEAULISTMODEL);
    BateauxPartantScrollPane_.setPreferredSize(dimbateaupartant_);
    lBateauxPartant_.setCellRenderer(new SinaviBateauCellRenderer());
    lBateauxPartant_.setModel(SinaviImplementation.SINAVIBATEAUPARTANTLISTMODEL);
    TrajetBateauScrollPane_.setPreferredSize(dimtable_);
    LoiTrajetScrollPane_.setPreferredSize(dimtable_);
    tTrajetBateau_.getColumnModel().getColumn(0).setCellRenderer(new SinaviTrajetCellRenderer());
    tLoiTrajet_.getColumnModel().getColumn(0).setCellRenderer(new SinaviLoiCellRenderer());
    // Mise a blanc des tables
    SinaviImplementation.SINAVITABLETRAJETMODEL.setBateau(null);
    SinaviImplementation.SINAVIBATEAUPARTANTLISTMODEL.setGare(null);
    SinaviImplementation.SINAVITABLETRAJETMODEL.setGare(null);
    SinaviImplementation.SINAVITABLELOIMODEL.setTrajetFluvial(null);
    // impossibilite de redimmensionner les colonnes des tables
    tTrajetBateau_.getColumnModel().getColumn(0).setResizable(false);
    tTrajetBateau_.getColumnModel().getColumn(1).setResizable(false);
    tLoiTrajet_.getColumnModel().getColumn(0).setResizable(false);
    tLoiTrajet_.getColumnModel().getColumn(1).setResizable(false);
    // Ajout des listeners aux elements
    bValiderGare_.addActionListener(this);
    bAjouterBateauPartant_.addActionListener(this);
    bSupprimerBateauPartant_.addActionListener(this);
    bCreerTrajet_.addActionListener(this);
    bSupprimerTrajet_.addActionListener(this);
    bCreerLoi_.addActionListener(this);
    bVoirLoi_.addActionListener(this);
    bSupprimerLoi_.addActionListener(this);
    lMenuTypeBateaux_.addMouseListener(this);
    lGaresCreees_.addMouseListener(this);
    lBateauxPartant_.addMouseListener(this);
    tTrajetBateau_.addMouseListener(this);
    tLoiTrajet_.addMouseListener(this);
    // Contraintes des boutons
    final Dimension dimpetitbouton_ = new Dimension(150, 33);
    final Dimension dimgrandbouton_ = new Dimension(250, 33);
    bAjouterBateauPartant_.setPreferredSize(dimpetitbouton_);
    bSupprimerBateauPartant_.setPreferredSize(dimpetitbouton_);
    bCreerTrajet_.setPreferredSize(dimgrandbouton_);
    bSupprimerTrajet_.setPreferredSize(dimgrandbouton_);
    pCreerVoirLoi_.setPreferredSize(dimgrandbouton_);
    bCreerLoi_.setPreferredSize(new Dimension(125, 37));
    bSupprimerLoi_.setPreferredSize(dimgrandbouton_);
    // Boutons "valider", "creer" et "Supprimer" pas visible par defaut
    bAjouterBateauPartant_.setEnabled(false);
    bSupprimerBateauPartant_.setEnabled(false);
    bCreerTrajet_.setEnabled(false);
    bSupprimerTrajet_.setEnabled(false);
    bCreerLoi_.setEnabled(false);
    bVoirLoi_.setEnabled(false);
    bSupprimerLoi_.setEnabled(false);
    bValiderGare_.setEnabled(false);
    // Contraintes communes � tous les composants
    c_.gridy = GridBagConstraints.RELATIVE;
    c_.anchor = GridBagConstraints.WEST;
    c_.ipadx = 5;
    c_.ipady = 5;
    // Remplissage de la premiere colonne
    c_.gridx = 0;
    placeComposant(lm_, new BuLabel("Nombre maximum de navire :"), c_);
    placeComposant(lm_, pNomGare_, c_);
    if (_mode_edition) {
      placeComposant(lm_, new BuLabel(""), c_);
    } else {
      placeComposant(lm_, new BuLabel("Gares Cr��es :"), c_);
    }
    c_.gridheight = GridBagConstraints.REMAINDER;
    if (_mode_edition) {
      placeComposant(lm_, new BuPicture(SinaviResource.SINAVI.getImage("sinaviimagegare")), c_);
    } else {
      placeComposant(lm_, GaresCreeesScrollPane_, c_);
    }
    // Remplissage de la deuxieme colonne
    c_.gridx = 1;
    c_.insets.top = 0;
    c_.insets.left = 5;
    c_.gridheight = 1;
    placeComposant(lm_, tfNombreMaxNavires_, c_);
    c_.gridheight = 5;
    placeComposant(lm_, pBateauxPartant_, c_);
    c_.gridheight = 1;
    placeComposant(lm_, new BuLabel("Bateaux Partant :"), c_);
    placeComposant(lm_, bAjouterBateauPartant_, c_);
    placeComposant(lm_, BateauxPartantScrollPane_, c_);
    placeComposant(lm_, bSupprimerBateauPartant_, c_);
    // Remplissage de la troisieme colonne
    c_.gridx = 2; // troisieme colonne
    c_.gridheight = 1;
    placeComposant(lm_, bCreerTrajet_, c_);
    c_.gridheight = 4;
    placeComposant(lm_, TrajetBateauScrollPane_, c_);
    c_.gridheight = 1;
    placeComposant(lm_, bSupprimerTrajet_, c_);
    placeComposant(lm_, new BuLabel(""), c_);
    placeComposant(lm_, pCreerVoirLoi_, c_);
    placeComposant(lm_, LoiTrajetScrollPane_, c_);
    placeComposant(lm_, bSupprimerLoi_, c_);
    // ajout du bouton valider au panel boutons
    pBoutons_.add(bValiderGare_);
    // ajout du message au panel message
    pMessage_.add(tfMessage_);
    // ajout de message et boutons a sud
    pSud_.add(pMessage_, BorderLayout.WEST);
    if (!_mode_edition) {
      pSud_.add(pBoutons_, BorderLayout.EAST);
    }
    // ajout des deux panels � l'onglet
    add(pDonnees_, BorderLayout.CENTER);
    add(pSud_, BorderLayout.SOUTH);
  }

  /**
   * methode permettant de sauvegarder les parametres de la gare
   * 
   * @param _gare
   */
  public void setGareParametres(final IGare _gare) {
    if (validGareParametres()) {
      _gare.nom(tfNomGare_.getText());
      _gare.nombreMaxNavire(((Integer) tfNombreMaxNavires_.getValue()).intValue());
    } else {
      final BuDialogMessage dialog_mess = new BuDialogMessage(app_, SinaviImplementation.informationsSoftware(),
          "Param�tres de la gare\nincomplets");
      dialog_mess.activate();
    }
  }

  /**
   * M�thode utile pour placer un composant avec ses contraintes dans le panel "donnees"
   * 
   * @param _lm le layout manager
   * @param _composant le composant
   * @param _c les contraintes
   */
  public void placeComposant(final GridBagLayout _lm, final Component _composant, final GridBagConstraints _c) {
    _lm.setConstraints(_composant, _c);
    pDonnees_.add(_composant);
  }

  /**
   * si clic sur la liste des gares cr��es, mise � jour des champs de l'onglet bouton valider gare valide si clic sur la
   * liste des types de bateau, bouton ajouter bateau partant valide, mise a jour de l'onglet si clic sur la liste des
   * bateaux partant , mise a jour de l'onglet, boutons supprimer bateau partant valide, bouton creer trajet valide si
   * clic sur la table des trajets, bouton supprimer trajet valide et bouton creer loi valide si clilc sur la table des
   * lois, bouton supprimer et voir loi valides
   * 
   * @param _e
   */
  public void mouseClicked(final MouseEvent _e) {
    if (_e.getSource() == lGaresCreees_) {
      if (lGaresCreees_.getSelectedValue() == null) {
        // System.out.println("liste vide");
      } else {
        setGareCourante(IGareHelper.narrow((org.omg.CORBA.Object) lGaresCreees_.getSelectedValue()));
        getGareParametres(gareCourante_);
        SinaviImplementation.SINAVITABLETRAJETMODEL.setGare(null);
        SinaviImplementation.SINAVITABLETRAJETMODEL.setBateau(null);
        SinaviImplementation.SINAVIBATEAUPARTANTLISTMODEL.setGare(gareCourante_);
        SinaviImplementation.SINAVITABLELOIMODEL.setTrajetFluvial(null);
        bValiderGare_.setEnabled(true);
      }
    } else if (_e.getSource() == lMenuTypeBateaux_) {
      if (lMenuTypeBateaux_.getSelectedValue() == null) {
        // System.out.println("liste vide");
      } else if ((lGaresCreees_.getSelectedValue() != null) || mode_edition_) {
        bAjouterBateauPartant_.setEnabled(true);
        SinaviImplementation.SINAVITABLETRAJETMODEL.setBateau(null);
        // SinaviImplementation.SINAVIBATEAUPARTANTLISTMODEL.setGare(null);
        SinaviImplementation.SINAVITABLETRAJETMODEL.setGare(null);
        SinaviImplementation.SINAVITABLELOIMODEL.setTrajetFluvial(null);
      }
    } else if (_e.getSource() == lBateauxPartant_) {
      if (lBateauxPartant_.getSelectedValue() == null) {
        // System.out.println("liste vide");
      } else {
        setNavireCourant(INavireTypeHelper.narrow((org.omg.CORBA.Object) lBateauxPartant_.getSelectedValue()));
        SinaviImplementation.SINAVITABLETRAJETMODEL.setGare(gareCourante_);
        SinaviImplementation.SINAVITABLETRAJETMODEL.setBateau(navireCourant_);
        SinaviImplementation.SINAVITABLELOIMODEL.setTrajetFluvial(null);
        bSupprimerBateauPartant_.setEnabled(true);
        bCreerTrajet_.setEnabled(true);
        bCreerLoi_.setEnabled(false);
        bVoirLoi_.setEnabled(false);
      }
    } else if (_e.getSource() == tTrajetBateau_) {
      bSupprimerTrajet_.setEnabled(true);
      SinaviImplementation.SINAVITABLELOIMODEL.setTrajetFluvial(ITrajetFluvialHelper
          .narrow((org.omg.CORBA.Object) tTrajetBateau_.getValueAt(tTrajetBateau_.getSelectedRow(), 0)));
      bCreerLoi_.setEnabled(true);
      bVoirLoi_.setEnabled(false);
    } else if (_e.getSource() == tLoiTrajet_) {
      bSupprimerLoi_.setEnabled(true);
      bVoirLoi_.setEnabled(true);
    }
  }

  /**
   * pas utilis�
   * 
   * @param e
   */
  public void mousePressed(final MouseEvent e) {}

  /**
   * pas utilis�
   * 
   * @param e
   */
  public void mouseReleased(final MouseEvent e) {}

  /**
   * pas utilis�
   * 
   * @param e
   */
  public void mouseEntered(final MouseEvent e) {}

  /**
   * pas utilis�
   * 
   * @param e
   */
  public void mouseExited(final MouseEvent e) {}

  /**
   * si action sur le bouton valider, sauvegarde des param�tres de la gare si action sur la bouton ajouter bateau
   * partant, test si d�j� partant, si non, affichage de plusieurs fen�tres internes pour d�finir un trajet puis une loi
   * associ�e � ce type de bateau si action sur le bouton supprimer bateau partant, suppression du type de navire
   * s�lectionn� et mise � jour de l'onglet si action sur le bouton creer trajet afichage de plusieurs fen�tres internes
   * pour d�finir le trajet puis la loi si action sur le bouton supprimer trajet, suppression du trajet s�lectionn� et
   * mise � jour de l'onglet si action sur le bouton creer loi affichage de plusieurs fen�tres pour d�finir la loi si
   * action sur le bouton supprimer loi, suppression de la loi s�lectionn�e et mise � jour de l'onglet si action sur le
   * bouton voir loi affichage d'une fen�tre interne pour afficher les param�tres de la loi
   * 
   * @param _e
   */
  public void actionPerformed(final ActionEvent _e) {
    if (_e.getSource() == bValiderGare_) {
      if (validGareParametres()) {
        setGareParametres(gareCourante_);
        bValiderGare_.setEnabled(false);
        getGareParametres(gareCourante_);
      } else {
        final BuDialogMessage dialog_mess = new BuDialogMessage(app_, SinaviImplementation.informationsSoftware(),
            "Nombre Maximum de bateau incorrect");
        dialog_mess.activate();
      }
    } else if (_e.getSource() == bAjouterBateauPartant_) {
      if (lMenuTypeBateaux_.getSelectedValue() != null) {
        if ((SinaviImplementation.ETUDE_SINAVI.dateDebutSimulation() == 0)
            || (SinaviImplementation.ETUDE_SINAVI.dateFinSimulation() == 0)) {
          final BuDialogMessage dialog_mess = new BuDialogMessage(
              app_,
              SinaviImplementation.informationsSoftware(),
              "Les dates de d�but de Simulation\net de fin de Simulation ne sont pas renseign�es\nVeillez les affecter dans\nl'onglet \"Donn�es G�n�rales\"");
          dialog_mess.activate();
        } else if (gareCourante_.navirePartant(INavireTypeHelper.narrow((org.omg.CORBA.Object) lMenuTypeBateaux_
            .getSelectedValue()))) {
          final BuDialogMessage dialog_mess = new BuDialogMessage(app_, SinaviImplementation.informationsSoftware(),
              "Bateau d�j� partant");
          dialog_mess.activate();
        } else {
          setNavireCourant(INavireTypeHelper.narrow((org.omg.CORBA.Object) lMenuTypeBateaux_.getSelectedValue()));
          SinaviImplementation.SINAVITABLETRAJETMODEL.setBateau(navireCourant_);
          SinaviImplementation.SINAVITABLETRAJETMODEL.setGare(gareCourante_);
          SinaviImplementation.SINAVIBATEAUPARTANTLISTMODEL.setGare(gareCourante_);
          System.out.println(navireCourant_.type());
          bAjouterBateauPartant_.setEnabled(false);
          if (sftb_ == null) {
            sftb_ = new SinaviFilleTrajetBateau(app_, gareCourante_, navireCourant_);
            app_.addInternalFrame(sftb_);
          } else {
            sftb_.setVisible(false);
            sftb_ = null;
            sftb_ = new SinaviFilleTrajetBateau(app_, gareCourante_, navireCourant_);
            app_.addInternalFrame(sftb_);
          }
        }
      } else {
        final BuDialogMessage dialog_mess = new BuDialogMessage(app_, SinaviImplementation.informationsSoftware(),
            "Pas de type de navires\ns�lectionn�");
        dialog_mess.activate();
      }
    } else if (_e.getSource() == bSupprimerBateauPartant_) {
      gareCourante_.enleveTrajetsNavire(INavireTypeHelper.narrow((org.omg.CORBA.Object) lBateauxPartant_
          .getSelectedValue()));
      SinaviImplementation.SINAVITABLETRAJETMODEL.refresh();
      SinaviImplementation.SINAVIBATEAUPARTANTLISTMODEL.refresh();
      SinaviImplementation.SINAVITABLELOIMODEL.setTrajetFluvial(null);
    } else if (_e.getSource() == bCreerTrajet_) {
      if (lBateauxPartant_.getSelectedValue() != null) {
        if (sftb_ == null) {
          sftb_ = new SinaviFilleTrajetBateau(app_, gareCourante_, navireCourant_);
          app_.addInternalFrame(sftb_);
        } else {
          sftb_.setVisible(false);
          sftb_ = null;
          sftb_ = new SinaviFilleTrajetBateau(app_, gareCourante_, navireCourant_);
          app_.addInternalFrame(sftb_);
        }
      } else {
        final BuDialogMessage dialog_mess = new BuDialogMessage(app_, SinaviImplementation.informationsSoftware(),
            "Aucun type de bateau\npartant s�lectionn�");
        dialog_mess.activate();
      }
    } else if (_e.getSource() == bSupprimerTrajet_) {
      if (tTrajetBateau_.getSelectedRow() != -1) {
        gareCourante_.enleveTrajet(ITrajetFluvialHelper.narrow((org.omg.CORBA.Object) tTrajetBateau_.getValueAt(
            tTrajetBateau_.getSelectedRow(), 0)));
        SinaviImplementation.SINAVITABLETRAJETMODEL.refresh();
        SinaviImplementation.SINAVIBATEAUPARTANTLISTMODEL.refresh();
        SinaviImplementation.SINAVITABLELOIMODEL.setTrajetFluvial(null);
      }
    } else if (_e.getSource() == bCreerLoi_) {
      if (tTrajetBateau_.getSelectedRow() != -1) {
        if (sfclt_ == null) {
          sfclt_ = new SinaviFilleChoixLoiTrajet(app_, ITrajetFluvialHelper
              .narrow((org.omg.CORBA.Object) tTrajetBateau_.getValueAt(tTrajetBateau_.getSelectedRow(), 0)));
          app_.addInternalFrame(sfclt_);
        } else {
          sfclt_.setVisible(false);
          sfclt_ = null;
          sfclt_ = new SinaviFilleChoixLoiTrajet(app_, ITrajetFluvialHelper
              .narrow((org.omg.CORBA.Object) tTrajetBateau_.getValueAt(tTrajetBateau_.getSelectedRow(), 0)));
          app_.addInternalFrame(sfclt_);
        }
      } else {
        final BuDialogMessage dialog_mess = new BuDialogMessage(app_, SinaviImplementation.informationsSoftware(),
            "Aucun trajet s�lectionn�");
        dialog_mess.activate();
      }
    } else if (_e.getSource() == bVoirLoi_) {
      if (tTrajetBateau_.getSelectedRow() != -1) {
        if (tLoiTrajet_.getValueAt(tLoiTrajet_.getSelectedRow(), 0) instanceof IGenerationJournaliereAleatoire) {
          app_.addInternalFrame(new SinaviFilleLoiAleatoireTrajet(app_, ITrajetFluvialHelper
              .narrow((org.omg.CORBA.Object) tTrajetBateau_.getValueAt(tTrajetBateau_.getSelectedRow(), 0)),
              IGenerationJournaliereAleatoireHelper.narrow((org.omg.CORBA.Object) tLoiTrajet_.getValueAt(tLoiTrajet_
                  .getSelectedRow(), 0))));
        } else {
          app_.addInternalFrame(new SinaviFilleLoiDeterministeTrajet(ITrajetFluvialHelper
              .narrow((org.omg.CORBA.Object) tTrajetBateau_.getValueAt(tTrajetBateau_.getSelectedRow(), 0)),
              IGenerationDeterministeHelper.narrow((org.omg.CORBA.Object) tLoiTrajet_.getValueAt(tLoiTrajet_
                  .getSelectedRow(), 0))));
        }
      } else {
        final BuDialogMessage dialog_mess = new BuDialogMessage(app_, SinaviImplementation.informationsSoftware(),
            "Aucun trajet s�lectionn�");
        dialog_mess.activate();
      }
    } else if (_e.getSource() == bSupprimerLoi_) {
      if (tTrajetBateau_.getSelectedRow() != -1) {
        (ITrajetFluvialHelper.narrow((org.omg.CORBA.Object) tTrajetBateau_.getValueAt(tTrajetBateau_.getSelectedRow(),
            0))).enleveGeneration(IGenerationHelper.narrow((org.omg.CORBA.Object) tLoiTrajet_.getValueAt(tLoiTrajet_
            .getSelectedRow(), 0)));
        SinaviImplementation.SINAVITABLELOIMODEL.refresh();
      }
    }
  }

  /**
   * methode pour valider les param�tres de la gare
   * 
   * @return ...
   */
  public boolean validGareParametres() {
    if (tfNomGare_.getText().equals(null)) {
      return false;
    }
    if (((Integer) tfNombreMaxNavires_.getValue()).intValue() == 0) {
      return false;
    }
    return true;
  }

  /**
   * methode permettant de definir la gare courante
   * 
   * @param _gareCourante La nouvelle valeur GareCourante
   */
  public void setGareCourante(final IGare _gareCourante) {
    gareCourante_ = _gareCourante;
  }

  /**
   * methode permettant de definir le bateau courant
   * 
   * @param _navireCourant La nouvelle valeur NavireCourant
   */
  public void setNavireCourant(final INavireType _navireCourant) {
    navireCourant_ = _navireCourant;
  }

  /**
   * mise a jour des champs de la gare s�lectionn�e dans la liste
   * 
   * @param _gare la gare s�lectionn�e dans la liste
   */
  public void getGareParametres(final IGare _gare) {
    if (_gare.nombreMaxNavire() != 0) {
      tfMessage_.setForeground(Color.green);
      tfMessage_.setText("Param�tres enregistr�s");
      tfNomGare_.setText(_gare.nom());
      tfNombreMaxNavires_.setValue(new Integer(_gare.nombreMaxNavire()));
    } else {
      tfMessage_.setForeground(Color.red);
      tfMessage_.setText("Param�tres non enregistr�s");
      tfNomGare_.setText(_gare.nom());
      tfNombreMaxNavires_.setValue(new Integer(1000));
    }
  }
}
