/*
 * @file         SinaviAnimationNord.java
 * @creation     2001-05-17
 * @modification $Date: 2006-09-19 15:08:59 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.sinavi;

import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import javax.swing.JProgressBar;
import javax.swing.border.EmptyBorder;

import com.memoire.bu.BuLabel;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuPicture;
import com.memoire.bu.BuTextField;

/**
 * ....
 * 
 * @version $Revision: 1.7 $ $Date: 2006-09-19 15:08:59 $ by $Author: deniger $
 * @author Aline Marechalle , Franck Lejeune
 */
public class SinaviAnimationNord extends BuPanel implements ActionListener, FocusListener {

  /**
   * Barre de progression pour l'�tat d'avancement.
   */
  protected static JProgressBar progres_ = new JProgressBar();
  /**
   * Affichage du delta t.
   */
  protected static BuTextField valDeltat_ = BuTextField.createIntegerField();
  /**
   * Affichage de ce qui concerne la graine.
   */
  private final BuLabel textGraine_ = new BuLabel();
  /**
   * Affichage de l'heure de la simulation.
   */
  private final BuLabel textHeure_ = new BuLabel();
  /**
   * Affichage du jour de la simulation.
   */
  private final BuLabel textJour_ = new BuLabel();

  /**
   * SinaviAnimationNord Constructeur qui met en page le cot� nord de l'animation: l�gende - graine - delta t - jour -
   * barre de progression.
   */
  public SinaviAnimationNord() {
    setLayout(new BorderLayout());
    // setBackground(Color.white);
    /* NORD OUEST */
    final BuPanel ouest = new BuPanel();
    ouest.setBorder(new EmptyBorder(10, 5, 5, 5));
    final GridBagLayout gridbag = new GridBagLayout();
    ouest.setLayout(gridbag);
    // on cr�e une instance de la classe GridBagConstraints qui va contenir
    // toutes les contraintes d'un objet par rapport a la grille
    final GridBagConstraints constraints = new GridBagConstraints();
    // label sens
    final BuLabel montant = new BuLabel("Sens Montant");
    SinaviAnimation.buildConstraints(constraints, 0, 0, 2, 1, 100, 100);
    constraints.anchor = GridBagConstraints.WEST;
    gridbag.setConstraints(montant, constraints);
    ouest.add(montant);
    constraints.insets.top = 0;
    // fleche
    final Image image = SinaviResource.SINAVI.getImage("flechemont");
    final BuPicture flechemont = new BuPicture(image);
    SinaviAnimation.buildConstraints(constraints, 0, 1, 2, 1, 100, 100);
    constraints.anchor = GridBagConstraints.CENTER;
    gridbag.setConstraints(flechemont, constraints);
    ouest.add(flechemont);
    /* NORD CENTRE */
    final BuPanel centre = new BuPanel();
    centre.setBorder(new EmptyBorder(10, 5, 5, 5));
    final GridBagLayout gridcentr = new GridBagLayout();
    constraints.fill = GridBagConstraints.NONE;
    centre.setLayout(gridcentr);
    constraints.insets.right = 0;
    constraints.insets.left = 0;
    /* Traitement de la graine */
    final BuLabel labGraine = new BuLabel("Graine : ");
    // col 0 lig 0 s'�tend sur 1 col 1 lig
    constraints.anchor = GridBagConstraints.EAST;
    SinaviAnimation.buildConstraints(constraints, 0, 0, 1, 1, 100, 100);
    gridcentr.setConstraints(labGraine, constraints);
    centre.add(labGraine);
    constructTextGraine(0, 0);
    constraints.anchor = GridBagConstraints.WEST;
    SinaviAnimation.buildConstraints(constraints, 1, 0, 1, 1, 100, 100);
    gridcentr.setConstraints(textGraine_, constraints);
    centre.add(textGraine_);
    /* Traitement du delta t */
    final BuLabel labDeltat = new BuLabel("Delta t : ");
    constraints.anchor = GridBagConstraints.EAST;
    SinaviAnimation.buildConstraints(constraints, 0, 1, 1, 1, 100, 100);
    gridcentr.setConstraints(labDeltat, constraints);
    centre.add(labDeltat);
    valDeltat_.addActionListener(this);
    valDeltat_.addFocusListener(this);
    final Integer valRecieve_ = new Integer(10);
    valDeltat_.setValue(valRecieve_);
    valDeltat_.setEnabled(false);
    valDeltat_.setColumns(2);
    // valDeltat_.setBackground(TRANSLUCENT);
    // constraints.insets.left = 10;
    constraints.anchor = GridBagConstraints.WEST;
    SinaviAnimation.buildConstraints(constraints, 1, 1, 1, 1, 100, 100);
    gridcentr.setConstraints(valDeltat_, constraints);
    centre.add(valDeltat_);
    /* NORD EST */
    final BuPanel est = new BuPanel();
    est.setBorder(new EmptyBorder(10, 5, 10, 5));
    final GridBagLayout gridest = new GridBagLayout();
    est.setLayout(gridest);
    constraints.anchor = GridBagConstraints.CENTER;
    constraints.insets.bottom = 5;
    constructTextJour(0);
    SinaviAnimation.buildConstraints(constraints, 0, 0, 1, 1, 100, 100);
    gridest.setConstraints(textJour_, constraints);
    est.add(textJour_);
    constructTextHeure(0, 0);
    SinaviAnimation.buildConstraints(constraints, 1, 0, 1, 1, 100, 100);
    gridest.setConstraints(textHeure_, constraints);
    est.add(textHeure_);
    constraints.insets.bottom = 0;
    // Barre de progression
    progres_.setValue(0);
    progres_.setStringPainted(true); // intitul� textuel %
    SinaviAnimation.buildConstraints(constraints, 0, 1, 2, 1, 100, 100);
    gridest.setConstraints(progres_, constraints);
    est.add(progres_);
    add("West", ouest);
    add("Center", centre);
    add("East", est);
  }

  /**
   * Construit la chaine n� graine sur nombre total.
   * 
   * @param _numGraine
   * @param _nbGraines
   */
  public void constructTextGraine(final int _numGraine, final int _nbGraines) {
    textGraine_.setText("n� " + _numGraine + "/" + _nbGraines);
  }

  /**
   * Construit la chaine de caract�re correspondant au jour de la simulation.
   * 
   * @param _noJour
   */
  public void constructTextJour(final int _noJour) {
    // Compteur des jours/heures de la simul
    String exposant = " �me";
    if (_noJour == 1) {
      exposant = exposant.replace('m', 'r');
      exposant = exposant.replace('e', ' ');
      exposant = exposant.replace('�', 'e');
    }
    final String numJour = (_noJour + exposant + " jour");
    textJour_.setText(numJour);
  }

  /**
   * Construit la chaine de caract�re correspondant � l'heure de simulation.
   * 
   * @param _hr
   * @param _min
   */
  public void constructTextHeure(final int _hr, final int _min) {
    String heure = "";
    if (_min < 10) {
      heure = (_hr + " h " + "0" + _min);
    } else {
      heure = (_hr + " h " + _min);
    }
    textHeure_.setText(heure);
  }

  /**
   * Ecoute si changement du delta t.
   * 
   * @param _evt
   */
  public void actionPerformed(final ActionEvent _evt) {
    if (_evt.getSource() == valDeltat_) {
      valDeltat_.requestFocus();
      // valDeltat_.setSelectionColor(Color.blue);
      valDeltat_.selectAll();
      SinaviImplementation.SERVICE_SINAVI.deltat(((Integer) valDeltat_.getValue()).intValue());
      SinaviAnimationSud.reprise_.requestFocus();
    }
  }

  /**
   * Quand on sort de la zone de texte plus de curseur clignotant.
   * 
   * @param _evt
   */
  public void focusLost(final FocusEvent _evt) {
    processFocusEvent(new FocusEvent(this, FocusEvent.FOCUS_LOST));
  }

  /**
   * Quand on entre dans la zone de texte c'est que l'on entre dans le composant.
   * 
   * @param _evt
   */
  public void focusGained(final FocusEvent _evt) {
    if (_evt.getSource() == valDeltat_) {
      valDeltat_.selectAll();
    }
  }
}
