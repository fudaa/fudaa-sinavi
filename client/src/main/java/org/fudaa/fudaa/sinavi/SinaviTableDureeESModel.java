/*
 * @file         SinaviTableDureeESModel.java
 * @creation     2001-05-17
 * @modification $Date: 2006-09-19 15:09:00 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.sinavi;

import javax.swing.table.AbstractTableModel;

import org.fudaa.dodico.corba.navigation.IEcluseFluviale;

/**
 * D�finit le mod�le pour la table affichant les dur�es de manoeuvres
 * 
 * @version $Revision: 1.6 $ $Date: 2006-09-19 15:09:00 $ by $Author: deniger $
 * @author Aline Marechalle , Franck Lejeune
 */
public class SinaviTableDureeESModel extends AbstractTableModel {

  /**
   * noms des colonnes
   */
  private final String[] nomsColonnes_ = { "Type de bateau", "Temps d'Entr�e", "Temps de Sortie", "Bassin�e Montante",
      "Bassin�e Avalante" };
  /**
   * objet m�tier �cluse � laquelle on va faire r�f�rence pour trouver les dur�es de manoeuvres
   */
  private IEcluseFluviale ecluse_;

  /**
   * Constructeur SinaviTableDureeESModel.
   */
  public SinaviTableDureeESModel() {}

  /**
   * retourne le nombre de colonne
   * 
   * @return le nombre de colonne
   */
  public int getColumnCount() {
    return nomsColonnes_.length;
  }

  /**
   * retourne le nombre de ligne donc le nombre de dur�es de manoeuvres
   * 
   * @return le nombre de dur�es de manoeuvres
   */
  public int getRowCount() {
    if (ecluse_ == null) {
      return 0;
    }
    return ecluse_.dureesManoeuvres().length;
  }

  /**
   * retourne le nom de la colonne
   * 
   * @param _col le numero de la colonne
   * @return le nom de la colonne
   */
  public String getColumnName(final int _col) {
    return nomsColonnes_[_col];
  }

  /**
   * retourne la valeur situ�e � la ligne row et la colonne col
   * 
   * @param _row la ligne
   * @param _col la colonne
   * @return la valeur situ�e � la ligne row et la colonne col
   */
  public Object getValueAt(final int _row, final int _col) {
    int secondes_;
    final DureeField df_ = new DureeField(false, false, true, true, false);
    if (ecluse_ == null) {
      return null;
    } else if (_col == 0) {
      return ecluse_.dureesManoeuvres()[_row].navireType();
    } else if (_col == 1) {
      secondes_ = (int) ecluse_.dureesManoeuvres()[_row].dureeEntree();
    } else if (_col == 2) {
      secondes_ = (int) ecluse_.dureesManoeuvres()[_row].dureeSortie();
    } else if (_col == 3) {
      secondes_ = (int) ecluse_.dureesManoeuvres()[_row].dureeBassineeMontante();
    } else {
      secondes_ = (int) ecluse_.dureesManoeuvres()[_row].dureeBassineeAvalante();
    }
    return df_.STRING_DUREE.valueToString(new Integer(secondes_));
  }

  /**
   * permet de rafraichir l'affichage de la table ( modification ou ajout d'une dur�e )
   */
  public void refresh() {
    fireTableDataChanged();
  }

  /**
   * d�finit l'�cluse � laquelle on va faire r�f�rence pour trouver les dur�es de manoeuvres
   * 
   * @param _ecluse l'�cluse
   */
  public void setEcluse(final IEcluseFluviale _ecluse) {
    ecluse_ = _ecluse;
    refresh();
  }
}
