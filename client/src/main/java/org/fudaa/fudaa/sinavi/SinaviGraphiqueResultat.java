/*
 * @file         SinaviGraphiqueResultat.java
 * @creation     
 * @modification $Date: 2006-09-19 15:09:00 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.sinavi;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.io.File;
import java.io.FileOutputStream;
import java.util.Vector;

import javax.swing.JComponent;

import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuPanel;

import org.fudaa.ctulu.gif.AcmeGifEncoder;
import org.fudaa.ebli.graphe.Axe;
import org.fudaa.ebli.graphe.BGraphe;
import org.fudaa.ebli.graphe.Contrainte;
import org.fudaa.ebli.graphe.CourbeDefault;
import org.fudaa.ebli.graphe.Graphe;
import org.fudaa.ebli.graphe.Valeur;

/**
 * @version $Revision: 1.12 $ $Date: 2006-09-19 15:09:00 $ by $Author: deniger $
 * @author Aline Marechalle , Franck Lejeune
 */
public class SinaviGraphiqueResultat extends JComponent {

  private final BGraphe graphique_ = new BGraphe();
  private final Graphe graphe_ = new Graphe();
  private final BuPanel pdessous = new BuPanel();
  private final Axe axe_des_x_ = new Axe();
  private final Axe axe_des_y_ = new Axe();
  private Image cache_;

  public SinaviGraphiqueResultat() {
    pdessous.setLayout(new BuGridLayout(3, 20, 10));
    axe_des_x_.graduations_ = true;
    axe_des_y_.graduations_ = true;
    axe_des_y_.vertical_ = true;
    graphe_.ajoute(axe_des_x_);
    graphe_.ajoute(axe_des_y_);
    graphique_.setGraphe(graphe_);
    this.setLayout(new BorderLayout());
    this.add(graphique_, BorderLayout.CENTER);
    this.add(pdessous, BorderLayout.SOUTH);
    pdessous.setBackground(Color.white);
  }

  public void setLibeleAxeX(final String _labelX) {
    axe_des_x_.titre_ = _labelX;
  }

  public void setLibeleAxeY(final String _labelY) {
    axe_des_y_.titre_ = _labelY;
  }

  public void setTitreGraphe(final String _titre) {
    graphe_.titre_ = _titre;
  }

  public BGraphe getGraphique() {
    return graphique_;
  }

  public void ajouteHistogramme(final String _titre, final Color _surface, final Color _contour,
      final double[] _valeursX, final double[] _valeursY, final int _nbinterval) {
    graphe_.marges_.droite_ = 10 + 7 * _titre.length();
    graphe_.marges_.gauche_ = 20;
    final CourbeDefault courbe = new CourbeDefault();
    courbe.titre_ = _titre;
    courbe.type_ = "histogramme";
    courbe.aspect_.largeur_ = 1;
    courbe.aspect_.surface_ = _surface;
    courbe.aspect_.contour_ = _contour;
    final Vector valeurs = new Vector();
    for (int i = 0; i < _valeursX.length; i++) {
      final Valeur v = new Valeur();
      if (i < _valeursX.length - 1) {
        v.s_ = (_valeursX[i] + _valeursX[i + 1]) / 2;
      } else {
        v.s_ = (_valeursX[i] + 10) / 2;
      }
      v.v_ = _valeursY[i];
      valeurs.addElement(v);
      System.out.println(v.s_ + " " + v.v_);
    }
    final double maxX = 10;
    final double maxY = 40;
    /*
     * double pas = _valeursX[_valeursX.length-1]/_nbinterval; System.out.println("pas "+pas); for (int i=0;i<_nbinterval;i++) {
     * Valeur v= new Valeur(); v.s=(i+1)*pas-pas/2; v.v = 0; for (int j=0;j<_valeursX.length;j++) { if (_valeursX[j] <
     * v.s) v.v=v.v+_valeursY[j]; } valeurs.addElement(v); if ( maxX < v.s) maxX=v.s; if ( maxY < v.v) maxY=v.v; total =
     * total + v.v; moy = (i+1)*pas*v.s; System.out.println(v.s+" "+v.v); } moy = moy / total;
     */
    courbe.valeurs_ = valeurs;
    if (axe_des_y_.maximum_ < maxY) {
      axe_des_y_.maximum_ = maxY;
    }
    if (axe_des_x_.maximum_ < maxX) {
      axe_des_x_.maximum_ = maxX;
    }
    graphe_.ajoute(courbe);
    final Contrainte moyenne = new Contrainte();
    moyenne.v_ = 35.46;
    moyenne.vertical_ = false;
    moyenne.type_ = "max";
    // graphe_.ajoute(moyenne);
    pdessous.add(new BuLabel("Moyenne pour " + _titre + " : " + moyenne.v_));
    pdessous.add(new BuLabel("Total Navire " + _titre + " : " + 151));
    pdessous.add(new BuLabel(""));
  }

  public void ajouteCourbe(final String _titre, final Color _contour, final double[] _valeursX, final double[] _valeursY) {
    graphe_.marges_.droite_ = 10 + 7 * _titre.length();
    graphe_.marges_.gauche_ = 20;
    axe_des_x_.maximum_ = _valeursX.length;
    final CourbeDefault courbe = new CourbeDefault();
    courbe.titre_ = _titre;
    courbe.type_ = "courbe";
    courbe.aspect_.contour_ = _contour;
    final Vector valeurs = new Vector();
    double moy = 0;
    double max = 0;
    double total = 0;
    for (int i = 0; i < _valeursX.length; i++) {
      final Valeur v = new Valeur();
      v.s_ = _valeursX[i];
      v.v_ = _valeursY[i];
      valeurs.addElement(v);
      moy = moy + v.v_;
      if (max < v.v_) {
        max = v.v_;
      }
    }
    total = moy;
    moy = moy / _valeursX.length;
    courbe.valeurs_ = valeurs;
    if (axe_des_y_.maximum_ < max) {
      axe_des_y_.maximum_ = max;
    }
    graphe_.ajoute(courbe);
    final Contrainte moyenne = new Contrainte();
    moyenne.v_ = moy;
    moyenne.couleur_ = _contour;
    moyenne.type_ = "max";
    graphe_.ajoute(moyenne);
    pdessous.add(new BuLabel("Moyenne " + _titre + " : " + moy));
    pdessous.add(new BuLabel("Maximum " + _titre + " : " + (int) max));
    pdessous.add(new BuLabel("Total " + _titre + " : " + (int) total));
  }

  public Image getImageCache() {
    Image cache = cache_;
    if (cache == null) {
      final Dimension dim = getSize();
      System.out.println(dim.width + " " + dim.height);
      cache = createImage(dim.width, dim.height);
      if (cache != null) {
        final Graphics cg = cache.getGraphics();
        paintComponents(cg);
      }
    }
    return cache;
  }

  public void ecritImageGIF(final File _file) {
    try {
      final Image img = getImageCache();
      final FileOutputStream fileoutstream = new FileOutputStream(_file);
      final AcmeGifEncoder age = new AcmeGifEncoder(img, fileoutstream);
      age.encode();
      fileoutstream.close();
    } catch (final Exception e) {
      System.out.println(e);
    }
  }
}
