/*
 * @file         SinaviOutilsDonnees.java
 * @creation     2001-05-17
 * @modification $Date: 2006-09-19 15:08:59 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.sinavi;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Vector;

import org.fudaa.dodico.corba.navigation.IBiefNavigationHelper;
import org.fudaa.dodico.corba.navigation.ICreneauHelper;
import org.fudaa.dodico.corba.navigation.IEcluseFluvialeHelper;
import org.fudaa.dodico.corba.navigation.IEtudeNavigationFluviale;
import org.fudaa.dodico.corba.navigation.IGare;
import org.fudaa.dodico.corba.navigation.IGareHelper;
import org.fudaa.dodico.corba.navigation.IGenerationJournaliereAleatoireHelper;
import org.fudaa.dodico.corba.navigation.INavireType;
import org.fudaa.dodico.corba.navigation.INavireTypeHelper;

/**
 * impl�mentation des outils permettant de cr�er des tableaux d'informations exploitables par JNI
 * 
 * @version $Revision: 1.6 $ $Date: 2006-09-19 15:08:59 $ by $Author: deniger $
 * @author Aline Marechalle , Franck Lejeune
 */
public class SinaviOutilsDonnees {

  /**
   * vecteur comportant les navires g�n�r�s
   */
  public Vector naviresGeneres_;
  /**
   * vecteur comportant les �cluses cr��es
   */
  public Vector eclusesCreees_;
  /**
   * vecteur comportant les biefs cr��s
   */
  public Vector biefsCrees_;
  /**
   * vecteur comportant les types de navire
   */
  public Vector naviresType_;
  /**
   * vecteur comportant les gares cr��es
   */
  public Vector garesCreees_;
  /**
   * vecteur comportant les cr�neaux horaires de toutes les �cluses
   */
  public Vector creneauxHorairesEcluses_;
  /**
   * vecteur comportant les dur�es de manoeuvres de toutes les �cluses
   */
  public Vector dureesManoeuvresEcluses_;
  /**
   * tableau d'entier comportant les donn�es g�n�rales
   */
  public int[] donneesGenerales_;
  /**
   * l'�tude � traiter
   */
  private IEtudeNavigationFluviale etude_;

  /**
   * Constructeur SinaviOutilsDonnees.
   * 
   * @param _etude l'�tude � traiter
   */
  public SinaviOutilsDonnees(final IEtudeNavigationFluviale _etude) {
    etude_ = _etude;
    naviresGeneres_ = new Vector();
    eclusesCreees_ = new Vector();
    biefsCrees_ = new Vector();
    naviresType_ = new Vector();
    garesCreees_ = new Vector();
    creneauxHorairesEcluses_ = new Vector();
    dureesManoeuvresEcluses_ = new Vector();
    etude_.genere();
  }

  /**
   * retourne l'index de la gare pass�e en param�tre
   * 
   * @param _gare une gare appartenant au r�seau
   * @return l'index de la gare pass�e en param�tres ou -1 si la gare n'appartient pas au r�seau
   */
  public int IndexGareArrivee(final IGare _gare) {
    for (int indexgare = 0; indexgare < etude_.reseau().gares().length; indexgare++) {
      if ((etude_.reseau().gares()[indexgare]).egale(_gare)) {
        return indexgare;
      }
    }
    return -1;
  }

  /**
   * retourne l'index du type de navires pass� en param�tre
   * 
   * @param _navire un type de navire appartenant � l'�tude
   * @return l'index du type de navire pass� en param�tres ou -1 si le type n'appartient pas � l'�tude
   */
  public int IndexTypeNavire(final INavireType _navire) {
    for (int indexnavire = 0; indexnavire < etude_.naviresType().length; indexnavire++) {
      if ((etude_.naviresType()[indexnavire]).egale(_navire)) {
        return indexnavire;
      }
    }
    return -1;
  }

  /**
   * construit un tableau d'entier avec les donn�es g�n�rales
   */
  public void ConstruitTableauDonneesGenerales() {
    donneesGenerales_ = new int[3];
    donneesGenerales_[0] = etude_.nombreSeries();
    donneesGenerales_[1] = (int) etude_.dateDebutSimulation();
    donneesGenerales_[2] = (int) etude_.dateDebutReelSimulation();
  }

  /**
   * construit un vecteur de tableaux de 8 entiers. chaque tableau d�finit une �cluse
   */
  public void ConstruitTableauEclusesCreees() {
    double[] ecluseCreee_;
    /*
     * "numero ecluse", "numero gare aval", "numero gare amont", "longueur", "largeur" "profondeur", "fausse bassinee
     * montante", "fausse bassinee avalante"
     */
    for (int i = 0; i < etude_.reseau().ecluses().length; i++) {
      ecluseCreee_ = new double[8];
      ecluseCreee_[0] = i;
      ecluseCreee_[1] = IndexGareArrivee(IGareHelper.narrow(etude_.reseau().ecluses()[i].gareAval()));
      ecluseCreee_[2] = IndexGareArrivee(IGareHelper.narrow(etude_.reseau().ecluses()[i].gareAmont()));
      ecluseCreee_[3] = IEcluseFluvialeHelper.narrow(etude_.reseau().ecluses()[i]).longueur();
      ecluseCreee_[4] = IEcluseFluvialeHelper.narrow(etude_.reseau().ecluses()[i]).largeur();
      ecluseCreee_[5] = IEcluseFluvialeHelper.narrow(etude_.reseau().ecluses()[i]).profondeur();
      ecluseCreee_[6] = IEcluseFluvialeHelper.narrow(etude_.reseau().ecluses()[i]).dureeFausseBassineeMontante();
      ecluseCreee_[7] = IEcluseFluvialeHelper.narrow(etude_.reseau().ecluses()[i]).dureeFausseBassineeAvalante();
      eclusesCreees_.addElement(ecluseCreee_);
    }
  }

  /**
   * construit un vecteur de tableaux de 3 entiers. chaque tableau d�finit un cr�neau horaire
   */
  public void ConstruitTableauCreneauxHorairesEcluses() {
    int[] creneauHoraireEcluse_;
    /* "numero ecluse", "heure debut", "heure fin" */
    for (int i = 0; i < etude_.reseau().ecluses().length; i++) {
      for (int j = 0; j < etude_.reseau().ecluses()[i].creneaux().length; j++) {
        creneauHoraireEcluse_ = new int[3];
        creneauHoraireEcluse_[0] = i;
        creneauHoraireEcluse_[1] = (int) ICreneauHelper.narrow(etude_.reseau().ecluses()[i].creneaux()[j]).heureDebut();
        creneauHoraireEcluse_[2] = (int) ICreneauHelper.narrow(etude_.reseau().ecluses()[i].creneaux()[j]).heureFin();
        creneauxHorairesEcluses_.addElement(creneauHoraireEcluse_);
      }
    }
  }

  /**
   * construit un vecteur de tableaux de 6 entiers. chaque tableau d�finit une dur�e de manoeuvre
   */
  public void ConstruitTableauDureesManoeuvresEcluses() {
    int[] dureesManoeuvresEcluse_;
    /*
     * "numero ecluse", "type bateau", "temps entree", "temps sortie" "temps bassinee montante", "temps bassinee
     * avalante"
     */
    for (int i = 0; i < etude_.reseau().ecluses().length; i++) {
      for (int j = 0; j < etude_.naviresType().length; j++) {
        dureesManoeuvresEcluse_ = new int[6];
        dureesManoeuvresEcluse_[0] = i;
        dureesManoeuvresEcluse_[1] = j;
        if ((IEcluseFluvialeHelper.narrow(etude_.reseau().ecluses()[i]).dureeNavire(INavireTypeHelper.narrow(etude_
            .naviresType()[j]))) == null) {
          dureesManoeuvresEcluse_[2] = (int) IEcluseFluvialeHelper.narrow(etude_.reseau().ecluses()[i])
              .dureeMoyenneEntree();
          dureesManoeuvresEcluse_[3] = (int) IEcluseFluvialeHelper.narrow(etude_.reseau().ecluses()[i])
              .dureeMoyenneSortie();
          dureesManoeuvresEcluse_[4] = (int) IEcluseFluvialeHelper.narrow(etude_.reseau().ecluses()[i])
              .dureeMoyenneBassineeMontante();
          dureesManoeuvresEcluse_[5] = (int) IEcluseFluvialeHelper.narrow(etude_.reseau().ecluses()[i])
              .dureeMoyenneBassineeAvalante();
        } else {
          dureesManoeuvresEcluse_[2] = (int) IEcluseFluvialeHelper.narrow(etude_.reseau().ecluses()[i]).dureeNavire(
              INavireTypeHelper.narrow(etude_.naviresType()[j])).dureeEntree();
          dureesManoeuvresEcluse_[3] = (int) IEcluseFluvialeHelper.narrow(etude_.reseau().ecluses()[i]).dureeNavire(
              INavireTypeHelper.narrow(etude_.naviresType()[j])).dureeSortie();
          dureesManoeuvresEcluse_[4] = (int) IEcluseFluvialeHelper.narrow(etude_.reseau().ecluses()[i]).dureeNavire(
              INavireTypeHelper.narrow(etude_.naviresType()[j])).dureeBassineeMontante();
          dureesManoeuvresEcluse_[5] = (int) IEcluseFluvialeHelper.narrow(etude_.reseau().ecluses()[i]).dureeNavire(
              INavireTypeHelper.narrow(etude_.naviresType()[j])).dureeBassineeAvalante();
        }
        dureesManoeuvresEcluses_.addElement(dureesManoeuvresEcluse_);
      }
    }
  }

  /**
   * construit un vecteur de tableaux de 9 entiers. chaque tableau d�finit un bief
   */
  public void ConstruitTableauBiefsCrees() {
    double[] biefCree_;
    /*
     * "numero bief", "numero gare aval", "numero gare amont", "longueur", "largeur" "profondeur", "vitesse maximum",
     * "trematage autorise", "croisement autorise"
     */
    for (int i = 0; i < etude_.reseau().biefs().length; i++) {
      biefCree_ = new double[9];
      biefCree_[0] = i;
      biefCree_[1] = IndexGareArrivee(IGareHelper.narrow(etude_.reseau().biefs()[i].gareAval()));
      biefCree_[2] = IndexGareArrivee(IGareHelper.narrow(etude_.reseau().biefs()[i].gareAmont()));
      biefCree_[3] = IBiefNavigationHelper.narrow(etude_.reseau().biefs()[i]).longueur();
      biefCree_[4] = IBiefNavigationHelper.narrow(etude_.reseau().biefs()[i]).largeur();
      biefCree_[5] = IBiefNavigationHelper.narrow(etude_.reseau().biefs()[i]).profondeur();
      biefCree_[6] = IBiefNavigationHelper.narrow(etude_.reseau().biefs()[i]).regle().vitesseMaximum();
      if (IBiefNavigationHelper.narrow(etude_.reseau().biefs()[i]).regle().trematageAutorise()) {
        biefCree_[7] = 1;
      } else {
        biefCree_[7] = 0;
      }
      if (IBiefNavigationHelper.narrow(etude_.reseau().biefs()[i]).regle().croisementAutorise()) {
        biefCree_[8] = 1;
      } else {
        biefCree_[8] = 0;
      }
      biefsCrees_.addElement(biefCree_);
    }
  }

  /**
   * construit un vecteur de tableaux de 5 entiers. chaque tableau d�finit un type de navire
   */
  public void ConstruitTableauNaviresType() {
    double[] navireType_;
    /* "type navire", "longueur", "largeur", "tirant d'eau", "gene admissible" */
    for (int i = 0; i < etude_.naviresType().length; i++) {
      navireType_ = new double[5];
      navireType_[0] = i;
      navireType_[1] = etude_.naviresType()[i].longueur();
      navireType_[2] = etude_.naviresType()[i].largeur();
      navireType_[3] = etude_.naviresType()[i].tirantMoy();
      navireType_[4] = etude_.naviresType()[i].dureeAttenteAdmissible();
      naviresType_.addElement(navireType_);
    }
  }

  /**
   * construit un vecteur de tableaux de 2 entiers. chaque tableau d�finit une gare
   */
  public void ConstruitTableauGaresCreees() {
    int[] gareCreee_;
    /* "numero gare", nombre maximum de navire" */
    for (int i = 0; i < etude_.reseau().gares().length; i++) {
      gareCreee_ = new int[2];
      gareCreee_[0] = i;
      gareCreee_[1] = etude_.reseau().gares()[i].nombreMaxNavire();
      garesCreees_.addElement(gareCreee_);
    }
  }

  /**
   * construit un vecteur de tableaux de 5 entiers. chaque tableau d�finit un navire g�n�r�
   */
  public void ConstruitTableauNaviresGeneres() {
    final String test_ = "";
    double[] navireGenere_;
    /*
     * "heure depart en secondes", "numero gare depart", "numero gare arrivee", "vitesse trajet", "numero type bateau"
     */
    navireGenere_ = new double[5];
    for (int i = 0; i < etude_.reseau().gares().length; i++) {
      if ((test_.equals(""))) {
        // numero gare depart = gare traitee (i)
        System.out.println("nom de la gare traitee " + etude_.reseau().gares()[i].nom() + " valeur de i " + i);
        navireGenere_[1] = i;
        System.out.println("nombre de trajets " + etude_.reseau().gares()[i].trajets().length);
        for (int j = 0; j < etude_.reseau().gares()[i].trajets().length; j++) {
          if ((test_.equals(""))) {
            // numero gare arrivee index de la gare d'arrivee du trajet traite (j)
            navireGenere_[2] = IndexGareArrivee(etude_.reseau().gares()[i].trajets()[j].gareArrivee());
            // vitesse du navire pour ce trajet
            navireGenere_[3] = (new Double(etude_.reseau().gares()[i].trajets()[j].vitesse())).intValue();
            // type du navire
            navireGenere_[4] = IndexTypeNavire(etude_.reseau().gares()[i].trajets()[j].navireType());
            System.out.println("nombre de creneaux " + etude_.reseau().gares()[i].trajets()[j].generations().length);
            for (int k = 0; k < etude_.reseau().gares()[i].trajets()[j].generations().length; k++) {
              if ((test_.equals(""))) {
                System.out.println("nombre d'instants "
                    + IGenerationJournaliereAleatoireHelper.narrow(
                        etude_.reseau().gares()[i].trajets()[j].generations()[k]).instantsGeneres()[0].length);
                for (int l = 0; l < (IGenerationJournaliereAleatoireHelper.narrow(
                    etude_.reseau().gares()[i].trajets()[j].generations()[k]).instantsGeneres()[0]).length; l++) {
                  navireGenere_[0] = IGenerationJournaliereAleatoireHelper.narrow(
                      etude_.reseau().gares()[i].trajets()[j].generations()[k]).instantsGeneres()[0][l];
                  final double[] temp = new double[5];
                  temp[0] = navireGenere_[0];
                  temp[1] = navireGenere_[1];
                  temp[2] = navireGenere_[2];
                  temp[3] = navireGenere_[3];
                  temp[4] = navireGenere_[4];
                  naviresGeneres_.addElement(temp);
                }
              } else {
                System.out.println("erreur");
                break;
              }
            }
          } else {
            break;
          }
        }
      } else {
        break;
      }
    }
  }

  /**
   * methode de tri des navires par ordre croissant d'apparition
   * 
   * @return un tableau d'objet qui est une matrice d'entier d�finissant les navires g�n�r�s
   */
  public Object[] triNaviresGeneres() {
    final Object[] resu_ = naviresGeneres_.toArray();
    Arrays.sort(resu_, new Comparator() {

      /**
       * comparateur de tableau de double � partir du premier �l�ment du tableau
       * 
       * @param obj1 le premier tableau de double
       * @param obj2 le deuxi�me tableau de double
       * @return 0 si �gal -1 si obj1 < obj2 et 1 si obj1 > obj2
       */
      public int compare(final Object obj1, final Object obj2) {
        if (((double[]) obj1)[0] < ((double[]) obj2)[0]) {
          return -1;
        } else if (((double[]) obj1)[0] > ((double[]) obj2)[0]) {
          return 1;
        } else {
          return 0;
        }
      }
    });
    return resu_;
  }
}
