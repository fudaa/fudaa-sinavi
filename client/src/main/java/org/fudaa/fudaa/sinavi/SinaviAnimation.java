/*
 * @file         SinaviAnimation.java
 * @creation     2001-05-17
 * @modification $Date: 2006-09-19 15:09:00 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.sinavi;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridBagConstraints;
import java.awt.Rectangle;
import java.awt.Stroke;

import com.memoire.dja.DjaGridInteractive;
import com.memoire.dja.DjaText;

import org.fudaa.dodico.corba.sinavi.SResuInter;

/**
 * ....
 * 
 * @version $Revision: 1.9 $ $Date: 2006-09-19 15:09:00 $ by $Author: deniger $
 * @author Aline Marechalle , Franck Lejeune
 */
public class SinaviAnimation {

  /**
   * Tableau de correspondance entre les n� de gares et leurs emplacements sur la grille du r�seau
   */
  private static SinaviReseauGare[] correspGare_;
  /**
   * Taille du tableau de correspondance des gares
   */
  private static int tailCorrespGare_;
  /**
   * Tableau de correspondance entre les n� de biefs et leurs emplacements sur la grille du r�seau
   */
  private static SinaviReseauBief[] correspBief_;
  /**
   * Taille du tableau de correspondance des biefs
   */
  private static int tailCorrespBief_;
  /**
   * Tableau de correspondance entre les n� d'�cluses et leurs emplacements sur la grille du r�seau
   */
  private static SinaviReseauEcluse[] correspEcluse_;
  /**
   * Taille du tableau de correspondance des �cluses
   */
  private static int tailCorrespEcluse_;
  /**
   * Structure rassemblant toutes les donn�es n�cessaires pour l'animation
   */
  // private static SResuInter res_;
  /**
   * Compteur pour faire avancer la barre de progression
   */
  private int progression_ = 0;

  /**
   * Constructeur de la classe SinaviAnimation qui rasssemble toutes les proc�dures n�cessaires pour l'animation
   */
  public SinaviAnimation() {}

  /**
   * Lancement de l'animation donc appel au serveur sur lequel sera lanc� un thread.
   */
  public void lancementCalcul() {
    if (SinaviImplementation.sinavireseauframe_ == null) {
      return;
    }
    SinaviReseauFrame.coteNord.setVisible(true);
    SinaviReseauFrame.coteSud.setVisible(true);
    // quand on quitte tous les boutons sont d�sactiv�s ... donc r�initilisation
    SinaviReseauFrame.coteSud.initBoutons();
    SinaviImplementation.sinavireseauframe_.repaint();
    final DjaGridInteractive grid = (DjaGridInteractive) SinaviImplementation.sinavireseauframe_.getGrid();
    grid.setInteractive(false);
    SinaviImplementation.sinavireseauframe_.activerBoutonInteractive(false);
    constructionTableauCorrespondances();
    // cas ou l'utilisateur a quitte la simulation
    // puis en demande une nouvelle avec la modif
    // r�initialisation des booleans de SERVICE_SINAVI
    SinaviImplementation.SERVICE_SINAVI.structureDisponible(false);
    SinaviImplementation.SERVICE_SINAVI.finCalcul(false);
    SinaviImplementation.SERVICE_SINAVI.demandeCalculUnDeltaT(false);
    SinaviImplementation.SERVICE_SINAVI.calculUnDeltaTFait(false);
    SinaviImplementation.SERVICE_SINAVI.arretCalculServeur(false);
    SinaviReseauFrame.coteSud.attente(false);
    // attente pour voir les nord & sud car Swing tres lent
    try {
      Thread.sleep(3000);
    } catch (final InterruptedException e) {}
    Thread.currentThread().setPriority(Thread.MIN_PRIORITY);
    SinaviImplementation.SERVICE_SINAVI.calcule(10, SinaviImplementation.ETUDE_SINAVI);
    System.out.println("j'ai appell� calcule(10)");
    progression_ = 0;
    // TODO a changer !
    SinaviAnimationNord.progres_.setValue(progression_);
    afficheCalcul();
  }

  /**
   * Synchronisation avec le serveur pour l'affichage des informations de l'animation. Cette synchronization se fait par
   * controles de bool�ens entre le client et le serveur Cette m�thode appelle apresCalculServeur, viderDjatext,
   * fermerToutesLesEcluses
   */
  public synchronized void afficheCalcul() {
    while (!(SinaviImplementation.SERVICE_SINAVI.finCalcul())
        && (!(SinaviImplementation.SERVICE_SINAVI.arretCalculServeur()))) {
      // le bouton pause est cliqu� j'attends la reprise
      while (SinaviReseauFrame.coteSud.attente() && (!(SinaviImplementation.SERVICE_SINAVI.arretCalculServeur()))
          && (!(SinaviImplementation.SERVICE_SINAVI.finCalcul()))) {
        try {
          System.out.println("******ds client Attente = true dc Calcul dort");
          Thread.sleep(500);
        } catch (final InterruptedException e) {}
      }
      // j'attend que le producteur ait mis � jour la structure
      while (!(SinaviImplementation.SERVICE_SINAVI.structureDisponible())
          && (!(SinaviImplementation.SERVICE_SINAVI.arretCalculServeur()))
          && (!(SinaviImplementation.SERVICE_SINAVI.finCalcul()))) {
        try {
          System.out.println("******ds le sleep client - struct non dispo " + Thread.currentThread() + " struct dispo "
              + SinaviImplementation.SERVICE_SINAVI.structureDisponible());
          Thread.sleep(500);
        } catch (final InterruptedException e) {}
      }
      // un calcul (pas � pas ) est fait j'attend la demande pour un autre
      while (SinaviImplementation.SERVICE_SINAVI.calculUnDeltaTFait()
          && (!(SinaviImplementation.SERVICE_SINAVI.arretCalculServeur()))
          && (!(SinaviImplementation.SERVICE_SINAVI.finCalcul()))) {
        try {
          System.out.println("******ds le sleep client -  calculUnDeltaTFait = true");
          Thread.sleep(500);
        } catch (final InterruptedException e) {}
      }
      // le serveur m'a fourni une structure je l'affiche
      System.out.println("je sors de ttes les boucles -le serveur m'a fourni une structure je l'affiche");
      if (!(SinaviImplementation.SERVICE_SINAVI.arretCalculServeur())
          || !(SinaviImplementation.SERVICE_SINAVI.finCalcul())) {
        apresCalculServeur();
      }
      // SinaviReseauFrame.coteNord.iterate((progression_ = progression_ +1));
      // TODO: a changer !
      SinaviAnimationNord.progres_.setValue((progression_ = progression_ + 1));
      // System.out.println(" APRES apresCalculServeur"+" struct dispo "+
      // SinaviImplementation.SERVICE_SINAVI.structureDisponible());
      SinaviImplementation.SERVICE_SINAVI.structureDisponible(false);
      if (SinaviImplementation.SERVICE_SINAVI.arretCalculServeur() || SinaviImplementation.SERVICE_SINAVI.finCalcul()) {
        System.out.println("arretCalculServeur() : " + SinaviImplementation.SERVICE_SINAVI.arretCalculServeur());
        viderDjatext();
        fermerToutesLesEcluses();
        // TODO a changer les methodes statiques ne sont pas bonnes !
        SinaviAnimationNord.progres_.setValue(0);
        SinaviAnimationSud.pause_.setEnabled(false);
        SinaviAnimationSud.pasApas_.setEnabled(false);
        SinaviAnimationSud.reprise_.setEnabled(false);
        // Arr�ter la methode lancer par BUTaskOperation c�d je sors de ma methode
        return;
      }
    }
    System.out
        .println("fin affiche calcul tout est termin� pour les calculs aniamtion.. restera l'expl res struct dispo "
            + SinaviImplementation.SERVICE_SINAVI.structureDisponible());
  }

  /**
   * Ecriture des informations sur la grille de l'animation, apr�s les diff�rents controles.
   */
  public static void apresCalculServeur() {
    final SResuInter res_ = SinaviImplementation.SERVICE_SINAVI.getResuInter();
    SinaviReseauFrame.coteNord.constructTextGraine(res_.numGraine, res_.nbGraines);
    SinaviReseauFrame.coteNord.constructTextJour(res_.jour);
    SinaviReseauFrame.coteNord.constructTextHeure(res_.heure, res_.minute);
    /* mise � jour des djatext */
    // pour les gares
    String nbBatMontGare = "";
    String nbBatAvalGare = "";
    final int[] _NbBatMontGare = res_.gareNaviresMontants;
    final int[] _NbBatAvalGare = res_.gareNaviresAvalants;
    for (int g = 0; g < correspGare_.length; g++) {
      nbBatMontGare = nbBatMontGare.trim(); // suppression de l'espace
      nbBatMontGare = ("Fr ");
      nbBatMontGare = (nbBatMontGare + _NbBatMontGare[g * 5] + "\n");
      nbBatMontGare = (nbBatMontGare + "Co " + _NbBatMontGare[(g * 5) + 1] + "\n");
      nbBatMontGare = (nbBatMontGare + "Pl " + _NbBatMontGare[(g * 5) + 2] + "\n");
      nbBatMontGare = (nbBatMontGare + "Pa " + _NbBatMontGare[(g * 5) + 3] + "\n");
      nbBatMontGare = (nbBatMontGare + "Pe " + _NbBatMontGare[(g * 5) + 4]);
      nbBatAvalGare = nbBatAvalGare.trim();
      nbBatAvalGare = ("Fr ");
      nbBatAvalGare = (nbBatAvalGare + _NbBatMontGare[g * 5] + "\n");
      nbBatAvalGare = (nbBatAvalGare + "Co " + _NbBatAvalGare[(g * 5) + 1] + "\n");
      nbBatAvalGare = (nbBatAvalGare + "Pl " + _NbBatAvalGare[(g * 5) + 2] + "\n");
      nbBatAvalGare = (nbBatAvalGare + "Pa " + _NbBatAvalGare[(g * 5) + 3] + "\n");
      nbBatAvalGare = (nbBatAvalGare + "Pe " + _NbBatAvalGare[(g * 5) + 4]);
      /*
       * for(int j = g * 5; j <= (g * 5 + 4); j++) { nbBatMontGare = nbBatMontGare.trim();//suppression de l'espace
       * nbBatAvalGare = nbBatAvalGare.trim(); nbBatMontGare = (nbBatMontGare + "\n" + _NbBatMontGare[j]); nbBatAvalGare =
       * (nbBatAvalGare + "\n" + _NbBatAvalGare[j]); }
       */
      correspGare_[g].ecrireTextGare(nbBatMontGare, true);
      // boolean true car il s'agit bien de nb bat montant
      // System.out.println("est ce que j'ecris les gares mont");
      correspGare_[g].ecrireTextGare(nbBatAvalGare, false);
    }
    // pour les biefs
    String nbBatMontBief = "";
    String nbBatAvalBief = "";
    final int[] _NbBatMontBief = res_.biefNaviresMontants;
    final int[] _NbBatAvalBief = res_.biefNaviresAvalants;
    for (int k = 0; k < correspBief_.length; k++) {
      nbBatMontBief = ("");
      nbBatAvalBief = ("");
      nbBatMontBief = nbBatMontBief.trim(); // suppression de l'espace
      nbBatMontBief = ("Fr ");
      nbBatMontBief = (nbBatMontBief + _NbBatMontBief[k * 5] + "\n");
      nbBatMontBief = (nbBatMontBief + "Co " + _NbBatMontBief[(k * 5) + 1] + "\n");
      nbBatMontBief = (nbBatMontBief + "Pl " + _NbBatMontBief[(k * 5) + 2] + "\n");
      nbBatMontBief = (nbBatMontBief + "Pa " + _NbBatMontBief[(k * 5) + 3] + "\n");
      nbBatMontBief = (nbBatMontBief + "Pe " + _NbBatMontBief[(k * 5) + 4]);
      nbBatAvalBief = nbBatAvalBief.trim();
      nbBatAvalBief = ("Fr ");
      nbBatAvalBief = (nbBatAvalBief + _NbBatAvalBief[k * 5] + "\n");
      nbBatAvalBief = (nbBatAvalBief + "Co " + _NbBatAvalBief[(k * 5) + 1] + "\n");
      nbBatAvalBief = (nbBatAvalBief + "Pl " + _NbBatAvalBief[(k * 5) + 2] + "\n");
      nbBatAvalBief = (nbBatAvalBief + "Pa " + _NbBatAvalBief[(k * 5) + 3] + "\n");
      nbBatAvalBief = (nbBatAvalBief + "Pe " + _NbBatAvalBief[(k * 5) + 4]);
      /*
       * for(int l = k * 5; l <= (k * 5 + 4); l++) { nbBatMontBief = nbBatMontBief.trim(); nbBatAvalBief =
       * nbBatAvalBief.trim(); nbBatMontBief = (nbBatMontBief + "\n" + _NbBatMontBief[l]); nbBatAvalBief =
       * (nbBatAvalBief + "\n" + _NbBatAvalBief[l]); }
       */
      correspBief_[k].ecrireTextBief(nbBatMontBief, true);
      correspBief_[k].ecrireTextBief(nbBatAvalBief, false);
    }
    // pour les ecluses
    String nbBatMontEcluse = "";
    String nbBatAvalEcluse = "";
    final int[] _NbBatMontEcluse = res_.ouvrNaviresMontants;
    final int[] _NbBatAvalEcluse = res_.ouvrNaviresAvalants;
    for (int m = 0; m < correspEcluse_.length; m++) {
      nbBatMontEcluse = ("");
      nbBatAvalEcluse = ("");
      /*
       * for(int n = m * 5; n <= (m * 5 + 4); n++) { nbBatMontEcluse = nbBatMontEcluse.trim(); nbBatAvalEcluse =
       * nbBatAvalEcluse.trim(); nbBatMontEcluse = (nbBatMontEcluse + "\n" + _NbBatMontEcluse[n]); nbBatAvalEcluse =
       * (nbBatAvalEcluse + "\n" + _NbBatAvalEcluse[n]); }
       */
      nbBatMontEcluse = nbBatMontEcluse.trim(); // suppression de l'espace
      nbBatMontEcluse = ("Fr ");
      nbBatMontEcluse = (nbBatMontEcluse + _NbBatMontEcluse[m * 5] + "\n");
      nbBatMontEcluse = (nbBatMontEcluse + "Co " + _NbBatMontEcluse[(m * 5) + 1] + "\n");
      nbBatMontEcluse = (nbBatMontEcluse + "Pl " + _NbBatMontEcluse[(m * 5) + 2] + "\n");
      nbBatMontEcluse = (nbBatMontEcluse + "Pa " + _NbBatMontEcluse[(m * 5) + 3] + "\n");
      nbBatMontEcluse = (nbBatMontEcluse + "Pe " + _NbBatMontEcluse[(m * 5) + 4]);
      nbBatAvalEcluse = nbBatAvalEcluse.trim();
      nbBatAvalEcluse = ("Fr ");
      nbBatAvalEcluse = (nbBatAvalEcluse + _NbBatAvalEcluse[m * 5] + "\n");
      nbBatAvalEcluse = (nbBatAvalEcluse + "Co " + _NbBatAvalEcluse[(m * 5) + 1] + "\n");
      nbBatAvalEcluse = (nbBatAvalEcluse + "Pl " + _NbBatAvalEcluse[(m * 5) + 2] + "\n");
      nbBatAvalEcluse = (nbBatAvalEcluse + "Pa " + _NbBatAvalEcluse[(m * 5) + 3] + "\n");
      nbBatAvalEcluse = (nbBatAvalEcluse + "Pe " + _NbBatAvalEcluse[(m * 5) + 4]);
      correspEcluse_[m].ecrireTextEcluse(nbBatMontEcluse, true);
      correspEcluse_[m].ecrireTextEcluse(nbBatAvalEcluse, false);
      if (res_.ouverturePortesOuvrage[m].equals("M")) {
        System.out.println("res_.ouverturePortesOuvrage[1] = M ? " + res_.ouverturePortesOuvrage[1]);
        correspEcluse_[m].setPositionPortes("M");
      }
      if (res_.ouverturePortesOuvrage[m].equals("A")) {
        System.out.println("res_.ouverturePortesOuvrage[1] = A ? " + res_.ouverturePortesOuvrage[1]);
        correspEcluse_[m].setPositionPortes("A");
      }
      if (res_.ouverturePortesOuvrage[m].equals("F")) {
        System.out.println("res_.ouverturePortesOuvrage[1] = F ? " + res_.ouverturePortesOuvrage[1]);
        correspEcluse_[m].setPositionPortes("F");
      }
    }
    SinaviImplementation.sinavireseauframe_.repaint();
  }

  /**
   * Construit les tableaux de correspondances entre les objets de la grille et les objets m�tiers.
   */
  private static void constructionTableauCorrespondances() {
    int i;
    // System.out.println("debut contructionTableauCorrespondances");
    tailCorrespGare_ = SinaviImplementation.ETUDE_SINAVI.reseau().gares().length;
    correspGare_ = new SinaviReseauGare[tailCorrespGare_];
    tailCorrespBief_ = SinaviImplementation.ETUDE_SINAVI.reseau().biefs().length;
    correspBief_ = new SinaviReseauBief[tailCorrespBief_];
    tailCorrespEcluse_ = SinaviImplementation.ETUDE_SINAVI.reseau().ecluses().length;
    correspEcluse_ = new SinaviReseauEcluse[tailCorrespEcluse_];
    // je scanne la grille, pour chaque element je cherche le I.. qui correspond
    final DjaGridInteractive grille = (DjaGridInteractive) SinaviImplementation.sinavireseauframe_.getGrid();
    final java.util.Enumeration eltEnum = grille.getObjects().elements();
    while (eltEnum.hasMoreElements()) {
      final Object obj = eltEnum.nextElement();
      // si c'est une gare je lui fait correspondre le Igare
      if (obj instanceof SinaviReseauGare) {
        final SinaviReseauGare srg = (SinaviReseauGare) obj;
        for (i = 0; i < tailCorrespGare_; i++) {
          if (srg.getGare().equals(SinaviImplementation.ETUDE_SINAVI.reseau().gares()[i])) {
            correspGare_[i] = srg;
          }
        }
      }
      // si c'est un bief je lui fait correspondre le Ibief
      if (obj instanceof SinaviReseauBief) {
        final SinaviReseauBief srb = (SinaviReseauBief) obj;
        for (i = 0; i < tailCorrespBief_; i++) {
          if (srb.getBief().equals(SinaviImplementation.ETUDE_SINAVI.reseau().biefs()[i])) {
            correspBief_[i] = srb;
          }
        }
      }
      // si c'est une ecluse je lui fait correspondre le Iecluse
      if (obj instanceof SinaviReseauEcluse) {
        final SinaviReseauEcluse sre = (SinaviReseauEcluse) obj;
        for (i = 0; i < tailCorrespEcluse_; i++) {
          if (sre.getEcluse().equals(SinaviImplementation.ETUDE_SINAVI.reseau().ecluses()[i])) {
            correspEcluse_[i] = sre;
          }
        }
      }
    } // fin while
  }

  /**
   * Dessine les contours des informations affich�es pour chaque composant de la grille.
   * 
   * @param _g : objet graphique
   * @param _text : texte � �crire
   * @param _rect : rectangle � dessiner
   * @param _couleur : couleur du rectangle � dessiner
   */
  public static void contourDjaText(final Graphics _g, final DjaText _text, Rectangle _rect, final Color _couleur) {
    final Graphics2D comp2D = (Graphics2D) _g;
    _rect = _text.getBounds();
    final Stroke oldStroke = comp2D.getStroke();
    final Color oldColor = comp2D.getColor();
    comp2D.setColor(_couleur);
    // pour avoir un trait en pointill�
    final float dash1[] = { 10.0f };
    final BasicStroke dashed = new BasicStroke(1.0f, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER, 10.0f, dash1, 0.0f);
    comp2D.setStroke(dashed);
    comp2D.drawRect(_rect.x, _rect.y, _rect.width, _rect.height);
    comp2D.setStroke(oldStroke);
    comp2D.setColor(oldColor);
  }

  /**
   * Efface les donn�es �crites sur le dessin du r�seau pour chaque composants.
   */
  public static void viderDjatext() {
    final String remiseABlanc = "      \n     \n     \n      \n     \n";
    // pour les gares
    for (int g = 0; g < correspGare_.length; g++) {
      correspGare_[g].ecrireTextGare(remiseABlanc, true);
      correspGare_[g].ecrireTextGare(remiseABlanc, false);
    }
    // pour les biefs
    for (int k = 0; k < correspBief_.length; k++) {
      correspBief_[k].ecrireTextBief(remiseABlanc, true);
      correspBief_[k].ecrireTextBief(remiseABlanc, false);
    }
    // pour les ecluses
    for (int m = 0; m < correspEcluse_.length; m++) {
      correspEcluse_[m].ecrireTextEcluse(remiseABlanc, true);
      correspEcluse_[m].ecrireTextEcluse(remiseABlanc, false);
    }
    SinaviImplementation.sinavireseauframe_.repaint();
  }

  /**
   * Ferme toutes les �cluses. appell�e quand on quitte l'animation ou que l'animation est termin�e
   */
  public static void fermerToutesLesEcluses() {
    for (int m = 0; m < correspEcluse_.length; m++) {
      correspEcluse_[m].setPositionPortes("F");
    }
  }

  /**
   * Permet de renseigner les contraintes de positionnement des composants graphiques utilis�s dans le panneau Nord
   * 
   * @param gbc : objets de contraintes auxquels il faut affecter les param�tres
   * @param gx : coordonn�es X de la celule dans le gridbagLayout
   * @param gy : coordonn�es Y de la celule dans le gridbagLayout
   * @param gw : nb cellules sur lequel s'etend le composant pour les colonnes
   * @param gh : nb cellules sur lequel s'etend le composant pour les lignes
   * @param wx : proportions des lignes
   * @param wy : proportions des colonnes
   */
  public static void buildConstraints(final GridBagConstraints gbc, final int gx, final int gy, final int gw,
      final int gh, final int wx, final int wy) {
    // coordonnees de la cell col puis lign
    // si comp sur +s cell ses coord = celles de cell sup gche
    gbc.gridx = gx;
    gbc.gridy = gy;
    // nb cellules sur lequel s'etend le composant
    gbc.gridwidth = gw; // colonnes
    gbc.gridheight = gh; // lignes
    // proportions des lignes et colonnes : larg et prof
    // pr un lig ou col sommes des wx ou wy = 100
    // doit �tre = wx = 0 si etendu sur +s col idem lig
    gbc.weightx = wx;
    gbc.weighty = wy;
  }
} // fin SinaviAnimation
