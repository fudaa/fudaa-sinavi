/*
 * @file         SinaviFilleChoixRappelDonnees.java
 * @creation     2001-05-17
 * @modification $Date: 2006-09-19 15:09:00 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.sinavi;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.JComponent;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.EmptyBorder;

import com.memoire.bu.BuBrowserFrame;
import com.memoire.bu.BuButton;
import com.memoire.bu.BuCheckBox;
import com.memoire.bu.BuCommonImplementation;
import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuInternalFrame;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuRadioButton;

import org.fudaa.dodico.corba.navigation.IBiefNavigation;
import org.fudaa.dodico.corba.navigation.IBiefNavigationHelper;
import org.fudaa.dodico.corba.navigation.IEcluseFluviale;
import org.fudaa.dodico.corba.navigation.IEcluseFluvialeHelper;
import org.fudaa.dodico.corba.navigation.IGare;
import org.fudaa.dodico.corba.navigation.IGareHelper;

/**
 * impl�mentation d'une fen�tre interne permetant de choisir les donn�es que l'on veut inclure dans le rappel de donn�es
 * 
 * @version $Revision: 1.8 $ $Date: 2006-09-19 15:09:00 $ by $Author: deniger $
 * @author Aline Marechalle , Franck Lejeune
 */
public class SinaviFilleChoixRappelDonnees extends BuInternalFrame implements ActionListener {

  /**
   * bouton valider
   */
  private final BuButton bValider_ = new BuButton(SinaviResource.SINAVI.getIcon("VALIDER"), "Valider");
  /**
   * bouton annuler
   */
  private final BuButton bAnnuler_ = new BuButton(SinaviResource.SINAVI.getIcon("ANNULER"), "Annuler");
  /**
   * case � cocher pour l'affichage des donn�es g�n�rales dans le rappel des donn�es
   */
  private final BuCheckBox cbDonneesGenerales_ = new BuCheckBox("Donn�es G�n�rales");
  /**
   * case � cocher pour l'affichage des bateaux d�finis dans le rappel des donn�es
   */
  private final BuCheckBox cbBateauxDefinis_ = new BuCheckBox("Bateaux D�finis");
  /**
   * groupe de boutons relatif aux biefs
   */
  private final ButtonGroup bgBiefs_ = new ButtonGroup();
  /**
   * bouton radio pour l'affichage d'aucun renseignement concernant les biefs
   */
  private final BuRadioButton rbAucunBief_ = new BuRadioButton("Aucun bief");
  /**
   * bouton radio pour l'affichage des renseignements concernant tous les biefs
   */
  private final BuRadioButton rbTousBiefs_ = new BuRadioButton("Tous les biefs");
  /**
   * bouton radio pour l'affichage des renseignements concernant les biefs s�lectionn�s dans la liste
   */
  private final BuRadioButton rbBiefsChoisis_ = new BuRadioButton("Biefs S�lectionn�s");
  /**
   * groupe de boutons relatif aux �cluses
   */
  private final ButtonGroup bgEcluses_ = new ButtonGroup();
  /**
   * bouton radio pour l'affichage d'aucun renseignement concernant les �cluses
   */
  private final BuRadioButton rbAucuneEcluse_ = new BuRadioButton("Aucune �cluse");
  /**
   * bouton radio pour l'affichage des renseignements concernant toutes les �cluses
   */
  private final BuRadioButton rbToutesEcluses_ = new BuRadioButton("Toutes les �cluses");
  /**
   * bouton radio pour l'affichage des renseignements concernant les �cluses s�lectionn�es dans la liste
   */
  private final BuRadioButton rbEclusesChoisies_ = new BuRadioButton("Ecluses s�lectionn�es");
  /**
   * groupe de boutons relatif aux gares
   */
  private final ButtonGroup bgGares_ = new ButtonGroup();
  /**
   * bouton radio pour l'affichage d'aucun renseignement concernant les gares
   */
  private final BuRadioButton rbAucuneGare_ = new BuRadioButton("Aucune gare");
  /**
   * bouton radio pour l'affichage des renseignements concernant toutes les gares
   */
  private final BuRadioButton rbToutesGares_ = new BuRadioButton("Toutes les gares");
  /**
   * bouton radio pour l'affichage des renseignements concernant les gares s�lectionn�es dans la liste
   */
  private final BuRadioButton rbGaresChoisies_ = new BuRadioButton("Gares s�lectionn�es");
  /**
   * liste affichant les biefs cr��s
   */
  private final JList lBiefsCrees_ = new JList();
  /**
   * ascenceur pour la liste des biefs cr��s
   */
  private final JScrollPane BiefsCreesScrollPane_ = new JScrollPane(lBiefsCrees_,
      ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
  /**
   * liste affichant la liste des �cluses cr��es
   */
  private final JList lEclusesCreees_ = new JList();
  /**
   * ascenceur pour la liste des �cluses cr��es
   */
  private final JScrollPane EclusesCreeesScrollPane_ = new JScrollPane(lEclusesCreees_,
      ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
  /**
   * liste affichant la liste des gares cr��es
   */
  private final JList lGaresCreees_ = new JList();
  /**
   * ascenceur pour la liste des gares cr��es
   */
  private final JScrollPane GaresCreeesScrollPane_ = new JScrollPane(lGaresCreees_,
      ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
  /**
   * panel contenant les boutons, il est plac� en bas � droite
   */
  private final BuPanel pBoutons_ = new BuPanel();
  /**
   * panel contenant les diff�rents champs de la fen�tre
   */
  private final BuPanel pDonnees_ = new BuPanel();
  /**
   * fen�tre interne affichant le rappel des donn�es sous forme HTML
   */
  private BuBrowserFrame fRappelDonnees_;
  /**
   * une instance de SinaviImplementation
   */
  private BuCommonImplementation app_;

  /**
   * Constructeur
   * 
   * @param _app une instance de SinaviImplementation
   */
  public SinaviFilleChoixRappelDonnees(final BuCommonImplementation _app) {
    super("Choix du rappel des donnees", false, false, false, false);
    app_ = _app;
    fRappelDonnees_ = null;
    // Ajout des listener aux boutons
    bValider_.addActionListener(this);
    bAnnuler_.addActionListener(this);
    // placement des checkbox dans les groupe de boutons dedies
    bgBiefs_.add(rbAucunBief_);
    bgBiefs_.add(rbTousBiefs_);
    bgBiefs_.add(rbBiefsChoisis_);
    bgEcluses_.add(rbAucuneEcluse_);
    bgEcluses_.add(rbToutesEcluses_);
    bgEcluses_.add(rbEclusesChoisies_);
    bgGares_.add(rbAucuneGare_);
    bgGares_.add(rbToutesGares_);
    bgGares_.add(rbGaresChoisies_);
    // selection par defaut du bouton Aucun
    rbAucunBief_.setSelected(true);
    rbAucuneEcluse_.setSelected(true);
    rbAucuneGare_.setSelected(true);
    // Contrainte des listes
    final Dimension dimliste_ = new Dimension(150, 200);
    BiefsCreesScrollPane_.setPreferredSize(dimliste_);
    lBiefsCrees_.setCellRenderer(new SinaviBiefCellRenderer());
    lBiefsCrees_.setModel(SinaviImplementation.SINAVIBIEFLISTMODEL);
    EclusesCreeesScrollPane_.setPreferredSize(dimliste_);
    lEclusesCreees_.setCellRenderer(new SinaviEcluseCellRenderer());
    lEclusesCreees_.setModel(SinaviImplementation.SINAVIECLUSELISTMODEL);
    GaresCreeesScrollPane_.setPreferredSize(dimliste_);
    lGaresCreees_.setCellRenderer(new SinaviGareCellRenderer());
    lGaresCreees_.setModel(SinaviImplementation.SINAVIGARELISTMODEL);
    // remplissage de la fenetre
    ((JComponent) getContentPane()).setBorder(new EmptyBorder(25, 25, 25, 25));
    getContentPane().setLayout(new BorderLayout());
    pDonnees_.setLayout(new BuGridLayout(3));
    pDonnees_.add(cbDonneesGenerales_);
    pDonnees_.add(cbBateauxDefinis_);
    pDonnees_.add(new BuLabel(""));
    pDonnees_.add(rbAucunBief_);
    pDonnees_.add(rbAucuneEcluse_);
    pDonnees_.add(rbAucuneGare_);
    pDonnees_.add(rbTousBiefs_);
    pDonnees_.add(rbToutesEcluses_);
    pDonnees_.add(rbToutesGares_);
    pDonnees_.add(rbBiefsChoisis_);
    pDonnees_.add(rbEclusesChoisies_);
    pDonnees_.add(rbGaresChoisies_);
    pDonnees_.add(BiefsCreesScrollPane_);
    pDonnees_.add(EclusesCreeesScrollPane_);
    pDonnees_.add(GaresCreeesScrollPane_);
    pBoutons_.setLayout(new FlowLayout(FlowLayout.RIGHT));
    pBoutons_.add(bAnnuler_);
    pBoutons_.add(bValider_);
    getContentPane().add(pDonnees_, BorderLayout.CENTER);
    getContentPane().add(pBoutons_, BorderLayout.SOUTH);
    pack();
    setVisible(true);
  }

  /**
   * si action sur le bouton valider, lecture des choix de l'utilisateur et affichage dans une fen�tre interne du rappel
   * des donn�es choisies si action sur le bouton annuler, fermeture de la fen�tre
   * 
   * @param _e
   */
  public void actionPerformed(final ActionEvent _e) {
    if (_e.getSource() == bValider_) {
      boolean donneesgenerales_ok_ = false;
      boolean bateauxdefinis_ok_ = false;
      boolean biefscrees_ok_ = false;
      boolean eclusescreees_ok_ = false;
      boolean garescreees_ok_ = false;
      IBiefNavigation[] biefschoisis_ = null;
      IEcluseFluviale[] ecluseschoisies_ = null;
      IGare[] gareschoisies_ = null;
      Object[] temp_;
      if (cbDonneesGenerales_.isSelected()) {
        donneesgenerales_ok_ = true;
      }
      if (cbBateauxDefinis_.isSelected()) {
        bateauxdefinis_ok_ = true;
      }
      if (rbTousBiefs_.isSelected()) {
        biefscrees_ok_ = true;
        biefschoisis_ = null; // tous les biefs
      }
      if (rbBiefsChoisis_.isSelected()) {
        biefscrees_ok_ = true;
        temp_ = lBiefsCrees_.getSelectedValues(); // les biefs s�lectionn�s
        biefschoisis_ = new IBiefNavigation[temp_.length];
        for (int i = 0; i < temp_.length; i++) {
          biefschoisis_[i] = (IBiefNavigationHelper.narrow((org.omg.CORBA.Object) temp_[i]));
        }
      }
      if (rbToutesEcluses_.isSelected()) {
        eclusescreees_ok_ = true;
        ecluseschoisies_ = null; // toutes les ecluses
      }
      if (rbEclusesChoisies_.isSelected()) {
        eclusescreees_ok_ = true;
        temp_ = lEclusesCreees_.getSelectedValues();
        // les �cluses s�lectionn�es
        ecluseschoisies_ = new IEcluseFluviale[temp_.length];
        for (int i = 0; i < temp_.length; i++) {
          ecluseschoisies_[i] = (IEcluseFluvialeHelper.narrow((org.omg.CORBA.Object) temp_[i]));
        }
      }
      if (rbToutesGares_.isSelected()) {
        garescreees_ok_ = true;
        gareschoisies_ = null; // toutes les gares
      }
      if (rbGaresChoisies_.isSelected()) {
        garescreees_ok_ = true;
        temp_ = lGaresCreees_.getSelectedValues(); // les gares s�lectionn�es
        gareschoisies_ = new IGare[temp_.length];
        for (int i = 0; i < temp_.length; i++) {
          gareschoisies_[i] = (IGareHelper.narrow((org.omg.CORBA.Object) temp_[i]));
        }
      }
      if (fRappelDonnees_ == null) {
        fRappelDonnees_ = new BuBrowserFrame(app_);
        fRappelDonnees_.putClientProperty("NomFichier", null);
        app_.addInternalFrame(fRappelDonnees_);
      } else {
        if (fRappelDonnees_.isClosed()) {
          app_.addInternalFrame(fRappelDonnees_);
        } else {
          app_.activateInternalFrame(fRappelDonnees_);
        }
      }
      final SinaviRappelDonnees srd_ = new SinaviRappelDonnees();
      fRappelDonnees_.setHtmlSource(srd_.construitRappelDonnees(donneesgenerales_ok_, bateauxdefinis_ok_,
          biefscrees_ok_, eclusescreees_ok_, garescreees_ok_, biefschoisis_, ecluseschoisies_, gareschoisies_), "");
      try {
        setClosed(true);
      } catch (final Exception _pve) {}
    } else {
      try {
        setClosed(true);
      } catch (final Exception _pve) {}
    }
  }
}
