/*
 * @file         SinaviBateauPartantListModel.java
 * @creation     2001-05-17
 * @modification $Date: 2006-09-19 15:08:59 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.sinavi;

import javax.swing.AbstractListModel;

import org.fudaa.dodico.corba.navigation.IGare;

/**
 * D�finit le mod�le pour la liste affichant les types de navire partant d'une gare
 * 
 * @version $Revision: 1.6 $ $Date: 2006-09-19 15:08:59 $ by $Author: deniger $
 * @author Aline Marechalle , Franck Lejeune
 */
public class SinaviBateauPartantListModel extends AbstractListModel {

  /**
   * variable d�finissant la gare de laquelle on affiche les types de navires partant
   */
  private IGare gare_;

  /**
   * retourne le type de navire s�lectionn� dans la liste
   * 
   * @param _index
   * @return le navire s�lectionn� dans la liste
   */
  public Object getElementAt(final int _index) {
    if (gare_ == null) {
      return null;
    } else if (gare_.naviresPartants().length == 0) {
      return null;
    } else {
      return gare_.naviresPartants()[_index];
    }
  }

  /**
   * nombre de navire partant contenu dans la liste
   * 
   * @return Le nombre de navires partant
   */
  public int getSize() {
    if (gare_ == null) {
      return 0;
    }
    return gare_.naviresPartants().length;
  }

  /**
   * permet de rafraichir l'affichage de la liste ( modification ou ajout d'un bateau )
   */
  public void refresh() {
    fireContentsChanged(this, 0, getSize() - 1);
  }

  /**
   * Affecte la valeur Gare pour afficher les types de navires partant de cette gare
   * 
   * @param _gare La nouvelle valeur Gare
   */
  public void setGare(final IGare _gare) {
    gare_ = _gare;
    refresh();
  }
}
