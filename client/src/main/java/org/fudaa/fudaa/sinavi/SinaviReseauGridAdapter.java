/*
 * @file         SinaviReseauGridAdapter.java
 * @creation     2001-05-17
 * @modification $Date: 2006-09-19 15:09:00 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.sinavi;

import com.memoire.dja.DjaGridEvent;
import com.memoire.dja.DjaGridListener;
import com.memoire.dja.DjaLink;

import org.fudaa.dodico.objet.UsineLib;

/**
 * impl�mentation d'un GridAdapter pour le r�seau de Sinavi
 * 
 * @version $Revision: 1.12 $ $Date: 2006-09-19 15:09:00 $ by $Author: deniger $
 * @author Aline Marechalle , Franck Lejeune
 */
public class SinaviReseauGridAdapter implements DjaGridListener {

  /**
   * pas utilis�
   * 
   * @param _evt
   */
  public void objectAdded(final DjaGridEvent _evt) {}

  /**
   * si un objet est supprim� du r�seau, suppression de l'objet m�tier associ� de l'�tude
   * 
   * @param _evt
   */
  public void objectRemoved(final DjaGridEvent _evt) {
    if ((_evt.getObject() instanceof SinaviReseauBief) || (_evt.getObject() instanceof SinaviReseauEcluse)) {
      if (((DjaLink) _evt.getObject()).getBeginObject() != null) {
        ((SinaviReseauGare) ((DjaLink) _evt.getObject()).getBeginObject()).LibereAttache(((DjaLink) (_evt.getObject()))
            .getBeginPosition());
      }
      if (((DjaLink) _evt.getObject()).getEndObject() != null) {
        ((SinaviReseauGare) ((DjaLink) _evt.getObject()).getEndObject()).LibereAttache(((DjaLink) (_evt.getObject()))
            .getEndPosition());
      }
    }
    if (_evt.getObject() instanceof SinaviReseauGare) {
      UsineLib.findUsine().supprimeNavigationGare(((SinaviReseauGare) _evt.getObject()).getGare());
    }
    if (_evt.getObject() instanceof SinaviReseauBief) {
      SinaviImplementation.ETUDE_SINAVI.reseau().enleveBief(((SinaviReseauBief) _evt.getObject()).getBief());
    }
    if (_evt.getObject() instanceof SinaviReseauEcluse) {
      SinaviImplementation.ETUDE_SINAVI.reseau().enleveEcluse(((SinaviReseauEcluse) _evt.getObject()).getEcluse());
    }
  }

  /**
   * pas utilis�
   * 
   * @param _evt
   */
  public void objectConnected(final DjaGridEvent _evt) {}

  /**
   * si un objet est d�connect� alors traitement
   * 
   * @param _evt
   */
  public void objectDisconnected(final DjaGridEvent _evt) {
    if ((_evt.getObject() instanceof SinaviReseauBief) || (_evt.getObject() instanceof SinaviReseauEcluse)) {
      if (((DjaLink) _evt.getObject()).getBeginObject() != null) {
        ((SinaviReseauGare) ((DjaLink) _evt.getObject()).getBeginObject()).LibereAttache(((DjaLink) (_evt.getObject()))
            .getBeginPosition());
      }
      if (((DjaLink) _evt.getObject()).getEndObject() != null) {
        ((SinaviReseauGare) ((DjaLink) _evt.getObject()).getEndObject()).LibereAttache(((DjaLink) (_evt.getObject()))
            .getEndPosition());
      }
    }
  }

  /**
   * pas utilis�
   * 
   * @param _evt
   */
  public void objectSelected(final DjaGridEvent _evt) {}

  /**
   * pas utilis�
   * 
   * @param _evt
   */
  public void objectUnselected(final DjaGridEvent _evt) {}

  /**
   * pas utilis�
   * 
   * @param _evt
   */
  public void objectModified(final DjaGridEvent _evt) {}
}
