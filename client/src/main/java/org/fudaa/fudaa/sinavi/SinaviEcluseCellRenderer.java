/*
 * @file         SinaviEcluseCellRenderer.java
 * @creation     2001-05-17
 * @modification $Date: 2006-09-19 15:09:00 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.sinavi;

import java.awt.Component;

import javax.swing.DefaultListCellRenderer;
import javax.swing.JList;
import javax.swing.ListCellRenderer;

import org.fudaa.dodico.corba.navigation.IEcluseFluvialeHelper;

/**
 * impl�mentation du rendu de la liste des �cluses
 * 
 * @version $Revision: 1.6 $ $Date: 2006-09-19 15:09:00 $ by $Author: deniger $
 * @author Aline Marechalle , Franck Lejeune
 */
public class SinaviEcluseCellRenderer extends DefaultListCellRenderer implements ListCellRenderer<Object> {

  /**
   * D�finit le rendu de la liste des �cluses
   */
  public SinaviEcluseCellRenderer() {}

  /**
   * retourne le nom de l'�cluse � partir de l'objet EcluseFluviale
   * 
   * @param list la liste � laquelle s'appplique le rendu
   * @param value l'objet EcluseFluviale
   * @param index l'index dans la liste
   * @param isSelected ...
   * @param cellHasFocus ...
   * @return le nom de l'�cluse
   */
  public Component getListCellRendererComponent(final JList list, final Object value, final int index,
      final boolean isSelected, final boolean cellHasFocus) {
    setBackground(isSelected ? list.getSelectionBackground() : list.getBackground());
    setForeground(list.getForeground());
    setText(IEcluseFluvialeHelper.narrow((org.omg.CORBA.Object) value).nom());
    return this;
  }
}
