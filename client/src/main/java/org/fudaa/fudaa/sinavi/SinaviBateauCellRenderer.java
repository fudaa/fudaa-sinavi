/*
 * @file         SinaviBateauCellRenderer.java
 * @creation     2001-05-17
 * @modification $Date: 2006-09-19 15:09:00 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.sinavi;

import java.awt.Component;

import javax.swing.DefaultListCellRenderer;
import javax.swing.JList;
import javax.swing.ListCellRenderer;

import org.fudaa.dodico.corba.navigation.INavireTypeHelper;

/**
 * impl�mentation du rendu de la liste des bateaux
 * 
 * @version $Revision: 1.7 $ $Date: 2006-09-19 15:09:00 $ by $Author: deniger $
 * @author Aline Marechalle , Franck Lejeune
 */
public class SinaviBateauCellRenderer extends DefaultListCellRenderer implements ListCellRenderer<Object> {

  /**
   * D�finit le rendu des listes affichant les types de navires
   */
  public SinaviBateauCellRenderer() {}

  /**
   * retourne le type de navires � partir de l'objet NavireType
   * 
   * @param list la liste sur laquelle le rendu s'applique
   * @param value l'objet NavireType
   * @param index l'index dans la liste
   * @param isSelected ...
   * @param cellHasFocus ...
   * @return retourne le type de navires � partir de l'objet NavireType
   */
  public Component getListCellRendererComponent(final JList list, final Object value, final int index,
      final boolean isSelected, final boolean cellHasFocus) {
    setBackground(isSelected ? list.getSelectionBackground() : list.getBackground());
    setForeground(list.getForeground());
    // affiche le type de navire partir de l'objet NavireType
    setText(INavireTypeHelper.narrow((org.omg.CORBA.Object) value).type());
    return this;
  }
}
