/*
 * @file         SinaviFilleReseauBief.java
 * @creation     2001-05-17
 * @modification $Date: 2006-09-19 15:09:00 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.sinavi;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JComponent;
import javax.swing.border.EmptyBorder;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuCommonImplementation;
import com.memoire.bu.BuDialogMessage;
import com.memoire.bu.BuInternalFrame;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuResource;

import org.fudaa.dodico.corba.navigation.IBiefNavigation;

/**
 * impl�mentation d'une fen�tre interne permettant de renseigner le bief
 * 
 * @version $Revision: 1.8 $ $Date: 2006-09-19 15:09:00 $ by $Author: deniger $
 * @author Aline Marechalle , Franck Lejeune
 */
public class SinaviFilleReseauBief extends BuInternalFrame implements ActionListener {

  /**
   * bouton valider
   */
  BuButton bValider_ = new BuButton(BuResource.BU.getIcon("VALIDER"), "Valider");
  /**
   * bouton annuler
   */
  BuButton bAnnuler_ = new BuButton(BuResource.BU.getIcon("ANNULER"), "Annuler");
  /**
   * panel contenant les boutons, il est plac� en bas � droite
   */
  BuPanel pbouton_;
  /**
   * panel contenant les param�tre du bief ( r�plique de l'onglet bief )
   */
  SinaviBiefsParametres parametresbief_;
  /**
   * une instance de SinaviImplementation
   */
  BuCommonImplementation app_;
  /**
   * Objet m�tier bief
   */
  IBiefNavigation bief_;

  /**
   * Constructeur SinaviFilleReseauBief.
   * 
   * @param _app une instance de SinaviImplementation
   * @param _bief l'objet m�tier bief � renseigner
   */
  public SinaviFilleReseauBief(final BuCommonImplementation _app, final IBiefNavigation _bief) {
    super("Parametres du bief", false, false, false, false);
    app_ = _app;
    bief_ = _bief;
    // panel des param�tres du bief avec true car mode_edition
    parametresbief_ = new SinaviBiefsParametres(app_, true);
    // ajout des listener aux boutons
    bAnnuler_.addActionListener(this);
    bValider_.addActionListener(this);
    pbouton_ = new BuPanel();
    pbouton_.setLayout(new FlowLayout(FlowLayout.RIGHT));
    pbouton_.add(bAnnuler_);
    pbouton_.add(bValider_);
    getContentPane().setLayout(new BorderLayout(25, 0));
    getContentPane().add("North", parametresbief_);
    getContentPane().add("South", pbouton_);
    ((JComponent) getContentPane()).setBorder(new EmptyBorder(25, 25, 25, 25));
    pack();
    setFrameIcon(BuResource.BU.getIcon("parametre"));
    if (!bief_.nom().equals(null)) {
      parametresbief_.getBiefParametres(bief_);
    }
    setVisible(true);
  }

  /**
   * si action sur le bouton valider test de la validit� des champs et sauvegarde si ok, fermeture de la fen�tre si
   * action sur le bouton annuler, fermeture de la fen�tre
   * 
   * @param _e
   */
  public void actionPerformed(final ActionEvent _e) {
    if (_e.getSource() == bValider_) {
      if (parametresbief_.validBiefParametres()) {
        parametresbief_.setBiefParametres(bief_);
        try {
          setClosed(true);
        } catch (final Exception _pve) {}
      } else {
        final BuDialogMessage dialog_mess = new BuDialogMessage(app_, SinaviImplementation.informationsSoftware(),
            "Param�tres du bief\nincomplets");
        dialog_mess.activate();
      }
    } else if (_e.getSource() == bAnnuler_) {
      try {
        setClosed(true);
      } catch (final Exception _pve) {}
    }
  }
}
