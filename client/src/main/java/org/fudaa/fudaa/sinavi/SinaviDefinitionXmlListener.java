/*
 * @file         SinaviDefinitionXmlListener.java
 * @creation     2001-05-17
 * @modification $Date: 2006-09-19 15:09:00 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.sinavi;

import java.util.Vector;

import com.memoire.xml.XmlListener;

import org.fudaa.dodico.corba.navigation.INavireTypeHelper;
import org.fudaa.dodico.corba.navigation.LGroupeNavireType;

import org.fudaa.dodico.objet.UsineLib;

/**
 * impl�mentation du Listener Xml n�cessaire au parseur Xml
 * 
 * @version $Revision: 1.12 $ $Date: 2006-09-19 15:09:00 $ by $Author: deniger $
 * @author Aline Marechalle , Franck Lejeune
 */
public class SinaviDefinitionXmlListener implements XmlListener {

  /**
   * num�ro de cat�gorie ( 0 = Plaisancier ; 1 = Passager ; 2 = Freycinet ; 3 = Convois Pouss�s ; 4 = Personnalis�
   */
  private int nbcat_;
  /**
   * nombre de navire dans une
   */
  private int nbnavire_;
  /**
   * chaine r�cup�r�e par le parseur
   */
  private String chaine_;
  /**
   * tableau de Vecteur de type de navire
   */
  private Vector[] navires_;

  /**
   * Listener Xml n�cessaire au parseur, d�finit les actions � ex�cuter pour chaque balise ouvrante ou fermante Xml
   * diff�rente
   * 
   * @param _navires
   */
  public SinaviDefinitionXmlListener(final Vector[] _navires) {
    navires_ = _navires;
    // categorie_ = "";
    nbcat_ = 0;
    nbnavire_ = 0;
    chaine_ = "";
  }

  /**
   * appel�e � la lecture d'une balise ouvrante
   * 
   * @param _tag la balise ouvrante lue
   */
  public void startElement(final String _tag) {
    if (_tag.equals("SINAVI")) {
      // System.out.println("Ouverture du fichier Sinavi.xml");
    }
    if (_tag.equals("CATEGORIE")) {
      navires_[nbcat_] = new Vector();
    }
    if (_tag.equals("NOMCATEGORIE")) {
      ;
    }
    if (_tag.equals("NAVIRE")) {
      navires_[nbcat_].addElement(UsineLib.findUsine().creeNavigationNavireType());
      nbnavire_++;
    }
    if (_tag.equals("NOM")) {
      ;
    }
    if (_tag.equals("LONGUEUR")) {
      ;
    }
    if (_tag.equals("LARGEUR")) {
      ;
    }
    if (_tag.equals("TIRANTDEAU")) {
      ;
    }
    if (_tag.equals("VITESSE")) {
      ;
    }
  }

  public void error(final String _message) {
    System.err.println("erreur lors de serialisation " + _message);
  }

  public void location(final String _orig, final int _lineno, final int _charno) {
    System.out.println("non implant�e");
  }

  /**
   * appel�e � la lecture d'une balise fermante
   * 
   * @param _tag la balise fermante lue
   */
  public void endElement(final String _tag) {
    if (_tag.equals("NOMCATEGORIE")) {
      // System.out.println("Lecture de la categorie " + chaine_);
    }
    if (_tag.equals("NOM")) {
      INavireTypeHelper.narrow((org.omg.CORBA.Object) navires_[nbcat_].elementAt(nbnavire_ - 1)).type(chaine_);
      if (nbcat_ == 0) {
        INavireTypeHelper.narrow((org.omg.CORBA.Object) navires_[nbcat_].elementAt(nbnavire_ - 1)).groupeNavire(
            LGroupeNavireType.PLAISANCIER);
      } else if (nbcat_ == 1) {
        INavireTypeHelper.narrow((org.omg.CORBA.Object) navires_[nbcat_].elementAt(nbnavire_ - 1)).groupeNavire(
            LGroupeNavireType.PASSAGER);
      } else if (nbcat_ == 2) {
        INavireTypeHelper.narrow((org.omg.CORBA.Object) navires_[nbcat_].elementAt(nbnavire_ - 1)).groupeNavire(
            LGroupeNavireType.FREYCINET);
      } else if (nbcat_ == 3) {
        INavireTypeHelper.narrow((org.omg.CORBA.Object) navires_[nbcat_].elementAt(nbnavire_ - 1)).groupeNavire(
            LGroupeNavireType.CONVOI_POUSSE);
      } else if (nbcat_ == 4) {
        INavireTypeHelper.narrow((org.omg.CORBA.Object) navires_[nbcat_].elementAt(nbnavire_ - 1)).groupeNavire(
            LGroupeNavireType.PERSONNALISE);
      }
    }
    if (_tag.equals("LONGUEUR")) {
      INavireTypeHelper.narrow((org.omg.CORBA.Object) navires_[nbcat_].elementAt(nbnavire_ - 1)).longueur(
          new Double(chaine_).doubleValue());
    }
    if (_tag.equals("LARGEUR")) {
      INavireTypeHelper.narrow((org.omg.CORBA.Object) navires_[nbcat_].elementAt(nbnavire_ - 1)).largeur(
          new Double(chaine_).doubleValue());
    }
    if (_tag.equals("TIRANTDEAU")) {
      INavireTypeHelper.narrow((org.omg.CORBA.Object) navires_[nbcat_].elementAt(nbnavire_ - 1)).tirantMoy(
          new Double(chaine_).doubleValue());
    }
    if (_tag.equals("VITESSE")) {
      INavireTypeHelper.narrow((org.omg.CORBA.Object) navires_[nbcat_].elementAt(nbnavire_ - 1)).vitesseMoy(
          new Double(chaine_).doubleValue());
    }
    if (_tag.equals("NAVIRE")) {
      ;
    }
    if (_tag.equals("CATEGORIE")) {
      nbcat_++;
      nbnavire_ = 0;
    }
    if (_tag.equals("SINAVI")) {
      // System.out.println("Fin du fichier Sinavi.xml");
    }
  }

  /**
   * non utilis�e
   * 
   * @param _name
   * @param _value
   */
  public void attribute(final String _name, final String _value) {}

  /**
   * appel�e � chaque lecture d'une chaine de caract�re
   * 
   * @param _data la chaine lue
   */
  public void text(final String _data) {
    chaine_ = _data;
  }
}
