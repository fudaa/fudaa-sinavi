/*
 * @file         SinaviReseauBief.java
 * @creation     2001-05-17
 * @modification $Date: 2006-09-19 15:09:00 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.sinavi;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Rectangle;

import com.memoire.dja.DjaDirectArrow;
import com.memoire.dja.DjaObject;
import com.memoire.dja.DjaText;

import org.fudaa.dodico.corba.navigation.IBiefNavigation;

/**
 * impl�mentation de l'objet Dja pour repr�senter un bief sur le r�seau.
 * 
 * @version $Revision: 1.8 $ $Date: 2006-09-19 15:09:00 $ by $Author: deniger $
 * @author Aline Marechalle , Franck Lejeune
 */
public class SinaviReseauBief extends DjaDirectArrow {

  /**
   * Objet m�tier bief.
   */
  private transient IBiefNavigation bief_;
  /**
   * Cadre o� les informations concernant le bief du cot� montant sont report�es pour l'animation.
   */
  private DjaText textMontBief_;
  /**
   * Cadre o� les informations concernant le bief du cot� avalant sont report�es pour l'animation.
   */
  private DjaText textAvalBief_;
  /**
   * Cadre o� est affich�e le nom du bief.
   */
  private DjaText nomBief_;
  /**
   * Rectangle � dessiner autour des informations.
   */
  private Rectangle rect_;
  /**
   * Numero d'enregistrement, la position du bief dans le tableau de biefs de l'�tude.
   */
  private int numeroEnregistrement_;

  /**
   * SinaviReseauBief Constructeur sp�cifique pour YAPOD.
   */
  public SinaviReseauBief() {
    bief_ = null;
    setEndType(NONE);
    setBeginType(NONE);
    setForeground(new Color(122, 220, 255));
    putProperty("epaisseur", "17");
    final String nom = "";
    nomBief_ = new DjaText(this, 0, nom);
    nomBief_.setPosition(CENTER);
    textMontBief_ = new DjaText(this, 0, "      \n     \n     \n      \n     \n", true, 0, -55);
    textMontBief_.setFont(new Font("Monospaced", Font.PLAIN, 10));
    textMontBief_.setPosition(CENTER);
    textMontBief_.setAlignment(RIGHT);
    textAvalBief_ = new DjaText(this, 0, "      \n     \n     \n      \n     \n", true, 0, 55);
    textAvalBief_.setFont(new Font("Monospaced", Font.PLAIN, 10));
    textAvalBief_.setPosition(CENTER);
    textAvalBief_.setAlignment(RIGHT);
    setTextArray(new DjaText[] { nomBief_, textMontBief_, textAvalBief_ });
  }

  /**
   * SinaviReseauBief Constructeur d'un bief au point de vue graphique.
   * 
   * @param _bief
   */
  public SinaviReseauBief(final IBiefNavigation _bief) {
    bief_ = _bief;
    setEndType(NONE);
    setBeginType(NONE);
    setForeground(new Color(122, 220, 255));
    putProperty("epaisseur", "17");
    putProperty("numeroEnregistrement", (new Integer(-1)).toString());
    nomBief_ = new DjaText(this, 0, bief_.nom());
    nomBief_.setPosition(CENTER);
    textMontBief_ = new DjaText(this, 0, "      \n     \n     \n      \n     \n", true, 0, -55);
    textMontBief_.setFont(new Font("Monospaced", Font.PLAIN, 10));
    textMontBief_.setPosition(CENTER);
    textMontBief_.setAlignment(RIGHT);
    textAvalBief_ = new DjaText(this, 0, "      \n     \n     \n      \n     \n", true, 0, 55);
    textAvalBief_.setFont(new Font("Monospaced", Font.PLAIN, 10));
    textAvalBief_.setPosition(CENTER);
    textAvalBief_.setAlignment(RIGHT);
    setTextArray(new DjaText[] { nomBief_, textMontBief_, textAvalBief_ });
  }

  /**
   * methode appel�e avant la sauvegarde du r�seau sous forme de String. Elle permet d'attribuer le num�ro
   * d'enregistrement du bief
   */
  public void beforeSaving() {
    super.beforeSaving();
    for (int indexbief = 0; indexbief < SinaviImplementation.ETUDE_SINAVI.reseau().biefs().length; indexbief++) {
      if ((SinaviImplementation.ETUDE_SINAVI.reseau().biefs()[indexbief]).egale(bief_)) {
        putProperty("numeroEnregistrement", (new Integer(indexbief)).toString());
      }
    }
  }

  /**
   * methode appel�e apres l'ouverture d'une etude et la construction physique du r�seau Elle permet de d�finir le bief
   * � partir du num�ro d'enregistrement du bief
   */
  public void afterLoading() {
    super.afterLoading();
    numeroEnregistrement_ = new Integer(getProperty("numeroEnregistrement")).intValue();
    if (numeroEnregistrement_ != -1) {
      bief_ = SinaviImplementation.ETUDE_SINAVI.reseau().biefs()[numeroEnregistrement_];
    } else {
      /*
       * BuDialogMessage dialog_mess = new BuDialogMessage(app_, SinaviImplementation.informationsSoftware(), "Problemes
       * de chargement des biefs"); dialog_mess.activate();
       */
    }
  }

  public void setBeginObject(final DjaObject _begin) {
    if (_begin != null) {
      super.setBeginObject(_begin);
    }
  }

  public void setEndObject(final DjaObject _begin) {
    if (_begin != null) {
      super.setEndObject(_begin);
    }
  }

  /*
   * public DjaText[] getTexts() { DjaText[] r=super.getTexts(); if(r.length>0) { r[0].setPosition (RELATIVE_ANCHOR);
   * r[0].setReference(nbAnchors/2); } return r; }
   */
  /**
   * retourne la valeur Bief de SinaviReseauBief object.
   * 
   * @return La valeur Bief
   */
  public IBiefNavigation getBief() {
    return bief_;
  }

  /**
   * retourne la valeur TextMontBief de SinaviReseauBief object.
   * 
   * @return La valeur TextMontBief
   */
  public DjaText getTextMontBief() {
    return textMontBief_;
  }

  /**
   * retourne la valeur TextAvalBief de SinaviReseauBief object
   * 
   * @return La valeur TextAvalBief
   */
  public DjaText getTextAvalBief() {
    return textAvalBief_;
  }

  /**
   * Dessine l'objet graphique bief avec ses dif�rentes propri�t�s.
   * 
   * @param _g
   */
  public void paintObject(final Graphics _g) {
    super.paintObject(_g);
    final Color coul = new Color(122, 220, 255);
    nomBief_.setText(bief_.nom());
    SinaviAnimation.contourDjaText(_g, textMontBief_, rect_, coul);
    SinaviAnimation.contourDjaText(_g, textAvalBief_, rect_, coul);
  }

  /**
   * Ecrit les informations pour les sens montant et descendant d'un bief.
   * 
   * @param _str : chaine de caract�re � �crire
   * @param _mont : sera = true si sens montant
   */
  public void ecrireTextBief(final String _str, final boolean _mont) {
    if (_mont) {
      textMontBief_.setText(_str);
    } else {
      textAvalBief_.setText(_str);
    }
  }
}
