/*
 * @file         SinaviFilleTrajetBateau.java
 * @creation     2001-05-17
 * @modification $Date: 2006-09-19 15:08:59 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.sinavi;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JComponent;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.EmptyBorder;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuCommonImplementation;
import com.memoire.bu.BuDialogMessage;
import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuInternalFrame;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuTextField;

import org.fudaa.dodico.corba.navigation.IGare;
import org.fudaa.dodico.corba.navigation.IGareHelper;
import org.fudaa.dodico.corba.navigation.INavireType;
import org.fudaa.dodico.corba.navigation.ITrajetFluvial;

import org.fudaa.dodico.objet.UsineLib;

/**
 * impl�mentation d'une fen�tre interne permettant de d�finir un trajet
 * 
 * @version $Revision: 1.11 $ $Date: 2006-09-19 15:08:59 $ by $Author: deniger $
 * @author Aline Marechalle , Franck Lejeune
 */
public class SinaviFilleTrajetBateau extends BuInternalFrame implements ActionListener {

  /**
   * vitesse du bateau sur le trajet
   */
  private final BuTextField tfVitesseBateau_ = BuTextField.createDoubleField();
  /**
   * bouton valider
   */
  private final BuButton bValider_ = new BuButton(SinaviResource.SINAVI.getIcon("VALIDER"), "Valider");
  /**
   * bouton annuler
   */
  private final BuButton bAnnuler_ = new BuButton(SinaviResource.SINAVI.getIcon("ANNULER"), "Annuler");
  /**
   * liste affichant les gares existantes sur le r�seau
   */
  private final JList lGaresCreees_ = new JList();
  /**
   * la gare de d�part auquel on va ajouter le trajet
   */
  private IGare gare_;
  /**
   * le type de navire pour le trajet
   */
  private INavireType navire_;
  /**
   * ascenseur pour la liste des gares cr��es
   */
  private final JScrollPane GaresCreeesScrollPane_ = new JScrollPane(lGaresCreees_,
      ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
  /**
   * panel contenant les boutons, il est plac� en bas � droite
   */
  private final BuPanel pBoutons_ = new BuPanel();
  /**
   * panel contenant les diff�rents champs de la fen�tre
   */
  private final BuPanel pDonnees_ = new BuPanel();
  /**
   * une instance de SinaviImplementation
   */
  private BuCommonImplementation app_;

  /**
   * Constructeur SinaviFilleTrajetBateau.
   * 
   * @param _app uns instance de SinaviImplementation
   * @param _gare la gare de d�part
   * @param _navire le type de navire partant
   */
  public SinaviFilleTrajetBateau(final BuCommonImplementation _app, final IGare _gare, final INavireType _navire) {
    super("D�finition d'un trajet", false, false, false, false);
    app_ = _app;
    gare_ = _gare;
    navire_ = _navire;
    // Ajout des listener aux boutons
    bValider_.addActionListener(this);
    bAnnuler_.addActionListener(this);
    // initialisation de la vitesse a 0
    tfVitesseBateau_.setValue(new Double(_navire.vitesseMoy()));
    // Contraintes des listes
    final Dimension dimgc_ = new Dimension(150, 90);
    GaresCreeesScrollPane_.setPreferredSize(dimgc_);
    lGaresCreees_.setCellRenderer(new SinaviGareCellRenderer());
    lGaresCreees_.setModel(SinaviImplementation.SINAVIGARELISTMODEL);
    // remplissage de la fenetre
    ((JComponent) getContentPane()).setBorder(new EmptyBorder(25, 25, 25, 25));
    getContentPane().setLayout(new BorderLayout());
    pDonnees_.setLayout(new BuGridLayout(2));
    pDonnees_.add(new BuLabel("Vitesse du bateau :"));
    pDonnees_.add(tfVitesseBateau_);
    pDonnees_.add(new BuLabel("Gare d'arriv�e :"));
    pDonnees_.add(GaresCreeesScrollPane_);
    pBoutons_.setLayout(new FlowLayout(FlowLayout.RIGHT));
    pBoutons_.add(bAnnuler_);
    pBoutons_.add(bValider_);
    getContentPane().add(pDonnees_, BorderLayout.CENTER);
    getContentPane().add(pBoutons_, BorderLayout.SOUTH);
    pack();
    setVisible(true);
  }

  /**
   * si action sur le bouton valider, lecture des param�tres du trajet, et si ok, cr�ation du trajet si action sur le
   * bouton annuler, fermeture de la fen�tre
   * 
   * @param _e
   */
  public void actionPerformed(final ActionEvent _e) {
    if (_e.getSource() == bValider_) {
      if ((((Double) tfVitesseBateau_.getValue()).doubleValue() != 0) && (lGaresCreees_.getSelectedValue() != null)) {
        final ITrajetFluvial trajet_ = UsineLib.findUsine().creeNavigationTrajetFluvial();
        trajet_.vitesse(((Double) tfVitesseBateau_.getValue()).doubleValue());
        trajet_.gareArrivee(IGareHelper.narrow((org.omg.CORBA.Object) lGaresCreees_.getSelectedValue()));
        System.out.println("valider" + navire_.type());
        trajet_.navireType(navire_);
        final String test_ = gare_.ajouteTrajet(trajet_);
        if (test_.equals("")) {
          SinaviImplementation.SINAVITABLETRAJETMODEL.refresh();
          SinaviImplementation.SINAVIBATEAUPARTANTLISTMODEL.refresh();
          SinaviImplementation.SINAVITABLELOIMODEL.setTrajetFluvial(trajet_);
          app_.addInternalFrame(new SinaviFilleChoixLoiTrajet(app_, gare_.trajetsParNavire(navire_)[0]));
          try {
            setClosed(true);
          } catch (final Exception _pve) {}
        } else {
          System.out.println(test_);
        }
      } else {
        final BuDialogMessage dialog_mess = new BuDialogMessage(app_, SinaviImplementation.informationsSoftware(),
            "Les champs ne sont pas tous renseign�s");
        dialog_mess.activate();
      }
    } else {
      try {
        setClosed(true);
      } catch (final Exception _pve) {}
    }
  }
}
