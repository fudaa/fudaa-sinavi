/*
 * @file         SinaviResource.java
 * @creation     2001-05-17
 * @modification $Date: 2006-09-19 15:09:00 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.sinavi;

import com.memoire.bu.BuResource;

import org.fudaa.fudaa.ressource.FudaaResource;

/**
 * Ressource de Sinavi
 * 
 * @version $Revision: 1.9 $ $Date: 2006-09-19 15:09:00 $ by $Author: deniger $
 * @author Aline Marechalle , Franck Lejeune
 */
public class SinaviResource extends FudaaResource {

  /**
   * une ressource � utiliser directement
   */
  public final static SinaviResource SINAVI = new SinaviResource(FudaaResource.FUDAA);

  /**
   * SinaviResource
   * 
   * @param _parent
   */
  public SinaviResource(final BuResource _parent) {
    super(_parent);
  }
}
