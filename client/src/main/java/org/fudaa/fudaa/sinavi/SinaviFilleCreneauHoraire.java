/*
 * @file         SinaviFilleCreneauHoraire.java
 * @creation     2001-05-17
 * @modification $Date: 2006-09-19 15:09:00 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.sinavi;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JComponent;
import javax.swing.border.EmptyBorder;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuInternalFrame;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuPanel;

import org.fudaa.dodico.corba.navigation.IEcluseFluviale;

/**
 * impl�mentation d'une fen�tre interne permetant de choisir l'heure de d�but et de fin d'un cr�neau horaire
 * 
 * @version $Revision: 1.8 $ $Date: 2006-09-19 15:09:00 $ by $Author: deniger $
 * @author Aline Marechalle , Franck Lejeune
 */
public class SinaviFilleCreneauHoraire extends BuInternalFrame implements ActionListener {

  /**
   * champs pour rentrer l'heure de d�but
   */
  private final DureeField heureDebut_ = new DureeField(false, false, true, true, false);
  /**
   * champs pour rentrer l'heure de fin
   */
  private final DureeField heureFin_ = new DureeField(false, false, true, true, false);
  /**
   * bouton valider
   */
  private final BuButton bValider_ = new BuButton(SinaviResource.SINAVI.getIcon("VALIDER"), "Valider");
  /**
   * bouton annuler
   */
  private final BuButton bAnnuler_ = new BuButton(SinaviResource.SINAVI.getIcon("ANNULER"), "Annuler");
  /**
   * panel contenant les boutons, il est plac� en bas � droite
   */
  private final BuPanel pBoutons_ = new BuPanel();
  /**
   * panel contenant les diff�rents champs de la fen�tre
   */
  private final BuPanel pDonnees_ = new BuPanel();
  /**
   * �cluse � laquelle on veut associer ce cr�neau horaire
   */
  private IEcluseFluviale ecluse_;

  /**
   * Constructeur SinaviFilleCreneauHoraire
   * 
   * @param _ecluse l'�cluse � laquelle on veut associ� le cr�neau horaire
   */
  public SinaviFilleCreneauHoraire(final IEcluseFluviale _ecluse) {
    super("D�finition du creneau horaire", false, false, false, false);
    ecluse_ = _ecluse;
    // ajout des listeners aux boutons
    bValider_.addActionListener(this);
    bAnnuler_.addActionListener(this);
    // test si param�tres par d�faut d�j� enregistrer
    if (SinaviPreferences.SINAVIPREFS.getStringProperty("heure_debut_creneau_horaire_ecluse").equals("")) {
      heureDebut_.setDureeField(0);
    } else {
      heureDebut_.setDureeField(SinaviPreferences.SINAVIPREFS.getIntegerProperty("heure_debut_creneau_horaire_ecluse"));
    }
    if (SinaviPreferences.SINAVIPREFS.getStringProperty("heure_fin_creneau_horaire_ecluse").equals("")) {
      heureFin_.setDureeField(0);
    } else {
      heureFin_.setDureeField(SinaviPreferences.SINAVIPREFS.getIntegerProperty("heure_fin_creneau_horaire_ecluse"));
    }
    ((JComponent) getContentPane()).setBorder(new EmptyBorder(25, 25, 25, 25));
    getContentPane().setLayout(new BorderLayout());
    pDonnees_.setLayout(new BuGridLayout(2));
    pDonnees_.add(new BuLabel("HEURE DEBUT"));
    pDonnees_.add(heureDebut_);
    pDonnees_.add(new BuLabel("HEURE FIN"));
    pDonnees_.add(heureFin_);
    pBoutons_.setLayout(new FlowLayout(FlowLayout.RIGHT));
    pBoutons_.add(bAnnuler_);
    pBoutons_.add(bValider_);
    getContentPane().add(pDonnees_, BorderLayout.CENTER);
    getContentPane().add(pBoutons_, BorderLayout.SOUTH);
    pack();
    setVisible(true);
  }

  /**
   * si action sur le bouton valider, lecture des champs de d�but et de fin et sauvegarde du cr�neau horaire si action
   * sur le bouton annuler, fermeture de la fen�tre
   * 
   * @param _e
   */
  public void actionPerformed(final ActionEvent _e) {
    if (_e.getSource() == bValider_) {
      // sauvegarde des valeurs pour en faire les valeurs par d�faut pour le prochain cr�neau horaire
      SinaviPreferences.SINAVIPREFS.putStringProperty("heure_debut_creneau_horaire_ecluse", ""
          + heureDebut_.getDureeField());
      SinaviPreferences.SINAVIPREFS.putStringProperty("heure_fin_creneau_horaire_ecluse", ""
          + heureFin_.getDureeField());
      // System.out.println("ajout"+ecluse_.ajouteCreneauConstant(heureDebut_.getDureeField(),
      // heureFin_.getDureeField()));
      ecluse_.ajouteCreneauConstant(heureDebut_.getDureeField(), heureFin_.getDureeField());
      SinaviImplementation.SINAVITABLECRENEAUMODEL.refresh();
      try {
        setClosed(true);
      } catch (final Exception _pve) {}
    } else {
      try {
        setClosed(true);
      } catch (final Exception _pve) {}
    }
  }
}
