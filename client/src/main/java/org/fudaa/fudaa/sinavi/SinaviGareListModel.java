/*
 * @file         SinaviGareListModel.java
 * @creation     2001-05-17
 * @modification $Date: 2006-09-19 15:09:00 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.sinavi;

import javax.swing.AbstractListModel;

/**
 * D�finit le mod�le pour la liste affichant les gares cr��es sur le r�seau
 * 
 * @version $Revision: 1.6 $ $Date: 2006-09-19 15:09:00 $ by $Author: deniger $
 * @author Aline Marechalle , Franck Lejeune
 */
public class SinaviGareListModel extends AbstractListModel {

  /**
   * retourne la gare s�lectionn�e dans la liste
   * 
   * @param _index
   * @return la gare s�lectionn�e dans la liste
   */
  public Object getElementAt(final int _index) {
    return SinaviImplementation.ETUDE_SINAVI.reseau().gares()[_index];
  }

  /**
   * nombre de gares contenu dans la liste
   * 
   * @return le nombre de gares
   */
  public int getSize() {
    return SinaviImplementation.ETUDE_SINAVI.reseau().gares().length;
  }

  /**
   * permet de rafraichir l'affichage de la liste ( modification ou ajout d'une gare )
   */
  public void refresh() {
    fireContentsChanged(this, 0, getSize() - 1);
  }
}
