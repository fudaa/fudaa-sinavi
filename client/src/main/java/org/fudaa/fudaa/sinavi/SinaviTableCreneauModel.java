/*
 * @file         SinaviTableCreneauModel.java
 * @creation     2001-05-17
 * @modification $Date: 2006-09-19 15:08:59 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.sinavi;

import javax.swing.table.AbstractTableModel;

import org.fudaa.dodico.corba.navigation.IEcluseFluviale;

/**
 * D�finit le mod�le pour la table affichant les cr�neaux horaires
 * 
 * @version $Revision: 1.6 $ $Date: 2006-09-19 15:08:59 $ by $Author: deniger $
 * @author Aline Marechalle , Franck Lejeune
 */
public class SinaviTableCreneauModel extends AbstractTableModel {

  /**
   * noms des colonnes
   */
  private final String[] nomsColonnes_ = { "D�but", "Fin" };
  /**
   * objet m�tier �cluse � laquelle on va faire r�f�rence pour trouver les cr�neaux
   */
  private IEcluseFluviale ecluse_;

  /**
   * Constructeur SinaviTableCreneauModel.
   */
  public SinaviTableCreneauModel() {}

  /**
   * retourne le nombre de colonne
   * 
   * @return le nombre de colonne
   */
  public int getColumnCount() {
    return nomsColonnes_.length;
  }

  /**
   * retourne le nombre de ligne donc le nombre de cr�neau
   * 
   * @return le nombre de cr�neau
   */
  public int getRowCount() {
    if (ecluse_ == null) {
      return 0;
    }
    return ecluse_.creneaux().length;
  }

  /**
   * retourne le nom de la colonne
   * 
   * @param _col le numero de la colonne
   * @return le nom de la colonne
   */
  public String getColumnName(final int _col) {
    return nomsColonnes_[_col];
  }

  /**
   * retourne la valeur situ�e � la ligne row et la colonne col
   * 
   * @param _row la ligne
   * @param _col la colonne
   * @return la valeur situ�e � la ligne row et la colonne col
   */
  public Object getValueAt(final int _row, final int _col) {
    int secondes_;
    final DureeField df_ = new DureeField(false, false, true, true, false);
    if (ecluse_ == null) {
      return null;
    } else if (_col == 0) {
      secondes_ = (int) ecluse_.creneaux()[_row].heureDebut();
    } else {
      secondes_ = (int) ecluse_.creneaux()[_row].heureFin();
    }
    return df_.STRING_DUREE.valueToString(new Integer(secondes_));
  }

  /**
   * permet de rafraichir l'affichage de la table ( modification ou ajout d'un cr�neau )
   */
  public void refresh() {
    fireTableDataChanged();
  }

  /**
   * d�finit l'�cluse � laquelle on va faire r�f�rence pour trouver les cr�neaux
   * 
   * @param _ecluse l'�cluse
   */
  public void setEcluse(final IEcluseFluviale _ecluse) {
    ecluse_ = _ecluse;
    refresh();
  }
}
