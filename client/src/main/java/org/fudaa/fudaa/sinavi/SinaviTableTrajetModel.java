/*
 * @file         SinaviTableTrajetModel.java
 * @creation     2001-05-17
 * @modification $Date: 2006-09-19 15:09:00 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.sinavi;

import javax.swing.table.AbstractTableModel;

import org.fudaa.dodico.corba.navigation.IGare;
import org.fudaa.dodico.corba.navigation.INavireType;

/**
 * D�finit le mod�le pour la table affichant les trajets
 * 
 * @version $Revision: 1.6 $ $Date: 2006-09-19 15:09:00 $ by $Author: deniger $
 * @author Aline Marechalle , Franck Lejeune
 */
public class SinaviTableTrajetModel extends AbstractTableModel {

  /**
   * noms des colonnes
   */
  private final String[] nomsColonnes_ = { "Gare d'arriv�e", "Vitesse" };
  /**
   * objet m�tier gare � laquelle on va faire r�f�rence pour trouver les trajets
   */
  private IGare gare_;
  /**
   * objet m�tier type de navire auquel on va faire r�f�rence pour trouver les trajets
   */
  private INavireType navire_;

  /**
   * Constructeur SinaviTableTrajetModel
   */
  public SinaviTableTrajetModel() {}

  /**
   * retourne le nombre de colonne
   * 
   * @return le nombre de colonne
   */
  public int getColumnCount() {
    return nomsColonnes_.length;
  }

  /**
   * retourne le nombre de ligne donc le nombre de trajet
   * 
   * @return le nombre de trajet
   */
  public int getRowCount() {
    if ((gare_ == null) || (navire_ == null)) {
      return 0;
    }
    return gare_.trajetsParNavire(navire_).length;
  }

  /**
   * retourne le nom de la colonne
   * 
   * @param _col le numero de la colonne
   * @return le nom de la colonne
   */
  public String getColumnName(final int _col) {
    return nomsColonnes_[_col];
  }

  /**
   * retourne la valeur situ�e � la ligne row et la colonne col
   * 
   * @param _row la ligne
   * @param _col la colonne
   * @return la valeur situ�e � la ligne row et la colonne col
   */
  public Object getValueAt(final int _row, final int _col) {
    if ((gare_ == null) || (navire_ == null)) {
      return null;
    } else if (_col == 0) {
      return gare_.trajetsParNavire(navire_)[_row];
    } else {
      return new Double(gare_.trajetsParNavire(navire_)[_row].vitesse());
    }
  }

  /**
   * permet de rafraichir l'affichage de la table ( modification ou ajout d'un trajet )
   */
  public void refresh() {
    fireTableDataChanged();
  }

  /**
   * d�finit la gare � laquelle on va faire r�f�rence pour trouver les trajet
   * 
   * @param _gare la gare
   */
  public void setGare(final IGare _gare) {
    gare_ = _gare;
    refresh();
  }

  /**
   * d�finit le type de navire auquel on va faire r�f�rence pour trouver les trajets
   * 
   * @param _navire le type de navire
   */
  public void setBateau(final INavireType _navire) {
    navire_ = _navire;
    refresh();
  }
}
