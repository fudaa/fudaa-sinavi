/*
 * @file         SinaviBateauListModel.java
 * @creation     2001-05-17
 * @modification $Date: 2006-09-19 15:08:59 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.sinavi;

import javax.swing.AbstractListModel;

/**
 * D�finit le mod�le pour la liste affichant les types de navire
 * 
 * @version $Revision: 1.6 $ $Date: 2006-09-19 15:08:59 $ by $Author: deniger $
 * @author Aline Marechalle , Franck Lejeune
 */
public class SinaviBateauListModel extends AbstractListModel {

  /**
   * retourne le type de navire s�lectionn� dans la liste
   * 
   * @param _index
   * @return le navire s�lectionn� dans la liste
   */
  public Object getElementAt(final int _index) {
    return SinaviImplementation.ETUDE_SINAVI.naviresType()[_index];
  }

  /**
   * nombre de navire contenu dans la liste
   * 
   * @return Le nombre de navire contenu dans la liste
   */
  public int getSize() {
    return SinaviImplementation.ETUDE_SINAVI.naviresType().length;
  }

  /**
   * permet de rafraichir l'affichage de la liste ( modification ou ajout d'un bateau )
   */
  public void refresh() {
    fireContentsChanged(this, 0, getSize() - 1);
  }
}
