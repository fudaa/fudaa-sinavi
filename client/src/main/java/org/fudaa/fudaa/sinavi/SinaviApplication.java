/*
 * @file         SinaviApplication.java
 * @creation     2001-05-17
 * @modification $Date: 2006-09-19 15:08:59 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.sinavi;

import com.memoire.bu.BuApplication;

/**
 * ....
 * 
 * @version $Revision: 1.6 $ $Date: 2006-09-19 15:08:59 $ by $Author: deniger $
 * @author Aline Marechalle , Franck Lejeune
 */
public class SinaviApplication extends BuApplication {

  /**
   * SinaviApplication
   */
  public SinaviApplication() {
    super();
    setImplementation(new SinaviImplementation());
  }
}
