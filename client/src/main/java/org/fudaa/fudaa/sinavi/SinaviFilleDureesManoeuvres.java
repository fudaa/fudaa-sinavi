/*
 * @file         SinaviFilleDureesManoeuvres.java
 * @creation     2001-05-17
 * @modification $Date: 2006-09-19 15:09:00 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.sinavi;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JComponent;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.EmptyBorder;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuInternalFrame;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuPanel;

import org.fudaa.dodico.corba.navigation.IEcluseFluviale;
import org.fudaa.dodico.corba.navigation.INavireTypeHelper;

/**
 * impl�mentation d'une fen�tre interne permetant de d�finir des dur�es de manoeuvres sp�cifiques pour un type de bateau
 * 
 * @version $Revision: 1.8 $ $Date: 2006-09-19 15:09:00 $ by $Author: deniger $
 * @author Aline Marechalle , Franck Lejeune
 */
public class SinaviFilleDureesManoeuvres extends BuInternalFrame implements ActionListener {

  /**
   * champs permettant de renseigner le temps d'entr�e
   */
  private final DureeField TempsEntree_ = new DureeField(false, false, true, true, false);
  /**
   * champs permettant de renseigner le temps de sortie
   */
  private final DureeField TempsSortie_ = new DureeField(false, false, true, true, false);
  /**
   * champs permettant de renseigner le temps de bassin�e montante
   */
  private final DureeField BassineeMontante_ = new DureeField(false, false, true, true, false);
  /**
   * champs permettant de renseigner le temps de bassin�e avalante
   */
  private final DureeField BassineeAvalante_ = new DureeField(false, false, true, true, false);
  /**
   * bouton valider
   */
  private final BuButton bValider_ = new BuButton(SinaviResource.SINAVI.getIcon("VALIDER"), "Valider");
  /**
   * bouton annuler
   */
  private final BuButton bAnnuler_ = new BuButton(SinaviResource.SINAVI.getIcon("ANNULER"), "Annuler");
  /**
   * liste affichant les types de bateau d�finis
   */
  private final JList lMenuTypeBateau_ = new JList();
  /**
   * ascenseur pour la liste des types de bateau d�finis
   */
  private final JScrollPane TypeBateauScrollPane_ = new JScrollPane(lMenuTypeBateau_,
      ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
  /**
   * panel contenant les boutons, il est plac� en bas � droite
   */
  private final BuPanel pBoutons_ = new BuPanel();
  /**
   * panel contenant les diff�rents champs de la fen�tre
   */
  private final BuPanel pDonnees_ = new BuPanel();
  /**
   * �cluse � laquelle on veut attribuer ces dur�es de manoeuvres sp�cifiques
   */
  private IEcluseFluviale ecluse_;

  /**
   * Constructeur SinaviFilleDureesManoeuvres
   * 
   * @param _ecluse �cluse � laquelle on veut attribuer ces dur�es de manoeuvres sp�cifiques
   */
  public SinaviFilleDureesManoeuvres(final IEcluseFluviale _ecluse) {
    super("D�finition des temps de manoeuvres", false, false, false, false);
    ecluse_ = _ecluse;
    // ajout des listeners aux boutons
    bValider_.addActionListener(this);
    bAnnuler_.addActionListener(this);
    // d�finition de la liste
    final Dimension dimtb_ = new Dimension(140, 28);
    TypeBateauScrollPane_.setPreferredSize(dimtb_);
    lMenuTypeBateau_.setModel(SinaviImplementation.SINAVIBATEAULISTMODEL);
    lMenuTypeBateau_.setCellRenderer(new SinaviBateauCellRenderer());
    ((JComponent) getContentPane()).setBorder(new EmptyBorder(25, 25, 25, 25));
    getContentPane().setLayout(new BorderLayout());
    pDonnees_.setLayout(new BuGridLayout(2));
    pDonnees_.add(new BuLabel("Type de bateau :"));
    pDonnees_.add(TypeBateauScrollPane_);
    pDonnees_.add(new BuLabel("Temps d'Entr�e :"));
    pDonnees_.add(TempsEntree_);
    pDonnees_.add(new BuLabel("Temps de Sortie :"));
    pDonnees_.add(TempsSortie_);
    pDonnees_.add(new BuLabel("Bassin�e Montante :"));
    pDonnees_.add(BassineeMontante_);
    pDonnees_.add(new BuLabel("Bassin�e Avalante :"));
    pDonnees_.add(BassineeAvalante_);
    pBoutons_.setLayout(new FlowLayout(FlowLayout.RIGHT));
    pBoutons_.add(bAnnuler_);
    pBoutons_.add(bValider_);
    getContentPane().add(pDonnees_, BorderLayout.CENTER);
    getContentPane().add(pBoutons_, BorderLayout.SOUTH);
    pack();
    setVisible(true);
  }

  /**
   * si action sur le bouton valider, lecture des temps de manoeuvres et sauvegarde de ceux-ci si action sur le bouton
   * annuler, fermeture de la fen�tre
   * 
   * @param _e
   */
  public void actionPerformed(final ActionEvent _e) {
    if (_e.getSource() == bValider_) {
      if (lMenuTypeBateau_.getSelectedValue() != null) {
        ecluse_.ajouteDureesNavire(
            INavireTypeHelper.narrow((org.omg.CORBA.Object) lMenuTypeBateau_.getSelectedValue()), TempsEntree_
                .getDureeField(), TempsSortie_.getDureeField(), BassineeMontante_.getDureeField(), BassineeAvalante_
                .getDureeField());
        SinaviImplementation.SINAVITABLEDUREEESMODEL.refresh();
        try {
          setClosed(true);
        } catch (final Exception _pve) {}
      }
    } else {
      try {
        setClosed(true);
      } catch (final Exception _pve) {}
    }
  }
}
