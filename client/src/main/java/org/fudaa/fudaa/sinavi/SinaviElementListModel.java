/*
 * @file         SinaviElementListModel.java
 * @creation     2001-05-17
 * @modification $Date: 2006-09-19 15:08:59 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.sinavi;

import javax.swing.AbstractListModel;

/**
 * D�finit le mod�le pour la liste affichant les biefs cr��s sur le r�seau
 * 
 * @version $Revision: 1.7 $ $Date: 2006-09-19 15:08:59 $ by $Author: deniger $
 * @author Aline Marechalle , Franck Lejeune
 */
public class SinaviElementListModel extends AbstractListModel {

  /**
   * le type d'ouvrage dont il faut afficher la liste
   */
  private String element_;

  /**
   * retourne le bief s�lectionn� dans la liste
   * 
   * @param _index
   * @return le bief s�lectionn� dans la liste
   */
  public Object getElementAt(final int _index) {
    if (element_ == null) {
      return null;
    }
    if (element_.equals("bief")) {
      return SinaviImplementation.ETUDE_SINAVI.reseau().biefs()[_index];
    } else if (element_.equals("ecluse")) {
      return SinaviImplementation.ETUDE_SINAVI.reseau().ecluses()[_index];
    } else if (element_.equals("gare")) {
      return SinaviImplementation.ETUDE_SINAVI.reseau().gares()[_index];
    } else {
      return SinaviImplementation.ETUDE_SINAVI.naviresType()[_index];
    }
  }

  /**
   * nombre de biefs contenu dans la liste
   * 
   * @return le nombre de biefs
   */
  public int getSize() {
    if (element_ == null) {
      return 0;
    }
    if (element_.equals("bief")) {
      return SinaviImplementation.ETUDE_SINAVI.reseau().biefs().length;
    } else if (element_.equals("ecluse")) {
      return SinaviImplementation.ETUDE_SINAVI.reseau().ecluses().length;
    } else if (element_.equals("gare")) {
      return SinaviImplementation.ETUDE_SINAVI.reseau().gares().length;
    } else {
      return SinaviImplementation.ETUDE_SINAVI.naviresType().length;
    }
  }

  /**
   * permet de rafraichir l'affichage de la liste ( modification ou ajout d'un bief )
   */
  public void refresh() {
    fireContentsChanged(this, 0, getSize() - 1);
  }

  /**
   * d�finit l'ouvrage auquel on va faire r�f�rence pour afficher la liste
   */
  public void setElement(final String _element) {
    element_ = _element;
    refresh();
  }
}
