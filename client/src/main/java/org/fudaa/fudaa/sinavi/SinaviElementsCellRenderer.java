/*
 * @file         SinaviElementsCellRenderer.java
 * @creation     2001-05-17
 * @modification $Date: 2006-09-19 15:09:00 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.sinavi;

import java.awt.Component;

import javax.swing.DefaultListCellRenderer;
import javax.swing.JList;
import javax.swing.ListCellRenderer;

import org.fudaa.dodico.corba.navigation.INavireTypeHelper;
import org.fudaa.dodico.corba.navigation.IOuvrage;
import org.fudaa.dodico.corba.navigation.IOuvrageHelper;

/**
 * impl�mentation du rendu de la liste des �cluses
 * 
 * @version $Revision: 1.7 $ $Date: 2006-09-19 15:09:00 $ by $Author: deniger $
 * @author Aline Marechalle , Franck Lejeune
 */
public class SinaviElementsCellRenderer extends DefaultListCellRenderer implements ListCellRenderer<Object> {

  /**
   * D�finit le rendu de la liste des ouvrages dans la fen�tre des r�sultats
   */
  public SinaviElementsCellRenderer() {}

  /**
   * retourne le nom de l'ouvrage � partir de l'objet Ouvrage ( �cluse, bief, gare )
   * 
   * @param list la liste � laquelle s'appplique le rendu
   * @param value l'objet Ouvrage
   * @param index l'index dans la liste
   * @param isSelected ...
   * @param cellHasFocus ...
   * @return le nom de l'ouvrage
   */
  public Component getListCellRendererComponent(final JList list, final Object value, final int index,
      final boolean isSelected, final boolean cellHasFocus) {
    setBackground(isSelected ? list.getSelectionBackground() : list.getBackground());
    setForeground(list.getForeground());
    if (value == null) {
      setText("");
    } else if (value instanceof IOuvrage) {
      setText(IOuvrageHelper.narrow((org.omg.CORBA.Object) value).nom());
    } else {
      setText(INavireTypeHelper.narrow((org.omg.CORBA.Object) value).type());
    }
    return this;
  }
}
