/*
 * @file         SinaviGareCellRenderer.java
 * @creation     2001-05-17
 * @modification $Date: 2006-09-19 15:08:59 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.sinavi;

import java.awt.Component;

import javax.swing.DefaultListCellRenderer;
import javax.swing.JList;
import javax.swing.ListCellRenderer;

import org.fudaa.dodico.corba.navigation.IGareHelper;

/**
 * impl�mentation du rendu de la liste des gares
 * 
 * @version $Revision: 1.6 $ $Date: 2006-09-19 15:08:59 $ by $Author: deniger $
 * @author Aline Marechalle , Franck Lejeune
 */
public class SinaviGareCellRenderer extends DefaultListCellRenderer implements ListCellRenderer<Object> {

  /**
   * D�finit le rendu de la liste affichant les gares cr��es sur le r�seau
   */
  public SinaviGareCellRenderer() {}

  /**
   * retourne le nom du bief � partir de l'objet Bief
   * 
   * @param list la liste sur laquelle le rendu s'applique
   * @param value l'objet Gare
   * @param index l'index dans la liste
   * @param isSelected ...
   * @param cellHasFocus ...
   * @return retourne le nom de la gare � partir de l'objet Gare
   */
  public Component getListCellRendererComponent(final JList list, final Object value, final int index,
      final boolean isSelected, final boolean cellHasFocus) {
    setBackground(isSelected ? list.getSelectionBackground() : list.getBackground());
    setForeground(list.getForeground());
    setText(IGareHelper.narrow((org.omg.CORBA.Object) value).nom());
    return this;
  }
}
