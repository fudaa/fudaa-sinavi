/*
 * @file         SinaviFilleLoiDeterministeTrajet.java
 * @creation     2001-05-17
 * @modification $Date: 2006-09-19 15:09:00 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.sinavi;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JComponent;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.EmptyBorder;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuInternalFrame;
import com.memoire.bu.BuPanel;
import com.memoire.fu.FuVectorint;

import org.fudaa.dodico.corba.navigation.IGenerationDeterministe;
import org.fudaa.dodico.corba.navigation.ITrajetFluvial;

import org.fudaa.dodico.objet.UsineLib;

/**
 * impl�mentation d'une fen�tre interne permetant de cr�er une loi d�terministe.
 * 
 * @version $Revision: 1.13 $ $Date: 2006-09-19 15:09:00 $ by $Author: deniger $
 * @author Aline Marechalle , Franck Lejeune
 */
public class SinaviFilleLoiDeterministeTrajet extends BuInternalFrame implements ActionListener {

  /**
   * champs pour rentrer le mois, jour, heure, minute d'apparition du navire
   */
  private final DureeField dfDeterministeHeure_ = new DureeField(true, true, true, true, false);
  /**
   * bouton valider
   */
  private final BuButton bValider_ = new BuButton(SinaviResource.SINAVI.getIcon("VALIDER"), "Valider");
  /**
   * bouton annuler
   */
  private final BuButton bAnnuler_ = new BuButton(SinaviResource.SINAVI.getIcon("ANNULER"), "Annuler");
  /**
   * bouton pour cr�er une heure d'apparition
   */
  private final BuButton bCreerHeure_ = new BuButton(SinaviResource.SINAVI.getIcon("VALIDER"), "Enregistrer");
  /**
   * panel contenant les boutons, il est plac� en bas � droite
   */
  private final BuPanel pBoutons_ = new BuPanel();
  /**
   * panel contenant les diff�rents champs de la fen�tre
   */
  private final BuPanel pDonnees_ = new BuPanel();
  /**
   * liste affichant les heures d'apparition entr�es
   */
  private final JList lHeuresCreees_ = new JList();
  /**
   * ascenseur pour la liste des heures d'apparition
   */
  private final JScrollPane HeuresCreeesScrollPane_ = new JScrollPane(lHeuresCreees_,
      ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
  /**
   * trajet asoci� � la loi
   */
  private ITrajetFluvial trajet_;
  /**
   * loi que l'on va cr�er ou afficher
   */
  private IGenerationDeterministe loi_;
  /**
   * vecteur d'entier repr�sentant les heures d'apparition desnavires
   */
  private FuVectorint instants_;

  /**
   * Constructeur SinaviFilleLoiDeterministeTrajet
   * 
   * @param _trajet le trajet associ� � la loi
   * @param _loi la loi � afficher ou null si la loi n'existe pas
   */
  public SinaviFilleLoiDeterministeTrajet(final ITrajetFluvial _trajet, final IGenerationDeterministe _loi) {
    super("D�finition d'une loi de g�n�ration", false, false, false, false);
    trajet_ = _trajet;
    loi_ = _loi;
    // Ajout des listener aux boutons
    bValider_.addActionListener(this);
    bAnnuler_.addActionListener(this);
    bCreerHeure_.addActionListener(this);
    lHeuresCreees_.setModel(SinaviImplementation.SINAVIHEURESDETERMINISTESLISTMODEL);
    HeuresCreeesScrollPane_.setPreferredSize(new Dimension(200, 100));
    // remplissage de la fenetre
    ((JComponent) getContentPane()).setBorder(new EmptyBorder(25, 25, 25, 25));
    getContentPane().setLayout(new BorderLayout());
    pDonnees_.setLayout(new BuGridLayout(2));
    pDonnees_.add(dfDeterministeHeure_);
    pDonnees_.add(bCreerHeure_);
    pBoutons_.setLayout(new FlowLayout(FlowLayout.RIGHT));
    pBoutons_.add(bAnnuler_);
    pBoutons_.add(bValider_);
    getContentPane().add(HeuresCreeesScrollPane_, BorderLayout.NORTH);
    getContentPane().add(pDonnees_, BorderLayout.CENTER);
    getContentPane().add(pBoutons_, BorderLayout.SOUTH);
    if (loi_ != null) {
      // FD:temporaire
      final long[] instantLong = loi_.instants();
      final int[] instantsInt = new int[instantLong.length];
      for (int i = 0; i < instantLong.length; i++) {
        instantsInt[i] = (int) instantLong[i];
      }
      instants_ = new FuVectorint(instantsInt);
      // FD:fin:temporaire
      // instants_ = new FuVectorint(loi_.instants());
    } else {
      instants_ = new FuVectorint();
    }
    SinaviImplementation.SINAVIHEURESDETERMINISTESLISTMODEL.setGenerationDeterministe(instants_);
    SinaviImplementation.SINAVIHEURESDETERMINISTESLISTMODEL.refresh();
    pack();
    setVisible(true);
  }

  /**
   * si action sur le bouton CreerHeure lecture du champs pour rentrer l'instant et sauvegarde si action sur le bouton
   * valider, sauvegarde de la loi d�terministe et fermeture de la fen�tre si action sur le bouton annuler, fermeture de
   * la fen�tre
   * 
   * @param _e
   */
  public void actionPerformed(final ActionEvent _e) {
    if (_e.getSource() == bCreerHeure_) {
      instants_.addElement(dfDeterministeHeure_.getDureeField());
      SinaviImplementation.SINAVIHEURESDETERMINISTESLISTMODEL.refresh();
      dfDeterministeHeure_.setDureeField(0);
    } else if (_e.getSource() == bValider_) {
      // FD:temporaire
      final int[] instantsInt = instants_.toArray();
      final long[] instantsLong = new long[instantsInt.length];
      for (int i = 0; i < instantsInt.length; i++) {
        instantsLong[i] = instantsInt[i];
      }
      // FD:fin:temporaire
      if (loi_ == null) {
        loi_ = UsineLib.findUsine().creeNavigationGenerationDeterministe();
        // loi_.definitInstants(instants_.toArray());
        loi_.definitInstants(instantsLong);
        final String test_ = trajet_.ajouteGeneration(loi_);
        if (test_.equals("")) {
          SinaviImplementation.SINAVITABLELOIMODEL.refresh();
          try {
            setClosed(true);
          } catch (final Exception _pve) {}
        }
      } else {
        loi_.definitInstants(instantsLong);
        // loi_.definitInstants(instants_.toArray());
        SinaviImplementation.SINAVITABLELOIMODEL.refresh();
        try {
          setClosed(true);
        } catch (final Exception _pve) {}
      }
    } else {
      try {
        setClosed(true);
      } catch (final Exception _pve) {}
    }
  }
}
