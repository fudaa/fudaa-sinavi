/*
 * @file         SinaviGestionProjet.java
 * @creation     2001-05-17
 * @modification $Date: 2006-09-19 15:08:59 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.sinavi;

import java.io.*;

import javax.swing.JFileChooser;

import com.memoire.bu.BuAssistant;
import com.memoire.bu.BuCommonImplementation;
import com.memoire.bu.BuDialogMessage;
import com.memoire.bu.BuFileChooser;
import com.memoire.bu.BuTaskOperation;
import com.memoire.dja.DjaGridInteractive;
import com.memoire.dja.DjaLoader;
import com.memoire.dja.DjaSaver;

import org.fudaa.dodico.navigation.DEtudeNavigationFluviale;

/**
 * Gestion du projet.
 * 
 * @version $Revision: 1.9 $ $Date: 2006-09-19 15:08:59 $ by $Author: deniger $
 * @author Aline Marechalle , Franck Lejeune
 */
public class SinaviGestionProjet {

  private static BuCommonImplementation app_;
  private SinaviReseauFrame sinavireseauframe_;
  private File fichier_;

  public SinaviGestionProjet(final BuCommonImplementation _app) {
    app_ = _app;
    fichier_ = null;
  }

  public void setSinaviReseauFrame(final SinaviReseauFrame _srf) {
    sinavireseauframe_ = _srf;
  }

  public SinaviReseauFrame getSinaviReseauFrame() {
    return sinavireseauframe_;
  }

  public String getNomFichier() {
    if (fichier_ == null) {
      return null;
    }
    return fichier_.getAbsolutePath();
  }

  public void enregistre() {
    new BuTaskOperation(app_, "Enregistre") {

      public void act() {
        enregistreSous(false);
      }
    }.start();
  }

  public void enregistreSous() {
    new BuTaskOperation(app_, "Enregistre sous ...") {

      public void act() {
        enregistreSous(true);
      }
    }.start();
  }

  public void ouvrir() {
    new BuTaskOperation(app_, "Ouverture") {

      public void act() {
        ouvrir(null);
      }
    }.start();
  }

  void ouvrir(String _nomFichier) {
    try {
      if (_nomFichier == null) {
        final File fichier = openSinaviFile();
        if (fichier == null) {
          ((SinaviImplementation) app_).glassStop_.setVisible(false);
          return;
        }
        _nomFichier = fichier.getAbsolutePath();
        fichier_ = fichier;
      } else {
        fichier_ = new File(_nomFichier);
      }
      final DataInputStream FluxLu = new DataInputStream(new BufferedInputStream(new FileInputStream(_nomFichier)));
      // initialisation d'ETUDE_SINAVI
      byte[] binaireEtudeSinavi = new byte[FluxLu.readInt()];
      final int nombre_octets = binaireEtudeSinavi.length;
      final int nombre_car = nombre_octets / 50;
      int index = 0;
      for (int i = 0; i < 50; i++) {
        FluxLu.read(binaireEtudeSinavi, index, nombre_car);
        index += nombre_car;
        app_.getMainPanel().setProgression(i);
      }
      FluxLu.read(binaireEtudeSinavi, index, nombre_octets - index);
      app_.getMainPanel().setProgression(50);
      SinaviImplementation.ETUDE_SINAVI = DEtudeNavigationFluviale.creeEtudeNavigationFluviale(binaireEtudeSinavi);
      app_.getMainPanel().setProgression(90);
      binaireEtudeSinavi = null;
      // initialisation de la fen�tre contenant le r�seau fluviale (DJA)
      initDjaFrameFromString(SinaviImplementation.ETUDE_SINAVI.descriptionGraphiqueDja());
      FluxLu.close();
      app_.getMainPanel().setProgression(100);
      app_.getMainPanel().setProgression(0);
    } catch (final IOException ioe) {
      System.err.println("$$$ " + ioe);
    }
    final String test = getNomFichier();
    if (test != null) {
      if (getSinaviReseauFrame() != null) {
        SinaviImplementation.sinavireseauframe_ = getSinaviReseauFrame();
      }
      ((SinaviImplementation) app_).activerCommandesSimulation();
      SinaviImplementation.assistant_.changeAttitude(BuAssistant.ATTENTE, "Etude Ouverte.\nA vous de jouer...");
      // BuDialogMessage dialog_mess = new BuDialogMessage(app_,SinaviImplementation.InfoSoftSinavi_, "Etude ouverte.");
      // dialog_mess.activate();
      app_.setTitle(SinaviImplementation.InfoSoftSinavi_.name + " " + SinaviImplementation.InfoSoftSinavi_.version
          + " " + getNomFichier());
    } else {
      SinaviImplementation.assistant_.changeAttitude(BuAssistant.PAROLE, "Ouverture annul�e");
    }
    ((SinaviImplementation) app_).glassStop_.setVisible(false);
  }

  void enregistreSous(final boolean _demande) {
    try {
      File fichier_intermediaire = null;
      if ((fichier_ == null) || (_demande)) {
        fichier_intermediaire = saveSinaviFile();
        if (fichier_intermediaire == null) {
          ((SinaviImplementation) app_).glassStop_.setVisible(false);
          return;
        }
        fichier_ = fichier_intermediaire;
      }
      final DataOutputStream FluxEcrit = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(fichier_)));
      final String stringDja = djaFrameToString();
      SinaviImplementation.ETUDE_SINAVI.descriptionGraphiqueDja(stringDja);
      byte[] binaireEtudeSinavi = SinaviImplementation.ETUDE_SINAVI.toByteArray();
      final int nombre_octets = binaireEtudeSinavi.length;
      final int nombre_car = nombre_octets / 100;
      int index = 0;
      FluxEcrit.writeInt(binaireEtudeSinavi.length);
      for (int i = 0; i < 100; i++) {
        FluxEcrit.write(binaireEtudeSinavi, index, nombre_car);
        index += nombre_car;
        app_.getMainPanel().setProgression(i);
      }
      FluxEcrit.write(binaireEtudeSinavi, index, nombre_octets - index);
      app_.getMainPanel().setProgression(100);
      app_.getMainPanel().setProgression(0);
      binaireEtudeSinavi = null;
      FluxEcrit.close();
      SinaviImplementation.assistant_.changeAttitude(BuAssistant.ATTENTE, "Enregistrement Termin�");
      ((SinaviImplementation) app_).glassStop_.setVisible(false);
      app_.setTitle(SinaviImplementation.InfoSoftSinavi_.name + " " + SinaviImplementation.InfoSoftSinavi_.version
          + " " + getNomFichier());
    } catch (final IOException ex) {
      ex.printStackTrace();
    }
  }

  private String djaFrameToString() {
    if (sinavireseauframe_ == null) {
      final BuDialogMessage dialog_mess = new BuDialogMessage(app_, SinaviImplementation.informationsSoftware(),
          "Pas de description du r�seau");
      dialog_mess.activate();
      return new String("");
    }
    final ByteArrayOutputStream byte_array_output_stream = new ByteArrayOutputStream();
    // Object[] infosDja = new Object[3];
    try {
      DjaSaver.saveAsText(sinavireseauframe_.getGrid().getObjects(), byte_array_output_stream);
    } catch (final Exception ioe) {
      System.err.println("$$$ " + ioe);
    }
    return byte_array_output_stream.toString();
  }

  private void initDjaFrameFromString(final String _stringDja) {
    if (_stringDja == null) {
      // System.out.println("Erreur de chaine de description Dja");
      return;
    }
    if (_stringDja.equals("")) {
      // System.out.println("Erreur de chaine de description Dja");
      return;
    }
    try {
      final byte[] buffer = _stringDja.getBytes();
      final ByteArrayInputStream byte_array_input_stream = new ByteArrayInputStream(buffer);
      final DjaGridInteractive grille = new DjaGridInteractive(true, DjaLoader.loadAsText(byte_array_input_stream));
      sinavireseauframe_ = new SinaviReseauFrame(app_, "CONNEXION DU RESEAU", grille);
      grille.addMouseListener(new SinaviReseauMouseAdapter(sinavireseauframe_, app_));
      grille.addGridListener(new SinaviReseauGridAdapter());
    } catch (final Exception ex) {
      System.err.println("$$$ " + ex);
      ex.printStackTrace();
    }
  }

  private static File openSinaviFile() {
    final String[] ext = { "sin", "SIN", "sinavi", "SINAVI" };
    File res = null;
    final BuFileChooser chooser = new BuFileChooser();
    chooser.setFileFilter(new javax.swing.filechooser.FileFilter() {

      public boolean accept(final java.io.File f) {
        boolean retour = f.isDirectory();
        final String nom = f.getName().toLowerCase();
        for (int i = 0; i < ext.length; i++) {
          if (nom.endsWith(ext[i])) {
            retour = true;
          }
        }
        return retour;
      }

      public String getDescription() {
        String texte = "";
        for (int i = 0; i < ext.length; i++) {
          texte += " *." + ext[i];
        }
        return texte;
      }
    });
    final int returnVal = chooser.showOpenDialog(app_.getFrame());
    String filename = null;
    if (returnVal == JFileChooser.APPROVE_OPTION) {
      filename = chooser.getSelectedFile().getPath();
    }
    if (filename == null) {
      return null;
    }
    res = new File(filename);
    return res;
  }

  private static File saveSinaviFile() {
    final String[] ext = { "sin", "SIN", "sinavi", "SINAVI" };
    File res = null;
    final BuFileChooser chooser = new BuFileChooser();
    chooser.setFileFilter(new javax.swing.filechooser.FileFilter() {

      public boolean accept(final java.io.File f) {
        boolean retour = f.isDirectory();
        final String nom = f.getName().toLowerCase();
        for (int i = 0; i < ext.length; i++) {
          if (nom.endsWith(ext[i])) {
            retour = true;
          }
        }
        return retour;
      }

      public String getDescription() {
        String texte = "";
        for (int i = 0; i < ext.length; i++) {
          texte += " *." + ext[i];
        }
        return texte;
      }
    });
    final int returnVal = chooser.showSaveDialog(app_.getFrame());
    String filename = null;
    if (returnVal == JFileChooser.APPROVE_OPTION) {
      filename = chooser.getSelectedFile().getPath();
    }
    if (filename == null) {
      return null;
    }
    boolean test = false;
    for (int i = 0; i < ext.length; i++) {
      if (filename.endsWith(ext[i])) {
        test = true;
      }
    }
    if (!test) {
      filename = filename + ".sinavi";
    }
    res = new File(filename);
    return res;
  }

  public static File saveHtmlFile() {
    final String[] ext = { "html", "HTML", "htm", "HTM" };
    File res = null;
    final BuFileChooser chooser = new BuFileChooser();
    chooser.setFileFilter(new javax.swing.filechooser.FileFilter() {

      public boolean accept(final java.io.File f) {
        boolean retour = f.isDirectory();
        final String nom = f.getName().toLowerCase();
        for (int i = 0; i < ext.length; i++) {
          if (nom.endsWith(ext[i])) {
            retour = true;
          }
        }
        return retour;
      }

      public String getDescription() {
        String texte = "";
        for (int i = 0; i < ext.length; i++) {
          texte += " *." + ext[i];
        }
        return texte;
      }
    });
    final int returnVal = chooser.showSaveDialog(app_.getFrame());
    String filename = null;
    if (returnVal == JFileChooser.APPROVE_OPTION) {
      filename = chooser.getSelectedFile().getPath();
    }
    if (filename == null) {
      return null;
    }
    boolean test = false;
    for (int i = 0; i < ext.length; i++) {
      if (filename.endsWith(ext[i])) {
        test = true;
      }
    }
    if (!test) {
      filename = filename + ".html";
    }
    res = new File(filename);
    return res;
  }

  public static File saveCsvFile() {
    final String[] ext = { "csv", "CSV" };
    File res = null;
    final BuFileChooser chooser = new BuFileChooser();
    chooser.setFileFilter(new javax.swing.filechooser.FileFilter() {

      public boolean accept(final java.io.File f) {
        boolean retour = f.isDirectory();
        final String nom = f.getName().toLowerCase();
        for (int i = 0; i < ext.length; i++) {
          if (nom.endsWith(ext[i])) {
            retour = true;
          }
        }
        return retour;
      }

      public String getDescription() {
        String texte = "";
        for (int i = 0; i < ext.length; i++) {
          texte += " *." + ext[i];
        }
        return texte;
      }
    });
    final int returnVal = chooser.showSaveDialog(app_.getFrame());
    String filename = null;
    if (returnVal == JFileChooser.APPROVE_OPTION) {
      filename = chooser.getSelectedFile().getPath();
    }
    if (filename == null) {
      return null;
    }
    boolean test = false;
    for (int i = 0; i < ext.length; i++) {
      if (filename.endsWith(ext[i])) {
        test = true;
      }
    }
    if (!test) {
      filename = filename + ".csv";
    }
    res = new File(filename);
    return res;
  }

  public static File saveGifFile() {
    final String[] ext = { "gif", "GIF" };
    File res = null;
    final BuFileChooser chooser = new BuFileChooser();
    chooser.setFileFilter(new javax.swing.filechooser.FileFilter() {

      public boolean accept(final java.io.File f) {
        boolean retour = f.isDirectory();
        final String nom = f.getName().toLowerCase();
        for (int i = 0; i < ext.length; i++) {
          if (nom.endsWith(ext[i])) {
            retour = true;
          }
        }
        return retour;
      }

      public String getDescription() {
        String texte = "";
        for (int i = 0; i < ext.length; i++) {
          texte += " *." + ext[i];
        }
        return texte;
      }
    });
    final int returnVal = chooser.showSaveDialog(app_.getFrame());
    String filename = null;
    if (returnVal == JFileChooser.APPROVE_OPTION) {
      filename = chooser.getSelectedFile().getPath();
    }
    if (filename == null) {
      return null;
    }
    boolean test = false;
    for (int i = 0; i < ext.length; i++) {
      if (filename.endsWith(ext[i])) {
        test = true;
      }
    }
    if (!test) {
      filename = filename + ".gif";
    }
    res = new File(filename);
    return res;
  }
}
