/*
 * @file         SinaviImplementation.java
 * @creation     2001-05-17
 * @modification $Date: 2007-01-19 13:14:36 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.sinavi;

import java.awt.Container;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.util.List;
import java.util.Map;

import com.memoire.bu.*;
import com.memoire.dja.DjaGridInteractive;
import com.memoire.dja.DjaRegistry;
import com.memoire.fu.FuLib;

import org.fudaa.dodico.corba.navigation.IEtudeNavigationFluviale;
import org.fudaa.dodico.corba.navigation.IServiceSinavi;
import org.fudaa.dodico.corba.navigation.IServiceSinaviHelper;
import org.fudaa.dodico.corba.objet.IConnexion;
import org.fudaa.dodico.corba.objet.IPersonne;

import org.fudaa.dodico.navigation.DServiceSinavi;
import org.fudaa.dodico.objet.UsineLib;

import org.fudaa.fudaa.commun.dodico.FudaaDodicoTacheConnexion;
import org.fudaa.fudaa.commun.dodico.FudaaImplementation;
import org.fudaa.fudaa.ressource.FudaaResource;

/**
 * Implementation de l'application.
 * 
 * @version $Revision: 1.17 $ $Date: 2007-01-19 13:14:36 $ by $Author: deniger $
 * @author Aline Marechalle , Franck Lejeune
 */
public class SinaviImplementation extends FudaaImplementation {

  // public final static String LOCAL_UPDATE = ".";
  public static IConnexion CONNEXION_SINAVI;
  public static IPersonne PERSONNE;
  /**
   * Etude Sinavi initialis�e dans la fonction creer() ou ouvrir().
   */
  public static IEtudeNavigationFluviale ETUDE_SINAVI;
  public static IServiceSinavi SERVICE_SINAVI;
  // public static IServiceSinavi SERVICE_SINAVI = DServiceSinavi.creeServiceSinavi(USINE_SINAVI);
  /**
   * mod�le de la liste des biefs.
   */
  public final static SinaviBiefListModel SINAVIBIEFLISTMODEL = new SinaviBiefListModel();
  /**
   * mod�le de la liste des types de bateau.
   */
  public final static SinaviBateauListModel SINAVIBATEAULISTMODEL = new SinaviBateauListModel();
  /**
   * mod�le de la liste des ouvrages.
   */
  public final static SinaviElementListModel SINAVIELEMENTLISTMODEL = new SinaviElementListModel();
  /**
   * mod�le de la liste des �cluses.
   */
  public final static SinaviEcluseListModel SINAVIECLUSELISTMODEL = new SinaviEcluseListModel();
  /**
   * mod�le de la liste des gares.
   */
  public final static SinaviGareListModel SINAVIGARELISTMODEL = new SinaviGareListModel();
  /**
   * mod�le de la table des cr�neaux horaires.
   */
  public static SinaviTableCreneauModel SINAVITABLECRENEAUMODEL = new SinaviTableCreneauModel();
  /**
   * mod�le de la table des dur�es de manoeuvres.
   */
  public static SinaviTableDureeESModel SINAVITABLEDUREEESMODEL = new SinaviTableDureeESModel();
  /**
   * mod�le de la table des trajets.
   */
  public static SinaviTableTrajetModel SINAVITABLETRAJETMODEL = new SinaviTableTrajetModel();
  /**
   * mod�le de la table des lois.
   */
  public static SinaviTableLoiModel SINAVITABLELOIMODEL = new SinaviTableLoiModel();
  /**
   * mod�le de la liste des bateaux partant.
   */
  public static SinaviBateauPartantListModel SINAVIBATEAUPARTANTLISTMODEL = new SinaviBateauPartantListModel();
  /**
   * mod�le de la liste des instants d'une loi d�terministe.
   */
  public static SinaviHeuresDeterministesListModel SINAVIHEURESDETERMINISTESLISTMODEL = new SinaviHeuresDeterministesListModel();
  protected static BuInformationsSoftware InfoSoftSinavi_ = new BuInformationsSoftware();
  protected static BuInformationsDocument InfoDocsSinavi_ = new BuInformationsDocument();
  /**
   * fen�tre interne affichant le r�seau.
   */
  public static SinaviReseauFrame sinavireseauframe_;
  /**
   * class permettant la gestion des fichiers de sauvegardes.
   */
  public static SinaviGestionProjet sinavigestionprojet_;
  /**
   * fen�tre interne affichant le classeur � onglet.
   */
  protected SinaviFilleParametres sinavifilleparametres_;
  /**
   * fen�tre interne permettant de choisir les donn�es que l'on veut inclure dans le rappel de donn�es.
   */
  protected SinaviFilleChoixRappelDonnees sinavifillechoixrappeldonnees_;
  /**
   * fen�tre interne permettant de choisir les r�sultats que l'on veut exploiter.
   */
  protected SinaviFilleChoixResultats sinavifillechoixresultats_;
  /**
   * fen�tre interne affichant les r�sultats que l'on veut exploiter.
   */
  public static SinaviFilleResultats sinavifilleresultats_;
  /**
   * fen�tre interne permettant de choisir le type d'animation lors du calcul.
   */
  protected SinaviFilleChoixCalcul sinavifillechoixcalcul_;
  /**
   * assistant du bureau.
   */
  protected static BuAssistant assistant_;
  /**
   * fen�tre affichant les pr�f�rences de Sinavi.
   */
  /**
   * fen�tre affichant l'aide.
   */
  protected BuHelpFrame aide_;
  /**
   * visualiseur des taches en cours.
   */
  protected BuTaskView taches_;
  /**
   * composant permettant de stopper l'interactivite de Sinavi.
   */
  protected BuGlassPaneStop glassStop_;
  static {
    InfoSoftSinavi_.name = "Sinavi";
    InfoSoftSinavi_.version = "0.01";
    InfoSoftSinavi_.date = "15-fevrier-2001";
    InfoSoftSinavi_.rights = "Tous droits r�serv�s. CETMEF (c)2001";
    InfoSoftSinavi_.contact = "alain.pourplanche@cetmef.equipement.gouv.fr";
    InfoSoftSinavi_.license = "GPL2";
    InfoSoftSinavi_.languages = "fr,en";
    InfoSoftSinavi_.http = "http://marina.cetmef.equipement.gouv.fr/fudaa/";
    InfoSoftSinavi_.update = "http://marina.cetmef.equipement.gouv.fr/fudaa/deltas/";
    InfoSoftSinavi_.man = "http://marina.cetmef.equipement.gouv.fr/fudaa/manuels/sipor/";
    InfoSoftSinavi_.logo = SinaviResource.SINAVI.getIcon("sinaviimagelogo");
    InfoSoftSinavi_.banner = SinaviResource.SINAVI.getIcon("sinaviimagebanner");
    InfoSoftSinavi_.authors = new String[] { "Aline MARECHALLE & Franck LEJEUNE" };
    InfoSoftSinavi_.contributors = new String[] { "Equipes Dodico, Ebli et Fudaa" };
    InfoSoftSinavi_.documentors = new String[] {};
    InfoSoftSinavi_.testers = new String[] { "Alain Pourplanche", "Laurent Luchez" };
    InfoDocsSinavi_.name = "Etude";
    InfoDocsSinavi_.version = "0.01";
    InfoDocsSinavi_.organization = "CETMEF";
    InfoDocsSinavi_.author = System.getProperty("user.name");
    InfoDocsSinavi_.contact = InfoDocsSinavi_.author + "@equipement.gouv.fr";
    InfoDocsSinavi_.date = FuLib.date();
    BuPrinter.INFO_LOG = InfoSoftSinavi_;
    BuPrinter.INFO_DOC = InfoDocsSinavi_;
  }

  /**
   * Informations concernant l'application.
   */
  public static BuInformationsSoftware informationsSoftware() {
    return InfoSoftSinavi_;
  }

  public BuInformationsSoftware getInformationsSoftware() {
    return InfoSoftSinavi_;
  }

  public BuInformationsDocument getInformationsDocument() {
    return InfoDocsSinavi_;
  }

  /**
   * methode init obligatoire ( initialise les objets java avant l'affichage du bureau ).
   */
  public void init() {
    super.init();
    sinavifilleparametres_ = null;
    aide_ = null;
    sinavireseauframe_ = null;
    sinavigestionprojet_ = null;
    sinavifillechoixrappeldonnees_ = null;
    sinavifillechoixresultats_ = null;
    // enregistrement des trois classes utilise dans la grille dja pour la sauvegarde �ventuelle avec Dja
    DjaRegistry.register("gare", SinaviReseauGare.class);
    DjaRegistry.register("ecluse", SinaviReseauEcluse.class);
    DjaRegistry.register("bief", SinaviReseauBief.class);
    try {
      setTitle(InfoSoftSinavi_.name + " " + InfoSoftSinavi_.version);
      final BuMenuBar mb = getMainMenuBar();
      // mb.addActionListener(this);
      mb.addMenu(construitMenuSimulation());
      mb.addMenu(construitMenuProjets());
      final BuToolBar tb = getMainToolBar();
      tb.addSeparator();
      tb.addToolButton("Calculer", "CALCULER", false);
      tb.addToolButton("Connecter", "CONNECTER", FudaaResource.FUDAA.getIcon("connecter"), true);
      setEnabledForAction("CREER", true);
      setEnabledForAction("OUVRIR", true);
      setEnabledForAction("ENREGISTRER", false);
      setEnabledForAction("ENREGISTRERSOUS", false);
      setEnabledForAction("FERMER", false);
      setEnabledForAction("QUITTER", true);
      setEnabledForAction("IMPORTER", false);
      setEnabledForAction("EXPORTER", false);
      setEnabledForAction("IMPRIMER", false);
      setEnabledForAction("PREFERENCE", true);
      setEnabledForAction("AIDE_INDEX", false);
      setEnabledForAction("AIDE_ASSISTANT", false);
      setEnabledForAction("TABLEAU", false);
      setEnabledForAction("GRAPHE", false);
      assistant_ = new BuAssistant();
      taches_ = new BuTaskView();
      glassStop_ = new BuGlassPaneStop();
      glassStop_.setVisible(false);
      getRootPane().setGlassPane(glassStop_);
      final BuScrollPane sp1_ = new BuScrollPane(taches_);
      sp1_.setPreferredSize(new Dimension(150, 80));
      getMainPanel().getRightColumn().addToggledComponent("Assistant", "ASSISTANT", assistant_, this);
      getMainPanel().getRightColumn().addToggledComponent(BuResource.BU.getString("T�ches"), "TACHE", sp1_, this);
      getMainPanel().setTaskView(taches_);
    } catch (final Throwable t) {
      System.err.println("$$$ " + t);
    }
  }

  public void start() {
    super.start();
    assistant_.changeAttitude(BuAssistant.PAROLE, "Bienvenue !\n" + InfoSoftSinavi_.name + " "
        + InfoSoftSinavi_.version);
    final BuMainPanel mp_ = getMainPanel();
    mp_.doLayout();
    mp_.validate();
    assistant_.addEmitters((Container) getApp());
    assistant_.changeAttitude(BuAssistant.ATTENTE, "Vous pouvez cr�er une\nnouvelle simulation\nou en ouvrir une");
    // Application des pr�f�rences
    BuPreferences.BU.applyOn(this);
    // SinaviPreferences.SINAVI.applyOn(this);
  }

  /**
   * gestion des menus du bureau.
   * 
   * @param _evt
   */
  public void actionPerformed(final ActionEvent _evt) {
    final String action = _evt.getActionCommand();
    System.err.println("ACTION=" + action);
    if (action.equals("CREER")) {
      creer();
    } else if (action.equals("OUVRIR")) {
      ouvrir();
    } else if (action.equals("ENREGISTRER")) {
      enregistrer();
    } else if (action.equals("ENREGISTRERSOUS")) {
      enregistrerSous();
    } else if (action.equals("TEXTE")) {
      rappelDonnees();
    } else if (action.equals("FERMER")) {
      fermer();
    } else if (action.equals("QUITTER")) {
      quitter();
    } else if (action.equals("PARAMETRE")) {
      parametre();
    } else if (action.equals("CALCULER")) {
      calculer();
    } else if (action.equals("PREFERENCE")) {
      preferences();
    } else if (action.equals("GRAPHE")) {
      resultats();
    } else if (action.equals("TABLEAU")) {
      resultatsSimulations();
    } else if (action.equals("VOIR_RESEAU")) {
      voir_reseau();
    } else if (action.equals("GENERER")) {
      genererNavires();
    } else {
      super.actionPerformed(_evt);
    }
  }

  /**
   * ....
   */
  public void oprCalculer() {
    final SinaviAnimation anim = new SinaviAnimation();
    anim.lancementCalcul();
  }

  /**
   * Cr�� le menu simulation.
   * 
   * @param _app
   * @return le menu simulation
   */
  protected BuMenu construitMenuSimulation() {
    final BuMenu menu1_ = new BuMenu("Simulation", "SIMULATION");
    menu1_.addMenuItem("Param�tres", "PARAMETRE", false);
    menu1_.addMenuItem("R�seau", "VOIR_RESEAU", false);
    menu1_.addMenuItem("G�n�rer Navires", "GENERER", false);
    menu1_.addMenuItem("Calculer", "CALCULER", false);
    menu1_.addMenuItem("Rappel des donn�es", "TEXTE", false);
    menu1_.addMenuItem("Resultats", "GRAPHE", false);
    menu1_.addMenuItem("Rapport", "TABLEAU", false);
    return menu1_;
  }

  protected BuMenu construitMenuProjets() {
    final BuMenu menu2_ = new BuMenu("Projet", "PROJET");
    menu2_.addMenuItem("Enregistrer", "ENREGISTRER_LISTE", false);
    menu2_.addMenuItem("Ouvrir", "OUVRIR_LISTE", true);
    return menu2_;
  }

  /**
   * Commandes activ�es d�s qu'une simulation est charg�e.
   */
  protected void activerCommandesSimulation() {
    setEnabledForAction("PARAMETRE", true);
    setEnabledForAction("TEXTE", true);
    setEnabledForAction("ENREGISTRER", true);
    setEnabledForAction("ENREGISTRERSOUS", true);
    setEnabledForAction("FERMER", true);
    setEnabledForAction("CALCULER", true);
    setEnabledForAction("ENREGISTRER_LISTE", true);
    setEnabledForAction("VOIR_RESEAU", true);
    setEnabledForAction("GENERER", true);
    // temporaire pour les tests de la fenetre...
    setEnabledForAction("GRAPHE", true);
  }

  /**
   * cr�er une nouvelle simulation.
   */
  protected void creer() {
    assistant_.changeAttitude(BuAssistant.PAROLE, "Cr�ation d'une nouvelle\n �tude");
    // Cr�ation d'une nouvelle usine et d'un nouveau reseau
    ETUDE_SINAVI = UsineLib.findUsine().creeNavigationEtudeNavigationFluviale();
    ETUDE_SINAVI.reseau(UsineLib.findUsine().creeNavigationReseauFluvial());
    if (ETUDE_SINAVI == null) {
      System.err.println("Etude nulle!!!!!");
    }
    if (ETUDE_SINAVI.reseau() == null) {
      System.err.println("Reseau nul!!!!!");
    }
    // Cr�ation de la fenetre de saisie de reseau
    final DjaGridInteractive grille_ = new DjaGridInteractive();
    grille_.setInteractive(true);
    sinavireseauframe_ = new SinaviReseauFrame(this, "CONNEXION_SINAVI DU RESEAU", grille_);
    sinavireseauframe_.setSize(750, 470);
    grille_.addMouseListener(new SinaviReseauMouseAdapter(sinavireseauframe_, this));
    grille_.addGridListener(new SinaviReseauGridAdapter());
    sinavigestionprojet_ = new SinaviGestionProjet(this);
    sinavigestionprojet_.setSinaviReseauFrame(sinavireseauframe_);
    final BuDialogMessage dialog_mess = new BuDialogMessage(
        getApp(),
        InfoSoftSinavi_,
        "Vous pouvez repr�senter le r�seau et d�finir les \ncaract�ristiques des diff�rents �l�ments � partir du menu\n \"Simulation\"");
    dialog_mess.activate();
    // acces aux differentes commande du menu Simulation
    activerCommandesSimulation();
    assistant_.changeAttitude(BuAssistant.ATTENTE, "Etude Cr��e.\nA vous de jouer...");
    setTitle(InfoSoftSinavi_.name + " " + InfoSoftSinavi_.version + " Noname.sinavi");
  }

  /**
   * ouvrir une simulation existante.
   */
  protected void ouvrir() {
    assistant_.changeAttitude(BuAssistant.PAROLE, "Ouverture d'une �tude\nexistante");
    sinavigestionprojet_ = new SinaviGestionProjet(this);
    glassStop_.setVisible(true);
    sinavigestionprojet_.ouvrir();
  }

  /**
   * enregistrer la simulation ouverte.
   */
  protected void enregistrer() {
    assistant_.changeAttitude(BuAssistant.PAROLE, "Enregistrement de l'�tude\nen cours...");
    glassStop_.setVisible(true);
    sinavigestionprojet_.enregistre();
  }

  /**
   * enregistrer une simulation.
   */
  protected void enregistrerSous() {
    assistant_.changeAttitude(BuAssistant.PAROLE, "Enregistrement de l'�tude\nsous un nouveau nom...");
    glassStop_.setVisible(true);
    sinavigestionprojet_.enregistreSous();
  }

  /**
   * fermer une simulation.
   */
  protected void fermer() {}

  /**
   * quitter Sinavi.
   */
  protected void quitter() {
    exit();
  }

  /**
   * importer un projet.
   */
  protected void importerProjet() {}

  /**
   * Cr�ation ou affichage de la fenetre pour les pr�f�rences.
   */
  protected void buildPreferences(final List _prefs) {
    _prefs.add(new BuBrowserPreferencesPanel(this));
    _prefs.add(new BuDesktopPreferencesPanel(this));
    _prefs.add(new BuLanguagePreferencesPanel(this));
  }

  /**
   * Cr�ation ou affichage d'une fenetre pour saisir les param�tres.
   */
  protected void parametre() {
    if (sinavifilleparametres_ == null) {
      // Cr�ation du classeur � onglets de saisie
      sinavifilleparametres_ = new SinaviFilleParametres(getApp());
      addInternalFrame(sinavifilleparametres_);
    } else {
      if (sinavifilleparametres_.isClosed()) {
        addInternalFrame(sinavifilleparametres_);
      } else {
        activateInternalFrame(sinavifilleparametres_);
      }
    }
    sinavifilleparametres_.ongletCourant_ = sinavifilleparametres_.tpMain_.getSelectedIndex();
  }

  /**
   * Cr�ation et affichage du rappel des donn�es.
   */
  protected void rappelDonnees() {
    if (sinavifillechoixrappeldonnees_ == null) {
      sinavifillechoixrappeldonnees_ = new SinaviFilleChoixRappelDonnees(this);
      addInternalFrame(sinavifillechoixrappeldonnees_);
    } else {
      if (sinavifillechoixrappeldonnees_.isClosed()) {
        addInternalFrame(sinavifillechoixrappeldonnees_);
      } else {
        activateInternalFrame(sinavifillechoixrappeldonnees_);
      }
    }
  }

  /**
   * methode pour g�n�rer les navires dans chaque loi.
   */
  protected void genererNavires() {
    assistant_.changeAttitude(BuAssistant.PAROLE, "G�n�ration des navires...");
    final String test = ETUDE_SINAVI.genere();
    if (test.equals("")) {
      final BuDialogMessage dialog_mess = new BuDialogMessage(getApp(), InfoSoftSinavi_, "Navires G�n�r�s");
      dialog_mess.activate();
      assistant_.changeAttitude(BuAssistant.ATTENTE, "G�n�ration des navires\ntermin�e.");
    } else {
      final BuDialogMessage dialog_mess = new BuDialogMessage(getApp(), InfoSoftSinavi_, test);
      dialog_mess.activate();
      assistant_.changeAttitude(BuAssistant.PEUR, "Erreur de g�n�ration.");
    }
  }

  /**
   * methode pour lancer le code de calcul de Sinavi.
   */
  protected void calculer() {
    if (!isConnected()) {
      new BuDialogError(getApp(), InfoSoftSinavi_, "vous n'etes pas connect� � un serveur!").activate();
      return;
    }
    /*
     * SinaviOutilsDonnees sod_ = new SinaviOutilsDonnees(ETUDE_SINAVI); sod_.ConstruitTableauNaviresGeneres();
     * sod_.ConstruitTableauBiefsCrees(); sod_.ConstruitTableauEclusesCreees(); sod_.ConstruitTableauGaresCreees();
     * sod_.ConstruitTableauNaviresType(); sod_.ConstruitTableauCreneauxHorairesEcluses();
     * sod_.ConstruitTableauDureesManoeuvresEcluses();
     */
    // System.out.println("creation BuTaskOperation ds implementation");
    // new BuTaskOperation(this, "Calcul", "oprCalculer").start();
    // System.out.println("Apres appel BuTaskOperation ds implementation");
    if (sinavifillechoixcalcul_ == null) {
      sinavifillechoixcalcul_ = new SinaviFilleChoixCalcul(this);
      addInternalFrame(sinavifillechoixcalcul_);
    } else {
      if (sinavifillechoixcalcul_.isClosed()) {
        addInternalFrame(sinavifillechoixcalcul_);
      } else {
        activateInternalFrame(sinavifillechoixcalcul_);
      }
    }
  }

  /**
   * Cr�ation ou affichage d'une fenetre pour d�finir l'exploitation des r�sultats d'une simulation.
   */
  protected void resultats() {
    if (sinavifillechoixresultats_ == null) {
      sinavifillechoixresultats_ = new SinaviFilleChoixResultats(this);
      addInternalFrame(sinavifillechoixresultats_);
    } else {
      if (sinavifillechoixresultats_.isClosed()) {
        addInternalFrame(sinavifillechoixresultats_);
      } else {
        activateInternalFrame(sinavifillechoixresultats_);
      }
    }
  }

  /**
   * Cr�ation ou affichage d'une fenetre pour l'exploitation des r�sultats des simulations ouvertes.
   */
  protected void resultatsSimulations() {
  // cr�ation du rapport
  }

  /**
   * methode qui rend visible la fen�tre de saisie du r�seau.
   */
  protected void voir_reseau() {
    if (!(sinavireseauframe_.isVisible())) {
      addInternalFrame(sinavireseauframe_);
    } else {
      activateInternalFrame(sinavireseauframe_);
    }
  }

  public void exit() {
    fermer();
    super.exit();
  }

  public boolean confirmExit() {
    return true;
  }

  public boolean isCloseFrameMode() {
    return false;
  }

  protected void clearVariables() {
    CONNEXION_SINAVI = null;
    SERVICE_SINAVI = null;
  }

  /**
   * @see org.fudaa.fudaa.commun.dodico.FudaaImplementation#getTacheConnexionMap()
   */
  protected FudaaDodicoTacheConnexion[] getTacheConnexionMap() {
    final FudaaDodicoTacheConnexion c = new FudaaDodicoTacheConnexion(SERVICE_SINAVI, CONNEXION_SINAVI);
    return new FudaaDodicoTacheConnexion[] { c };
  }

  /**
   * @see org.fudaa.fudaa.commun.dodico.FudaaImplementation#getTacheDelegateClass()
   */
  protected Class[] getTacheDelegateClass() {
    return new Class[] { DServiceSinavi.class };
  }

  /**
   * @see org.fudaa.fudaa.commun.dodico.FudaaImplementation#initConnexions(java.util.Map)
   */
  protected void initConnexions(final Map _r) {
    final FudaaDodicoTacheConnexion c = (FudaaDodicoTacheConnexion) _r.get(DServiceSinavi.class);
    CONNEXION_SINAVI = c.getConnexion();
    SERVICE_SINAVI = IServiceSinaviHelper.narrow(c.getTache());
  }

  /**
   * @see org.fudaa.fudaa.commun.impl.FudaaCommonImplementation#getApplicationPreferences()
   */
  public BuPreferences getApplicationPreferences() {
    return SinaviPreferences.SINAVIPREFS;
  }
}
