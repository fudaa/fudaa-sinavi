/*
 * @file         SinaviReseauMouseAdapter.java
 * @creation     2001-05-17
 * @modification $Date: 2006-09-19 15:09:00 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.sinavi;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import com.memoire.bu.BuCommonImplementation;
import com.memoire.dja.DjaGridInteractive;

/**
 * impl�mentation d'un MouseListener pour le r�seau de Sinavi.
 * 
 * @version $Revision: 1.6 $ $Date: 2006-09-19 15:09:00 $ by $Author: deniger $
 * @author Aline Marechalle , Franck Lejeune
 */
public class SinaviReseauMouseAdapter extends MouseAdapter {

  /**
   * la grille du r�seau
   */
  private DjaGridInteractive grille_;
  /**
   * la fen�tre interne repr�sentant le r�seau
   */
  private SinaviReseauFrame fenetre_;
  /**
   * une instance de SinaviImpl�mentation
   */
  private BuCommonImplementation app_;

  /**
   * Constructeur SinaviReseauMouseAdapter
   * 
   * @param _fenetre la fen�tre interne repr�sentant le r�seau
   * @param _app une instance de SinaviImplementation
   */
  public SinaviReseauMouseAdapter(final SinaviReseauFrame _fenetre, final BuCommonImplementation _app) {
    fenetre_ = _fenetre;
    app_ = _app;
    grille_ = (DjaGridInteractive) fenetre_.getGrid();
  }

  /**
   * si clic sur un bief, affichage d'une fen�tre interne pour rentrer les param�tres du bief si clic sur une �cluse,
   * affichage d'une fen�tre interne pour rentrer les param�tres de l'�cluse si clic sur une gare, affichage d'une
   * fen�tre interne pour rentrer les param�tres de la gare
   * 
   * @param _e
   */
  public void mouseClicked(final MouseEvent _e) {
    if (grille_.isInteractive()) {
      return;
    }
    final java.util.Enumeration enum_ = grille_.getObjects().elements();
    while (enum_.hasMoreElements()) {
      final Object o_ = enum_.nextElement();
      if (o_ instanceof SinaviReseauBief) {
        final SinaviReseauBief srb_ = (SinaviReseauBief) o_;
        if (srb_.contains(_e.getX(), _e.getY())) {
          app_.addInternalFrame(new SinaviFilleReseauBief(app_, srb_.getBief()));
          return;
        }
      } else if (o_ instanceof SinaviReseauEcluse) {
        final SinaviReseauEcluse sre_ = (SinaviReseauEcluse) o_;
        if (sre_.contains(_e.getX(), _e.getY())) {
          app_.addInternalFrame(new SinaviFilleReseauEcluse(app_, sre_.getEcluse()));
          return;
        }
      } else if (o_ instanceof SinaviReseauGare) {
        final SinaviReseauGare srg_ = (SinaviReseauGare) o_;
        if (srg_.contains(_e.getX(), _e.getY())) {
          app_.addInternalFrame(new SinaviFilleReseauGare(app_, srg_.getGare()));
          return;
        }
      }
    }
  } // fin while
}
