/*
 * @file         SinaviFilleResultats.java
 * @creation     2001-05-17
 * @modification $Date: 2006-09-19 15:09:00 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.sinavi;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.swing.ButtonGroup;
import javax.swing.JComponent;
import javax.swing.border.EmptyBorder;

import com.memoire.bu.BuAssistant;
import com.memoire.bu.BuButton;
import com.memoire.bu.BuComboBox;
import com.memoire.bu.BuCommonImplementation;
import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuInternalFrame;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuRadioButton;

/**
 * impl�mentation d'une fen�tre interne affichant un calendrier pour le choix de date
 * 
 * @version $Revision: 1.6 $ $Date: 2006-09-19 15:09:00 $ by $Author: deniger $
 * @author Aline Marechalle , Franck Lejeune
 */
public class SinaviFilleResultats extends BuInternalFrame implements ActionListener {

  /**
   * composant Sinavi Graphique contenant le graphe et les valeurs caract�ristiques des courbes
   */
  private final SinaviGraphiqueResultat sgr_ = new SinaviGraphiqueResultat();
  /**
   * bouton annuler
   */
  private final BuButton bAnnuler_ = new BuButton(SinaviResource.SINAVI.getIcon("ANNULER"), "Annuler");
  /**
   * bouton retour
   */
  private final BuButton bSauver_ = new BuButton(SinaviResource.SINAVI.getIcon("ENREGISTRER"), "Enregistrer");
  /**
   * panel contenant les boutons, il est plac� en bas � droite
   */
  private final BuPanel pBoutons_ = new BuPanel();
  /**
   * panel contenant les diff�rents champs de la fen�tre
   */
  private final BuPanel pDonnees_ = new BuPanel();
  /**
   * panel contenant le menu deroulant proposant les differents elements
   */
  private final BuPanel pMenu_ = new BuPanel();
  /**
   * panel les deux panels : pBoutons et pMenu
   */
  private final BuPanel pSud_ = new BuPanel();
  /**
   * groupe de bouton contenant les types de sauvegarde
   */
  private final ButtonGroup bgTypeSauvegarde_ = new ButtonGroup();
  /**
   * menu deroulant proposant les differents elements
   */
  private BuComboBox cbElements_;
  /**
   * radio bouton pour sortie fichier texte
   */
  private final BuRadioButton rbTexte_ = new BuRadioButton("Fichier Texte");
  /**
   * radio bouton pour sortie fichier HTML
   */
  private final BuRadioButton rbHtml_ = new BuRadioButton("Fichier HTML");
  /**
   * radio bouton pour sortie image GIF
   */
  private final BuRadioButton rbImage_ = new BuRadioButton("Image GIF");
  /**
   * une instance de SinaviImplementation
   */
  private BuCommonImplementation app_;
  /**
   * le tableau contenant les elements sur lesquels portent les resultats
   */
  private Object[] elements_;
  /**
   * chaine de caractere representant le type de resultat demande
   */
  private String typeResultat_;

  /**
   * booleen representant le choix d'un graphique pour toutes les courbes
   */
  // private boolean unGraphique_;
  /**
   * Constructeur
   */
  public SinaviFilleResultats(final BuCommonImplementation _app, final Object[] _elements, final boolean _unGraphique,
      final String _typeResultat) {
    super("R�sultat", true, true, true, true);
    app_ = _app;
    elements_ = _elements;
    typeResultat_ = _typeResultat;
    // unGraphique_= _unGraphique;
    setSize(710, 572);
    cbElements_ = new BuComboBox(elements_);
    cbElements_.setRenderer(new SinaviElementsCellRenderer());
    cbElements_.setPreferredSize(new Dimension(250, 28));
    if (_unGraphique) {
      cbElements_.setEnabled(false);
    }
    // ajout des listeners aux boutons
    bAnnuler_.addActionListener(this);
    bSauver_.addActionListener(this);
    // Cr�ation du layout manager et du bord de la fenetre
    ((JComponent) getContentPane()).setBorder(new EmptyBorder(25, 25, 25, 25));
    getContentPane().setLayout(new BorderLayout());
    // boutons dans le groupe
    bgTypeSauvegarde_.add(rbTexte_);
    bgTypeSauvegarde_.add(rbHtml_);
    bgTypeSauvegarde_.add(rbImage_);
    // bouton HTML s�lectionne par defaut
    rbHtml_.setSelected(true);
    // layout des panels
    pBoutons_.setLayout(new FlowLayout(FlowLayout.RIGHT));
    pMenu_.setLayout(new FlowLayout(FlowLayout.LEFT));
    pDonnees_.setLayout(new BuGridLayout(3));
    pSud_.setLayout(new BorderLayout());
    // ajout du menu deroulant au panel pMenu
    pMenu_.add(cbElements_);
    // ajout des donnees au panel sud
    pSud_.add(pMenu_, BorderLayout.WEST);
    pSud_.add(pBoutons_, BorderLayout.EAST);
    // ajout des donnees au panel donnees
    pDonnees_.add(rbTexte_);
    pDonnees_.add(rbHtml_);
    pDonnees_.add(rbImage_);
    // ajout des boutons au panel bouton
    pBoutons_.add(bSauver_);
    pBoutons_.add(bAnnuler_);
    getContentPane().add(pDonnees_, BorderLayout.NORTH);
    getContentPane().add(sgr_, BorderLayout.CENTER);
    getContentPane().add(pSud_, BorderLayout.SOUTH);
    sgr_.setLibeleAxeX("min");
    sgr_.setLibeleAxeY("bat");
    final double[] valX = new double[10];
    final double[] valY = new double[10];
    for (int i = 0; i < 10; i++) {
      valX[i] = i;
      valY[i] = 2 + i;
    }
    sgr_.ajouteHistogramme("�cluse 3", Color.red, Color.red, valX, valY, 10);
  }

  private void ecritFichierHTML(final StringBuffer _chaine, final File _fichierHTML) {
    // a completer ...
    final File fich = new File(_fichierHTML.getPath().substring(0, _fichierHTML.getPath().lastIndexOf(".")) + ".gif");
    System.out.println("������������" + fich.toString());
    sgr_.ecritImageGIF(fich);
    _chaine.append("<HTML><HEAD><TITLE></TITLE></HEAD><BODY>");
    _chaine.append("<P><H1 align=center>" + cbElements_.getSelectedItem() + "</H1></P>");
    _chaine.append("<P><H2 align=center>" + typeResultat_ + "</H2></P>");
    _chaine.append("<IMG ALIGN=CENTER SRC=" + fich.toString() + " BORDER=1></A>");
    _chaine.append("</BODY></HTML>");
  }

  private void ecritFichierCSV(final StringBuffer _chaine) {
    // a completer ...
    _chaine.append("essai de fichier texte");
  }

  /**
   * gestion du bouton Sauver ( trois types de sauvegarde possibles) et du bouton Annuler
   * 
   * @param _e
   */
  public void actionPerformed(final ActionEvent _e) {
    if (_e.getSource() == bSauver_) {
      System.out.println(elements_.length);
      try {
        ((SinaviImplementation) app_).glassStop_.setVisible(true);
        File fichier_ = null;
        if (rbHtml_.isSelected()) {
          fichier_ = SinaviGestionProjet.saveHtmlFile();
        } else if (rbTexte_.isSelected()) {
          fichier_ = SinaviGestionProjet.saveCsvFile();
        } else if (rbImage_.isSelected()) {
          fichier_ = SinaviGestionProjet.saveGifFile();
        }
        if (fichier_ == null) {
          ((SinaviImplementation) app_).glassStop_.setVisible(false);
          return;
        }
        if (rbImage_.isSelected()) {
          sgr_.ecritImageGIF(fichier_);
        } else {
          final FileOutputStream FluxEcrit = new FileOutputStream(fichier_);
          final StringBuffer chaine = new StringBuffer("");
          if (rbHtml_.isSelected()) {
            ecritFichierHTML(chaine, fichier_);
          } else if (rbTexte_.isSelected()) {
            ecritFichierCSV(chaine);
          }
          byte[] binaireEcrit = new byte[chaine.toString().length()];
          binaireEcrit = chaine.toString().getBytes();
          final int nombre_octets = binaireEcrit.length;
          final int nombre_car = nombre_octets / 100;
          int index = 0;
          for (int i = 0; i < 100; i++) {
            FluxEcrit.write(binaireEcrit, index, nombre_car);
            index += nombre_car;
            app_.getMainPanel().setProgression(i);
          }
          FluxEcrit.write(binaireEcrit, index, nombre_octets - index);
          app_.getMainPanel().setProgression(100);
          app_.getMainPanel().setProgression(0);
          binaireEcrit = null;
          FluxEcrit.close();
        }
        SinaviImplementation.assistant_.changeAttitude(BuAssistant.ATTENTE, "Enregistrement Termin�");
        ((SinaviImplementation) app_).glassStop_.setVisible(false);
      } catch (final IOException ex) {
        ex.printStackTrace();
      }
    } else {
      try {
        setClosed(true);
      } catch (final Exception _pve) {}
    }
  }
}
