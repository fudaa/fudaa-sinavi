/*
 * @file         SinaviRappelDonnees.java
 * @creation     2001-05-17
 * @modification $Date: 2006-09-19 15:09:00 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.sinavi;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.fudaa.dodico.corba.navigation.IBiefNavigation;
import org.fudaa.dodico.corba.navigation.IEcluseFluviale;
import org.fudaa.dodico.corba.navigation.IGare;
import org.fudaa.dodico.corba.navigation.IGenerationDeterministe;
import org.fudaa.dodico.corba.navigation.IGenerationDeterministeHelper;
import org.fudaa.dodico.corba.navigation.IGenerationJournaliereAleatoire;
import org.fudaa.dodico.corba.navigation.IGenerationJournaliereAleatoireHelper;

/**
 * G�n�ration du rappel des donn�es au format HTML.
 * 
 * @version $Revision: 1.6 $ $Date: 2006-09-19 15:09:00 $ by $Author: deniger $
 * @author Aline Marechalle , Franck Lejeune
 */
public class SinaviRappelDonnees {

  /**
   * SinaviRappelDonnees.
   */
  public SinaviRappelDonnees() {}

  /**
   * M�thode de g�n�ration du rappel des donn�es au format HTML dans une chaine de caract�res.
   */
  public String construitRappelDonnees(final boolean _ok_donneesgenerales, final boolean _ok_bateauxdefinis,
      final boolean _ok_biefs, final boolean _ok_ecluses, final boolean _ok_gares, final IBiefNavigation[] _biefs,
      final IEcluseFluviale[] _ecluses, final IGare[] _gares) {
    final StringBuffer html_ = new StringBuffer("");
    html_.append("<HTML><HEAD><TITLE></TITLE></HEAD><BODY>");
    html_.append("<H1 align=center>RAPPEL DES DONNEES</H1>");
    if (_ok_donneesgenerales) {
      html_.append(HTMLDonneesGenerales());
    }
    if (_ok_bateauxdefinis) {
      html_.append(HTMLBateauxDefinis());
    }
    if (_ok_biefs) {
      html_.append(HTMLBiefsDefinis(_biefs));
    }
    if (_ok_ecluses) {
      html_.append(HTMLEclusesDefinies(_ecluses));
    }
    if (_ok_gares) {
      html_.append(HTMLGaresDefinies(_gares));
    }
    html_.append("</BODY></HTML>");
    try {
      final FileOutputStream outstream = new FileOutputStream(new File("/home/users/lejeune/toto.html"));
      byte[] out = new byte[html_.toString().length()];
      out = html_.toString().getBytes();
      outstream.write(out);
    } catch (final IOException ioe) {}
    return html_.toString();
  }

  private String MiseEnFormeDuree(final int _secondes) {
    final DureeField duree_ = new DureeField(false, false, true, true, false);
    return (duree_.STRING_DUREE.valueToString(new Integer(_secondes)));
  }

  private String MiseEnFormeDate(final int _secondes) {
    final Date date_ = new Date(_secondes * 1000l);
    final SimpleDateFormat format_ = new SimpleDateFormat("EEEE dd MMMM yyyy");
    return format_.format(date_);
  }

  private String MiseEnFormeTypeEcluse(final boolean _auto) {
    // TODO appele SinaviResource
    if (_auto) {
      return "Automatique";
    }
    return "Manuelle";
  }

  // TODO appele SinaviResource
  private String MiseEnFormeBooleen(final boolean _booleen) {
    if (_booleen) {
      return "Oui";
    }
    return "Non";
  }

  private StringBuffer HTMLDonneesGenerales() {
    final StringBuffer htmlDonneesGenerales_ = new StringBuffer("");
    htmlDonneesGenerales_.append("<H2><I><B>DONNEES GENERALES :</B></I></H2>");
    htmlDonneesGenerales_.append("<H3><P>Nom de l'&eacute;tude : " + SinaviImplementation.ETUDE_SINAVI.nom() + "</P>");
    htmlDonneesGenerales_.append("<P>D&eacute;but de la Simulation : "
        + MiseEnFormeDate((int) SinaviImplementation.ETUDE_SINAVI.dateDebutSimulation()) + "</P>");
    htmlDonneesGenerales_.append("<P>D&eacute;but r&eacute;el de la Simulation : "
        + MiseEnFormeDate((int) SinaviImplementation.ETUDE_SINAVI.dateDebutReelSimulation()) + "</P>");
    htmlDonneesGenerales_.append("<P>Fin de la Simulation : "
        + MiseEnFormeDate((int) SinaviImplementation.ETUDE_SINAVI.dateFinSimulation()) + "</P>");
    htmlDonneesGenerales_.append("<P>Nombre de graine utilis&eacute;e : "
        + SinaviImplementation.ETUDE_SINAVI.nombreSeries() + "</P>");
    htmlDonneesGenerales_.append("<P><BR><BR></P></H3>");
    return htmlDonneesGenerales_;
  }

  private StringBuffer HTMLBateauxDefinis() {
    final StringBuffer htmlBateauxDefinis_ = new StringBuffer("");
    htmlBateauxDefinis_.append("<H2><P><I><B>BATEAUX DEFINIS :</B></I></P></H2>");
    for (int i = 0; i < SinaviImplementation.ETUDE_SINAVI.naviresType().length; i++) {
      htmlBateauxDefinis_.append("<TABLE width=100% cols=4 border=1 cellpadding=4 cellspacing=0>");
      htmlBateauxDefinis_.append("<TR valign=top>");
      htmlBateauxDefinis_.append("<TH width=50% colspan=2 bgcolor=\"#9999ff\">");
      htmlBateauxDefinis_.append("<P><B>Categorie : "
          + SinaviImplementation.ETUDE_SINAVI.naviresType()[i].groupeEnChaine() + "</B></P></TH>");
      htmlBateauxDefinis_.append("<TH width=50% colspan=2 bgcolor=\"#9999ff\">");
      htmlBateauxDefinis_.append("<P><B>Type de bateau : " + SinaviImplementation.ETUDE_SINAVI.naviresType()[i].type()
          + "</B></P></TH></TR>");
      htmlBateauxDefinis_.append("<TR valign=top>");
      htmlBateauxDefinis_.append("<TD width=25% bgcolor=\"#e6e6ff\">");
      htmlBateauxDefinis_.append("<P align=center>Longueur (m) :</P></TD>");
      htmlBateauxDefinis_.append("<TD width=25%>");
      htmlBateauxDefinis_.append("<P align=center>" + SinaviImplementation.ETUDE_SINAVI.naviresType()[i].longueur()
          + "</P></TD>");
      htmlBateauxDefinis_.append("<TD width=25% bgcolor=\"#e6e6ff\">");
      htmlBateauxDefinis_.append("<P align=center>Tirant d'eau (m) :</P></TD>");
      htmlBateauxDefinis_.append("<TD width=25% >");
      htmlBateauxDefinis_.append("<P align=center>" + SinaviImplementation.ETUDE_SINAVI.naviresType()[i].tirantMoy()
          + "</P></TD></TR>");
      htmlBateauxDefinis_.append("<TR valign=top>");
      htmlBateauxDefinis_.append("<TD bgcolor=\"#e6e6ff\">");
      htmlBateauxDefinis_.append("<P align=center>Largeur (m) :</P></TD>");
      htmlBateauxDefinis_.append("<TD width=25%>");
      htmlBateauxDefinis_.append("<P align=center>" + SinaviImplementation.ETUDE_SINAVI.naviresType()[i].largeur()
          + "</P></TD>");
      htmlBateauxDefinis_.append("<TD width=25% bgcolor=\"#e6e6ff\">");
      htmlBateauxDefinis_.append("<P align=center>G&egrave;ne admissible (h:min) :</P></TD>");
      htmlBateauxDefinis_.append("<TD width=25%>");
      htmlBateauxDefinis_.append("<P align=center>"
          + MiseEnFormeDuree((int) SinaviImplementation.ETUDE_SINAVI.naviresType()[i].dureeAttenteAdmissible())
          + "</P></TD></TR>");
      htmlBateauxDefinis_.append("<TR valign=top>");
      htmlBateauxDefinis_.append("<TD width=25% bgcolor=\"#e6e6ff\">");
      htmlBateauxDefinis_.append("<P align=center>Vitesse (km/h) :</P></TD>");
      htmlBateauxDefinis_.append("<TD width=25%>");
      htmlBateauxDefinis_.append("<P align=center>" + SinaviImplementation.ETUDE_SINAVI.naviresType()[i].vitesseMoy()
          + "</P></TD>");
      htmlBateauxDefinis_.append("<TD width=25% bgcolor=\"#e6e6ff\">");
      htmlBateauxDefinis_.append("<P><BR></P></TD>");
      htmlBateauxDefinis_.append("<TD width=25%>");
      htmlBateauxDefinis_.append("<P><BR></P></TD></TR></TABLE>");
      htmlBateauxDefinis_.append("<H3><P><BR></P></H3>");
    }
    htmlBateauxDefinis_.append("<H3><P><BR></P></H3>");
    return htmlBateauxDefinis_;
  }

  private StringBuffer HTMLBiefsDefinis(final IBiefNavigation[] _biefs) {
    final StringBuffer htmlBiefsDefinis_ = new StringBuffer("");
    IBiefNavigation[] biefs_;
    if (_biefs == null) {
      biefs_ = SinaviImplementation.ETUDE_SINAVI.reseau().biefs();
    } else {
      biefs_ = _biefs;
    }
    htmlBiefsDefinis_.append("<H2><P><I><B>BIEFS DEFINIS :</B></I></P></H2>");
    for (int i = 0; i < biefs_.length; i++) {
      htmlBiefsDefinis_.append("<TABLE width=100% cols=4 border=1 cellpadding=4 cellspacing=0>");
      htmlBiefsDefinis_.append("<TR valign=top>");
      htmlBiefsDefinis_.append("<TH colspan=4 bgcolor=\"#9999ff\">");
      htmlBiefsDefinis_.append("<P><B>Nom du Bief : " + biefs_[i].nom() + "</B></P></TH></TR>");
      htmlBiefsDefinis_.append("<TR valign=top>");
      htmlBiefsDefinis_.append("<TD width=25% bgcolor=\"#e6e6ff\">");
      htmlBiefsDefinis_.append("<P align=center>Longueur (km) :</P></TD>");
      htmlBiefsDefinis_.append("<TD width=25%>");
      htmlBiefsDefinis_.append("<P align=center>" + (biefs_[i].longueur() / 1000) + "</P></TD>");
      htmlBiefsDefinis_.append("<TD width=25% bgcolor=\"#e6e6ff\">");
      htmlBiefsDefinis_.append("<P align=center>Profondeur (m) :</P></TD>");
      htmlBiefsDefinis_.append("<TD width=25%>");
      htmlBiefsDefinis_.append("<P align=center>" + biefs_[i].profondeur() + "</P></TD></TR>");
      htmlBiefsDefinis_.append("<TR valign=top>");
      htmlBiefsDefinis_.append("<TD width=25% bgcolor=\"#e6e6ff\">");
      htmlBiefsDefinis_.append("<P align=center>Largeur (m) :</P></TD>");
      htmlBiefsDefinis_.append("<TD width=25%>");
      htmlBiefsDefinis_.append("<P align=center>" + biefs_[i].largeur() + "</P></TD>");
      htmlBiefsDefinis_.append("<TD width=25% bgcolor=\"#e6e6ff\">");
      htmlBiefsDefinis_.append("<P align=center>Vitesse Maximum (km/h) :</P></TD>");
      htmlBiefsDefinis_.append("<TD width=25%>");
      htmlBiefsDefinis_.append("<P align=center>" + biefs_[i].regle().vitesseMaximum() + "</P></TD></TR>");
      htmlBiefsDefinis_.append("<TR valign=top>");
      htmlBiefsDefinis_.append("<TD width=25% bgcolor=\"#e6e6ff\">");
      htmlBiefsDefinis_.append("<P align=center>Croisement autoris&eacute; :</P></TD>");
      htmlBiefsDefinis_.append("<TD width=25%>");
      htmlBiefsDefinis_.append("<P align=center>" + MiseEnFormeBooleen(biefs_[i].regle().croisementAutorise())
          + "</P></TD>");
      htmlBiefsDefinis_.append("<TD width=25% bgcolor=\"#e6e6ff\">");
      htmlBiefsDefinis_.append("<P align=center>Tr&eacute;matage autoris&eacute; :</P></TD>");
      htmlBiefsDefinis_.append("<TD width=25%>");
      htmlBiefsDefinis_.append("<P align=center>" + MiseEnFormeBooleen(biefs_[i].regle().trematageAutorise())
          + "</P></TD></TR></TABLE>");
      htmlBiefsDefinis_.append("<H3><P><BR></P></H3>");
    }
    htmlBiefsDefinis_.append("<H3><P><BR></P></H3>");
    return htmlBiefsDefinis_;
  }

  private StringBuffer HTMLEclusesDefinies(final IEcluseFluviale[] _ecluses) {
    final StringBuffer htmlEclusesDefinies_ = new StringBuffer("");
    IEcluseFluviale[] ecluses_;
    if (_ecluses == null) {
      ecluses_ = SinaviImplementation.ETUDE_SINAVI.reseau().ecluses();
    } else {
      ecluses_ = _ecluses;
    }
    htmlEclusesDefinies_.append("<H2><P><I><B>ECLUSES DEFINIES :</B></I></P></H2>");
    for (int i = 0; i < ecluses_.length; i++) {
      htmlEclusesDefinies_.append("<TABLE width=100% cols=4 border=1 cellpadding=4 cellspacing=0>");
      htmlEclusesDefinies_.append("<TR valign=top>");
      htmlEclusesDefinies_.append("<TH colspan=4 bgcolor=\"#9999ff\">");
      htmlEclusesDefinies_.append("<P><B>Nom de l'&eacute;cluse : " + ecluses_[i].nom() + "</B></P></TH></TR>");
      htmlEclusesDefinies_.append("<TR valign=top>");
      htmlEclusesDefinies_.append("<TD width=25% bgcolor=\"#e6e6ff\">");
      htmlEclusesDefinies_.append("<P align=center>Longueur (m) :</P></TD>");
      htmlEclusesDefinies_.append("<TD width=25%>");
      htmlEclusesDefinies_.append("<P align=center>" + ecluses_[i].longueur() + "</P></TD>");
      htmlEclusesDefinies_.append("<TD width=25% bgcolor=\"#e6e6ff\">");
      htmlEclusesDefinies_.append("<P align=center>Profondeur (m) :</P></TD>");
      htmlEclusesDefinies_.append("<TD width=25%>");
      htmlEclusesDefinies_.append("<P align=center>" + ecluses_[i].profondeur() + "</P></TD></TR>");
      htmlEclusesDefinies_.append("<TR valign=top>");
      htmlEclusesDefinies_.append("<TD width=25% bgcolor=\"#e6e6ff\">");
      htmlEclusesDefinies_.append("<P align=center>Largeur (m):</P></TD>");
      htmlEclusesDefinies_.append("<TD width=25%>");
      htmlEclusesDefinies_.append("<P align=center>" + ecluses_[i].largeur() + "</P></TD>");
      htmlEclusesDefinies_.append("<TD width=25% bgcolor=\"#e6e6ff\">");
      htmlEclusesDefinies_.append("<P align=center>Type d'&eacute;cluse :</P></TD>");
      htmlEclusesDefinies_.append("<TD width=25%>");
      htmlEclusesDefinies_.append("<P align=center>" + MiseEnFormeTypeEcluse(ecluses_[i].automatique())
          + "</P></TD></TR>");
      htmlEclusesDefinies_.append("<TR valign=top>");
      htmlEclusesDefinies_.append("<TD width=25% bgcolor=\"#e6e6ff\">");
      htmlEclusesDefinies_.append("<P align=center>Fausse Bassin&eacute;e Montante (h:min) :</P></TD>");
      htmlEclusesDefinies_.append("<TD width=25%>");
      htmlEclusesDefinies_.append("<P align=center>"
          + MiseEnFormeDuree((int) ecluses_[i].dureeFausseBassineeMontante()) + "</P></TD>");
      htmlEclusesDefinies_.append("<TD width=25% bgcolor=\"#e6e6ff\">");
      htmlEclusesDefinies_.append("<P align=center>Fausse Bassin&eacute;e Avalante (h:min) :</P></TD>");
      htmlEclusesDefinies_.append("<TD width=25%>");
      htmlEclusesDefinies_.append("<P align=center>"
          + MiseEnFormeDuree((int) ecluses_[i].dureeFausseBassineeAvalante()) + "</P></TD></TR>");
      htmlEclusesDefinies_.append("<TR valign=top>");
      htmlEclusesDefinies_.append("<TD width=25% bgcolor=\"#e6e6ff\">");
      htmlEclusesDefinies_.append("<P align=center>Temps d'Entr�e Moyen (h:min) :</P></TD>");
      htmlEclusesDefinies_.append("<TD width=25%>");
      htmlEclusesDefinies_.append("<P align=center>" + MiseEnFormeDuree((int) ecluses_[i].dureeMoyenneEntree())
          + "</P></TD>");
      htmlEclusesDefinies_.append("<TD width=25% bgcolor=\"#e6e6ff\">");
      htmlEclusesDefinies_.append("<P align=center>Temps Sortie Moyen (h:min) :</P></TD>");
      htmlEclusesDefinies_.append("<TD width=25%>");
      htmlEclusesDefinies_.append("<P align=center>" + MiseEnFormeDuree((int) ecluses_[i].dureeMoyenneSortie())
          + "</P></TD></TR>");
      htmlEclusesDefinies_.append("<TR valign=top>");
      htmlEclusesDefinies_.append("<TD width=25% bgcolor=\"#e6e6ff\">");
      htmlEclusesDefinies_.append("<P align=center>Bassin&eacute;e Montante Moyenne (h:min) :</P></TD>");
      htmlEclusesDefinies_.append("<TD width=25%>");
      htmlEclusesDefinies_.append("<P align=center>"
          + MiseEnFormeDuree((int) ecluses_[i].dureeMoyenneBassineeMontante()) + "</P></TD>");
      htmlEclusesDefinies_.append("<TD width=25% bgcolor=\"#e6e6ff\">");
      htmlEclusesDefinies_.append("<P align=center>Bassin&eacute;e Avalante Moyenne (h:min) :</P></TD>");
      htmlEclusesDefinies_.append("<TD width=25%>");
      htmlEclusesDefinies_.append("<P align=center>"
          + MiseEnFormeDuree((int) ecluses_[i].dureeMoyenneBassineeAvalante()) + "</P></TD></TR>");
      for (int j = 0; j < ecluses_[i].creneaux().length; j++) {
        htmlEclusesDefinies_.append("<TR valign=top>");
        htmlEclusesDefinies_.append("<TD width=25% bgcolor=\"#e6e6ff\">");
        htmlEclusesDefinies_.append("<P align=center>D�but Creneau " + (j + 1) + " (h:min)</P></TD>");
        htmlEclusesDefinies_.append("<TD width=25%>");
        htmlEclusesDefinies_.append("<P align=center>" + MiseEnFormeDuree((int) ecluses_[i].creneaux()[j].heureDebut())
            + "</P></TD>");
        htmlEclusesDefinies_.append("<TD width=25% bgcolor=\"#e6e6ff\">");
        htmlEclusesDefinies_.append("<P align=center>Fin Creneau " + (j + 1) + " (h:min)</P></TD>");
        htmlEclusesDefinies_.append("<TD width=25%>");
        htmlEclusesDefinies_.append("<P align=center>" + MiseEnFormeDuree((int) ecluses_[i].creneaux()[j].heureFin())
            + "</P></TD></TR>");
      }
      for (int k = 0; k < ecluses_[i].dureesManoeuvres().length; k++) {
        if (k / 2 == 0) {
          htmlEclusesDefinies_.append("<TR valign=top>");
          htmlEclusesDefinies_.append("<TH colspan=4 bgcolor=\"#00cccc\">");
          htmlEclusesDefinies_.append("<P align=center><B>Temps de manoeuvres selon le type de bateau :"
              + ecluses_[i].dureesManoeuvres()[k].navireType().type() + "</B></P></TH></TR>");
          htmlEclusesDefinies_.append("<TR valign=top>");
          htmlEclusesDefinies_.append("<TD width=25% bgcolor=\"#ccffff\">");
          htmlEclusesDefinies_.append("<P align=center>Entr&eacute;e :</P></TD>");
          htmlEclusesDefinies_.append("<TD width=25%>");
          htmlEclusesDefinies_.append("<P align=center>"
              + MiseEnFormeDuree((int) ecluses_[i].dureesManoeuvres()[k].dureeEntree()) + "</P></TD>");
          htmlEclusesDefinies_.append("<TD width=25% bgcolor=\"#ccffff\">");
          htmlEclusesDefinies_.append("<P align=center>Bassin&eacute;e Montante :</P></TD>");
          htmlEclusesDefinies_.append("<TD width=25%>");
          htmlEclusesDefinies_.append("<P align=center>"
              + MiseEnFormeDuree((int) ecluses_[i].dureesManoeuvres()[k].dureeBassineeMontante()) + "</P></TD></TR>");
          htmlEclusesDefinies_.append("<TR valign=top>");
          htmlEclusesDefinies_.append("<TD width=25% bgcolor=\"#ccffff\">");
          htmlEclusesDefinies_.append("<P align=center>Sortie :</P></TD>");
          htmlEclusesDefinies_.append("<TD width=25%>");
          htmlEclusesDefinies_.append("<P align=center>"
              + MiseEnFormeDuree((int) ecluses_[i].dureesManoeuvres()[k].dureeSortie()) + "</P></TD>");
          htmlEclusesDefinies_.append("<TD width=25% bgcolor=\"#ccffff\">");
          htmlEclusesDefinies_.append("<P align=center>Bassin&eacute;e Avalante :</P></TD>");
          htmlEclusesDefinies_.append("<TD width=25%>");
          htmlEclusesDefinies_.append("<P align=center>"
              + MiseEnFormeDuree((int) ecluses_[i].dureesManoeuvres()[k].dureeBassineeAvalante()) + "</P></TD></TR>");
        } else {
          htmlEclusesDefinies_.append("<TR valign=top>");
          htmlEclusesDefinies_.append("<TH colspan=4 bgcolor=\"#ffda9a\">");
          htmlEclusesDefinies_.append("<P align=center><B>Temps de manoeuvres selon le type de bateau :"
              + ecluses_[i].dureesManoeuvres()[k].navireType() + "</B></P></TH></TR>");
          htmlEclusesDefinies_.append("<TR valign=top>");
          htmlEclusesDefinies_.append("<TD width=25% bgcolor=\"#ffecd0\">");
          htmlEclusesDefinies_.append("<P align=center>Entr&eacute;e :</P></TD>");
          htmlEclusesDefinies_.append("<TD width=25%>");
          htmlEclusesDefinies_.append("<P align=center>"
              + MiseEnFormeDuree((int) ecluses_[i].dureesManoeuvres()[k].dureeEntree()) + "</P></TD>");
          htmlEclusesDefinies_.append("<TD width=25% bgcolor=\"#ffecd0\">");
          htmlEclusesDefinies_.append("<P align=center>Bassin&eacute;e Montante :</P></TD>");
          htmlEclusesDefinies_.append("<TD width=25%>");
          htmlEclusesDefinies_.append("<P align=center>"
              + MiseEnFormeDuree((int) ecluses_[i].dureesManoeuvres()[k].dureeBassineeMontante()) + "</P></TD></TR>");
          htmlEclusesDefinies_.append("<TR valign=top>");
          htmlEclusesDefinies_.append("<TD width=25% bgcolor=\"#ffecd0\">");
          htmlEclusesDefinies_.append("<P align=center>Sortie :</P></TD>");
          htmlEclusesDefinies_.append("<TD width=25%>");
          htmlEclusesDefinies_.append("<P align=center>"
              + MiseEnFormeDuree((int) ecluses_[i].dureesManoeuvres()[k].dureeSortie()) + "</P></TD>");
          htmlEclusesDefinies_.append("<TD width=25% bgcolor=\"#ffecd0\">");
          htmlEclusesDefinies_.append("<P align=center>Bassin&eacute;e Avalante :</P></TD>");
          htmlEclusesDefinies_.append("<TD width=25%>");
          htmlEclusesDefinies_.append("<P align=center>"
              + MiseEnFormeDuree((int) ecluses_[i].dureesManoeuvres()[k].dureeBassineeAvalante()) + "</P></TD></TR>");
        }
      }
      htmlEclusesDefinies_.append("</TABLE>");
      htmlEclusesDefinies_.append("<H3><P><BR></P></H3>");
    }
    htmlEclusesDefinies_.append("<H3><P><BR></P></H3>");
    return htmlEclusesDefinies_;
  }

  private StringBuffer HTMLGaresDefinies(final IGare[] _gares) {
    final StringBuffer htmlGaresDefinies_ = new StringBuffer("");
    IGare[] gares_;
    if (_gares == null) {
      gares_ = SinaviImplementation.ETUDE_SINAVI.reseau().gares();
    } else {
      gares_ = _gares;
    }
    htmlGaresDefinies_.append("<H2><P><I><B>GARES DEFINIES :</B></I></P></H2>");
    for (int i = 0; i < gares_.length; i++) {
      htmlGaresDefinies_.append("<TABLE width=100% cols=5 border=1 cellpadding=4 cellspacing=0>");
      htmlGaresDefinies_.append("<TR valign=top>");
      htmlGaresDefinies_.append("<TH colspan=2 bgcolor=\"#9999ff\">");
      htmlGaresDefinies_.append("<P><B>Nom de la gare : " + gares_[i].nom() + "</B></P></TH>");
      htmlGaresDefinies_.append("<TH colspan=3 bgcolor=\"#9999ff\">");
      htmlGaresDefinies_.append("<P><B>nombre maximum de navires : " + gares_[i].nombreMaxNavire()
          + "</B></P></TH></TR>");
      for (int j = 0; j < gares_[i].naviresPartants().length; j++) {
        htmlGaresDefinies_.append("<TR valign=top>");
        htmlGaresDefinies_.append("<TD colspan=5 bgcolor=\"#e6e6ff\">");
        htmlGaresDefinies_.append("<P align=center>Type de bateau partant : " + gares_[i].naviresPartants()[j].type()
            + "</P></TD></TR>");
        for (int k = 0; k < gares_[i].trajetsParNavire(gares_[i].naviresPartants()[j]).length; k++) {
          if ((k + 1) / 2 == 0) {
            htmlGaresDefinies_.append("<TR valign=top>");
            htmlGaresDefinies_.append("<TD width=20% bgcolor=\"#00cccc\">");
            htmlGaresDefinies_.append("<P align=center>Trajet " + (k + 1) + " :</P></TD>");
            htmlGaresDefinies_.append("<TD width=20% bgcolor=\"#00cccc\">");
            htmlGaresDefinies_.append("<P align=center>Vitesse :</P></TD>");
            htmlGaresDefinies_.append("<TD width=20% bgcolor=\"#00cccc\">");
            htmlGaresDefinies_.append("<P align=center>"
                + gares_[i].trajetsParNavire(gares_[i].naviresPartants()[j])[k].vitesse() + "</P></TD>");
            htmlGaresDefinies_.append("<TD width=20% bgcolor=\"#00cccc\">");
            htmlGaresDefinies_.append("<P align=center>Gare d'arriv&eacute;e :</P></TD>");
            htmlGaresDefinies_.append("<TD width=20% bgcolor=\"#00cccc\">");
            htmlGaresDefinies_.append("<P align=center>"
                + gares_[i].trajetsParNavire(gares_[i].naviresPartants()[j])[k].gareArrivee().nom() + "</P></TD></TR>");
            htmlGaresDefinies_.append("<TR valign=top>");
            htmlGaresDefinies_.append("<TD colspan=5 bgcolor=\"#ccffff\">");
            htmlGaresDefinies_.append("<P align=center>Lois associ&eacute;es au trajet " + (k + 1) + ".</P></TD></TR>");
          } else {
            htmlGaresDefinies_.append("<TR valign=top>");
            htmlGaresDefinies_.append("<TD width=20% bgcolor=\"#ffda9a\">");
            htmlGaresDefinies_.append("<P align=center>Trajet " + (k + 1) + " :</P></TD>");
            htmlGaresDefinies_.append("<TD width=20% bgcolor=\"#ffda9a\">");
            htmlGaresDefinies_.append("<P align=center>Vitesse :</P></TD>");
            htmlGaresDefinies_.append("<TD width=20% bgcolor=\"#ffda9a\">");
            htmlGaresDefinies_.append("<P align=center>"
                + gares_[i].trajetsParNavire(gares_[i].naviresPartants()[j])[k].vitesse() + "</P></TD>");
            htmlGaresDefinies_.append("<TD width=20% bgcolor=\"#ffda9a\">");
            htmlGaresDefinies_.append("<P align=center>Gare d'arriv&eacute;e :</P></TD>");
            htmlGaresDefinies_.append("<TD width=20% bgcolor=\"#ffda9a\">");
            htmlGaresDefinies_.append("<P align=center>"
                + gares_[i].trajetsParNavire(gares_[i].naviresPartants()[j])[k].gareArrivee().nom() + "</P></TD></TR>");
            htmlGaresDefinies_.append("<TR valign=top>");
            htmlGaresDefinies_.append("<TD colspan=5 bgcolor=\"#ffecd0\">");
            htmlGaresDefinies_.append("<P align=center>Lois associ&eacute;es au trajet " + (k + 1) + ".</P></TD></TR>");
          }
          for (int l = 0; l < gares_[i].trajetsParNavire(gares_[i].naviresPartants()[j])[k].generations().length; l++) {
            if (((k + 1) / 2 == 0)
                && (gares_[i].trajetsParNavire(gares_[i].naviresPartants()[j])[k].generations()[l] instanceof IGenerationJournaliereAleatoire)) {
              final IGenerationJournaliereAleatoire cga_ = IGenerationJournaliereAleatoireHelper.narrow(gares_[i]
                  .trajetsParNavire(gares_[i].naviresPartants()[j])[k].generations()[l]);
              htmlGaresDefinies_.append("<TR valign=top>");
              htmlGaresDefinies_.append("<TD width=20% bgcolor=\"#ccffff\">");
              htmlGaresDefinies_.append("<P align=center>Date D&eacute;but :</P></TD>");
              htmlGaresDefinies_.append("<TD width=20% bgcolor=\"#ccffff\">");
              htmlGaresDefinies_.append("<P align=center>" + MiseEnFormeDate((int) cga_.dateDebut()) + "</P></TD>");
              htmlGaresDefinies_.append("<TD width=20% bgcolor=\"#ccffff\">");
              htmlGaresDefinies_.append("<P align=center><BR></P></TD>");
              htmlGaresDefinies_.append("<TD width=20% bgcolor=\"#ccffff\">");
              htmlGaresDefinies_.append("<P align=center>Date Fin :</P></TD>");
              htmlGaresDefinies_.append("<TD width=20% bgcolor=\"#ccffff\">");
              htmlGaresDefinies_.append("<P align=center>" + MiseEnFormeDate((int) cga_.dateFin()) + "</P></TD></TR>");
              htmlGaresDefinies_.append("<TR valign=top>");
              htmlGaresDefinies_.append("<TD width=20% bgcolor=\"#ccffff\">");
              htmlGaresDefinies_.append("<P align=center>Aleatoire</P></TD>");
              htmlGaresDefinies_.append("<TD width=20% bgcolor=\"#ccffff\">");
              htmlGaresDefinies_.append("<P align=center>Heure D&eacute;but : "
                  + MiseEnFormeDuree((int) cga_.heureDebut()) + "</P></TD>");
              htmlGaresDefinies_.append("<TD width=20% bgcolor=\"#ccffff\">");
              htmlGaresDefinies_.append("<P align=center>Heure Fin : " + MiseEnFormeDuree((int) cga_.heureFin())
                  + "</P></TD>");
              htmlGaresDefinies_.append("<TD width=20% bgcolor=\"#ccffff\">");
              htmlGaresDefinies_.append("<P align=center>Intervalle Moyen : "
                  + MiseEnFormeDuree(new Double(cga_.valeurMoyenne()).intValue()) + "</P></TD>");
              htmlGaresDefinies_.append("<TD width=20% bgcolor=\"#ccffff\">");
              htmlGaresDefinies_.append("<P align=center>Ordre Loi Erlang : " + cga_.ordreLoi() + "</P></TD></TR>");
            } else if (gares_[i].trajetsParNavire(gares_[i].naviresPartants()[j])[k].generations()[l] instanceof IGenerationJournaliereAleatoire) {
              final IGenerationJournaliereAleatoire cga_ = IGenerationJournaliereAleatoireHelper.narrow(gares_[i]
                  .trajetsParNavire(gares_[i].naviresPartants()[j])[k].generations()[l]);
              htmlGaresDefinies_.append("<TR valign=top>");
              htmlGaresDefinies_.append("<TD width=20% bgcolor=\"#ffecd0\">");
              htmlGaresDefinies_.append("<P align=center>Date D&eacute;but :</P></TD>");
              htmlGaresDefinies_.append("<TD width=20% bgcolor=\"#ffecd0\">");
              htmlGaresDefinies_.append("<P align=center>" + MiseEnFormeDate((int) cga_.dateDebut()) + "</P></TD>");
              htmlGaresDefinies_.append("<TD width=20% bgcolor=\"#ffecd0\">");
              htmlGaresDefinies_.append("<P align=center><BR></P></TD>");
              htmlGaresDefinies_.append("<TD width=20% bgcolor=\"#ffecd0\">");
              htmlGaresDefinies_.append("<P align=center>Date Fin :</P></TD>");
              htmlGaresDefinies_.append("<TD width=20% bgcolor=\"#ffecd0\">");
              htmlGaresDefinies_.append("<P align=center>" + MiseEnFormeDate((int) cga_.dateFin()) + "</P></TD></TR>");
              htmlGaresDefinies_.append("<TR valign=top>");
              htmlGaresDefinies_.append("<TD width=20% bgcolor=\"#ffecd0\">");
              htmlGaresDefinies_.append("<P align=center>Aleatoire</P></TD>");
              htmlGaresDefinies_.append("<TD width=20% bgcolor=\"#ffecd0\">");
              htmlGaresDefinies_.append("<P align=center>Heure D&eacute;but : "
                  + MiseEnFormeDuree((int) cga_.heureDebut()) + "</P></TD>");
              htmlGaresDefinies_.append("<TD width=20% bgcolor=\"#ffecd0\">");
              htmlGaresDefinies_.append("<P align=center>Heure Fin : " + MiseEnFormeDuree((int) cga_.heureFin())
                  + "</P></TD>");
              htmlGaresDefinies_.append("<TD width=20% bgcolor=\"#ffecd0\">");
              htmlGaresDefinies_.append("<P align=center>Intervalle Moyen : "
                  + MiseEnFormeDuree(new Double(cga_.valeurMoyenne()).intValue()) + "</P></TD>");
              htmlGaresDefinies_.append("<TD width=20% bgcolor=\"#ffecd0\">");
              htmlGaresDefinies_.append("<P align=center>Ordre Loi Erlang : " + cga_.ordreLoi() + "</P></TD></TR>");
            } else if (((k + 1) / 2 == 0)
                && (gares_[i].trajetsParNavire(gares_[i].naviresPartants()[j])[k].generations()[l] instanceof IGenerationDeterministe)) {
              final IGenerationDeterministe cgd_ = IGenerationDeterministeHelper.narrow(gares_[i]
                  .trajetsParNavire(gares_[i].naviresPartants()[j])[k].generations()[l]);
              htmlGaresDefinies_.append("<TR valign=top>");
              htmlGaresDefinies_.append("<TD width=20% bgcolor=\"#ccffff\">");
              htmlGaresDefinies_.append("<P align=center>deterministe</P></TD>");
              htmlGaresDefinies_.append("<TD width=20% bgcolor=\"#ccffff\">");
              htmlGaresDefinies_.append("<P align=center>Premier Horaire : " + MiseEnFormeDuree((int) cgd_.dateDebut())
                  + "</P></TD>");
              htmlGaresDefinies_.append("<TD width=20% bgcolor=\"#ccffff\">");
              htmlGaresDefinies_.append("<P align=center>...</P></TD>");
              htmlGaresDefinies_.append("<TD width=20% bgcolor=\"#ccffff\">");
              htmlGaresDefinies_.append("<P align=center>...</P></TD>");
              htmlGaresDefinies_.append("<TD width=20% bgcolor=\"#ccffff\">");
              htmlGaresDefinies_.append("<P align=center>Dernier Horaire : " + MiseEnFormeDuree((int) cgd_.dateFin())
                  + "</P></TD></TR>");
            } else if (gares_[i].trajetsParNavire(gares_[i].naviresPartants()[j])[k].generations()[l] instanceof IGenerationDeterministe) {
              final IGenerationDeterministe cgd_ = IGenerationDeterministeHelper.narrow(gares_[i]
                  .trajetsParNavire(gares_[i].naviresPartants()[j])[k].generations()[l]);
              htmlGaresDefinies_.append("<TR valign=top>");
              htmlGaresDefinies_.append("<TD width=20% bgcolor=\"#ffecd0\">");
              htmlGaresDefinies_.append("<P align=center>deterministe</P></TD>");
              htmlGaresDefinies_.append("<TD width=20% bgcolor=\"#ffecd0\">");
              htmlGaresDefinies_.append("<P align=center>Premier Horaire : " + MiseEnFormeDuree((int) cgd_.dateDebut())
                  + "</P></TD>");
              htmlGaresDefinies_.append("<TD width=20% bgcolor=\"#ffecd0\">");
              htmlGaresDefinies_.append("<P align=center>...</P></TD>");
              htmlGaresDefinies_.append("<TD width=20% bgcolor=\"#ffecd0\">");
              htmlGaresDefinies_.append("<P align=center>...</P></TD>");
              htmlGaresDefinies_.append("<TD width=20% bgcolor=\"#ffecd0\">");
              htmlGaresDefinies_.append("<P align=center>Dernier Horaire : " + MiseEnFormeDuree((int) cgd_.dateFin())
                  + "</P></TD></TR>");
            }
          }
        }
      }
      htmlGaresDefinies_.append("</TABLE>");
      htmlGaresDefinies_.append("<H3><P><BR></P></H3>");
    }
    return htmlGaresDefinies_;
  }
}
