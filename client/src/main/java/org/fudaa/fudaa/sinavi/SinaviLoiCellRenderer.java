/*
 * @file         SinaviLoiCellRenderer.java
 * @creation     2001-05-17
 * @modification $Date: 2006-09-19 15:09:00 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.sinavi;

import java.awt.Component;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;

import org.fudaa.dodico.corba.navigation.IGenerationHelper;

/**
 * impl�mentation du rendu de la table affichant les loi associ�es � un trajet
 * 
 * @version $Revision: 1.6 $ $Date: 2006-09-19 15:09:00 $ by $Author: deniger $
 * @author Aline Marechalle , Franck Lejeune
 */
public class SinaviLoiCellRenderer extends DefaultTableCellRenderer implements TableCellRenderer {

  /**
   * D�finit le rendu de la table affichant les loi associ�es � un trajet
   */
  public SinaviLoiCellRenderer() {}

  /**
   * retourne une chaine de caract�re compos�e de la date de d�but et de fin de la loi � partir de la loi
   * 
   * @param table la table sur lquelle s'applique le rendu
   * @param value la loi
   * @param isSelected ...
   * @param hasFocus ...
   * @param row la ligne
   * @param column la colonne
   * @return la chaine de caract�re
   */
  public Component getTableCellRendererComponent(final JTable table, final Object value, final boolean isSelected,
      final boolean hasFocus, final int row, final int column) {
    setBackground(isSelected ? table.getSelectionBackground() : table.getBackground());
    setForeground(table.getForeground());
    final Date debut_ = new Date((IGenerationHelper.narrow((org.omg.CORBA.Object) value).dateDebut() * 1000l));
    final Date fin_ = new Date((IGenerationHelper.narrow((org.omg.CORBA.Object) value).dateFin() * 1000l));
    final SimpleDateFormat format_ = new SimpleDateFormat("dd/MM");
    setText(format_.format(debut_) + " - " + format_.format(fin_));
    return this;
  }
}
