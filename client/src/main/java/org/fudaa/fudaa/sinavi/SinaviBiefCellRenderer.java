/*
 * @file         SinaviBiefCellRenderer.java
 * @creation     2001-05-17
 * @modification $Date: 2006-09-19 15:09:00 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.sinavi;

import java.awt.Component;

import javax.swing.DefaultListCellRenderer;
import javax.swing.JList;
import javax.swing.ListCellRenderer;

import org.fudaa.dodico.corba.navigation.IBiefNavigationHelper;

/**
 * impl�mentation du rendu de la liste des biefs
 * 
 * @version $Revision: 1.7 $ $Date: 2006-09-19 15:09:00 $ by $Author: deniger $
 * @author Aline Marechalle , Franck Lejeune
 */
public class SinaviBiefCellRenderer extends DefaultListCellRenderer implements ListCellRenderer<Object> {

  /**
   * D�finit le rendu de la liste affichant les biefs cr��s sur le r�seau
   */
  public SinaviBiefCellRenderer() {}

  /**
   * retourne le nom du bief � partir de l'objet Bief
   * 
   * @param list la liste sur laquelle le rendu s'applique
   * @param value l'objet Bief
   * @param index l'index dans la liste
   * @param isSelected ...
   * @param cellHasFocus ...
   * @return retourne le nom du bief � partir de l'objet Bief
   */
  public Component getListCellRendererComponent(final JList list, final Object value, final int index,
      final boolean isSelected, final boolean cellHasFocus) {
    setBackground(isSelected ? list.getSelectionBackground() : list.getBackground());
    setForeground(list.getForeground());
    setText(IBiefNavigationHelper.narrow((org.omg.CORBA.Object) value).nom());
    return this;
  }
}
