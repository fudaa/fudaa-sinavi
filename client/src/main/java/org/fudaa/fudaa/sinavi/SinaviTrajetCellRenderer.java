/*
 * @file         SinaviTrajetCellRenderer.java
 * @creation     2001-05-17
 * @modification $Date: 2006-09-19 15:08:59 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.sinavi;

import java.awt.Component;

import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;

import org.fudaa.dodico.corba.navigation.ITrajetFluvialHelper;

/**
 * impl�mentation du rendu de la table affichant les trajets associ�s � un type de bateau partant
 * 
 * @version $Id: SinaviTrajetCellRenderer.java,v 1.6 2006-09-19 15:08:59 deniger Exp $
 * @author Aline Marechalle , Franck Lejeune
 */
public class SinaviTrajetCellRenderer extends DefaultTableCellRenderer implements TableCellRenderer {

  /**
   * D�finit le rendu de la table affichant les trajets associ�s � un type de bateau partant
   */
  public SinaviTrajetCellRenderer() {}

  /**
   * retourne le nom de la gare d'arriv�e � partir du trajet
   * 
   * @param table la table sur lquelle s'applique le rendu
   * @param value le trajet
   * @param isSelected ...
   * @param hasFocus ...
   * @param row la ligne
   * @param column la colonne
   * @return le nom de la gare d'arriv�e
   */
  public Component getTableCellRendererComponent(final JTable table, final Object value, final boolean isSelected,
      final boolean hasFocus, final int row, final int column) {
    setBackground(isSelected ? table.getSelectionBackground() : table.getBackground());
    setForeground(table.getForeground());
    setText(ITrajetFluvialHelper.narrow((org.omg.CORBA.Object) value).gareArrivee().nom());
    return this;
  }
}
