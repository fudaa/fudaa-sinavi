/*
 * @file         SinaviBiefsParametres.java
 * @creation     2001-05-17
 * @modification $Date: 2006-09-19 15:09:00 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.sinavi;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.EmptyBorder;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuCheckBox;
import com.memoire.bu.BuCommonImplementation;
import com.memoire.bu.BuCommonInterface;
import com.memoire.bu.BuDialogMessage;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuPicture;
import com.memoire.bu.BuTextField;

import org.fudaa.dodico.corba.navigation.IBiefNavigation;
import org.fudaa.dodico.corba.navigation.IBiefNavigationHelper;

/**
 * impl�mentation de l'onglet "Biefs" pour les param�tres de sinavi
 * 
 * @version $Revision: 1.9 $ $Date: 2006-09-19 15:09:00 $ by $Author: deniger $
 * @author Aline Marechalle , Franck Lejeune
 */
public class SinaviBiefsParametres extends BuPanel implements ActionListener, MouseListener {

  /**
   * nom du bief
   */
  private final BuTextField tfNomBief_ = new BuTextField();
  /**
   * longueur du bief en m�tre
   */
  private final LongueurField lfTailleBiefLongueur_ = new LongueurField(true, true, false);
  /**
   * largeur du bief en m�tre
   */
  private final LongueurField lfTailleBiefLargeur_ = new LongueurField(false, true, true);
  /**
   * hauteur d'eau du bief en m�tre
   */
  private final LongueurField lfHauteurEau_ = new LongueurField(false, true, true);
  /**
   * vitesse maximum autoris�e dans le bief en km/h
   */
  private final BuTextField tfVitesseMaximum_ = BuTextField.createDoubleField();
  /**
   * boite � cocher d�finissant l'autorisation de croisement
   */
  private final BuCheckBox cbCroisementAutorise_ = new BuCheckBox("Croisement autorise");
  /**
   * boite � cocher d�finissant l'autorisationde tr�matage
   */
  private final BuCheckBox cbTrematageAutorise_ = new BuCheckBox("Trematage autorise");
  /**
   * liste des biefs cr��s
   */
  private final JList lBiefsCrees_ = new JList();
  /**
   * bouton de validation du bief s�lectionn� dans la liste
   */
  private final BuButton bValiderBief_ = new BuButton(SinaviResource.SINAVI.getIcon("VALIDER"), "Enregistrer");
  /**
   * ascenseur pour la liste des biefs cr��s
   */
  private final JScrollPane BiefsCreesScrollPane_ = new JScrollPane(lBiefsCrees_,
      ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
  /**
   * panel contenant les boutons, il est plac� en bas � droite
   */
  private final BuPanel pBoutons_ = new BuPanel();
  /**
   * panel contenant les diff�rents champs de l'onglet
   */
  private final BuPanel pDonnees_ = new BuPanel();
  /**
   * panel contenant le champs de texte message
   */
  private final BuPanel pMessage_ = new BuPanel();
  /**
   * panel contenant les panels boutons et message
   */
  private final BuPanel pSud_ = new BuPanel();
  /**
   * champ de texte pour afficher des messages
   */
  private final BuTextField tfMessage_ = new BuTextField();
  /**
   * instance de SinaviImplementation
   */
  private BuCommonImplementation app_;

  /**
   * Constructeur. _appli doit etre instance de SinaviImplementation
   * 
   * @param _appli
   * @param _mode_edition �dition � partir du r�seau
   */
  public SinaviBiefsParametres(final BuCommonInterface _appli, boolean _mode_edition) {
    super();
    app_ = _appli.getImplementation();
    // Cr�ation du layout manager et des contraintes du panel donnees
    final GridBagLayout lm_ = new GridBagLayout();
    final GridBagConstraints c_ = new GridBagConstraints();
    // Cr�ation du layout manager et du bord de l'onglet
    this.setLayout(new BorderLayout());
    this.setBorder(new EmptyBorder(0, 0, 0, 0));
    // layout des panels
    pDonnees_.setLayout(lm_);
    pMessage_.setLayout(new FlowLayout(FlowLayout.LEFT));
    pBoutons_.setLayout(new FlowLayout(FlowLayout.RIGHT));
    pSud_.setLayout(new BorderLayout());
    // Ajout d'un listener aux differents elements
    bValiderBief_.addActionListener(this);
    lBiefsCrees_.addMouseListener(this);
    // Bouton "valider" pas visible par defaut
    bValiderBief_.setEnabled(false);
    // Contraintes communes � tous les composants
    c_.gridy = GridBagConstraints.RELATIVE;
    c_.anchor = GridBagConstraints.WEST;
    c_.ipadx = 0;
    c_.ipady = 0;
    // Largeur des zones de texte
    tfNomBief_.setColumns(15);
    tfMessage_.setColumns(15);
    tfMessage_.setEditable(false);
    tfVitesseMaximum_.setColumns(3);
    tfVitesseMaximum_.setValue(new Double(0));
    // Contrainte de la liste
    final Dimension dimbc_ = new Dimension(200, 250);
    BiefsCreesScrollPane_.setPreferredSize(dimbc_);
    lBiefsCrees_.setCellRenderer(new SinaviBiefCellRenderer());
    lBiefsCrees_.setModel(SinaviImplementation.SINAVIBIEFLISTMODEL);
    c_.gridheight = 1; // 1 ligne pour les �tiquettes.
    // Remplissage de la premiere colonne
    c_.gridx = 0; // premiere colonne
    if (_mode_edition) {
      placeComposant(lm_, new BuLabel(""), c_);
    } else {
      placeComposant(lm_, new BuLabel("Biefs Cr��s :"), c_);
    }
    c_.insets.top = 10;
    c_.gridheight = GridBagConstraints.REMAINDER;
    if (_mode_edition) {
      placeComposant(lm_, new BuPicture(SinaviResource.SINAVI.getImage("sinaviimagebief")), c_);
    } else {
      placeComposant(lm_, BiefsCreesScrollPane_, c_);
    }
    // Remplissage de la deuxieme colonne
    c_.gridheight = 1;
    c_.gridx = 1; // deuxieme colonne
    c_.insets.left = 25;
    placeComposant(lm_, new BuLabel(""), c_);
    placeComposant(lm_, new BuLabel(""), c_);
    placeComposant(lm_, new BuLabel("Nom :"), c_);
    placeComposant(lm_, new BuLabel("Longueur :"), c_);
    placeComposant(lm_, new BuLabel("Largeur :"), c_);
    placeComposant(lm_, new BuLabel("Hauteur d'eau :"), c_);
    placeComposant(lm_, new BuLabel("Vitesse Maxi :"), c_);
    placeComposant(lm_, cbCroisementAutorise_, c_);
    placeComposant(lm_, tfMessage_, c_);
    // Remplissage de la troisieme colonne
    c_.gridx = 2; // troisieme colonne
    placeComposant(lm_, new BuLabel(""), c_);
    placeComposant(lm_, new BuLabel(""), c_);
    placeComposant(lm_, tfNomBief_, c_);
    placeComposant(lm_, lfTailleBiefLongueur_, c_);
    placeComposant(lm_, lfTailleBiefLargeur_, c_);
    placeComposant(lm_, lfHauteurEau_, c_);
    placeComposant(lm_, tfVitesseMaximum_, c_);
    placeComposant(lm_, cbTrematageAutorise_, c_);
    // ajout du bouton valider au panel boutons
    pBoutons_.add(bValiderBief_);
    // ajout du message au panel message
    pMessage_.add(tfMessage_);
    // ajout de message et boutons a sud
    pSud_.add(pMessage_, BorderLayout.WEST);
    if (!_mode_edition) {
      pSud_.add(pBoutons_, BorderLayout.EAST);
    }
    // ajout des deux panels � l'onglet
    add(pDonnees_, BorderLayout.CENTER);
    add(pSud_, BorderLayout.SOUTH);
  }

  /**
   * sauvegarde des param�tres du bief s�lectionn�
   * 
   * @param _bief le bief s�lectionn� dans la liste
   */
  public void setBiefParametres(final IBiefNavigation _bief) {
    if (validBiefParametres()) {
      SinaviPreferences.SINAVIPREFS.putStringProperty("longueur_bief", "" + lfTailleBiefLongueur_.getLongueurField());
      SinaviPreferences.SINAVIPREFS.putStringProperty("largeur_bief", "" + lfTailleBiefLargeur_.getLongueurField());
      SinaviPreferences.SINAVIPREFS.putStringProperty("hauteur_eau_bief", "" + lfHauteurEau_.getLongueurField());
      SinaviPreferences.SINAVIPREFS.putStringProperty("vitesse_max_bief", ""
          + ((Double) tfVitesseMaximum_.getValue()).doubleValue());
      _bief.nom(tfNomBief_.getText());
      _bief.longueur(lfTailleBiefLongueur_.getLongueurField());
      _bief.largeur(lfTailleBiefLargeur_.getLongueurField());
      _bief.profondeur(lfHauteurEau_.getLongueurField());
      _bief.regle().croisementAutorise(cbCroisementAutorise_.isSelected());
      _bief.regle().trematageAutorise(cbTrematageAutorise_.isSelected());
      _bief.regle().vitesseMaximum(((Double) tfVitesseMaximum_.getValue()).doubleValue());
    }
    /*
     * else { BuDialogMessage dialog_mess = new BuDialogMessage(app_, SinaviImplementation.informationsSoftware(),
     * "Param�tres du bief\nincomplets"); dialog_mess.activate();
     * SinaviImplementation.assistant_.changeAttitude(BuAssistant.PAROLE, "Parametres du bief\nincomplets");
     * //System.out.println("Parametres du bief incomplets"); }
     */
  }

  /**
   * M�thode utile pour placer un composant avec ses contraintes dans le panel "donnees"
   * 
   * @param _lm le layout manager
   * @param _composant le composant
   * @param _c les contraintes
   */
  public void placeComposant(final GridBagLayout _lm, final Component _composant, final GridBagConstraints _c) {
    _lm.setConstraints(_composant, _c);
    pDonnees_.add(_composant);
  }

  /**
   * si clic sur la liste des biefs, affichages des param�tres du bief s�lectionn� dans la liste
   * 
   * @param _e
   */
  public void mouseClicked(final MouseEvent _e) {
    if (lBiefsCrees_.getSelectedValue() == null) {
      // System.out.println("liste vide");
    } else {
      getBiefParametres(IBiefNavigationHelper.narrow((org.omg.CORBA.Object) lBiefsCrees_.getSelectedValue()));
      bValiderBief_.setEnabled(true);
    }
  }

  /**
   * pas utilis�
   * 
   * @param e
   */
  public void mousePressed(final MouseEvent e) {}

  /**
   * pas utilis�
   * 
   * @param e
   */
  public void mouseReleased(final MouseEvent e) {}

  /**
   * pas utilis�
   * 
   * @param e
   */
  public void mouseEntered(final MouseEvent e) {}

  /**
   * pas utilis�
   * 
   * @param e
   */
  public void mouseExited(final MouseEvent e) {}

  /**
   * si action sur le bouton valider bief, test de la validit� des param�tres, si ok sauvegarde puis affichage des
   * nouveaux param�tres ou des anciens si pas valide
   * 
   * @param _e
   */
  public void actionPerformed(final ActionEvent _e) {
    if (_e.getSource() == bValiderBief_) {
      if (validBiefParametres()) {
        setBiefParametres(IBiefNavigationHelper.narrow((org.omg.CORBA.Object) lBiefsCrees_.getSelectedValue()));
        bValiderBief_.setEnabled(false);
        getBiefParametres(IBiefNavigationHelper.narrow((org.omg.CORBA.Object) lBiefsCrees_.getSelectedValue()));
      } else {
        final BuDialogMessage dialog_mess = new BuDialogMessage(app_, SinaviImplementation.informationsSoftware(),
            "Param�tres du bief\nincomplets");
        dialog_mess.activate();
        System.out.println("Parametres du bief incomplets");
        // getBiefParametres(IBiefNavigationHelper.narrow((org.omg.CORBA.Object)lBiefsCrees_.getSelectedValue()));
      }
    }
  }

  /**
   * test de validit� de tous les champs de l'onglet
   */
  public boolean validBiefParametres() {
    if (tfNomBief_.getText().equals(null)) {
      return false;
    }
    if (lfTailleBiefLongueur_.getLongueurField() <= 0) {
      return false;
    }
    if (lfTailleBiefLargeur_.getLongueurField() <= 0) {
      return false;
    }
    if (lfHauteurEau_.getLongueurField() <= 0) {
      return false;
    }
    if (((Double) tfVitesseMaximum_.getValue()).doubleValue() <= 0) {
      return false;
    }
    return true;
  }

  /**
   * mise � jour des champs du bief s�lectionn� dans la liste
   * 
   * @param _bief le bief s�lectionn� dans la liste
   */
  public void getBiefParametres(final IBiefNavigation _bief) {
    // les parametres du bief ont deja ete renseigne
    if (_bief.longueur() != 0) {
      tfMessage_.setForeground(Color.green);
      tfMessage_.setText("Param�tres enregistr�s");
      tfNomBief_.setText(_bief.nom());
      lfTailleBiefLongueur_.setLongueurField(_bief.longueur());
      lfTailleBiefLargeur_.setLongueurField(_bief.largeur());
      lfHauteurEau_.setLongueurField(_bief.profondeur());
      tfVitesseMaximum_.setValue(new Double(_bief.regle().vitesseMaximum()));
      cbCroisementAutorise_.setSelected(_bief.regle().croisementAutorise());
      cbTrematageAutorise_.setSelected(_bief.regle().trematageAutorise());
    } else {
      // les parametres du biefs ne sont pas renseignes ( test pour savoir s'il existe des valeurs par defaut )
      tfMessage_.setForeground(Color.red);
      tfMessage_.setText("Param�tres non enregistr�s");
      tfNomBief_.setText(_bief.nom());
      if (SinaviPreferences.SINAVIPREFS.getStringProperty("longueur_bief").equals("")) {
        lfTailleBiefLongueur_.setLongueurField(0);
      } else {
        lfTailleBiefLongueur_.setLongueurField(new Double(SinaviPreferences.SINAVIPREFS
            .getStringProperty("longueur_bief")).doubleValue());
      }
      if (SinaviPreferences.SINAVIPREFS.getStringProperty("largeur_bief").equals("")) {
        lfTailleBiefLargeur_.setLongueurField(0);
      } else {
        lfTailleBiefLargeur_.setLongueurField(new Double(SinaviPreferences.SINAVIPREFS
            .getStringProperty("largeur_bief")).doubleValue());
      }
      if (SinaviPreferences.SINAVIPREFS.getStringProperty("hauteur_eau_bief").equals("")) {
        lfHauteurEau_.setLongueurField(0);
      } else {
        lfHauteurEau_.setLongueurField(new Double(SinaviPreferences.SINAVIPREFS.getStringProperty("hauteur_eau_bief"))
            .doubleValue());
      }
      if (SinaviPreferences.SINAVIPREFS.getStringProperty("vitesse_max_bief").equals("")) {
        tfVitesseMaximum_.setValue(new Double(0));
      } else {
        tfVitesseMaximum_.setValue(new Double(SinaviPreferences.SINAVIPREFS.getStringProperty("vitesse_max_bief")));
      }
      cbCroisementAutorise_.setSelected(false);
      cbTrematageAutorise_.setSelected(false);
    }
  }
}
