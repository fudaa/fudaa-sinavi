/*
 * @file         SinaviFilleEditionNaviresGeneres.java
 * @creation     2001-05-17
 * @modification $Date: 2006-09-19 15:08:59 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.sinavi;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JComponent;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.EmptyBorder;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuComboBox;
import com.memoire.bu.BuInternalFrame;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuPanel;
import com.memoire.fu.FuVectorint;

import org.fudaa.dodico.corba.navigation.IGenerationJournaliereAleatoire;

/**
 * impl�mentation d'une fen�tre interne permetant d'afficher tous les navires g�n�res pour une loi donn�e
 * 
 * @version $Revision: 1.8 $ $Date: 2006-09-19 15:08:59 $ by $Author: deniger $
 * @author Aline Marechalle , Franck Lejeune
 */
public class SinaviFilleEditionNaviresGeneres extends BuInternalFrame implements ActionListener {

  /**
   * bouton valider
   */
  private final BuButton bValider_ = new BuButton(SinaviResource.SINAVI.getIcon("VALIDER"), "OK");
  /**
   * panel contenant les boutons, il est plac� en bas � droite
   */
  private final BuPanel pboutons_ = new BuPanel();
  /**
   * panel contenant les diff�rents champs de la fen�tre
   */
  private final BuPanel pdonnees_ = new BuPanel();
  /**
   * menu d�roulant pour choisir la s�rie
   */
  private final BuComboBox cbChoixSerie_ = new BuComboBox();
  /**
   * liste affichant les navires g�n�r�s
   */
  private final JList lHeuresCreees_ = new JList();
  /**
   * ascenseur pour la liste affichant les navires g�n�r�s
   */
  private final JScrollPane HeuresCreeesScrollPane_ = new JScrollPane(lHeuresCreees_,
      ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
  /**
   * loi � partir de laquelle ont �t� g�n�r� les navires
   */
  private IGenerationJournaliereAleatoire loi_;

  /**
   * Constructeur SinaviFilleEditionNaviresGeneres
   * 
   * @param _loi � partir de laquelle ont �t� g�n�r� les navires
   */
  public SinaviFilleEditionNaviresGeneres(final IGenerationJournaliereAleatoire _loi) {
    super("Navires G�n�r�s", false, false, false, false);
    loi_ = _loi;
    // Ajout des listener aux boutons
    bValider_.addActionListener(this);
    cbChoixSerie_.addActionListener(this);
    // contraintes de la liste
    lHeuresCreees_.setModel(SinaviImplementation.SINAVIHEURESDETERMINISTESLISTMODEL);
    HeuresCreeesScrollPane_.setPreferredSize(new Dimension(250, 100));
    // creation du contenu de la combobox
    for (int i = 0; i < loi_.instantsGeneres().length; i++) {
      cbChoixSerie_.addItem(("S�rie " + i).toString());
    }
    // remplissage de la fenetre
    ((JComponent) getContentPane()).setBorder(new EmptyBorder(25, 25, 25, 25));
    getContentPane().setLayout(new BorderLayout());
    pdonnees_.add(new BuLabel(""));
    pdonnees_.add(cbChoixSerie_);
    pboutons_.setLayout(new FlowLayout(FlowLayout.RIGHT));
    pboutons_.add(bValider_);
    getContentPane().add(HeuresCreeesScrollPane_, BorderLayout.NORTH);
    getContentPane().add(pdonnees_, BorderLayout.CENTER);
    getContentPane().add(pboutons_, BorderLayout.SOUTH);
    // FD:temporaire
    final long[] instantLong = loi_.instantsGeneres()[0];
    final int[] instantsInt = new int[instantLong.length];
    for (int i = 0; i < instantLong.length; i++) {
      instantsInt[i] = (int) instantLong[i];
    }
    // FD:temporaire
    // SinaviImplementation.SINAVIHEURESDETERMINISTESLISTMODEL.setGenerationDeterministe(loi_.instantsGeneres()[0]);
    SinaviImplementation.SINAVIHEURESDETERMINISTESLISTMODEL.setGenerationDeterministe(new FuVectorint(instantsInt));
    SinaviImplementation.SINAVIHEURESDETERMINISTESLISTMODEL.refresh();
    pack();
    setVisible(true);
  }

  /**
   * si action sur le bouton valider, fermeture de la fen�tre si action sur le menu d�roulant, affichage de la s�rie
   * s�lectionn�e
   * 
   * @param _e
   */
  public void actionPerformed(final ActionEvent _e) {
    if (_e.getSource() == bValider_) {
      try {
        setClosed(true);
      } catch (final Exception _pve) {}
    } else if (_e.getSource() == cbChoixSerie_) {
      // FD:temporaire
      final long[] instantLong = loi_.instantsGeneres()[cbChoixSerie_.getSelectedIndex()];
      final int[] instantsInt = new int[instantLong.length];
      for (int i = 0; i < instantLong.length; i++) {
        instantsInt[i] = (int) instantLong[i];
      }
      // FD:temporaire
      SinaviImplementation.SINAVIHEURESDETERMINISTESLISTMODEL.setGenerationDeterministe(new FuVectorint(instantsInt));
      // SinaviImplementation.SINAVIHEURESDETERMINISTESLISTMODEL.setGenerationDeterministe(new
      // FuVectorint(loi_.instantsGeneres()[cbChoixSerie_.getSelectedIndex()]));
      SinaviImplementation.SINAVIHEURESDETERMINISTESLISTMODEL.refresh();
    }
  }
}
