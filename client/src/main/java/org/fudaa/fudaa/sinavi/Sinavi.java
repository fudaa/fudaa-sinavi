/**
 * @file         Sinavi.java
 * @creation     2001-05-17
 * @modification $Date: 2007-01-19 13:14:36 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.sinavi;

import org.fudaa.fudaa.commun.impl.Fudaa;

/**
 * Permet de lancer l'application cliente de SINAVI.
 * 
 * @version $Revision: 1.8 $ $Date: 2007-01-19 13:14:36 $ by $Author: deniger $
 * @author Aline Marechalle , Franck Lejeune
 */
public class Sinavi {

  /**
   * main de Sinavi class.
   * 
   * @param args The command line arguments
   */
  public static void main(final String[] args) {
    final Fudaa f = new Fudaa();
    f.launch(args, SinaviImplementation.informationsSoftware(), false);
    f.startApp(new SinaviImplementation());
  }
}
