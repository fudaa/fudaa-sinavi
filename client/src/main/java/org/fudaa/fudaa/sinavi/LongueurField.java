/*
 * @file         LongueurField.java
 * @creation     2001-05-17
 * @modification $Date: 2007-04-16 16:35:28 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.sinavi;

import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.util.StringTokenizer;

import javax.swing.JComponent;

import com.memoire.bu.BuCharValidator;
import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuTextField;

import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluLibString;

/**
 * Composant permettant de saisir des longueurs sous diff�rents formats.
 * 
 * @version $Revision: 1.7 $ $Date: 2007-04-16 16:35:28 $ by $Author: deniger $
 * @author Aline Marechalle , Franck Lejeune
 */
public class LongueurField extends JComponent implements FocusListener {

  /**
   * Un validateur de caract�re n'autorisant qu'un seul point et des chiffres.
   */
  final BuCharValidator CHAR_LONG = new BuCharValidator() {

    public boolean isCharValid(char _char) {
      if (_char == '.') {
        String chaine = zoneTexte.getText();
        StringTokenizer token = new StringTokenizer(chaine, CtuluLibString.DOT);
        int nbElts = token.countTokens();
        if ((chaine.endsWith(CtuluLibString.DOT))) {
          return false;
        }
        if (nbElts < 2) {
          return true;
        }
      } else if (Character.isDigit(_char)) {
        return true;
      }
      return false;
    }
  };
  private boolean presenceKilometres, presenceMetres, presenceCentimetres;
  BuTextField zoneTexte = BuTextField.createDoubleField();
  private final BuLabel label = new BuLabel();

  public LongueurField(final boolean k, boolean m, final boolean cm) {
    String textLabel = "";
    // test de compatibilite de l'argument
    if ((k) && (!m) && (cm)) {
      throw new IllegalArgumentException("ERREUR de LongueurField : manque les metres");
    }
    // Flags pour les champs demand�s
    presenceKilometres = k;
    presenceMetres = m;
    presenceCentimetres = cm;
    // ajout d'un validateur a la zone de texte
    zoneTexte.setCharValidator(CHAR_LONG);
    // Layout manager
    final BuGridLayout lodate = new BuGridLayout(2, 5, 5, false, false);
    setLayout(lodate);
    add(zoneTexte);
    add(label);
    zoneTexte.setColumns(6);
    zoneTexte.addFocusListener(this);
    if (presenceKilometres) {
      textLabel = "km";
    } else if (presenceMetres) {
      textLabel = "m";
    } else if (presenceCentimetres) {
      textLabel = "cm";
    }
    label.setText(textLabel.substring(0, textLabel.length()));
    setValue(0);
  }

  /**
   * Permet de lire la valeur de la zone de saisie (en nombre de centimetres).
   * 
   * @return La valeur LongueurField
   */
  public double getLongueurField() {
    if (zoneTexte.getValue() == null) {
      return 0;
    }
    final double val = ((Double) zoneTexte.getValue()).doubleValue();
    if (presenceKilometres) {
      if (presenceMetres) {
        return (val * 1000);
      }
      return (Math.round(val) * 1000);
    }
    if (presenceMetres) {
      if (presenceCentimetres) {
        return val;
      }
      return Math.round(val);
    }
    return (val / 100);
  }

  /**
   * Pour savoir si le composant est actif. Renvoie true si zoneTexte et label sont actifs.
   * 
   * @return La valeur Enabled
   */
  public boolean isEnabled() {
    final boolean flag1 = zoneTexte.isEnabled();
    final boolean flag2 = label.isEnabled();
    return (flag1 & flag2);
  }

  /**
   * Quand on entre dans la zone de texte c'est que l'on entre dans le composant.
   * 
   * @param e
   */
  public void focusGained(final FocusEvent e) {
    zoneTexte.selectAll();
    processFocusEvent(new FocusEvent(this, FocusEvent.FOCUS_GAINED));
  }

  /**
   * Quand on sort de la zone de texte c'est que l'on sort du composant.
   * 
   * @param e
   */
  public void focusLost(final FocusEvent e) {
    processFocusEvent(new FocusEvent(this, FocusEvent.FOCUS_LOST));
  }

  /**
   * Permet de modifier la valeur de la zone de texte.
   * 
   * @param k presence des kilometres.
   * @param m presence des metres.
   * @param cm La nouvelle valeur LongueurField
   */
  public void setLongueurField(final int k, int m, final int cm) {
    if ((cm >= 0) && (m >= 0) && (k >= 0)) {
      m = cm / 100 + m + k * 1000;
      setLongueurField(m);
    } else {
      setLongueurField(-1);
    }
  }

  /**
   * Affecte la valeur Value de LongueurField object.
   * 
   * @param k La nouvelle valeur Value
   * @param m La nouvelle valeur Value
   * @param cm La nouvelle valeur Value
   */
  public void setValue(final int k, final int m, final int cm) {
    setLongueurField(k, m, cm);
  }

  /**
   * Permet de modifier la valeur de la zone de texte.
   * 
   * @param val : nouvelle valeur, en metres.
   */
  public void setLongueurField(final double val) {
    if (presenceKilometres) {
      zoneTexte.setValue(CtuluLib.getDouble(val / 1000));
    } else if (presenceMetres) {
      zoneTexte.setValue(CtuluLib.getDouble(val));
    } else {
      zoneTexte.setValue(CtuluLib.getDouble(val * 100));
    }
  }

  /**
   * Idem setDureeField(int val).
   * 
   * @param val La nouvelle valeur Value
   */
  public void setValue(final double val) {
    setLongueurField(val);
  }

  /**
   * Pour activer ou desactiver le composant.
   * 
   * @param flag La nouvelle valeur Enabled
   */
  public void setEnabled(final boolean flag) {
    zoneTexte.setEnabled(flag);
    label.setEnabled(flag);
  }
}
