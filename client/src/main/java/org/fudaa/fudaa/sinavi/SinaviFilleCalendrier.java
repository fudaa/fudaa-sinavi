/*
 * @file         SinaviFilleCalendrier.java
 * @creation     2001-05-17
 * @modification $Date: 2006-09-19 15:09:00 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.sinavi;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JComponent;
import javax.swing.border.EmptyBorder;

import com.toedter.calendar.JCalendar;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuInternalFrame;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuTextField;

/**
 * impl�mentation d'une fen�tre interne affichant un calendrier pour le choix de date
 * 
 * @version $Revision: 1.6 $ $Date: 2006-09-19 15:09:00 $ by $Author: deniger $
 * @author Aline Marechalle , Franck Lejeune
 */
public class SinaviFilleCalendrier extends BuInternalFrame implements ActionListener {

  /**
   * composant calendrier
   */
  private final JCalendar calendrier_ = new JCalendar();
  /**
   * bouton valider
   */
  private final BuButton bValider_ = new BuButton(SinaviResource.SINAVI.getIcon("VALIDER"), "Valider");
  /**
   * bouton annuler
   */
  private final BuButton bAnnuler_ = new BuButton(SinaviResource.SINAVI.getIcon("ANNULER"), "Annuler");
  /**
   * panel contenant les boutons, il est plac� en bas � droite
   */
  private final BuPanel pBoutons_ = new BuPanel();
  /**
   * panel contenant les diff�rents champs de la fen�tre
   */
  private final BuPanel pDonnees_ = new BuPanel();
  /**
   * champs de texte � remplir par une date du calendrier
   */
  private BuTextField textField_;

  /**
   * Constructeur qui prend en argument le champs de texte � remplir avec une date du calendrier
   * 
   * @param _textField le champs de texte � remplir
   */
  public SinaviFilleCalendrier(final BuTextField _textField) {
    super("Calendrier", false, false, false, false);
    textField_ = _textField;
    bValider_.addActionListener(this);
    bAnnuler_.addActionListener(this);
    ((JComponent) getContentPane()).setBorder(new EmptyBorder(25, 25, 25, 25));
    getContentPane().setLayout(new BorderLayout());
    pDonnees_.add(calendrier_);
    pBoutons_.setLayout(new FlowLayout(FlowLayout.RIGHT));
    pBoutons_.add(bAnnuler_);
    pBoutons_.add(bValider_);
    getContentPane().add(pDonnees_, BorderLayout.CENTER);
    getContentPane().add(pBoutons_, BorderLayout.SOUTH);
    pack();
    setVisible(true);
  }

  /**
   * si action sur le bouton valider, sauvegarde de la date choisie dans le champs de texte puis fermeture de la fen�tre
   * si action sur le bouton annuler, fermeture de la fen�tre
   * 
   * @param _e
   */
  public void actionPerformed(final ActionEvent _e) {
    if (_e.getSource() == bValider_) {
      textField_.setValue(calendrier_.getCalendar().getTime());
      try {
        setClosed(true);
      } catch (final Exception _pve) {}
    } else {
      textField_.setText(new Long(0).toString());
      try {
        setClosed(true);
      } catch (final Exception _pve) {}
    }
  }
}
