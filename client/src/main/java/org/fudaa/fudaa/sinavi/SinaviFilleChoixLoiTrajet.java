/*
 * @file         SinaviFilleChoixLoiTrajet.java
 * @creation     2001-05-17
 * @modification $Date: 2006-09-19 15:09:00 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.sinavi;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JComponent;
import javax.swing.border.EmptyBorder;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuCommonImplementation;
import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuInternalFrame;

import org.fudaa.dodico.corba.navigation.ITrajetFluvial;

/**
 * impl�mentation d'une fen�tre interne affichant deux boutons pour choisir le type de loi
 * 
 * @version $Revision: 1.8 $ $Date: 2006-09-19 15:09:00 $ by $Author: deniger $
 * @author Aline Marechalle , Franck Lejeune
 */
public class SinaviFilleChoixLoiTrajet extends BuInternalFrame implements ActionListener {

  /**
   * bouton pour choisir une loi deterministe
   */
  private final BuButton bLoiDeterministe_ = new BuButton(SinaviResource.SINAVI.getIcon("VALIDER"), "Loi D�terministe");
  /**
   * bouton pour choisir une loi aleatoire
   */
  private final BuButton bLoiAleatoire_ = new BuButton(SinaviResource.SINAVI.getIcon("VALIDER"), "Loi Aleatoire");
  /**
   * trajet fluvial pour lequel on d�finit la loi ( al�atoire ou d�terministe )
   */
  private ITrajetFluvial trajet_;
  /**
   * une instance de SinaviImplementation
   */
  private BuCommonImplementation app_;

  /**
   * Constructeur
   * 
   * @param _app une instance de SinaviImplementation
   * @param _trajet le trajet fluvial
   */
  public SinaviFilleChoixLoiTrajet(final BuCommonImplementation _app, final ITrajetFluvial _trajet) {
    super("Choix du type de loi", false, false, false, false);
    app_ = _app;
    trajet_ = _trajet;
    // Ajout des listener aux boutons
    bLoiDeterministe_.addActionListener(this);
    bLoiAleatoire_.addActionListener(this);
    // remplissage de la fenetre
    ((JComponent) getContentPane()).setBorder(new EmptyBorder(25, 25, 25, 25));
    getContentPane().setLayout(new BuGridLayout());
    getContentPane().add(bLoiDeterministe_);
    getContentPane().add(bLoiAleatoire_);
    pack();
    setVisible(true);
  }

  /**
   * si action sur le bouton loi d�terministe affichage d'une fen�tre interne pour renseigner la loi d�terministe si
   * action sur le bouton loi al�atoire affichage d'une fen�tre interne pour renseigner la loi al�atoire
   * 
   * @param _e
   */
  public void actionPerformed(final ActionEvent _e) {
    if (_e.getSource() == bLoiDeterministe_) {
      app_.addInternalFrame(new SinaviFilleLoiDeterministeTrajet(trajet_, null));
      try {
        setClosed(true);
      } catch (final Exception _pve) {}
    } else {
      app_.addInternalFrame(new SinaviFilleLoiAleatoireTrajet(app_, trajet_, null));
      try {
        setClosed(true);
      } catch (final Exception _pve) {}
    }
  }
}
