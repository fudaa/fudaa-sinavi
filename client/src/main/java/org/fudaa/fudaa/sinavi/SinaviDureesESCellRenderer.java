/*
 * @file         SinaviDureesESCellRenderer.java
 * @creation     2001-05-17
 * @modification $Date: 2006-09-19 15:08:59 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.sinavi;

import java.awt.Component;

import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;

import org.fudaa.dodico.corba.navigation.INavireTypeHelper;

/**
 * impl�mentation du rendu de la premi�re colonne de la table des dur�es d'entr�es/sorties sp�cifiques � chaque bateau
 * 
 * @version $Revision: 1.7 $ $Date: 2006-09-19 15:08:59 $ by $Author: deniger $
 * @author Aline Marechalle , Franck Lejeune
 */
public class SinaviDureesESCellRenderer extends DefaultTableCellRenderer implements TableCellRenderer {

  /**
   * D�finit le rendu de la premi�re colonne de la table des dur�es d'entr�es/sorties sp�cifiques
   */
  public SinaviDureesESCellRenderer() {}

  /**
   * retourne le type de bateau � partir de l'objet NavireType
   * 
   * @param table la table � laquelle s'applique ce rendu
   * @param value l'objet NavireType
   * @param isSelected ...
   * @param hasFocus ...
   * @param row la ligne
   * @param column la colonne
   * @return le type du navire
   */
  public Component getTableCellRendererComponent(final JTable table, final Object value, final boolean isSelected,
      final boolean hasFocus, final int row, final int column) {
    setBackground(isSelected ? table.getSelectionBackground() : table.getBackground());
    setForeground(table.getForeground());
    setText(INavireTypeHelper.narrow((org.omg.CORBA.Object) value).type());
    return this;
  }
}
