/*
 * @creation     2001-05-17
 * @modification $Date: 2006-09-19 15:09:00 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.sinavi;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Calendar;
import java.util.Date;
import java.util.StringTokenizer;

import javax.swing.border.EmptyBorder;

import com.memoire.bu.BuAssistant;
import com.memoire.bu.BuButton;
import com.memoire.bu.BuCommonImplementation;
import com.memoire.bu.BuCommonInterface;
import com.memoire.bu.BuDialogMessage;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuTextField;

/**
 * impl�mentation de l'onglet "Donn�es g�n�rales" pour les param�tres de sinavi
 * 
 * @version $Revision: 1.6 $ $Date: 2006-09-19 15:09:00 $ by $Author: deniger $
 * @author Aline Marechalle , Franck Lejeune
 */
public class SinaviDonneesGeneralesParametres extends BuPanel implements ActionListener {

  /**
   * nombre de graine maximum sur lesquelles une moyenne sera faite
   */
  private final int nbMaxGraine_ = 6;
  /**
   * nom de l'�tude
   */
  private final BuTextField tfNomEtude_ = new BuTextField();
  /**
   * nom de la simulation
   */
  private final BuTextField tfNomSimulation_ = new BuTextField();
  /**
   * bouton de choix de la date de d�but de la simulation, son action fait apparaitre une fenetre avec un calendrier
   */
  private final BuButton bChoixDateDebut_ = new BuButton(SinaviResource.SINAVI.getIcon("VALIDER"),
      "D�but de la Simulation");
  /**
   * bouton de choix de la date de fin de la simulation, son action fait apparaitre une fenetre avec un calendrier
   */
  private final BuButton bChoixDateFin_ = new BuButton(SinaviResource.SINAVI.getIcon("VALIDER"), "Fin de la Simulation");
  /**
   * champ de texte affichant la date de d�but de la simulation
   */
  private final BuTextField tfDateDebut_ = BuTextField.createDateField();
  /**
   * champ de texte affichant la date de fin de la simulation
   */
  private final BuTextField tfDateFin_ = BuTextField.createDateField();
  /**
   * nombre de graine utilis� pour la simulation
   */
  private final BuTextField tfnbGraine_ = BuTextField.createIntegerField();
  /**
   * champ de texte affichant le nombre de jour d'�tablissement
   */
  private final DureeField tfNombreJourEtablissement_ = new DureeField(false, true, false, false, false);
  /**
   * fen�tre fille pour afficher un calendrier
   */
  private SinaviFilleCalendrier sfc_;
  /**
   * instance de SinaviImplementation
   */
  private BuCommonImplementation app_;
  /**
   * bouton de validation des parametres de l'onglet
   */
  private final BuButton bValider_ = new BuButton(SinaviResource.SINAVI.getIcon("VALIDER"), "Valider");
  /**
   * panel contenant le bouton valider, il est plac� en bas � droite
   */
  private final BuPanel pBoutons_ = new BuPanel();
  /**
   * panel contenant les diff�rents champs de l'onglet
   */
  private final BuPanel pDonnees_ = new BuPanel();
  /**
   * panel contenant le champs de texte message
   */
  private final BuPanel pMessage_ = new BuPanel();
  /**
   * panel contenant les panels boutons et message
   */
  private final BuPanel pSud_ = new BuPanel();
  /**
   * champ de texte pour afficher des messages
   */
  private final BuTextField tfMessage_ = new BuTextField();

  /**
   * Constructeur. _appli doit etre instance de SinaviImplementation
   * 
   * @param _appli
   */
  public SinaviDonneesGeneralesParametres(final BuCommonInterface _appli) {
    super();
    sfc_ = null;
    app_ = _appli.getImplementation();
    // ajout des listeners aux boutons
    bChoixDateDebut_.addActionListener(this);
    bChoixDateFin_.addActionListener(this);
    bValider_.addActionListener(this);
    // taille fixee des boutons avec celle du bouton le plus grand
    bChoixDateFin_.setPreferredSize(bChoixDateDebut_.getPreferredSize());
    // Largeur des zones de texte
    final Dimension dimensionTexte_ = new Dimension(200, 27);
    tfNomEtude_.setPreferredSize(dimensionTexte_);
    tfNomSimulation_.setPreferredSize(dimensionTexte_);
    tfnbGraine_.setPreferredSize(new Dimension(40, 27));
    tfnbGraine_.setValue(new Integer(6));
    tfDateDebut_.setPreferredSize(dimensionTexte_);
    tfDateDebut_.setEnabled(false);
    tfDateFin_.setEnabled(false);
    tfDateFin_.setPreferredSize(dimensionTexte_);
    tfDateDebut_.setDisplayFormat(new java.text.SimpleDateFormat("EEEE dd MMMM yyyy"));
    tfDateFin_.setDisplayFormat(new java.text.SimpleDateFormat("EEEE dd MMMM yyyy"));
    tfMessage_.setColumns(15);
    tfMessage_.setEditable(false);
    tfMessage_.setForeground(Color.red);
    tfMessage_.setText("Param�tres non enregistr�s");
    // Cr�ation du layout manager et des contraintes du panel donn�es
    final GridBagLayout lm_ = new GridBagLayout();
    final GridBagConstraints c_ = new GridBagConstraints();
    // Cr�ation du layout manager et du bord de l'onglet
    setLayout(new BorderLayout());
    setBorder(new EmptyBorder(0, 0, 0, 0));
    // layout des panels
    pDonnees_.setLayout(lm_);
    pMessage_.setLayout(new FlowLayout(FlowLayout.LEFT));
    pBoutons_.setLayout(new FlowLayout(FlowLayout.RIGHT));
    pSud_.setLayout(new BorderLayout());
    // lecture des parametres dans ETUDE_SINAVI si ouverture d'un fichier
    if (!(SinaviImplementation.ETUDE_SINAVI.nom().equals(""))) {
      tfMessage_.setForeground(Color.green);
      tfMessage_.setText("Param�tres enregistr�s");
      final StringTokenizer token = new StringTokenizer(SinaviImplementation.ETUDE_SINAVI.nom(), ":");
      tfNomEtude_.setText(token.nextToken());
      tfNomSimulation_.setText(token.nextToken());
      tfnbGraine_.setValue(new Integer(SinaviImplementation.ETUDE_SINAVI.nombreSeries()));
      tfDateDebut_.setValue(new Date(SinaviImplementation.ETUDE_SINAVI.dateDebutSimulation() * 1000l));
      tfDateFin_.setValue(new Date(SinaviImplementation.ETUDE_SINAVI.dateFinSimulation() * 1000l));
      final int debut = (int) SinaviImplementation.ETUDE_SINAVI.dateDebutSimulation();
      final int debutreel = (int) SinaviImplementation.ETUDE_SINAVI.dateDebutReelSimulation();
      tfNombreJourEtablissement_.setDureeField(debutreel - debut);
    }
    // Contraintes communes � tous les composants du panel donn�es
    c_.gridy = GridBagConstraints.RELATIVE;
    c_.anchor = GridBagConstraints.WEST;
    c_.ipadx = 0;
    c_.ipady = 0;
    c_.gridheight = 1; // 1 ligne pour les �tiquettes.
    // Remplissage de la premiere colonne
    c_.gridx = 0; // premiere colonne
    placeComposant(lm_, new BuLabel("Nom de l'etude : "), c_);
    c_.insets.top = 10;
    placeComposant(lm_, new BuLabel("Nom de la simulation :"), c_);
    placeComposant(lm_, new BuLabel("Nombre de Graine :"), c_);
    placeComposant(lm_, bChoixDateDebut_, c_);
    placeComposant(lm_, bChoixDateFin_, c_);
    placeComposant(lm_, new BuLabel("Nombre de Jours d'�tablissement"), c_);
    // Remplissage de la deuxieme colonne
    c_.gridx = 1; // deuxieme colonne
    c_.insets.left = 20;
    c_.insets.top = 0;
    placeComposant(lm_, tfNomEtude_, c_);
    c_.insets.top = 10;
    placeComposant(lm_, tfNomSimulation_, c_);
    placeComposant(lm_, tfnbGraine_, c_);
    placeComposant(lm_, tfDateDebut_, c_);
    placeComposant(lm_, tfDateFin_, c_);
    placeComposant(lm_, tfNombreJourEtablissement_, c_);
    // ajout du bouton valider au panel boutons
    pBoutons_.add(bValider_);
    // ajout du message au panel message
    pMessage_.add(tfMessage_);
    // ajout de message et boutons a sud
    pSud_.add(pMessage_, BorderLayout.WEST);
    pSud_.add(pBoutons_, BorderLayout.EAST);
    // ajout des deux panels � l'onglet
    add(pDonnees_, BorderLayout.CENTER);
    add(pSud_, BorderLayout.SOUTH);
  }

  /**
   * sauvegarde les donnees g�n�rales
   */
  public void setDonneesGeneralesParametres() {
    if (validDonneesGeneralesParametres()) {
      tfMessage_.setForeground(Color.green);
      tfMessage_.setText("Param�tres enregistr�s");
      SinaviImplementation.ETUDE_SINAVI.nom(tfNomEtude_.getText() + ":" + tfNomSimulation_.getText());
      SinaviImplementation.ETUDE_SINAVI.nombreSeries(((Integer) tfnbGraine_.getValue()).intValue());
      SinaviImplementation.ETUDE_SINAVI.definitDebutFinSimulation(
          ((int) (((Date) tfDateDebut_.getValue()).getTime() / 1000l)), ((int) (((Date) tfDateFin_.getValue())
              .getTime() / 1000l)));
      final Calendar calendar = Calendar.getInstance();
      calendar.setTime((Date) tfDateDebut_.getValue());
      calendar.add(Calendar.DATE, (tfNombreJourEtablissement_.getDureeField() / 86400));
      SinaviImplementation.ETUDE_SINAVI.dateDebutReelSimulation(((int) (calendar.getTime().getTime() / 1000l)));
    } else {
      final BuDialogMessage dialog_mess = new BuDialogMessage(app_, SinaviImplementation.informationsSoftware(),
          "Donn�es g�n�rales incompl�tes");
      dialog_mess.activate();
      SinaviImplementation.assistant_.changeAttitude(BuAssistant.PAROLE, "Donn�es G�n�rales\nincompl�tes");
      // System.out.println("Donn�es G�n�rales incompl�tes");
    }
  }

  /**
   * M�thode utile pour placer un composant avec ses contraintes dans le panel "donnees"
   * 
   * @param _lm le layout manager
   * @param _composant le composant a placer
   * @param _c les contraintes associees au composant
   */
  public void placeComposant(final GridBagLayout _lm, final Component _composant, final GridBagConstraints _c) {
    _lm.setConstraints(_composant, _c);
    pDonnees_.add(_composant);
  }

  /**
   * si action sur le bouton choix date d�but, s�lection de cette date dans un calendrier inclu dans une fen�tre fille
   * si action sur le bouton choix date fin, s�lection de cette date dans un calendrier inclu dans une fen�tre fille si
   * action sur le bouton valider, sauvegarde des parametres de l'onglet
   * 
   * @param _e
   */
  public void actionPerformed(final ActionEvent _e) {
    if (_e.getSource() == bChoixDateDebut_) {
      if (sfc_ == null) {
        sfc_ = new SinaviFilleCalendrier(tfDateDebut_);
        app_.addInternalFrame(sfc_);
      } else {
        sfc_.setVisible(false);
        sfc_ = new SinaviFilleCalendrier(tfDateDebut_);
        app_.addInternalFrame(sfc_);
      }
    } else if (_e.getSource() == bChoixDateFin_) {
      if (sfc_ == null) {
        sfc_ = new SinaviFilleCalendrier(tfDateFin_);
        app_.addInternalFrame(sfc_);
      } else {
        sfc_.setVisible(false);
        sfc_ = new SinaviFilleCalendrier(tfDateFin_);
        app_.addInternalFrame(sfc_);
      }
    } else if (_e.getSource() == bValider_) {
      setDonneesGeneralesParametres();
    }
  }

  /**
   * test de tous les champs du bateau
   * 
   * @return true si tous les champs sont corrects
   */
  private boolean validDonneesGeneralesParametres() {
    if ((tfNomEtude_.getText().equals(null)) || (tfNomEtude_.getText().equals(""))) {
      return false;
    }
    if ((tfNomSimulation_.getText().equals(null)) || (tfNomSimulation_.getText().equals(""))) {
      return false;
    }
    if (((Integer) tfnbGraine_.getValue()).intValue() == 0) {
      return false;
    }
    if (((Integer) tfnbGraine_.getValue()).intValue() > nbMaxGraine_) {
      tfnbGraine_.setValue(new Integer(0));
      return false;
    }
    if (tfDateDebut_.getValue() == null) {
      return false;
    }
    if (tfDateFin_.getValue() == null) {
      return false;
    }
    return true;
  }
}
