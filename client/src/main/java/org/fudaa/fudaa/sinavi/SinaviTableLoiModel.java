/*
 * @file         SinaviTableLoiModel.java
 * @creation     2001-05-17
 * @modification $Date: 2006-09-19 15:09:00 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.sinavi;

import javax.swing.table.AbstractTableModel;

import org.fudaa.dodico.corba.navigation.IGenerationHelper;
import org.fudaa.dodico.corba.navigation.IGenerationJournaliereAleatoireHelper;
import org.fudaa.dodico.corba.navigation.ITrajetFluvial;

/**
 * D�finit le mod�le pour la table affichant les cr�neaux horaires
 * 
 * @version $Revision: 1.6 $ $Date: 2006-09-19 15:09:00 $ by $Author: deniger $
 * @author Aline Marechalle , Franck Lejeune
 */
public class SinaviTableLoiModel extends AbstractTableModel {

  /**
   * noms des colonnes
   */
  private final String[] nomsColonnes_ = { "Creneau Horaire", "Type de loi" };
  /**
   * objet m�tier trajet fluvial auquel on va faire r�f�rence pour trouver les lois
   */
  private ITrajetFluvial trajetFluvial_;

  /**
   * Constructeur SinaviTableCreneauModel.
   */
  public SinaviTableLoiModel() {}

  /**
   * retourne le nombre de colonne
   * 
   * @return le nombre de colonne
   */
  public int getColumnCount() {
    return nomsColonnes_.length;
  }

  /**
   * retourne le nombre de ligne donc le nombre de loi
   * 
   * @return le nombre de loi
   */
  public int getRowCount() {
    if (trajetFluvial_ == null) {
      return 0;
    }
    return trajetFluvial_.generations().length;
  }

  /**
   * retourne le nom de la colonne
   * 
   * @param _col le numero de la colonne
   * @return le nom de la colonne
   */
  public String getColumnName(final int _col) {
    return nomsColonnes_[_col];
  }

  /**
   * retourne la valeur situ�e � la ligne row et la colonne col
   * 
   * @param _row la ligne
   * @param _col la colonne
   * @return la valeur situ�e � la ligne row et la colonne col
   */
  public Object getValueAt(final int _row, final int _col) {
    if (trajetFluvial_ == null) {
      return null;
    } else if (_col == 0) {
      return IGenerationHelper.narrow(trajetFluvial_.generations()[_row]);
    } else {
      if (trajetFluvial_.generations()[_row].typeEnChaine().equals("Loi d'Erlang")) {
        return trajetFluvial_.generations()[_row].typeEnChaine() + " "
            + (IGenerationJournaliereAleatoireHelper.narrow(trajetFluvial_.generations()[_row]).ordreLoi());
      }
      return trajetFluvial_.generations()[_row].typeEnChaine();
    }
  }

  /**
   * permet de rafraichir l'affichage de la table ( modification ou ajout d'une loi )
   */
  public void refresh() {
    fireTableDataChanged();
  }

  /**
   * d�finit le trajet auquel on va faire r�f�rence pour trouver les lois
   * 
   * @param _trajetFluvial le trajet
   */
  public void setTrajetFluvial(final ITrajetFluvial _trajetFluvial) {
    trajetFluvial_ = _trajetFluvial;
    refresh();
  }
}
