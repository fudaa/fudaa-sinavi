/*
 * @file         SinaviPreferences.java
 * @creation     
 * @modification $Date: 2006-09-19 15:09:00 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.sinavi;

import com.memoire.bu.BuPreferences;

/**
 * Préférences de Sinavi
 * 
 * @version $Revision: 1.6 $ $Date: 2006-09-19 15:09:00 $ by $Author: deniger $
 * @author Aline Marechalle , Franck Lejeune
 */
public class SinaviPreferences extends BuPreferences {

  public final static SinaviPreferences SINAVIPREFS = new SinaviPreferences();
}
