/*
 * @file         DureeField.java
 * @creation     2001-05-17
 * @modification $Date: 2006-09-19 15:09:00 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.sinavi;

import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.util.StringTokenizer;

import javax.swing.JComponent;

import com.memoire.bu.BuCharValidator;
import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuStringValidator;
import com.memoire.bu.BuTextField;
import com.memoire.bu.BuValueValidator;

/**
 * Composant permettant de saisir des dur�es sous diff�rents formats. Le format des donn�es est controle par des
 * validateur. Principe de convertion automatique (chaine vers nombre de minutes et vise versa) :<br>
 * 60 minutes : 1 heure <br>
 * 24 heures : 1 jour <br>
 * 31 jours : 1 mois<br>
 * 31 :nombre de jours de janvier<br>
 * 31+28 jours :2 mois ( = nombre de jours de janv. + fevrier ).
 * 
 * @version $Revision: 1.6 $ $Date: 2006-09-19 15:09:00 $ by $Author: deniger $
 * @author Aline Marechalle , Franck Lejeune
 */
public class DureeField extends JComponent implements FocusListener {

  /**
   * Caract�res valides pour les dur�es : num�ro ou deux points.
   */
  final BuCharValidator CHAR_DUREE = new BuCharValidator() {

    /**
     * retourne la valeur CharValid de DureeField object
     * 
     * @param _char
     * @return La valeur CharValid
     */
    public boolean isCharValid(char _char) {
      if (_char == ':') {
        String chaine = zoneTexte.getText();
        StringTokenizer token = new StringTokenizer(chaine, ":");
        int nbElts = token.countTokens();
        if (nbElts < nbEltsMax) {
          return true;
        }
      } else if (Character.isDigit(_char)) {
        return true;
      }
      return false;
    }
  };
  /**
   * ....
   */
  final BuValueValidator VALUE_DUREE = new BuValueValidator() {

    /**
     * retourne la valeur ValueValid de DureeField object
     * 
     * @param _value
     * @return La valeur ValueValid
     */
    public boolean isValueValid(Object _value) {
      // Seules les instances de Integer sont accept�es
      if (!(_value instanceof Integer)) {
        return false;
      }
      int val = ((Integer) _value).intValue();
      if (val < 0) {
        return false;
      }
      // Erreur s'il reste des secondes et qu'elles ne sont pas demand�es
      if (presenceMinutes && !presenceSecondes && (val % 60) != 0) {
        return false;
      }
      val = val / 60; // val converti en minutes
      // Erreur s'il reste des minutes et qu'elles ne sont pas demand�es
      if (presenceHeures && !presenceMinutes && (val % 60) != 0) {
        return false;
      }
      val = val / 60; // val converti en heures
      // Erreur s'il reste des heures et qu'elles ne sont pas demand�es
      if (presenceJours && !presenceHeures && (val % 24) != 0) {
        return false;
      }
      val = val / 24; // val converti en jours
      // Si les jours ne sont pas demand�es
      if (presenceMois && !presenceJours) {
        int m = 0;
        while (val >= (calendrier[m % 12]) * 1440) {
          val = val - (calendrier[m % 12]) * 1440;
          m++;
        }
        // Erreur s'il reste des jours et qu'ils ne sont pas demand�s
        if (val != 0) {
          return false;
        }
      }
      return true;
    }
  };
  /**
   * ....
   */
  final BuStringValidator STRING_DUREE = new BuStringValidator() {

    /**
     * retourne la valeur StringValid de DureeField object
     * 
     * @param _string
     * @return La valeur StringValid
     */
    public boolean isStringValid(String _string) {
      StringTokenizer token = new StringTokenizer(_string, ":");
      if (token.countTokens() > nbEltsMax) {
        return false;
      }
      return true;
    }

    /**
     * Transforme le nombre de secondes donn� en param�tre en champ affichable. Les donn�es sont suppos�es accept�es par
     * isValueValid.
     * 
     * @param value
     */
    public String valueToString(Object value) {
      int secondes = ((Integer) value).intValue();
      String nbMois = "";
      String nbJours = "";
      String nbHeures = "";
      String nbMinutes = "";
      String nbSecondes = "";
      // Calcule du nombre de mois s'ils sont demand�s
      if (presenceMois) {
        int m = 0;
        while (secondes >= (calendrier[m % 12]) * 86400) {
          secondes -= (calendrier[m % 12]) * 86400;
          m++;
        }
        nbMois = (m < 10 ? "0" + String.valueOf(m) : String.valueOf(m));
      }
      // Calcule le nombre de jours s'ils sont demand�s
      if (presenceJours) {
        int jours = secondes / 86400;
        secondes -= jours * 86400;
        if (presenceMois) {
          nbJours = ":";
        }
        nbJours += (jours < 10 ? "0" + String.valueOf(jours) : String.valueOf(jours));
      }
      // Calcule le nombre d'heures si elles sont demand�es
      if (presenceHeures) {
        int heures = secondes / 3600;
        secondes -= heures * 3600;
        if (presenceJours) {
          nbHeures = ":";
        }
        nbHeures += (heures < 10 ? "0" + String.valueOf(heures) : String.valueOf(heures));
      }
      // Calcule le nombre de minutes si elles sont demand�es
      if (presenceMinutes) {
        int minutes = secondes / 60;
        secondes -= minutes * 60;
        if (presenceHeures) {
          nbMinutes = ":";
        }
        nbMinutes += (minutes < 10 ? "0" + String.valueOf(minutes) : String.valueOf(minutes));
      }
      // Affecte les secondes si elles sont demand�es
      if (presenceSecondes) {
        if (presenceMinutes) {
          nbSecondes = ":";
        }
        nbSecondes += (secondes < 10 ? "0" + String.valueOf(secondes) : String.valueOf(secondes));
      }
      return nbMois + nbJours + nbHeures + nbMinutes + nbSecondes;
    }

    /**
     * Calcule le nombre de secondes correspondant au texte donn�. Chaine est suppos� accept� par isStringValid.
     * principe de convertion automatique <br>
     * 60 secondes :1 minute<br>
     * 60 minutes :1 heure<br>
     * 24 heures :1 jour<br>
     * 31 jours:1 mois<br>
     * 31+28 jours :2 mois ( = nombre de jours de janv. + fevrier )
     * 
     * @param chaine
     */
    public Object stringToValue(String chaine) {
      StringTokenizer token = new StringTokenizer(chaine, ":");
      // Nb d'�l�ments de la chaine separes par ":"
      int nbEltDonnes = token.countTokens();
      int nbEltsAttendus = nbEltsMax;
      int sec = 0;
      if (presenceMois && nbEltDonnes == nbEltsAttendus--) {
        int tempo = Integer.parseInt(token.nextToken());
        for (int i = 0; i < tempo; i++) {
          sec += +86400 * calendrier[i % 12];
        }
      }
      if (presenceJours && nbEltDonnes >= nbEltsAttendus--) {
        sec += 86400 * Integer.parseInt(token.nextToken());
      }
      if (presenceHeures && nbEltDonnes >= nbEltsAttendus--) {
        sec += 3600 * Integer.parseInt(token.nextToken());
      }
      if (presenceMinutes && nbEltDonnes >= nbEltsAttendus--) {
        sec += 60 * Integer.parseInt(token.nextToken());
      }
      if (presenceSecondes && nbEltDonnes >= nbEltsAttendus--) {
        sec += Integer.parseInt(token.nextToken());
      }
      return new Integer(sec);
    }
  };
  /**
   * ....
   */
  static int[] calendrier = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
  /**
   * ....
   */
  boolean presenceMois;
  boolean presenceJours;
  boolean presenceHeures;
  boolean presenceSecondes;
  boolean presenceMinutes;
  /**
   * ....
   */
  BuTextField zoneTexte = new BuTextField();
  /**
   * ....
   */
  private final BuLabel label = new BuLabel();
  /**
   * Nb maxi d'�l�ments s�par�s par des ":"
   */
  int nbEltsMax = 0;

  /**
   * Cr�ation d'une zone de texte. Le format des donn�es saisies devra respecter les champs demand�s. ATTENTION : restez
   * coherant si vous creez les mois et les heures, creez aussi les jours.
   * 
   * @param m presence des mois.
   * @param j presence des jours.
   * @param h presence des heures.
   * @param min presence des minutes.
   * @param sec
   */
  public DureeField(final boolean m, boolean j, boolean h, boolean min, final boolean sec) {
    String textLabel = "";
    // test de compatibilite de l'argument
    if ((m) && (!j) && (h)) {
      throw new IllegalArgumentException("ERREUR de DureeField : manque les jours");
    }
    if ((j) && (!h) && (min)) {
      throw new IllegalArgumentException("ERREUR de DureeField : manque les heures");
    }
    if ((h) && (!min) && (sec)) {
      throw new IllegalArgumentException("ERREUR de DureeField : manque les minutes");
    }
    if ((m) && (!j) && (!h) && (min)) {
      throw new IllegalArgumentException("ERREUR de DureeField : manque les jours et les heures");
    }
    if ((m) && (!j) && (!h) && (!min) && (sec)) {
      throw new IllegalArgumentException("ERREUR de DureeField : manque les jours, les heures et les minutes");
    }
    if ((j) && (!h) && (!min) && (sec)) {
      throw new IllegalArgumentException("ERREUR de DureeField : manque les heures et les minutes");
    }
    // Flags pour les champs demand�s
    presenceMois = m;
    presenceJours = j;
    presenceHeures = h;
    presenceMinutes = min;
    presenceSecondes = sec;
    // Validators pour les dur�e
    zoneTexte.setCharValidator(CHAR_DUREE);
    zoneTexte.setValueValidator(VALUE_DUREE);
    zoneTexte.setStringValidator(STRING_DUREE);
    // Layout manager
    final BuGridLayout lodate = new BuGridLayout(2, 5, 5, false, false);
    setLayout(lodate);
    add(zoneTexte);
    add(label);
    zoneTexte.setColumns(6);
    zoneTexte.addFocusListener(this);
    if (presenceMois) {
      textLabel += " mois :";
      nbEltsMax++;
    }
    if (presenceJours) {
      textLabel += " jours :";
      nbEltsMax++;
    }
    if (presenceHeures) {
      textLabel += " h :";
      nbEltsMax++;
    }
    if (presenceMinutes) {
      textLabel += " min :";
      nbEltsMax++;
    }
    if (presenceSecondes) {
      textLabel += " sec:";
      nbEltsMax++;
    }
    label.setText(textLabel.substring(0, textLabel.length() - 1));
    setValue(0);
  }

  /**
   * Permet de lire la valeur de la zone de saisie (en nombre de minutes).
   * 
   * @return La valeur DureeField
   */
  public int getDureeField() {
    final Integer val = (Integer) zoneTexte.getValue();
    return val.intValue();
  }

  /**
   * Pour savoir si le composant est actif. Renvoie true si zoneTexte et label sont actifs
   * 
   * @return La valeur Enabled
   */
  public boolean isEnabled() {
    final boolean flag1 = zoneTexte.isEnabled();
    final boolean flag2 = label.isEnabled();
    return (flag1 & flag2);
  }

  /**
   * Quand on entre dans la zone de texte c'est que l'on entre dans le composant.
   * 
   * @param e
   */
  public void focusGained(final FocusEvent e) {
    zoneTexte.selectAll();
    processFocusEvent(new FocusEvent(this, FocusEvent.FOCUS_GAINED));
  }

  /**
   * Quand on sort de la zone de texte c'est que l'on sort du composant.
   * 
   * @param e
   */
  public void focusLost(final FocusEvent e) {
    processFocusEvent(new FocusEvent(this, FocusEvent.FOCUS_LOST));
  }

  /**
   * Permet de modifier la valeur de la zone de texte.
   * 
   * @param m nombre de mois.
   * @param j nombre de jours.
   * @param h nombre d' heures.
   * @param min nombre de minutes.
   * @param sec nombre de secondes;
   */
  public void setDureeField(final int m, final int j, final int h, final int min, int sec) {
    if ((sec >= 0) && (min >= 0) && (h >= 0) && (j >= 0) && (m >= 0)) {
      sec = sec + min * 60 + h * 60 + j * 24 * 60;
      for (int i = 0; i < m; i++) {
        sec = sec + 60 * 60 * 24 * calendrier[i % 12];
      }
      setDureeField(sec);
    } else {
      setDureeField(-1);
    }
  }

  /**
   * Idem setDureeField(int m,int j,int h, int min).
   * 
   * @param m La nouvelle valeur Value
   * @param j La nouvelle valeur Value
   * @param h La nouvelle valeur Value
   * @param min La nouvelle valeur Value
   * @param sec La nouvelle valeur Value
   */
  public void setValue(final int m, final int j, final int h, final int min, final int sec) {
    setDureeField(m, j, h, min, sec);
  }

  /**
   * Permet de modifier la valeur de la zone de texte.
   * 
   * @param val : nouvelle valeur, en minutes.
   */
  public void setDureeField(final int val) {
    zoneTexte.setValue(new Integer(val));
  }

  /**
   * Idem setDureeField(int val)
   * 
   * @param val La nouvelle valeur Value
   */
  public void setValue(final int val) {
    setDureeField(val);
  }

  /**
   * Pour activer ou desactiver le composant.
   * 
   * @param flag La nouvelle valeur Enabled
   */
  public void setEnabled(final boolean flag) {
    zoneTexte.setEnabled(flag);
    label.setEnabled(flag);
  }
  /*
   * public static String formatter(boolean pMois, boolean pJours, boolean pHeures, boolean pMinutes,boolean pSecondes,
   * int secondes) { String retour = ""; Calcule du nombre de mois d'ils sont demand�s if ( pMois ) { int m = 0; while
   * (secondes >= (calendrier[m % 12]) * 86400) { secondes -= (calendrier[m % 12]) * 86400; m++; } retour += ( m < 10 ?
   * "0" : "") + String.valueOf( m ) + ":"; } Calcule le nombre de jours s'ils sont demand�s if ( pJours ) { int jours =
   * secondes / 86400; secondes -= jours * 86400; retour += ( jours < 10 ? "0" : "") + String.valueOf(jours) + ":"; }
   * Calcule le nombre d'heures si elles sont demand�es if ( pHeures ) { int heures = secondes / 3600; secondes -=
   * heures * 3600; retour += ( heures < 10 ? "0" : "") + String.valueOf(heures) + ":"; } Calcule le nombre de minutes
   * si elles sont demand�es if ( pHeures ) { int minutes = secondes / 60; secondes -= minutes * 60; retour += ( minutes <
   * 10 ? "0" : "") + String.valueOf(minutes) + ":"; } Affecte les secondes. if ( !pSecondes && secondes>0 ) retour +=
   * (secondes < 10 ? "0" : "") + String.valueOf ( secondes ) + "min:"; else if ( pSecondes ) retour += (secondes < 10 ?
   * "0" : "") + String.valueOf ( secondes ) + ":"; return retour.substring(0, retour.length()-1); }
   */
}
