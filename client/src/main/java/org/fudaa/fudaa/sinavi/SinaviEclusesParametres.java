/*
 * @file         SinaviEclusesParametres.java
 * @creation     2001-05-17
 * @modification $Date: 2006-09-19 15:09:00 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.sinavi;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.ButtonGroup;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.EmptyBorder;

import com.memoire.bu.*;

import org.fudaa.dodico.corba.navigation.IEcluseFluviale;
import org.fudaa.dodico.corba.navigation.IEcluseFluvialeHelper;
import org.fudaa.dodico.corba.navigation.INavireTypeHelper;

/**
 * impl�mentation de l'onglet "Ecluses" pour les param�tres de sinavi
 * 
 * @version $Revision: 1.9 $ $Date: 2006-09-19 15:09:00 $ by $Author: deniger $
 * @author Aline Marechalle , Franck Lejeune
 */
public class SinaviEclusesParametres extends BuPanel implements ActionListener, MouseListener {

  /**
   * nom de l'�cluse
   */
  private final BuTextField tfNomEcluse_ = new BuTextField();
  /**
   * longueur de l'�cluse en m�tre
   */
  private final LongueurField lfTailleEcluseLongueur_ = new LongueurField(false, true, true);
  /**
   * hauteur de chute d'eau de l'�cluse en m�tre
   */
  private final LongueurField lfHauteurChuteEau_ = new LongueurField(false, true, true);
  /**
   * largeur de l'�cluse en m�tre
   */
  private final LongueurField lfTailleEcluseLargeur_ = new LongueurField(false, true, true);
  /**
   * profondeur de l'�cluse ne m�tre
   */
  private final LongueurField lfEcluseProfondeur_ = new LongueurField(false, true, true);
  /**
   * dur�e de fausse bassin�e montante en heure:minute
   */
  private final DureeField dfFausseBassineeMontante_ = new DureeField(false, false, true, true, false);
  /**
   * dur�e de fausse bassin�e avalante en heure:minute
   */
  private final DureeField dfFausseBassineeAvalante_ = new DureeField(false, false, true, true, false);
  /**
   * dur�e moyenne de bassin�e montante en heure:minute
   */
  private final DureeField dfBassineeMontanteMoyen_ = new DureeField(false, false, true, true, false);
  /**
   * dur�e moyenne de bassin�e avalante en heure:minute
   */
  private final DureeField dfBassineeAvalanteMoyen_ = new DureeField(false, false, true, true, false);
  /**
   * temps d'entr�e moyen en heure:minute
   */
  private final DureeField dfTempsEntreeMoyen_ = new DureeField(false, false, true, true, false);
  /**
   * temps de sortie moyen en heure:minute....
   */
  private final DureeField dfTempsSortieMoyen_ = new DureeField(false, false, true, true, false);
  /**
   * groupe de bouton incluant les radio bouton "automatique" et "manuelle"
   */
  private final ButtonGroup bgManuelleAutomatique_ = new ButtonGroup();
  /**
   * bouton radio pour indiquer que l'�cluse est automatique
   */
  private final BuRadioButton rbEcluseAutomatique_ = new BuRadioButton("Automatique");
  /**
   * bouton radio pour indiquer que l'�cluse est manuelle
   */
  private final BuRadioButton rbEcluseManuelle_ = new BuRadioButton("Manuelle");
  /**
   * bouton pour cr�er un cr�neau horaire
   */
  private final BuButton bCreerCreneau_ = new BuButton(SinaviResource.SINAVI.getIcon("VALIDER"), "Cr�er");
  /**
   * bouton pour supprimer un cr�neau horaire
   */
  private final BuButton bSupprimerCreneau_ = new BuButton(SinaviResource.SINAVI.getIcon("ANNULER"), "Supprimer");
  /**
   * panel contenant les deux boutons relatifs aux dur�es de manoeuvres sp�cifiques
   */
  private final BuPanel pCreerAnnuler_ = new BuPanel();
  /**
   * liste des �cluses cr��es
   */
  private final JList lEclusesCreees_ = new JList();
  /**
   * bouton de validation de l'�cluse s�lectionn�e dans la liste
   */
  private final BuButton bValiderEcluse_ = new BuButton(SinaviResource.SINAVI.getIcon("VALIDER"), "Enregistrer");
  /**
   * bouton pour cr�er des dur�es de manoeuvres sp�cifiques � un bateau s�lectionn�
   */
  private final BuButton bCreerDureesManoeuvres_ = new BuButton(SinaviResource.SINAVI.getIcon("VALIDER"),
      "Cr�er des dur�es sp�cifiques pour un type de bateau");
  /**
   * bouton pour supprimer des dur�es de manoeuvres sp�cifiques � un bateau s�lectionn�
   */
  private final BuButton bSupprimerDureesManoeuvres_ = new BuButton(SinaviResource.SINAVI.getIcon("ANNULER"),
      "Supprimer");
  /**
   * ascenseur pour la liste des �cluses cr��es
   */
  private final JScrollPane EclusesCreeesScrollPane_ = new JScrollPane(lEclusesCreees_,
      ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
  /**
   * table affichant les cr�neaux horaires relatifs � l'�cluse s�lectionn�e dans la liste
   */
  private final BuTable tCreneauHoraire_ = new BuTable(SinaviImplementation.SINAVITABLECRENEAUMODEL);
  /**
   * ascenseur pour la table des cr�neaux horaires
   */
  private final JScrollPane CreneauHoraireScrollPane_ = new JScrollPane(tCreneauHoraire_,
      ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
  /**
   * table affichant les dur�es de manoeuvres sp�cifiques relatifs � l'�cluse s�lectionn�e dans la liste
   */
  private final BuTable tDureesESTypeBateau_ = new BuTable(SinaviImplementation.SINAVITABLEDUREEESMODEL);
  /**
   * ascenseur pour la table des dur�es de manoeuvres
   */
  private final JScrollPane DureeESScrollPane_ = new JScrollPane(tDureesESTypeBateau_,
      ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
  /**
   * ecluse courante, s�lectionn�e dans la lsite
   */
  private IEcluseFluviale ecluseCourante_;
  /**
   * instance de SinaviImplementation
   */
  private BuCommonImplementation app_;
  /**
   * panel contenant les boutons, il est plac� en bas � droite
   */
  private final BuPanel pBoutons_ = new BuPanel();
  /**
   * panel contenant les diff�rents champs de l'onglet
   */
  private final BuPanel pDonnees_ = new BuPanel();
  /**
   * panel contenant le champs de texte message
   */
  private final BuPanel pMessage_ = new BuPanel();
  /**
   * panel contenant les panels boutons et message
   */
  private final BuPanel pSud_ = new BuPanel();
  /**
   * champ de texte pour afficher des messages
   */
  private final BuTextField tfMessage_ = new BuTextField();

  /**
   * Constructeur. _appli doit etre instance de SinaviImplementation
   * 
   * @param _appli
   * @param _mode_edition �dition � partir du r�seau
   */
  public SinaviEclusesParametres(final BuCommonInterface _appli, boolean _mode_edition) {
    super();
    app_ = _appli.getImplementation();
    // Cr�ation du layout manager et des contraintes du panel donnees
    final GridBagLayout lm_ = new GridBagLayout();
    final GridBagConstraints c_ = new GridBagConstraints();
    // Cr�ation du layout manager et du bord de l'onglet
    this.setLayout(new BorderLayout());
    this.setBorder(new EmptyBorder(0, 0, 0, 0));
    // layout des panels
    pDonnees_.setLayout(lm_);
    pMessage_.setLayout(new FlowLayout(FlowLayout.LEFT));
    pBoutons_.setLayout(new FlowLayout(FlowLayout.RIGHT));
    pSud_.setLayout(new BorderLayout());
    // Ajout d'un listener aux differents elements
    bCreerCreneau_.addActionListener(this);
    bSupprimerCreneau_.addActionListener(this);
    bValiderEcluse_.addActionListener(this);
    bCreerDureesManoeuvres_.addActionListener(this);
    bSupprimerDureesManoeuvres_.addActionListener(this);
    lEclusesCreees_.addMouseListener(this);
    tCreneauHoraire_.addMouseListener(this);
    tDureesESTypeBateau_.addMouseListener(this);
    // d�finition du panel interne pour les deux boutons "creer" et "annuler" des durees de manoeuvres
    pCreerAnnuler_.add(bCreerDureesManoeuvres_);
    pCreerAnnuler_.add(bSupprimerDureesManoeuvres_);
    // Ajout dans le groupe de bouton des deux boutons radio
    bgManuelleAutomatique_.add(rbEcluseAutomatique_);
    bgManuelleAutomatique_.add(rbEcluseManuelle_);
    // S�lection par d�faut du bouton "Ecluse Manuelle"
    rbEcluseManuelle_.setSelected(true);
    // Bouton "valider" "cr�er" et "Supprimer" pas visible par defaut
    bValiderEcluse_.setEnabled(false);
    bSupprimerCreneau_.setEnabled(false);
    bSupprimerDureesManoeuvres_.setEnabled(false);
    if (!_mode_edition) {
      bCreerCreneau_.setEnabled(false);
      bCreerDureesManoeuvres_.setEnabled(false);
    }
    // Contraintes des listes
    final Dimension dimec_ = new Dimension(150, 155);
    final Dimension dimtch_ = new Dimension(140, 60);
    final Dimension dimbcc_ = new Dimension(140, 33);
    final Dimension dimbsc_ = new Dimension(140, 33);
    // Dimension dimbve_ = new Dimension(150, 33);
    // Dimension dimbvdm_ = new Dimension(120, 33);
    final Dimension dimtdes_ = new Dimension(650, 43);
    EclusesCreeesScrollPane_.setPreferredSize(dimec_);
    CreneauHoraireScrollPane_.setPreferredSize(dimtch_);
    bCreerCreneau_.setPreferredSize(dimbcc_);
    bSupprimerCreneau_.setPreferredSize(dimbsc_);
    lEclusesCreees_.setCellRenderer(new SinaviEcluseCellRenderer());
    lEclusesCreees_.setModel(SinaviImplementation.SINAVIECLUSELISTMODEL);
    DureeESScrollPane_.setPreferredSize(dimtdes_);
    bCreerDureesManoeuvres_.setPreferredSize(new Dimension(495, 20));
    bSupprimerDureesManoeuvres_.setPreferredSize(new Dimension(150, 20));
    tDureesESTypeBateau_.getColumnModel().getColumn(0).setCellRenderer(new SinaviDureesESCellRenderer());
    // mise a blanc des tables
    SinaviImplementation.SINAVITABLECRENEAUMODEL.setEcluse(null);
    SinaviImplementation.SINAVITABLEDUREEESMODEL.setEcluse(null);
    // impossibilite de redimmensionner les colonnes des tables
    tDureesESTypeBateau_.getColumnModel().getColumn(0).setResizable(false);
    tDureesESTypeBateau_.getColumnModel().getColumn(1).setResizable(false);
    tDureesESTypeBateau_.getColumnModel().getColumn(2).setResizable(false);
    tDureesESTypeBateau_.getColumnModel().getColumn(3).setResizable(false);
    tDureesESTypeBateau_.getColumnModel().getColumn(4).setResizable(false);
    tCreneauHoraire_.getColumnModel().getColumn(0).setResizable(false);
    tCreneauHoraire_.getColumnModel().getColumn(1).setResizable(false);
    // Contraintes communes � tous les composants
    c_.gridy = GridBagConstraints.RELATIVE;
    c_.anchor = GridBagConstraints.WEST;
    c_.ipadx = 0;
    c_.ipady = 0;
    // Largeur des zones de texte
    tfNomEcluse_.setColumns(10);
    tfMessage_.setColumns(15);
    tfMessage_.setEditable(false);
    // selection de la premiere ecluse
    if ((!_mode_edition) && (lEclusesCreees_.getFirstVisibleIndex() != -1)) {
      // System.out.println("liste pas vide");
      lEclusesCreees_.setSelectedIndex(lEclusesCreees_.getFirstVisibleIndex());
    }
    c_.gridheight = 1; // 1 ligne pour les �tiquettes.
    // Remplissage de la premiere colonne
    c_.gridx = 0; // premiere colonne
    if (_mode_edition) {
      placeComposant(lm_, new BuLabel(""), c_);
    } else {
      placeComposant(lm_, new BuLabel("Ecluses cr��es"), c_);
    }
    c_.anchor = GridBagConstraints.CENTER;
    c_.gridheight = 5;
    if (_mode_edition) {
      placeComposant(lm_, new BuPicture(SinaviResource.SINAVI.getImage("sinaviimageecluse")), c_);
      c_.gridheight = 1;
      placeComposant(lm_, new BuLabel(""), c_);
    } else {
      placeComposant(lm_, EclusesCreeesScrollPane_, c_);
      c_.gridheight = 1;
      placeComposant(lm_, new BuLabel(""), c_);
    }
    c_.anchor = GridBagConstraints.WEST;
    c_.insets.top = 10;
    placeComposant(lm_, new BuLabel("Temps d'entree Moyen :"), c_);
    placeComposant(lm_, new BuLabel("Temps de sortie Moyen :"), c_);
    // Remplissage de la deuxieme colonne
    c_.gridx = 1; // deuxieme colonne
    c_.insets.top = 0;
    c_.insets.left = 10;
    placeComposant(lm_, new BuLabel("Creneaux :"), c_);
    placeComposant(lm_, bCreerCreneau_, c_);
    c_.gridheight = 2;
    placeComposant(lm_, CreneauHoraireScrollPane_, c_);
    c_.gridheight = 1;
    placeComposant(lm_, bSupprimerCreneau_, c_);
    placeComposant(lm_, rbEcluseManuelle_, c_);
    placeComposant(lm_, rbEcluseAutomatique_, c_);
    c_.insets.top = 10;
    placeComposant(lm_, dfTempsEntreeMoyen_, c_);
    placeComposant(lm_, dfTempsSortieMoyen_, c_);
    // Remplissage de la troisieme colonne
    c_.gridx = 2; // troisieme colonne
    c_.insets.top = 0;
    placeComposant(lm_, new BuLabel("Nom :"), c_);
    placeComposant(lm_, new BuLabel("Longueur :"), c_);
    placeComposant(lm_, new BuLabel("Largeur :"), c_);
    placeComposant(lm_, new BuLabel("Profondeur :"), c_);
    placeComposant(lm_, new BuLabel("Hauteur de chute :"), c_);
    placeComposant(lm_, new BuLabel("Fausse Bassin�e montante :"), c_);
    placeComposant(lm_, new BuLabel("Fausse Bassin�e avalante :"), c_);
    c_.insets.top = 10;
    placeComposant(lm_, new BuLabel("Bassin�e Montante Moyenne :"), c_);
    placeComposant(lm_, new BuLabel("Bassin�e Avalante Moyenne :"), c_);
    // Remplissage de la quatrieme colonne
    c_.gridx = 3; // quatrieme colonne
    c_.insets.top = 0;
    c_.insets.left = 0;
    placeComposant(lm_, tfNomEcluse_, c_);
    placeComposant(lm_, lfTailleEcluseLongueur_, c_);
    placeComposant(lm_, lfTailleEcluseLargeur_, c_);
    placeComposant(lm_, lfEcluseProfondeur_, c_);
    placeComposant(lm_, lfHauteurChuteEau_, c_);
    placeComposant(lm_, dfFausseBassineeMontante_, c_);
    placeComposant(lm_, dfFausseBassineeAvalante_, c_);
    c_.insets.top = 10;
    placeComposant(lm_, dfBassineeMontanteMoyen_, c_);
    placeComposant(lm_, dfBassineeAvalanteMoyen_, c_);
    c_.gridx = 0;
    c_.insets.top = 10;
    c_.insets.left = 0;
    c_.gridwidth = 4;
    c_.anchor = GridBagConstraints.CENTER;
    placeComposant(lm_, DureeESScrollPane_, c_);
    c_.insets.top = 0;
    placeComposant(lm_, pCreerAnnuler_, c_);
    // ajout du bouton valider au panel boutons
    pBoutons_.add(bValiderEcluse_);
    // ajout du message au panel message
    pMessage_.add(tfMessage_);
    // ajout de message et boutons a sud
    pSud_.add(pMessage_, BorderLayout.WEST);
    if (!_mode_edition) {
      pSud_.add(pBoutons_, BorderLayout.EAST);
    }
    // ajout des deux panels � l'onglet
    add(pDonnees_, BorderLayout.CENTER);
    add(pSud_, BorderLayout.SOUTH);
  }

  /**
   * sauvegarde des param�tres de l'�cluse s�lectionn�e
   * 
   * @param _ecluse l'�cluse s�lectionn�e dans la liste
   */
  public void setEcluseParametres(final IEcluseFluviale _ecluse) {
    // sauvegarde des dernieres valeurs qui deviennent les valeurs par defaut
    SinaviPreferences.SINAVIPREFS.putStringProperty("longueur_ecluse", "" + lfTailleEcluseLongueur_.getLongueurField());
    SinaviPreferences.SINAVIPREFS.putStringProperty("largeur_ecluse", "" + lfTailleEcluseLargeur_.getLongueurField());
    SinaviPreferences.SINAVIPREFS.putStringProperty("profondeur_ecluse", "" + lfEcluseProfondeur_.getLongueurField());
    SinaviPreferences.SINAVIPREFS.putStringProperty("duree_fausse_bassinee_montante_ecluse", ""
        + dfFausseBassineeMontante_.getDureeField());
    SinaviPreferences.SINAVIPREFS.putStringProperty("duree_fausse_bassinee_avalante_ecluse", ""
        + dfFausseBassineeAvalante_.getDureeField());
    SinaviPreferences.SINAVIPREFS.putStringProperty("duree_bassinee_moyenne_montante_ecluse", ""
        + dfBassineeMontanteMoyen_.getDureeField());
    SinaviPreferences.SINAVIPREFS.putStringProperty("duree_bassinee_moyenne_avalante_ecluse", ""
        + dfBassineeAvalanteMoyen_.getDureeField());
    SinaviPreferences.SINAVIPREFS.putStringProperty("duree_entree_moyenne_ecluse", ""
        + dfTempsEntreeMoyen_.getDureeField());
    SinaviPreferences.SINAVIPREFS.putStringProperty("duree_sortie_moyenne_ecluse", ""
        + dfTempsSortieMoyen_.getDureeField());
    // sauvegarde des parametres de l'ecluse
    _ecluse.nom(tfNomEcluse_.getText());
    _ecluse.longueur(lfTailleEcluseLongueur_.getLongueurField());
    _ecluse.largeur(lfTailleEcluseLargeur_.getLongueurField());
    _ecluse.profondeur(lfEcluseProfondeur_.getLongueurField());
    _ecluse.dureeFausseBassineeMontante(dfFausseBassineeMontante_.getDureeField());
    _ecluse.dureeFausseBassineeAvalante(dfFausseBassineeAvalante_.getDureeField());
    _ecluse.automatique(rbEcluseAutomatique_.isSelected());
    _ecluse.dureeMoyenneBassineeMontante(dfBassineeMontanteMoyen_.getDureeField());
    _ecluse.dureeMoyenneBassineeAvalante(dfBassineeAvalanteMoyen_.getDureeField());
    _ecluse.dureeMoyenneEntree(dfTempsEntreeMoyen_.getDureeField());
    _ecluse.dureeMoyenneSortie(dfTempsSortieMoyen_.getDureeField());
  }

  /**
   * M�thode utile pour placer un composant dans l'onglet.
   * 
   * @param _lm le layout manager
   * @param _composant le composant
   * @param _c les contraintes
   */
  public void placeComposant(final GridBagLayout _lm, final Component _composant, final GridBagConstraints _c) {
    _lm.setConstraints(_composant, _c);
    pDonnees_.add(_composant);
  }

  /**
   * si clic sur la liste des �cluses, affichage de l'�cluse s�lectionn�e dans la liste si clic sur la table des
   * cr�neaux horaires, possibilit� de supprimer le cr�neau s�lectionn� si clic sur la table des dur�es de manoeuvres,
   * possibilit� de supprimer celles s�lectionn�es
   * 
   * @param _e
   */
  public void mouseClicked(final MouseEvent _e) {
    if (_e.getSource() == lEclusesCreees_) {
      if (lEclusesCreees_.getSelectedValue() == null) {
        // System.out.println("liste vide");
      } else {
        setEcluseCourante(IEcluseFluvialeHelper.narrow((org.omg.CORBA.Object) lEclusesCreees_.getSelectedValue()));
        SinaviImplementation.SINAVITABLECRENEAUMODEL.setEcluse(ecluseCourante_);
        SinaviImplementation.SINAVITABLEDUREEESMODEL.setEcluse(ecluseCourante_);
        getEcluseParametres(ecluseCourante_);
        bValiderEcluse_.setEnabled(true);
        bCreerCreneau_.setEnabled(true);
        bCreerDureesManoeuvres_.setEnabled(true);
      }
    } else if (_e.getSource() == tCreneauHoraire_) {
      bSupprimerCreneau_.setEnabled(true);
    } else if (_e.getSource() == tDureesESTypeBateau_) {
      bSupprimerDureesManoeuvres_.setEnabled(true);
    }
  }

  /**
   * pas utilis�
   * 
   * @param e
   */
  public void mousePressed(final MouseEvent e) {}

  /**
   * pas utilis�
   * 
   * @param e
   */
  public void mouseReleased(final MouseEvent e) {}

  /**
   * pas utilis�
   * 
   * @param e
   */
  public void mouseEntered(final MouseEvent e) {}

  /**
   * pas utilis�
   * 
   * @param e
   */
  public void mouseExited(final MouseEvent e) {}

  /**
   * si action sur le bouton cr�er cr�neau, apparition d'une fen�tre interne pour rentrer les valeurs du cr�neau si
   * action sur supprimer cr�neau, suppression du cr�neau s�lectionn� si action sur le bouton valider �cluse, test de la
   * validit� des param�tres, si ok sauvegarde puis affichage des nouveaux param�tres ou des anciens si pas valide si
   * action sur le bouton cr�er dur�es manoeuvres, apparition d'une fen�tre interne pour rentrer les dur�es si action
   * sur supprimer dur�es manoeuvres, suppression des dur�es s�lectionn�es
   * 
   * @param _e
   */
  public void actionPerformed(final ActionEvent _e) {
    if (_e.getSource() == bCreerCreneau_) {
      app_.addInternalFrame(new SinaviFilleCreneauHoraire(ecluseCourante_));
    } else if (_e.getSource() == bSupprimerCreneau_) {
      if (tCreneauHoraire_.getSelectedRow() != -1) {
        final DureeField creneauDebut_ = new DureeField(false, false, true, true, false);
        final DureeField creneauFin_ = new DureeField(false, false, true, true, false);
        final int debut_ = ((Integer) creneauDebut_.STRING_DUREE.stringToValue((String) tCreneauHoraire_.getValueAt(
            tCreneauHoraire_.getSelectedRow(), 0))).intValue();
        final int fin_ = ((Integer) creneauFin_.STRING_DUREE.stringToValue((String) tCreneauHoraire_.getValueAt(
            tCreneauHoraire_.getSelectedRow(), 1))).intValue();
        ecluseCourante_.enleveCreneauConstant(debut_, fin_);
        SinaviImplementation.SINAVITABLECRENEAUMODEL.refresh();
      }
    } else if (_e.getSource() == bValiderEcluse_) {
      if (validEcluseParametres()) {
        setEcluseParametres(ecluseCourante_);
        bValiderEcluse_.setEnabled(false);
        getEcluseParametres(ecluseCourante_);
      } else {
        final BuDialogMessage dialog_mess = new BuDialogMessage(app_, SinaviImplementation.informationsSoftware(),
            "Param�tres de l'�cluse\nincomplets");
        dialog_mess.activate();
        SinaviImplementation.assistant_.changeAttitude(BuAssistant.PAROLE, "Param�tres de l'�cluse\nincomplets");
        // getEcluseParametres(ecluseCourante_);
      }
    } else if (_e.getSource() == bCreerDureesManoeuvres_) {
      app_.addInternalFrame(new SinaviFilleDureesManoeuvres(ecluseCourante_));
    } else if (_e.getSource() == bSupprimerDureesManoeuvres_) {
      if (tDureesESTypeBateau_.getSelectedRow() != -1) {
        ecluseCourante_.enleveDureesNavire(INavireTypeHelper.narrow((org.omg.CORBA.Object) tDureesESTypeBateau_
            .getValueAt(tDureesESTypeBateau_.getSelectedRow(), 0)));
        SinaviImplementation.SINAVITABLEDUREEESMODEL.refresh();
      }
    }
  }

  /**
   * test de validit� de tous les champs de l'onglet
   */
  public boolean validEcluseParametres() {
    if (tfNomEcluse_.getText().equals(null)) {
      return false;
    }
    if (lfTailleEcluseLongueur_.getLongueurField() <= 0) {
      return false;
    }
    if (lfTailleEcluseLargeur_.getLongueurField() <= 0) {
      return false;
    }
    if (lfEcluseProfondeur_.getLongueurField() <= 0) {
      return false;
    }
    if (dfFausseBassineeMontante_.getDureeField() <= 0) {
      return false;
    }
    if (dfFausseBassineeAvalante_.getDureeField() <= 0) {
      return false;
    }
    if (dfBassineeAvalanteMoyen_.getDureeField() <= 0) {
      return false;
    }
    if (dfBassineeMontanteMoyen_.getDureeField() <= 0) {
      return false;
    }
    if (dfTempsEntreeMoyen_.getDureeField() <= 0) {
      return false;
    }
    if (dfTempsSortieMoyen_.getDureeField() <= 0) {
      return false;
    }
    return true;
  }

  /**
   * methode permettant de d�finir l'ecluse courante
   * 
   * @param _ecluseCourante La nouvelle valeur ecluseCourante
   */
  public void setEcluseCourante(final IEcluseFluviale _ecluseCourante) {
    ecluseCourante_ = _ecluseCourante;
  }

  /**
   * mise � jour des champs de l'�cluse s�lectionn�e dans la liste
   * 
   * @param _ecluse l'�cluse s�lectionn�e dans la liste
   */
  public void getEcluseParametres(final IEcluseFluviale _ecluse) {
    // les parametres de l'�cluse ont deja ete renseigne
    if (_ecluse.longueur() != 0) {
      tfMessage_.setForeground(Color.green);
      tfMessage_.setText("Param�tres enregistr�s");
      tfNomEcluse_.setText(_ecluse.nom());
      lfTailleEcluseLongueur_.setLongueurField(_ecluse.longueur());
      lfTailleEcluseLargeur_.setLongueurField(_ecluse.largeur());
      lfEcluseProfondeur_.setLongueurField(_ecluse.profondeur());
      dfFausseBassineeMontante_.setDureeField((int) _ecluse.dureeFausseBassineeMontante());
      dfFausseBassineeAvalante_.setDureeField((int) _ecluse.dureeFausseBassineeMontante());
      rbEcluseAutomatique_.setSelected(_ecluse.automatique());
      dfBassineeMontanteMoyen_.setDureeField((int) _ecluse.dureeMoyenneBassineeMontante());
      dfBassineeAvalanteMoyen_.setDureeField((int) _ecluse.dureeMoyenneBassineeAvalante());
      dfTempsEntreeMoyen_.setDureeField((int) _ecluse.dureeMoyenneEntree());
      dfTempsSortieMoyen_.setDureeField((int) _ecluse.dureeMoyenneSortie());
    } else {
      // les parametres de l'ecluse ne sont pas renseignes ( test pour savoir s'il existe des valeurs par defaut )
      tfMessage_.setForeground(Color.red);
      tfMessage_.setText("Param�tres non enregistr�s");
      tfNomEcluse_.setText(_ecluse.nom());
      if (SinaviPreferences.SINAVIPREFS.getStringProperty("longueur_ecluse").equals("")) {
        lfTailleEcluseLongueur_.setLongueurField(0);
      } else {
        lfTailleEcluseLongueur_.setLongueurField(new Double(SinaviPreferences.SINAVIPREFS
            .getStringProperty("longueur_ecluse")).doubleValue());
      }
      if (SinaviPreferences.SINAVIPREFS.getStringProperty("largeur_ecluse").equals("")) {
        lfTailleEcluseLargeur_.setLongueurField(0);
      } else {
        lfTailleEcluseLargeur_.setLongueurField(new Double(SinaviPreferences.SINAVIPREFS
            .getStringProperty("largeur_ecluse")).doubleValue());
      }
      if (SinaviPreferences.SINAVIPREFS.getStringProperty("profondeur_ecluse").equals("")) {
        lfEcluseProfondeur_.setLongueurField(0);
      } else {
        lfEcluseProfondeur_.setLongueurField(new Double(SinaviPreferences.SINAVIPREFS
            .getStringProperty("profondeur_ecluse")).doubleValue());
      }
      if (SinaviPreferences.SINAVIPREFS.getStringProperty("duree_fausse_bassinee_montante_ecluse").equals("")) {
        dfFausseBassineeMontante_.setDureeField(0);
      } else {
        dfFausseBassineeMontante_.setDureeField(SinaviPreferences.SINAVIPREFS
            .getIntegerProperty("duree_fausse_bassinee_montante_ecluse"));
      }
      if (SinaviPreferences.SINAVIPREFS.getStringProperty("duree_fausse_bassinee_avalante_ecluse").equals("")) {
        dfFausseBassineeAvalante_.setDureeField(0);
      } else {
        dfFausseBassineeAvalante_.setDureeField(SinaviPreferences.SINAVIPREFS
            .getIntegerProperty("duree_fausse_bassinee_avalante_ecluse"));
      }
      if (SinaviPreferences.SINAVIPREFS.getStringProperty("duree_bassinee_moyenne_montante_ecluse").equals("")) {
        dfBassineeMontanteMoyen_.setDureeField(0);
      } else {
        dfBassineeMontanteMoyen_.setDureeField(SinaviPreferences.SINAVIPREFS
            .getIntegerProperty("duree_bassinee_moyenne_montante_ecluse"));
      }
      if (SinaviPreferences.SINAVIPREFS.getStringProperty("duree_bassinee_moyenne_avalante_ecluse").equals("")) {
        dfBassineeAvalanteMoyen_.setDureeField(0);
      } else {
        dfBassineeAvalanteMoyen_.setDureeField(SinaviPreferences.SINAVIPREFS
            .getIntegerProperty("duree_bassinee_moyenne_avalante_ecluse"));
      }
      if (SinaviPreferences.SINAVIPREFS.getStringProperty("duree_entree_moyenne_ecluse").equals("")) {
        dfTempsEntreeMoyen_.setDureeField(0);
      } else {
        dfTempsEntreeMoyen_.setDureeField(SinaviPreferences.SINAVIPREFS
            .getIntegerProperty("duree_entree_moyenne_ecluse"));
      }
      if (SinaviPreferences.SINAVIPREFS.getStringProperty("duree_sortie_moyenne_ecluse").equals("")) {
        dfTempsSortieMoyen_.setDureeField(0);
      } else {
        dfTempsSortieMoyen_.setDureeField(SinaviPreferences.SINAVIPREFS
            .getIntegerProperty("duree_sortie_moyenne_ecluse"));
      }
      rbEcluseManuelle_.setSelected(true);
    }
  }
}
