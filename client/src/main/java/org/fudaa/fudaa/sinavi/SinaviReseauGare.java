/*
 * @file         SinaviReseauGare.java
 * @creation     2001-05-17
 * @modification $Date: 2006-09-19 15:08:59 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.sinavi;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Rectangle;

import com.memoire.dja.DjaAnchor;
import com.memoire.dja.DjaBox;
import com.memoire.dja.DjaHandle;
import com.memoire.dja.DjaText;

import org.fudaa.dodico.corba.navigation.IGare;

/**
 * impl�mentation de l'objet Dja pour repr�senter une gare sur le r�seau
 * 
 * @version $Revision: 1.9 $ $Date: 2006-09-19 15:08:59 $ by $Author: deniger $
 * @author Aline Marechalle , Franck Lejeune
 */
public class SinaviReseauGare extends DjaBox {

  /**
   *
   */
  private final boolean[] attaches_ = { false, false, false, false, false, false, false, false, false, false };
  /**
   * Objet m�tier gare
   */
  private transient IGare gare_;
  /**
   * Cadre o� les informations concernant la gare du cot� montant sont report�es pour l'animation.
   */
  private DjaText textMontGare_;
  /**
   * Cadre o� les informations concernant la gare du cot� avalant sont report�es pour l'animation.
   */
  private DjaText textAvalGare_;
  /**
   * Rectangle � dessiner autour des informations
   */
  private Rectangle rect_;
  /**
   * Numero d'enregistrement, la position de la gare dans le tableau de gares de l'�tude
   */
  private int numeroEnregistrement_;

  /**
   * SinaviReseauGare Constructeur Sp�cifique pour YAPOD
   */
  public SinaviReseauGare() {
    gare_ = null;
    setHeight(21);
    setWidth(21);
    setBackground(new Color(122, 220, 255));
    setForeground(Color.black);
    putProperty("epaisseur", "2");
    textMontGare_ = new DjaText(this, 0, "      \n     \n     \n      \n     ", true, 0, -5);
    textMontGare_.setFont(new Font("Monospaced", Font.PLAIN, 10));
    textMontGare_.setPosition(NORTH);
    textMontGare_.setAlignment(LEFT);
    textAvalGare_ = new DjaText(this, 0, "      \n     \n     \n      \n     ", true, 0, 5);
    textAvalGare_.setFont(new Font("Monospaced", Font.PLAIN, 10));
    textAvalGare_.setPosition(SOUTH);
    textAvalGare_.setAlignment(LEFT);
    setTextArray(new DjaText[] { textMontGare_, textAvalGare_ });
  }

  /**
   * SinaviReseauGare Constructeur d'une gare au point de vue graphique.
   * 
   * @param _gare
   */
  public SinaviReseauGare(final IGare _gare) {
    setHeight(21);
    setWidth(21);
    numeroEnregistrement_ = -1;
    setBackground(new Color(122, 220, 255));
    setForeground(Color.black);
    gare_ = _gare;
    putProperty("epaisseur", "2");
    putProperty("numeroEnregistrement", (new Integer(-1)).toString());
    textMontGare_ = new DjaText(this, 0, "      \n     \n     \n      \n     ", true, 0, -5);
    textMontGare_.setFont(new Font("Monospaced", Font.PLAIN, 10));
    textMontGare_.setPosition(NORTH);
    textMontGare_.setAlignment(RIGHT);
    textAvalGare_ = new DjaText(this, 0, "      \n     \n     \n      \n     ", true, 0, 5);
    textAvalGare_.setFont(new Font("Monospaced", Font.PLAIN, 10));
    textAvalGare_.setPosition(SOUTH);
    textAvalGare_.setAlignment(RIGHT);
    setTextArray(new DjaText[] { textMontGare_, textAvalGare_ });
  }

  /**
   * methode appel�e avant la sauvegarde du r�seau sous forme de String Elle permet d'attribuer le num�ro
   * d'enregistrement de la gare
   */
  public void beforeSaving() {
    super.beforeSaving();
    for (int indexgare = 0; indexgare < SinaviImplementation.ETUDE_SINAVI.reseau().gares().length; indexgare++) {
      if ((SinaviImplementation.ETUDE_SINAVI.reseau().gares()[indexgare]).egale(gare_)) {
        putProperty("numeroEnregistrement", (new Integer(indexgare)).toString());
      }
    }
  }

  /**
   * methode appel�e apres l'ouverture d'une etude et la construction physique du r�seau Elle permet de d�finir la gare
   * � partir du num�ro d'enregistrement de la gare
   */
  public void afterLoading() {
    super.afterLoading();
    numeroEnregistrement_ = new Integer(getProperty("numeroEnregistrement")).intValue();
    if (numeroEnregistrement_ != -1) {
      gare_ = SinaviImplementation.ETUDE_SINAVI.reseau().gares()[numeroEnregistrement_];
    } else {
      /*
       * BuDialogMessage dialog_mess = new BuDialogMessage(app_, SinaviImplementation.informationsSoftware(), "Probl�mes
       * de chargement des gares"); dialog_mess.activate();
       */
    }
  }

  /**
   * retourne la valeur Anchors de SinaviReseauGare object
   * 
   * @return La valeur Anchors
   */
  public DjaAnchor[] getAnchors() {
    final int x = getX();
    final int y = getY();
    final DjaAnchor[] r = { new DjaAnchor(this, 0, WEST, x, y + 10), new DjaAnchor(this, 1, EAST, x + 20, y + 10),
        new DjaAnchor(this, 2, WEST, x, y + 5), new DjaAnchor(this, 3, EAST, x + 20, y + 5),
        new DjaAnchor(this, 4, WEST, x, y + 15), new DjaAnchor(this, 5, EAST, x + 20, y + 15),
        new DjaAnchor(this, 6, WEST, x, y), new DjaAnchor(this, 7, EAST, x + 20, y),
        new DjaAnchor(this, 8, WEST, x, y + 20), new DjaAnchor(this, 9, EAST, x + 20, y + 20) };
    return r;
  }

  /**
   * retourne la valeur Handles de SinaviReseauGare object
   * 
   * @return La valeur Handles
   */
  public DjaHandle[] getHandles() {
    return new DjaHandle[0];
  }

  /**
   * retourne la valeur Gare de SinaviReseauGare object
   * 
   * @return La valeur Gare
   */
  public IGare getGare() {
    return gare_;
  }

  /**
   * retourne la valeur TextMontGare de SinaviReseauGare object
   * 
   * @return La valeur TextMontGare
   */
  public DjaText getTextMontGare() {
    return textMontGare_;
  }

  /**
   * retourne la valeur TextAvalGare de SinaviReseauGare object
   * 
   * @return La valeur TextAvalGare
   */
  public DjaText getTextAvalGare() {
    return textAvalGare_;
  }

  /**
   * Dessine un objet graphique gare avec ses dif�rentes propri�t�s.
   * 
   * @param g
   */
  public void paintObject(final Graphics g) {
    super.paintObject(g);
    final Color coul = new Color(0, 0, 0);
    SinaviAnimation.contourDjaText(g, textMontGare_, rect_, coul);
    SinaviAnimation.contourDjaText(g, textAvalGare_, rect_, coul);
  }

  public boolean AttachesEntreePleines() {
    for (int i = 0; i < 10; i = i + 2) {
      if (!attaches_[i]) {
        return false;
      }
    }
    return true;
  }

  public boolean AttachesSortiePleines() {
    for (int i = 1; i < 10; i = i + 2) {
      if (!attaches_[i]) {
        return false;
      }
    }
    return true;
  }

  public int AttacheEntreeLibre() {
    if (!AttachesEntreePleines()) {
      for (int i = 0; i < 10; i = i + 2) {
        if (!attaches_[i]) {
          attaches_[i] = true;
          return i;
        }
      }
    }
    return -1;
  }

  public int AttacheSortieLibre() {
    if (!AttachesSortiePleines()) {
      for (int i = 1; i < 10; i = i + 2) {
        if (!attaches_[i]) {
          attaches_[i] = true;
          return i;
        }
      }
    }
    return -1;
  }

  public void LibereAttache(final int i) {
    attaches_[i] = false;
  }

  /**
   * Ecrit les informations pour les sens montant et descendant d'une gare.
   * 
   * @param _str : chaine de caract�re � �crire
   * @param mont : sera = true si sens montant
   */
  public void ecrireTextGare(final String _str, final boolean mont) {
    if (mont) {
      textMontGare_.setText(_str);
    } else {
      textAvalGare_.setText(_str);
    }
  }
}
