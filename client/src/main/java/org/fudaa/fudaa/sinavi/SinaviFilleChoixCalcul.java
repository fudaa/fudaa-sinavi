/*
 * @file         SinaviFilleChoixCalcul.java
 * @creation     2001-05-17
 * @modification $Date: 2006-09-19 15:09:00 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.sinavi;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.JComponent;
import javax.swing.border.EmptyBorder;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuCommonImplementation;
import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuInternalFrame;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuRadioButton;

/**
 * impl�mentation d'une fen�tre interne permettant de choisir le mode de calcul ( avec ou sans animation)
 * 
 * @version $Revision: 1.6 $ $Date: 2006-09-19 15:09:00 $ by $Author: deniger $
 * @author Aline Marechalle , Franck Lejeune
 */
public class SinaviFilleChoixCalcul extends BuInternalFrame implements ActionListener {

  /**
   * bouton pour d�marrer le calcul
   */
  private final BuButton bDemarrerCalcul_ = new BuButton(SinaviResource.SINAVI.getIcon("VALIDER"), "D�marrer calcul");
  /**
   * bouton pour annuler
   */
  private final BuButton bAnnuler_ = new BuButton(SinaviResource.SINAVI.getIcon("ANNULER"), "Annuler");
  /**
   * bouton radio s�lection animation
   */
  private final BuRadioButton rbAnimation_ = new BuRadioButton("Animation graphique");
  /**
   * bouton radio s�lection pas d'animation
   */
  private final BuRadioButton rbNoAnimation_ = new BuRadioButton("Pas d'animation graphique");
  /**
   * champs pour rentrer le pas de temps
   */
  private final DureeField dfDeltaT_ = new DureeField(false, true, true, true, false);
  /**
   * groupe pour les radio boutons
   */
  private final ButtonGroup bgAnimation_ = new ButtonGroup();
  /**
   * label permettant de le griser
   */
  private final BuLabel lPasTemps_ = new BuLabel("pas de temps entre deux affichages");
  /**
   * panel contenant les boutons, il est plac� en bas � droite
   */
  private final BuPanel pBoutons_ = new BuPanel();
  /**
   * panel contenant les diff�rents champs de la fen�tre
   */
  private final BuPanel pDonnees_ = new BuPanel();

  /**
   * une instance de SinaviImplementation
   */
  // private BuCommonImplementation app_;
  /**
   * Constructeur
   * 
   * @param _app une instance de SinaviImplementation
   */
  public SinaviFilleChoixCalcul(final BuCommonImplementation _app) {
    super("Choix du calcul", false, false, false, false);
    // app_= _app;
    // Ajout des listener aux boutons
    bDemarrerCalcul_.addActionListener(this);
    bAnnuler_.addActionListener(this);
    rbAnimation_.addActionListener(this);
    rbNoAnimation_.addActionListener(this);
    // placement des boutons radio dans le groupe de boutons
    bgAnimation_.add(rbAnimation_);
    bgAnimation_.add(rbNoAnimation_);
    // Selection du bouton pas d'animation par defaut
    rbNoAnimation_.setSelected(true);
    lPasTemps_.setEnabled(false);
    dfDeltaT_.setEnabled(false);
    // remplissage de la fenetre
    ((JComponent) getContentPane()).setBorder(new EmptyBorder(25, 25, 25, 25));
    pDonnees_.setLayout(new BuGridLayout(2, 15, 15));
    pDonnees_.add(rbNoAnimation_);
    pDonnees_.add(rbAnimation_);
    pDonnees_.add(lPasTemps_);
    pDonnees_.add(dfDeltaT_);
    pBoutons_.setLayout(new FlowLayout(FlowLayout.RIGHT));
    pBoutons_.add(bAnnuler_);
    pBoutons_.add(bDemarrerCalcul_);
    getContentPane().add(pDonnees_, BorderLayout.CENTER);
    getContentPane().add(pBoutons_, BorderLayout.SOUTH);
    pack();
    setVisible(true);
  }

  /**
   * @param _e
   */
  public void actionPerformed(final ActionEvent _e) {
    if (_e.getSource() == rbAnimation_) {
      lPasTemps_.setEnabled(true);
      dfDeltaT_.setEnabled(true);
    } else if (_e.getSource() == rbNoAnimation_) {
      lPasTemps_.setEnabled(false);
      dfDeltaT_.setEnabled(false);
    } else if (_e.getSource() == bDemarrerCalcul_) {
      System.out.println("Demarrer calcul");
      try {
        setClosed(true);
      } catch (final Exception _pve) {}
    } else {
      try {
        setClosed(true);
      } catch (final Exception _pve) {}
    }
  }
}
