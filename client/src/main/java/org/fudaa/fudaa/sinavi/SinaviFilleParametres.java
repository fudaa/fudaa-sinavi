/*
 * @file         SinaviFilleParametres.java
 * @creation     2001-05-17
 * @modification $Date: 2006-09-19 15:08:59 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.sinavi;

import java.awt.Dimension;

import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import com.memoire.bu.BuBorderLayout;
import com.memoire.bu.BuCommonInterface;
import com.memoire.bu.BuInternalFrame;
import com.memoire.bu.BuResource;

/**
 * impl�mentation de la fen�tre interne des param�tres de Sinavi
 * 
 * @version $Revision: 1.6 $ $Date: 2006-09-19 15:08:59 $ by $Author: deniger $
 * @author Aline Marechalle , Franck Lejeune
 */
public class SinaviFilleParametres extends BuInternalFrame implements ChangeListener {

  /**
   * D�claration du classeur � onglets.
   */
  JTabbedPane tpMain_;
  /**
   * Num�ro de l'onglet courant.
   */
  int ongletCourant_;
  /**
   * onglet d�finissant les donn�es g�n�rales
   */
  SinaviDonneesGeneralesParametres pnDonneesGenerales_;
  /**
   * onglet d�finissant les types de bateau
   */
  SinaviBateauxParametres pnBateaux_;
  /**
   * onglet d�finissant les biefs
   */
  SinaviBiefsParametres pnBiefs_;
  /**
   * onglet d�finissant les �cluses
   */
  SinaviEclusesParametres pnEcluses_;
  /**
   * onglets d�finissant les gares
   */
  SinaviGaresParametres pnGares_;
  /**
   * le contenant de la fen�tre
   */
  JPanel content_;

  /**
   * Constructeur SinaviFilleParametres.
   * 
   * @param _appli
   */
  public SinaviFilleParametres(final BuCommonInterface _appli) {
    super("", false, true, true, true);
    tpMain_ = new JTabbedPane();
    pnDonneesGenerales_ = new SinaviDonneesGeneralesParametres(_appli);
    pnBateaux_ = new SinaviBateauxParametres(_appli);
    pnBiefs_ = new SinaviBiefsParametres(_appli, false);
    pnEcluses_ = new SinaviEclusesParametres(_appli, false);
    pnGares_ = new SinaviGaresParametres(_appli, false);
    tpMain_.addTab(" Donn�es G�n�rales ", null, pnDonneesGenerales_, "Param�tres Donn�es G�n�rales");
    tpMain_.addTab(" Bateaux ", null, pnBateaux_, "Param�tres Bateaux");
    tpMain_.addTab(" Biefs ", null, pnBiefs_, "Param�tres Biefs");
    tpMain_.addTab(" Ecluses ", null, pnEcluses_, "Param�tres Ecluses");
    tpMain_.addTab(" Gares ", null, pnGares_, "Param�tres Gares");
    // content_ = (JComponent)(super.getContentPane());
    content_ = new JPanel();
    content_.setBorder(new EmptyBorder(5, 5, 5, 5));
    content_.setLayout(new BuBorderLayout());
    content_.add(tpMain_, BuBorderLayout.CENTER);
    setContentPane(content_);
    setTitle("Param�tres de calcul");
    setFrameIcon(BuResource.BU.getIcon("parametre"));
    setPreferredSize(new Dimension(750, 500));
    setSize(new Dimension(750, 500));
    // La fille param�tre est � l'�coute des changements d'onglet
    tpMain_.addChangeListener(this);
  }

  /**
   * Recepteur pour les changements d'onglets.
   * 
   * @param _evt
   */
  public void stateChanged(final ChangeEvent _evt) {
    switch (ongletCourant_) {
    case 0: // Quitte donn�es g�nerales
      // System.out.println("Quitte l'onglet donn�es g�n�rales");
      break;
    case 1: // Quitte les bateaux
      // System.out.println("Quitte l'onglet bateaux");
      break;
    case 2: // Quitte les biefs
      // System.out.println("Quitte l'onglet biefs");
      break;
    case 3: // Quitte les ecluses
      // System.out.println("Quitte l'onglet �cluses");
      break;
    case 4: // Quitte les gares
      // System.out.println("Quitte l'onglet gares");
      break;
    default:
      // System.out.println("Onglet inconnu");
    }
    // Mise � jour de l'onglet courant
    ongletCourant_ = tpMain_.getSelectedIndex();
  }
}
