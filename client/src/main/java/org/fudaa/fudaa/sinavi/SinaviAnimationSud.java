/*
 * @file         SinaviAnimationSud.java
 * @creation     2001-05-17
 * @modification $Date: 2006-09-19 15:09:00 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.sinavi;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuPicture;

/**
 * Panel superieur de l'animation.
 * 
 * @version $Revision: 1.6 $ $Date: 2006-09-19 15:09:00 $ by $Author: deniger $
 * @author Aline Marechalle , Franck Lejeune
 */
public class SinaviAnimationSud extends BuPanel implements ActionListener {

  /**
   * Bouton reprise
   */
  protected static BuButton reprise_ = new BuButton("Reprise");
  /**
   * Booleen attente qui sert pour la pause
   */
  private static boolean attente_ = false;
  /**
   * Bouton pause
   */
  protected static BuButton pause_ = new BuButton("Pause");
  /**
   * Bouton pasApas, pour avoir un mode d'aniamtion pas� pas
   */
  protected static BuButton pasApas_ = new BuButton("Pas � Pas");
  /**
   * Bouton quitter
   */
  protected static BuButton quitter_ = new BuButton("Quitter");

  /**
   * SinaviAnimationSud Constructeur qui met en page le cot� sud de l'animation: les boutons et la l�gende.
   */
  public SinaviAnimationSud() {
    setLayout(new GridLayout(3, 6));
    setBorder(new EmptyBorder(5, 5, 5, 5));
    final BuPicture imgFrec = new BuPicture(SinaviResource.SINAVI.getImage("freycinet"));
    add(imgFrec);
    final BuPicture imgConv = new BuPicture(SinaviResource.SINAVI.getImage("convoi_pousse"));
    add(imgConv);
    final BuPicture imgPlais = new BuPicture(SinaviResource.SINAVI.getImage("plaisancier"));
    add(imgPlais);
    final BuPicture imgPassg = new BuPicture(SinaviResource.SINAVI.getImage("passagers"));
    add(imgPassg);
    final BuPicture imgPerso = new BuPicture(SinaviResource.SINAVI.getImage("personnalise"));
    add(imgPerso);
    final BuPicture flecheaval = new BuPicture(SinaviResource.SINAVI.getImage("flecheaval"));
    add(flecheaval);
    final BuLabel frec = new BuLabel("Freycinet", SwingConstants.CENTER);
    add(frec);
    final BuLabel conv = new BuLabel("Convoi pouss�", SwingConstants.CENTER);
    add(conv);
    final BuLabel plais = new BuLabel("Plaisancier", SwingConstants.CENTER);
    add(plais);
    final BuLabel passg = new BuLabel("Passager", SwingConstants.CENTER);
    add(passg);
    final BuLabel perso = new BuLabel("Personnalis�", SwingConstants.CENTER);
    add(perso);
    final BuLabel avalant = new BuLabel("Sens Avalant", SwingConstants.CENTER);
    add(avalant);
    final BuLabel vide = new BuLabel("");
    add(vide);
    add(pause_);
    add(reprise_);
    add(pasApas_);
    add(quitter_);
  }

  /**
   * G�re les boutons reprise - pause - pas � pas - quitter.
   * 
   * @param evt
   */
  public void actionPerformed(final ActionEvent evt) {
    if (evt.getSource() == quitter_) {
      System.out.println("appuyer sur quitter pour quitter");
      attente(false);
      reprise_.setEnabled(false);
      pause_.setEnabled(false);
      pasApas_.setEnabled(false);
      quitter_.setEnabled(false);
      SinaviImplementation.sinavireseauframe_.activerBoutonInteractive(true);
      SinaviImplementation.SERVICE_SINAVI.arretCalculServeur(true);
      SinaviAnimation.viderDjatext();
      SinaviAnimation.fermerToutesLesEcluses();
      SinaviAnimationNord.progres_.setValue(0);
      SinaviReseauFrame.coteNord.setVisible(false);
      SinaviReseauFrame.coteSud.setVisible(false);
      SinaviReseauFrame.coteNord.constructTextGraine(0, 0);
      SinaviReseauFrame.coteNord.constructTextJour(0);
      SinaviReseauFrame.coteNord.constructTextHeure(0, 0);
    }
    if (evt.getSource() == pause_) {
      pause_.setEnabled(false);
      reprise_.setEnabled(true);
      reprise_.requestFocus();
      pasApas_.setEnabled(true);
      SinaviAnimationNord.valDeltat_.setEnabled(true);
      attente(true);
      System.out.println("appuyer sur pause");
    }
    if (evt.getSource() == reprise_) {
      reprise_.setEnabled(false);
      pause_.requestFocus();
      pause_.setEnabled(true);
      pasApas_.setEnabled(false);
      SinaviImplementation.SERVICE_SINAVI.deltat(((Integer) SinaviAnimationNord.valDeltat_.getValue()).intValue());
      SinaviAnimationNord.valDeltat_.setEnabled(false);
      SinaviImplementation.SERVICE_SINAVI.calculUnDeltaTFait(false);
      SinaviImplementation.SERVICE_SINAVI.demandeCalculUnDeltaT(false);
      attente(false);
      System.out.println("appuyer sur reprise");
    }
    if (evt.getSource() == pasApas_) {
      attente(false);
      pasApas_.setEnabled(true);
      pasApas_.requestFocus();
      pause_.setEnabled(false);
      reprise_.setEnabled(true);
      SinaviAnimationNord.valDeltat_.setEnabled(true);
      SinaviImplementation.SERVICE_SINAVI.calculUnDeltaTFait(false);
      System.out.println("Ds pasApas " + Thread.currentThread());
      SinaviImplementation.SERVICE_SINAVI.deltat(((Integer) SinaviAnimationNord.valDeltat_.getValue()).intValue());
      SinaviImplementation.SERVICE_SINAVI.demandeCalculUnDeltaT(true);
    }
  }

  /**
   * @return la valeur de la variable attente.
   */
  public boolean attente() {
    return attente_;
  }

  /**
   * Affecte une valeur � la variable attente.
   * 
   * @param _entree : valeur de la variable attente
   */
  public void attente(final boolean _entree) {
    attente_ = _entree;
  }

  /**
   * Initialise les activations ou non des boutons, car on peut quitter puis reprendre une simulation avec la m�me
   * grille.
   */
  public void initBoutons() {
    pause_.addActionListener(this);
    // focus par defaut sur pause
    pause_.setDefaultCapable(true);
    pause_.setEnabled(true);
    pause_.requestFocus();
    reprise_.setEnabled(false);
    reprise_.addActionListener(this);
    pasApas_.addActionListener(this);
    pasApas_.setEnabled(false);
    quitter_.addActionListener(this);
    quitter_.setEnabled(true);
  }
}
