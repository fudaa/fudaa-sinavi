/*
 * @file         SinaviHeuresDeterministesListModel.java
 * @creation     2001-05-17
 * @modification $Date: 2006-09-19 15:08:59 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.sinavi;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.AbstractListModel;

import com.memoire.fu.FuVectorint;

/**
 * D�finit le mod�le pour la liste affichant les instants pour une loi d�terministe
 * 
 * @version $Revision: 1.6 $ $Date: 2006-09-19 15:08:59 $ by $Author: deniger $
 * @author Aline Marechalle , Franck Lejeune
 */
public class SinaviHeuresDeterministesListModel extends AbstractListModel {

  /**
   * vecteur d'entier contenant les instants
   */
  private FuVectorint creneauGenerationDeterministe_;

  /**
   * retourne l'instant s�lectionn� dans la liste
   * 
   * @param _index
   * @return l'instant s�lectionn� dans la liste
   */
  public Object getElementAt(final int _index) {
    if (creneauGenerationDeterministe_ == null) {
      return null;
    }
    final Date date_ = new Date(creneauGenerationDeterministe_.elementAt(_index) * 1000l);
    final SimpleDateFormat format_ = new java.text.SimpleDateFormat("EEEE dd MMMM yyyy � HH:mm");
    return format_.format(date_);
  }

  /**
   * nombre d'instants contenu dans la liste
   * 
   * @return le nombre d'instants contenu dans la liste
   */
  public int getSize() {
    if (creneauGenerationDeterministe_ == null) {
      return 0;
    }
    return creneauGenerationDeterministe_.size();
  }

  /**
   * permet de rafraichir l'affichage de la liste ( ajout d'un instant )
   */
  public void refresh() {
    fireContentsChanged(this, 0, getSize() - 1);
  }

  /**
   * methode pour definir le cr�neau de g�n�ration
   */
  public void setGenerationDeterministe(final FuVectorint _creneauGenerationDeterministe) {
    creneauGenerationDeterministe_ = _creneauGenerationDeterministe;
  }
}
