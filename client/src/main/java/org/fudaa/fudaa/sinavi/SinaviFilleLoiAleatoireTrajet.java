/*
 * @file         SinaviFilleLoiAleatoireTrajet.java
 * @creation     2001-05-17
 * @modification $Date: 2006-09-19 15:08:59 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 */
package org.fudaa.fudaa.sinavi;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;

import javax.swing.JComponent;
import javax.swing.border.EmptyBorder;

import com.memoire.bu.*;

import org.fudaa.dodico.corba.navigation.IGenerationJournaliereAleatoire;
import org.fudaa.dodico.corba.navigation.ITrajetFluvial;

import org.fudaa.dodico.objet.UsineLib;

/**
 * impl�mentation d'une fen�tre interne permettant de cr�er une loi al�atoire
 * 
 * @version $Revision: 1.12 $ $Date: 2006-09-19 15:08:59 $ by $Author: deniger $
 * @author Aline Marechalle , Franck Lejeune
 */
public class SinaviFilleLoiAleatoireTrajet extends BuInternalFrame implements ActionListener {

  /**
   * champs pour renseigner l'intervalle moyen de la loi d'Erlang
   */
  private final DureeField dfIntervalleMoyen_ = new DureeField(false, false, true, true, false);
  /**
   * champs pour renseigner l'heure de d�but de la loi d'Erlang
   */
  private final DureeField dfAleatoireHeureDebut_ = new DureeField(false, false, true, true, false);
  /**
   * champs pour renseigner l'heure de fin de la loi d'Erlang
   */
  private final DureeField dfAleatoireHeureFin_ = new DureeField(false, false, true, true, false);
  /**
   * champs pour renseigner le jour de d�but de la loi d'Erlang
   */
  private final BuTextField tfAleatoireJourDebut_ = BuTextField.createDateField();
  /**
   * champs pour renseigner le jour de fin de la loi d'Erlang
   */
  private final BuTextField tfAleatoireJourFin_ = BuTextField.createDateField();
  /**
   * menu d�roulant pour choisir l'ordre de la loi d'Erlang
   */
  private final BuComboBox cbOrdreLoiErlang_ = new BuComboBox();
  /**
   * bouton pour afficher dans une autre fen�tre interne les navires g�n�r�s
   */
  private final BuButton bVoirNaviresGeneres_ = new BuButton(SinaviResource.SINAVI.getIcon("VOIR"), "Voir");
  /**
   * bouton valider
   */
  private final BuButton bValider_ = new BuButton(SinaviResource.SINAVI.getIcon("VALIDER"), "Valider");
  /**
   * bouton annuler
   */
  private final BuButton bAnnuler_ = new BuButton(SinaviResource.SINAVI.getIcon("ANNULER"), "Annuler");
  /**
   * bouton pour choisir le jour de d�but
   */
  private final BuButton bChoixJourDebut_ = new BuButton(SinaviResource.SINAVI.getIcon("VALIDER"), "Premier Jour");
  /**
   * bouton pour choisir le jour de fin
   */
  private final BuButton bChoixJourFin_ = new BuButton(SinaviResource.SINAVI.getIcon("VALIDER"), "Dernier Jour");
  /**
   * case � cocher pour valider ou non la navigation le lundi
   */
  private final BuCheckBox cbLundi_ = new BuCheckBox("Lundi");
  /**
   * case � cocher pour valider ou non la navigation le mardi
   */
  private final BuCheckBox cbMardi_ = new BuCheckBox("Mardi");
  /**
   * case � cocher pour valider ou non la navigation le mercredi
   */
  private final BuCheckBox cbMercredi_ = new BuCheckBox("Mercredi");
  /**
   * case � cocher pour valider ou non la navigation le jeudi
   */
  private final BuCheckBox cbJeudi_ = new BuCheckBox("Jeudi");
  /**
   * case � cocher pour valider ou non la navigation le vendredi
   */
  private final BuCheckBox cbVendredi_ = new BuCheckBox("Vendredi");
  /**
   * case � cocher pour valider ou non la navigation le samedi
   */
  private final BuCheckBox cbSamedi_ = new BuCheckBox("Samedi");
  /**
   * case � cocher pour valider ou non la navigation le dimanche
   */
  private final BuCheckBox cbDimanche_ = new BuCheckBox("Dimanche");
  /**
   * panel contenant les boutons, il est plac� en bas � droite
   */
  private final BuPanel pBoutons_ = new BuPanel();
  /**
   * panel contenant les diff�rents champs de la fen�tre
   */
  private final BuPanel pDonnees_ = new BuPanel();
  /**
   * trajet associ� � la loi
   */
  private ITrajetFluvial trajet_;
  /**
   * loi que l'on va cr�er ou afficher
   */
  private IGenerationJournaliereAleatoire loi_;
  /**
   * une instance de SinaviImplementation
   */
  private BuCommonImplementation app_;
  /**
   * une fen�tre interne qui affiche un calendrier
   */
  private SinaviFilleCalendrier sfc_;

  /**
   * Constructeur SinaviFilleLoiAleatoireTrajet.
   * 
   * @param _app une instance de SinaviImplementation
   * @param _trajet le trajet associ� � la loi
   * @param _loi la loi � afficher ou null si la loi n'existe pas
   */
  public SinaviFilleLoiAleatoireTrajet(final BuCommonImplementation _app, final ITrajetFluvial _trajet,
      final IGenerationJournaliereAleatoire _loi) {
    super("D�finition d'une loi de g�n�ration", false, false, false, false);
    trajet_ = _trajet;
    app_ = _app;
    loi_ = _loi;
    // Ajout des listener aux boutons
    bValider_.addActionListener(this);
    bAnnuler_.addActionListener(this);
    bVoirNaviresGeneres_.addActionListener(this);
    bChoixJourDebut_.addActionListener(this);
    bChoixJourFin_.addActionListener(this);
    // Contraintes du menu d�roulant
    cbOrdreLoiErlang_.setPreferredSize(new BuLabel("").getPreferredSize());
    // contraintesvaleurs par defaut des jours debut et fin
    final Dimension dimensionTexte_ = new Dimension(200, 27);
    tfAleatoireJourDebut_.setPreferredSize(dimensionTexte_);
    tfAleatoireJourFin_.setPreferredSize(dimensionTexte_);
    tfAleatoireJourDebut_.setDisplayFormat(new java.text.SimpleDateFormat("EEEE dd MMMM yyyy"));
    tfAleatoireJourFin_.setDisplayFormat(new java.text.SimpleDateFormat("EEEE dd MMMM yyyy"));
    tfAleatoireJourDebut_.setValue(new Date(SinaviImplementation.ETUDE_SINAVI.dateDebutSimulation() * 1000l));
    tfAleatoireJourFin_.setValue(new Date(SinaviImplementation.ETUDE_SINAVI.dateFinSimulation() * 1000l));
    // selection par defaut des checkbox
    cbLundi_.setSelected(true);
    cbMardi_.setSelected(true);
    cbMercredi_.setSelected(true);
    cbJeudi_.setSelected(true);
    cbVendredi_.setSelected(true);
    cbSamedi_.setSelected(true);
    cbDimanche_.setSelected(true);
    // Creation du menu deroulant permettant de choisir l'ordre de la loi d'Erlang
    cbOrdreLoiErlang_.addItem("Ordre 1");
    cbOrdreLoiErlang_.addItem("Ordre 2");
    cbOrdreLoiErlang_.addItem("Ordre 3");
    cbOrdreLoiErlang_.addItem("Ordre 4");
    cbOrdreLoiErlang_.addItem("Ordre 5");
    // remplissage de la fenetre
    ((JComponent) getContentPane()).setBorder(new EmptyBorder(25, 25, 25, 25));
    getContentPane().setLayout(new BorderLayout());
    pDonnees_.setLayout(new BuGridLayout(4, 10, 10));
    pDonnees_.add(cbLundi_);
    pDonnees_.add(cbMardi_);
    pDonnees_.add(cbMercredi_);
    pDonnees_.add(cbJeudi_);
    pDonnees_.add(cbVendredi_);
    pDonnees_.add(cbSamedi_);
    pDonnees_.add(cbDimanche_);
    pDonnees_.add(new BuLabel(""));
    pDonnees_.add(bChoixJourDebut_);
    pDonnees_.add(tfAleatoireJourDebut_);
    pDonnees_.add(bChoixJourFin_);
    pDonnees_.add(tfAleatoireJourFin_);
    pDonnees_.add(new BuLabel("Heure de d�but :"));
    pDonnees_.add(dfAleatoireHeureDebut_);
    pDonnees_.add(new BuLabel("Heure de fin :"));
    pDonnees_.add(dfAleatoireHeureFin_);
    pDonnees_.add(new BuLabel("loi d'Erlang :"));
    pDonnees_.add(cbOrdreLoiErlang_);
    pDonnees_.add(new BuLabel("Intervalle moyen :"));
    pDonnees_.add(dfIntervalleMoyen_);
    pDonnees_.add(new BuLabel("Navires G�n�r�s"));
    pDonnees_.add(bVoirNaviresGeneres_);
    pBoutons_.setLayout(new FlowLayout(FlowLayout.RIGHT));
    pBoutons_.add(bAnnuler_);
    pBoutons_.add(bValider_);
    getContentPane().add(pDonnees_, BorderLayout.CENTER);
    getContentPane().add(pBoutons_, BorderLayout.SOUTH);
    if (loi_ != null) {
      getLoiAleatoireParametres(loi_);
    }
    pack();
    setVisible(true);
  }

  /**
   * si action sur le bouton ChoixJourDebut affichage d'une fen�tre interne pour choisir le jour dans un calendrier si
   * action sur le bouton ChoixJourFin affichage d'une fen�tre interne pour choisir le jour dans un calendrier si action
   * sur le bouton VoirNaviresG�n�r�s et que la loi est pas null alors affichage d'une fen�tre interne dans laquelle
   * apparaissent les navires g�n�r�s si action sur le bouton valider lecture des param�tres de la loi et sauvegarde
   * avec fermeture de la fen�tre si action sur le bouton annuler fermeture de la fen�tre
   * 
   * @param _e
   */
  public void actionPerformed(final ActionEvent _e) {
    if (_e.getSource() == bChoixJourDebut_) {
      if (sfc_ == null) {
        sfc_ = new SinaviFilleCalendrier(tfAleatoireJourDebut_);
        app_.addInternalFrame(sfc_);
      } else {
        sfc_.setVisible(false);
        sfc_ = new SinaviFilleCalendrier(tfAleatoireJourDebut_);
        app_.addInternalFrame(sfc_);
      }
    } else if (_e.getSource() == bChoixJourFin_) {
      if (sfc_ == null) {
        sfc_ = new SinaviFilleCalendrier(tfAleatoireJourFin_);
        app_.addInternalFrame(sfc_);
      } else {
        sfc_.setVisible(false);
        sfc_ = new SinaviFilleCalendrier(tfAleatoireJourFin_);
        app_.addInternalFrame(sfc_);
      }
    } else if (_e.getSource() == bVoirNaviresGeneres_) {
      if (loi_ != null) {
        if (loi_.instantsGeneres().length == 0) {
          final BuDialogMessage dialog_mess = new BuDialogMessage(app_, SinaviImplementation.informationsSoftware(),
              "navires non g�n�r�s :\nallez dans le menu simulation\n et s�lectionnez \"G�n�rer Navires\"");
          dialog_mess.activate();
        } else {
          app_.addInternalFrame(new SinaviFilleEditionNaviresGeneres(loi_));
        }
      }
    } else if (_e.getSource() == bValider_) {
      String test_ = "";
      if (loi_ == null) {
        final IGenerationJournaliereAleatoire creneauGenerationAleatoire_ = UsineLib.findUsine()
            .creeNavigationGenerationJournaliereAleatoire();
        creneauGenerationAleatoire_.definitHeuresDebutFin(dfAleatoireHeureDebut_.getDureeField(), dfAleatoireHeureFin_
            .getDureeField());
        creneauGenerationAleatoire_.definitDatesDebutFin(
            ((int) (((Date) tfAleatoireJourDebut_.getValue()).getTime() / 1000l)), ((int) (((Date) tfAleatoireJourFin_
                .getValue()).getTime() / 1000l)));
        creneauGenerationAleatoire_.valeurMoyenne(dfIntervalleMoyen_.getDureeField());
        creneauGenerationAleatoire_.definitJoursActifs(cbLundi_.isSelected(), cbLundi_.isSelected(), cbMardi_
            .isSelected(), cbJeudi_.isSelected(), cbVendredi_.isSelected(), cbSamedi_.isSelected(), cbDimanche_
            .isSelected());
        if (cbOrdreLoiErlang_.getSelectedItem() == "Ordre 1") {
          creneauGenerationAleatoire_.ordreLoi(1);
        }
        if (cbOrdreLoiErlang_.getSelectedItem() == "Ordre 2") {
          creneauGenerationAleatoire_.ordreLoi(2);
        }
        if (cbOrdreLoiErlang_.getSelectedItem() == "Ordre 3") {
          creneauGenerationAleatoire_.ordreLoi(3);
        }
        if (cbOrdreLoiErlang_.getSelectedItem() == "Ordre 4") {
          creneauGenerationAleatoire_.ordreLoi(4);
        }
        if (cbOrdreLoiErlang_.getSelectedItem() == "Ordre 5") {
          creneauGenerationAleatoire_.ordreLoi(5);
        }
        if (((Date) tfAleatoireJourDebut_.getValue()).before(new Date(SinaviImplementation.ETUDE_SINAVI
            .dateDebutSimulation() * 1000l))) {
          final BuDialogMessage dialog_mess = new BuDialogMessage(app_, SinaviImplementation.informationsSoftware(),
              "le jour de d�but de la loi est\n ant�rieur au jour de d�but\nde la simulation");
          dialog_mess.activate();
          test_ = "erreur debut";
        }
        if (((Date) tfAleatoireJourFin_.getValue()).after(new Date(SinaviImplementation.ETUDE_SINAVI
            .dateFinSimulation() * 1000l))) {
          final BuDialogMessage dialog_mess = new BuDialogMessage(app_, SinaviImplementation.informationsSoftware(),
              "le jour de fin de la loi est\n post�rieur au jour de fin\nde la simulation");
          dialog_mess.activate();
          test_ = "erreur fin";
        }
        if (test_.equals("")) {
          test_ = trajet_.ajouteGeneration(creneauGenerationAleatoire_);
        }
        if (test_.equals("")) {
          SinaviImplementation.SINAVITABLELOIMODEL.refresh();
          try {
            setClosed(true);
          } catch (final Exception _pve) {}
        } else {
          System.out.println(test_);
        }
      } else {
        if (((Date) tfAleatoireJourDebut_.getValue()).before(new Date(SinaviImplementation.ETUDE_SINAVI
            .dateDebutSimulation() * 1000l))) {
          final BuDialogMessage dialog_mess = new BuDialogMessage(app_, SinaviImplementation.informationsSoftware(),
              "le jour de d�but de la loi est\n ant�rieur au jour de d�but\nde la simulation");
          dialog_mess.activate();
          test_ = "erreur debut";
        }
        if (((Date) tfAleatoireJourFin_.getValue()).after(new Date(SinaviImplementation.ETUDE_SINAVI
            .dateFinSimulation() * 1000l))) {
          final BuDialogMessage dialog_mess = new BuDialogMessage(app_, SinaviImplementation.informationsSoftware(),
              "le jour de fin de la loi est\n post�rieur au jour de fin\nde la simulation");
          dialog_mess.activate();
          test_ = "erreur fin";
        }
        loi_.definitHeuresDebutFin(dfAleatoireHeureDebut_.getDureeField(), dfAleatoireHeureFin_.getDureeField());
        loi_.valeurMoyenne(dfIntervalleMoyen_.getDureeField());
        loi_.definitDatesDebutFin(((int) (((Date) tfAleatoireJourDebut_.getValue()).getTime() / 1000l)),
            ((int) (((Date) tfAleatoireJourFin_.getValue()).getTime() / 1000l)));
        loi_.definitJoursActifs(cbLundi_.isSelected(), cbLundi_.isSelected(), cbMardi_.isSelected(), cbJeudi_
            .isSelected(), cbVendredi_.isSelected(), cbSamedi_.isSelected(), cbDimanche_.isSelected());
        if (cbOrdreLoiErlang_.getSelectedItem() == "Ordre 1") {
          loi_.ordreLoi(1);
        }
        if (cbOrdreLoiErlang_.getSelectedItem() == "Ordre 2") {
          loi_.ordreLoi(2);
        }
        if (cbOrdreLoiErlang_.getSelectedItem() == "Ordre 3") {
          loi_.ordreLoi(3);
        }
        if (cbOrdreLoiErlang_.getSelectedItem() == "Ordre 4") {
          loi_.ordreLoi(4);
        }
        if (cbOrdreLoiErlang_.getSelectedItem() == "Ordre 5") {
          loi_.ordreLoi(5);
        }
        test_ += loi_.valide();
        if (test_.equals("")) {
          SinaviImplementation.SINAVITABLELOIMODEL.refresh();
          try {
            setClosed(true);
          } catch (final Exception _pve) {}
        } else {
          System.out.println(test_);
        }
      }
    } else {
      try {
        setClosed(true);
      } catch (final Exception _pve) {}
    }
  }

  /**
   * mise a jour des champs de la fen�tre avec ceux de la loi pass�e en param�tres
   * 
   * @param _loi la loi � afficher
   */
  private void getLoiAleatoireParametres(final IGenerationJournaliereAleatoire _loi) {
    dfAleatoireHeureDebut_.setDureeField((int) _loi.heureDebut());
    dfAleatoireHeureFin_.setDureeField((int) _loi.heureFin());
    dfIntervalleMoyen_.setDureeField(new Double(_loi.valeurMoyenne()).intValue());
    tfAleatoireJourDebut_.setValue(new Date(_loi.dateDebut() * 1000l));
    tfAleatoireJourFin_.setValue(new Date(_loi.dateFin() * 1000l));
    if (_loi.ordreLoi() == 1) {
      cbOrdreLoiErlang_.setSelectedItem("Ordre 1");
    }
    if (_loi.ordreLoi() == 2) {
      cbOrdreLoiErlang_.setSelectedItem("Ordre 2");
    }
    if (_loi.ordreLoi() == 3) {
      cbOrdreLoiErlang_.setSelectedItem("Ordre 3");
    }
    if (_loi.ordreLoi() == 4) {
      cbOrdreLoiErlang_.setSelectedItem("Ordre 4");
    }
    if (_loi.ordreLoi() == 5) {
      cbOrdreLoiErlang_.setSelectedItem("Ordre 5");
    }
    if (_loi.lundi()) {
      cbLundi_.setSelected(true);
    } else {
      cbLundi_.setSelected(false);
    }
    if (_loi.mardi()) {
      cbMardi_.setSelected(true);
    } else {
      cbMardi_.setSelected(false);
    }
    if (_loi.mercredi()) {
      cbMercredi_.setSelected(true);
    } else {
      cbMercredi_.setSelected(false);
    }
    if (_loi.jeudi()) {
      cbJeudi_.setSelected(true);
    } else {
      cbJeudi_.setSelected(false);
    }
    if (_loi.vendredi()) {
      cbVendredi_.setSelected(true);
    } else {
      cbVendredi_.setSelected(false);
    }
    if (_loi.samedi()) {
      cbSamedi_.setSelected(true);
    } else {
      cbSamedi_.setSelected(false);
    }
    if (_loi.dimanche()) {
      cbDimanche_.setSelected(true);
    } else {
      cbDimanche_.setSelected(false);
    }
  }
}
