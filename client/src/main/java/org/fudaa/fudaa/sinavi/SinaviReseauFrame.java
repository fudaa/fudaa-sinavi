/*
 * @file         SinaviReseauFrame.java
 * @creation     2001-05-17
 * @modification $Date: 2006-09-19 15:09:00 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.sinavi;

import java.awt.event.ActionEvent;

import javax.swing.JComponent;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuCommonImplementation;
import com.memoire.bu.BuDialogMessage;
import com.memoire.dja.DjaFrame;
import com.memoire.dja.DjaGrid;
import com.memoire.dja.DjaGridInteractive;
import com.memoire.dja.DjaObject;

import org.fudaa.dodico.corba.navigation.IBiefNavigation;
import org.fudaa.dodico.corba.navigation.IEcluseFluviale;
import org.fudaa.dodico.corba.navigation.IGare;

import org.fudaa.dodico.objet.UsineLib;

/**
 * impl�mentation de la fen�tre interne permettant de repr�senter le r�seau
 * 
 * @version $Revision: 1.12 $ $Date: 2006-09-19 15:09:00 $ by $Author: deniger $
 * @author Aline Marechalle , Franck Lejeune
 */
public class SinaviReseauFrame extends DjaFrame {

  /**
   * panel nord pour l'animation
   */
  public static SinaviAnimationNord coteNord = new SinaviAnimationNord();
  /**
   * panel sud pour l'animation
   */
  public static SinaviAnimationSud coteSud = new SinaviAnimationSud();
  /**
   * bouton pour ajouter un bief sur le r�seau ( la grille )
   */
  private final BuButton bbief_ = new BuButton(SinaviResource.SINAVI.getIcon("sinaviconnexionbief"));
  /**
   * bouton pour ajouter une �cluse sur le r�seau ( la grille )
   */
  private final BuButton becluse_ = new BuButton(SinaviResource.SINAVI.getIcon("sinaviconnexionecluse"));
  /**
   * bouton pour ajouter un egare sur le r�seau ( la grille )
   */
  private final BuButton bgare_ = new BuButton(SinaviResource.SINAVI.getIcon("sinaviconnexiongare"));
  /**
   * une instance de SinaviImplementation
   */
  private BuCommonImplementation app_;

  /**
   * Constructeur SinaviReseauFrame
   * 
   * @param _app une instance de SinaviImpl�mentation
   * @param _file le titre de la fen�tre interne
   * @param _grid la grille interactive
   */
  public SinaviReseauFrame(final BuCommonImplementation _app, final String _file, final DjaGrid _grid) {
    super(_app, _file, _grid, false);
    app_ = _app;
    getContentPane().add("North", coteNord);
    getContentPane().add("South", coteSud);
    coteNord.setVisible(false);
    coteSud.setVisible(false);
    bbief_.setToolTipText("Ajouter un bief");
    bbief_.setActionCommand("SINAVI_CREATE_BIEF");
    bgare_.setToolTipText("Ajouter une gare");
    bgare_.setActionCommand("SINAVI_CREATE_GARE");
    becluse_.setToolTipText("Ajouter une ecluse");
    becluse_.setActionCommand("SINAVI_CREATE_ECLUSE");
    bbief_.addActionListener(this);
    bgare_.addActionListener(this);
    becluse_.addActionListener(this);
  }

  /**
   * permet de cr�er un menu de bouton sp�cifique � cette fen�tre sous celui existant
   * 
   * @return un tableau des boutons � ajouter au menu
   */
  public JComponent[] getSpecificTools() {
    JComponent[] r_;
    r_ = new JComponent[10];
    r_[0] = tbInteractive_;
    r_[1] = null;
    r_[2] = btRecenter_;
    r_[3] = btForeground_;
    r_[4] = btBackground_;
    r_[5] = btTextColor_;
    r_[6] = null;
    r_[7] = bbief_;
    r_[8] = bgare_;
    r_[9] = becluse_;
    return r_;
  }

  /**
   * m�thode retournant les outils valides
   */
  public String[] getEnabledActions() {
    final String[] r = new String[] { "IMPRIMER", "PREVISUALISER", "COUPER", "COPIER", "COLLER", "DUPLIQUER",
        "TOUTSELECTIONNER", };
    return r;
  }

  /**
   * m�thode permettant d'ajouter une gare � la fois au r�seau mais aussi � l'�tude
   */
  public void ajouteGare() {
    final IGare gare = UsineLib.findUsine().creeNavigationGare();
    SinaviImplementation.ETUDE_SINAVI.reseau().ajouteGare(gare);
    grid_.add(new SinaviReseauGare(gare));
    SinaviImplementation.SINAVIGARELISTMODEL.refresh();
    repaint();
  }

  /**
   * m�thode permettant d'ajouter un bief � la fois au r�seau mais aussi � l'�tude on ne peut ajouter un bief qu'entre
   * deux gares s�lectionn�es
   * 
   * @param _grilleinteractive la grille de la fen�tre
   */
  public void ajouteBief(final DjaGridInteractive _grilleinteractive) {
    final IBiefNavigation bief_ = UsineLib.findUsine().creeNavigationBiefNavigation();
    bief_.regle(UsineLib.findUsine().creeNavigationRegleNavigationFluviale());
    if (_grilleinteractive.getSelection().size() == 2) {
      final DjaObject obj1_ = (DjaObject) _grilleinteractive.getSelection().elementAt(0);
      final DjaObject obj2_ = (DjaObject) _grilleinteractive.getSelection().elementAt(1);
      if ((obj1_ instanceof SinaviReseauGare) && (obj2_ instanceof SinaviReseauGare)) {
        final SinaviReseauBief srb_ = new SinaviReseauBief(bief_);
        if (obj1_.getX() > obj2_.getX()) {
          final int in_ = ((SinaviReseauGare) obj1_).AttacheEntreeLibre();
          if (in_ == -1) {
            final BuDialogMessage dialog_mess = new BuDialogMessage(app_, SinaviImplementation.informationsSoftware(),
                "ATTENTION : Le nombre maximun d'entree\nest atteint (5)");
            dialog_mess.activate();
            // System.out.println("ATTENTION : Le nombre maximun d'entree est atteint (5)");
            return;
          }
          final int out_ = ((SinaviReseauGare) obj2_).AttacheSortieLibre();
          if (out_ == -1) {
            final BuDialogMessage dialog_mess = new BuDialogMessage(app_, SinaviImplementation.informationsSoftware(),
                "ATTENTION : Le nombre maximun de sortie\nest atteint (5)");
            dialog_mess.activate();
            // System.out.println("ATTENTION : Le nombre maximun de sortie est atteint (5)");
            return;
          }
          srb_.setBeginPosition(in_);
          srb_.setEndPosition(out_);
          srb_.setBeginObject(obj1_);
          srb_.setEndObject(obj2_);
          bief_.definitGareAval(((SinaviReseauGare) obj2_).getGare());
          bief_.definitGareAmont(((SinaviReseauGare) obj1_).getGare());
        } else {
          final int in_ = ((SinaviReseauGare) obj2_).AttacheEntreeLibre();
          if (in_ == -1) {
            final BuDialogMessage dialog_mess = new BuDialogMessage(app_, SinaviImplementation.informationsSoftware(),
                "ATTENTION : Le nombre maximun d'entree\nest atteint (5)");
            dialog_mess.activate();
            // System.out.println("ATTENTION : Le nombre maximun d'entree est atteint (5)");
            return;
          }
          final int out_ = ((SinaviReseauGare) obj1_).AttacheSortieLibre();
          if (out_ == -1) {
            final BuDialogMessage dialog_mess = new BuDialogMessage(app_, SinaviImplementation.informationsSoftware(),
                "ATTENTION : Le nombre maximun de sortie\nest atteint (5)");
            dialog_mess.activate();
            // System.out.println("ATTENTION : Le nombre maximun de sortie est atteint (5)");
            return;
          }
          // System.out.println("else numero d'attache en entree " + in_);
          srb_.setBeginPosition(in_);
          // System.out.println("else numero d'attache en sortie " + out_);
          srb_.setEndPosition(out_);
          srb_.setBeginObject(obj2_);
          srb_.setEndObject(obj1_);
          bief_.definitGareAval(((SinaviReseauGare) obj1_).getGare());
          bief_.definitGareAmont(((SinaviReseauGare) obj2_).getGare());
        }
        grid_.add(srb_);
        SinaviImplementation.ETUDE_SINAVI.reseau().ajouteBief(bief_);
        SinaviImplementation.SINAVIBIEFLISTMODEL.refresh();
        repaint();
      } else {
        final BuDialogMessage dialog_mess = new BuDialogMessage(app_, SinaviImplementation.informationsSoftware(),
            "Erreur : ce n'est pas deux gares\nqui sont s�lectionn�es");
        dialog_mess.activate();
        // System.out.println("Erreur : ce n'est pas deux gares qui sont selectionneees");
      }
    } else {
      final BuDialogMessage dialog_mess = new BuDialogMessage(app_, SinaviImplementation.informationsSoftware(),
          "Erreur : il n'y a pas deux �l�ments\ns�lectionn�s");
      dialog_mess.activate();
      // System.out.println("Erreur : il n'y a pas deux elements selectionnes");
    }
  }

  /**
   * m�thode permettant d'ajouter une �cluse � la fois au r�seau mais aussi � l'�tude on ne peut ajouter une �cluse
   * qu'entre deux gares s�lectionn�es
   * 
   * @param _grilleinteractive la grille de la fen�tre
   */
  public void ajouteEcluse(final DjaGridInteractive _grilleinteractive) {
    final IEcluseFluviale eclusefluviale_ = UsineLib.findUsine().creeNavigationEcluseFluviale();
    if (_grilleinteractive.getSelection().size() == 2) {
      final DjaObject obj1_ = (DjaObject) _grilleinteractive.getSelection().elementAt(0);
      final DjaObject obj2_ = (DjaObject) _grilleinteractive.getSelection().elementAt(1);
      if ((obj1_ instanceof SinaviReseauGare) && (obj2_ instanceof SinaviReseauGare)) {
        final SinaviReseauEcluse sre_ = new SinaviReseauEcluse(eclusefluviale_);
        if (obj1_.getX() > obj2_.getX()) {
          final int in_ = ((SinaviReseauGare) obj1_).AttacheEntreeLibre();
          if (in_ == -1) {
            final BuDialogMessage dialog_mess = new BuDialogMessage(app_, SinaviImplementation.informationsSoftware(),
                "ATTENTION : Le nombre maximun d'entree\nest atteint (5)");
            dialog_mess.activate();
            // System.out.println("ATTENTION : Le nombre maximun d'entree est atteint (5)");
            return;
          }
          final int out_ = ((SinaviReseauGare) obj2_).AttacheSortieLibre();
          if (out_ == -1) {
            final BuDialogMessage dialog_mess = new BuDialogMessage(app_, SinaviImplementation.informationsSoftware(),
                "ATTENTION : Le nombre maximun de sortie\nest atteint (5)");
            dialog_mess.activate();
            // System.out.println("ATTENTION : Le nombre maximun de sortie est atteint (5)");
            return;
          }
          sre_.setBeginPosition(in_);
          sre_.setEndPosition(out_);
          sre_.setBeginObject(obj1_);
          sre_.setEndObject(obj2_);
          eclusefluviale_.definitGareAval(((SinaviReseauGare) obj2_).getGare());
          eclusefluviale_.definitGareAmont(((SinaviReseauGare) obj1_).getGare());
        } else {
          final int in_ = ((SinaviReseauGare) obj2_).AttacheEntreeLibre();
          if (in_ == -1) {
            final BuDialogMessage dialog_mess = new BuDialogMessage(app_, SinaviImplementation.informationsSoftware(),
                "ATTENTION : Le nombre maximun d'entree\nest atteint (5)");
            dialog_mess.activate();
            // System.out.println("ATTENTION : Le nombre maximun d'entree est atteint (5)");
            return;
          }
          final int out_ = ((SinaviReseauGare) obj1_).AttacheSortieLibre();
          if (out_ == -1) {
            final BuDialogMessage dialog_mess = new BuDialogMessage(app_, SinaviImplementation.informationsSoftware(),
                "ATTENTION : Le nombre maximun de sortie\nest atteint (5)");
            dialog_mess.activate();
            // System.out.println("ATTENTION : Le nombre maximun de sortie est atteint (5)");
            return;
          }
          sre_.setBeginPosition(in_);
          sre_.setEndPosition(out_);
          sre_.setBeginObject(obj2_);
          sre_.setEndObject(obj1_);
          eclusefluviale_.definitGareAval(((SinaviReseauGare) obj1_).getGare());
          eclusefluviale_.definitGareAmont(((SinaviReseauGare) obj2_).getGare());
        }
        grid_.add(sre_);
        SinaviImplementation.ETUDE_SINAVI.reseau().ajouteEcluse(eclusefluviale_);
        SinaviImplementation.SINAVIECLUSELISTMODEL.refresh();
        repaint();
      } else {
        final BuDialogMessage dialog_mess = new BuDialogMessage(app_, SinaviImplementation.informationsSoftware(),
            "Erreur : ce n'est pas deux gares\nqui sont s�lectionn�es");
        dialog_mess.activate();
        // System.out.println("Erreur : ce n'est pas deux gares qui sont selectionneees");
      }
    } else {
      final BuDialogMessage dialog_mess = new BuDialogMessage(app_, SinaviImplementation.informationsSoftware(),
          "Erreur : il n'y a pas deux �l�ments\ns�lectionn�s");
      dialog_mess.activate();
      // System.out.println("Erreur : il n'y a pas deux elements selectionnes");
    }
  }

  /**
   * si clic sur le bouton gare ajout d'une gare si clic sur le bouton ecluse ajout d'une �cluse si clic sur le bouton
   * bief ajout d'un bief
   * 
   * @param _evt
   */
  public void actionPerformed(final ActionEvent _evt) {
    final String action = _evt.getActionCommand();
    final DjaGridInteractive grille_ = (DjaGridInteractive) grid_;
    if ("DJA_TOGGLE_INTERACTIVE".equals(action)) {
      /*
       * if(grille_.isInteractive()) { Enumeration enum_ = grille_.getObjects().elements();
       * while(enum_.hasMoreElements()) { System.out.println(enum_.nextElement()); } }
       */
    }
    if ("SINAVI_CREATE_BIEF".equals(action) && (grille_.isInteractive())) {
      ajouteBief((DjaGridInteractive) grid_);
    }
    if ("SINAVI_CREATE_GARE".equals(action) && (grille_.isInteractive())) {
      ajouteGare();
    }
    if ("SINAVI_CREATE_ECLUSE".equals(action) && (grille_.isInteractive())) {
      ajouteEcluse((DjaGridInteractive) grid_);
    }
    super.actionPerformed(_evt);
  }

  /**
   * m�thode permettant de d�sactiver le qui autorise � passer en mode int�ractif
   * 
   * @param entree_ bool�en pour l'activation ou la d�sactivation
   */
  public void activerBoutonInteractive(final boolean entree_) {
    tbInteractive_.setEnabled(entree_);
  }
}
