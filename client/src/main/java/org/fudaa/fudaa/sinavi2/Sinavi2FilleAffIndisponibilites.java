package org.fudaa.fudaa.sinavi2;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.ListIterator;

import javax.swing.JComponent;
import javax.swing.border.EmptyBorder;
import javax.swing.event.InternalFrameEvent;
import javax.swing.event.InternalFrameListener;

import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuComboBox;
import com.memoire.bu.BuCommonImplementation;
import com.memoire.bu.BuInternalFrame;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuScrollPane;
import com.memoire.bu.BuTable;
import com.memoire.bu.BuTableSortModel;
import com.memoire.fu.FuLog;

import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.table.CtuluTableExcelWriter;

import org.fudaa.dodico.corba.sinavi2.SParametresBief;
import org.fudaa.dodico.corba.sinavi2.SParametresEcluse;
import org.fudaa.dodico.corba.sinavi2.SParametresIndispoLoiD;
import org.fudaa.dodico.corba.sinavi2.SParametresTrajets;

/**
 * Cette classe permet d'afficher les indisponibilit�s et de r�pondre aux fonctions usuelles d�finies dans toutes les
 * fen�tres df. Sinavi2FilleAddModIndisponibilites
 * 
 * @version $Revision: 1.7 $ $Date: 2006-09-19 15:08:58 $ by $Author: deniger $
 * @author Fatimatou Ka , Benoit Maneuvrier
 */

public class Sinavi2FilleAffIndisponibilites extends BuInternalFrame implements ActionListener, InternalFrameListener {
  // private static final int AUTO_RESIZE_ALL_COLUMNS = 0;

  private final BuLabel lTitre_ = new BuLabel("Affichage des indisponibilit�s");

  private final BuButton bAnnuler_ = new BuButton(Sinavi2Resource.SINAVI2.getIcon("QUITTER"), "Quitter");
  private final BuButton bAjouter_ = new BuButton(Sinavi2Resource.SINAVI2.getIcon("AJOUTER"), "Ajouter");
  private final BuButton bModifier_ = new BuButton(Sinavi2Resource.SINAVI2.getIcon("MODIFIER"), "Modifier");
  private final BuButton bSupprimer_ = new BuButton(Sinavi2Resource.SINAVI2.getIcon("SUPPRIMER"), "Suppimer");
  private final BuButton bImprimer_ = new BuButton(Sinavi2Resource.SINAVI2.getIcon("IMPRIMER"), "Imprimer");
  private final BuButton bMiseAJour_ = new BuButton(Sinavi2Resource.SINAVI2.getIcon("MISAJOUR"), "Mise � Jour");

  /** **gerer la liste des trajets **** */
  public ArrayList listetrajets2_;

  public SParametresTrajets trajetcourant_;

  /** * panel contenant les boutons, il est plac� en bas */

  private final BuPanel pBoutons_ = new BuPanel();
  /**
   * panel contenant les diff�rents champs de la fen�tre
   */
  private final BuPanel pTitre_ = new BuPanel();

  private final BuPanel pDonnees2_ = new BuPanel();

  /**
   * une instance de SinaviImplementation
   */
  /**
   * Constructeur
   * 
   * @param _app une instance de SinaviImplementation
   */
  private static final int AUTO_RESIZE_ALL_COLUMNS = 0;
  private final BuLabel lElement_ = new BuLabel("Element");
  private final BuComboBox coElement_ = new BuComboBox();
  public Sinavi2TableauIndisponibilites tb_;
  public BuTable table_;
  public BuTableSortModel tabTrajets_;
  public Sinavi2Implementation imp_ = null;
  public BuScrollPane scrollPane_;

  // public TableColumn column=null;
  public Sinavi2FilleAffIndisponibilites(final BuCommonImplementation _appli, final int _elem) {

    super("Affichage des Indisponibilit�s", true, true, true, false);
    imp_ = (Sinavi2Implementation) _appli.getImplementation();
    pTitre_.add(lTitre_);
    pTitre_.add(coElement_);

    coElement_.addItem("Toutes");

    if (imp_.listeBiefs_ != null) {
      final ListIterator it = imp_.listeBiefs_.listIterator();
      while (it.hasNext()) {
        final String s = "bie:" + ((SParametresBief) it.next()).identification;
        coElement_.addItem(s);
      }
    }

    if (imp_.listeEcluses_ != null) {
      final ListIterator it = imp_.listeEcluses_.listIterator();
      while (it.hasNext()) {
        final String s = "ecl:" + ((SParametresEcluse) it.next()).identification;
        coElement_.addItem(s);
      }
    }
    coElement_.setSelectedIndex(_elem);
    miseajour();
    table_.setRowSelectionAllowed(true);
    table_.setAutoResizeMode(AUTO_RESIZE_ALL_COLUMNS);
    table_.setColumnSelectionAllowed(false);
    scrollPane_ = new BuScrollPane(table_);
    pDonnees2_.add(scrollPane_);

    bAnnuler_.addActionListener(this);
    bAjouter_.addActionListener(this);
    bModifier_.addActionListener(this);
    bSupprimer_.addActionListener(this);
    bImprimer_.addActionListener(this);
    bMiseAJour_.addActionListener(this);

    ((JComponent) getContentPane()).setBorder(new EmptyBorder(25, 25, 25, 25));

    pBoutons_.setLayout(new FlowLayout(FlowLayout.CENTER));
    pBoutons_.add(bAnnuler_);
    pBoutons_.add(bAjouter_);
    pBoutons_.add(bModifier_);
    pBoutons_.add(bSupprimer_);
    pBoutons_.add(bImprimer_);
    pBoutons_.add(bMiseAJour_);
    // FuLog.debug("test1");
    getContentPane().add(pTitre_, BorderLayout.NORTH);
    getContentPane().add(pDonnees2_, BorderLayout.CENTER);
    getContentPane().add(pBoutons_, BorderLayout.SOUTH);
    // FuLog.debug("test1");
    //

    pack();

    /** *reinitialisation des valeurs* */

    setVisible(true);
    addInternalFrameListener(this);
    imp_.addInternalFrame(this);

  }

  public String heureCorrecte(final int _heure, final int _minutes) {
    // String s=_heure+","+_minutes;
    // return SinaviLib.deuxChiffresApresVirgule(s);
    return HeureMinute(_heure, _minutes);
  }

  public void miseajour() {
    // FuLog.debug("test1 ");
    tb_ = new Sinavi2TableauIndisponibilites();
    int row = 1;
    if (coElement_.getSelectedIndex() == 0) {
      // FuLog.debug("testelem 0 ");
      if (imp_.listeBiefs_ != null) {

        // FuLog.debug("test bief non null ");
        final ListIterator iter = imp_.listeBiefs_.listIterator();

        while (iter.hasNext()) {

          final SParametresBief el = (SParametresBief) iter.next();
          // il faut regarder les trois lois
          // FuLog.debug("test bief avant test loid ");
          if (el.loiD != null) {
            FuLog.debug("test loid non null " + el.loiD.length);

            for (int i = 0; i < el.loiD.length; i++) {
              tb_.setValueAt("bie:" + el.identification, row, 0);
              tb_.setValueAt("D", row, 1);
              tb_.setValueAt("" + el.loiD[i].dateDebut, row, 2);

              System.out.println("test affi sate fin  " + el.loiD[i].dateFin);
              tb_.setValueAt(heureCorrecte(el.loiD[i].heureDebut.heure, el.loiD[i].heureDebut.minutes), row, 3);
              tb_.setValueAt("" + el.loiD[i].dateFin, row, 4);
              tb_.setValueAt(heureCorrecte(el.loiD[i].heureFin.heure, el.loiD[i].heureFin.minutes), row, 5);

              row++;
            }
          }
          if (el.loiE.ordreDuree != 0) {
            FuLog.debug("test loie");
            tb_.setValueAt("bie:" + el.identification, row, 0);
            tb_.setValueAt("E", row, 1);
            tb_.setValueAt("" + el.loiE.ordreEcart, row, 6);
            tb_.setValueAt("" + el.loiE.nbJoursDEcart, row, 7);
            tb_.setValueAt("" + el.loiE.ordreDuree, row, 8);
            tb_.setValueAt(heureCorrecte(el.loiE.duree.heure, el.loiE.duree.minutes), row, 9);
            row++;
          }
          if (!(el.loiJ.heureFin.heure == 0 && el.loiJ.heureFin.minutes == 0)) {
            FuLog.debug("test loi j ");
            tb_.setValueAt("bie:" + el.identification, row, 0);
            tb_.setValueAt("J", row, 1);
            tb_.setValueAt(heureCorrecte(el.loiJ.heureDebut.heure, el.loiJ.heureDebut.minutes), row, 10);
            tb_.setValueAt(heureCorrecte(el.loiJ.heureFin.heure, el.loiJ.heureFin.minutes), row, 11);

            row++;
          }
        }
      }
      if (imp_.listeEcluses_ != null) {
        final ListIterator iter = imp_.listeEcluses_.listIterator();
        // int row=1;
        while (iter.hasNext()) {
          final SParametresEcluse el = (SParametresEcluse) iter.next();
          // il faut regarder les trois lois
          if (el.loiD != null) {

            for (int i = 0; i < el.loiD.length; i++) {
              tb_.setValueAt("ecl:" + el.identification, row, 0);
              tb_.setValueAt("D", row, 1);
              tb_.setValueAt("" + el.loiD[i].dateDebut, row, 2);

              tb_.setValueAt(heureCorrecte(el.loiD[i].heureDebut.heure, el.loiD[i].heureDebut.minutes), row, 3);
              tb_.setValueAt("" + el.loiD[i].dateFin, row, 4);
              tb_.setValueAt(heureCorrecte(el.loiD[i].heureFin.heure, el.loiD[i].heureFin.minutes), row, 5);
              row++;
            }
          }
          if (el.loiE.ordreDuree != 0) {
            tb_.setValueAt("ecl:" + el.identification, row, 0);
            tb_.setValueAt("E", row, 1);
            tb_.setValueAt("" + el.loiE.ordreEcart, row, 6);
            tb_.setValueAt("" + el.loiE.nbJoursDEcart, row, 7);
            tb_.setValueAt("" + el.loiE.ordreDuree, row, 8);
            tb_.setValueAt(heureCorrecte(el.loiE.duree.heure, el.loiE.duree.minutes), row, 9);

            row++;
          }
          if (el.loiJ.heureFin.heure != 0 && el.loiJ.heureFin.minutes != 0) {
            tb_.setValueAt("ecl:" + el.identification, row, 0);
            tb_.setValueAt("J", row, 1);
            tb_.setValueAt(heureCorrecte(el.loiJ.heureDebut.heure, el.loiJ.heureDebut.minutes), row, 10);
            tb_.setValueAt(heureCorrecte(el.loiJ.heureFin.heure, el.loiJ.heureFin.minutes), row, 11);

            row++;
          }
        }
      }

    } else {// on a un �l�ment de choisi
      final String prec = coElement_.getSelectedItem().toString().substring(0, 4);
      final String elem = coElement_.getSelectedItem().toString().substring(4);
      // int row=1;
      if (prec.equalsIgnoreCase("bie:")) {
        final ListIterator iter = imp_.listeBiefs_.listIterator();
        boolean trouve = false;
        while (iter.hasNext() && !trouve) {
          final SParametresBief el = (SParametresBief) iter.next();
          if (el.identification.equalsIgnoreCase(elem)) {
            trouve = true;
            FuLog.debug("avant loi ");
            if (el.loiD != null) {
              FuLog.debug("loi D");
              for (int i = 0; i < el.loiD.length; i++) {
                tb_.setValueAt("ecl:" + el.identification, row, 0);
                tb_.setValueAt("D", row, 1);
                tb_.setValueAt("" + el.loiD[i].dateDebut, row, 2);

                tb_.setValueAt(heureCorrecte(el.loiD[i].heureDebut.heure, el.loiD[i].heureDebut.minutes), row, 3);
                tb_.setValueAt("" + el.loiD[i].dateFin, row, 4);
                tb_.setValueAt(heureCorrecte(el.loiD[i].heureFin.heure, el.loiD[i].heureFin.minutes), row, 5);
                row++;
              }
            }
            if (el.loiE.ordreDuree != 0) {
              FuLog.debug("test loi E ");
              tb_.setValueAt("ecl:" + el.identification, row, 0);
              tb_.setValueAt("E", row, 1);
              tb_.setValueAt("" + el.loiE.ordreEcart, row, 6);
              tb_.setValueAt("" + el.loiE.nbJoursDEcart, row, 7);
              tb_.setValueAt("" + el.loiE.ordreDuree, row, 8);
              tb_.setValueAt(heureCorrecte(el.loiE.duree.heure, el.loiE.duree.minutes), row, 9);

              row++;
            }
            if (!(el.loiJ.heureFin.heure == 0 && el.loiJ.heureFin.minutes == 0)) {
              FuLog.debug("test loi J");
              tb_.setValueAt("ecl:" + el.identification, row, 0);
              tb_.setValueAt("J", row, 1);
              tb_.setValueAt(heureCorrecte(el.loiJ.heureDebut.heure, el.loiJ.heureDebut.minutes), row, 10);
              tb_.setValueAt(heureCorrecte(el.loiJ.heureFin.heure, el.loiJ.heureFin.minutes), row, 11);

            }
          }
        }
      } else {
        final ListIterator iter = imp_.listeEcluses_.listIterator();
        boolean trouve = false;
        while (iter.hasNext() && !trouve) {
          final SParametresEcluse el = (SParametresEcluse) iter.next();
          if (el.identification.equalsIgnoreCase(elem)) {
            trouve = true;
            if (el.loiD != null) {
              for (int i = 0; i < el.loiD.length; i++) {
                tb_.setValueAt("ecl:" + el.identification, row, 0);
                tb_.setValueAt("D", row, 1);
                tb_.setValueAt("" + el.loiD[i].dateDebut, row, 2);

                tb_.setValueAt(heureCorrecte(el.loiD[i].heureDebut.heure, el.loiD[i].heureDebut.minutes), row, 3);
                tb_.setValueAt("" + el.loiD[i].dateFin, row, 4);
                tb_.setValueAt(heureCorrecte(el.loiD[i].heureFin.heure, el.loiD[i].heureFin.minutes), row, 5);
                row++;
              }
            }
            if (el.loiE.ordreDuree != 0) {
              tb_.setValueAt("ecl:" + el.identification, row, 0);
              tb_.setValueAt("E", row, 1);
              tb_.setValueAt("" + el.loiE.ordreEcart, row, 6);
              tb_.setValueAt("" + el.loiE.nbJoursDEcart, row, 7);
              tb_.setValueAt("" + el.loiE.ordreDuree, row, 8);
              tb_.setValueAt("" + el.loiE.duree, row, 9);

              row++;
            }
            if (!(el.loiJ.heureFin.heure == 0 && el.loiJ.heureFin.minutes == 0)) {
              tb_.setValueAt("ecl:" + el.identification, row, 0);
              tb_.setValueAt("J", row, 1);
              tb_.setValueAt(heureCorrecte(el.loiJ.heureDebut.heure, el.loiJ.heureDebut.minutes), row, 10);
              tb_.setValueAt(heureCorrecte(el.loiJ.heureFin.heure, el.loiJ.heureFin.minutes), row, 11);

            }
          }
        }
      }

    }

    tb_.initNomCol(0);

    final String[] nomCol = { "Element", "Loi", "Det", "erm", "ini", "ste", "E", "rl", "an", "g", "Journa", "li�re" };

    table_ = new BuTable(tb_.data_, nomCol);

    table_.repaint();

  }

  /**
   * @param _e
   */

  public void annuler() {
    imp_.removeInternalFrame(this);
    imp_.resetFilleAffIndisponibilites();
  }

  public void actionPerformed(final ActionEvent _e) {
    if (_e.getSource() == bAnnuler_) {
      annuler();
    }

    else if (_e.getSource() == bAjouter_) {
      // imp_.ResetFilleAffIndisponibilites();
      annuler();
      imp_.addModIndisponibilites("", 3);

    }

    else if (_e.getSource() == bModifier_) {
      final int selection = table_.getSelectedRow();
      if (selection > 0) {

        if ((typeLoi(tb_.getValue(selection, 1).toString().charAt(0))) != 1) {
          imp_.addModIndisponibilites((String) tb_.getValue(selection, 0), typeLoi(tb_.getValue(selection, 1)
              .toString().charAt(0)));
          annuler();
        } else {
          imp_.affMessage("On ne peut qu'ajouter ou supprimer une indisponibilit� de type d�terministe.");
          // imp_.addModIndisponibilites("",3);
        }
      } else {
        imp_.affMessage("Selectionnez une ligne");
      }

    } else if (_e.getSource() == bSupprimer_) {
      /*
       * File f=new File("test.ouv"); SParametresEcluse[] b=(SParametresEcluse[])( imp_.listeEcluses_.toArray(new
       * SParametresEcluse[imp_.listeEcluses_.size()])); DParametresSinavi2.ecritParametresEcluses(b,f,null,null);
       */
      final int selection = table_.getSelectedRow();
      if (selection > 0) {
        final String prec = tb_.getValue(selection, 0).toString().substring(0, 4);
        final String elem = tb_.getValue(selection, 0).toString().substring(4);
        if (prec.equalsIgnoreCase("bie:")) {
          boolean trouve = false;
          final ListIterator iter = imp_.listeBiefs_.listIterator();
          while (iter.hasNext() && !trouve) {
            final SParametresBief el = (SParametresBief) iter.next();
            if (el.identification.equalsIgnoreCase(elem)) {
              if (typeLoi(tb_.getValue(selection, 1).toString().charAt(0)) == 0) {// ERLANG
                el.loiE.duree.heure = 0;
                el.loiE.duree.minutes = 0;
                el.loiE.nbJoursDEcart = 0;
                el.loiE.ordreDuree = 0;
                el.loiE.ordreEcart = 0;
                imp_.affMessage("Suppression de l'indisponibilit� selectionn�e");
              } else if (typeLoi(tb_.getValue(selection, 1).toString().charAt(0)) == 1) {// DETERMINISTE
                final SParametresIndispoLoiD[] temp = new SParametresIndispoLoiD[el.loiD.length - 1];
                boolean trouve2 = false;
                int i = 0;
                while (!trouve2) {

                  if ((el.loiD[i].dateDebut == (Integer.parseInt(tb_.getValueAt(selection, 2).toString())))
                      && el.loiD[i].dateFin == Integer.parseInt((String) tb_.getValueAt(selection, 4))
                      && el.loiD[i].heureDebut.heure == Integer.parseInt(((String) tb_.getValueAt(selection, 3))
                          .substring(0, 2))
                      && el.loiD[i].heureDebut.minutes == Integer.parseInt(((String) tb_.getValueAt(selection, 3))
                          .substring(3))
                      && el.loiD[i].heureFin.heure == Integer.parseInt(((String) tb_.getValueAt(selection, 5))
                          .substring(0, 2))
                      && el.loiD[i].heureFin.minutes == Integer.parseInt(((String) tb_.getValueAt(selection, 5))
                          .substring(3))) {
                    trouve2 = true;
                    imp_.affMessage("Suppression de l'indisponibilit� selectionn�e");
                  } else {
                    temp[i] = el.loiD[i];
                  }
                  i++;
                }
                for (int j = i; j < el.loiD.length; j++) {
                  temp[j - 1] = el.loiD[j];
                }
                el.loiD = temp;
              } else {// journaliere
                el.loiJ.heureDebut.heure = 0;
                el.loiJ.heureDebut.minutes = 0;
                el.loiJ.heureFin.heure = 0;
                el.loiJ.heureFin.minutes = 0;
                imp_.affMessage("Suppression de l'indisponibilit� selectionn�e");
              }

            }
          }
        }

      } else {
        imp_.affMessage("Selectionnez une ligne");
      }
    }

    else if (_e.getSource() == bMiseAJour_) {
      annuler();
      imp_.afficherIndisponibilites(coElement_.getSelectedIndex());

    } else if (_e.getSource() == bImprimer_) {
      final File f = imp_.enregistrerXls();
      if (f == null) {
        imp_.affMessage("Nom de Fichier incorrect");
        return;
      }
      final Sinavi2TableauIndisponibilites tbtemp = new Sinavi2TableauIndisponibilites();
      tbtemp.data_ = new Object[tb_.getRowCount() + 2][tb_.getColumnCount()];
      tbtemp.setNbRow(tb_.getRowCount() + 1);
      tbtemp.initNomCol(1);
      for (int i = 2; i <= (tb_.getRowCount() + 1); i++) {
        for (int j = 0; j < tb_.getColumnCount(); j++) {
          tbtemp.data_[i][j] = tb_.getValue(i - 2, j);
        }
      }
      tbtemp.setColumnName("Liste des Indisponibilit�s", 0);

      for (int i = 1; i < tbtemp.getColumnCount(); i++) {
        tbtemp.setColumnName(CtuluLibString.ESPACE, i);
      }
      final CtuluTableExcelWriter test = new CtuluTableExcelWriter(tbtemp, f);

      try {
        test.write(null);

      } catch (final RowsExceededException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      } catch (final WriteException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      } catch (final IOException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }

    }
  }

  /** M�thode permettant de donner la loi a partir d'une lettre */
  public int typeLoi(final char _c) {
    if (_c == 'E') {
      return 0;
    } else if (_c == 'D') {
      return 1;
    } else {
      return 2;
    }
  }

  /** m�thode permettant de mettre sous forme d'une seule chaine l'heure et la minute* */
  public static String HeureMinute(final int _h, final int _min) {
    String s;
    String h;
    if (_h < 10) {
      h = "0" + String.valueOf(_h);
    } else {
      h = String.valueOf(_h);
    }
    if (_min < 10) {
      s = h + "H" + String.valueOf(_min) + "0";
    } else {
      s = h + "H" + String.valueOf(_min);
    }
    return s;
  }

  public void internalFrameActivated(final InternalFrameEvent _e) {
  // TODO Auto-generated method stub

  }

  public void internalFrameClosed(final InternalFrameEvent _e) {
    this.closable = true;
    annuler();
  }

  public void internalFrameClosing(final InternalFrameEvent _e) {
  // TODO Auto-generated method stub

  }

  public void internalFrameDeactivated(final InternalFrameEvent _e) {
  // TODO Auto-generated method stub

  }

  public void internalFrameDeiconified(final InternalFrameEvent _e) {
  // TODO Auto-generated method stub

  }

  public void internalFrameIconified(final InternalFrameEvent _e) {
  // TODO Auto-generated method stub

  }

  public void internalFrameOpened(final InternalFrameEvent _e) {
  // TODO Auto-generated method stub

  }

}
