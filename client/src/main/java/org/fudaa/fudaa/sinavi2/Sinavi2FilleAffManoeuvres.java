/*
 * @file         Sinavi2FilleAff�Bateaux.java
 * @creation     2001-05-17
 * @modification $Date: 2007-05-04 14:01:05 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.sinavi2;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.ListIterator;

import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.border.EmptyBorder;
import javax.swing.event.InternalFrameEvent;
import javax.swing.event.InternalFrameListener;

import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuComboBox;
import com.memoire.bu.BuCommonImplementation;
import com.memoire.bu.BuDialogMessage;
import com.memoire.bu.BuInternalFrame;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuScrollPane;
import com.memoire.bu.BuTable;
import com.memoire.fu.FuLog;

import org.fudaa.ctulu.table.CtuluTableExcelWriter;

import org.fudaa.dodico.corba.sinavi2.SParametresBateau;
import org.fudaa.dodico.corba.sinavi2.SParametresEcluse;
import org.fudaa.dodico.corba.sinavi2.SParametresManoeuvres;

import org.fudaa.dodico.sinavi2.Sinavi2Helper;

/**
 * impl�mentation d'une fen�tre interne permettant d'afficher les parametres des manoeuvres sous forme de tableau
 * 
 * @version $Revision: 1.15 $ $Date: 2007-05-04 14:01:05 $ by $Author: deniger $
 * @author Fatimatou Ka , Beno�t Maneuvrier
 */
public class Sinavi2FilleAffManoeuvres extends BuInternalFrame implements ActionListener, InternalFrameListener {

  private final BuLabel lTitre_ = new BuLabel("Dur�e des manoeuvres ");// des bateaux dans les �cluses");
  private final BuLabel lFiltre_ = new BuLabel("Filtrer : ");
  /** bouton pour annuler */
  private final BuButton bAnnuler_ = new BuButton(Sinavi2Resource.SINAVI2.getIcon("QUITTER"), "Quitter");

  private final BuButton bModifier_ = new BuButton(Sinavi2Resource.SINAVI2.getIcon("MODIFIER"), "Modifier");

  private final BuButton bImprimer_ = new BuButton(Sinavi2Resource.SINAVI2.getIcon("IMPRIMER"), "Imprimer");

  private final BuButton bMiseAJour_ = new BuButton(Sinavi2Resource.SINAVI2.getIcon("MISAJOUR"), "Mise � Jour");
  private final BuButton bManoeuvreParDefaut_ = new BuButton(Sinavi2Resource.SINAVI2.getIcon("MANOEUVRE"),
      "Manoeuvres Par D�fauts");

  /** **gerer la liste des bateaux**** */
  // ---------------------------------public LinkedList listeBateaux2_;
  public ArrayList listeManoeuvres2_;
  // public static int nbBateaux_;

  /** **bateau pour les champs en cas de modifications*** */
  public SParametresManoeuvres manCourant_;
  // private int nbat_=-1;

  /** * panel contenant les boutons, il est plac� en bas */

  private final BuPanel pBoutons_ = new BuPanel();
  /**
   * panel contenant les diff�rents champs de la fen�tre
   */
  private final BuPanel pTitre_ = new BuPanel();

  private final BuPanel pDonnees2_ = new BuPanel();
  private final BuLabel lTypeBateau_ = new BuLabel("Bateau ");
  private final BuComboBox cTypeBateau_ = new BuComboBox();
  private final BuLabel lEcluse_ = new BuLabel("Ecluse ");
  private final BuComboBox cEcluse_ = new BuComboBox();

  private Sinavi2FilleModManoeuvres sinavi2fillemodmanoeuvres_;
  public Sinavi2TableauManoeuvre tb_;
  public BuTable table_;
  public Sinavi2Implementation imp2_ = null;

  public Sinavi2FilleAffManoeuvres(final BuCommonImplementation _appli, final ArrayList _liste2, final int _ecluse,
      final int _bateau) {
    super("Dur�e des manoeuvres des bateaux dans les �cluses", true, true, true, false);
    // pTitre_.add(lTitre_);
    imp2_ = (Sinavi2Implementation) _appli.getImplementation();
    listeManoeuvres2_ = _liste2;
    /* SinaviTableauBateau */
    // tabBateaux_= new BuTableSortModel(tb);
    cEcluse_.addItem("TOUS");
    if (imp2_.listeEcluses_ != null) {
      final ListIterator it = imp2_.listeEcluses_.listIterator();
      while (it.hasNext()) {
        final SParametresEcluse b = (SParametresEcluse) it.next();
        cEcluse_.addItem(b.identification);
      }
    }
    cEcluse_.setSelectedIndex(_ecluse);
    cTypeBateau_.addItem("TOUS");
    if (imp2_.listeBateaux_ != null) {
      final ListIterator itBateau = imp2_.listeBateaux_.listIterator();
      while (itBateau.hasNext()) {
        final SParametresBateau b = (SParametresBateau) itBateau.next();
        cTypeBateau_.addItem(b.identification);
      }
    }
    cTypeBateau_.setSelectedIndex(_bateau);
    miseajour(listeManoeuvres2_);
    final GridBagLayout g = new GridBagLayout();
    pTitre_.setLayout(g);
    final GridBagConstraints c = new GridBagConstraints();
    c.fill = GridBagConstraints.BOTH;
    c.gridx = 0;
    c.gridy = 2;
    pTitre_.add(lTitre_, c);
    c.gridx = 1;
    c.gridy = 2;

    pTitre_.add(lFiltre_, c);
    c.gridx = 2;
    c.gridy = 2;
    pTitre_.add(lTypeBateau_, c);

    c.gridx = 3;
    c.gridy = 2;
    pTitre_.add(cTypeBateau_, c);

    c.gridx = 2;
    c.gridy = 3;
    pTitre_.add(lEcluse_, c);

    c.gridx = 3;
    c.gridy = 3;
    pTitre_.add(cEcluse_, c);
    // table_.setSize(250,250);
    // table_.setAutoResizeMode(AUTO_RESIZE_ALL_COLUMNS);
    table_.setRowSelectionAllowed(true);

    final BuScrollPane tab = new BuScrollPane(table_);
    // pDonnees2_.add(table_);
    pDonnees2_.add(tab);

    bAnnuler_.addActionListener(this);
    bModifier_.addActionListener(this);
    bImprimer_.addActionListener(this);
    bMiseAJour_.addActionListener(this);
    bManoeuvreParDefaut_.addActionListener(this);
    if (!imp2_.isPermettreModif()) {
      bModifier_.setEnabled(false);
      bManoeuvreParDefaut_.setEnabled(false);

    }
    ((JComponent) getContentPane()).setBorder(new EmptyBorder(25, 25, 25, 25));

    pBoutons_.setLayout(new FlowLayout(FlowLayout.CENTER));
    pBoutons_.add(bAnnuler_);
    pBoutons_.add(bManoeuvreParDefaut_);
    pBoutons_.add(bModifier_);
    pBoutons_.add(bImprimer_);
    pBoutons_.add(bMiseAJour_);
    getContentPane().add(pTitre_, BorderLayout.NORTH);
    getContentPane().add(pDonnees2_, BorderLayout.CENTER);
    getContentPane().add(pBoutons_, BorderLayout.SOUTH);

    pack();

    setVisible(true);
    addInternalFrameListener(this);
    imp2_.addInternalFrame(this);

  }

  public void miseajour(final ArrayList lstManoeuvres_) {
    if (tb_ == null) {
      tb_ = new Sinavi2TableauManoeuvre();
    }
    int tabSize = 0;
    if (cTypeBateau_.getSelectedIndex() == 0) {
      tabSize = imp2_.listeBateaux_.size();
    } else {
      tabSize = 1;
    }
    if (cEcluse_.getSelectedIndex() == 0) {
      tabSize = tabSize * imp2_.listeEcluses_.size();
    }

    tb_.data_ = new Object[tabSize][tb_.getColumnCount()];
    // tb_.init_nomcol();
    if (lstManoeuvres_ != null) {
      final ListIterator iter = lstManoeuvres_.listIterator();
      int row = 0;
      while (iter.hasNext()) {

        SParametresManoeuvres c = new SParametresManoeuvres();
        c = (SParametresManoeuvres) iter.next();
        if ((c.type.equalsIgnoreCase((String) cTypeBateau_.getSelectedItem()) || cTypeBateau_.getSelectedIndex() == 0)
            && (c.ecluse.equalsIgnoreCase((String) cEcluse_.getSelectedItem()) || cEcluse_.getSelectedIndex() == 0)) {
          tb_.setValueAt(c.ecluse, row, 0);
          tb_.setValueAt(c.type, row, 1);

          String s = "";
          s = (Double.toString(c.entree));
          s = Sinavi2Helper.en2Chiffres(s);
          tb_.setValueAt(s, row, 2);

          s = (Double.toString(c.sortie));
          s = Sinavi2Helper.en2Chiffres(s);
          tb_.setValueAt(s, row, 3);

          row++;
          tb_.setNbRow(row);
        }
      }
    }
    table_ = new BuTable(tb_.data_, tb_.getNomColonnes());
    table_.repaint();
  }

  /**
   * @param _e
   */

  public void annuler() {
    imp2_.removeInternalFrame(this);
    imp2_.resetFille2ModManoeuvre();
  }

  public void actionPerformed(final ActionEvent _e) {
    if (_e.getSource() == bAnnuler_) {
      annuler();
    } else if (_e.getSource() == bManoeuvreParDefaut_) {
      FuLog.debug("test :" + table_.getSelectedRow());
      manoeuvreParDefaut(table_.getSelectedRow());
    } else if (_e.getSource() == bModifier_) {

      final int selection = table_.getSelectedRow();
      System.out.println("selection --> " + selection);
      if (selection >= 0) {

        String bateauCourant = "";
        bateauCourant = (String) table_.getValueAt(selection, 1);
        String ecluseCourant = "";
        ecluseCourant = (String) table_.getValueAt(selection, 0);
        // imp2_.modifier_vitesse(bateauCourant, biefCourant,tb);
        sinavi2fillemodmanoeuvres_ = new Sinavi2FilleModManoeuvres(imp2_, listeManoeuvres2_, bateauCourant,
            ecluseCourant, tb_);
        sinavi2fillemodmanoeuvres_.setVisible(true);

      } else {
        JOptionPane.showMessageDialog(null, "Selectionnez une ligne");
      }

      /*
       * if(controler_entrees()==true){ JOptionPane.showMessageDialog(null,"Cr�ation R�ussie"); initialise_champs(-1);
       */
    }

    else if (_e.getSource() == bImprimer_) {
      final File f = imp2_.enregistrerXls();
      if (f == null) {
        return;
      }
      final Sinavi2TableauManoeuvre tbtemp = new Sinavi2TableauManoeuvre();
      tbtemp.data_ = new Object[tb_.getRowCount() + 2][tb_.getColumnCount()];
      tbtemp.initNomCol(1);
      tbtemp.setColumnName("Liste des Dur�es de Manoeuvres", 0);
      for (int i = 1; i < tb_.getColumnCount(); i++) {
        tbtemp.setColumnName(" ", i);
      }

      tbtemp.setNbRow(tb_.getRowCount() + 2);
      for (int i = 2; i <= (tb_.getRowCount() + 1); i++) {
        for (int j = 0; j < tb_.getColumnCount(); j++) {
          tbtemp.data_[i][j] = tb_.data_[i - 2][j];
        }

      }
      final CtuluTableExcelWriter test = new CtuluTableExcelWriter(tbtemp, f);

      try {
        test.write(null);

      } catch (final RowsExceededException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      } catch (final WriteException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      } catch (final IOException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }
    } else if (_e.getSource() == bMiseAJour_) {
      annuler();
      imp2_.menuManoeuvres(cEcluse_.getSelectedIndex(), cTypeBateau_.getSelectedIndex());
      // miseajour(listeBateaux2_);

      /*
       * if(controler_entrees()==true){ JOptionPane.showMessageDialog(null,"Cr�ation R�ussie"); initialise_champs(-1);
       */
    }
  }

  /**
   * si aucune ligne n'est selectionn� -1, on met toutes les valeurs par d�faut sinon, seulement des lignes
   * selectionn�es.
   */

  private void manoeuvreParDefaut(final int _indice) {
    if (_indice == -1) {
      imp2_.listeManoeuvres_ = new ArrayList();
      listeManoeuvres2_ = imp2_.listeManoeuvres_;
      final ListIterator itEcl = imp2_.listeEcluses_.listIterator();
      final ListIterator itBat = imp2_.listeBateaux_.listIterator();
      /* remplissage des biefs dans une liste */
      final String[] ecl = new String[imp2_.listeEcluses_.size()];
      final double[] eclE = new double[imp2_.listeEcluses_.size()];
      final double[] eclS = new double[imp2_.listeEcluses_.size()];
      int i = 0;
      while (itEcl.hasNext()) {
        SParametresEcluse b = new SParametresEcluse();
        b = (SParametresEcluse) itEcl.next();
        ecl[i] = b.identification;
        eclE[i] = b.dureeManoeuvresEnEntree;
        eclS[i] = b.dureeManoeuvresEnSortie;
        i++;
      }
      while (itBat.hasNext()) {
        SParametresBateau b = new SParametresBateau();
        b = (SParametresBateau) itBat.next();
        for (int j = 0; j < ecl.length; j++) {
          final SParametresManoeuvres c = new SParametresManoeuvres();
          c.type = b.identification;
          c.ecluse = ecl[j];
          c.entree = eclE[j];
          c.sortie = eclS[j];
          listeManoeuvres2_.add(c);
        }
      }
    } else {
      for (int h = 0; h < table_.getSelectedRowCount(); h++) {

        listeManoeuvres2_ = imp2_.listeManoeuvres_;
        final String bateauCourant = (String) table_.getValueAt(table_.getSelectedRows()[h], 1);
        final String eclCourant = (String) table_.getValueAt(table_.getSelectedRows()[h], 0);
        boolean trouve = false;
        final ListIterator man = listeManoeuvres2_.listIterator();
        while (man.hasNext() & !trouve) {
          final SParametresManoeuvres c = (SParametresManoeuvres) man.next();
          if (c.type.equalsIgnoreCase(bateauCourant) && c.ecluse.equalsIgnoreCase(eclCourant)) {
            final ListIterator it = imp2_.listeEcluses_.listIterator();
            while (it.hasNext() & !trouve) {
              final SParametresEcluse b = (SParametresEcluse) it.next();
              if (b.identification.equalsIgnoreCase(eclCourant)) {
                c.entree = b.dureeManoeuvresEnEntree;
                c.sortie = b.dureeManoeuvresEnSortie;
                trouve = true;
              }
            }
            listeManoeuvres2_.set(table_.getSelectedRows()[h], c);
          }
        }
      }
    }
  }

  public void affMessage(final String _t) {
    final BuDialogMessage dialog_mess = new BuDialogMessage(imp2_.getApp(), imp2_.getInformationsSoftware(), "" + _t);
    dialog_mess.activate();
  }

  public void internalFrameActivated(final InternalFrameEvent _e) {
  // TODO Auto-generated method stub

  }

  public void internalFrameClosed(final InternalFrameEvent _e) {
    this.closable = true;
    System.out.println("fermeture centralis�e des portes");
    annuler();
  }

  public void internalFrameDeactivated(final InternalFrameEvent _e) {
  // TODO Auto-generated method stub

  }

  public void internalFrameDeiconified(final InternalFrameEvent _e) {
  // TODO Auto-generated method stub

  }

  public void internalFrameIconified(final InternalFrameEvent _e) {
  // TODO Auto-generated method stub

  }

  public void internalFrameOpened(final InternalFrameEvent _e) {
  // TODO Auto-generated method stub

  }

  public void internalFrameClosing(final InternalFrameEvent _e) {
  // TODO Auto-generated method stub

  }

}
