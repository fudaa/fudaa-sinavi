package org.fudaa.fudaa.sinavi2;

import org.fudaa.dodico.corba.sinavi2.SParametresBief;

class SinaviTypeBief {

  /*
   * public void desssineMoi(){ dessinateur.dessineBief(); }
   */

  // constructeur
  public SinaviTypeBief() {}

  /**
   * @param id non null
   * @param longueur
   * @param largeur_
   * @param hauteur
   * @param vitesse_
   * @param gareEnAmont
   * @param gareEnAval
   */
  public SinaviTypeBief(final String _id, final double _longueur, final double _largeur, final double _hauteur,
      final double _vitesse) {

    bief_ = new SParametresBief();
    bief_.identification = _id;
    // System.out.println("testative id reussie");
    bief_.longueur = _longueur;
    // System.out.println("testative id reussie");
    bief_.largeur = _largeur;
    bief_.hauteur = _hauteur;
    bief_.vitesse = _vitesse / 1000;
    bief_.gareEnAmont = 0;
    bief_.gareEnAval = 0;
  }

  /*
   * public Component getEditeur(){ return new double(true,false,true); }
   */

  // constructeur de recopie m�thode
  // accesseurs
  /** ************accesseuuuuuuuuuurrrrrrrrrrrrrrrrrrsssssssssssssss */
  public String getIdentification() {
    return bief_.identification;
  }

  public double getLongueur() {
    return bief_.longueur;
  }

  public double getLargeur() {
    return bief_.largeur;
  }

  public double getHauteur() {
    return bief_.hauteur;
  }

  public double getVitesse() {
    return bief_.vitesse;
  }

  public int getGareEnAmont() {
    return bief_.gareEnAmont;
  }

  public int getGareEnAval() {
    return bief_.gareEnAval;
  }

  // modifieurs
  public void setIdentification(final String _id) {
    bief_.identification = _id;
  }

  public void setLongueur(final double _longueur) {
    bief_.longueur = _longueur;
  }

  public void setLargeur(final double _largeur) {
    bief_.largeur = _largeur;
  }

  public void setHauteur(final double _hauteur) {
    bief_.hauteur = _hauteur;
  }

  public void setVitesse(final double _vitesse) {
    bief_.vitesse = _vitesse;
  }

  public void setGareEnAmont(final int _gare) {
    bief_.gareEnAmont = _gare;
  }

  public void setGareEnAval(final int _gare) {
    bief_.gareEnAval = _gare;
  }

  /*
   * public double creeComponent(boolean _useK,boolean _useM,boolean _useL){ LongueurField field=new
   * LongueurField(_useK,_useM,_useL); field.setValueValidator(new BuValueValidator() { public boolean
   * isValueValid(Object _value) { boolean r=(_value instanceof LongueurField); if(r){ double
   * l=((LongueurField)_value).getLongueurField(); return l>0 && l<1E6; } // TODO Auto-generated method stub return
   * false; } }); return field; }
   */

  public void arrondir(double _a, final int _nbChiffres) {
    for (int i = 0; i < _nbChiffres; i++) {
      _a *= 10;
    }
    _a = (int) (_a + .5);
    for (int i = 0; i < _nbChiffres; i++) {
      _a /= 10;
    }
  }

  /** *********m�thodes utiles********************** */

  public void dessineMoi() {

  }

  public boolean typeBiefEquals(final SinaviTypeBief _b) {
    return (this.getGareEnAmont() == _b.getGareEnAmont() && this.getGareEnAval() == _b.getGareEnAval()
        && this.getHauteur() == _b.getHauteur() && this.getIdentification() == _b.getIdentification()
        && this.getLargeur() == _b.getLargeur() && this.getLongueur() == _b.getLongueur() && this.getVitesse() == _b
        .getVitesse());
  }

  /*
   * private String id_; private double longueur_,largeur_,tirant_deau_,h_deb_nav_,h_fin_nav_,gene_;
   */
  SParametresBief bief_;
}
