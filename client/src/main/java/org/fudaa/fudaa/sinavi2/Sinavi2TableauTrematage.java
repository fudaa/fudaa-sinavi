package org.fudaa.fudaa.sinavi2;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

import javax.swing.table.AbstractTableModel;

import jxl.write.Label;
import jxl.write.Number;
import jxl.write.WritableCell;

import org.fudaa.dodico.corba.sinavi2.SParametresTrematages;

/**
 * @author maneuvrier Cette classe a pour but d'afficher le tableau des tr�matages (c'est � dire le droit de doubler ou
 *         non) et d'offrir la possibilit� de l'impression c'est � dire la cr�ation d'un tableau excel d'o�
 *         l'impl�mentation de CtuluTableModelInterface. elle est repr�sent�e sous forme de matrice avec les types de
 *         bateaux en lignes et en colonnes pour un bief donn�.
 */
public class Sinavi2TableauTrematage extends AbstractTableModel implements org.fudaa.ctulu.table.CtuluTableModelInterface {

  private int nbBateau_ = 0;
  private String[] nomsBateaux_ = null; // variable suivant le nombre de bateaux
  public Object[][] data_;

  // mise � jour des infos de trematage pour le bief � l'aide data
  /**
   * On met � jour les tr�matages pour le bief indiqu� On parcours les tr�matages et on assure la sym�trie entre les oui
   * non que si bat1 et bat2 egale O bat 2 et bat1 egale aussi � oui
   * 
   * @param _bief
   * @param _trematages
   */
  public void mAJTrematages(final String _bief, final ArrayList _trematages) {
    final ListIterator iter = _trematages.listIterator();
    while (iter.hasNext()) {
      SParametresTrematages c = new SParametresTrematages();
      c = (SParametresTrematages) iter.next();
      if (c.bief.equalsIgnoreCase(_bief)) {
        final int type1 = rechercheNumColonne(c.type1) + 1;
        final int type2 = rechercheNumColonne(c.type2) + 1;
        if (data_[type1][type2].toString().equalsIgnoreCase("O")) {
          c.ouiNon = true;
        } else {
          c.ouiNon = false;

        }
      }
    }

  }

  /**
   * recherche le num�ro de la colonne ou se trouve le bateau _s
   * 
   * @param _s nom du type de bateaux
   * @return -1 si non trouv� et le num�ro sinon
   */
  public int rechercheNumColonne(final String _s) {
    boolean trouve = false;
    int i = 0;
    while (!trouve & i < nomsBateaux_.length) {// !trouve facultatif
      if (nomsBateaux_[i] == _s)// test egal
      {
        trouve = true;
        return i;
      } else {
        i++;
      }
    }
    // if(!trouve)//facultatif
    return -1;
  }

  /**
   * ajoute un type d ebateau _s dans la liste des noms de bateaux
   * 
   * @param _s :
   */
  public void addNomsBateaux(final String _s) {
    if (nomsBateaux_ != null) {
      final String[] temp = new String[nomsBateaux_.length + 1];
      int i = 0;
      for (i = 0; i < nomsBateaux_.length; i++) {
        temp[i] = nomsBateaux_[i];
      }
      temp[i] = _s;
      nomsBateaux_ = new String[temp.length];
      nomsBateaux_ = temp;
    } else {
      nomsBateaux_ = new String[1];
      nomsBateaux_[0] = _s;
    }
    nbBateau_++;
  }

  /**
   * supprime un type de bateau dans la liste
   * 
   * @param _s type de bateau
   */

  public void supNomsBateaux(final String _s) {
    final String[] temp = new String[nomsBateaux_.length - 1];
    boolean trouve = false;
    for (int i = 0; i < nomsBateaux_.length; i++) {
      if (!nomsBateaux_[i].equalsIgnoreCase(_s) && !trouve) {
        temp[i] = nomsBateaux_[i];
      } else if (!nomsBateaux_[i].equalsIgnoreCase(_s) && trouve) {
        temp[i - 1] = nomsBateaux_[i];
      } else {
        trouve = true;
      }
    }
    nomsBateaux_ = temp;

  }

  public String[] getNomBateau() {
    return nomsBateaux_;
  }

  public void setNbRow(final int _n) {}

  public void setNbBateau(final int _n) {
    nbBateau_ = _n;

  }

  public int getNbBateau() {
    return nbBateau_;

  }
  
     public int[] getSelectedRows() {
        return null;
    }


  /**
   * initialise le nom des lignes et des colonnes � partir de la ligne _row
   * 
   * @param _row
   */
  public void initNomCol(final int _row) {
    for (int i = 0; i < getNbBateau(); i++) {
      data_[_row][i + 1] = new Object();
      data_[i + 1 + _row][0] = new Object();
      data_[i + 1 + _row][0] = nomsBateaux_[i];
      data_[_row][i + 1] = nomsBateaux_[i];

    }
  }

  public int getColumnCount() {

    return nbBateau_ + 1;
  }

  public int getRowCount() {
    return nbBateau_ + 1;
  }

  public Object getValueAt(final int _rowIndex, final int _columnIndex) {

    return data_[_rowIndex][_columnIndex];
  }

  public void setValueAt(final Object _value, final int _row, final int _col) {
    final String x = _value.toString();
    data_[_row][_col] = new Object();
    data_[_row][_col] = x;

  }

  public int getMaxCol() {
    // TODO Auto-generated method stub
    return nomsBateaux_.length + 1;
  }

  public int getMaxRow() {
    // TODO Auto-generated method stub
    return nomsBateaux_.length + 1;
  }

  public Object getValue(final int _row, final int _col) {
    // TODO Auto-generated method stub
    return getValueAt(_row, _col);
  }

  public WritableCell getExcelWritable(final int _row, final int _col, int _rowXls, int _colXls) {
    final int r = _row;
    final int c = _col;
    final Object o = data_[r][c];
    if (o == null) {
      return null;
    }
    String s = o.toString();
    if (s == null) {
      return null;
    }
    s = s.trim();
    if (s.length() == 0) {
      return null;
    }
    try {
      return new Number(_colXls, _rowXls, Double.parseDouble(s));
    } catch (final NumberFormatException e) {}
    return new Label(_colXls, _rowXls, s);

  }

  public String getColumnName(final int _i) {
    if (_i == 0) return "";
    return nomsBateaux_[_i - 1];

  }

  public void setColumnName(final String _name, final int _i) {
    nomsBateaux_[_i] = _name;

  }

public List<String> getComments() {
	// TODO Auto-generated method stub
	return null;
}

}
