/*
 * @file         Sinavi2FilleAff�Bateaux.java
 * @creation     2001-05-17
 * @modification $Date: 2006-09-19 15:08:58 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.sinavi2;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.ListIterator;

import javax.swing.JComponent;
import javax.swing.border.EmptyBorder;
import javax.swing.event.InternalFrameEvent;
import javax.swing.event.InternalFrameListener;

import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuComboBox;
import com.memoire.bu.BuCommonImplementation;
import com.memoire.bu.BuDialogMessage;
import com.memoire.bu.BuInternalFrame;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuScrollPane;
import com.memoire.bu.BuTable;

import org.fudaa.ctulu.table.CtuluTableExcelWriter;

import org.fudaa.dodico.corba.sinavi2.SParametresBateau;
import org.fudaa.dodico.corba.sinavi2.SParametresBief;
import org.fudaa.dodico.corba.sinavi2.SParametresTrematages;

/**
 * impl�mentation d'une fen�tre interne permettant d'afficher les parametres des trematages sous forme de tableau
 * 
 * @version $Revision: 1.12 $ $Date: 2006-09-19 15:08:58 $ by $Author: deniger $
 * @author Fatimatou Ka , Beno�t Maneuvrier
 */

/**
 * mettre � jour la liste des biefs dans la liste d�roulante mettre le tableau � jour mettre a jour si sauvegarde :
 * mettre a jour les champs de trematages nremplir data
 */

public class Sinavi2FilleAffTrematages extends BuInternalFrame implements ActionListener, InternalFrameListener {

  private static final int AUTO_RESIZE_ALL_COLUMNS = 0;

  /*********************************************************************************************************************
   * tableau***** identification longueur largeur navigation debut fin type charg� ou l�ge tirant d'eau gene admissible
   */

  // private BuTable tabBateaux_ =new BuTable();
  private final BuLabel lTitre_ = new BuLabel("R�gles de Trematage au sein des biefs");

  /** bouton pour annuler */
  private final BuButton bAnnuler_ = new BuButton(Sinavi2Resource.SINAVI2.getIcon("QUITTER"), "Quitter");

  private final BuButton bOui_ = new BuButton(Sinavi2Resource.SINAVI2.getIcon("OUI"), "Oui");

  private final BuButton bNon_ = new BuButton(Sinavi2Resource.SINAVI2.getIcon("NON"), "Non");
  /** sauver */
  private final BuButton bSelectionnerTout_ = new BuButton(Sinavi2Resource.SINAVI2
      .loadButtonCommandIcon("TOUTSELECTIONNER"), "Selectionner Tout");

  private final BuButton bSauver_ = new BuButton(Sinavi2Resource.SINAVI2.loadButtonCommandIcon("ENREGISTRER"),
      Sinavi2Resource.SINAVI2.getString("Enregistrer"));

  private final BuButton bImprimer_ = new BuButton(Sinavi2Resource.SINAVI2.getIcon("IMPRIMER"), "Imprimer");

  private final BuButton bMiseAJour_ = new BuButton(Sinavi2Resource.SINAVI2.getIcon("MISAJOUR"), "Mise � Jour");

  /* Liste d�roulante contenant les biefs */
  private BuComboBox cNomBief_; // Comment on fait
  /** **gerer la liste des biefs**** */
  // ---------------------------------public LinkedList listeBateaux2_;
  public ArrayList listeBiefs2_;// /je me demande si bien utile
  public ArrayList listeTrematages2_;
  public ArrayList listeBateaux2_;
  // public static int nbBateaux_;

  /** **bateau pour les champs en cas de modifications*** */
  public SParametresBief biefCourant_;
  public static String strTrematageCourant_;
  // private int nbat_=-1;

  /** * panel contenant les boutons, il est plac� en bas */

  private final BuPanel pBoutons_ = new BuPanel();
  /**
   * panel contenant les diff�rents champs de la fen�tre
   */
  private final BuPanel pTitre_ = new BuPanel();

  private final BuPanel pDonnees2_ = new BuPanel();

  /**
   * une instance de SinaviImplementation
   */
  // private BuCommonImplementation app_;
  /**
   * Constructeur
   * 
   * @param _app une instance de SinaviImplementation
   */

  // private BuCommonImplementation _appli;
  // public JTable tabBateaux_;
  public Sinavi2TableauTrematage tb_;
  public BuTable table_;
  // public BuTableSortModel tabBiefs_;
  public Sinavi2Implementation imp2_ = null;

  public Sinavi2FilleAffTrematages(final BuCommonImplementation _appli, final ArrayList _listeBief,
      final ArrayList _listeTrematages, final ArrayList _listeBateaux, final String _bief) {
    super("Affichage des trematages dans le bief selectionn�", true, true, true, false);
    pTitre_.add(lTitre_);

    listeBiefs2_ = _listeBief;
    listeTrematages2_ = _listeTrematages;
    listeBateaux2_ = _listeBateaux;
    cNomBief_ = new BuComboBox();
    cNomBief_.setEnabled(true);
    // cNomBief_.setSelectedItem(strTrematageCourant_);
    // remplissage de la liste de bief
    final ListIterator it = listeBiefs2_.listIterator();
    while (it.hasNext()) {
      SParametresBief b = new SParametresBief();
      b = (SParametresBief) it.next();
      cNomBief_.addItem(b.identification);

    }
    if (_bief != null) {
      cNomBief_.setSelectedItem(_bief);
    }
    pTitre_.add(cNomBief_);
    /* recherche du bief selectionn� */

    miseAJourTableau(listeTrematages2_, cNomBief_.getSelectedItem().toString(), listeBateaux2_);
    /* SinaviTableauBateau */
    // tabBateaux_= new BuTableSortModel(tb);
    // miseajour(listeBiefs2_);
    table_.setSize(250, 250);
    table_.setAutoResizeMode(AUTO_RESIZE_ALL_COLUMNS);
    table_.setRowSelectionAllowed(true);
    table_.setColumnSelectionAllowed(true);
    table_.setCellSelectionEnabled(true);

    final BuScrollPane scrollPane_ = new BuScrollPane(table_);
    // pDonnees2_.add(table_);
    pDonnees2_.add(scrollPane_);

    imp2_ = (Sinavi2Implementation) _appli.getImplementation();

    bAnnuler_.addActionListener(this);
    bOui_.addActionListener(this);
    bNon_.addActionListener(this);
    bSelectionnerTout_.addActionListener(this);
    bSauver_.addActionListener(this);
    bImprimer_.addActionListener(this);
    bMiseAJour_.addActionListener(this);
    cNomBief_.addActionListener(this);
    if (!imp2_.isPermettreModif()) {
      bOui_.setEnabled(false);
      bNon_.setEnabled(false);
      bSauver_.setEnabled(false);
    }
    ((JComponent) getContentPane()).setBorder(new EmptyBorder(25, 25, 25, 25));

    pBoutons_.setLayout(new FlowLayout(FlowLayout.CENTER));
    pBoutons_.add(bAnnuler_);
    pBoutons_.add(bOui_);
    pBoutons_.add(bNon_);
    pBoutons_.add(bSelectionnerTout_);
    pBoutons_.add(bSauver_);
    pBoutons_.add(bImprimer_);
    pBoutons_.add(bMiseAJour_);
    getContentPane().add(pTitre_, BorderLayout.NORTH);
    getContentPane().add(pDonnees2_, BorderLayout.CENTER);
    getContentPane().add(pBoutons_, BorderLayout.SOUTH);

    pack();

    /** *reinitialisation des valeurs* */

    setVisible(true);
    addInternalFrameListener(this);
    imp2_.addInternalFrame(this);

  }

  // fonction qui remplit le tableau suivant le bief selectionn�

  public void miseAJourTableau(final ArrayList trematages_, final ArrayList bateaux_) {
    /*
     * String s=(String) cNomBief_.getItemAt(0); miseAJourTableau(trematages_,s,bateaux_);
     */

    miseAJourTableau(trematages_, strTrematageCourant_, bateaux_);
  }

  public void miseAJourTableau(final ArrayList _trematages, final String _sBiefCourant, final ArrayList _bateaux) {
    // **********************************

    strTrematageCourant_ = _sBiefCourant;
    /** ***************************Creation du tableau************************************** */
    tb_ = new Sinavi2TableauTrematage();
    // ajout des bateaux
    final ListIterator itBat = _bateaux.listIterator();
    while (itBat.hasNext()) {
      SParametresBateau b = new SParametresBateau();
      b = (SParametresBateau) itBat.next();
      tb_.addNomsBateaux(b.identification);
    }

    // initialisation des cases en parcourant les trematages
    tb_.data_ = new Object[tb_.getNbBateau() + 1][tb_.getNbBateau() + 1];
    final ListIterator iter = _trematages.listIterator();
    while (iter.hasNext()) {

      SParametresTrematages c = new SParametresTrematages();
      c = (SParametresTrematages) iter.next();
      if (c.bief.equalsIgnoreCase(_sBiefCourant)) {
        final int row = tb_.rechercheNumColonne(c.type1) + 1;
        // System.out.println(row);
        final int col = tb_.rechercheNumColonne(c.type2) + 1;

        if (c.ouiNon) {
          tb_.setValueAt("O", row, col);
          tb_.setValueAt("O", col, row);
        } else {
          tb_.setValueAt("N", row, col);
          tb_.setValueAt("N", col, row);
        }

      }
    }

    // initialisation des colonnes
    tb_.initNomCol(0);

    final String[] nomcol = new String[tb_.getNomBateau().length + 1];
    nomcol[0] = "X";
    for (int i = 0; i < tb_.getNomBateau().length; i++) {
      nomcol[i + 1] = tb_.getNomBateau()[i];
    }

    // cr�ation de la table
    table_ = new BuTable(tb_.data_, nomcol);
    table_.repaint();
  }

  public void annuler() {

    imp2_.removeInternalFrame(this);
    // imp2_.sinavi2filleafftrematages_ =null;
    imp2_.resetFille2AffTrematage();
  }

  public void actionPerformed(final ActionEvent _e) {
    if (_e.getSource() == bAnnuler_) {

      annuler();
    } else if (_e.getSource() == bOui_) {
      // recup�r� les cases selctionn�s et faire des setvalue a oui
      // donc sauvegarder avec une demande de confirmation en appelant mise ajour de tableau
      int[] rows = new int[table_.getSelectedColumnCount()];
      rows = table_.getSelectedRows();
      int[] columns = new int[table_.getSelectedRowCount()];
      columns = table_.getSelectedColumns();

      /*
       * for(int i=0;i<table.getSelectedRowCount();i++) System.out.println("row" + i +rows[i]); for(int i=0;i<table.getSelectedColumnCount();i++)
       * System.out.println("col" + i +columns[i]);
       */
      for (int i = 0; i < table_.getSelectedRowCount(); i++) {
        if (rows[i] > 0) {
          for (int j = 0; j < table_.getSelectedColumnCount(); j++) {
            if (columns[j] > 0) {
              table_.setValueAt("O", rows[i], columns[j]);
              table_.setValueAt("O", columns[j], rows[i]);
            }
          }
        }
      }

      /*
       * if(controler_entrees()==true){ JOptionPane.showMessageDialog(null,"Cr�ation R�ussie"); initialise_champs(-1);
       */

    } else if (_e.getSource() == bNon_) {

      int[] rows = new int[table_.getSelectedColumnCount()];
      rows = table_.getSelectedRows();
      int[] columns = new int[table_.getSelectedRowCount()];
      columns = table_.getSelectedColumns();

      /*
       * for(int i=0;i<table.getSelectedRowCount();i++) System.out.println("row" + i +rows[i]); for(int i=0;i<table.getSelectedColumnCount();i++)
       * System.out.println("col" + i +columns[i]);
       */

      for (int i = 0; i < table_.getSelectedRowCount(); i++) {
        if (rows[i] > 0) {
          for (int j = 0; j < table_.getSelectedColumnCount(); j++) {
            if (columns[j] > 0) {
              table_.setValueAt("N", rows[i], columns[j]);
              table_.setValueAt("N", columns[j], rows[i]);
            }
          }
        }
      }

    } else if (_e.getSource() == bSauver_) {
      tb_.mAJTrematages(strTrematageCourant_, listeTrematages2_);
      annuler();

      /*
       * int selection = table.getSelectedRow(); if(selection > 0) imp2_.supprimer_biefs(selection-1);
       */
      /*
       * if(controler_entrees()==true){ JOptionPane.showMessageDialog(null,"Cr�ation R�ussie"); initialise_champs(-1);
       */
    } else if (_e.getSource() == bImprimer_) {
      final File s = imp2_.enregistrerXls();
      if (s == null) {
        return;
      }
      final Sinavi2TableauCroisement tbtemp = new Sinavi2TableauCroisement();
      tbtemp.data_ = new Object[tb_.getColumnCount() + 2][tb_.getColumnCount() + 2];
      for (int i = 1; i <= tb_.getColumnCount(); i++) {
        for (int j = 0; j < tb_.getColumnCount(); j++) {
          tbtemp.data_[i][j] = tb_.getValue(i - 1, j);
        }
      }
      tbtemp.addNomsBateaux("Tr�matages pour le bief " + cNomBief_.getSelectedItem().toString());

      for (int i = 1; i < tb_.getColumnCount(); i++) {
        tbtemp.addNomsBateaux(" ");
      }
      final CtuluTableExcelWriter test = new CtuluTableExcelWriter(tbtemp, s);
      try {
        test.write(null);
      } catch (final RowsExceededException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      } catch (final WriteException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      } catch (final IOException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }

    } else if (_e.getSource() == bMiseAJour_) {
      /*
       * annuler(); imp2_.menu_trematages();
       */
      // tb.mAJTrematages(strTrematageCourant_,listeTrematages2_);
      /*
       * annuler(); imp2_.menu_trematages();
       */
      tb_.mAJTrematages(strTrematageCourant_, listeTrematages2_);
    } else if (_e.getSource() == bSelectionnerTout_) {
      table_.selectAll();

    } else if (_e.getSource() == cNomBief_) {
      /*****************************************************************************************************************
       * sauvegarder le tableau courant dans la structure listeTrematage regarder le bief selectionn� mettre ajour le
       * tableau
       */

      /*
       * System.out.println("mise a jour des trematage :" +strTrematageCourant_);
       * tb.mAJTrematages(strTrematageCourant_,listeTrematages2_);
       * strTrematageCourant_=cNomBief_.getSelectedItem().toString(); Object s=cNomBief_.getSelectedItem(); int
       * x=cNomBief_.getSelectedIndex(); System.out.println("nouveau bief sele :" +strTrematageCourant_ +" -->" +x);
       * annuler(); imp2_.menu_trematages(); strTrematageCourant_=s.toString(); System.out.println("tableau du bief :
       * "+strTrematageCourant_); miseAJourTableau(listeTrematages2_,strTrematageCourant_,listeBateaux2_);
       * System.out.println("bief a afficher :" +strTrematageCourant_); //cNomBief_.setSelectedIndex(2);
       * if(s==cNomBief_.getSelectedItem()){ //cNomBief_.enableInputMethods(true); cNomBief_.setSelectedItem(s);
       * System.out.println("test reyussi"); //System.out.println("je me fou de ta gueule connard"); }
       * System.out.println("selceted item :" +cNomBief_.getSelectedItem().toString());
       * miseAJourTableau(listeTrematages2_,strTrematageCourant_,listeBateaux2_); System.out.println("combo box");
       */
      // System.out.println("mise a jour des trematage :" +strTrematageCourant_);
      tb_.mAJTrematages(strTrematageCourant_, listeTrematages2_);
      final String str = new String(cNomBief_.getSelectedItem().toString());
      strTrematageCourant_ = cNomBief_.getSelectedItem().toString();
      // Object s=cNomBief_.getSelectedItem();
      // int x=cNomBief_.getSelectedIndex();
      System.out.println("nouveau bief sele :" + strTrematageCourant_);
      annuler();
      imp2_.menuTrematages(str);
      // strTrematageCourant_=s.toString();
      // System.out.println("tableau du bief : "+strTrematageCourant_);
      // miseAJourTableau(listeTrematages2_,strTrematageCourant_,listeBateaux2_);
      /*
       * System.out.println("bief a afficher :" +strTrematageCourant_); System.out.println("bief a afficher (str) :"
       * +str); strTrematageCourant_=str; System.out.println("bief a afficher 2 :" +strTrematageCourant_);
       * cNomBief_.setSelectedItem(strTrematageCourant_); System.out.println("bief select combo :" +
       * cNomBief_.getSelectedItem().toString());
       */
      // cNomBief_=new BuComboBox();
      // cNomBief_.setSelectedItem(strTrematageCourant_);
      // remplissage de la liste de bief
      /*
       * ListIterator it=listeBiefs2_.listIterator(); while (it.hasNext()){ SParametresBief b= new SParametresBief();
       * b=(SParametresBief) it.next(); cNomBief_.addItem(b.identification); }
       * cNomBief_.setSelectedItem(strTrematageCourant_); //pTitre_.add(cNomBief_); //cNomBief_.setSelectedIndex(2);
       * /*if(s==cNomBief_.getSelectedItem()){ //cNomBief_.enableInputMethods(true); cNomBief_.setSelectedItem(s);
       * System.out.println("test reyussi"); //System.out.println("je me fou de ta gueule connard"); }
       */

      // System.out.println("selceted item :" +cNomBief_.getSelectedItem().toString());
      miseAJourTableau(listeTrematages2_, strTrematageCourant_, listeBateaux2_);
      System.out.println("combo box");

    }

  }

  public void affMessage(final String _t) {
    final BuDialogMessage dialog_mess = new BuDialogMessage(imp2_.getApp(), imp2_.getInformationsSoftware(), "" + _t);
    dialog_mess.activate();
  }

  public void internalFrameActivated(final InternalFrameEvent _e) {
  // TODO Auto-generated method stub

  }

  public void internalFrameClosed(final InternalFrameEvent _e) {
    this.closable = true;
    System.out.println("fermeture centralis�e des portes");
    annuler();
  }

  public void internalFrameDeactivated(final InternalFrameEvent _e) {
  // TODO Auto-generated method stub

  }

  public void internalFrameDeiconified(final InternalFrameEvent _e) {
  // TODO Auto-generated method stub

  }

  public void internalFrameIconified(final InternalFrameEvent _e) {
  // TODO Auto-generated method stub

  }

  public void internalFrameOpened(final InternalFrameEvent _e) {
  // TODO Auto-generated method stub

  }

  public void internalFrameClosing(final InternalFrameEvent _e) {
    annuler();

  }

}
