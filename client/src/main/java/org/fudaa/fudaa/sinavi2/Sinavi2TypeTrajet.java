package org.fudaa.fudaa.sinavi2;

import org.fudaa.dodico.corba.sinavi2.SDateHeure;
import org.fudaa.dodico.corba.sinavi2.SHeure;
import org.fudaa.dodico.corba.sinavi2.SParametresLoiD;
import org.fudaa.dodico.corba.sinavi2.SParametresLoiE;
import org.fudaa.dodico.corba.sinavi2.SParametresLoiJ;
import org.fudaa.dodico.corba.sinavi2.SParametresTrajets;

/**
 * @author ka cf Sinavi2TypeBateau Le type Sinavi2TypeTrajet a �t� cr�� afin d'encapsuler les donn�es entr�es par
 *         l'utilisateur et de les adapter au type SParametresTrajet, structure corba utilis�e pour la communication
 *         Fortran
 */

public class Sinavi2TypeTrajet {

  // constructeur sans param�tres
  public Sinavi2TypeTrajet() {}

  /**
   * Constructeur de Type Trajet
   * 
   * @param _id : identifiant du bateau non null
   * @param _sens : sens true avalant et false montant
   * @param _numeroGareDepart : gare de d�part du bateau en int
   * @param _numeroGareArrivee :gare d'arriv�e du bateau
   * @param _typeLoi : type de loi: 0: Erlang , 1: Deterministe ,2:Journali�re
   */
  public Sinavi2TypeTrajet(final String _bat, final boolean _sens, final int _numeroGareDepart,
      final int _numeroGareArrivee, final int _typeLoi) {

    trajet_ = new SParametresTrajets();
    trajet_.bateau = _bat;
    trajet_.avalantMontant = _sens;
    trajet_.gareDepart = _numeroGareDepart;
    trajet_.gareArrivee = _numeroGareArrivee;
    trajet_.typeDeLoi = _typeLoi;
  }

  /**
   * Constructeur de Type Trajet pour la loi Erlang
   * 
   * @param _id : identifiant du bateau non null
   * @param _sens : sens true avalant et false montant
   * @param _numeroGareDepart : gare de d�part du bateau en int
   * @param _numeroGareArrivee :gare d'arriv�e du bateau
   * @param _typeLoi : type de loi: 0
   * @param _heuredeb: heure de d�but de la g�n�ration en entier
   * @param _mindeb : minute de d�but de la g�n�ration en entier
   * @param _heurefin : heure de fin de la g�n�ration
   * @param _minfin : minute de fin de la g�n�ration en entier
   * @param _ordreloi :ordre de la loi en Erlang en entier
   * @param _nbrebateaux: le nombre de bateaux attendus en entier
   */
  public Sinavi2TypeTrajet(final String _bat, final boolean _sens, final int _numeroGareDepart,
      final int _numeroGareArrivee, final int _typeLoi, final int _heuredeb, final int _mindeb, final int _heurefin,
      final int _minfin, final int _ordreloi, final int _nbrebateaux) {
    trajet_ = new SParametresTrajets();
    trajet_.bateau = _bat;
    trajet_.avalantMontant = _sens;
    trajet_.gareDepart = _numeroGareDepart;
    trajet_.gareArrivee = _numeroGareArrivee;
    trajet_.typeDeLoi = _typeLoi;
    E_ = new SParametresLoiE();
    final SHeure hdeb_ = new SHeure();
    final SHeure hfin_ = new SHeure();
    hdeb_.heure = determineHeureSeule(_heuredeb);
    hdeb_.minutes = determineMinuteSeule(_mindeb);
    hfin_.heure = determineHeureSeule(_heurefin);
    hfin_.minutes = determineMinuteSeule(_minfin);
    E_.heureDebutGeneration = hdeb_;
    E_.heureFinGeneration = hfin_;
    E_.ordreDeLaLoi = _ordreloi;
    E_.nbreBateauxAttendus = _nbrebateaux;
    trajet_.loiE = E_;
  }

  /**
   * Constructeur de Type Trajet pour la loi Deterministe
   * 
   * @param _id : identifiant du bateau non null
   * @param _sens : sens true avalant et false montant
   * @param _numeroGareDepart : gare de d�part du bateau en int
   * @param _numeroGareArrivee :gare d'arriv�e du bateau
   * @param _typeLoi : type de loi: 1
   * @param _heure: heure de g�n�ration
   * @param _minute: minute de d�but de la g�n�ration en entier
   * @param _date: date de g�n�ration
   */

  public Sinavi2TypeTrajet(final String _bat, final boolean _sens, final int _numeroGareDepart,
      final int _numeroGareArrivee, final int _typeLoi, final int _heure, final int _minute, final int _date) {
    trajet_ = new SParametresTrajets();
    trajet_.bateau = _bat;
    trajet_.avalantMontant = _sens;
    trajet_.gareDepart = _numeroGareDepart;
    trajet_.gareArrivee = _numeroGareArrivee;
    trajet_.typeDeLoi = _typeLoi;
    Deter_ = new SParametresLoiD();
    final SDateHeure[] dateHeures = new SDateHeure[1];
    final SDateHeure dateHeure = new SDateHeure();
    final SHeure h_ = new SHeure();
    h_.heure = determineHeureSeule(_heure);
    h_.minutes = determineMinuteSeule(_minute);
    dateHeure.date = _date;
    dateHeure.heure = h_;
    dateHeures[0] = dateHeure;
    Deter_.dateHeure = dateHeures;
    trajet_.loiD = Deter_;
  }

  /**
   * Constructeur de Type Trajet pour la loi Joournali�re
   * 
   * @param _id : identifiant du bateau non null
   * @param _sens : sens true avalant et false montant
   * @param _numeroGareDepart : gare de d�part du bateau en int
   * @param _numeroGareArrivee :gare d'arriv�e du bateau
   * @param _typeLoi : type de loi: 2
   * @param _heure: heure de g�n�ration
   * @param _minute: minute de d�but de la g�n�ration en entier
   */

  public Sinavi2TypeTrajet(final String _bat, final boolean _sens, final int _numeroGareDepart,
      final int _numeroGareArrivee, final int _typeLoi, final int _heure, final int _minute) {
    trajet_ = new SParametresTrajets();
    trajet_.bateau = _bat;
    trajet_.avalantMontant = _sens;
    trajet_.gareDepart = _numeroGareDepart;
    trajet_.gareArrivee = _numeroGareArrivee;
    trajet_.typeDeLoi = _typeLoi;
    Jour_ = new SParametresLoiJ();
    final SHeure h_ = new SHeure();
    final SHeure[] heures = new SHeure[1];
    h_.heure = determineHeureSeule(_heure);
    h_.minutes = determineMinuteSeule(_minute);
    heures[0] = h_;
    Jour_.heureJ = heures;
    trajet_.loiJ = Jour_;
  }

  /** * m�thode permettant de d�terminer les heures � partir des secondes** */
  public static int determineHeureSeule(final int _nbSecondes) {
    return _nbSecondes / 3600;
  }

  /** * m�thode permettant de d�terminer les minutes � partir des secondes* */
  public static int determineMinuteSeule(final int _nbSecondes) {
    return _nbSecondes / 60;
  }

  /** ***accesseurs** */
  public String getbateau() {
    return trajet_.bateau;
  }

  public boolean getSens() {
    return trajet_.avalantMontant;
  }

  public int getNumeroGareDepart() {
    return trajet_.gareDepart;
  }

  public int getNumeroGareArrivee() {
    return trajet_.gareArrivee;
  }

  public int getTypeLoi() {
    return trajet_.typeDeLoi;
  }

  /** *******************modifieurs*********************** */
  public void setbateau(final String _bat) {
    trajet_.bateau = _bat;
  }

  public void setSens(final boolean _sens) {
    trajet_.avalantMontant = _sens;
  }

  public void setNumeroGareDepart(final int _gareDepart) {
    trajet_.gareDepart = _gareDepart;
  }

  public void setNumeroGareArrivee(final int _gareArrivee) {
    trajet_.gareArrivee = _gareArrivee;
  }

  public void setTypeLoi(final int _loi) {
    trajet_.typeDeLoi = _loi;
  }

  /** *********m�thodes utiles********************** */

  public boolean typeTrajetEgale(final Sinavi2TypeTrajet _traj) {
    return (this.getbateau() == _traj.getbateau() && this.getSens() == _traj.getSens()
        && this.getNumeroGareDepart() == _traj.getNumeroGareDepart()
        && this.getNumeroGareArrivee() == _traj.getNumeroGareArrivee() && this.getTypeLoi() == _traj.getTypeLoi());
  }

  SParametresTrajets trajet_;
  SParametresLoiE E_;
  SParametresLoiD Deter_;
  SParametresLoiJ Jour_;

}
