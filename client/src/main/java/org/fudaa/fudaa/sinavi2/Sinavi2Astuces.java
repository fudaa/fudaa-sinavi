package org.fudaa.fudaa.sinavi2;

import com.memoire.bu.BuPreferences;

import org.fudaa.fudaa.commun.FudaaAstuces;
import org.fudaa.fudaa.commun.FudaaAstucesAbstract;

/**
 * Classe pour les astuces du logiciel Fudaa-Sinavi .
 * 
 * @version $Revision: 1.7 $ $Date: 2007-05-04 14:01:06 $ by $Author: deniger $
 * @author Benoît MANEUVRIER Cette classe permet d'appeler les astuces avec les préférences de Sinavi Les astuces se
 *         trouvent dans le fichier astuces.txt
 */
public class Sinavi2Astuces extends FudaaAstucesAbstract {
  /**
   * renvoie un nouvel objet Sinavi2Astuces
   */
  public static Sinavi2Astuces SINAVI2 = new Sinavi2Astuces();

  /**
   * renvoie un nouvel objet FudaaAstuces (parent de Sinavi2Astuces).
   * 
   * @return une instance d'objet <code>FudaaAstuces</code>
   */
  protected FudaaAstucesAbstract getParent() {
    return FudaaAstuces.FUDAA;
  }

  /**
   * renvoie un nouvel objet sinavi2Preferences.
   * 
   * @return une instance d'objet <code>sinavi2Preferences</code>
   */
  protected BuPreferences getPrefs() {
    return Sinavi2Preferences.SINAVI2;
  }
}
