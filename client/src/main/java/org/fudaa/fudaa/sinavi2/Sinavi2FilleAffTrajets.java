package org.fudaa.fudaa.sinavi2;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.beans.PropertyVetoException;

import javax.swing.JComponent;
import javax.swing.JTable;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.event.InternalFrameEvent;
import javax.swing.event.InternalFrameListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.TableCellRenderer;

import com.memoire.bu.*;
import com.memoire.fu.FuLog;

import org.fudaa.ctulu.gui.CtuluCellTextRenderer;
import org.fudaa.ctulu.gui.CtuluTableExportPanel;
import org.fudaa.ctulu.table.CtuluTable;
import org.fudaa.ctulu.table.CtuluTableColumnHeader;

import org.fudaa.dodico.corba.sinavi2.SParametresTrajets;

/**
 * impl�mentation d'une classe permettant d'afficher tous les trajets saisis sous forme de tableau.
 * 
 * @version $Revision: 1.7 $ $Date: 2006-09-19 15:08:58 $ by $Author: deniger $
 * @author Fatimatou Ka , Benoit Maneuvrier
 */

public class Sinavi2FilleAffTrajets extends BuInternalFrame implements ActionListener, InternalFrameListener {

  /**
   * tableau. Nom du type de bateau sesns du trajet gare de d�part gare d'arriv�e Loi E: heure de d�but de g�n�ration,
   * heure de fin de g�n�ration, ordre de la loi, nombre de bateaux attendus Loi D: date , heures de g�n�ration LoiJ
   * :heure de g�n�ration
   */
  private static final long serialVersionUID = 1L;
  // private Sinavi2FilleTrajet sinavi2filletrajet_;
  private final BuLabel lTitre_ = new BuLabel("Affichage du trajet des types de bateau");

  private final BuButton bAnnuler_ = new BuButton(Sinavi2Resource.SINAVI2.getIcon("quitter"), "Quitter");
  private final BuButton bAjouter_ = new BuButton(Sinavi2Resource.SINAVI2.getIcon("ajouter"), "Ajouter");

  private final BuButton bModifier_ = new BuButton(Sinavi2Resource.SINAVI2.getIcon("editer"), "Modifier");

  private final BuButton bSupprimer_ = new BuButton(Sinavi2Resource.SINAVI2.getIcon("enlever"), "Suppimer");

  // private BuButton bImprimer_ = new BuButton(Sinavi2Resource.SINAVI2.getIcon("IMPRIMER"),"Imprimer");

  private final BuButton bPalette_ = new BuButton(BuResource.BU.getIcon("voir"), "Visualiser Palette");
  private final BuButton bImprimer_ = new BuButton(Sinavi2Resource.SINAVI2.getIcon("imprimer"), "Imprimer");

  /** Gerer la liste des trajets. */

  /** Panel contenant les boutons, il est plac� en bas. */
  private final BuPanel pBoutons_ = new BuPanel();
  /**
   * panel contenant les diff�rents champs de la fen�tre.
   */
  private final BuPanel pTitre_ = new BuPanel();

  private final BuPanel pDonnees2_ = new BuPanel();

  void majButtonState() {
    final int nb = table_.getSelectedRowCount();
    bModifier_.setEnabled(nb == 1);
    bPalette_.setEnabled(nb == 1);
    bSupprimer_.setEnabled(nb > 0);

  }

  /**
   * @param _app une instance de SinaviImplementation
   */
  public CtuluTable table_;
  public Sinavi2Implementation imp2_;

  // public TableColumn column=null;
  // public boolean AjouterouModifier_=false;
  public Sinavi2FilleAffTrajets(final BuCommonImplementation _appli, final Sinavi2TrajetMng _liste2) {
    super("Affichage des Trajets des types de bateau", true, true, true, false);
    pTitre_.add(lTitre_);

    final CtuluCellTextRenderer renderer = new CtuluCellTextRenderer();
    renderer.setVerticalAlignment(SwingConstants.TOP);
    renderer.setVerticalTextPosition(SwingConstants.TOP);
    final CtuluCellTextRenderer renderLast = new CtuluCellTextRenderer() {
      private static final long serialVersionUID = 1L;
      /*
       * protected void setValue(Object _value) { final long serialVersionUID = 1L; super.setValue(_value); String s =
       * (String) _value; if (s == null || s.length() == 0) setToolTipText(null); else setToolTipText(s); }
       */
    };
    renderLast.setVerticalAlignment(SwingConstants.TOP);
    renderer.setVerticalTextPosition(SwingConstants.TOP);

    // table_.setSize(400,400);

    table_ = new CtuluTable() {
      private static final long serialVersionUID = 1L;

      public TableCellRenderer getCellRenderer(final int _row, final int _column) {

        if (_column >= getColumnCount() - 2) {
          return renderLast;
        }

        return renderer;
      }
    };

    table_.setRowHeight(20);
    table_.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
    table_.setRowSelectionAllowed(true);
    table_.setColumnSelectionAllowed(false);
    table_.addMouseListener(new MouseAdapter() {
      public void mouseClicked(final MouseEvent _e) {
        if (_e.getClickCount() == 2 && table_.getSelectedRowCount() == 1) {
          editSelected();
        }
      }
    });
    table_.setModel(_liste2);
    table_.setTableHeader(new CtuluTableColumnHeader(table_.getColumnModel()));
    table_.setResizable(true, true);
    pDonnees2_.setLayout(new BuBorderLayout(0, 0, true, true));
    final BuScrollPane scroll = new BuScrollPane(table_);
    scroll.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
    scroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
    pDonnees2_.add(scroll);

    imp2_ = (Sinavi2Implementation) _appli.getImplementation();

    bAnnuler_.addActionListener(this);
    bAjouter_.addActionListener(this);
    bModifier_.addActionListener(this);
    bSupprimer_.addActionListener(this);
    bImprimer_.addActionListener(this);
    bPalette_.addActionListener(this);

    if (!imp2_.isPermettreModif()) {
      bAjouter_.setEnabled(false);
      bModifier_.setEnabled(false);
      bSupprimer_.setEnabled(false);
    }

    ((JComponent) getContentPane()).setBorder(BuBorders.EMPTY5555);

    pBoutons_.setLayout(new FlowLayout(FlowLayout.CENTER));
    pBoutons_.add(bAnnuler_);
    pBoutons_.add(bAjouter_);
    pBoutons_.add(bModifier_);
    pBoutons_.add(bSupprimer_);
    pBoutons_.add(bImprimer_);
    pBoutons_.add(bPalette_);
    getContentPane().add(pTitre_, BorderLayout.NORTH);
    getContentPane().add(pDonnees2_, BorderLayout.CENTER);
    getContentPane().add(pBoutons_, BorderLayout.SOUTH);
    pack();

    /** *reinitialisation des valeurs* */

    setVisible(true);
    addInternalFrameListener(this);
    majButtonState();
    table_.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
      public void valueChanged(final ListSelectionEvent _e) {
        majButtonState();
      }
    });

  }

  public SParametresTrajets getTrajetSelected() {
    final int idx = table_.getSelectedRow();
    if (idx >= 0) {
      imp2_.listeTrajets_.getTrajet(idx);
    }
    return null;
  }

  public void actionPerformed(final ActionEvent _e) {
    if (_e.getSource() == bAnnuler_) {
      try {
        setClosed(true);
      } catch (final PropertyVetoException _evt) {
        FuLog.error(_evt);

      }
    }

    else if (_e.getSource() == bAjouter_) {
      Sinavi2FilleAddModTrajets.addTrajet(imp2_);
    }

    else if (_e.getSource() == bModifier_) {
      editSelected();
    } else if (_e.getSource() == bSupprimer_) {
      final int[] selection = table_.getSelectedRows();
      if (selection != null) {
        final Sinavi2TrajetMng model = (Sinavi2TrajetMng) table_.getModel();
        if (selection.length == 1) {
          model.supprimerTrajets(selection[0], imp2_);
        } else {
          model.supprimerTrajets(selection, imp2_);
        }
      }
    }

    else if (_e.getSource() == bPalette_) {

      final int selection = table_.getSelectedRow();
      imp2_.menuPalette(imp2_.listeTrajets_.getTrajet(selection));

    }

    else if (_e.getSource() == bImprimer_) {
      CtuluTableExportPanel.doExport(table_, imp2_);
    }

  }

  void editSelected() {
    final int selection = table_.getSelectedRow();
    if (selection >= 0) {
      Sinavi2FilleAddModTrajets.modifyTrajet(imp2_, selection);
    }
  }

  /**
   * donner la loi � partir d'un string.
   */
  public static int getLoi(final String _l) {
    int loi = -1;
    if (_l.equalsIgnoreCase("Erlang")) {
      loi = 0;
    } else if (_l.equalsIgnoreCase("Deterministe")) {
      loi = 1;
    } else if (_l.equalsIgnoreCase("Journali�re")) {
      loi = 2;
    }
    return loi;
  }

  /** M�thode permettant de donner la loi a partir d'un entier. */

  public void affMessage(final String _t) {
    new BuDialogMessage(imp2_.getApp(), imp2_.getInformationsSoftware(), "" + _t).activate();
  }

  public void internalFrameActivated(final InternalFrameEvent _e) {

  }

  public void internalFrameClosed(final InternalFrameEvent _e) {
    imp2_.sinavi2filleafftrajets_ = null;
  }

  public void internalFrameClosing(final InternalFrameEvent _e) {

  }

  public void internalFrameDeactivated(final InternalFrameEvent _e) {

  }

  public void internalFrameDeiconified(final InternalFrameEvent _e) {

  }

  public void internalFrameIconified(final InternalFrameEvent _e) {

  }

  public void internalFrameOpened(final InternalFrameEvent _e) {

  }

}
