/*
 * @file         Sinavi2ResAttentesElements.java
 * @creation     2005-09-17
 * @modification $Date: 2007-05-04 14:01:05 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.sinavi2;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.ListIterator;

import javax.swing.JComponent;
import javax.swing.border.EmptyBorder;
import javax.swing.event.InternalFrameEvent;
import javax.swing.event.InternalFrameListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

import com.memoire.bu.*;
import com.memoire.fu.FuLog;

import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.table.CtuluTableExcelWriter;
import org.fudaa.ctulu.table.CtuluTableModelInterface;

import org.fudaa.dodico.corba.sinavi2.SParametresBief;
import org.fudaa.dodico.corba.sinavi2.SParametresEcluse;
import org.fudaa.dodico.corba.sinavi2.SSimulationSinavi2;

import org.fudaa.ebli.graphe.BGraphe;

/**
 * impl�mentation d'une fen�tre interne affichant les r�sultats des attentes par �l�ment.
 * 
 * @version $Revision: 1.15 $ $Date: 2007-05-04 14:01:05 $ by $Author: deniger $
 * @author Fatimatou Ka , Beno�t Maneuvrier
 */
public class Sinavi2ResAttentesElements extends BuInternalFrame implements ActionListener, InternalFrameListener,
    FocusListener, ListSelectionListener, ItemListener {

  ArrayList listeAttente_;
  private final BuLabel lTitre_ = new BuLabel("Attentes par El�ments");
  private final BuLabel lBlanc_ = new BuLabel("    ");
  private final BuLabel lBlanc2_ = new BuLabel("    ");
  private final BuLabel lBlanc3_ = new BuLabel("    ");
  private final BuLabel lBlanc4_ = new BuLabel("    ");
  private final BuLabel lBlanc5_ = new BuLabel("    ");

  /** bouton pour annuler. */
  private final BuButton bAnnuler_ = new BuButton(Sinavi2Resource.SINAVI2.getIcon("QUITTER"), "Quitter");

  /** afficher le graphique. */
  private final BuButton bAfficher_ = new BuButton(Sinavi2Resource.SINAVI2.getIcon("EDITER"), "Afficher");

  /** Param�tres d'entr�es pour le graphique. * */
  private final BuLabel lItineraire_ = new BuLabel("Itin�raire");
  private final BuLabel lGareDeb_ = new BuLabel("Gare Debut");
  private final BuLabel lGareFin_ = new BuLabel("Gare Fin");
  private final BuLabel lSensNav_ = new BuLabel("Sens Navigation");
  private final BuComboBox cGareDeb_ = new BuComboBox();
  private final BuComboBox cGareFin_ = new BuComboBox();
  public BuComboBox cSensNav_ = new BuComboBox();

  private final BuLabel lElement_ = new BuLabel("El�ments");
  private final BuLabel lListeElement_ = new BuLabel("Liste des El�ments");
  // private BuLabel lListeBateauxSel_=new BuLabel("Liste Bateaux Selectionn�s");
  public BuList liListeElements;

  public BuScrollPane sliListeElements;

  private final BuLabel lCrenaux_ = new BuLabel("Cr�naux Horaires");
  private final BuLabel lHoraireDebut_ = new BuLabel("Horaires de D�but");
  private final BuLabel lHoraireFin_ = new BuLabel("Horaires de Fin");
  private final DureeField dHoraireDebutHeure_ = new DureeField(false, false, true, false, false);
  private final DureeField dHoraireDebutMinute_ = new DureeField(false, false, false, true, false);
  private final DureeField dHoraireFinHeure_ = new DureeField(false, false, true, false, false);
  private final DureeField dHoraireFinMinute_ = new DureeField(false, false, false, true, false);

  private final BuLabel ltypeGraphe_ = new BuLabel("Type Graphe");
  private final BuRadioButton rHistogramme_ = new BuRadioButton("Histogramme");
  private final BuRadioButton rCourbe_ = new BuRadioButton("Courbe");
  private final BuRadioButton rPourcentage_ = new BuRadioButton("Pourcentage");
  private final BuRadioButton rCumul_ = new BuRadioButton("Cumul�");

  private final BuLabel lAxeY_ = new BuLabel("Axe des ordonn�es");
  public BuRadioButton rNbBateaux_ = new BuRadioButton("Nb de Bateaux");
  public BuRadioButton rPourcentagesY_ = new BuRadioButton("Dur�e Attente");

  private final BuLabel lDonneesAAfficher_ = new BuLabel("Donn�es � Afficher");
  public BuCheckBox cMin_ = new BuCheckBox("Minimum");
  public BuCheckBox cMoy_ = new BuCheckBox("Moyenne");
  public BuCheckBox cMax_ = new BuCheckBox("Maximum");
  public BuCheckBox cMoyF_ = new BuCheckBox("Moyenne F");
  private final BuCheckBox cSeuil_ = new BuCheckBox("Seuil");// **pas n�cessaire seulement si seuil !=0

  private final BuTextField tSeuil_ = new BuTextField("0");

  private final BuLabel lSeuil_ = new BuLabel("Seuil");

  /** * panel contenant les boutons, il est plac� en bas. */

  private final BuPanel pBoutons_ = new BuPanel();
  /**
   * panel contenant les diff�rents champs de la fen�tre.
   */
  private final BuPanel pTitre_ = new BuPanel();

  private final BuPanel pDonnees2_ = new BuPanel();
  private GridBagLayout g2;
  private GridBagConstraints c;
  public ArrayList listeSimulations_;
  // private BuCommonImplementation _appli;
  public ArrayList donneesGraph_;
  public boolean comparaison_;
  public Sinavi2Implementation imp_;
  public double minCourant_;
  public double moyCourant_;
  public double maxCourant_;
  public double maxiFlotte;
  public double moyFlotte;
  public double miniFlotte = 10000000;
  public ArrayList listeCoordonnees_;
  public Sinavi2TableauResAttentesElements t_;

  // private String[]
  // tabCouleurs={"FF0000","00FF00","888888","444FFF","AA1111","444444","666666","222222","484848","848484"};
  // ---------------------------public Sinavi2FilleAddModBateaux (BuCommonImplementation appli_,LinkedList liste_/*,int
  // bat_*/) {
  public Sinavi2ResAttentesElements(final BuCommonImplementation _appli, final ArrayList _listeAttente,
      final ArrayList _listeBateaux, final Sinavi2TrajetMng _listeTrajets, final ArrayList _listeGares,
      final ArrayList _listeBiefs, final ArrayList _listeEcluses, final ArrayList _listeSimulations,
      final boolean _comparaison) {
    super("Attentes par El�ments", true, true, true, false);
    imp_ = (Sinavi2Implementation) _appli.getImplementation();
    comparaison_ = _comparaison;
    listeSimulations_ = _listeSimulations;
    listeAttente_ = _listeAttente;
    bAnnuler_.addActionListener(this);
    bAfficher_.addActionListener(this);
    /*******************************************************************************************************************
     * ------- bAddBat_.addActionListener(this); bSupBat_.addActionListener(this); ------
     ******************************************************************************************************************/
    rCourbe_.addItemListener(this);
    rCourbe_.addActionListener(this);
    rHistogramme_.addItemListener(this);
    rHistogramme_.addActionListener(this);

    rNbBateaux_.addActionListener(this);

    rPourcentagesY_.addActionListener(this);
    rPourcentage_.addActionListener(this);
    rCumul_.addActionListener(this);
    ((JComponent) getContentPane()).setBorder(new EmptyBorder(25, 25, 25, 25));
    pTitre_.add(lTitre_, "center");

    g2 = new GridBagLayout();
    pDonnees2_.setLayout(g2);
    c = new GridBagConstraints();
    c.fill = c.BOTH;

    ListIterator it;
    c.gridx = 1;
    c.gridy = 2;
    pDonnees2_.add(lItineraire_, c);

    it = _listeGares.listIterator();
    while (it.hasNext()) {
      final String s = (String) it.next();
      cGareDeb_.addItem(s);
      cGareFin_.addItem(s);
    }

    c.gridx = 2;
    c.gridy = 2;
    pDonnees2_.add(lGareDeb_, c);

    c.gridx = 3;
    c.gridy = 2;
    pDonnees2_.add(cGareDeb_, c);
    cGareDeb_.setEnabled(false);
    c.gridx = 4;
    c.gridy = 2;
    pDonnees2_.add(lGareFin_, c);

    c.gridx = 5;
    c.gridy = 2;
    pDonnees2_.add(cGareFin_, c);
    cGareFin_.setEnabled(false);
    c.gridx = 6;
    c.gridy = 2;
    pDonnees2_.add(lSensNav_, c);

    cSensNav_.addItem("les 2");
    cSensNav_.addItem("Montant");
    cSensNav_.addItem("Avalant");
    c.gridx = 7;
    c.gridy = 2;
    pDonnees2_.add(cSensNav_, c);

    c.gridx = 1;
    c.gridy = 3;
    pDonnees2_.add(lBlanc_, c);

    c.gridx = 1;
    c.gridy = 4;
    pDonnees2_.add(lElement_, c);

    c.gridx = 2;
    c.gridy = 4;
    pDonnees2_.add(lListeElement_, c);

    final String[] elements = new String[_listeBiefs.size() + _listeEcluses.size()];
    int i = 0;
    it = _listeBiefs.listIterator();
    while (it.hasNext()) {
      elements[i] = "bie:" + ((SParametresBief) it.next()).identification;
      i++;
    }
    it = _listeEcluses.listIterator();
    while (it.hasNext()) {
      elements[i] = "ecl:" + ((SParametresEcluse) it.next()).identification;
      i++;
    }
    liListeElements = new BuList(elements);
    sliListeElements = new BuScrollPane(liListeElements);
    sliListeElements.setPreferredHeight(150);
    sliListeElements.setPreferredWidth(100);
    c.gridx = 3;
    c.gridy = 4;
    pDonnees2_.add(sliListeElements, c);
    c.gridx = 1;
    c.gridy = 5;
    pDonnees2_.add(lBlanc2_, c);

    c.gridx = 1;
    c.gridy = 6;
    pDonnees2_.add(lCrenaux_, c);

    c.gridx = 2;
    c.gridy = 6;
    pDonnees2_.add(lHoraireDebut_, c);

    c.gridx = 3;
    c.gridy = 6;
    pDonnees2_.add(dHoraireDebutHeure_, c);

    c.gridx = 4;
    c.gridy = 6;
    pDonnees2_.add(dHoraireDebutMinute_, c);

    c.gridx = 5;
    c.gridy = 6;
    pDonnees2_.add(lHoraireFin_, c);

    c.gridx = 6;
    c.gridy = 6;
    pDonnees2_.add(dHoraireFinHeure_, c);

    c.gridx = 7;
    c.gridy = 6;
    pDonnees2_.add(dHoraireFinMinute_, c);

    dHoraireDebutHeure_.setDureeField(0);
    dHoraireDebutMinute_.setDureeField(0);
    dHoraireFinHeure_.setDureeField(86400);
    dHoraireFinMinute_.setDureeField(0);

    c.gridx = 1;
    c.gridy = 7;
    pDonnees2_.add(lBlanc3_, c);

    c.gridx = 1;
    c.gridy = 8;
    pDonnees2_.add(ltypeGraphe_, c);

    c.gridx = 2;
    c.gridy = 8;
    pDonnees2_.add(rCourbe_, c);
    rCourbe_.setSelected(true);
    c.gridx = 3;
    c.gridy = 8;
    pDonnees2_.add(rHistogramme_, c);
    rHistogramme_.setSelected(false);

    c.gridx = 1;
    c.gridy = 10;
    pDonnees2_.add(lBlanc4_, c);

    c.gridx = 1;
    c.gridy = 10;
    pDonnees2_.add(lDonneesAAfficher_, c);

    c.gridx = 2;
    c.gridy = 10;
    pDonnees2_.add(cMin_, c);

    c.gridx = 3;
    c.gridy = 10;
    pDonnees2_.add(cMoy_, c);

    c.gridx = 4;
    c.gridy = 10;
    pDonnees2_.add(cMax_, c);

    c.gridx = 5;
    c.gridy = 10;
    pDonnees2_.add(cMoyF_, c);

    c.gridx = 2;
    c.gridy = 11;
    pDonnees2_.add(cSeuil_, c);

    c.gridx = 3;
    c.gridy = 11;
    pDonnees2_.add(lSeuil_, c);

    c.gridx = 4;
    c.gridy = 11;
    pDonnees2_.add(tSeuil_, c);

    c.gridx = 1;
    c.gridy = 12;
    pDonnees2_.add(lBlanc5_, c);

    c.gridx = 1;
    c.gridy = 13;
    pDonnees2_.add(lAxeY_, c);

    c.gridx = 2;
    c.gridy = 13;
    pDonnees2_.add(rNbBateaux_, c);
    rNbBateaux_.setSelected(false);

    c.gridx = 3;
    c.gridy = 13;
    pDonnees2_.add(rPourcentagesY_, c);
    rPourcentagesY_.setSelected(true);

    pBoutons_.setLayout(new FlowLayout(FlowLayout.CENTER));
    pBoutons_.add(bAnnuler_);

    pBoutons_.add(bAfficher_);
    // getContentPane().add(pDonnees_, BorderLayout.NORTH);
    getContentPane().add(pTitre_, BorderLayout.NORTH);
    getContentPane().add(pDonnees2_, BorderLayout.CENTER);
    getContentPane().add(pBoutons_, BorderLayout.SOUTH);

    pack();

    /** *reinitialisation des valeurs* */

    setVisible(true);
    // addInternalFrameListener(this);
    imp_.addInternalFrame(this);

  }

  /**
   * @param _e
   */
  private boolean verifParametres() {

    if (liListeElements.getSelectedIndex() == -1) {
      imp_.affMessage("Selectionnez un type de bateau.");
      return false;

    }
    return true;
  }

  public void actionPerformed(final ActionEvent _e) {
    if (_e.getSource() == bAnnuler_) {
      annuler();
    } else if (_e.getSource() == bAfficher_) {
      if (verifParametres()) {
        afficher_graphe();
        System.out.println("avant la construction du tableau");
        if (t_ != null) {
          t_.annuler();
        }
        t_ = new Sinavi2TableauResAttentesElements(imp_, comparaison_);
        System.out.println("apr�s la construction du tableau");
        imp_.activateInternalFrame(imp_.fillegraphe_);
      }
    }

    else if (_e.getSource() == rHistogramme_) {
      rHistogramme_.setSelected(true);
      rCourbe_.setSelected(false);
    } else if (_e.getSource() == rCourbe_) {
      rHistogramme_.setSelected(false);
      rCourbe_.setSelected(true);
    } else if (_e.getSource() == rPourcentage_) {
      rPourcentage_.setSelected(true);
      rCumul_.setSelected(false);
    } else if (_e.getSource() == rCumul_) {
      rCumul_.setSelected(true);
      rPourcentage_.setSelected(false);
    } else if (_e.getSource() == rPourcentagesY_) {
      rPourcentagesY_.setSelected(true);
      rNbBateaux_.setSelected(false);
    } else if (_e.getSource() == rNbBateaux_) {
      rNbBateaux_.setSelected(true);
      rPourcentagesY_.setSelected(false);
    }
  }

  private void annuler() {
    imp_.removeInternalFrame(this);
    imp_.resetFille2ModBateaux();

  }

  private void afficher_graphe() {
    System.out.println(" nombre de simulation " + imp_.listeSimulationsSinavi2_.size());
    if (!comparaison_) {
      selectionDonnees(/* imp_.getNumeroSimulationCourante() */0);
    } else {
      listeCoordonnees_ = new ArrayList();
      for (int i = 0; i < imp_.simulationsSel_.length; i++) {
        donneesGraph_ = new ArrayList();
        selectionDonnees(/* imp_.simulationsSel[ */i/* ] */);
        listeCoordonnees_.add(donneesGraph_);
      }

    }
    // selectionDonnees();
    final BGraphe graphe = new BGraphe();
    String str = "graphe\n";
    str += "{\n";
    if (!comparaison_) {
      str += "titre \"Attentes des �l�ments selectionn�s";
    } else {
      str += "titre \"Attentes pour " + liListeElements.getSelectedValue().toString();
    }
    if (cSensNav_.getSelectedIndex() == 0) {
      str += " dans les deux sens de navigation \" \n";
    } else if (cSensNav_.getSelectedIndex() == 1) {
      str += " dans le sens Montant \" \n";
    } else {
      str += " dans le sens Avalant \" \n";
    }
    str += "legende oui \n";
    str += "animation non \n";
    str += "marges\n";
    str += "{\n";
    str += "gauche 80\n";
    str += "droite 120\n";
    str += "haut 50\n";
    str += "bas 30\n";
    final String fin = "}\n";
    str += fin;
    str += "axe\n";
    str += "{\n";
    if (!comparaison_) {
      str += "titre \"El�ments\" ";
      str += "unite \"El�ment\" ";
      str += "orientation horizontal\n";
      str += "graduations non\n";
      str += "minimum  0\n";
      str += "maximum  20 \n";
    } else {
      str += "titre \"Simulations\" ";
      str += "unite \"simulation\" ";
      str += "orientation horizontal\n";
      str += "graduations non\n";
      str += "minimum  0\n";
      str += "maximum  10 \n";
    }
    final Object[] elemSel = liListeElements.getSelectedValues();
    str += fin;
    if (rNbBateaux_.isSelected()) {
      str += "axe\n";
      str += "{\n";
      str += "titre \"Nb Bateaux \" \n";
      str += "unite \"Bateau\" \n";
      str += "orientation vertical\n";
      str += "graduations oui\n";
      str += "minimum  0\n";
      final double maxNbBateaux = maxNbBateaux();
      str += "maximum  " + (maxNbBateaux + maxNbBateaux / 20) + " \n";
      int y = (int) Math.round(maxNbBateaux / 20);
      if (y == 0) {
        y = 1;
      }
      str += "pas " + y + CtuluLibString.LINE_SEP_SIMPLE;
      str += fin;
      str += "courbe\n";
      str += "{\n";
      str += "titre \"Nb de bateaux total\"\n";
      if (rCourbe_.isSelected()) {
        str += "type courbe\n";
      } else {
        str += "type histogramme\n";
      }
      str += "aspect\n";
      str += " {\n";
      if (rHistogramme_.isSelected()) {
        str += "contour.largeur 1 \n";
        str += "surface.couleur FF8800 \n";
        str += "texte.couleur 000000 \n";
        str += "contour.couleur 000000 \n";
      } else {
        str += "contour.couleur FF8800\n";
      }
      str += fin;
      str += " valeurs\n";
      str += "  {\n";
      if (!comparaison_) {
        final ListIterator it = donneesGraph_.listIterator();
        int h = 1;
        while (it.hasNext()) {
          System.out.println("max : " + h);
          final Sinavi2TypeAttente s = (Sinavi2TypeAttente) it.next();
          if (cSensNav_.getSelectedIndex() == 0) {// les 2

            // double xxx=s.getNbBatTotal()-20;
            str += h + CtuluLibString.ESPACE + /* xxx */s.getNbBatTotal()/*
                                                                           * + " etiquette "+
                                                                           * ((String)elemSel[h-1]).substring(4)
                                                                           */
                + CtuluLibString.LINE_SEP_SIMPLE;

          } else if (cSensNav_.getSelectedIndex() == 1) {// montant
            str += h + CtuluLibString.ESPACE + s.getNbBatMontant()/* +" etiquette "+ ((String)elemSel[h-1]).substring(4) */
                + CtuluLibString.LINE_SEP_SIMPLE;
          } else {
            str += h + CtuluLibString.ESPACE + s.getNbBatAvalant()/* +" etiquette "+ ((String)elemSel[h-1]).substring(4) */
                + CtuluLibString.LINE_SEP_SIMPLE;
          }
          h++;
        }
      } else {
        // ListIterator it=donneesGraph_.listIterator();
        int h = 1;
        while (h <= listeCoordonnees_.size()) {
          System.out.println("max : " + h);
          final ListIterator it = ((ArrayList) listeCoordonnees_.get(h - 1)).listIterator();
          while (it.hasNext()) {
            final Sinavi2TypeAttente s = (Sinavi2TypeAttente) it.next();
            if (cSensNav_.getSelectedIndex() == 0) {// les 2

              // double xxx=s.getNbBatTotal()-20;
              str += h + CtuluLibString.ESPACE + /* xxx */s.getNbBatTotal()/*
                                                                             * + " etiquette "+
                                                                             * ((String)elemSel[h-1]).substring(4)
                                                                             */
                  + CtuluLibString.LINE_SEP_SIMPLE;

            } else if (cSensNav_.getSelectedIndex() == 1) {// montant
              str += h + CtuluLibString.ESPACE + s.getNbBatMontant()/*
                                                                     * +" etiquette "+
                                                                     * ((String)elemSel[h-1]).substring(4)
                                                                     */
                  + CtuluLibString.LINE_SEP_SIMPLE;
            } else {
              str += h + CtuluLibString.ESPACE + s.getNbBatAvalant()/*
                                                                     * +" etiquette "+
                                                                     * ((String)elemSel[h-1]).substring(4)
                                                                     */
                  + CtuluLibString.LINE_SEP_SIMPLE;
            }
          }
          h++;
        }
      }
      str += "  }\n";
      str += fin;
      str += "courbe\n";
      str += "{\n";
      str += "titre \"Nb Bat Att\"\n";
      if (rCourbe_.isSelected()) {
        str += "type courbe\n";
      } else {
        str += "type histogramme\n";
      }
      str += "aspect\n";
      str += " {\n";
      if (rHistogramme_.isSelected()) {
        str += "contour.largeur 1 \n";
        str += "surface.couleur FF0000 \n";
        // str+="contour.couleur FFFFFF \n";
        str += "texte.couleur 000000 \n";
        str += "contour.couleur 000000 \n";
      } else {
        str += "contour.couleur FF0000\n";
      }
      str += fin;
      str += " valeurs\n";
      str += "  {\n";
      if (!comparaison_) {
        final ListIterator it = donneesGraph_.listIterator();
        int h = 1;
        while (it.hasNext()) {
          System.out.println("att : " + h);
          final Sinavi2TypeAttente s = (Sinavi2TypeAttente) it.next();

          // str+="1 50 \n etiquette \n testat \n";
          if (cSensNav_.getSelectedIndex() == 0) {// les 2
            s.calculePourcentage(true, true);
            final String eti = s.pourcentageTotalAtt_ + "%";
            str += h + CtuluLibString.ESPACE + s.getNbBatTotalAtt() + "\n etiquette  \n \"" + eti + "\" \n";
          } else if (cSensNav_.getSelectedIndex() == 1) {// montant
            s.calculePourcentage(true, false);
            final String eti = s.pourcentageMontantAtt_ + "%";
            str += h + CtuluLibString.ESPACE + s.getNbBatMontantAtt() + "\n etiquette \n \"" + eti + "\" \n";
          } else {
            s.calculePourcentage(false, true);
            final String eti = s.pourcentageAvalantAtt_ + "%";

            str += h + CtuluLibString.ESPACE + s.getNbBatAvalantAtt() + "\n etiquette \n \"" + eti + "\" \n";
          }
          h++;
        }
      } else {
        // it=donneesGraph_.listIterator();
        int h = 1;
        while (h <= listeCoordonnees_.size()) {
          System.out.println("att : " + h);
          final ListIterator it = ((ArrayList) listeCoordonnees_.get(h - 1)).listIterator();
          while (it.hasNext()) {
            final Sinavi2TypeAttente s = (Sinavi2TypeAttente) it.next();

            // str+="1 50 \n etiquette \n testat \n";
            if (cSensNav_.getSelectedIndex() == 0) {// les 2
              s.calculePourcentage(true, true);
              final String eti = s.pourcentageTotalAtt_ + "%";
              str += h + CtuluLibString.ESPACE + s.getNbBatTotalAtt() + "\n etiquette  \n \"" + eti + "\" \n";
            } else if (cSensNav_.getSelectedIndex() == 1) {// montant
              s.calculePourcentage(true, false);
              final String eti = s.pourcentageMontantAtt_ + "%";
              str += h + CtuluLibString.ESPACE + s.getNbBatMontantAtt() + "\n etiquette \n \"" + eti + "\" \n";
            } else {
              s.calculePourcentage(false, true);
              final String eti = s.pourcentageAvalantAtt_ + "%";

              str += h + CtuluLibString.ESPACE + s.getNbBatAvalantAtt() + "\n etiquette \n \"" + eti + "\" \n";
            }
          }
          h++;
        }

      }
      str += "  }\n";
      str += fin;
      // str+=" \n";

    } else {// dur�e qui est selectionn�e
      str += "axe\n";
      str += "{\n";
      str += "titre \"Attente \" \n";
      str += "unite \"heure\" \n";
      str += "orientation vertical \n";
      str += "conversionHM \n";
      str += "graduations oui\n";
      str += "minimum  0.0\n";
      System.out.println("max dur�e attente " + maxDureeAttente() / 3600);
      double tmp = maxDureeAttente();
      tmp = tmp + tmp / 20;
      tmp = tmp / 3600;
      System.out.println("new max " + tmp);
      // str+="maximum "+((int)((maxDureeAttente()+maxDureeAttente()/20))/3600)+"\n";
      str += "maximum  " + tmp + CtuluLibString.LINE_SEP_SIMPLE;
      final double y = tmp / 20;
      str += "pas " + y + CtuluLibString.LINE_SEP_SIMPLE;
      str += fin;
      if (cMax_.isSelected()) {
        str += "courbe\n";
        str += "{\n";
        str += "titre \"max\"\n";
        if (rCourbe_.isSelected()) {
          str += "type courbe\n";
        } else {
          str += "type histogramme\n";
        }
        str += "aspect\n";
        str += " {\n";
        if (rHistogramme_.isSelected()) {
          str += "contour.largeur 1 \n";
          str += "surface.couleur FF0000 \n";
          // str+="contour.couleur FFFFFF \n";
          str += "texte.couleur 000000 \n";
          str += "contour.couleur 000000 \n";
        } else {
          str += "contour.couleur FF0000\n";
        }
        str += fin;
        str += " valeurs\n";
        str += "  {\n";
        final ListIterator it = donneesGraph_.listIterator();
        if (!comparaison_) {
          int h = 1;
          while (it.hasNext()) {

            final Sinavi2TypeAttente s = (Sinavi2TypeAttente) it.next();
            if (cSensNav_.getSelectedIndex() == 0) {// les 2
              System.out.println(h + " ----> " + (s.getDureeMaxTotalAtt() / 3600) + CtuluLibString.LINE_SEP_SIMPLE);
              final double t = s.getDureeMaxTotalAtt() / 3600;
              str += h + CtuluLibString.ESPACE + (t) + CtuluLibString.LINE_SEP_SIMPLE;
            } else if (cSensNav_.getSelectedIndex() == 1) {// montant
              str += h + CtuluLibString.ESPACE + (s.getDureeMaxMontantAtt() / 3600) + CtuluLibString.LINE_SEP_SIMPLE;
            } else {
              str += h + CtuluLibString.ESPACE + (s.getDureeMaxAvalantAtt() / 3600) + CtuluLibString.LINE_SEP_SIMPLE;
            }
            h++;
          }
        } else {

          // ListIterator it=listeCoordonnees_.listIterator();
          int h = 1;
          System.out.println("taille list co : " + listeCoordonnees_.size());
          while (h <= listeCoordonnees_.size()) {
            final ListIterator it2 = ((ArrayList) listeCoordonnees_.get(h - 1)).listIterator();
            while (it2.hasNext()) {
              final Sinavi2TypeAttente s = (Sinavi2TypeAttente) it2.next();
              if (cSensNav_.getSelectedIndex() == 0) {// les 2
                str += h + CtuluLibString.ESPACE + (s.getDureeMaxTotalAtt() / 3600) + CtuluLibString.LINE_SEP_SIMPLE;
              } else if (cSensNav_.getSelectedIndex() == 1) {// montant
                str += h + CtuluLibString.ESPACE + (s.getDureeMaxMontantAtt() / 3600) + CtuluLibString.LINE_SEP_SIMPLE;
              } else {
                str += h + CtuluLibString.ESPACE + (s.getDureeMaxAvalantAtt() / 3600) + CtuluLibString.LINE_SEP_SIMPLE;
              }
              h++;
            }
          }

        }
        str += fin;
        str += fin;
      }
      if (cMoy_.isSelected()) {
        str += "courbe\n";
        str += "{\n";
        str += "titre \"moy\"\n";
        if (rCourbe_.isSelected()) {
          str += "type courbe\n";
        } else {
          str += "type histogramme\n";
        }
        str += "aspect\n";
        str += " {\n";
        if (rHistogramme_.isSelected()) {
          str += "contour.largeur 1 \n";
          str += "surface.couleur FF8800 \n";
          str += "texte.couleur 000000 \n";
          str += "contour.couleur 000000 \n";
        } else {
          str += "contour.couleur FF8800\n";
        }
        str += fin;
        str += " valeurs\n";
        str += "  {\n";
        if (!comparaison_) {
          final ListIterator it = donneesGraph_.listIterator();
          int h = 1;
          while (it.hasNext()) {

            final Sinavi2TypeAttente s = (Sinavi2TypeAttente) it.next();
            if (cSensNav_.getSelectedIndex() == 0) {// les 2
              str += h + CtuluLibString.ESPACE + (s.getDureeMoyTotalAtt() / 3600) + CtuluLibString.LINE_SEP_SIMPLE;
            } else if (cSensNav_.getSelectedIndex() == 1) {// montant
              str += h + CtuluLibString.ESPACE + (s.getDureeMoyMontantAtt() / 3600) + CtuluLibString.LINE_SEP_SIMPLE;
            } else {
              str += h + CtuluLibString.ESPACE + (s.getDureeMoyAvalantAtt() / 3600) + CtuluLibString.LINE_SEP_SIMPLE;
            }
            h++;
          }
        } else {

          // ListIterator it=listeCoordonnees_.listIterator();
          int h = 1;
          System.out.println("taille list co : " + listeCoordonnees_.size());
          while (h <= listeCoordonnees_.size()) {
            final ListIterator it2 = ((ArrayList) listeCoordonnees_.get(h - 1)).listIterator();
            while (it2.hasNext()) {
              final Sinavi2TypeAttente s = (Sinavi2TypeAttente) it2.next();
              if (cSensNav_.getSelectedIndex() == 0) {// les 2
                str += h + CtuluLibString.ESPACE + (s.getDureeMoyTotalAtt() / 3600) + CtuluLibString.LINE_SEP_SIMPLE;
              } else if (cSensNav_.getSelectedIndex() == 1) {// montant
                str += h + CtuluLibString.ESPACE + (s.getDureeMoyMontantAtt() / 3600) + CtuluLibString.LINE_SEP_SIMPLE;
              } else {
                str += h + CtuluLibString.ESPACE + (s.getDureeMoyAvalantAtt() / 3600) + CtuluLibString.LINE_SEP_SIMPLE;
              }
              h++;
            }
          }

        }

        str += fin;
        str += fin;
      }
      if (cMin_.isSelected()) {
        str += "courbe\n";
        str += "{\n";
        str += "titre \"min\"\n";
        if (rCourbe_.isSelected()) {
          str += "type courbe\n";
        } else {
          str += "type histogramme\n";
        }
        str += "aspect\n";
        str += " {\n";
        if (rHistogramme_.isSelected()) {
          str += "contour.largeur 1 \n";
          str += "surface.couleur 008000 \n";
          str += "texte.couleur 000000 \n";
          str += "contour.couleur 000000 \n";
        } else {
          str += "contour.couleur 008000\n";
        }
        str += fin;
        str += " valeurs\n";
        str += "  {\n";
        if (!comparaison_) {
          final ListIterator it = donneesGraph_.listIterator();
          int h = 1;
          while (it.hasNext()) {

            final Sinavi2TypeAttente s = (Sinavi2TypeAttente) it.next();
            if (cSensNav_.getSelectedIndex() == 0) {// les 2
              str += h + CtuluLibString.ESPACE + (s.getDureeMinTotalAtt() / 3600) + CtuluLibString.LINE_SEP_SIMPLE;
            } else if (cSensNav_.getSelectedIndex() == 1) {// montant
              str += h + CtuluLibString.ESPACE + (s.getDureeMinMontantAtt() / 3600) + CtuluLibString.LINE_SEP_SIMPLE;
            } else {
              str += h + CtuluLibString.ESPACE + (s.getDureeMinAvalantAtt() / 3600) + CtuluLibString.LINE_SEP_SIMPLE;
            }
            h++;
          }
        } else {

          // ListIterator it=listeCoordonnees_.listIterator();
          int h = 1;
          System.out.println("taille list co : " + listeCoordonnees_.size());
          while (h <= listeCoordonnees_.size()) {
            final ListIterator it2 = ((ArrayList) listeCoordonnees_.get(h - 1)).listIterator();
            while (it2.hasNext()) {
              final Sinavi2TypeAttente s = (Sinavi2TypeAttente) it2.next();
              if (cSensNav_.getSelectedIndex() == 0) {// les 2
                str += h + CtuluLibString.ESPACE + (s.getDureeMinTotalAtt() / 3600) + CtuluLibString.LINE_SEP_SIMPLE;
              } else if (cSensNav_.getSelectedIndex() == 1) {// montant
                str += h + CtuluLibString.ESPACE + (s.getDureeMinMontantAtt() / 3600) + CtuluLibString.LINE_SEP_SIMPLE;
              } else {
                str += h + CtuluLibString.ESPACE + (s.getDureeMinAvalantAtt() / 3600) + CtuluLibString.LINE_SEP_SIMPLE;
              }
              h++;
            }
          }

        }
        str += "  }\n";
        str += fin;
      }
      if (cMoyF_.isSelected()) {
        str += "courbe\n";
        str += "{\n";
        str += "titre \"moy Flotte\"\n";
        str += "type courbe\n";
        str += "aspect\n";
        str += " {\n";
        str += "contour.couleur 0000FF\n";
        str += fin;
        str += " valeurs\n";
        str += "  {\n";
        if (!comparaison_) {
          final ListIterator it = donneesGraph_.listIterator();
          int h = 1;
          while (it.hasNext()) {

            final Sinavi2TypeAttente s = (Sinavi2TypeAttente) it.next();
            if (cSensNav_.getSelectedIndex() == 0) {// les 2
              // FuLog.debug("ta "+ s.listeMontantDesTempsDAtt_.length);
              // s.calculeMoy();
              /*
               * double yc=(s.getDureeMoyTotalAtt()*s.getNbBatTotalAtt()); yc=yc/s.getNbBatTotal();
               */
              double yc = s.dureeMoyTtlTotalAtt_;
              yc = yc / 3600;
              str += h + CtuluLibString.ESPACE + yc + CtuluLibString.LINE_SEP_SIMPLE;
            } else if (cSensNav_.getSelectedIndex() == 1) {// montant
              /*
               * double yc=(s.getDureeMoyMontantAtt()*s.getNbBatMontantAtt()); yc=yc/s.getNbBatMontant(); yc=yc/3600;
               */
              double yc = s.dureeMoyTtlMontantAtt_;
              yc = yc / 3600;
              str += h + CtuluLibString.ESPACE + yc + CtuluLibString.LINE_SEP_SIMPLE;
            } else {
              /*
               * double yc=(s.getDureeMoyAvalantAtt()*s.getNbBatAvalantAtt()); yc=yc/s.getNbBatAvalant(); yc=yc/3600;
               */
              double yc = s.dureeMoyTtlAvalantAtt_;
              yc = yc / 3600;
              str += h + CtuluLibString.ESPACE + yc + CtuluLibString.LINE_SEP_SIMPLE;
            }
            h++;
          }
        } else {
          int h = 1;
          // System.out.println("taille list co : "+listeCoordonnees_.size());
          while (h <= listeCoordonnees_.size()) {
            final ListIterator it2 = ((ArrayList) listeCoordonnees_.get(h - 1)).listIterator();
            while (it2.hasNext()) {
              final Sinavi2TypeAttente s = (Sinavi2TypeAttente) it2.next();
              if (cSensNav_.getSelectedIndex() == 0) {// les 2
                double yc = s.dureeMoyTtlTotalAtt_;
                yc = yc / 3600;
                str += h + CtuluLibString.ESPACE + yc + CtuluLibString.LINE_SEP_SIMPLE;
                // str+=h+ CtuluLibString.ESPACE +(s.getDureeMinTotalAtt()/3600)+"\n";
              } else if (cSensNav_.getSelectedIndex() == 1) {// montant
                double yc = s.dureeMoyTtlMontantAtt_;
                yc = yc / 3600;
                str += h + CtuluLibString.ESPACE + yc + CtuluLibString.LINE_SEP_SIMPLE;
                // str+=h+ CtuluLibString.ESPACE +(s.getDureeMinMontantAtt()/3600)+"\n";
              } else {
                double yc = s.dureeMoyTtlAvalantAtt_;
                yc = yc / 3600;
                str += h + CtuluLibString.ESPACE + yc + CtuluLibString.LINE_SEP_SIMPLE;
                // str+=h+ CtuluLibString.ESPACE +(s.getDureeMinAvalantAtt()/3600)+"\n";
              }
              h++;
            }
          }
        }
        str += fin;
        str += fin;
      }
      str += " \n";
    }

    // affichage des simulaton ou des bief ou eluses
    str += "courbe\n";
    str += "{\n";
    str += "titre \"\"\n";
    str += "type courbe\n";
    str += "aspect\n";
    str += "{\n";
    str += "contour.couleur FFFFFF\n";
    str += "surface.couleur FFFFFF\n";
    str += "texte.couleur 000000 \n";
    str += "contour.largeur 0\n";
    str += fin;
    str += "valeurs\n";
    str += "  {\n";

    // double z1=maxDureeAttente()+maxDureeAttente()/40;
    double z1 = 0;
    if (rNbBateaux_.isSelected()) {
      z1 = maxNbBateaux() + maxNbBateaux() / 40;
    } else {
      z1 = (maxDureeAttente() + maxDureeAttente() / 40) / 3600;
    }
    // if(rTypeBateaux_.isSelected()){
    if (!comparaison_) {
      // ListIterator it=typePrec.listIterator();
      for (int k = 0; k < elemSel.length; k++) {
        final int h = k + 1;
        System.out.println("test etiquette");
        str += h + "     " + z1 + "\n   etiquette \n \"" + ((String) elemSel[h - 1]).substring(4) + "\" \n ";
      }
      /*
       * if(liListeBateaux_.getSelectedIndex()==0){ int h=tabMin.length+1; str+=h + CtuluLibString.ESPACE+ z1+"\n
       * etiquette \n \""+"flotte" +"\" \n "; }
       */
    } else {
      for (int k = 0; k < imp_.simulationsSel_.length; k++) {
        final int h = k + 1;
        System.out.println("t-est etiquette");
        str += h + "     " + z1 + "\n   etiquette \n \""
            + ((SSimulationSinavi2) imp_.listeSimulationsSinavi2_.get(imp_.simulationsSel_[k])).nomSim + "\" \n ";
      }
    }

    str += "  }\n";
    str += " }\n";

    if (cSeuil_.isSelected() && !tSeuil_.getValue().equals("0")) {
      str += " contrainte\n";
      str += "{\n";
      // a mettre le seuil
      final String tempSeuil = Sinavi2Lib.convertirHeureEnCentieme(Double.parseDouble(tSeuil_.getText().replace(',',
          '.')));
      str += "titre \"seuil : max =" + Double.parseDouble(tSeuil_.getText().replace(',', '.')) + "\"\n";
      // str+="orientation horizontal \n";
      str += " type max\n";
      str += " valeur " + tempSeuil + CtuluLibString.LINE_SEP_SIMPLE;

      str += fin;
      str += " \n";

    }
    str += "  } \"\n";
    graphe.setFluxDonnees(new ByteArrayInputStream(str.getBytes()));
    // Sinavi2FilleGraphe f=new Sinavi2FilleGraphe(imp_,imp_.getInformationsDocument(),graphe);
    imp_.affGraphe2(graphe);
    // f.setGraphe(graphe);

  }

  public double maxDureeAttente() {
    if (!comparaison_) {
      final ListIterator it = donneesGraph_.listIterator();
      double maxDuree = 0;
      while (it.hasNext()) {
        final Sinavi2TypeAttente d = (Sinavi2TypeAttente) it.next();
        if (maxDuree < d.getDureeMaxTotalAtt()) {
          maxDuree = d.getDureeMaxTotalAtt();
        }
      }
      return maxDuree;
    } else {
      double maxDuree = 0;
      for (int i = 0; i < listeCoordonnees_.size(); i++) {

        final ListIterator it = ((ArrayList) listeCoordonnees_.get(i)).listIterator();
        while (it.hasNext()) {
          final Sinavi2TypeAttente d = (Sinavi2TypeAttente) it.next();
          if (maxDuree < d.getDureeMaxTotalAtt()) {
            maxDuree = d.getDureeMaxTotalAtt();
          }
        }

      }
      return maxDuree;
    }

  }

  public int maxNbBateaux() {
    if (!comparaison_) {
      final ListIterator it = donneesGraph_.listIterator();
      int nbBatMax = 0;
      while (it.hasNext()) {
        final Sinavi2TypeAttente d = (Sinavi2TypeAttente) it.next();
        if (nbBatMax < d.getNbBatTotal()) {
          nbBatMax = d.getNbBatTotal();
        }
      }
      return nbBatMax;
    } else {
      int nbBatMax = 0;
      for (int i = 0; i < listeCoordonnees_.size(); i++) {

        final ListIterator it = ((ArrayList) listeCoordonnees_.get(i)).listIterator();
        while (it.hasNext()) {
          final Sinavi2TypeAttente d = (Sinavi2TypeAttente) it.next();
          if (nbBatMax < d.getNbBatTotal()) {
            nbBatMax = d.getNbBatTotal();
          }
        }

      }
      return nbBatMax;
    }

  }

  public void selectionDonnees(final int _indiceSimulation) {
    donneesGraph_ = new ArrayList();
    Object[] elementsSel;
    if (!comparaison_) {
      elementsSel = liListeElements.getSelectedValues();
    } else {
      elementsSel = new Object[1];
      elementsSel[0] = liListeElements.getSelectedValue();
    }

    System.out.println("nb object : " + elementsSel.length);
    // boolean trouve=false;
    /*
     * ListIterator it=listeAttente_.listIterator(); SinaviTypeAttente d=(SinaviTypeAttente)it.next();
     */
    for (int c1 = 0; c1 < elementsSel.length/* && !trouve */; c1++) {
      // trouve=false;
      final String precElem = ((String) elementsSel[c1]).substring(0, 4);
      final String elem = ((String) elementsSel[c1]).substring(4);
      // ListIterator iter=listeAttente_.listIterator();
      // ListIterator iter=((ArrayList)(listeAttente_.get(_indiceSimulation))).listIterator();
      final ListIterator iter = ((ArrayList) (listeAttente_.get(_indiceSimulation))).listIterator();
      Sinavi2TypeAttente d = null;
      while (iter.hasNext() /* && !trouve */) {
        // SinaviTypeAttente d2=(SinaviTypeAttente)iter.next();
        final Sinavi2TypeAttente d2 = new Sinavi2TypeAttente((Sinavi2TypeAttente) iter.next());
        if (d2.element_.equalsIgnoreCase(elem)
            && ((d2.typeElement_ == 'O' && precElem.equalsIgnoreCase("ecl:")) || (d2.typeElement_ == 'B' && precElem
                .equalsIgnoreCase("bie:")))) {
          if (d == null) {
            d = d2;
            donneesGraph_.add(d);
          } else {

            if (d.getDureeMaxAvalantAtt() < d2.getDureeMaxAvalantAtt()) {
              d.setDureeMaxAvalantAtt(d2.getDureeMaxAvalantAtt());
            }
            if (d.getDureeMaxMontantAtt() < d2.getDureeMaxMontantAtt()) {
              d.setDureeMaxMontantAtt(d2.getDureeMaxMontantAtt());
            }
            if (d.getDureeMaxTotalAtt() < d2.getDureeMaxTotalAtt()) {
              d.setDureeMaxTotalAtt(d2.getDureeMaxTotalAtt());
            }
            if (d.getDureeMinAvalantAtt() > d2.getDureeMinAvalantAtt()) {
              d.setDureeMinAvalantAtt(d2.getDureeMinAvalantAtt());
            }
            if (d.getDureeMinMontantAtt() > d2.getDureeMinMontantAtt()) {
              d.setDureeMinMontantAtt(d2.getDureeMinMontantAtt());
            }
            if (d.getDureeMinTotalAtt() > d2.getDureeMinTotalAtt()) {
              d.setDureeMinTotalAtt(d2.getDureeMinTotalAtt());
            }
            d.setNbBatMontant(d.getNbBatMontant() + d2.getNbBatMontant());
            d.setNbBatAvalant(d.getNbBatAvalant() + d2.getNbBatAvalant());
            d.setNbBatMontantAtt(d.getNbBatMontantAtt() + d2.getNbBatMontantAtt());
            d.setNbBatAvalantAtt(d.getNbBatAvalantAtt() + d2.getNbBatAvalantAtt());
            // d.listeAvalantDesTempsDAtt_=d2.listeAvalantDesTempsDAtt_;
            // d.listeMontantDesTempsDAtt_=d2.listeMontantDesTempsDAtt_;
            // d.setDureeMoyAvalantAtt((d2.getDureeMoyAvalantAtt()*d2.getNbBatAvalantAtt()+d.getDureeMoyAvalantAtt()*d.getNbBatAvalantAtt())/d.getNbBatAvalantAtt());
            // d.setDureeMoyMontantAtt((d2.getDureeMoyMontantAtt()*d2.getNbBatMontantAtt()+d.getDureeMoyMontantAtt()*d.getNbBatMontantAtt())/d.getNbBatMontantAtt());
            // d.dureeMoyMontantAtt_=d2.dureeMoyMontantAtt_;
            int tailleMontant = 0;
            int tailleMontantd = 0;
            int tailleMontantd2 = 0;
            if (d.listeMontantDesTempsDAtt_ != null) {
              tailleMontantd = d.listeMontantDesTempsDAtt_.length;
            }
            if (d2.listeMontantDesTempsDAtt_ != null) {
              tailleMontantd2 = d2.listeMontantDesTempsDAtt_.length;
            }
            tailleMontant = tailleMontantd + tailleMontantd2;
            // System.out.println("tm "+tailleMontant + " tmd "+tailleMontantd+" tmd2 "+tailleMontantd2);
            final double[] tabMontant = new double[tailleMontant];

            for (int j = 0; j < tailleMontantd; j++) {
              tabMontant[j] = d.listeMontantDesTempsDAtt_[j];
            }
            for (int j = tailleMontantd; j < tailleMontant; j++) {
              tabMontant[j] = d2.listeMontantDesTempsDAtt_[j - tailleMontantd];
            }
            d.listeMontantDesTempsDAtt_ = tabMontant;

            int tailleAvalant = 0;
            int tailleAvalantd = 0;
            int tailleAvalantd2 = 0;
            if (d.listeAvalantDesTempsDAtt_ != null) {
              tailleAvalantd = d.listeAvalantDesTempsDAtt_.length;
            }
            if (d2.listeAvalantDesTempsDAtt_ != null) {
              tailleAvalantd2 = d2.listeAvalantDesTempsDAtt_.length;
            }
            tailleAvalant = tailleAvalantd + tailleAvalantd2;

            final double[] tabAvalant = new double[tailleAvalant];
            for (int j = 0; j < tailleAvalantd; j++) {
              tabAvalant[j] = d.listeAvalantDesTempsDAtt_[j];
            }
            for (int j = tailleAvalantd; j < tailleAvalant; j++) {
              tabAvalant[j] = d2.listeAvalantDesTempsDAtt_[j - tailleAvalantd];
            }
            d2.listeAvalantDesTempsDAtt_ = tabAvalant;
            // d.setNbBatTotalAtt(d.getNbBatAvalant()+d.getNbBatMontantAtt());
            // d.setDureeMoyTotalAtt((d.getDureeMoyAvalantAtt()*d.getNbBatAvalantAtt()+d.getDureeMoyMontantAtt()*d.getNbBatMontantAtt())/(d.getNbBatTotalAtt()));
            // d.calculeTotaux();

          }
          // System.out.println("***"+d2.element_);
          // trouve =true;

        }
      }
    }
    final ListIterator it = donneesGraph_.listIterator();
    while (it.hasNext()) {
      final Sinavi2TypeAttente temp = (Sinavi2TypeAttente) it.next();
      temp.calculeTotaux();
      temp.calculeMoy();
      temp.reguleNbBateaux();
    }
  }

  public class Sinavi2TableauResAttentesElements extends BuInternalFrame implements ActionListener,
      InternalFrameListener {

    private final BuLabel lTitre1_ = new BuLabel("Affichage du temps d'attentes par elements");

    private final BuButton bAnnuler_ = new BuButton(Sinavi2Resource.SINAVI2.getIcon("QUITTER"), "Quitter");
    private final BuButton bImprimer_ = new BuButton(Sinavi2Resource.SINAVI2.getIcon("IMPRIMER"), "Imprimer");

    private final BuPanel pBoutons1_ = new BuPanel();
    private final BuPanel pTitre1_ = new BuPanel();
    private final BuPanel pDonnees_ = new BuPanel();
    public BuScrollPane scrollPane_;
    public Sinavi2TableauAttenteElementDuree tb1_;
    public Sinavi2TableauAttentesElements tb2_;
    public BuTable table1_;
    public BuTable table2_;
    public Sinavi2Implementation imp2_;

    public Sinavi2TableauResAttentesElements(final BuCommonImplementation _appli, final boolean _comparaison) {
      super("Affichage du temps d'attentes par �l�ments", true, true, true, false);
      pTitre1_.add(lTitre1_);

      if (rNbBateaux_.isSelected()) {
        tableauAttentesElementsBateaux();
        table1_.setRowSelectionAllowed(true);
        table1_.setColumnSelectionAllowed(false);
        table1_.getColumnModel().getColumn(0).setPreferredWidth(150);
        table1_.getColumnModel().getColumn(1).setPreferredWidth(150);
        table1_.getColumnModel().getColumn(2).setPreferredWidth(150);
        table1_.getColumnModel().getColumn(3).setPreferredWidth(150);
        scrollPane_ = new BuScrollPane(table1_);
        pDonnees_.add(scrollPane_);
        table1_.setAutoscrolls(true);
      } else if (rPourcentagesY_.isSelected()) {
        tableauAttentesElementsDuree();
        table2_.setRowSelectionAllowed(true);
        table2_.setColumnSelectionAllowed(false);
        table2_.getColumnModel().getColumn(0).setPreferredWidth(150);
        table2_.getColumnModel().getColumn(1).setPreferredWidth(100);
        table2_.getColumnModel().getColumn(2).setPreferredWidth(100);
        table2_.getColumnModel().getColumn(3).setPreferredWidth(100);
        table2_.getColumnModel().getColumn(4).setPreferredWidth(150);
        scrollPane_ = new BuScrollPane(table2_);
        pDonnees_.add(scrollPane_);
        table2_.setAutoscrolls(true);
      }

      imp2_ = (Sinavi2Implementation) _appli.getImplementation();

      bAnnuler_.addActionListener(this);
      bImprimer_.addActionListener(this);

      ((JComponent) getContentPane()).setBorder(new EmptyBorder(25, 25, 25, 25));

      pBoutons1_.setLayout(new FlowLayout(FlowLayout.CENTER));
      pBoutons1_.add(bAnnuler_);
      pBoutons1_.add(bImprimer_);
      getContentPane().add(pTitre1_, BorderLayout.NORTH);
      getContentPane().add(pDonnees_, BorderLayout.CENTER);
      getContentPane().add(pBoutons1_, BorderLayout.SOUTH);

      setLocation(20, 20);

      setVisible(true);
      addInternalFrameListener(this);
      imp2_.addInternalFrame(this);

    }

    public void tableauAttentesElementsBateaux() {
      int row = 0;
      String chaine = "";
      if (tb1_ == null) {
        tb1_ = new Sinavi2TableauAttenteElementDuree();
      }
      final Object[] elemSel = liListeElements.getSelectedValues();
      if (!comparaison_) {
        selectionDonnees(0);
      } else {
        listeCoordonnees_ = new ArrayList();
        for (int i = 0; i < imp_.simulationsSel_.length; i++) {
          donneesGraph_ = new ArrayList();
          selectionDonnees(/* imp_.simulationsSel[ */i/* ] */);
          listeCoordonnees_.add(donneesGraph_);
        }
      }
      if (rNbBateaux_.isSelected()) {
        // int maxNbBateaux=maxNbBateaux();
        // int y=maxNbBateaux/20;
        if (!comparaison_) {
          final ListIterator it = donneesGraph_.listIterator();
          int h = 1;
          while (it.hasNext()) {
            final Sinavi2TypeAttente s = (Sinavi2TypeAttente) it.next();

            chaine = elemSel[h - 1].toString();
            tb1_.setValueAt(chaine, row, 0);

            if (cSensNav_.getSelectedIndex() == 0) {// les 2
              System.out.println(h + CtuluLibString.ESPACE + s.getNbBatTotal());
              chaine = String.valueOf(s.getNbBatTotal());
              tb1_.setValueAt(chaine, row, 1);

              chaine = String.valueOf(s.getNbBatTotalAtt());
              tb1_.setValueAt(chaine, row, 2);

              s.calculePourcentage(true, true);
              chaine = "" + s.pourcentageTotalAtt_;
              tb1_.setValueAt(chaine, row, 3);
              System.out.println(h + CtuluLibString.ESPACE + s.getNbBatTotalAtt());

            } else if (cSensNav_.getSelectedIndex() == 1) {// montant
              System.out.println(h + CtuluLibString.ESPACE + s.getNbBatMontant());

              chaine = String.valueOf(s.getNbBatMontant());
              tb1_.setValueAt(s, row, 1);

              chaine = String.valueOf(s.getNbBatMontantAtt());
              tb1_.setValueAt(chaine, row, 2);

              s.calculePourcentage(true, false);
              chaine = "" + s.pourcentageMontantAtt_;
              tb1_.setValueAt(chaine, row, 3);
              System.out.println(h + CtuluLibString.ESPACE + s.getNbBatMontantAtt() + chaine);

            } else {
              System.out.println(h + CtuluLibString.ESPACE + s.getNbBatAvalant());
              chaine = String.valueOf(s.getNbBatAvalant());
              tb1_.setValueAt(s, row, 1);

              chaine = String.valueOf(s.getNbBatAvalantAtt());
              tb1_.setValueAt(chaine, row, 2);

              chaine = "" + s.pourcentageAvalantAtt_;
              tb1_.setValueAt(chaine, row, 3);
              System.out.println(h + CtuluLibString.ESPACE + s.getNbBatAvalantAtt() + chaine);

            }
            h++;
            row++;
            tb1_.setNbRow(row);
          }
        } else {
          int h = 1;
          while (h <= listeCoordonnees_.size()) {
            System.out.println("max : " + h);
            final ListIterator it = ((ArrayList) listeCoordonnees_.get(h - 1)).listIterator();
            while (it.hasNext()) {
              final Sinavi2TypeAttente s = (Sinavi2TypeAttente) it.next();

              chaine = ((SSimulationSinavi2) listeSimulations_.get(imp_.simulationsSel_[h - 1])).nomSim;
              tb1_.setValueAt(chaine, row, 0);

              if (cSensNav_.getSelectedIndex() == 0) {// les 2
                System.out.println(h + CtuluLibString.ESPACE + s.getNbBatTotal());

                chaine = String.valueOf(s.getNbBatTotal());
                tb1_.setValueAt(chaine, row, 1);

                chaine = String.valueOf(s.getNbBatTotalAtt());
                tb1_.setValueAt(chaine, row, 2);

                s.calculePourcentage(true, true);
                chaine = "" + s.pourcentageTotalAtt_;
                tb1_.setValueAt(chaine, row, 3);
                System.out.println(h + CtuluLibString.ESPACE + s.getNbBatTotalAtt() + "\n etiquette  \n \"" + chaine
                    + "\" \n");
              } else if (cSensNav_.getSelectedIndex() == 1) {// montant
                System.out.println(h + CtuluLibString.ESPACE + s.getNbBatMontant());
                chaine = String.valueOf(s.getNbBatMontant());
                tb1_.setValueAt(s, row, 1);

                chaine = String.valueOf(s.getNbBatMontantAtt());
                tb1_.setValueAt(chaine, row, 2);
                s.calculePourcentage(true, false);
                chaine = "" + s.pourcentageMontantAtt_;
                tb1_.setValueAt(chaine, row, 3);
                System.out.println(h + CtuluLibString.ESPACE + s.getNbBatMontantAtt() + "\n etiquette \n \"" + chaine
                    + "\" \n");
              } else {
                System.out.println(h + CtuluLibString.ESPACE + s.getNbBatAvalant());
                chaine = String.valueOf(s.getNbBatAvalant());
                tb1_.setValueAt(s, row, 1);

                chaine = String.valueOf(s.getNbBatAvalantAtt());
                tb1_.setValueAt(chaine, row, 2);
                s.calculePourcentage(false, true);
                chaine = "" + s.pourcentageAvalantAtt_;
                tb1_.setValueAt(chaine, row, 3);
                System.out.println(h + CtuluLibString.ESPACE + s.getNbBatAvalantAtt() + "\n etiquette \n \"" + chaine
                    + "\" \n");
              }
            }
            h++;
            row++;
            tb1_.setNbRow(row);
          }
        }
      }
      table1_ = new BuTable(tb1_.data_, tb1_.nomCol());
      table1_.repaint();

    }

    public void tableauAttentesElementsDuree() {
      int row = 0;
      // Fred inutile de creer un new String
      String chaine = "";
      if (tb2_ == null) {
        tb2_ = new Sinavi2TableauAttentesElements();
      }
      final Object[] elementsSel = liListeElements.getSelectedValues();
      if (!comparaison_) {
        selectionDonnees(0);
      } else {
        listeCoordonnees_ = new ArrayList();
        for (int i = 0; i < imp_.simulationsSel_.length; i++) {
          donneesGraph_ = new ArrayList();
          selectionDonnees(/* imp_.simulationsSel[ */i/* ] */);
          listeCoordonnees_.add(donneesGraph_);
        }
      }
      // double y =(maxDureeAttente()/20);
      if (!comparaison_) {
        final ListIterator it = donneesGraph_.listIterator();
        int h = 1;
        while (it.hasNext()) {
          // int h=1;
          final Sinavi2TypeAttente s = (Sinavi2TypeAttente) it.next();
          System.out.println("valeur de h" + h);
          chaine = elementsSel[h - 1].toString();
          System.out.println("valeur de la chaine " + chaine);
          tb2_.setValueAt(chaine, row, 0);

          // if(cMin_.isSelected()){

          if (cSensNav_.getSelectedIndex() == 0) {// les 2
            System.out.println(CtuluLibString.ESPACE + s.getDureeMinTotalAtt() + CtuluLibString.LINE_SEP_SIMPLE);
            chaine = "" + Sinavi2Lib.determineHeureString((int) (s.getDureeMinTotalAtt()));
            tb2_.setValueAt(chaine, row, 1);
          } else if (cSensNav_.getSelectedIndex() == 1) {// montant
            System.out.println(CtuluLibString.ESPACE + s.getDureeMinMontantAtt() + CtuluLibString.LINE_SEP_SIMPLE);
            chaine = "" + Sinavi2Lib.determineHeureString((int) (s.getDureeMinMontantAtt()));
            tb2_.setValueAt(chaine, row, 1);
          } else {
            System.out.println(CtuluLibString.ESPACE + s.getDureeMinAvalantAtt() + CtuluLibString.LINE_SEP_SIMPLE);
            chaine = "" + Sinavi2Lib.determineHeureString((int) (s.getDureeMinAvalantAtt()));
            tb2_.setValueAt(chaine, row, 1);
          }
          /*
           * } else if(cMin_.isSelected()==false){ chaine="pas de donn�e"; tb2_.setValueAt(chaine,row,1); }
           * if(cMoy_.isSelected()){
           */
          if (cSensNav_.getSelectedIndex() == 0) {// les 2
            System.out.println("" + s.getDureeMoyTotalAtt() + CtuluLibString.LINE_SEP_SIMPLE);
            chaine = "" + Sinavi2Lib.determineHeureString((int) (s.getDureeMoyTotalAtt()));
            tb2_.setValueAt(chaine, row, 2);
          }

          else if (cSensNav_.getSelectedIndex() == 1) {// montant
            System.out.println(CtuluLibString.ESPACE + s.getDureeMoyMontantAtt() + CtuluLibString.LINE_SEP_SIMPLE);
            chaine = "" + Sinavi2Lib.determineHeureString((int) (s.getDureeMoyMontantAtt()));
            tb2_.setValueAt(chaine, row, 2);
          }

          else {
            System.out.println(CtuluLibString.ESPACE + s.getDureeMoyAvalantAtt() + CtuluLibString.LINE_SEP_SIMPLE);
            chaine = "" + Sinavi2Lib.determineHeureString((int) (s.getDureeMoyAvalantAtt()));
            tb2_.setValueAt(chaine, row, 2);
          }
          /*
           * } else if(cMoy_.isSelected()==false){ chaine="pas de donn�e"; tb2_.setValueAt(chaine,row,2); }
           */

          // if(cMax_.isSelected()){
          if (cSensNav_.getSelectedIndex() == 0) {// les 2
            System.out.println(CtuluLibString.ESPACE + s.getDureeMaxTotalAtt() + CtuluLibString.LINE_SEP_SIMPLE);
            chaine = "" + Sinavi2Lib.determineHeureString((int) (s.getDureeMaxTotalAtt()));
            tb2_.setValueAt(chaine, row, 3);
          }

          else if (cSensNav_.getSelectedIndex() == 1) {// montant
            System.out.println(CtuluLibString.ESPACE + s.getDureeMaxMontantAtt() + CtuluLibString.LINE_SEP_SIMPLE);
            chaine = "" + Sinavi2Lib.determineHeureString((int) (s.getDureeMaxMontantAtt()));
            tb2_.setValueAt(chaine, row, 3);
          } else {
            System.out.println("" + s.getDureeMaxAvalantAtt() + CtuluLibString.LINE_SEP_SIMPLE);
            chaine = "" + Sinavi2Lib.determineHeureString((int) (s.getDureeMaxAvalantAtt()));
            tb2_.setValueAt(chaine, row, 3);
          }
          /*
           * } else if(cMax_.isSelected()==false){ chaine="pas de donn�e"; tb2_.setValueAt(chaine,row,3); }
           * if(cMoyF_.isSelected()){
           */
          if (cSensNav_.getSelectedIndex() == 0) {// les 2
            /*
             * double yc=(s.getDureeMoyTotalAtt()*s.getNbBatTotalAtt()); yc=yc/s.getNbBatTotal(); yc=yc/3600;
             */
            // chaine = String.valueOf(SinaviLib.conversionDeuxChiffres(yc));
            chaine = "" + Sinavi2Lib.determineHeureString((int) (s.getDureeMoyTtlTotalAtt()));
            tb2_.setValueAt(chaine, row, 4);
            // System.out.println(s.getDureeMoyTotalAtt()+"*"+s.getNbBatTotalAtt()+"/"+s.getNbBatTotal());

          }

          else if (cSensNav_.getSelectedIndex() == 1) {// montant
            /*
             * double yc=(s.getDureeMoyMontantAtt()*s.getNbBatMontantAtt()); yc=yc/s.getNbBatMontant(); yc=yc/3600;
             * chaine =""+ SinaviLib.conversionDeuxChiffres(yc);
             */
            chaine = "" + Sinavi2Lib.determineHeureString((int) (s.getDureeMoyTtlMontantAtt()));
            tb2_.setValueAt(chaine, row, 4);

          } else {
            /*
             * double yc=(s.getDureeMoyAvalantAtt()*s.getNbBatAvalantAtt()); yc=yc/s.getNbBatAvalant(); yc=yc/3600;
             * chaine =""+ SinaviLib.conversionDeuxChiffres(yc);
             */
            chaine = "" + Sinavi2Lib.determineHeureString((int) (s.getDureeMoyTtlAvalantAtt()));
            tb2_.setValueAt(chaine, row, 4);
          }

          /*
           * } if(cMoyF_.isSelected()==false){ chaine="pas de donn�e"; tb2_.setValueAt(chaine,row,4); }
           */
          h++;
          row++;
          tb2_.setNbRow(row);
        }

      }

      else if (comparaison_) {
        int h = 1;
        System.out.println("taille list co : " + listeCoordonnees_.size());
        final Object elementSel = liListeElements.getSelectedValue();
        while (h <= listeCoordonnees_.size()) {
          final ListIterator it2 = ((ArrayList) listeCoordonnees_.get(h - 1)).listIterator();
          while (it2.hasNext()) {
            final Sinavi2TypeAttente s = (Sinavi2TypeAttente) it2.next();
            chaine = ((SSimulationSinavi2) listeSimulations_.get(imp_.simulationsSel_[h - 1])).nomSim + " : "
                + elementSel.toString();
            tb2_.setValueAt(chaine, row, 0);
            // /if(cMin_.isSelected()){

            if (cSensNav_.getSelectedIndex() == 0) {// les 2
              System.out.println(CtuluLibString.ESPACE + s.getDureeMinTotalAtt() + CtuluLibString.LINE_SEP_SIMPLE);
              // chaine=""+ s.getDureeMinTotalAtt()/3600;
              chaine = Sinavi2Lib.determineHeureString((int) (s.getDureeMinTotalAtt()));
              tb2_.setValueAt(chaine, row, 1);
            } else if (cSensNav_.getSelectedIndex() == 1) {// montant
              System.out.println(CtuluLibString.ESPACE + s.getDureeMinMontantAtt() + CtuluLibString.LINE_SEP_SIMPLE);
              // chaine=""+ s.getDureeMinMontantAtt()/3600;
              chaine = Sinavi2Lib.determineHeureString((int) (s.getDureeMinMontantAtt()));
              tb2_.setValueAt(chaine, row, 1);
            } else {
              System.out.println(CtuluLibString.ESPACE + s.getDureeMinAvalantAtt() + CtuluLibString.LINE_SEP_SIMPLE);
              // chaine=""+ s.getDureeMinAvalantAtt()/3600;
              chaine = Sinavi2Lib.determineHeureString((int) (s.getDureeMinAvalantAtt()));
              tb2_.setValueAt(chaine, row, 1);
            }
            // }

            // if(cMoy_.isSelected()){

            if (cSensNav_.getSelectedIndex() == 0) {// les 2
              System.out.println("" + s.getDureeMoyTotalAtt() + CtuluLibString.LINE_SEP_SIMPLE);
              // chaine=""+ s.getDureeMoyTotalAtt()/3600;
              chaine = Sinavi2Lib.determineHeureString((int) (s.getDureeMoyTotalAtt()));
              tb2_.setValueAt(chaine, row, 2);
            } else if (cSensNav_.getSelectedIndex() == 1) {// montant
              System.out.println(CtuluLibString.ESPACE + s.getDureeMoyMontantAtt() + CtuluLibString.LINE_SEP_SIMPLE);
              // chaine=""+ s.getDureeMoyMontantAtt()/3600;*
              chaine = Sinavi2Lib.determineHeureString((int) (s.getDureeMoyMontantAtt()));
              tb2_.setValueAt(chaine, row, 2);
            } else {
              System.out.println(CtuluLibString.ESPACE + s.getDureeMoyAvalantAtt() + CtuluLibString.LINE_SEP_SIMPLE);
              // chaine=""+ s.getDureeMoyAvalantAtt()/3600;
              chaine = Sinavi2Lib.determineHeureString((int) (s.getDureeMoyAvalantAtt()));
              tb2_.setValueAt(chaine, row, 2);
            }
            // }
            /*
             * else if(cMoy_.isSelected()==false){ chaine="pas de donn�e"; tb2_.setValueAt(chaine,row,2); }
             * if(cMax_.isSelected()){
             */

            if (cSensNav_.getSelectedIndex() == 0) {// les 2
              System.out.println(CtuluLibString.ESPACE + s.getDureeMaxTotalAtt() + CtuluLibString.LINE_SEP_SIMPLE);
              // chaine=""+ s.getDureeMaxTotalAtt()/3600;
              chaine = Sinavi2Lib.determineHeureString((int) (s.getDureeMaxTotalAtt()));
              tb2_.setValueAt(chaine, row, 3);
            } else if (cSensNav_.getSelectedIndex() == 1) {// montant
              System.out.println(CtuluLibString.ESPACE + s.getDureeMaxMontantAtt() + CtuluLibString.LINE_SEP_SIMPLE);
              // chaine=""+ s.getDureeMaxMontantAtt()/3600;
              chaine = Sinavi2Lib.determineHeureString((int) (s.getDureeMaxMontantAtt()));
              tb2_.setValueAt(chaine, row, 3);
            } else {
              // System.out.println("" +s.getDureeMaxAvalantAtt()+"\n");
              // chaine=""+ s.getDureeMaxAvalantAtt()/3600;
              chaine = Sinavi2Lib.determineHeureString((int) (s.getDureeMaxAvalantAtt()));
              tb2_.setValueAt(chaine, row, 3);
            }

            // Moyenne sur la flotte
            if (cSensNav_.getSelectedIndex() == 0) {// les 2
              /*
               * double yc=(s.getDureeMoyTotalAtt()*s.getNbBatTotalAtt()); yc=yc/s.getNbBatTotal(); yc=yc/3600;
               */
              // chaine = String.valueOf(SinaviLib.conversionDeuxChiffres(yc));
              chaine = "" + Sinavi2Lib.determineHeureString((int) (s.getDureeMoyTtlTotalAtt()));
              tb2_.setValueAt(chaine, row, 4);
              // System.out.println(s.getDureeMoyTotalAtt()+"*"+s.getNbBatTotalAtt()+"/"+s.getNbBatTotal());

            }

            else if (cSensNav_.getSelectedIndex() == 1) {// montant
              /*
               * double yc=(s.getDureeMoyMontantAtt()*s.getNbBatMontantAtt()); yc=yc/s.getNbBatMontant(); yc=yc/3600;
               * chaine =""+ SinaviLib.conversionDeuxChiffres(yc);
               */
              chaine = "" + Sinavi2Lib.determineHeureString((int) (s.getDureeMoyTtlMontantAtt()));
              tb2_.setValueAt(chaine, row, 4);

            } else {
              /*
               * double yc=(s.getDureeMoyAvalantAtt()*s.getNbBatAvalantAtt()); yc=yc/s.getNbBatAvalant(); yc=yc/3600;
               * chaine =""+ SinaviLib.conversionDeuxChiffres(yc);
               */
              chaine = "" + Sinavi2Lib.determineHeureString((int) (s.getDureeMoyTtlAvalantAtt()));
              tb2_.setValueAt(chaine, row, 4);
            }

            /*
             * } else if(cMax_.isSelected()==false){ chaine="pas de donn�e"; tb2_.setValueAt(chaine,row,3); } else if
             * (cMoyF_.isSelected() || cMoyF_.isSelected()==false){ chaine="pas de donn�e";
             * tb2_.setValueAt(chaine,row,4); }
             */

            h++;
            row++;
            tb2_.setNbRow(row);
          }

        }

      }
      table2_ = new BuTable(tb2_.data_, tb2_.nomCol());
      table2_.repaint();

    }

    /**
     * @param _e
     */

    public void annuler() {
      imp2_.removeInternalFrame(this);
      imp2_.resetFille2AffBateaux();
    }

    public void actionPerformed(final ActionEvent _e) {

      if (_e.getSource() == bAnnuler_) {
        annuler();

      }

      else if (_e.getSource() == bImprimer_) {
        printFile();

      }

    }

    /**
     * on fait choisir un fichier � l'aide d'un chooser.<br>
     * on cr� un tableau temporaire tbtemp o� :<br>
     * on ajoute une ligne avec le titre et du blanc pour les autres<br>
     * cellules de la ligne<br>
     * on ajoute les colonnes sur la ligne suivante<br>
     * on ajoute enfin les donn�es<br>
     * on cr� le fichier excel<br>
     * on write le bateau.
     */
    private void printFile() {

      final File s = imp2_.enregistrerXls();
      if (s == null) {
        return;
      }
      CtuluTableModelInterface model = null;
      if (rNbBateaux_.isSelected()) {
        final Sinavi2TableauAttenteElementDuree tbtemp = new Sinavi2TableauAttenteElementDuree();
        model = tbtemp;
        tbtemp.data_ = new Object[tb1_.getRowCount() + 2][tb1_.getColumnCount()];
        tbtemp.initNomCol(1);
        if (!comparaison_) {
          tbtemp.setColumnName(" Attente des �l�ments s�lectionn�s ", 0);
        } else {
          tbtemp.setColumnName(" Attente de l'�l�ment " + liListeElements.getSelectedValue().toString(), 0);
        }
        for (int i = 1; i < tb1_.getColumnCount(); i++) {
          tbtemp.setColumnName(CtuluLibString.ESPACE, i);
        }

        tbtemp.setNbRow(tb1_.getRowCount() + 2);
        for (int i = 2; i <= (tb1_.getRowCount() + 1); i++) {
          for (int j = 0; j < tb1_.getColumnCount(); j++) {
            tbtemp.data_[i][j] = tb1_.data_[i - 2][j];
          }

        }
      } else if (rPourcentagesY_.isSelected()) {
        final Sinavi2TableauAttentesElements tbtemp = new Sinavi2TableauAttentesElements();
        model = tbtemp;
        tbtemp.data_ = new Object[tb2_.getRowCount() + 2][tb2_.getColumnCount()];
        FuLog.debug("nb row + " + tb2_.getRowCount());
        tbtemp.initNomCol(1);
        if (!comparaison_) {
          tbtemp.setColumnName(" Attente des �l�ments s�l�ctionn�s ", 0);
        } else {
          tbtemp.setColumnName(" Attente de l'�l�ment " + liListeElements.getSelectedValue().toString(), 0);
        }
        for (int i = 1; i < tb2_.getColumnCount(); i++) {
          tbtemp.setColumnName(CtuluLibString.ESPACE, i);
        }

        tbtemp.setNbRow(tb2_.getRowCount() + 2);
        for (int i = 2; i <= (tb2_.getRowCount() + 1); i++) {
          for (int j = 0; j < tb2_.getColumnCount(); j++) {
            tbtemp.data_[i][j] = tb2_.data_[i - 2][j];
          }

        }
      }
      final CtuluTableExcelWriter test = new CtuluTableExcelWriter(model, s);

      try {
        test.write(null);

      } catch (final RowsExceededException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      } catch (final WriteException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      } catch (final IOException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }
    }

    public void affMessage(final String _t) {
      new BuDialogMessage(imp_.getApp(), imp_.getInformationsSoftware(), _t).activate();
    }

    public void internalFrameActivated(final InternalFrameEvent _e) {
    // TODO Auto-generated method stub

    }

    public void internalFrameClosed(final InternalFrameEvent _e) {}

    public void internalFrameDeactivated(final InternalFrameEvent _e) {

    }

    public void internalFrameDeiconified(final InternalFrameEvent _e) {

    }

    public void internalFrameIconified(final InternalFrameEvent _e) {

    }

    public void internalFrameOpened(final InternalFrameEvent _e) {

    }

    public void internalFrameClosing(final InternalFrameEvent _e) {

    }

  }

  public void internalFrameActivated(final InternalFrameEvent _e) {}

  public void internalFrameClosed(final InternalFrameEvent _e) {

  }

  public void internalFrameClosing(final InternalFrameEvent _e) {}

  public void internalFrameDeactivated(final InternalFrameEvent _e) {}

  public void internalFrameDeiconified(final InternalFrameEvent _e) {}

  public void internalFrameIconified(final InternalFrameEvent _e) {}

  public void internalFrameOpened(final InternalFrameEvent _e) {}

  public void affMessage(final String _t) {
    new BuDialogMessage(imp_.getApp(), imp_.getInformationsSoftware(), _t).activate();
  }

  public void focusGained(final FocusEvent _e) {

  }

  public void focusLost(final FocusEvent _e) {

  }

  public void valueChanged(final ListSelectionEvent _e) {
  // TODO Auto-generated method stub

  }

  public void itemStateChanged(final ItemEvent _e) {
  // TODO Auto-generated method stub

  }

}
