/*
 * @file         SinaviFilleChoixCalcul.java
 * @creation     2001-05-17
 * @modification $Date: 2006-09-19 15:08:58 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.sinavi2;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.ListIterator;

import javax.swing.JComponent;
import javax.swing.border.EmptyBorder;
import javax.swing.event.InternalFrameEvent;
import javax.swing.event.InternalFrameListener;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuCommonImplementation;
import com.memoire.bu.BuDialogMessage;
import com.memoire.bu.BuInternalFrame;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuPanel;

import org.fudaa.dodico.corba.sinavi2.SParametresManoeuvres;

import org.fudaa.dodico.sinavi2.Sinavi2Helper;

/**
 * impl�mentation d'une fen�tre interne permettant de modifier les dur�es des manoeuvres dans une �cluse
 * 
 * @version $Revision: 1.12 $ $Date: 2006-09-19 15:08:58 $ by $Author: deniger $
 * @author Fatimatou Ka , Beno�t Maneuvrier
 */
public class Sinavi2FilleModManoeuvres extends BuInternalFrame implements ActionListener, InternalFrameListener {

  /** **************************liste des boutons***************************** */

  // private BuCommonInterface _appli;
  private final BuLabel lTitre_ = new BuLabel("Modification des dur�es de manoeuvres du bateau dans l'�cluse");

  /** bouton pour annuler */
  private final BuButton bAnnuler_ = new BuButton(Sinavi2Resource.SINAVI2.getIcon("ANNULER"), "Annuler");

  /** sauver */
  private final BuButton bSauver_ = new BuButton(Sinavi2Resource.SINAVI2.getIcon("VALIDER"), "Sauver");

  private final BuButton bAfficher_ = new BuButton(Sinavi2Resource.SINAVI2.getIcon("EDITER"), "Afficher");

  private final BuLabel lTypeBateau_ = new BuLabel("Type de bateau");
  private final BuLabel vTypeBateau_ = new BuLabel("");

  private final BuLabel lEcluse_ = new BuLabel("Nom de l'�cluse");
  private final BuLabel vEcluse_ = new BuLabel("");

  private final BuLabel lEntree_ = new BuLabel("Manoeuvres en Entr�e");
  private final DureeField vEntreeMinute_ = new DureeField(false, false, false, true, false);
  private final DureeField vEntreeSeconde_ = new DureeField(false, false, false, false, true);

  private final BuLabel lSortie_ = new BuLabel("Manoeuvres en Sortie");
  private final DureeField vSortieMinute_ = new DureeField(false, false, false, true, false);
  private final DureeField vSortieSeconde_ = new DureeField(false, false, false, false, true);
  private final BuPanel pBoutons_ = new BuPanel();
  /**
   * panel contenant les diff�rents champs de la fen�tre
   */
  private final BuPanel pTitre_ = new BuPanel();

  private final BuPanel pDonnees2_ = new BuPanel();

  public ArrayList listeManoeuvres_;
  public Sinavi2Implementation imp_ = null;

  public Sinavi2FilleModManoeuvres(final BuCommonImplementation _appli, final ArrayList _liste,
      final String _bateauCourant, final String _ecluseCourant, final Sinavi2TableauManoeuvre tb) {

    super("Dur�es de Manoeuvres du type de bateau dans l'�cluse", true, true, true, false);
    // "Vitesse du type de bateau dans le bief"
    listeManoeuvres_ = _liste;

    imp_ = (Sinavi2Implementation) _appli.getImplementation();

    bSauver_.addActionListener(this);
    bAnnuler_.addActionListener(this);
    bAfficher_.addActionListener(this);

    ((JComponent) getContentPane()).setBorder(new EmptyBorder(25, 25, 25, 25));

    pTitre_.add(lTitre_, "center");
    final GridBagLayout g2 = new GridBagLayout();
    pDonnees2_.setLayout(g2);
    final GridBagConstraints c = new GridBagConstraints();
    c.fill = GridBagConstraints.BOTH;

    c.gridx = 0;
    c.gridy = 2;
    pDonnees2_.add(lEcluse_, c);
    vEcluse_.setText(_ecluseCourant);
    c.gridx = 1;
    c.gridy = 2;
    pDonnees2_.add(vEcluse_, c);

    c.gridx = 0;
    c.gridy = 3;
    pDonnees2_.add(lTypeBateau_, c);
    vTypeBateau_.setText(_bateauCourant);
    c.gridx = 1;
    c.gridy = 3;
    pDonnees2_.add(vTypeBateau_, c);

    final ListIterator iter = listeManoeuvres_.listIterator();
    boolean trouve = false;
    while (iter.hasNext() & !trouve) {
      SParametresManoeuvres m = new SParametresManoeuvres();
      m = (SParametresManoeuvres) iter.next();
      if (m.type.equalsIgnoreCase(_bateauCourant) && m.ecluse.equalsIgnoreCase(_ecluseCourant)) {
        /*
         * vEntreeMinute_.setDureeField(SinaviTypeBateau.determineSeconde(m.entree));
         * vSortieMinute_.setDureeField(SinaviTypeBateau.determineSeconde(m.sortie));
         */
        vEntreeMinute_.setDureeField(Sinavi2TypeEcluse.determineMinuteSeule(m.entree));
        vEntreeSeconde_.setDureeField(Sinavi2TypeEcluse.determineSecondeSeule(m.entree));
        vSortieMinute_.setDureeField(Sinavi2TypeEcluse.determineMinuteSeule(m.sortie));
        vSortieSeconde_.setDureeField(Sinavi2TypeEcluse.determineSecondeSeule(m.sortie));
      }
    }
    c.gridx = 0;
    c.gridy = 4;
    pDonnees2_.add(lEntree_, c);
    c.gridx = 1;
    c.gridy = 4;
    pDonnees2_.add(vEntreeMinute_, c);
    c.gridx = 2;
    c.gridy = 4;
    pDonnees2_.add(vEntreeSeconde_, c);

    c.gridx = 0;
    c.gridy = 5;
    pDonnees2_.add(lSortie_, c);
    c.gridx = 1;
    c.gridy = 5;
    pDonnees2_.add(vSortieMinute_, c);
    c.gridx = 2;
    c.gridy = 5;
    pDonnees2_.add(vSortieSeconde_, c);

    pBoutons_.setLayout(new FlowLayout(FlowLayout.CENTER));
    pBoutons_.add(bAnnuler_);
    pBoutons_.add(bSauver_);
    pBoutons_.add(bAfficher_);
    // getContentPane().add(pDonnees_, BorderLayout.NORTH);
    getContentPane().add(pTitre_, BorderLayout.NORTH);
    getContentPane().add(pDonnees2_, BorderLayout.CENTER);
    getContentPane().add(pBoutons_, BorderLayout.SOUTH);

    pack();

    /** *reinitialisation des valeurs* */

    setVisible(true);
    // addInternalFrameListener(this);
    imp_.addInternalFrame(this);

  }

  public void actionPerformed(final ActionEvent _e) {
    if (_e.getSource() == bAnnuler_) {
      annuler();

    } else if (_e.getSource() == bSauver_) {
      final int x = -1;
      final SParametresManoeuvres m = new SParametresManoeuvres();
      controler_entrees(x, m);
      System.out.println("x : " + x);
      // affMessage("les dur�es de manoeuvre en entr�e et en sortie ont �t� effectu�es");
      annuler();

      /*
       * if(x!=-1){ listeManoeuvres_.remove(x); listeManoeuvres_.add(x,m); }
       */

    } else if (_e.getSource() == bAfficher_) {
      afficher_manoeuvres();
    }
  }

  private void annuler() {
    imp_.removeInternalFrame(this);
    // imp_.sinavi2fillemodvitesses_ =null;

  }

  private void controler_entrees(int x, SParametresManoeuvres man) {

    x = -1;
    if (vEntreeMinute_.getDureeField() == 0.0) {
      affMessage("Entrez la dur�e en minutes");
    } else if (vSortieMinute_.getDureeField() == 0.0) {
      affMessage("Entrez la dur�e en minutes");
    }

    else {
      // recherche dans la liste des vitesses puis modification
      final SParametresManoeuvres manoeuvreCourant = new SParametresManoeuvres();
      manoeuvreCourant.ecluse = vEcluse_.getText();
      manoeuvreCourant.type = vTypeBateau_.getText();
      /*
       * manoeuvreCourant.entree=SinaviTypeEcluse.determineHeure2(vEntreeMinute_.getDureeField());
       * manoeuvreCourant.sortie=SinaviTypeEcluse.determineHeure2(vSortieMinute_.getDureeField());
       */
      manoeuvreCourant.entree = Sinavi2TypeEcluse.determineHeure(vEntreeMinute_.getDureeField()
          + vEntreeSeconde_.getDureeField());
      manoeuvreCourant.sortie = Sinavi2TypeEcluse.determineHeure(vSortieMinute_.getDureeField()
          + vSortieSeconde_.getDureeField());

      final ListIterator iter = listeManoeuvres_.listIterator();
      boolean trouve = false;
      while (iter.hasNext() & !trouve) {
        SParametresManoeuvres m = new SParametresManoeuvres();
        m = (SParametresManoeuvres) iter.next();

        if (Sinavi2Helper.typeManoeuvresEquals(manoeuvreCourant, m)) {

          m.entree = manoeuvreCourant.entree;
          m.sortie = manoeuvreCourant.sortie;
          trouve = true;
        }

      }
      if (trouve) {
        if (iter.hasNext()) {
          x = iter.nextIndex() - 1;
        } else if (iter.hasPrevious()) {
          x = iter.previousIndex() + 1;
        } else {
          x = 0;
        }

        man = manoeuvreCourant;
      }
    }

  }

  private void afficher_manoeuvres() {
    /** *boucle sur la lecture des bateaux en affichant seuelment identifiant pour voir si ca marche** */

    // affMessage();
    /*
     * ListIterator iter= listeBateaux_.listIterator(); while (iter.hasNext()){ SParametresBateau c= new
     * SParametresBateau(); c= (SParametresBateau) iter.next(); BuDialogMessage dialog_mess = new
     * BuDialogMessage(imp_.getApp(), imp_.InfoSoftSinavi2_, c.identification); dialog_mess.activate(); }
     */
    /*
     * if(listeBiefs_ != null) imp_.afficher_biefs();
     */
    // affMessage();
    annuler();
  }

  public void internalFrameActivated(final InternalFrameEvent _e) {
  // TODO Auto-generated method stub

  }

  public void internalFrameClosed(final InternalFrameEvent _e) {

  // TODO Auto-generated method stub

  }

  public void internalFrameClosing(final InternalFrameEvent _e) {
  // TODO Auto-generated method stub

  }

  public void internalFrameDeactivated(final InternalFrameEvent _e) {
  // TODO Auto-generated method stub

  }

  public void internalFrameDeiconified(final InternalFrameEvent _e) {
  // TODO Auto-generated method stub

  }

  public void internalFrameIconified(final InternalFrameEvent _e) {
  // TODO Auto-generated method stub

  }

  public void internalFrameOpened(final InternalFrameEvent _e) {
  // TODO Auto-generated method stub

  }

  public void affMessage(final String _t) {
    final BuDialogMessage dialog_mess = new BuDialogMessage(imp_.getApp(), imp_.getInformationsSoftware(), "" + _t);
    dialog_mess.activate();
  }

}
