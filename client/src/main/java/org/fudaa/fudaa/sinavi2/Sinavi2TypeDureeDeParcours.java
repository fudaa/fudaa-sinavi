package org.fudaa.fudaa.sinavi2;

import org.fudaa.ctulu.CtuluLibString;

/**
 * @author maneuvrier Cette classe est utilis�e comme structure pour stocker le nom d'un type de bateau et une dur�e
 *         Cette dur�e est transform�e ensuite dans le format souhait�
 */
public class Sinavi2TypeDureeDeParcours {
  public Sinavi2TypeDureeDeParcours() {

  }

  public Sinavi2TypeDureeDeParcours(final int _duree, final String _typeBateau) {
    setDureeParcours(_duree);
    typeBateau = _typeBateau;
  }

  public static double determineHeure(final int _nbSecondes) {
    final int heure = _nbSecondes / 3600;
    final int minute = _nbSecondes / 60 - heure * 60;
    String s = "";
    if (minute > 9) {
      s = heure + CtuluLibString.DOT + minute;
    } else {
      s = heure + ".0" + minute;
    }
    System.out.println("nb seconde :" + _nbSecondes + " -> " + s);
    final Double h = new Double(s);

    return h.doubleValue();
  }

  public static String determineHeureString(final int _nbSecondes) {
    final int heure = _nbSecondes / 3600;
    final int minute = _nbSecondes / 60 - heure * 60;
    String s = "";
    if (minute > 9) {
      s = heure + CtuluLibString.DOT + minute;
    } else {
      s = heure + ".0" + minute;
    }
    System.out.println("nb seconde :" + _nbSecondes + " -> " + s);
    return s;
  }

  public double getDureeParcours() {
    return dureeParcours;
  }

  public void setDureeParcours(final double _dureeParcours) {
    dureeParcours = _dureeParcours / 3600;

  }

  public String getTypeBateau() {
    return typeBateau;
  }

  public void setTypeBateau(final String _typeBateau) {
    typeBateau = _typeBateau;
  }

  double dureeParcours;
  String typeBateau;
}
