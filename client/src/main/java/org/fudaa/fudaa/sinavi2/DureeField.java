/*
 * @file         DureeField.java
 * @creation     2001-05-17
 * @modification $Date: 2006-12-20 16:13:19 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.sinavi2;

import java.awt.Color;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.util.StringTokenizer;

import javax.swing.JComponent;

import com.memoire.bu.BuCharValidator;
import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuStringValidator;
import com.memoire.bu.BuTableCellEditor;
import com.memoire.bu.BuTextField;
import com.memoire.bu.BuValueValidator;

import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.gui.CtuluCellTextRenderer;
import org.fudaa.ctulu.gui.CtuluLibSwing;

/**
 * Composant permettant de saisir des dur�es sous diff�rents formats. Le format des donn�es est contr�l� par des
 * validateurs. Principe de convertion automatique (chaine vers nombre de minutes et vise versa) :<br>
 * 60 minutes : 1 heure <br>
 * 24 heures : 1 jour <br>
 * 31 jours : 1 mois<br>
 * 31 :nombre de jours de janvier<br>
 * 31+28 jours :2 mois ( = nombre de jours de janv. + fevrier ).
 * 
 * @version $Revision: 1.10 $ $Date: 2006-12-20 16:13:19 $ by $Author: deniger $
 * @author Aline Marechalle , Franck Lejeune
 */
public class DureeField extends JComponent implements FocusListener {

  final String sep_ = ":";

  public CtuluCellTextRenderer createRenderer() {
    return new CtuluCellTextRenderer() {
      protected void setValue(final Object _value) {
        super.setValue(STRING_DUREE.valueToString(_value));
      }
    };
  }

  public BuTableCellEditor createEditor() {
    return new BuTableCellEditor(zoneTexte_);
  }
  /**
   * Caract�res valides pour les dur�es : num�ro ou deux points.
   */
  final BuCharValidator CHAR_DUREE = new BuCharValidator() {

    /**
     * retourne la valeur CharValid de DureeField object
     * 
     * @param _char
     * @return La valeur CharValid
     */
    public boolean isCharValid(char _char) {
      // fred
      valueIsOk();
      if (_char == ':') {
        String chaine = zoneTexte_.getText();
        StringTokenizer token = new StringTokenizer(chaine, sep_);
        int nbElts = token.countTokens();
        if (nbElts < nbEltsMax_) {
          return true;
        }
      } else if (Character.isDigit(_char)) {
        return true;
      }
      // fred
      valueIsWrong();
      return false;
    }
  };
  final BuValueValidator VALUE_DUREE = new BuValueValidator() {

    /**
     * retourne la valeur ValueValid de DureeField object
     * 
     * @param _value
     * @return La valeur ValueValid
     */
    public boolean isValueValid(Object _value) {
      // Seules les instances de Integer sont accept�es
      // FRED bizarre
      /*
       * if ((_value instanceof Integer) == false) { return false; }
       */
      // le tooltip par defaut
      valueIsOk();
      if (val_ != null) {
        zoneTexte_.setToolTipText(val_.getDescription());
      }
      if (!(_value instanceof Integer)) {
        return false;
      }
      final int initVal = ((Integer) _value).intValue();
      int val = initVal;
      if (val < 0) {
        // TODO: fredcomme ca l'utilisateur sait pour sa valeur est fausse
        zoneTexte_.setToolTipText(Sinavi2Resource.SINAVI2.getString("La valeur doit �tre positive."));
        valueIsWrong();
        return false;
      }
      // Erreur s'il reste des secondes et qu'elles ne sont pas demand�es
      if (presenceMinutes_ && !presenceSecondes_ && (val % 60) != 0) {
        // TODO: fred ajouter setTooltip...
        zoneTexte_.setToolTipText(Sinavi2Resource.SINAVI2.getString("La valeur doit �tre entre 0 et 59."));
        valueIsWrong();
        return false;
      }
      val = val / 60; // val converti en minutes
      // Erreur s'il reste des minutes et qu'elles ne sont pas demand�es
      if (presenceHeures_ && !presenceMinutes_ && (val % 60) != 0) {
        // TODO: fred ajouter setTooltip...
        zoneTexte_.setToolTipText(Sinavi2Resource.SINAVI2.getString("La valeur doit �tre entre 0 et 59."));
        valueIsWrong();
        return false;
      }
      val = val / 60; // val converti en heures
      // Erreur s'il reste des heures et qu'elles ne sont pas demand�es
      if (presenceJours_ && !presenceHeures_ && (val % 24) != 0) {
        // TODO: fred ajouter setTooltip...
        zoneTexte_.setToolTipText(Sinavi2Resource.SINAVI2.getString("La valeur doit �tre entre 0 et 24."));
        valueIsWrong();
        return false;
      }
      val = val / 24; // val converti en jours
      // Si les jours ne sont pas demand�es
      if (presenceMois_ && !presenceJours_) {
        int m = 0;
        while (val >= (calendrier[m % 12]) * 1440) {
          val = val - (calendrier[m % 12]) * 1440;
          m++;
        }
        // Erreur s'il reste des jours et qu'ils ne sont pas demand�s
        if (val != 0) {
          // TODO: fred ajouter setTooltip...
          zoneTexte_.setToolTipText(Sinavi2Resource.SINAVI2.getString("Les jours ne sont pas demand�s."));
          valueIsWrong();
          return false;
        }
      }
      // fred le validateur specifique
      if (val_ != null) {
        final boolean res = val_.isSecondeValid(initVal);
        if (!res) {
          setToolTipText(val_.getDescription());
          valueIsWrong();
        }
        return res;
      }
      return true;
    }
  };
  /**
   * ....
   */
  final BuStringValidator STRING_DUREE = new BuStringValidator() {

    /**
     * retourne la valeur StringValid de DureeField object
     * 
     * @param _string
     * @return La valeur StringValid
     */
    public boolean isStringValid(String _string) {
      StringTokenizer token = new StringTokenizer(_string, ":");
      if (token.countTokens() > nbEltsMax_) {
        valueIsWrong();
        return false;
      }
      valueIsOk();
      return true;
    }

    /**
     * Transforme le nombre de secondes donn� en param�tre en champ affichable. Les donn�es sont suppos�es accept�es par
     * isValueValid.
     * 
     * @param value
     */
    public String valueToString(Object value) {
      int secondes = ((Integer) value).intValue();
      String nbMois = "";
      String nbJours = "";
      String nbHeures = "";
      String nbMinutes = "";
      String nbSecondes = "";
      // Calcule du nombre de mois s'ils sont demand�s
      if (presenceMois_) {
        int m = 0;
        while (secondes >= (calendrier[m % 12]) * 86400) {
          secondes -= (calendrier[m % 12]) * 86400;
          m++;
        }
        nbMois = (m < 10 ? CtuluLibString.ZERO + String.valueOf(m) : String.valueOf(m));
      }
      // Calcule le nombre de jours s'ils sont demand�s
      if (presenceJours_) {
        int jours = secondes / 86400;
        secondes -= jours * 86400;
        if (presenceMois_) {
          nbJours = sep_;
        }
        nbJours += (jours < 10 ? CtuluLibString.ZERO + String.valueOf(jours) : String.valueOf(jours));
      }
      // Calcule le nombre d'heures si elles sont demand�es
      if (presenceHeures_) {
        int heures = secondes / 3600;
        secondes -= heures * 3600;
        if (presenceJours_) {
          nbHeures = sep_;
        }
        nbHeures += (heures < 10 ? CtuluLibString.ZERO + String.valueOf(heures) : String.valueOf(heures));
      }
      // Calcule le nombre de minutes si elles sont demand�es
      if (presenceMinutes_) {
        int minutes = secondes / 60;
        secondes -= minutes * 60;
        if (presenceHeures_) {
          nbMinutes = sep_;
        }
        nbMinutes += (minutes < 10 ? CtuluLibString.ZERO + String.valueOf(minutes) : String.valueOf(minutes));
      }
      // Affecte les secondes si elles sont demand�es
      if (presenceSecondes_) {
        if (presenceMinutes_) {
          nbSecondes = sep_;
        }
        nbSecondes += (secondes < 10 ? CtuluLibString.ZERO + String.valueOf(secondes) : String.valueOf(secondes));
      }
      return nbMois + nbJours + nbHeures + nbMinutes + nbSecondes;
    }

    /**
     * Calcule le nombre de secondes correspondant au texte donn�. Chaine est suppos� accept� par isStringValid.
     * principe de convertion automatique <br>
     * 60 secondes :1 minute<br>
     * 60 minutes :1 heure<br>
     * 24 heures :1 jour<br>
     * 31 jours:1 mois<br>
     * 31+28 jours :2 mois ( = nombre de jours de janv. + fevrier )
     * 
     * @param chaine
     */
    public Object stringToValue(String chaine) {
      StringTokenizer token = new StringTokenizer(chaine, ":");
      // Nb d'�l�ments de la chaine separes par ":"
      int nbEltDonnes = token.countTokens();
      int nbEltsAttendus = nbEltsMax_;
      int sec = 0;
      if (presenceMois_ && nbEltDonnes == nbEltsAttendus--) {
        int tempo = Integer.parseInt(token.nextToken());
        for (int i = 0; i < tempo; i++) {
          sec += +86400 * calendrier[i % 12];
        }
      }
      if (presenceJours_ && nbEltDonnes >= nbEltsAttendus--) {
        sec += 86400 * Integer.parseInt(token.nextToken());
      }
      if (presenceHeures_ && nbEltDonnes >= nbEltsAttendus--) {
        sec += 3600 * Integer.parseInt(token.nextToken());
      }
      /*
       * if (presenceHeures && nbEltDonnes >= nbEltsAttendus--) { sec += 0 * Integer.parseInt(token.nextToken()); }
       */
      if (presenceMinutes_ && nbEltDonnes >= nbEltsAttendus--) {
        sec += 60 * Integer.parseInt(token.nextToken());
      }
      /*
       * if (presenceMinutes && nbEltDonnes >= nbEltsAttendus--) { sec += 0* Integer.parseInt(token.nextToken()); }
       */

      if (presenceSecondes_ && nbEltDonnes >= nbEltsAttendus--) {
        sec += Integer.parseInt(token.nextToken());
      }
      return new Integer(sec);
    }
  };
  static int[] calendrier = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
  boolean presenceMois_;
  boolean presenceJours_;
  boolean presenceHeures_;
  boolean presenceSecondes_;
  boolean presenceMinutes_;
  BuTextField zoneTexte_ = new BuTextField();
  private final BuLabel label_ = new BuLabel();
  /**
   * Nb maxi d'�l�ments s�par�s par des ":".
   */
  int nbEltsMax_;

  /**
   * Cr�ation d'une zone de texte. Le format des donn�es saisies devra respecter les champs demand�s. ATTENTION : restez
   * coherant si vous creez les mois et les heures, creez aussi les jours.
   * 
   * @param m presence des mois.
   * @param j presence des jours.
   * @param h presence des heures.
   * @param min presence des minutes.
   * @param sec
   */
  public DureeField(final boolean m, boolean j, boolean h, boolean min, final boolean sec) {
    String textLabel = "";
    // test de compatibilite de l'argument
    if ((m) && (!j) && (h)) {
      throw new IllegalArgumentException("ERREUR de DureeField : manque les jours");
    }
    if ((j) && (!h) && (min)) {
      throw new IllegalArgumentException("ERREUR de DureeField : manque les heures");
    }
    if ((h) && (!min) && (sec)) {
      throw new IllegalArgumentException("ERREUR de DureeField : manque les minutes");
    }
    if ((m) && (!j) && (!h) && (min)) {
      throw new IllegalArgumentException("ERREUR de DureeField : manque les jours et les heures");
    }
    if ((m) && (!j) && (!h) && (!min) && (sec)) {
      throw new IllegalArgumentException("ERREUR de DureeField : manque les jours, les heures et les minutes");
    }
    if ((j) && (!h) && (!min) && (sec)) {
      throw new IllegalArgumentException("ERREUR de DureeField : manque les heures et les minutes");
    }
    // Flags pour les champs demand�s
    presenceMois_ = m;
    presenceJours_ = j;
    presenceHeures_ = h;
    presenceMinutes_ = min;
    presenceSecondes_ = sec;
    // Validators pour les dur�e
    zoneTexte_.setCharValidator(CHAR_DUREE);
    zoneTexte_.setValueValidator(VALUE_DUREE);
    zoneTexte_.setStringValidator(STRING_DUREE);
    // Layout manager
    final BuGridLayout lodate = new BuGridLayout(2, 5, 5, false, false);
    setLayout(lodate);
    add(zoneTexte_);
    add(label_);
    zoneTexte_.setColumns(6);
    zoneTexte_.addFocusListener(this);
    if (presenceMois_) {
      textLabel += " mois :";
      nbEltsMax_++;
    }
    if (presenceJours_) {
      textLabel += " jours :";
      nbEltsMax_++;
    }
    if (presenceHeures_) {
      textLabel += " h :";
      nbEltsMax_++;
    }
    if (presenceMinutes_) {
      textLabel += " min :";
      nbEltsMax_++;
    }
    if (presenceSecondes_) {
      textLabel += " sec:";
      nbEltsMax_++;
    }
    label_.setText(textLabel.substring(0, textLabel.length() - 1));
    setValue(0);
  }

  /**
   * Permet de lire la valeur de la zone de saisie (en nombre de secondes).
   * 
   * @return La valeur DureeField
   */
  public int getDureeField() {
    final Integer val = (Integer) zoneTexte_.getValue();
    return val.intValue();
  }

  public int getHeure() {
    return getDureeField() / 3600;
  }

  public int getMin() {
    return getDureeField() / 60;
  }

  /**
   * Pour savoir si le composant est actif. Renvoie true si zoneTexte et label sont actifs
   * 
   * @return La valeur Enabled
   */
  public boolean isEnabled() {
    final boolean flag1 = zoneTexte_.isEnabled();
    final boolean flag2 = label_.isEnabled();
    return (flag1 & flag2);
  }

  // validator
  DureeFieldValidator val_;

  // valeur et description du validator
  public void setDureeValidator(final DureeFieldValidator _validator) {
    val_ = _validator;
    zoneTexte_.setToolTipText(_validator.getDescription());
  }

  // en noir si la valeur est juste
  protected void valueIsOk() {
    label_.setForeground(CtuluLibSwing.getDefaultLabelForegroundColor());
    zoneTexte_.setForeground(CtuluLibSwing.getDefaultLabelForegroundColor());
  }

  // en rouge si la valeur est fausse
  protected void valueIsWrong() {
    label_.setForeground(Color.RED);
    zoneTexte_.setForeground(Color.RED);
  }

  /**
   * Quand on entre dans la zone de texte c'est que l'on entre dans le composant.
   * 
   * @param _e
   */
  public void focusGained(final FocusEvent _e) {
    zoneTexte_.selectAll();
    processFocusEvent(new FocusEvent(this, FocusEvent.FOCUS_GAINED));
  }

  /**
   * Quand on sort de la zone de texte c'est que l'on sort du composant.
   * 
   * @param _e
   */
  public void focusLost(final FocusEvent _e) {
    processFocusEvent(new FocusEvent(this, FocusEvent.FOCUS_LOST));
  }

  public boolean isValueValid() {
    return zoneTexte_.getValue() != null;
  }

  /**
   * Permet de modifier la valeur de la zone de texte.
   * 
   * @param m nombre de mois.
   * @param j nombre de jours.
   * @param h nombre d' heures.
   * @param min nombre de minutes.
   * @param sec nombre de secondes;
   */
  public void setDureeField(final int m, final int j, final int h, final int min, int sec) {
    if ((sec >= 0) && (min >= 0) && (h >= 0) && (j >= 0) && (m >= 0)) {
      sec = sec + min * 60 + h * 60 + j * 24 * 60;
      for (int i = 0; i < m; i++) {
        sec = sec + 60 * 60 * 24 * calendrier[i % 12];
      }
      setDureeField(sec);
    } else {
      setDureeField(-1);
    }
  }

  /**
   * Idem setDureeField(int m,int j,int h, int min).
   * 
   * @param m La nouvelle valeur Value
   * @param j La nouvelle valeur Value
   * @param h La nouvelle valeur Value
   * @param min La nouvelle valeur Value
   * @param sec La nouvelle valeur Value
   */
  public void setValue(final int m, final int j, final int h, final int min, final int sec) {
    setDureeField(m, j, h, min, sec);
  }

  /**
   * Permet de modifier la valeur de la zone de texte.
   * 
   * @param : nouvelle valeur, en minutes.
   */
  public void setDureeField(final int val) {
    zoneTexte_.setValue(new Integer(val));
  }

  public void setValueHeure(final int _heure) {
    zoneTexte_.setValue(new Integer(3600 * _heure));
  }

  public void setValueMin(final int _minute) {
    zoneTexte_.setValue(new Integer(60 * _minute));
  }

  /**
   * Idem setDureeField(int val).
   * 
   * @param val La nouvelle valeur Value
   */
  public void setValue(final int val) {
    setDureeField(val);
  }

  /**
   * Pour activer ou desactiver le composant.
   * 
   * @param _flag La nouvelle valeur Enabled
   */
  public void setEnabled(final boolean _flag) {
    zoneTexte_.setEnabled(_flag);
    label_.setEnabled(_flag);
  }
  /*
   * public static String formatter(boolean pMois, boolean pJours, boolean pHeures, boolean pMinutes,boolean pSecondes,
   * int secondes) { String retour = ""; Calcule du nombre de mois d'ils sont demand�s if ( pMois ) { int m = 0; while
   * (secondes >= (calendrier[m % 12]) * 86400) { secondes -= (calendrier[m % 12]) * 86400; m++; } retour += ( m < 10 ?
   * CtuluLibString.ZERO : "") + String.valueOf( m ) + ":"; } Calcule le nombre de jours s'ils sont demand�s if ( pJours ) {
   * int jours = secondes / 86400; secondes -= jours * 86400; retour += ( jours < 10 ? CtuluLibString.ZERO : "") +
   * String.valueOf(jours) + ":"; } Calcule le nombre d'heures si elles sont demand�es if ( pHeures ) { int heures =
   * secondes / 3600; secondes -= heures * 3600; retour += ( heures < 10 ? CtuluLibString.ZERO : "") +
   * String.valueOf(heures) + ":"; } Calcule le nombre de minutes si elles sont demand�es if ( pHeures ) { int minutes =
   * secondes / 60; secondes -= minutes * 60; retour += ( minutes < 10 ? CtuluLibString.ZERO : "") +
   * String.valueOf(minutes) + ":"; } Affecte les secondes. if ( !pSecondes && secondes>0 ) retour += (secondes < 10 ?
   * CtuluLibString.ZERO : "") + String.valueOf ( secondes ) + "min:"; else if ( pSecondes ) retour += (secondes < 10 ?
   * CtuluLibString.ZERO : "") + String.valueOf ( secondes ) + ":"; return retour.substring(0, retour.length()-1); }
   */
}
