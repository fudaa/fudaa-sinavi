package org.fudaa.fudaa.sinavi2;

import org.fudaa.dodico.corba.sinavi2.SResultatConsommationDEau;

/**
 * @author ka cf Sinavi2TypeBateau Le type Sinavi2TypeConsommationDEau a �t� cr�� afin d'encapsuler les donn�es du
 *         fichier .cons et de les adapter au type SResultatConsommationDEau, structure corba utilis�e pour la
 *         communication Fortran
 */

public class Sinavi2TypeConsommationDEau {

  /** * constructeur sans param�tre* */
  public Sinavi2TypeConsommationDEau() {}

  /**
   * @param numero de l'�cluse
   * @param sens de navigation
   * @param date et heure d'ouverture de l'�cluse
   * @param date et heure de fermeture de
   * @param nombre de bateaux dans l'�cluse
   * @param type de bateau
   */

  public Sinavi2TypeConsommationDEau(final SResultatConsommationDEau _cons) {
    this.numeroEcluse_ = _cons.numeroEcluse;
    this.avalantMontant_ = avalantMontant(_cons.sens);
    this.dateHeureOuverture_ = _cons.dateHeureOuverture;
    this.dateHeureFermeture_ = _cons.dateHeureFermeture;
    this.nbBateaux_ = _cons.nombreBateaux;
    this.typeBateau_ = _cons.typeBateau;

  }

  /** constructeur avec param�tres* */
  public static char avalantMontant(final boolean _avalantMontant) {
    if (_avalantMontant) {
      return 'A';
    } else {
      return 'M';
    }

  }

  /** * accesseurs** */
  public char getAvalantMontant() {
    return avalantMontant_;
  }

  public void setAvalantMontant(final char _avalantMontant) {
    avalantMontant_ = _avalantMontant;
  }

  public int getNumeroEcluse() {
    return numeroEcluse_;
  }

  public void SetNumeroEcluse(final int _numeroEcluse) {
    numeroEcluse_ = _numeroEcluse;
  }

  public int getTypeBateau() {
    return typeBateau_;
  }

  public void SetTypeBateau(final int _typeBateau) {
    typeBateau_ = _typeBateau;
  }

  public double getDateHeureOuverture() {
    return dateHeureOuverture_;
  }

  public void SetDateHeureOuverture(final double _dateHeureOuverture) {
    dateHeureOuverture_ = _dateHeureOuverture;
  }

  public double getDateFermeture() {
    return dateHeureFermeture_;
  }

  public void SetDateHeureFermeture(final double _dateHeureFermeture) {
    dateHeureFermeture_ = _dateHeureFermeture;
  }

  public int getNbBateaux() {
    return nbBateaux_;
  }

  public void SetNbBateaux(final int _nbBateaux) {
    nbBateaux_ = _nbBateaux;
  }

  int numeroEcluse_; // num�ro de l'�cluse
  char avalantMontant_; // true pour avalant
  double dateHeureOuverture_; // heure d'ouverture de l'�cluse
  double dateHeureFermeture_; // heure de fermeture de l'�cluse
  int nbBateaux_; // nombre de bateaux
  int typeBateau_; // type de bateau

}
