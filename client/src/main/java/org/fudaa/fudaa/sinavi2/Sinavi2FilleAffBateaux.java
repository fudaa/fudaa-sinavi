/*
 * @file         Sinavi2FilleAffBateaux.java
 * @creation     2005-09-17
 * @modification $Date: 2007-05-04 14:01:05 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.sinavi2;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.ListIterator;

import javax.swing.JComponent;
import javax.swing.border.EmptyBorder;
import javax.swing.event.InternalFrameEvent;
import javax.swing.event.InternalFrameListener;

import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

import com.memoire.bu.BuBorderLayout;
import com.memoire.bu.BuButton;
import com.memoire.bu.BuCommonImplementation;
import com.memoire.bu.BuInternalFrame;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuScrollPane;
import com.memoire.bu.BuTable;
import com.memoire.bu.BuTableSortModel;
import com.memoire.fu.FuLog;

import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.table.CtuluTableExcelWriter;

import org.fudaa.dodico.corba.sinavi2.SParametresBateau;

import org.fudaa.dodico.sinavi2.Sinavi2Helper;

import org.fudaa.fudaa.ressource.FudaaResource;

/**
 * impl�mentation d'une fen�tre interne permettant d'afficher les param�tres des bateaux sous forme de tableau. Cette
 * fen�tre regroupe toutes les fonctionnalit�s li�es aux bateaux ajouter/Modifier/Supprimer/Imprimer
 * 
 * @version $Revision: 1.19 $ $Date: 2007-05-04 14:01:05 $ by $Author: deniger $
 * @author Fatimatou Ka , Beno�t Maneuvrier
 */
public class Sinavi2FilleAffBateaux extends BuInternalFrame implements ActionListener, InternalFrameListener {

  private static final int AUTO_RESIZE_ALL_COLUMNS = 0;

  private final BuLabel lTitre_ = new BuLabel("Affichage des types de bateau");

  private final BuButton bAnnuler_ = new BuButton(FudaaResource.FUDAA.getIcon("quitter"), "Quitter");
  private final BuButton bAjouter_ = new BuButton(FudaaResource.FUDAA.getIcon("ajouter"), "Ajouter");
  private final BuButton bModifier_ = new BuButton(FudaaResource.FUDAA.getIcon("editer"), "Modifier");
  private final BuButton bSupprimer_ = new BuButton(FudaaResource.FUDAA.getIcon("enlever"), "Suppimer");
  private final BuButton bImprimer_ = new BuButton(FudaaResource.FUDAA.getIcon("imprimer"), "Imprimer");
  private final BuButton bMiseAJour_ = new BuButton(FudaaResource.FUDAA.getIcon("rafraichir"), "Mise � Jour");

  /** **gerer la liste des bateaux. */
  // ---------------------------------public LinkedList listeBateaux2_;
  public ArrayList listeBateaux2_;
  /** **bateau pour les champs en cas de modifications. */
  public SParametresBateau batCourant_;
  // private int nbat_=-1;

  /** * panel contenant les boutons, il est plac� en bas. */

  private final BuPanel pBoutons_ = new BuPanel();
  private final BuPanel pTitre_ = new BuPanel();
  private final BuPanel pDonnees2_ = new BuPanel();

  public BuScrollPane scrollPane_;
  public Sinavi2TableauBateau tb_;
  public BuTable table_;
  public BuTableSortModel tabBateaux_;
  public Sinavi2Implementation imp2_;

  /**
   * constructeur de la fen�tre d'affichage des bateaux.
   * 
   * @param _appli :instance de Sinavi2implementation
   * @param _liste2 : listeBateaux -> liste des bateaux
   */
  public Sinavi2FilleAffBateaux(final BuCommonImplementation _appli, final ArrayList _liste2) {
    super("Affichage des Types de Bateaux", true, true, true, false);
    pTitre_.add(lTitre_);
    listeBateaux2_ = _liste2;

    miseAJour(listeBateaux2_);

    table_.setAutoResizeMode(AUTO_RESIZE_ALL_COLUMNS);
    table_.setRowSelectionAllowed(true);
    table_.setColumnSelectionAllowed(false);

    scrollPane_ = new BuScrollPane(table_);
    pDonnees2_.setLayout(new BuBorderLayout(0, 0, true, true));
    pDonnees2_.add(scrollPane_);
    table_.setAutoscrolls(true);

    imp2_ = (Sinavi2Implementation) _appli.getImplementation();

    bAnnuler_.addActionListener(this);
    bAjouter_.addActionListener(this);
    bModifier_.addActionListener(this);
    bSupprimer_.addActionListener(this);
    bImprimer_.addActionListener(this);
    bMiseAJour_.addActionListener(this);
    /**
     * Ceci permet de "griser" les boutons, d'emp�cher le clic C'est utilis� pour le mode consultation, qd on observe
     * une simulation qui a d�j� �t� lanc�e.
     */
    if (!imp2_.isPermettreModif()) {
      bAjouter_.setEnabled(false);
      bModifier_.setEnabled(false);
      bSupprimer_.setEnabled(false);
    }

    ((JComponent) getContentPane()).setBorder(new EmptyBorder(5, 5, 5, 5));

    pBoutons_.setLayout(new FlowLayout(FlowLayout.CENTER));
    pBoutons_.add(bAnnuler_);
    pBoutons_.add(bAjouter_);
    pBoutons_.add(bModifier_);
    pBoutons_.add(bSupprimer_);
    pBoutons_.add(bImprimer_);
    pBoutons_.add(bMiseAJour_);
    getContentPane().add(pTitre_, BorderLayout.NORTH);
    getContentPane().add(pDonnees2_, BorderLayout.CENTER);
    getContentPane().add(pBoutons_, BorderLayout.SOUTH);

    pack();

    setVisible(true);
    addInternalFrameListener(this);
    imp2_.addInternalFrame(this);
    this.closable = true;
  }

  /**
   * on parcours les bateaux et on les ins�re dans le tableau en modifiant le format de fa�on ad�quate.
   * 
   * @param _lstBateaux : liste des bateaux
   */
  public void miseAJour(final ArrayList _lstBateaux) {
    if (tb_ == null) {
      tb_ = new Sinavi2TableauBateau();
    }
    if (_lstBateaux != null) {
      final ListIterator iter = _lstBateaux.listIterator();
      int row = 0;
      while (iter.hasNext()) {

        SParametresBateau c = new SParametresBateau();
        c = (SParametresBateau) iter.next();
        tb_.setValueAt(c.identification, row, 0);

        String s = "";
        s = (Double.toString(c.longueur));
        tb_.setValueAt(s, row, 1);

        s = (Double.toString(c.largeur));
        tb_.setValueAt(s, row, 2);

        s = (Double.toString(c.debutNavigation));
        s = Sinavi2Helper.en2Chiffres(s);
        tb_.setValueAt(s, row, 3);

        s = (Double.toString(c.finNavigation));
        s = Sinavi2Helper.en2Chiffres(s);
        tb_.setValueAt(s, row, 4);

        /** *charge** */
        final int nbCar_ = c.identification.length() - 1;
        String temp = "";
        temp = c.identification;
        temp = temp.substring(0, temp.length() - 1);

        if (c.identification.charAt(nbCar_) == 'C') {
          tb_.setValueAt("C", row, 5);
        } else {
          tb_.setValueAt("L", row, 5);
        }

        s = (Double.toString(c.tirantDeau));
        tb_.setValueAt(s, row, 6);

        s = (Double.toString(c.dureeGeneAdmissible));
        s = Sinavi2Helper.en2Chiffres(s);
        tb_.setValueAt(s, row, 7);

        s = (Double.toString(c.vitesseMontantParDefaut));
        tb_.setValueAt(s, row, 8);

        s = (Double.toString(c.vitesseDescendantParDefaut));
        tb_.setValueAt(s, row, 9);

        row++;
        tb_.setNbRow(row);

      }
    }
    table_ = new BuTable(tb_.data_, tb_.nomCol());
    table_.repaint();
  }

  /**
   * @param _e
   */

  public void annuler() {

    imp2_.removeInternalFrame(this);
    // imp2_.sinavi2filleaffbateaux_ =null;
    imp2_.resetFille2AffBateaux();
  }

  public void actionPerformed(final ActionEvent _e) {
    if (_e.getSource() == bAnnuler_) {
      annuler();

    }

    else if (_e.getSource() == bAjouter_) {
      imp2_.ajouterBateaux(-1, false);

    } else if (_e.getSource() == bModifier_) {

      final int selection = table_.getSelectedRow();
      if (selection >= 0) {
        imp2_.ajouterBateaux(selection, true);
      } else {
        imp2_.affMessage("Selectionnez une ligne");
        /*
         * if(controler_entrees()==true){ JOptionPane.showMessageDialog(null,"Cr�ation R�ussie"); initialise_champs(-1);
         */
      }
    } else if (_e.getSource() == bSupprimer_) {
      final int selection = table_.getSelectedRow();
      if (selection >= 0) {
        imp2_.supprimerBateaux(selection);
      } else {
        imp2_.affMessage("Selectionnez une ligne");
        /*
         * if(controler_entrees()==true){ JOptionPane.showMessageDialog(null,"Cr�ation R�ussie"); initialise_champs(-1);
         */
      }
    } else if (_e.getSource() == bImprimer_) {
      /**
       * on fait choisir un fichier � l'aide d'un chooser on cr� un tableau temporaire tbtemp o� : on ajoute une ligne
       * avec le titre et du blanc pour les autres cellules de la ligne on ajoute les colonnes sur la ligne suivante on
       * ajoute enfin les donn�es on cr�e le fichier excel on ecrit le bateau
       */
      final File f = imp2_.enregistrerXls();
      if (f == null) {
        imp2_.affMessage("Nom de Fichier incorrect");
        return;
      }
      final Sinavi2TableauBateau tbtemp = new Sinavi2TableauBateau();
      tbtemp.data_ = new Object[tb_.getRowCount() + 2][tb_.getColumnCount()];
      tbtemp.data_[0][0] = "Liste des Types de Bateaux";
      for (int i = 1; i < tbtemp.getColumnCount(); i++) {
        tbtemp.data_[0][i] = "";
      }
      tbtemp.initNomCol(1);
      tbtemp.setNbRow(tb_.getRowCount() + 2);
      for (int i = 2; i <= (tb_.getRowCount() + 1); i++) {
        for (int j = 0; j < tb_.getColumnCount(); j++) {
          if (j == 3 || j == 4 || j == 7) {
            tbtemp.data_[i][j] = tb_.getValue(i - 2, j).toString().replace('.', 'h');
          } else {
            tbtemp.data_[i][j] = tb_.getValue(i - 2, j);
          }
        }
      }
      tbtemp.setColumnName("Liste des Types de Bateaux", 0);
      for (int i = 1; i < tb_.getColumnCount(); i++) {
        tbtemp.setColumnName(CtuluLibString.ESPACE, i);
      }
      final CtuluTableExcelWriter test = new CtuluTableExcelWriter(tbtemp, f);

      try {
        test.write(null);

      } catch (final RowsExceededException _err) {
        FuLog.error(_err);
      } catch (final WriteException _err) {
        FuLog.error(_err);
      } catch (final IOException _err) {
        FuLog.error(_err);
      }

    } else if (_e.getSource() == bMiseAJour_) {
      annuler();
      imp2_.afficherBateaux();
    }
  }

  public void internalFrameActivated(final InternalFrameEvent _e) {

  }

  public void internalFrameClosed(final InternalFrameEvent _e) {
    imp2_.resetFille2AffBateaux();
  }

  public void internalFrameDeactivated(final InternalFrameEvent _e) {

  }

  public void internalFrameDeiconified(final InternalFrameEvent _e) {

  }

  public void internalFrameIconified(final InternalFrameEvent _e) {

  }

  public void internalFrameOpened(final InternalFrameEvent _e) {

  }

  public void internalFrameClosing(final InternalFrameEvent _e) {

  }

}
