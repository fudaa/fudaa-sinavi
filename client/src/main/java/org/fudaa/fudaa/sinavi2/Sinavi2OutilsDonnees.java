/*
 * @creation     2006-06-01
 * @modification $Date$
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.fr
 */
package org.fudaa.fudaa.sinavi2;

import java.util.ArrayList;
import java.util.ListIterator;

import org.fudaa.ctulu.CtuluLibString;

import org.fudaa.dodico.corba.sinavi2.SParametresBateau;
import org.fudaa.dodico.corba.sinavi2.SParametresBief;
import org.fudaa.dodico.corba.sinavi2.SParametresCroisements;
import org.fudaa.dodico.corba.sinavi2.SParametresEcluse;
import org.fudaa.dodico.corba.sinavi2.SParametresManoeuvres;
import org.fudaa.dodico.corba.sinavi2.SParametresTrematages;
import org.fudaa.dodico.corba.sinavi2.SParametresVitesses;

import org.fudaa.dodico.sinavi2.Sinavi2Helper;

/**
 * Classe qui fourni des outils pour manipuler les donn�es : listes de bateaux, �cluses, biefs, etc...
 * 
 * @version $Revision$ $Date$ by $Author$
 * @author Beno�t Maneuvrier, Fatimatou Ka
 */

public final class Sinavi2OutilsDonnees {
  private Sinavi2OutilsDonnees() {}

  public static ArrayList creationListeCroisements_(final ArrayList _listeBiefs, final ArrayList _listeBateaux) {
    // par d�faut on met tout a oui
    final ArrayList lC = new ArrayList();
    final ListIterator itBief = _listeBiefs.listIterator();
    final ListIterator itBat = _listeBateaux.listIterator();
    final String[] bat = new String[_listeBateaux.size()];
    int i = 0;
    while (itBat.hasNext()) {
      SParametresBateau b = new SParametresBateau();
      b = (SParametresBateau) itBat.next();
      bat[i] = b.identification;
      i++;
    }

    while (itBief.hasNext()) {
      SParametresBief b = new SParametresBief();
      b = (SParametresBief) itBief.next();
      for (int j = 0; j < bat.length; j++) {
        for (int k = 0; k < bat.length; k++) {
          final SParametresCroisements c = new SParametresCroisements();
          c.bief = b.identification;
          c.type1 = bat[j];
          c.type2 = bat[k];
          c.ouiNon = true;
          lC.add(c);
        }
      }
    }
    return lC;
  }

  public static ArrayList creationListeCroisements_(final ArrayList _listeBiefs, final ArrayList _listeBateaux,
      final ArrayList _listeCroisements) {

    boolean trouve = false;

    final ArrayList lC = new ArrayList();
    final ListIterator itBief = _listeBiefs.listIterator();
    final ListIterator itBat = _listeBateaux.listIterator();

    final String[] bat = new String[_listeBateaux.size()];
    int i = 0;
    while (itBat.hasNext()) {
      SParametresBateau b = new SParametresBateau();
      b = (SParametresBateau) itBat.next();
      bat[i] = b.identification;
      i++;
    }
    while (itBief.hasNext()) {
      SParametresBief b = new SParametresBief();
      b = (SParametresBief) itBief.next();
      for (int j = 0; j < bat.length; j++) {
        for (int k = 0; k < bat.length; k++) {
          final SParametresCroisements c = new SParametresCroisements();
          c.bief = b.identification;
          c.type1 = bat[j];
          c.type2 = bat[k];
          final ListIterator itCro = _listeCroisements.listIterator();
          while (itCro.hasNext() && !trouve) {
            SParametresCroisements tmp = new SParametresCroisements();
            tmp = (SParametresCroisements) itCro.next();
            if (Sinavi2Helper.typeCroisementsEquals(tmp, c)) {
              trouve = true;

              if (!tmp.ouiNon) {
                System.out.println("erreur croisements " + bat[j] + CtuluLibString.ESPACE + bat[k]);
              }
              c.ouiNon = tmp.ouiNon;
            }
          }
          if (!trouve) {
            c.ouiNon = true;
          }

          lC.add(c);
          trouve = false;
        }
      }
    }
    return lC;
  }

  public static ArrayList creationListeManoeuvres(final ArrayList _listeBateaux, final ArrayList _listeEcluses,
      final ArrayList _listeManoeuvres) {

    boolean trouve = false;

    final ArrayList lM = new ArrayList();
    final ListIterator itEcl = _listeEcluses.listIterator();
    final ListIterator itBat = _listeBateaux.listIterator();

    /* remplissage des manoeuvres dans une liste */
    final String[] bateaux = new String[_listeBateaux.size()];
    int i = 0;
    while (itBat.hasNext()) {
      SParametresBateau b = new SParametresBateau();
      b = (SParametresBateau) itBat.next();
      bateaux[i] = b.identification;
      i++;
    }

    while (itEcl.hasNext()) {
      SParametresEcluse e = new SParametresEcluse();
      e = (SParametresEcluse) itEcl.next();
      for (int j = 0; j < bateaux.length; j++) {
        final SParametresManoeuvres m = new SParametresManoeuvres();
        m.type = bateaux[j];
        m.ecluse = e.identification;

        if (_listeManoeuvres != null) {
          final ListIterator itMan = _listeManoeuvres.listIterator();
          while (itMan.hasNext() && !trouve) {
            SParametresManoeuvres tmp = new SParametresManoeuvres();
            tmp = (SParametresManoeuvres) itMan.next();
            if (Sinavi2Helper.typeManoeuvresEquals(tmp, m)) {
              m.entree = tmp.entree;
              m.sortie = tmp.sortie;
              trouve = true;
            }
            if (!trouve) {
              m.entree = e.dureeManoeuvresEnEntree;
              m.sortie = e.dureeManoeuvresEnSortie;
            }
          }
        } else {
          m.entree = e.dureeManoeuvresEnEntree;
          m.sortie = e.dureeManoeuvresEnSortie;
        }

        lM.add(m);
        trouve = false;
      }
    }
    return lM;
  }

  public static ArrayList creationListeTrematages(final ArrayList _listeBiefs, final ArrayList _listeBateaux) {
    // par d�faut on met tout a oui
    final ArrayList lC = new ArrayList();
    final ListIterator itBief = _listeBiefs.listIterator();
    final ListIterator itBat = _listeBateaux.listIterator();
    final String[] bat = new String[_listeBateaux.size()];
    int i = 0;
    while (itBat.hasNext()) {
      SParametresBateau b = new SParametresBateau();
      b = (SParametresBateau) itBat.next();
      bat[i] = b.identification;
      i++;
    }

    while (itBief.hasNext()) {
      SParametresBief b = new SParametresBief();
      b = (SParametresBief) itBief.next();
      for (int j = 0; j < bat.length; j++) {
        for (int k = 0; k < bat.length; k++) {
          final SParametresTrematages c = new SParametresTrematages();
          c.bief = b.identification;
          c.type1 = bat[j];
          c.type2 = bat[k];
          c.ouiNon = true;
          lC.add(c);
        }
      }
    }
    return lC;

  }

  public static ArrayList creationListeTrematages_(final ArrayList _listeBiefs, final ArrayList _listeBateaux,
      final ArrayList _listeTrematages) {

    boolean trouve = false;

    final ArrayList lC = new ArrayList();
    final ListIterator itBief = _listeBiefs.listIterator();
    final ListIterator itBat = _listeBateaux.listIterator();

    final String[] bat = new String[_listeBateaux.size()];
    int i = 0;
    while (itBat.hasNext()) {
      SParametresBateau b = new SParametresBateau();
      b = (SParametresBateau) itBat.next();
      bat[i] = b.identification;
      i++;
    }
    while (itBief.hasNext()) {
      SParametresBief b = new SParametresBief();
      b = (SParametresBief) itBief.next();
      for (int j = 0; j < bat.length; j++) {
        for (int k = 0; k < bat.length; k++) {
          final SParametresTrematages c = new SParametresTrematages();
          c.bief = b.identification;
          c.type1 = bat[j];
          c.type2 = bat[k];
          final ListIterator itCro = _listeTrematages.listIterator();
          while (itCro.hasNext() && !trouve) {
            SParametresTrematages tmp = new SParametresTrematages();
            tmp = (SParametresTrematages) itCro.next();
            if (Sinavi2Helper.typeTrematagesEquals(tmp, c)) {
              trouve = true;
              c.ouiNon = tmp.ouiNon;
            }
          }
          if (!trouve) {
            c.ouiNon = true;
          }

          lC.add(c);
          trouve = false;
        }
      }
    }
    return lC;
  }

  // fonction pour creer une liste de vitesse a partir d'une vitesse existante
  public static ArrayList creationListeVitesses(final ArrayList _listeBateaux, final ArrayList _listeBiefs,
      final ArrayList _listeVitesses) {

    boolean trouve = false;

    final ArrayList lV = new ArrayList();
    final ListIterator itBief = _listeBiefs.listIterator();
    final ListIterator itBat = _listeBateaux.listIterator();

    /* remplissage des vitesses dans une liste */
    final String[] bief = new String[_listeBiefs.size()];
    final double[] biefVit = new double[_listeBiefs.size()];
    int i = 0;
    while (itBief.hasNext()) {
      SParametresBief b = new SParametresBief();
      b = (SParametresBief) itBief.next();
      bief[i] = b.identification;
      biefVit[i] = b.vitesse;
      i++;
    }
    while (itBat.hasNext()) {
      SParametresBateau b = new SParametresBateau();
      b = (SParametresBateau) itBat.next();
      for (int j = 0; j < bief.length; j++) {
        final SParametresVitesses c = new SParametresVitesses();
        c.bateau = b.identification;
        c.bief = bief[j];
        System.out.println("bateau " + c.bateau);
        System.out.println("bief " + c.bief);
        if (_listeVitesses != null) {
          final ListIterator itVit = _listeVitesses.listIterator();
          while (itVit.hasNext() && !trouve) {
            SParametresVitesses tmp = new SParametresVitesses();
            tmp = (SParametresVitesses) itVit.next();
            if (Sinavi2Helper.typeVitessesEquals(tmp, c)) {
              c.vitesseDesMontants = tmp.vitesseDesMontants;
              c.vitesseDesAvalants = tmp.vitesseDesAvalants;
              trouve = true;
            }
            if (!trouve) {
              c.vitesseDesMontants = Math.min(b.vitesseMontantParDefaut, biefVit[j]);
              c.vitesseDesAvalants = Math.min(b.vitesseDescendantParDefaut, biefVit[j]);
            }
          }
        } else {
          c.vitesseDesMontants = Math.min(b.vitesseMontantParDefaut, biefVit[j]);
          c.vitesseDesAvalants = Math.min(b.vitesseDescendantParDefaut, biefVit[j]);
          System.out.println("montant " + c.vitesseDesMontants);
          System.out.println("avalant " + c.vitesseDesAvalants);
        }
        lV.add(c);
        trouve = false;
      }
    }
    return lV;
  }

}
