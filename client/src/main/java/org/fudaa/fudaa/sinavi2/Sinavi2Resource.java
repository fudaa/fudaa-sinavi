/*
 * @creation     2001-05-17
 * @modification $Date: 2006-09-19 15:08:58 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.sinavi2;

import com.memoire.bu.BuResource;

import org.fudaa.fudaa.ressource.FudaaResource;

/**
 * Ressource de Sinavi.
 * 
 * @version $Revision: 1.7 $ $Date: 2006-09-19 15:08:58 $ by $Author: deniger $
 * @author Fatimatou Ka , Beno�t Maneuvrier
 */

public class Sinavi2Resource extends FudaaResource {

  public final static Sinavi2Resource SINAVI2 = new Sinavi2Resource(FudaaResource.FUDAA);

  public Sinavi2Resource(final BuResource _parent) {
    super(_parent);
    ;
  }
}
