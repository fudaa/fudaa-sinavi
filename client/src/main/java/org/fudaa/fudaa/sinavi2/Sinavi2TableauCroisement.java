package org.fudaa.fudaa.sinavi2;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

import javax.swing.table.AbstractTableModel;

import jxl.write.Label;
import jxl.write.Number;
import jxl.write.WritableCell;

import org.fudaa.dodico.corba.sinavi2.SParametresCroisements;

/**
 * @author maneuvrier Cette classe a pour but d'afficher le tableau des croisements (c'est � dire le droit de doubler ou
 *         non) et d'offrir la possibilit� de l'impression c'est � dire la cr�ation d'un tableau excel d'o�
 *         l'impl�mentation de CtuluTableModelInterface. elle est repr�sent�e sous forme de matrice avec les types de
 *         bateaux en lignes et en colonnes pour un bief donn�. cf. Sinavi2TabbleauTrematage
 */
public class Sinavi2TableauCroisement extends AbstractTableModel implements
    org.fudaa.ctulu.table.CtuluTableModelInterface {

  private int nbBateau_ = 0;
  private String[] nomsBateaux_ = null;
  public Object[][] data_;// =new Object[15][15];

  // mise � jour des infos de croisement pour le bief � l'aide data
  public void mAJCroisements(final String _bief, final ArrayList _croisements) {
    // est ce que l'ordre a de l'importance ?
    final ListIterator iter = _croisements.listIterator();
    while (iter.hasNext()) {
      SParametresCroisements c = new SParametresCroisements();
      c = (SParametresCroisements) iter.next();
      if (c.bief.equalsIgnoreCase(_bief)) {
        final int type1 = rechercheNumColonne(c.type1) + 1;
        final int type2 = rechercheNumColonne(c.type2) + 1;
        if (data_[type1][type2].toString().equalsIgnoreCase("O")) {
          c.ouiNon = true;
        } else {
          c.ouiNon = false;
          System.out.println("hup");
        }
      }
    }

  }

  public int rechercheNumColonne(final String _s) {
    boolean trouve = false;
    int i = 0;
    while (!trouve & i < nomsBateaux_.length) {// !trouve facultatif
      if (nomsBateaux_[i] == _s)// test egal
      {
        trouve = true;
        return i;
      } else {
        i++;
      }
    }
    // if(!trouve)//facultatif
    return -1;
  }

  // faire dans mise a jour remplir data

  public void addNomsBateaux(final String _s) {
    if (nomsBateaux_ != null) {
      final String[] temp = new String[nomsBateaux_.length + 1];
      int i = 0;
      for (i = 0; i < nomsBateaux_.length; i++) {
        temp[i] = nomsBateaux_[i];
      }
      temp[i] = _s;
      nomsBateaux_ = new String[temp.length];
      nomsBateaux_ = temp;
    } else {
      nomsBateaux_ = new String[1];
      nomsBateaux_[0] = _s;
    }
    nbBateau_++;
  }

  public void supNomsBateaux(final String _s) {
    final String[] temp = new String[nomsBateaux_.length - 1];
    boolean trouve = false;
    for (int i = 0; i < nomsBateaux_.length; i++) {
      if (!nomsBateaux_[i].equalsIgnoreCase(_s) && !trouve) {
        temp[i] = nomsBateaux_[i];
      } else if (!nomsBateaux_[i].equalsIgnoreCase(_s) && trouve) {
        temp[i - 1] = nomsBateaux_[i];
      } else {
        trouve = true;
      }
    }
    nomsBateaux_ = temp;

  }

  public String[] getNomBateau() {
    return nomsBateaux_;
  }

  public void setNbRow(final int _n) {}

  public void setNbBateau(final int _n) {
    nbBateau_ = _n;

  }

  public int getNbBateau() {
    return nbBateau_;

  }
  
   public int[] getSelectedRows() {
        return null;
    }

  public void initNomCol(final int _row) {
    for (int i = 0; i < getNbBateau(); i++) {
      data_[_row][i + 1] = new Object();
      data_[i + 1 + _row][0] = new Object();
      data_[_row][i + 1] = nomsBateaux_[i];
      data_[i + 1 + _row][0] = nomsBateaux_[i];
    }
  }

  public int getColumnCount() {

    return nbBateau_ + 1;
  }

  public int getRowCount() {
    /*
     * if(data!=null) return data.length; else
     */
    return nbBateau_ + 1;// nbrow;//data.length;
    // return 0;
  }

  public Object getValueAt(final int _rowIndex, final int _columnIndex) {

    return data_[_rowIndex][_columnIndex];
  }

  public void setValueAt(final Object value, final int row, final int col) {
    final String x = value.toString();
    // System.out.println(x);
    data_[row][col] = new Object();
    // 0.out.println(data[row][col].toString());
    data_[row][col] = x;

    // fireTableCellUpdated(row, col);

  }

  // String x=new String(value);
  // x=value;
  /*
   * Object [][] temp=new Object[nbbateau+1][nbbateau+1]; if(data!=null){ for(int i=1;i<=row;i++){ if(i==row){ for(int
   * j=1;j<col;j++){ //temp[i][j]=new Object(); temp[i][j]=data[i][j]; } } else { for(int j=1;j<getColumnCount();j++){
   * //temp[i][j]=new Object(); temp[i][j]=data[i][j]; } } } } temp[row][col]=value; data=new
   * Object[row+1][getColumnCount()]; for(int i=1;i<=row;i++){ if(i==row){ for(int j=1;j<=col;j++){ //temp[i][j]=new
   * Object(); data[i][j]=temp[i][j]; } } else { for(int j=1;j<getColumnCount();j++){ //temp[i][j]=new Object();
   * data[i][j]=temp[i][j]; } } } //init_nomcol(); //data[row][col]=new String(value); // nbrow=row+1;
   * //fireTableCellUpdated(row, col); }
   */

  public int getMaxCol() {
    // TODO Auto-generated method stub
    return nomsBateaux_.length + 1;
  }

  public int getMaxRow() {
    // TODO Auto-generated method stub
    return nomsBateaux_.length + 1;
  }

  public Object getValue(final int _row, final int _col) {
    // TODO Auto-generated method stub
    return getValueAt(_row, _col);
  }

  public WritableCell getExcelWritable(final int _row, final int _col, int _rowXls, int _colXls) {
    final int r = _row;
    final int c = _col;
    /*
     * if (column_ != null) c = column_[c]; if (row_ != null) { r = row_[r]; }
     */
    final Object o = data_[r][c];
    if (o == null) {
      return null;
    }
    String s = o.toString();
    if (s == null) {
      return null;
    }
    s = s.trim();
    if (s.length() == 0) {
      return null;
    }
    try {
      return new Number(_colXls, _rowXls, Double.parseDouble(s));
    } catch (final NumberFormatException e) {}
    return new Label(_colXls, _rowXls, s);

    // return cell;
  }

  public String getColumnName(final int i) {
    // FuLog.debug(""+nomsBateaux_[i]);
    if (i == 0) {
      return "";
    } else {
      return nomsBateaux_[i - 1];
    }

  }

  public void setColumnName(final String _name, final int _i) {
    nomsBateaux_[_i] = _name;

  }

public List<String> getComments() {
	// TODO Auto-generated method stub
	return null;
}

}
