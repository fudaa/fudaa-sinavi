/*
 * @file         SinaviFilleChoixCalcul.java
 * @creation     2001-05-17
 * @modification $Date: 2006-12-20 16:13:19 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.sinavi2;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

import javax.swing.JComponent;
import javax.swing.border.EmptyBorder;
import javax.swing.event.InternalFrameEvent;
import javax.swing.event.InternalFrameListener;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuCommonImplementation;
import com.memoire.bu.BuInternalFrame;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuTextField;

import org.fudaa.ctulu.gui.CtuluLibSwing;

import org.fudaa.dodico.corba.sinavi2.SParametresEcluse;

/**
 * impl�mentation d'une fen�tre interne permettant d'ajouter ou modifier une �cluse
 * 
 * @version $Revision: 1.14 $ $Date: 2006-12-20 16:13:19 $ by $Author: deniger $
 * @author Fatimatou Ka ,Beno�t Maneuvrier Classe utilis�e pour ajouter ou modifier une �cluse cf.
 *         Sinavi2FilleAddModBateaux
 */
public class Sinavi2FilleAddModEcluses extends BuInternalFrame implements ActionListener, InternalFrameListener,
    FocusListener {

  protected boolean modif_;
  private final BuLabel lBlanc_ = new BuLabel("    ");
  private final BuLabel lBlanc2_ = new BuLabel("    ");

  private final BuLabel lTitre_ = new BuLabel("Ecluse");

  /** bouton pour annuler */
  private final BuButton bAnnuler_ = new BuButton(Sinavi2Resource.SINAVI2.getIcon("QUITTER"), "Quitter");

  /** sauver */
  private final BuButton bSauver_ = new BuButton(Sinavi2Resource.SINAVI2.getIcon("VALIDER"), "Sauver");

  /** afficher les ecluses */
  private final BuButton bAfficher_ = new BuButton(Sinavi2Resource.SINAVI2.getIcon("EDITER"), "Afficher");

  // private BuButton bDisponibilit�s_ = new BuButton(Sinavi2Resource.SINAVI2.getIcon(""),"Disponibilit�s");
  /** caract�ristiques du ecluses en haut */
  private final BuLabel lId_ = new BuLabel("Identification");
  private final BuTextField tId_ = new BuTextField("");

  private final BuLabel lLong_ = new BuLabel("Longueur");
  private final LongueurField tLong_ = new LongueurField(false, true, false);

  private final BuLabel lLarg_ = new BuLabel("Largeur");
  private final LongueurField tLarg_ = new LongueurField(false, true, false);

  private final BuLabel lPro_ = new BuLabel("Profondeur");
  private final LongueurField tPro_ = new LongueurField(false, true, true);

  private final BuLabel lHau_ = new BuLabel("Htr Chute d'eau");
  private final LongueurField tHau_ = new LongueurField(false, true, true);

  private final BuLabel lDureeDeBassinee_ = new BuLabel("Dur�es des Bassin�es");
  private final BuLabel lBassM_ = new BuLabel("Bassin�e Montante");
  private final DureeField dBassMMinute_ = new DureeField(false, false, false, true, false);
  private final DureeField dBassMSeconde_ = new DureeField(false, false, false, false, true);

  private final BuLabel lBassD_ = new BuLabel("Bassin�e Avalante");
  private final DureeField dBassDMinute_ = new DureeField(false, false, false, true, false);
  private final DureeField dBassDSeconde_ = new DureeField(false, false, false, false, true);

  /** **utile pour les manoeuvres** */
  private final BuLabel lDureeManoeuvres_ = new BuLabel("Dur�es par D�faut des manoeuvres");
  private final BuLabel lDureeEntrant_ = new BuLabel("Dur�e en Entrant");
  private final BuLabel lDureeSortant_ = new BuLabel("Dur�e en Sortant");
  private final DureeField dDureeEntrantMinute_ = new DureeField(false, false, false, true, false);
  private final DureeField dDureeEntrantSeconde_ = new DureeField(false, false, false, false, true);

  private final DureeField dDureeSortantMinute_ = new DureeField(false, false, false, true, false);
  private final DureeField dDureeSortantSeconde_ = new DureeField(false, false, false, false, true);

  public ArrayList listeEcluses_;

  /**
   * num�ro de l'�cluse � modifier -1 si ajout
   */
  private int nEcluse_ = -1;// -1 a lorigine

  /**
   * panel contenant les boutons, il est plac� en bas
   */

  private final BuPanel pBoutons_ = new BuPanel();
  /**
   * panel contenant les diff�rents champs de la fen�tre
   */
  private final BuPanel pTitre_ = new BuPanel();

  private final BuPanel pDonnees2_ = new BuPanel();

  /**
   * liste des validators
   */
  DureeField[] dureesAValider_;
  LongueurField[] longueursAValider_;
  /**
   * une instance de SinaviImplementation
   */
  public Sinavi2Implementation imp_ = null;

  /**
   * @param appli_ : Instance de Sinavi2implementation
   * @param liste_ : liste des �cluses
   * @param _modif : indique si on est en modification ou non
   */
  public Sinavi2FilleAddModEcluses(final BuCommonImplementation appli_, final ArrayList liste_, final boolean _modif) {

    super("Ajouter/Modifier une Ecluse", true, true, true, false);
    listeEcluses_ = liste_;
    modif_ = _modif;
    imp_ = (Sinavi2Implementation) appli_.getImplementation();

    bSauver_.addActionListener(this);
    bAnnuler_.addActionListener(this);
    bAfficher_.addActionListener(this);

    tId_.setToolTipText("Entrez l'identifiant de l'�cluse");
    tId_.addFocusListener(this);

    ((JComponent) getContentPane()).setBorder(new EmptyBorder(25, 25, 25, 25));

    pTitre_.add(lTitre_, "center");

    final GridBagLayout g2 = new GridBagLayout();
    pDonnees2_.setLayout(g2);
    final GridBagConstraints c = new GridBagConstraints();
    c.fill = GridBagConstraints.BOTH;

    c.gridx = 1;
    c.gridy = 2;
    pDonnees2_.add(lId_, c);

    c.gridx = 2;
    c.gridy = 2;
    pDonnees2_.add(tId_, c);

    tId_.addFocusListener(this);
    c.gridx = 1;
    c.gridy = 3;
    pDonnees2_.add(lLong_, c);

    c.gridx = 2;
    c.gridy = 3;
    pDonnees2_.add(tLong_, c);
    tLong_.addFocusListener(this);
    c.gridx = 1;
    c.gridy = 4;
    pDonnees2_.add(lLarg_, c);

    c.gridx = 2;
    c.gridy = 4;
    pDonnees2_.add(tLarg_, c);
    tLarg_.addFocusListener(this);
    c.gridx = 1;
    c.gridy = 5;
    pDonnees2_.add(lPro_, c);

    c.gridx = 2;
    c.gridy = 5;
    pDonnees2_.add(tPro_, c);
    tPro_.addFocusListener(this);

    c.gridx = 1;
    c.gridy = 6;
    pDonnees2_.add(lHau_, c);

    lHau_.setToolTipText("Cette donn�e n'a pas de rapport avec la simulation. \n "
        + "Elle sera utile pour d�terminer la consommation d'eau.");
    c.gridx = 2;
    c.gridy = 6;
    pDonnees2_.add(tHau_, c);

    tHau_.setToolTipText("Cette donn�e n'a pas de rapport avec la simulation \n "
        + "Elle sera utile pour d�terminer la consommation d'eau.");

    c.gridx = 0;
    c.gridy = 7;
    pDonnees2_.add(lBlanc_, c);

    /*
     * c.gridx =1; c.gridy =5; pDonnees2_.add(lHoraireDeNavigation_,c);
     */
    c.gridx = 0;
    c.gridy = 8;
    pDonnees2_.add(lDureeDeBassinee_, c);
    c.gridx = 1;
    c.gridy = 9;
    pDonnees2_.add(lBassM_, c);

    c.gridx = 2;
    c.gridy = 9;
    pDonnees2_.add(dBassMMinute_, c);
    dBassMMinute_.addFocusListener(this);
    c.gridx = 3;
    c.gridy = 9;
    pDonnees2_.add(dBassMSeconde_, c);
    dBassMSeconde_.addFocusListener(this);

    c.gridx = 1;
    c.gridy = 10;
    pDonnees2_.add(lBassD_, c);

    c.gridx = 2;
    c.gridy = 10;
    pDonnees2_.add(dBassDMinute_, c);
    dBassDMinute_.addFocusListener(this);
    c.gridx = 3;
    c.gridy = 10;
    pDonnees2_.add(dBassDSeconde_, c);
    dBassDSeconde_.addFocusListener(this);

    c.gridx = 0;
    c.gridy = 11;
    pDonnees2_.add(lBlanc2_, c);

    c.gridx = 0;
    c.gridy = 12;
    pDonnees2_.add(lDureeManoeuvres_, c);

    c.gridx = 1;
    c.gridy = 13;
    pDonnees2_.add(lDureeEntrant_, c);
    c.gridx = 2;
    c.gridy = 13;
    pDonnees2_.add(dDureeEntrantMinute_, c);
    dDureeEntrantMinute_.addFocusListener(this);

    c.gridx = 3;
    c.gridy = 13;
    pDonnees2_.add(dDureeEntrantSeconde_, c);
    dDureeEntrantSeconde_.addFocusListener(this);

    c.gridx = 1;
    c.gridy = 14;
    pDonnees2_.add(lDureeSortant_, c);

    c.gridx = 2;
    c.gridy = 14;
    pDonnees2_.add(dDureeSortantMinute_, c);
    dDureeSortantMinute_.addFocusListener(this);
    c.gridx = 3;
    c.gridy = 14;
    pDonnees2_.add(dDureeSortantSeconde_, c);
    dDureeSortantSeconde_.addFocusListener(this);

    pBoutons_.setLayout(new FlowLayout(FlowLayout.CENTER));
    pBoutons_.add(bAnnuler_);
    pBoutons_.add(bSauver_);
    pBoutons_.add(bAfficher_);

    getContentPane().add(pTitre_, BorderLayout.NORTH);
    getContentPane().add(pDonnees2_, BorderLayout.CENTER);
    getContentPane().add(pBoutons_, BorderLayout.SOUTH);

    initialiseChamps(nEcluse_);

    pack();

    /** *reinitialisation des valeurs* */

    setVisible(true);
    imp_.addInternalFrame(this);
    final List dureeList = new ArrayList();
    final List longueurList = new ArrayList();
    tLong_.setLongueurValidator(LongueurFieldValidator.creeMin(1));
    longueurList.add(tLong_);
    tLarg_.setLongueurValidator(LongueurFieldValidator.creeMin(1));
    longueurList.add(tLarg_);
    tPro_.setLongueurValidator(LongueurFieldValidator.creeMin(1));
    longueurList.add(tPro_);
    tHau_.setLongueurValidator(LongueurFieldValidator.creeMin(1));
    longueurList.add(tHau_);
    dBassMMinute_.setDureeValidator(DureeFieldValidator.creeMax(7200, "Entrez une heure de bassin�e correcte"));
    dureeList.add(dBassDMinute_);
    dBassMSeconde_.setDureeValidator(DureeFieldValidator.creeMax(60, "Entrez une heure de bassin�e correcte"));
    dureeList.add(dBassDSeconde_);
    dBassDMinute_.setDureeValidator(DureeFieldValidator.creeMax(7200, "Entrez une heure de bassin�e correcte"));
    dureeList.add(dBassDMinute_);
    dBassDSeconde_.setDureeValidator(DureeFieldValidator.creeMax(60, "Entrez une heure de bassin�e correcte"));
    dureeList.add(dBassDSeconde_);
    dDureeEntrantMinute_.setDureeValidator(DureeFieldValidator.creeMax(7200, "Entrez une heure de bassin�e correcte"));
    dureeList.add(dDureeEntrantMinute_);
    dDureeEntrantSeconde_.setDureeValidator(DureeFieldValidator.creeMax(60, "Entrez une heure de bassin�e correcte"));
    dureeList.add(dDureeEntrantSeconde_);
    dDureeSortantMinute_.setDureeValidator(DureeFieldValidator.creeMax(7200, "Entrez une heure de bassin�e correcte"));
    dureeList.add(dDureeSortantMinute_);
    dDureeSortantSeconde_.setDureeValidator(DureeFieldValidator.creeMax(60, "Entrez une heure de bassin�e correcte"));
    dureeList.add(dDureeSortantSeconde_);
    dureesAValider_ = (DureeField[]) dureeList.toArray(new DureeField[dureeList.size()]);
    longueursAValider_ = (LongueurField[]) longueurList.toArray(new LongueurField[longueurList.size()]);
  }

  /**
   * @param _e
   */
  public void actionPerformed(final ActionEvent _e) {
    if (_e.getSource() == bAnnuler_) {
      annuler();

    } else if (_e.getSource() == bSauver_) {
      if (controlerEntrees()) {
        if (modif_) {
          annuler();
        } else {
          initialiseChamps(-1);
        }
      }
      tId_.requestFocus();

    } else if (_e.getSource() == bAfficher_) {
      afficherEcluses();
    }
  }

  private void annuler() {
    imp_.removeInternalFrame(this);
    // imp_.sinavi2filleaddmodecluses_ =null;
    imp_.resetFille2ModEcluses();

  }

  /**
   * v�rifie les param�tres et ajoute ou modifie l'�cluse
   * 
   * @return vrai si les valeurs sont correctes
   */
  private boolean controlerEntrees() {
    // boolean valide=false;
    if (tId_.getText().equals("")) {
      imp_.affMessage("Entrez l'identifiant de l'�cluse");
      return false;
    } else {
      for (int i = 0; i < dureesAValider_.length; i++) {
        if (!dureesAValider_[i].isValueValid()) {
          imp_.affMessage(dureesAValider_[i].val_.getDescription());
          return false;
        }
      }
      for (int i = 0; i < longueursAValider_.length; i++) {
        if (!longueursAValider_[i].isValueValid()) {
          imp_.affMessage(longueursAValider_[i].val_.getDescription());
          return false;
        }
      }
    }

    final Sinavi2TypeEcluse b = new Sinavi2TypeEcluse(tId_.getText(), tLong_.getLongueurField(), tLarg_
        .getLongueurField(), tPro_.getLongueurField(), dBassMMinute_.getDureeField(), dBassMSeconde_.getDureeField(),
        dBassDMinute_.getDureeField(), dBassDSeconde_.getDureeField(), dDureeEntrantMinute_.getDureeField(),
        dDureeEntrantSeconde_.getDureeField(), dDureeSortantMinute_.getDureeField(), dDureeSortantSeconde_
            .getDureeField());
    SParametresEcluse ecluse = new SParametresEcluse();
    ecluse = b.ecluse_;
    ecluse.hauteurChuteDEau = tHau_.getLongueurField();
    addEcluse(b.getIdentification(), ecluse);
    return true;

  }

  /**
   * appelle la fonction d'ajout et valide la cr�ation par un message
   * 
   * @param _nom : nom de l'�cluse
   * @param _ecluse : �cluse � ajouter ou modifier
   */
  private void addEcluse(final String _nom, final SParametresEcluse _ecluse) {
    addListeEcluse(_ecluse);
    imp_.affMessage("Cr�ation de l'ecluse de nom " + _nom + " r�ussie");
  }

  public void addListeEcluse(final SParametresEcluse _b) {
    /** verifier l'existance de l'�cluse par une recherche* */

    final int x = rechercherEcluse(_b.identification);
    if (x == -1) {
      // intialisation des indisponibilit�s
      imp_.definirIndisponibilites(null, _b);
      listeEcluses_.add(_b);
    } else {
      _b.gareEnAmont = ((SParametresEcluse) listeEcluses_.get(x)).gareEnAmont;
      _b.gareEnAval = ((SParametresEcluse) listeEcluses_.get(x)).gareEnAval;
      imp_.definirIndisponibilites(null, _b);
      listeEcluses_.remove(x);
      listeEcluses_.add(x, _b);
    }
  }

  /**
   * recherch d'une �cluse
   * 
   * @param _ecluseCourant : nom de l'�cluse � rechercher
   * @return le num�ro de l'�cluse si existante sinon -1
   */
  private int rechercherEcluse(final String _ecluseCourant) {
    final ListIterator iter = listeEcluses_.listIterator();
    int i = 0;
    boolean trouve = false;
    while (iter.hasNext() && !trouve) {
      SParametresEcluse c = new SParametresEcluse();
      c = (SParametresEcluse) iter.next();
      if (c.identification.equalsIgnoreCase(_ecluseCourant)) {
        trouve = true;
        return i;
      } else {
        i++;
      }
    }
    return -1;
  }

  private void afficherEcluses() {
    if (listeEcluses_ != null) {
      imp_.afficherEcluses();
    }
  }

  public void setnEcluse(final int _ecluse) {

    nEcluse_ = _ecluse;
  }

  public void internalFrameActivated(final InternalFrameEvent _e) {
  // TODO Auto-generated method stub

  }

  public void internalFrameClosed(final InternalFrameEvent _e) {

    annuler();

  }

  public void internalFrameClosing(final InternalFrameEvent _e) {
  // TODO Auto-generated method stub

  }

  public void internalFrameDeactivated(final InternalFrameEvent _e) {
  // TODO Auto-generated method stub

  }

  public void internalFrameDeiconified(final InternalFrameEvent _e) {
  // TODO Auto-generated method stub

  }

  public void internalFrameIconified(final InternalFrameEvent _e) {
  // TODO Auto-generated method stub

  }

  public void internalFrameOpened(final InternalFrameEvent _e) {
  // TODO Auto-generated method stub

  }

  /**
   * @param _ecluse num�ro de l'�cluse pour mettre � jour les champs lui correspondant -1 pour r�initiliaser
   */
  public void initialiseChamps(final int _ecluse) {
    if (_ecluse == -1) {
      tId_.setText("");
      tLong_.setLongueurField(0);
      tLarg_.setLongueurField(0);
      tPro_.setLongueurField(0);
      dBassMMinute_.setDureeField(0);
      dBassMSeconde_.setDureeField(0);
      dBassDMinute_.setDureeField(0);
      dBassDSeconde_.setDureeField(0);
      dDureeEntrantMinute_.setDureeField(0);
      dDureeEntrantSeconde_.setDureeField(0);
      dDureeSortantMinute_.setDureeField(0);
      dDureeSortantSeconde_.setDureeField(0);
      tHau_.setLongueurField(0);
    } else {

      final SParametresEcluse ecluseCourant_ = (SParametresEcluse) listeEcluses_.get(nEcluse_);
      tId_.setText(ecluseCourant_.identification);
      tLong_.setLongueurField(ecluseCourant_.longueur);
      tLarg_.setLongueurField(ecluseCourant_.largeur);
      tPro_.setLongueurField(ecluseCourant_.profondeur);
      tHau_.setLongueurField(ecluseCourant_.hauteurChuteDEau);
      dBassMMinute_.setDureeField(Sinavi2TypeEcluse.determineMinuteSeule(ecluseCourant_.dureeBassineeMontante));
      dBassMSeconde_.setDureeField(Sinavi2TypeEcluse.determineSecondeSeule(ecluseCourant_.dureeBassineeMontante));
      dBassDMinute_.setDureeField(Sinavi2TypeEcluse.determineMinuteSeule(ecluseCourant_.dureeBassineeDescendante));
      dBassDSeconde_.setDureeField(Sinavi2TypeEcluse.determineSecondeSeule(ecluseCourant_.dureeBassineeDescendante));
      dDureeEntrantMinute_
          .setDureeField(Sinavi2TypeEcluse.determineMinuteSeule(ecluseCourant_.dureeManoeuvresEnEntree));
      dDureeEntrantSeconde_.setDureeField(Sinavi2TypeEcluse
          .determineSecondeSeule(ecluseCourant_.dureeManoeuvresEnEntree));
      dDureeSortantMinute_
          .setDureeField(Sinavi2TypeEcluse.determineMinuteSeule(ecluseCourant_.dureeManoeuvresEnSortie));
      dDureeSortantSeconde_.setDureeField(Sinavi2TypeEcluse
          .determineSecondeSeule(ecluseCourant_.dureeManoeuvresEnSortie));
    }

  }

  public void focusGained(final FocusEvent _e) {
  // TODO Auto-generated method stub

  }

  public void focusLost(final FocusEvent _e) {
    if (_e.getSource() == tId_) {
      if (tId_.getText().equalsIgnoreCase("")) {
        lId_.setForeground(Color.RED);

      } else {
        lId_.setForeground(CtuluLibSwing.getDefaultLabelForegroundColor());
      }

    }
  }
}
