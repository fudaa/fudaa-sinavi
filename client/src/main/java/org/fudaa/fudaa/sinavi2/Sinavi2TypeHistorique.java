package org.fudaa.fudaa.sinavi2;

import org.fudaa.dodico.corba.sinavi2.SResultatHistorique;

/**
 * @author maneuvrier Cette classe est utilis�e pour encapsuler la donn�es lues dans le fichier .his en les transformant
 *         pour obtenir des valeurs ais�ment utilisables cf. Sinavi2TypeGeneration pour le descriptif des m�thodes
 */
public class Sinavi2TypeHistorique {

  public static char avalantMontant(final boolean _avalantMontant) {
    if (_avalantMontant) {
      return 'A';
    }
    return 'M';

  }

  double attente_; // attente pour traversr l'el�ment

  char avalantMontant_; // sens dans le lequel le bateau effectue le trajet, true pour avalant, false pour montant

  String elementAFranchir_; // type de l'element � franchir G -> Gare, O -> Ecluse (anciennement ouvrage), B --> Bief

  double heureEntree_; // heure d'entr�e dans cette el�ment en secondes

  double heureSortie_; // heure de sortie de cette el�ment en secondes

  int numeroBateau_; // num�ro du bateau effectuant le trajet

  int numeroElement_;// num�ro de l'�lement dans la liste

  int numeroTypeBateau_;// num�ro du type de bateau

  public Sinavi2TypeHistorique() {}

  /**
   * @param numero bateau dans la simulation
   * @param numero du type de bateau
   * @param sens de navigation
   * @param �l�m�nt � franchir
   * @param num�ro de l'�l�ment � franchir
   * @param heure d'entr�e dans l'�l�m�nt
   * @param heure de sortie dans l'�l�ment
   * @param attente en seconde
   */

  public Sinavi2TypeHistorique(final SResultatHistorique _hist) {
    this.numeroBateau_ = _hist.numeroBateau;
    this.numeroTypeBateau_ = _hist.numeroTypeBateau;
    this.avalantMontant_ = avalantMontant(_hist.avalantMontant);
    this.elementAFranchir_ = _hist.elementAFranchir;
    this.numeroElement_ = _hist.numeroElement;
    this.heureEntree_ = _hist.heureEntree;
    this.heureSortie_ = _hist.heureSortie;
    this.attente_ = _hist.attente;
  }

  public double getAttente() {
    return attente_;
  }

  public char getAvalantMontant() {
    return avalantMontant_;
  }

  public String getElementAFranchir() {
    return elementAFranchir_;
  }

  public double getHeureEntree() {
    return heureEntree_;
  }

  public double getHeureSortie() {
    return heureSortie_;
  }

  public int getNumeroBateau() {
    return numeroBateau_;
  }

  public int getNumeroElement() {
    return numeroElement_;
  }

  public int getNumeroTypeBateau() {
    return numeroTypeBateau_;
  }

  public void setAttente(final double _attente) {
    attente_ = _attente;
  }

  public void setAvalantMontant(final char _avalantMontant) {
    avalantMontant_ = _avalantMontant;
  }

  public void setElementAFranchir(final String _elementAFranchir) {
    elementAFranchir_ = _elementAFranchir;
  }

  public void setHeureEntree(final double _heureEntree) {
    heureEntree_ = _heureEntree;
  }

  public void setHeureSortie(final double _heureSortie) {
    heureSortie_ = _heureSortie;
  }

  public void setNumeroBateau(final int _numeroBateau) {
    numeroBateau_ = _numeroBateau;
  }

  public void setNumeroElement(final int _numeroElement) {
    numeroElement_ = _numeroElement;
  }

  public void setNumeroTypeBateau(final int _numeroTypeBateau) {
    numeroBateau_ = _numeroTypeBateau;
  }

}
