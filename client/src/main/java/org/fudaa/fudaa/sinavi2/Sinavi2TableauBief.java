package org.fudaa.fudaa.sinavi2;

import java.util.List;

import javax.swing.table.AbstractTableModel;

import jxl.write.Label;
import jxl.write.Number;
import jxl.write.WritableCell;

/**
 * @author maneuvrier Cette classe a pour but d'afficher un tableau de biefs et d'offrir la possibilit� de l'impression
 *         c'est � dire la cr�ation d'un tableau excel d'o� l'impl�mentation de CtuluTableModelInterface. cf.
 *         Sinavi2TableauBateau
 */

public class Sinavi2TableauBief extends AbstractTableModel implements org.fudaa.ctulu.table.CtuluTableModelInterface {

  private int nbRow_ = 0;
  private final String[] nomColonnes_ = { "Identification", "longueur", "largeur", "Hauteur d'eau", "Vitesse" };
  public Object[][] data_ = null;// =new Object[0][0];

  public String[] nomCol() {
    return nomColonnes_;
  }

  public void setNbRow(final int _n) {
    nbRow_ = _n;

  }

  public void initNomCol(final int _row) {
    for (int i = 0; i < getColumnCount(); i++) {
      data_[_row][i] = nomColonnes_[i];
    }
  }

      
    public int[] getSelectedRows() {
       return null;
    }
  public int getColumnCount() {

    return nomColonnes_.length;
  }

  public int getRowCount() {
    return nbRow_;

  }

  public Object getValueAt(final int _rowIndex, final int _columnIndex) {

    return data_[_rowIndex][_columnIndex];
  }

  public void setValueAt(final Object _value, final int _row, final int _col) {
    // String x=new String(value);
    // x=value;
    final Object[][] temp = new Object[_row + 1][getColumnCount()];
    nbRow_ = _row;
    if (data_ != null) {
      for (int i = 0; i <= _row; i++) {
        if (i == _row) {
          for (int j = 0; j < _col; j++) {
            // temp[i][j]=new Object();
            temp[i][j] = data_[i][j];
          }
        } else {
          for (int j = 0; j < getColumnCount(); j++) {
            // temp[i][j]=new Object();
            temp[i][j] = data_[i][j];
          }
        }
      }
    }
    temp[_row][_col] = _value;
    data_ = new Object[_row + 1][getColumnCount()];
    for (int i = 0; i <= _row; i++) {
      if (i == _row) {
        for (int j = 0; j <= _col; j++) {
          // temp[i][j]=new Object();
          data_[i][j] = temp[i][j];
        }
      } else {
        for (int j = 0; j < getColumnCount(); j++) {
          // temp[i][j]=new Object();
          data_[i][j] = temp[i][j];
        }
      }
    }

  }

  public int getMaxCol() {

    return nomCol().length;
  }

  public int getMaxRow() {

    return nbRow_;
  }

  public Object getValue(final int _row, final int _col) {
    // TODO Auto-generated method stub
    return getValueAt(_row, _col);
  }

  public WritableCell getExcelWritable(final int _row, final int _col, int _rowXls, int _colXls) {
    final int r = _row;
    final int c = _col;

    final Object o = data_[r][c];
    if (o == null) {
      return null;
    }
    String s = o.toString();
    if (s == null) {
      return null;
    }
    s = s.trim();
    if (s.length() == 0) {
      return null;
    }
    try {
      return new Number(_colXls, _rowXls, Double.parseDouble(s));
    } catch (final NumberFormatException e) {}
    return new Label(_colXls, _rowXls, s);

  }

  public String getColumnName(final int _i) {
    return nomColonnes_[_i];

  }

  public void setColumnName(final String _name, final int _i) {
    nomColonnes_[_i] = _name;

  }

public List<String> getComments() {
	// TODO Auto-generated method stub
	return null;
}

}
