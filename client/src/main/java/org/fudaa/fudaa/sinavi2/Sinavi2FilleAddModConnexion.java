package org.fudaa.fudaa.sinavi2;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.util.ArrayList;
import java.util.ListIterator;

import javax.swing.JComponent;
import javax.swing.border.EmptyBorder;
import javax.swing.event.InternalFrameEvent;
import javax.swing.event.InternalFrameListener;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuComboBox;
import com.memoire.bu.BuCommonImplementation;
import com.memoire.bu.BuInternalFrame;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuTextField;

import org.fudaa.dodico.corba.sinavi2.SParametresBief;
import org.fudaa.dodico.corba.sinavi2.SParametresEcluse;

import org.fudaa.dodico.sinavi2.DParametresSinavi2;

/**
 * @version $Revision: 1.13 $ $Date: 2007-05-04 14:01:05 $ by $Author: deniger $
 * @author Fatimatou Ka et Beno�t Maneuvrier Le but de cette classe est d'ajouter ou modifier les connexions en ajoutant
 *         des gares entre les biefs et les �cluses. cf. Sinavi2FilleAddModBateaux
 */

public class Sinavi2FilleAddModConnexion extends BuInternalFrame implements ActionListener, InternalFrameListener,
    FocusListener {

  private final BuLabel lTitre_ = new BuLabel("Param�tres de connexion du r�seau ");

  /** nom du bief ou de l'�cluse: nom de la voie */
  public BuComboBox tfNomvoie_ = new BuComboBox();
  private final BuLabel lVoie_ = new BuLabel("Nom de l'�l�ment");

  /** num�ro de la gare en amont */
  private final BuTextField tfNumeroGareAmont_ = new BuTextField();
  private final BuLabel lAmont_ = new BuLabel("Numero de gare en amont");

  /** num�ro de gare en aval */
  private final BuTextField tfNumeroGareAval_ = new BuTextField();
  private final BuLabel lAval_ = new BuLabel("Numero de gare en aval");

  private final BuButton bAnnuler_ = new BuButton(Sinavi2Resource.SINAVI2.getIcon("QUITTER"), "Quitter");

  /** bouton pour sauvegarder les connexions entr�es */
  private final BuButton bSauver_ = new BuButton(Sinavi2Resource.SINAVI2.getIcon("VALIDER"), "Sauver");

  /** bouton pour afficher la liste des gares cr��es */
  private final BuButton bAfficher_ = new BuButton(Sinavi2Resource.SINAVI2.getIcon("AFFICHER"), "Afficher");

  /** * panel contenant les boutons, il est plac� en bas */
  private final BuPanel pBoutons_ = new BuPanel();

  /**
   * panel contenant les diff�rents champs de la fen�tre
   */
  private final BuPanel pTitre_ = new BuPanel();
  private final BuPanel pDonnees2_ = new BuPanel();

  /** liste contenant les gares qui ont �t� valid�es* */
  public ArrayList listeGares_;
  public ArrayList listeBiefs_;
  public ArrayList listeEcluses_;
  /** instance de BuCommonImplementation */

  public Sinavi2Implementation imp_ = null;
  protected String elemPrec_ = "";
  protected String precElemPrec_ = "";

  /**
   * @param _appli instance de sinavi2Implementation
   * @param _listeGares liste des gares courantes
   * @param _listeBiefs liste des biefs
   * @param _listeEcluses liste des �cluses
   */
  public Sinavi2FilleAddModConnexion(final BuCommonImplementation _appli, final ArrayList _listeGares,
      final ArrayList _listeBiefs, final ArrayList _listeEcluses) {
    super("Param�tres de connexion du r�seau", true, true, true, false);
    listeGares_ = _listeGares;
    listeBiefs_ = _listeBiefs;
    listeEcluses_ = _listeEcluses;
    imp_ = (Sinavi2Implementation) _appli.getImplementation();

    /** Ajout des listeneurs aux boutons* */
    bAnnuler_.addActionListener(this);
    bSauver_.addActionListener(this);
    bAfficher_.addActionListener(this);
    tfNomvoie_.addActionListener(this);

    /** bulles d'aide � l'utilisateur */
    tfNumeroGareAmont_.setToolTipText("Entrez le num�ro de gare en amont");
    tfNumeroGareAval_.setToolTipText("Entrez le num�ro de gare en aval");
    tfNomvoie_.setToolTipText("Choisissez votre type de voie");

    /** Contraintes sur les zones de texte */
    tfNumeroGareAval_.setText("");
    tfNumeroGareAmont_.setText("");

    /** ajout de focus sur les zones de texte* */
    tfNumeroGareAmont_.addFocusListener(this);
    tfNumeroGareAval_.addFocusListener(this);

    /**
     * remplissage des listes on fait pr�c�der de ***bie: pour les biefs ***ecl: pour les �cluses pour �viter les
     * confusions
     */
    if (listeBiefs_ != null) {
      final ListIterator itBief = listeBiefs_.listIterator();
      while (itBief.hasNext()) {
        SParametresBief b = new SParametresBief();
        b = (SParametresBief) itBief.next();
        tfNomvoie_.addItem("bie:" + b.identification);
      }
    }

    if (listeEcluses_ != null) {
      final ListIterator itEcluse = listeEcluses_.listIterator();
      while (itEcluse.hasNext()) {
        SParametresEcluse b = new SParametresEcluse();
        b = (SParametresEcluse) itEcluse.next();
        tfNomvoie_.addItem("ecl:" + b.identification);
      }
    }

    ((JComponent) getContentPane()).setBorder(new EmptyBorder(25, 25, 25, 25));

    pTitre_.add(lTitre_, "CENTER");
    final GridBagLayout g2 = new GridBagLayout();
    pDonnees2_.setLayout(g2);
    final GridBagConstraints c = new GridBagConstraints();
    c.fill = GridBagConstraints.BOTH;

    c.gridx = 0;
    c.gridy = 3;
    // g2.setConstraints(labelvoie,c);
    pDonnees2_.add(lVoie_, c);

    c.gridx = 2;
    c.gridy = 3;
    pDonnees2_.add(tfNomvoie_, c);

    c.gridx = 0;
    c.gridy = 4;
    pDonnees2_.add(lAmont_, c);

    c.gridx = 2;
    c.gridy = 4;
    pDonnees2_.add(tfNumeroGareAmont_, c);

    c.gridx = 0;
    c.gridy = 5;
    pDonnees2_.add(lAval_, c);

    c.gridx = 2;
    c.gridy = 5;
    pDonnees2_.add(tfNumeroGareAval_, c);

    pBoutons_.setLayout(new FlowLayout(FlowLayout.CENTER));
    pBoutons_.add(bAnnuler_);
    pBoutons_.add(bSauver_);
    pBoutons_.add(bAfficher_);

    getContentPane().add(pTitre_, BorderLayout.NORTH);
    getContentPane().add(pDonnees2_, BorderLayout.CENTER);
    getContentPane().add(pBoutons_, BorderLayout.SOUTH);
    // setTitle("Param�tres des connexions du r�seau");
    // setLocation(20, 20);
    pack();
    setVisible(true);
    // addInternalFrameListener(this);
    imp_.addInternalFrame(this);

  }

  private boolean controlerEntrees() {

    if (tfNumeroGareAmont_.getText().equals("")) {
      imp_.affMessage("Entrez le num�ro de gare en amont");
      return false;
    } else if (tfNumeroGareAval_.getText().equals("")) {
      // BuDialogMessage dialog_mess = new BuDialogMessage(imp_.getApp(),imp_.getInformationsSoftware(), "Entrez
      // l'identifiant du type de bateau");
      imp_.affMessage("Entrez le num�ro de gare en aval");
      return false;
    }

    else if (Integer.parseInt(tfNumeroGareAmont_.getText()) < 1) {
      imp_.affMessage("Le num�ro de la gare en amont doit �tre compris entre 1 et " + DParametresSinavi2.nbMaxDeGares);
      return false;
    } else if (Integer.parseInt(tfNumeroGareAmont_.getText()) > DParametresSinavi2.nbMaxDeGares) {
      imp_.affMessage("Le nombre de gares est limit� � " + DParametresSinavi2.nbMaxDeGares);
      return false;
    }

    else if (Integer.parseInt(tfNumeroGareAval_.getText()) < 1) {

      imp_.affMessage("Le num�ro de la gare doit �tre compris entre 1 et " + DParametresSinavi2.nbMaxDeGares);
      return false;
    }

    else if (Integer.parseInt(tfNumeroGareAval_.getText()) > DParametresSinavi2.nbMaxDeGares) {
      imp_.affMessage("Le nombre de gares est limit� � " + DParametresSinavi2.nbMaxDeGares);
      return false;
    }

    else if (Integer.parseInt(tfNumeroGareAmont_.getText()) == Integer.parseInt(tfNumeroGareAval_.getText())) {
      imp_.affMessage("Les num�ros de gare en amont et en aval doivent �tre diff�rents");
      return false;

    } else {
      return true;
    }
  }

  /**
   * @param _nomElement : nom de l'�l�ment � enregistrer
   * @param _prec : correspond � bie: ou ecl: on enregistre les gares en amont et en aval du bief ou de l'�cluse
   *          correspondant
   */
  public void sauvegarder(final String _nomElement, final String _prec) {
    if (controlerEntrees()) {
      // String NomElement=tfNomvoie_.getSelectedItem().toString();
      boolean trouve = false;
      if (listeBiefs_ != null) {
        final ListIterator itBief = listeBiefs_.listIterator();
        while (itBief.hasNext() && !trouve) {
          SParametresBief b = new SParametresBief();
          b = (SParametresBief) itBief.next();
          if (_nomElement.equalsIgnoreCase(b.identification) && _prec.equalsIgnoreCase("bie:")) {
            trouve = true;
            b.gareEnAmont = Integer.parseInt(tfNumeroGareAmont_.getValue().toString());
            b.gareEnAval = Integer.parseInt(tfNumeroGareAval_.getValue().toString());
            addListeGare(b.gareEnAmont + "");
            addListeGare(b.gareEnAval + "");
          }
        }
      }
      if (!trouve && listeEcluses_ != null) {
        final ListIterator itEcluse = listeEcluses_.listIterator();
        while (itEcluse.hasNext() && !trouve) {
          SParametresEcluse b = new SParametresEcluse();
          b = (SParametresEcluse) itEcluse.next();
          if (_nomElement.equalsIgnoreCase(b.identification) && _prec.equalsIgnoreCase("ecl:")) {
            trouve = true;
            b.gareEnAmont = Integer.parseInt(tfNumeroGareAmont_.getValue().toString());
            b.gareEnAval = Integer.parseInt(tfNumeroGareAval_.getValue().toString());
            addListeGare(b.gareEnAmont + "");
            addListeGare(b.gareEnAval + "");
          }
        }
      }
    }

  }

  public void actionPerformed(final ActionEvent _e) {

    if (_e.getSource() == tfNomvoie_) {
      /**
       * si on change l'�l�ment de la liste on sauvegarde les gares et on initialise les champs avec le nouvel el�ment
       * selectionn�
       */
      final String temp = tfNomvoie_.getSelectedItem().toString();
      final String s = temp.substring(4);
      final String prec = temp.substring(0, 4);
      if (elemPrec_.equalsIgnoreCase("")) {
        initialiseChamps("", prec);
      } else if (!s.equalsIgnoreCase(elemPrec_)) {
        sauvegarder(elemPrec_, precElemPrec_);
      }
      elemPrec_ = s;
      precElemPrec_ = prec;
      initialiseChamps(s, prec);

      // sauvegarder(s,prec);
      /*
       * sauvegarder(elemPrec_,prec); initialise_champs(s,prec);
       */
      // annuler();
    } else if (_e.getSource() == bAfficher_) {
      if (listeGares_ == null) {
        imp_.affMessage("Aucune gare n'a encore �t� affect�e");

      } else {
        imp_.afficherConnexions();
      }
    }

    else if (_e.getSource() == bSauver_) {
      /**
       * enregistre les gares pour l'el�ment en cours
       */
      if (controlerEntrees()) {
        final String temp = tfNomvoie_.getSelectedItem().toString();
        final String s = temp.substring(4);
        final String prec = temp.substring(0, 4);

        sauvegarder(s, prec);
        annuler();

      }

    }
    if (_e.getSource() == bAnnuler_) {
      annuler();
    }

  }

  private void annuler() {
    imp_.removeInternalFrame(this);
    imp_.resetFille2ModConnexion();

  }

  /** M�thode permettant d'ajouter une gare a la liste des gares. */
  private void addListeGare(final String _gare) {
    final ListIterator i = listeGares_.listIterator();
    boolean trouve = false;
    //String g = new String();
    while (i.hasNext() && !trouve) {
      //g = (String) i.next();
      if (((String) i.next()).equalsIgnoreCase(_gare)) {
        trouve = true;
      }
    }
    if (!trouve) {
      listeGares_.add(_gare);
    }

  }

  public void internalFrameActivated(final InternalFrameEvent _e) {

  }

  public void internalFrameClosed(final InternalFrameEvent _e) {
    this.closable = true;
    annuler();

  }

  public void internalFrameClosing(final InternalFrameEvent _e) {

  }

  public void internalFrameDeactivated(final InternalFrameEvent _e) {

  }

  public void internalFrameDeiconified(final InternalFrameEvent _e) {

  }

  public void internalFrameIconified(final InternalFrameEvent _e) {

  }

  public void internalFrameOpened(final InternalFrameEvent _e) {

  }

  public void initialiseChamps(final String _elem, final String _prec) {
    System.out.println(_elem);
    System.out.println("Entr�e dans initialise champs");
    if (_elem == "") {
      tfNumeroGareAval_.setText("");
      tfNumeroGareAmont_.setText("");
      tfNomvoie_.setSelectedIndex(0);
    }

    else {
      boolean trouve = false;
      tfNomvoie_.setSelectedItem(_prec + _elem);
      final ListIterator itBief = listeBiefs_.listIterator();
      while (itBief.hasNext() && !trouve) {
        SParametresBief b = new SParametresBief();
        b = (SParametresBief) itBief.next();
        if (_elem.equalsIgnoreCase(b.identification) && _prec.equalsIgnoreCase("bie:")) {
          trouve = true;
          tfNumeroGareAval_.setText("" + b.gareEnAval);
          tfNumeroGareAmont_.setText("" + b.gareEnAmont);
          System.out.println("ca passe bief");

        }
      }
      if (!trouve) {
        final ListIterator itEcluse = listeEcluses_.listIterator();
        while (itEcluse.hasNext() && !trouve) {
          SParametresEcluse b = new SParametresEcluse();
          b = (SParametresEcluse) itEcluse.next();
          if (_elem.equalsIgnoreCase(b.identification) && _prec.equalsIgnoreCase("ecl:")) {
            trouve = true;
            tfNumeroGareAval_.setText("" + b.gareEnAval);
            tfNumeroGareAmont_.setText("" + b.gareEnAmont);
            System.out.println("ca passe ecluse");
          }
        }
      }
    }
  }

  /**
   * Quand on entre dans la zone de texte c'est que l'on entre dans le composant.
   * 
   * @param e
   */
  public void focusGained(final FocusEvent _e) {
    tfNumeroGareAval_.selectAll();
    tfNumeroGareAmont_.selectAll();
    processFocusEvent(new FocusEvent(this, FocusEvent.FOCUS_GAINED));
  }

  /**
   * Quand on sort de la zone de texte c'est que l'on sort du composant.
   * 
   * @param e
   */
  public void focusLost(final FocusEvent _e) {
    processFocusEvent(new FocusEvent(this, FocusEvent.FOCUS_LOST));
  }

}
