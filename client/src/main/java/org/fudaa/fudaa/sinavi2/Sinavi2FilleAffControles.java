/*
 * @file         Sinavi2FilleAff�Bateaux.java
 * @creation     2001-05-17
 * @modification $Date: 2006-09-19 15:08:58 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.sinavi2;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.ListIterator;

import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JTextPane;
import javax.swing.border.EmptyBorder;
import javax.swing.event.InternalFrameEvent;
import javax.swing.event.InternalFrameListener;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultStyledDocument;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyleContext;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuCommonImplementation;
import com.memoire.bu.BuDialogMessage;
import com.memoire.bu.BuFileChooser;
import com.memoire.bu.BuFileFilter;
import com.memoire.bu.BuInternalFrame;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuScrollPane;

import org.fudaa.dodico.corba.sinavi2.SParametresBateau;
import org.fudaa.dodico.corba.sinavi2.SParametresBief;
import org.fudaa.dodico.corba.sinavi2.SParametresCroisements;
import org.fudaa.dodico.corba.sinavi2.SParametresEcluse;
import org.fudaa.dodico.corba.sinavi2.SParametresTrematages;

/**
 * impl�mentation d'une fen�tre interne permettant de controler les donn�es Cette classe a un but informatif, elle
 * indique les erreurs possibles sans qu'elles ne soient forc�ment irr�voczble.
 * 
 * @version $Revision: 1.9 $ $Date: 2006-09-19 15:08:58 $ by $Author: deniger $
 * @author Fatimatou Ka , Beno�t Maneuvrier
 */
public class Sinavi2FilleAffControles extends BuInternalFrame implements ActionListener, InternalFrameListener {

  private String text = "";
  private BuScrollPane scrollPane_;
  final double paramCroisement = 0;
  final double paramTrematage = 0;
  private final BuLabel lTitre_ = new BuLabel("Informations de contr�le de votre mod�le");

  /** bouton pour annuler */
  private final BuButton bAnnuler_ = new BuButton(Sinavi2Resource.SINAVI2.getIcon("QUITTER"), "Quitter");

  private final BuButton bImprimer_ = new BuButton(Sinavi2Resource.SINAVI2.getIcon("IMPRIMER"), "Imprimer");

  private final JTextPane tText = new JTextPane();

  private final BuPanel pBoutons_ = new BuPanel();

  private final BuPanel pTitre_ = new BuPanel();

  private final BuPanel pDonnees2_ = new BuPanel();

  public ArrayList listeBateaux_;
  public ArrayList listeBiefs_;
  public ArrayList listeEcluses_;
  public ArrayList listeCroisements_;
  public ArrayList listeTrematages_;
  public Sinavi2Implementation imp2_ = null;

  /**
   * a faire
   */
  public Sinavi2FilleAffControles(final BuCommonImplementation _appli, final ArrayList _listeBateaux,
      final ArrayList _listeBiefs, final ArrayList _listeEcluses, final ArrayList _listeCroisements,
      final ArrayList _listeTrematages) {
    super("Contr�le des informations", true, true, true, false);
    listeBateaux_ = _listeBateaux;
    listeBiefs_ = _listeBiefs;
    listeEcluses_ = _listeEcluses;
    listeCroisements_ = _listeCroisements;
    listeTrematages_ = _listeTrematages;
    pTitre_.add(lTitre_);
    remplissage(tText);
    scrollPane_ = new BuScrollPane(tText);
    imp2_ = (Sinavi2Implementation) _appli.getImplementation();

    bAnnuler_.addActionListener(this);

    bImprimer_.addActionListener(this);

    ((JComponent) getContentPane()).setBorder(new EmptyBorder(25, 25, 25, 25));
    // ((JComponent) getContentPane()).setMaximumSize( ((JComponent) getContentPane()).getMaximumSize());

    pBoutons_.setLayout(new FlowLayout(FlowLayout.CENTER));
    pBoutons_.add(bAnnuler_);
    pBoutons_.add(bImprimer_);
    getContentPane().add(pTitre_, BorderLayout.NORTH);
    getContentPane().add(scrollPane_, BorderLayout.CENTER);
    getContentPane().add(pBoutons_, BorderLayout.SOUTH);

    pack();

    setVisible(true);
    addInternalFrameListener(this);
    imp2_.addInternalFrame(this);

  }

  /**
   * remplissage des controles dans le TextPane
   */
  private void remplissage(final JTextPane _text) {
    // creation des styles : Titre soulign� et en rouge
    // modifie le style par d�fault
    final Style def = StyleContext.getDefaultStyleContext().getStyle(StyleContext.DEFAULT_STYLE);
    StyleConstants.setFontFamily(def, "TimesNewRoman");
    StyleConstants.setAlignment(def, StyleConstants.ALIGN_JUSTIFIED);
    StyleConstants.setFontSize(def, 12);
    // style normal
    final Style normal = _text.addStyle("normal", def);
    Style s = _text.addStyle("titre", normal);
    StyleConstants.setBold(s, true);
    StyleConstants.setUnderline(s, true);
    StyleConstants.setFontSize(def, 14);
    StyleConstants.setForeground(s, Color.red);
    s = _text.addStyle("txt", normal);
    StyleConstants.setItalic(s, true);
    StyleConstants.setForeground(s, Color.blue);
    double biefLargeur = 0;

    final DefaultStyledDocument doc = new DefaultStyledDocument();

    if (listeBateaux_ == null) {
      try {
        doc.insertString(doc.getLength(), " Il n'y a pas de bateaux dans la base" + "\n", _text.getStyle("txt"));
        text += " Il n'y a pas de bateaux dans la base" + "\n";
      } catch (final BadLocationException e) {
        e.printStackTrace();
      }

    }

    else {

      try {

        // recherche les bateaux qui ne peuvent pas passer dans un bief

        doc.insertString(doc.getLength(), "Hauteur de Bief et tirant d'eau" + "\n", _text.getStyle("titre"));
        text += "Hauteur de Bief et tirant d'eau" + "\n";
        ListIterator itBat = listeBateaux_.listIterator();
        boolean trouve = false;
        while (itBat.hasNext()) {
          SParametresBateau bat = new SParametresBateau();
          bat = (SParametresBateau) itBat.next();
          if (listeBiefs_ != null) {

            final ListIterator itBief = listeBiefs_.listIterator();
            while (itBief.hasNext()) {
              SParametresBief bief = new SParametresBief();
              bief = (SParametresBief) itBief.next();
              if (bief.hauteur < bat.tirantDeau) {
                trouve = true;
                doc.insertString(doc.getLength(), " * Le type de bateau " + bat.identification
                    + " ne passe pas dans le bief " + bief.identification + "\n", _text.getStyle("txt"));
                text += " * Le type de bateau " + bat.identification + " ne passe pas dans le bief "
                    + bief.identification + "\n";
              }

            }
          }

        }
        if (!trouve) {
          doc.insertString(doc.getLength(), " OK" + "\n", _text.getStyle("txt"));
          text += " OK" + "\n";

        }
        trouve = false;

        // recherche les bateaux qui ne peuvent pas passer dans une �cluse
        doc.insertString(doc.getLength(), "Profondeur d'Ecluse et tirant d'eau" + "\n", _text.getStyle("titre"));
        text += "Profondeur d'Ecluse et tirant d'eau" + "\n";
        itBat = listeBateaux_.listIterator();
        trouve = false;
        while (itBat.hasNext()) {
          SParametresBateau bat = new SParametresBateau();
          bat = (SParametresBateau) itBat.next();
          if (listeEcluses_ != null) {
            final ListIterator itEcluse = listeEcluses_.listIterator();
            while (itEcluse.hasNext()) {
              SParametresEcluse ecl = new SParametresEcluse();
              ecl = (SParametresEcluse) itEcluse.next();
              if (ecl.profondeur < bat.tirantDeau) {
                trouve = true;
                doc.insertString(doc.getLength(), " * Le type de bateau " + bat.identification
                    + " ne passe pas dans l'�cluse " + ecl.identification + "\n", _text.getStyle("txt"));
                text += " * Le type de bateau " + bat.identification + " ne passe pas dans l'�cluse "
                    + ecl.identification + "\n";
              }

            }
          }
        }
        if (!trouve) {
          doc.insertString(doc.getLength(), " OK" + "\n", _text.getStyle("txt"));
          text += " OK" + "\n";
        }

        // controle des croisements dans les �cluses
        doc.insertString(doc.getLength(), "R�gles de Croisements" + "\n", _text.getStyle("titre"));
        text += "R�gles de Croisements" + "\n";
        if (listeCroisements_ != null) {
          final ListIterator itCro = listeCroisements_.listIterator();
          itBat = listeBateaux_.listIterator();
          trouve = false;

          while (itCro.hasNext()) {
            double larg = 0;
            SParametresCroisements cro = new SParametresCroisements();
            cro = (SParametresCroisements) itCro.next();
            while (itBat.hasNext()) {
              SParametresBateau bat = new SParametresBateau();
              bat = (SParametresBateau) itBat.next();
              if (cro.type1.equalsIgnoreCase(bat.identification)) {
                larg += bat.largeur;
              }
            }
            final ListIterator itBief = listeBiefs_.listIterator();
            boolean tb = false;
            while (itBief.hasNext() && !tb) {
              SParametresBief bief = new SParametresBief();
              bief = (SParametresBief) itBief.next();
              if (bief.identification.equalsIgnoreCase(cro.bief)) {
                biefLargeur = bief.largeur;
                tb = true;
              }
              if (tb) {
                if (larg + paramCroisement > biefLargeur) {
                  trouve = true;
                  doc.insertString(doc.getLength(), " * Les types de bateau " + cro.type1 + " et " + cro.type2
                      + " ne peuvent se croiser dans le bief " + bief.identification + "\n", _text.getStyle("txt"));
                  text += " * Les types de bateau " + cro.type1 + " et " + cro.type2
                      + " ne peuvent se croiser dans le bief " + bief.identification + "\n";
                }
              }
            }
          }
        }
        if (!trouve) {
          doc.insertString(doc.getLength(), " OK" + "\n", _text.getStyle("txt"));
          text += " OK" + "\n";
        } else {
          doc.insertString(doc.getLength(),
              " Verifiez les largeurs des bateaux et des biefs ou changer les regles de croisements" + "\n", _text
                  .getStyle("txt"));
          text += " Verifiez les largeurs des bateaux et des biefs ou changer les regles de croisements" + "\n";
        }

        // controle des trematages dans les �cluses
        doc.insertString(doc.getLength(), "R�gles de Tr�matages" + "\n", _text.getStyle("titre"));
        text += "R�gles de Tr�matages" + "\n";
        if (listeTrematages_ != null) {
          final ListIterator itTre = listeTrematages_.listIterator();
          itBat = listeBateaux_.listIterator();
          trouve = false;
          biefLargeur = 0;
          while (itTre.hasNext()) {
            double larg = 0;
            SParametresTrematages tre = new SParametresTrematages();
            tre = (SParametresTrematages) itTre.next();
            while (itBat.hasNext()) {
              SParametresBateau bat = new SParametresBateau();
              bat = (SParametresBateau) itBat.next();
              if (tre.type1.equalsIgnoreCase(bat.identification)) {
                larg += bat.largeur;
              }
            }
            final ListIterator itBief = listeBiefs_.listIterator();
            boolean tb = false;
            while (itBief.hasNext() && !tb) {
              SParametresBief bief = new SParametresBief();
              bief = (SParametresBief) itBief.next();
              if (bief.identification.equalsIgnoreCase(tre.bief)) {
                biefLargeur = bief.largeur;
                tb = true;
              }
              if (tb) {
                if (larg + paramTrematage > biefLargeur) {
                  trouve = true;
                  doc.insertString(doc.getLength(), " * Les types de bateau " + tre.type1 + " et " + tre.type2
                      + " ne peuvent se doubler dans le bief " + bief.identification + "\n", _text.getStyle("txt"));
                  text += " * Les types de bateau " + tre.type1 + " et " + tre.type2
                      + " ne peuvent se doubler dans le bief " + bief.identification + "\n";
                }
              }
            }
          }
        }
        if (!trouve) {
          doc.insertString(doc.getLength(), " OK" + "\n", _text.getStyle("txt"));
          text += " OK" + "\n";
        } else {
          doc.insertString(doc.getLength(),
              " Verifiez les largeurs des bateaux et des biefs ou changer les r�gles de trematages" + "\n", _text
                  .getStyle("txt"));
          text += " Verifiez les largeurs des bateaux et des biefs ou changer les r�gles de trematages" + "\n";
        }

        // controle de la longueur des bateaux par rapport a la longueur des ecluses*/
        doc.insertString(doc.getLength(), "Longueur Bateau et longueur Ecluse" + "\n", _text.getStyle("titre"));
        text += "Longueur Bateau et longueur Ecluse" + "\n";
        itBat = listeBateaux_.listIterator();
        trouve = false;
        while (itBat.hasNext()) {
          SParametresBateau bat = new SParametresBateau();
          bat = (SParametresBateau) itBat.next();
          if (listeEcluses_ != null) {
            final ListIterator itEcluse = listeEcluses_.listIterator();
            while (itEcluse.hasNext()) {
              SParametresEcluse ecl = new SParametresEcluse();
              ecl = (SParametresEcluse) itEcluse.next();
              if (ecl.longueur < bat.longueur) {
                trouve = true;
                doc.insertString(doc.getLength(), " * Le type de bateau " + bat.identification
                    + " ne passe pas dans l'�cluse " + ecl.identification + "\n", _text.getStyle("txt"));
                text += " * Le type de bateau " + bat.identification + " ne passe pas dans l'�cluse "
                    + ecl.identification + "\n";
              }

            }
          }
        }
        if (!trouve) {
          doc.insertString(doc.getLength(), " OK" + "\n", _text.getStyle("txt"));
          text += " OK" + "\n";
        }

        // controle de la largeur des bateaux par rapport a la largeur de l'ecluse
        doc.insertString(doc.getLength(), "Largeur Bateau et largeur Ecluse" + "\n", _text.getStyle("titre"));
        text += "Largeur Bateau et largeur Ecluse" + "\n";
        itBat = listeBateaux_.listIterator();
        trouve = false;
        while (itBat.hasNext()) {
          SParametresBateau bat = new SParametresBateau();
          bat = (SParametresBateau) itBat.next();
          if (listeEcluses_ != null) {
            final ListIterator itEcluse = listeEcluses_.listIterator();
            while (itEcluse.hasNext()) {
              SParametresEcluse ecl = new SParametresEcluse();
              ecl = (SParametresEcluse) itEcluse.next();
              if (ecl.largeur < bat.largeur) {
                trouve = true;
                doc.insertString(doc.getLength(), " * Le type de bateau " + bat.identification
                    + " ne passe pas dans l'�cluse " + ecl.identification + "\n", _text.getStyle("txt"));
                text += " * Le type de bateau " + bat.identification + " ne passe pas dans l'�cluse "
                    + ecl.identification + "\n";
              }

            }
          }
        }
        if (!trouve) {
          doc.insertString(doc.getLength(), " OK" + "\n", _text.getStyle("txt"));

          text += " OK" + "\n";
        }

        // controle de la largeur des bateaux par rapport a la largeur du bief
        doc.insertString(doc.getLength(), "Largeur Bateau et largeur Bief" + "\n", _text.getStyle("titre"));
        text += "Largeur Bateau et largeur Bief" + "\n";
        itBat = listeBateaux_.listIterator();
        trouve = false;
        while (itBat.hasNext()) {
          SParametresBateau bat = new SParametresBateau();
          bat = (SParametresBateau) itBat.next();
          if (listeBiefs_ != null) {
            final ListIterator itEcluse = listeBiefs_.listIterator();
            while (itEcluse.hasNext()) {
              SParametresBief bief = new SParametresBief();
              bief = (SParametresBief) itEcluse.next();
              if (bief.largeur < bat.largeur) {
                trouve = true;
                doc.insertString(doc.getLength(), " * Le type de bateau " + bat.identification
                    + " ne passe pas dans l'�cluse " + bief.identification + "\n", _text.getStyle("txt"));
                text += " * Le type de bateau " + bat.identification + " ne passe pas dans l'�cluse "
                    + bief.identification + "\n";
              }

            }
          }
        }
        if (!trouve) {
          doc.insertString(doc.getLength(), " OK" + "\n", _text.getStyle("txt"));
          text += " OK" + "\n";
        }

      } catch (final BadLocationException e) {

        e.printStackTrace();
      }
    }

    _text.setDocument(doc);

  }

  /**
   * @param _e
   */

  public void annuler() {
    imp2_.removeInternalFrame(this);
  }

  public void actionPerformed(final ActionEvent _e) {
    if (_e.getSource() == bAnnuler_) {
      annuler();
    }

    else if (_e.getSource() == bImprimer_) {
      String filename = "";
      final BuFileChooser chooser = new BuFileChooser();
      chooser.setFileFilter(new BuFileFilter("txt", "Fichiers : " + ".txt"));
      final int returnVal = chooser.showOpenDialog((JFrame) imp2_.getApp());
      if (returnVal == JFileChooser.APPROVE_OPTION) {
        filename = chooser.getSelectedFile().getAbsolutePath();
      }
      if (!filename.substring(filename.length() - 4).equalsIgnoreCase(".txt")) {
        filename = filename + ".txt";
      }

      /*
       * String s=""; try { //s = tText.getDocument().getText(0,tText.getText().length()); s =
       * tText.getText(0,tText.getText().length()); } catch (BadLocationException e1) { // TODO Auto-generated catch
       * block e1.printStackTrace(); }
       */
      try {
        final FileWriter fw = new FileWriter(filename);
        fw.write(text);
        fw.close();
      } catch (final IOException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }

    }
  }

  /*
   * a virer
   */
  public void affMessage(final String _t) {
    final BuDialogMessage dialog_mess = new BuDialogMessage(imp2_.getApp(), imp2_.getInformationsSoftware(), "" + _t);
    dialog_mess.activate();
  }

  public void internalFrameActivated(final InternalFrameEvent _e) {
  // TODO Auto-generated method stub

  }

  public void internalFrameClosed(final InternalFrameEvent _e) {
    // this.closable=true;

    annuler();
  }

  public void internalFrameDeactivated(final InternalFrameEvent _e) {
  // TODO Auto-generated method stub

  }

  public void internalFrameDeiconified(final InternalFrameEvent _e) {
  // TODO Auto-generated method stub

  }

  public void internalFrameIconified(final InternalFrameEvent _e) {
  // TODO Auto-generated method stub

  }

  public void internalFrameOpened(final InternalFrameEvent _e) {
  // TODO Auto-generated method stub

  }

  public void internalFrameClosing(final InternalFrameEvent _e) {
  // TODO Auto-generated method stub

  }

}
