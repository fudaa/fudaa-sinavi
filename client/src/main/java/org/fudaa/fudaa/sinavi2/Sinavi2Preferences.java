/*
 * @file         MascaretPreferences.java
 * @creation     2000-11-06
 * @modification $Date: 2006-09-19 15:08:58 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 EDF/LNHE
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.sinavi2;

import com.memoire.bu.BuPreferences;

/**
 * Preferences pour Mascaret.
 * 
 * @version $Revision: 1.6 $ $Date: 2006-09-19 15:08:58 $ by $Author: deniger $
 * @author Jean-Marc Lacombe
 */
public class Sinavi2Preferences extends BuPreferences {
  public final static Sinavi2Preferences SINAVI2 = new Sinavi2Preferences();

  public void applyOn(final Object _o) {
    if (!(_o instanceof Sinavi2Implementation)) {
      throw new RuntimeException("" + _o + " is not a Sinavi2Implementation.");
    }
  }
}
