package org.fudaa.fudaa.sinavi2;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.ListIterator;

import javax.swing.JComponent;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;
import javax.swing.event.InternalFrameEvent;
import javax.swing.event.InternalFrameListener;

import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuCheckBox;
import com.memoire.bu.BuComboBox;
import com.memoire.bu.BuCommonImplementation;
import com.memoire.bu.BuDialogMessage;
import com.memoire.bu.BuInternalFrame;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuTable;

import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.table.CtuluTableExcelWriter;

import org.fudaa.dodico.corba.sinavi2.SParametresBateau;
import org.fudaa.dodico.corba.sinavi2.SParametresBief;
import org.fudaa.dodico.corba.sinavi2.SParametresEcluse;
import org.fudaa.dodico.corba.sinavi2.SResultatHistorique;

/***********************************************************************************************************************
 * impl�mentation d'une fen�tre interne permettant d'afficher les resultats du fichier his sous forme de tableau avec
 * des filtres sur les donn�es cf. Sinavi2FilleAffTableauGeneration
 * 
 * @version $Revision: 1.12 $ $Date: 2007-05-04 14:01:05 $ by $Author: deniger $
 * @author Fatimatou Ka , Beno�t Maneuvrier
 **********************************************************************************************************************/
public class Sinavi2FilleAffHistorique extends BuInternalFrame implements ActionListener, InternalFrameListener {

  private final BuLabel lTitre_ = new BuLabel("Historique de la simulation  ");
  private final BuLabel lFiltre_ = new BuLabel("Filtrer : ");
  private final BuButton bAnnuler_ = new BuButton(Sinavi2Resource.SINAVI2.getIcon("QUITTER"), "Quitter");

  private final BuButton bImprimer_ = new BuButton(Sinavi2Resource.SINAVI2.getIcon("IMPRIMER"), "Imprimer");

  private final BuButton bMiseAJour_ = new BuButton(Sinavi2Resource.SINAVI2.getIcon("MISAJOUR"), "Mise � Jour");

  /** * panel contenant les boutons, il est plac� en bas */
  private final BuPanel pBoutons_ = new BuPanel();
  /**
   * panel contenant les diff�rents champs de la fen�tre
   */
  private final BuPanel pTitre_ = new BuPanel();

  private final BuPanel pDonnees2_ = new BuPanel();
  private final BuComboBox cBateau_ = new BuComboBox();

  private final BuLabel lBateau_ = new BuLabel("Bateau");
  private final BuComboBox cTypeBateau_ = new BuComboBox();
  private final BuLabel lTypeBateau_ = new BuLabel("Type de bateau");
  private final BuLabel lElementAFranchir_ = new BuLabel("Type d'Element");
  private final BuComboBox cElementAFranchir_ = new BuComboBox();
  private final BuLabel lElement_ = new BuLabel("Element");
  private final BuComboBox cElement_ = new BuComboBox();
  private final BuCheckBox cAttente_ = new BuCheckBox("Attente ");
  public ArrayList listenumerobateau_;
  public ArrayList listeHistorique_;
  public ArrayList listeBateaux_;
  public ArrayList listeBiefs_;
  public ArrayList listeEcluses_;
  public ArrayList listeElement_;
  public ArrayList listeGares_;
  public BuTable table_;
  public Sinavi2TableauHistorique tb_;
  public Sinavi2Implementation imp2_ = null;
  private SResultatHistorique[] tableauResHistorique_;

  public Sinavi2FilleAffHistorique(final BuCommonImplementation _appli, final SResultatHistorique[] _listeHistorique,
      final ArrayList _listeBateaux, final ArrayList _listeBiefs, final ArrayList _listeEcluses,
      final int _bateauSelected, final int _typeBateauSelected, final int _elementAFranchir,
      final int _elementSelected, final ArrayList _listeGares, final boolean _attenteSelected) {
    super("Affichage de l'historique", true, true, true, false);
    tableauResHistorique_ = _listeHistorique;

    remplirListeHistorique(_listeHistorique);

    listeBateaux_ = _listeBateaux;
    listeBiefs_ = _listeBiefs;
    listeEcluses_ = _listeEcluses;
    listeGares_ = _listeGares;
    listenumerobateau_ = new ArrayList();
    cBateau_.addItem("TOUS");
    cTypeBateau_.addItem("TOUS");
    cElement_.addItem("TOUS");
    if (listeBateaux_ != null) {
      final ListIterator itBateau = listeBateaux_.listIterator();
      while (itBateau.hasNext()) {
        final SParametresBateau b = (SParametresBateau) itBateau.next();
        cTypeBateau_.addItem(b.identification);
      }
    }
    if (_typeBateauSelected != 0) {
      cTypeBateau_.setSelectedIndex(_typeBateauSelected);
    }
    if (_listeHistorique != null) {
      if (_typeBateauSelected == 0) {
        listenumerobateau_ = new ArrayList();
        // FuLog.debug("on entre dans pas d etype bateau");
        for (int i = 0; i < _listeHistorique.length; i++) {
          final SResultatHistorique his = _listeHistorique[i];
          if (!listenumerobateau_.contains(String.valueOf(his.numeroBateau))) {
            listenumerobateau_.add("" + his.numeroBateau);
            cBateau_.addItem("" + his.numeroBateau);
          }
        }
      } else {
        // FuLog.debug("on entre dans type bateau");
        listenumerobateau_ = new ArrayList();
        for (int i = 0; i < tableauResHistorique_.length; i++) {
          final SResultatHistorique his = tableauResHistorique_[i];
          // System.out.println(" num bateau :" +his.numeroBateau + " type bateau " + his.numeroTypeBateau);
          if (!listenumerobateau_.contains(String.valueOf(his.numeroBateau))
              && his.numeroTypeBateau == cTypeBateau_.getSelectedIndex()) {
            // FuLog.debug("message test");
            listenumerobateau_.add("" + his.numeroBateau);
            cBateau_.addItem("" + his.numeroBateau);
          }
        }

      }
    }

    if (_elementAFranchir == 0) {
      listeElement_ = new ArrayList();
      // FuLog.debug("on entre dans pas d etype bateau");
      ListIterator it = listeBiefs_.listIterator();
      while (it.hasNext()) {
        cElement_.addItem("bie:" + ((SParametresBief) it.next()).identification);
      }
      it = listeEcluses_.listIterator();
      while (it.hasNext()) {
        cElement_.addItem("ecl:" + ((SParametresEcluse) it.next()).identification);
      }
      // FuLog.debug("gare 0 ");
      // imp2_.mAJGares();
      // FuLog.debug("gare ");
      it = listeGares_.listIterator();
      while (it.hasNext()) {

        final String g = it.next().toString();
        // FuLog.debug("gare "+g);
        cElement_.addItem("gar:" + g);
      }
    } else {
      ListIterator it;
      if (cElementAFranchir_.getSelectedIndex() == 2) {
        it = listeBiefs_.listIterator();
        while (it.hasNext()) {
          cElement_.addItem("bie:" + ((SParametresBief) it.next()).identification);
        }
      } else if (cElementAFranchir_.getSelectedIndex() == 1) {
        it = listeEcluses_.listIterator();
        while (it.hasNext()) {
          cElement_.addItem("ecl:" + ((SParametresEcluse) it.next()).identification);
        }
      } else if (cElementAFranchir_.getSelectedIndex() == 3) {
        it = listeGares_.listIterator();
        while (it.hasNext()) {
          final String g = it.next().toString();
          cElement_.addItem("gar:" + g);
        }
      }

    }

    /** menu d�roulant pour l'�l�m�nt � franchir* */
    cElementAFranchir_.addItem("TOUS");
    cElementAFranchir_.addItem("G");
    cElementAFranchir_.addItem("B");
    cElementAFranchir_.addItem("O");

    cBateau_.addActionListener(this);
    cTypeBateau_.addActionListener(this);
    cElementAFranchir_.addActionListener(this);
    cElement_.addActionListener(this);
    cAttente_.setSelected(_attenteSelected);
    /*
     * if(_bateauSelected != 0 || _typeBateauSelected !=0 || _elementAFranchir != 0){
     * cBateau_.setSelectedIndex(_bateauSelected); cTypeBateau_.setSelectedIndex(_typeBateauSelected);
     * cElementAFranchir_.setSelectedIndex(_elementAFranchir); miseajour(); }
     */
    // FuLog.debug("taille liste bateau :"+cBateau_.getItemCount());
    if (_bateauSelected != 0) {
      cBateau_.setSelectedIndex(_bateauSelected);
    }

    if (_elementAFranchir != 0) {
      cElementAFranchir_.setSelectedIndex(_elementAFranchir);
    }
    if (_elementSelected != 0) {
      cElement_.setSelectedIndex(_elementSelected);
    }
    miseajour();
    imp2_ = (Sinavi2Implementation) _appli.getImplementation();

    /** ajout de listeners aux boutons* */
    bAnnuler_.addActionListener(this);
    bImprimer_.addActionListener(this);
    bMiseAJour_.addActionListener(this);

    final GridBagLayout g = new GridBagLayout();
    pTitre_.setLayout(g);
    final GridBagConstraints c = new GridBagConstraints();
    c.fill = GridBagConstraints.BOTH;
    c.gridx = 0;
    c.gridy = 2;
    pTitre_.add(lTitre_, c);
    c.gridx = 1;
    c.gridy = 2;

    pTitre_.add(lFiltre_, c);
    c.gridx = 2;
    c.gridy = 2;
    pTitre_.add(lBateau_, c);

    c.gridx = 2;
    c.gridy = 1;
    pTitre_.add(lTypeBateau_, c);

    c.gridx = 2;
    c.gridy = 3;
    pTitre_.add(lElementAFranchir_, c);

    c.gridx = 2;
    c.gridy = 4;
    pTitre_.add(lElement_, c);
    c.gridx = 3;
    c.gridy = 2;
    pTitre_.add(cBateau_, c);

    c.gridx = 3;
    c.gridy = 1;
    pTitre_.add(cTypeBateau_, c);

    cTypeBateau_.addActionListener(this);
    c.gridx = 3;
    c.gridy = 3;
    pTitre_.add(cElementAFranchir_, c);

    cElement_.addActionListener(this);
    c.gridx = 3;
    c.gridy = 4;
    pTitre_.add(cElement_, c);

    c.gridx = 3;
    c.gridy = 5;
    pTitre_.add(cAttente_, c);
    ((JComponent) getContentPane()).setBorder(new EmptyBorder(25, 25, 25, 25));
    /*
     * pTitre_.add(lTitre_); pTitre_.add(lBateau_); pTitre_.add(cBateau_); pTitre_.add(lTypeBateau_);
     * pTitre_.add(cTypeBateau_); pTitre_.add(lElementAFranchir_); pTitre_.add(cElementAFranchir_);
     */
    pBoutons_.setLayout(new FlowLayout(FlowLayout.CENTER));
    pBoutons_.add(bAnnuler_);
    pBoutons_.add(bImprimer_);
    pBoutons_.add(bMiseAJour_);
    getContentPane().add(pTitre_, BorderLayout.NORTH);
    getContentPane().add(pDonnees2_, BorderLayout.CENTER);
    getContentPane().add(pBoutons_, BorderLayout.SOUTH);

    pack();

    /** *reinitialisation des valeurs* */
    setVisible(true);
    addInternalFrameListener(this);
    imp2_.addInternalFrame(this);
  }

  private void remplirListeHistorique(final SResultatHistorique[] _listeHistorique) {
    listeHistorique_ = new ArrayList();
    for (int i = 0; i < _listeHistorique.length; i++) {

      final Sinavi2TypeHistorique tg = new Sinavi2TypeHistorique(_listeHistorique[i]);
      listeHistorique_.add(tg);
    }
  }

  public void miseajour() {
    // pDonnees2_.removeAll();
    ArrayList selection = new ArrayList();
    int row = 0;
    ListIterator iter;
    tb_ = new Sinavi2TableauHistorique();
    if (cTypeBateau_.getSelectedIndex() != 0) {
      final int bateauSel = cTypeBateau_.getSelectedIndex();
      iter = listeHistorique_.listIterator();
      while (iter.hasNext()) {
        Sinavi2TypeHistorique tg;// = new SinaviTypeHistorique();
        tg = (Sinavi2TypeHistorique) iter.next();
        // System.out.println(""+tg.numeroTypeBateau_);
        // System.out.println("bateau sel "+bateauSel);
        if (tg.numeroTypeBateau_ == bateauSel) {
          selection.add(tg);
          // System.out.println("ajout de type bateau "+bateauSel);
        }
      }
    }
    if (cBateau_.getSelectedIndex() != 0) {
      final String Bateau = cBateau_.getSelectedItem().toString();
      // System.out.println("aaaaaaaaaaaaaaaa mmmmmmmmmm "+ Bateau);
      final ArrayList temp = new ArrayList();
      if ((selection.size() != 0) || (selection.size() == 0 && cTypeBateau_.getSelectedIndex() != 0)) {
        iter = selection.listIterator();
      } else {
        iter = listeHistorique_.listIterator();
      }
      while (iter.hasNext()) {
        Sinavi2TypeHistorique tg;// = new SinaviTypeHistorique();
        tg = (Sinavi2TypeHistorique) iter.next();
        // System.out.println(am.charAt(0) + " difff "+tg.avalantMontant_);
        // System.out.println("tg = "+tg.numeroBateau_ + " bateau sel "+Integer.parseInt(Bateau));
        if (tg.numeroBateau_ == Integer.parseInt(Bateau)) {
          temp.add(tg);
          // System.out.println("ajout de bateau");
        }
      }
      /** selection=new ArrayList(temp);* */
      // selection=new ArrayList();
      selection = temp;
      // System.out.println("taille dela selection -->-->--> "+selection.size());
    }

    if (cElementAFranchir_.getSelectedIndex() != 0) {
      final String elem = cElementAFranchir_.getSelectedItem().toString();
      final ArrayList temp = new ArrayList();
      if ((selection.size() != 0)
          || ((selection.size() == 0 && cBateau_.getSelectedIndex() != 0 || cTypeBateau_.getSelectedIndex() != 0))) {
        iter = selection.listIterator();
      } else {
        iter = listeHistorique_.listIterator();
      }
      while (iter.hasNext()) {
        Sinavi2TypeHistorique tg = new Sinavi2TypeHistorique();
        tg = (Sinavi2TypeHistorique) iter.next();
        if (elem.equalsIgnoreCase(tg.elementAFranchir_)) {
          temp.add(tg);
          // System.out.println("ajout de l'�l�m�nt � franchir");
        }
      }
      // *selection=new ArrayList(temp);
      selection = new ArrayList();
      selection = temp;
    }
    if (cElement_.getSelectedIndex() != 0) {
      String prec = cElement_.getSelectedItem().toString().substring(0, 4);
      final String elem = cElement_.getSelectedItem().toString().substring(4);
      if (prec.equalsIgnoreCase("bie:")) {
        prec = "B";
      } else if (prec.equalsIgnoreCase("ecl:")) {
        prec = "E";
      } else {
        prec = "G";
      }

      final ArrayList temp = new ArrayList();
      if ((selection.size() != 0)) {
        iter = selection.listIterator();
      } else {
        iter = listeHistorique_.listIterator();
      }
      while (iter.hasNext()) {
        Sinavi2TypeHistorique tg;// = new SinaviTypeHistorique();
        tg = (Sinavi2TypeHistorique) iter.next();
        // System.out.println(am.charAt(0) + " difff "+tg.avalantMontant_);
        System.out.println(" test " + tg.numeroElement_);
        if (prec.equalsIgnoreCase("B") && tg.elementAFranchir_.equalsIgnoreCase("B")
            && ((SParametresBief) listeBiefs_.get(tg.numeroElement_ - 1)).identification.equalsIgnoreCase(elem)) {
          temp.add(tg);
        } else if (prec.equalsIgnoreCase("E") && tg.elementAFranchir_.equalsIgnoreCase("O")
            && ((SParametresEcluse) listeEcluses_.get(tg.numeroElement_ - 1)).identification.equalsIgnoreCase(elem)) {
          temp.add(tg);
        } else if (prec.equalsIgnoreCase("G") && tg.elementAFranchir_.equalsIgnoreCase("G")
            && ("" + tg.getNumeroElement()).equalsIgnoreCase(elem)) {
          temp.add(tg);
          // System.out.println("ajout de bateau");
        }

      }
      /** selection=new ArrayList(temp);* */
      selection = new ArrayList();
      selection = temp;
      // System.out.println("taille dela selection -->-->--> "+selection.size());
    }
    if (cAttente_.isSelected()) {
      final ArrayList temp = new ArrayList();
      if ((selection.size() != 0)) {
        iter = selection.listIterator();
      } else {
        iter = listeHistorique_.listIterator();
      }
      while (iter.hasNext()) {
        Sinavi2TypeHistorique tg;// = new SinaviTypeHistorique();
        tg = (Sinavi2TypeHistorique) iter.next();
        if (tg.attente_ > 0) {
          temp.add(tg);
        }
      }
      selection = new ArrayList();
      selection = temp;
    }

    if (selection != null) {
      // System.out.println("taille de de la selection :" +selection.size());

      iter = selection.listIterator();
      final Object[][] data = new Object[selection.size()][tb_.getColumnCount()];
      while (iter.hasNext()) {
        final Sinavi2TypeHistorique tg = (Sinavi2TypeHistorique) iter.next();

        String s = "";
        String nombief = "";
        String nomEcluse = "";
        boolean trouve = false;
        // System.out.println("copie "+tb_.getValueAt(row,0));

        /*
         * int indice=0; ListIterator bat= listeBateaux_.listIterator(); while(indice<cTypeBateau_.getSelectedIndex()-1 &&
         * bat.hasNext()){ bat.next(); indice++; } SParametresBateau b= (SParametresBateau) bat.next();
         * //System.out.print(tg.getNumeroElement()); s=b.identification;
         */

        // tb_.setValueAt(s,row,0);
        s = ((SParametresBateau) listeBateaux_.get(tg.getNumeroTypeBateau() - 1)).identification;
        data[row][0] = s;

        s = "" + tg.numeroBateau_;
        data[row][1] = s;

        s = tg.avalantMontant_ + "";
        // tb_.setValueAt(s,row,1);

        data[row][2] = s;
        if (tg.elementAFranchir_.equalsIgnoreCase("G")) {
          // System.out.println("Gare");
          s = tg.getElementAFranchir();
          // tb_.setValueAt(s,row,2);
          data[row][3] = s;
        }

        else {
          if (tg.elementAFranchir_.equalsIgnoreCase("B") && !trouve) {
            SParametresBief bi;
            final int place = tg.getNumeroElement();
            bi = (SParametresBief) listeBiefs_.get(place - 1);
            nombief = bi.identification;
            trouve = true;
            // System.out.println("--> "+ nombief);
          }

          else if (tg.elementAFranchir_.equalsIgnoreCase("O") && !trouve) {
            SParametresEcluse e;
            final int place = tg.getNumeroElement();
            e = (SParametresEcluse) listeEcluses_.get(place - 1);
            nomEcluse = e.identification;
            trouve = true;
            // System.out.println("--> "+ nomEcluse);
          }
          if (nombief.equalsIgnoreCase("")) {
            s = ConcatElem(tg.getElementAFranchir(), nomEcluse);
          } else if (nomEcluse.equalsIgnoreCase("")) {
            s = ConcatElem(tg.getElementAFranchir(), nombief);
          }
          // System.out.println("--> "+ s);
          // tb_.setValueAt(s,row,2);
          data[row][3] = s;
        }

        s = Integer.toString(tg.getNumeroElement());
        // tb_.setValueAt(s,row,3);
        data[row][4] = s;
        // s=Double.toString(tg.getHeureEntree());
        s = ConversionJourHeureMinuteSeconde(tg.getHeureEntree());
        // tb_.setValueAt(s,row,4);
        data[row][5] = s;
        // s=Double.toString(tg.getHeureSortie());
        s = ConversionJourHeureMinuteSeconde(tg.getHeureSortie());
        // tb_.setValueAt(s,row,5);
        data[row][6] = s;
        s = Sinavi2TypeDureeDeParcours.determineHeureString((int) tg.getAttente());
        // s=Double.toString(tg.getAttente());
        // tb_.setValueAt(s,row,6);
        data[row][7] = s;
        row++;

        // tb_.setNbrow(row);

      }
      tb_.setNbRow(row);
      tb_.data_ = data;
    }
    // if(tb_.data_ !=null)
    System.out.println("nb lignes -> " + tb_.getRowCount());

    table_ = new BuTable(tb_.data_, tb_.nomCol());
    table_.setSize(700, 500);
    table_.getColumnModel().getColumn(0).setPreferredWidth(50);
    table_.getColumnModel().getColumn(1).setPreferredWidth(30);
    table_.getColumnModel().getColumn(2).setPreferredWidth(30);
    table_.getColumnModel().getColumn(3).setPreferredWidth(50);
    table_.getColumnModel().getColumn(4).setPreferredWidth(40);
    table_.getColumnModel().getColumn(5).setPreferredWidth(123);
    table_.getColumnModel().getColumn(6).setPreferredWidth(123);
    table_.getColumnModel().getColumn(7).setPreferredWidth(60);

    final JScrollPane scrollPane = new JScrollPane(table_);
    pDonnees2_.add(scrollPane);
    scrollPane.repaint();
    // pDonnees2_.add(scrollPane);
    // table_.repaint();

  }

  private String ConversionJourHeureMinuteSeconde(final double secondes) {
    String s = null;
    int jour, h, min, sec;
    jour = (int) (secondes / 86400);
    final int tempHeure = (int) (secondes - jour * 86400);
    h = tempHeure / 3600;
    final int tempMinute = tempHeure - h * 3600;
    min = tempMinute / 60;
    sec = tempMinute - min * 60;
    s = String.valueOf(jour) + "J " + String.valueOf(h) + "H " + String.valueOf(min) + "m " + String.valueOf(sec)
        + "s ";
    return s;

  }

  private String ConcatElem(final String sigleElem, final String elem) {
    return sigleElem + ":" + elem;
  }

  public void annuler() {
    imp2_.removeInternalFrame(this);
    imp2_.resetFilleAffHistorique();
  }

  public void actionPerformed(final ActionEvent _e) {
    if (_e.getSource() == cTypeBateau_) {
      cBateau_.removeAllItems();
      cBateau_.addItem("TOUS");
      listenumerobateau_ = new ArrayList();
      if (tableauResHistorique_ != null) {
        for (int i = 0; i < tableauResHistorique_.length; i++) {
          final SResultatHistorique his = tableauResHistorique_[i];
          if (!listenumerobateau_.contains(String.valueOf(his.numeroBateau))
              && (his.numeroTypeBateau == cTypeBateau_.getSelectedIndex() || cTypeBateau_.getSelectedIndex() == 0)) {
            listenumerobateau_.add("" + his.numeroBateau);
            cBateau_.addItem("" + his.numeroBateau);
          }
        }
      }

    }
    if (_e.getSource() == cElementAFranchir_) {
      cElement_.removeAllItems();
      cElement_.addItem("TOUS");
      ListIterator it;
      if (cElementAFranchir_.getSelectedIndex() == 2 || cElementAFranchir_.getSelectedIndex() == 0) {
        it = listeBiefs_.listIterator();
        while (it.hasNext()) {
          cElement_.addItem("bie:" + ((SParametresBief) it.next()).identification);
        }
      }
      if (cElementAFranchir_.getSelectedIndex() == 3 || cElementAFranchir_.getSelectedIndex() == 0) {
        it = listeEcluses_.listIterator();
        while (it.hasNext()) {
          cElement_.addItem("ecl:" + ((SParametresEcluse) it.next()).identification);
        }
      }
      if (cElementAFranchir_.getSelectedIndex() == 1 || cElementAFranchir_.getSelectedIndex() == 0) {
        it = listeGares_.listIterator();
        while (it.hasNext()) {
          final String g = it.next().toString();
          cElement_.addItem("gar:" + g);
        }
      }
    }

    if (_e.getSource() == bAnnuler_) {
      annuler();

    }

    else if (_e.getSource() == bImprimer_) {
      final File s = imp2_.enregistrerXls();
      if (s == null) {
        imp2_.affMessage("Nom de Fichier incorrect");
        return;
      }
      final Sinavi2TableauHistorique tbtemp = new Sinavi2TableauHistorique();
      tbtemp.data_ = new Object[tb_.getRowCount() + 2][tb_.getColumnCount()];
      tbtemp.initNomCol(1);
      tbtemp.setColumnName("Historique des bateaux", 0);
      for (int i = 1; i < tb_.getColumnCount(); i++) {
        tbtemp.setColumnName(CtuluLibString.ESPACE, i);
      }

      tbtemp.setNbRow(tb_.getRowCount() + 2);
      for (int i = 2; i <= (tb_.getRowCount() + 1); i++) {
        for (int j = 0; j < tb_.getColumnCount(); j++) {
          tbtemp.data_[i][j] = tb_.data_[i - 2][j];
        }

      }
      final CtuluTableExcelWriter test = new CtuluTableExcelWriter(tbtemp, s);

      try {
        test.write(null);

      } catch (final RowsExceededException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      } catch (final WriteException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      } catch (final IOException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }
    } else if (_e.getSource() == bMiseAJour_) {
      final int x = cBateau_.getSelectedIndex();
      final int y = cTypeBateau_.getSelectedIndex();
      final int z = cElementAFranchir_.getSelectedIndex();
      final int w = cElement_.getSelectedIndex();
      final boolean v = cAttente_.isSelected();
      annuler();
      // miseajour();
      imp2_.affHistorique(x, y, z, w, v);
      // miseajour();
    }

  }

  public void affMessage(final String _t) {
    final BuDialogMessage dialog_mess = new BuDialogMessage(imp2_.getApp(), imp2_.getInformationsSoftware(), "" + _t);
    dialog_mess.activate();
  }

  public void internalFrameActivated(final InternalFrameEvent e) {
  // TODO Auto-generated method stub

  }

  public void internalFrameClosed(final InternalFrameEvent e) {
    this.closable = true;
    System.out.println("fermeture centralis�e des portes");
    annuler();

  }

  public void internalFrameClosing(final InternalFrameEvent e) {
  // TODO Auto-generated method stub

  }

  public void internalFrameDeactivated(final InternalFrameEvent e) {
  // TODO Auto-generated method stub

  }

  public void internalFrameDeiconified(final InternalFrameEvent e) {
  // TODO Auto-generated method stub

  }

  public void internalFrameIconified(final InternalFrameEvent e) {
  // TODO Auto-generated method stub

  }

  public void internalFrameOpened(final InternalFrameEvent e) {
  // TODO Auto-generated method stub

  }

}
