/*
 * @file         Sinavi2FilleAddModBateaux.java
 * @creation     2005-09-17
 * @modification $Date: 2006-12-20 16:13:19 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.sinavi2;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

import javax.swing.JComponent;
import javax.swing.border.EmptyBorder;
import javax.swing.event.InternalFrameEvent;
import javax.swing.event.InternalFrameListener;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuCommonImplementation;
import com.memoire.bu.BuInternalFrame;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuTextField;

import org.fudaa.ctulu.gui.CtuluLibSwing;

import org.fudaa.dodico.corba.sinavi2.SParametresBateau;

/**
 * impl�mentation d'une fen�tre interne permettant de saisir les param�tres pour les bateaux. Cette classe va permettre
 * l'ajout ou la modification d'un type de bateau
 * 
 * @version $Revision: 1.16 $ $Date: 2006-12-20 16:13:19 $ by $Author: deniger $
 * @author Fatimatou Ka , Beno�t Maneuvrier
 */
public class Sinavi2FilleAddModBateaux extends BuInternalFrame implements ActionListener, InternalFrameListener,
    FocusListener {

  /** **************************liste des boutons***************************** */

  // champs blanc pour am�liorer le design
  private final BuLabel lBlanc_ = new BuLabel("    ");
  private final BuLabel lBlanc2_ = new BuLabel("    ");
  private final BuLabel lBlanc3_ = new BuLabel("    ");
  private final BuLabel lBlanc4_ = new BuLabel("    ");
  private final BuLabel lBlanc5_ = new BuLabel("           ");
  private final BuLabel lTitre_ = new BuLabel("Type de Bateau");

  /** bouton pour annuler */
  private final BuButton bAnnuler_ = new BuButton(Sinavi2Resource.SINAVI2.getIcon("QUITTER"), "Quitter");
  /** ajouter ou modifier le type de bateau */
  private final BuButton bSauver_ = new BuButton(Sinavi2Resource.SINAVI2.getIcon("VALIDER"), "Sauver");

  /** afficher la liste des bateaux */
  private final BuButton bAfficher_ = new BuButton(Sinavi2Resource.SINAVI2.getIcon("EDITER"), "Afficher");

  /** caract�ristiques du bateaux en haut */
  private final BuLabel lId_ = new BuLabel(Sinavi2Resource.SINAVI2.getString("Identification"));
  private final BuTextField tId_ = new BuTextField("");

  private final BuLabel lLong_ = new BuLabel(Sinavi2Resource.SINAVI2.getString("Longueur"));
  private final LongueurField tLong_ = new LongueurField(false, true, true);

  private final BuLabel lLarg_ = new BuLabel(Sinavi2Resource.SINAVI2.getString("Largeur"));
  private final LongueurField tLarg_ = new LongueurField(false, true, true);

  /** horaire de navigation milieu */

  private final BuLabel lHoraireDeNavigation_ = new BuLabel(Sinavi2Resource.SINAVI2.getString("Horaires de navigation"));
  private final BuLabel lHoraireDebut_ = new BuLabel(Sinavi2Resource.SINAVI2.getString("Horaires de D�but   "));
  private final BuLabel lHoraireFin_ = new BuLabel(Sinavi2Resource.SINAVI2.getString("Horaires de Fin"));

  private final DureeField dHoraireDebutHeure_ = new DureeField(false, false, true, false, false);
  private final DureeField dHoraireDebutMinute_ = new DureeField(false, false, false, true, false);

  private final DureeField dHoraireFinHeure_ = new DureeField(false, false, true, false, false);
  private final DureeField dHoraireFinMinute_ = new DureeField(false, false, false, true, false);

  // -----------------------------------------------------------------------------
  /** **utile pour les vitesses** */
  private final BuLabel lVitesseParDefaut_ = new BuLabel("Vitesses par D�faut de navigation");
  private final BuLabel lVitesseMontant_ = new BuLabel("Vitesse des Montants");
  private final BuLabel lVitesseDescendant_ = new BuLabel("Vitesse des Avalants");
  private final LongueurField tVitesseMontantC_ = new LongueurField(true, true, false);
  private final LongueurField tVitesseMontantL_ = new LongueurField(true, true, false);
  private final LongueurField tVitesseDescendantC_ = new LongueurField(true, true, false);
  private final LongueurField tVitesseDescendantL_ = new LongueurField(true, true, false);

  // ------------------------------------------------------------------------------
  /** charg� l�ges tirant d'eau gene admissible 3 colonnes */

  private final BuLabel lCharge_ = new BuLabel("Charge");
  private final BuLabel lLege_ = new BuLabel("L�ge");
  private final BuLabel lTirantDEau_ = new BuLabel("Tirant d'eau");
  private final BuLabel lGeneAdmissible_ = new BuLabel("G�ne Admissible");

  private final LongueurField tChargeT_ = new LongueurField(false, true, true);
  private final DureeField dChargeHeureG_ = new DureeField(false, false, true, false, false);
  private final DureeField dChargeMinuteG_ = new DureeField(false, false, false, true, false);

  private final LongueurField tLegeT_ = new LongueurField(false, true, true);
  private final DureeField dLegeHeureG_ = new DureeField(false, false, true, false, false);
  private final DureeField dLegeMinuteG_ = new DureeField(false, false, false, true, false);

  /** **gerer la liste des bateaux**** */
  // ---------------------- public LinkedList listeBateaux_;
  public ArrayList listeBateaux_;

  /** **bateau pour les champs en cas de modifications*** */
  // ---------------------public SParametresBateau batCourant_;
  private int nBat_ = -1;// -1 a lorigine

  /** * panel contenant les boutons, il est plac� en bas */

  private final BuPanel pBoutons_ = new BuPanel();
  /**
   * panel contenant les diff�rents champs de la fen�tre
   */
  private final BuPanel pTitre_ = new BuPanel();

  private final BuPanel pDonnees2_ = new BuPanel();

  /**
   * une instance de SinaviImplementation
   */
  // private BuCommonImplementation app_;
  /**
   * Constructeur
   * 
   * @param _app une instance de SinaviImplementation
   */

  // private BuCommonImplementation _appli;
  public Sinavi2Implementation imp_ = null;

  /*
   * boolean modif indique si on est en modification ou en ajout si en modif on ferme la fenetre apr�s validation sinon
   * on r�initialise les champs et on met le focus au champ identification pour resaisir un nouveau bateau
   */

  protected boolean modif_;

  // ---------------------------public Sinavi2FilleAddModBateaux (BuCommonImplementation appli_,LinkedList liste_/*,int
  // bat_*/) {
  /*
   * parametres du constructeur appli : sinavi2implementation _liste : liste des bateaux modif : vrai si on est en
   * modification
   */
  public Sinavi2FilleAddModBateaux(final BuCommonImplementation _appli, final ArrayList _liste, final boolean _modif) {
    super("Ajouter/Modifier un Type de Bateau", true, true, true, false);
    /** ************************* nbat_=bat_;************************** */
    listeBateaux_ = _liste;
    modif_ = _modif;
    imp_ = (Sinavi2Implementation) _appli.getImplementation();
    bSauver_.addActionListener(this);
    bAnnuler_.addActionListener(this);
    bAfficher_.addActionListener(this);
    tId_.setToolTipText("Entrez l'identifiant du type de bateau");

    ((JComponent) getContentPane()).setBorder(new EmptyBorder(25, 25, 25, 25));
    /** creation de la fenetre */

    pTitre_.add(lTitre_, "center");
    /*
     * Layout pour organiser par ligne et par colonne grid x : colonne grid y : ligne
     */

    final GridBagLayout g2 = new GridBagLayout();
    pDonnees2_.setLayout(g2);
    final GridBagConstraints c = new GridBagConstraints();
    c.fill = GridBagConstraints.BOTH;

    /*
     * c.gridx =2; c.gridy =0; pDonnees2_.add(lTitre_);
     */

    c.gridx = 1;
    c.gridy = 2;
    pDonnees2_.add(lId_, c);

    c.gridx = 2;
    c.gridy = 2;
    pDonnees2_.add(tId_, c);
    tId_.addFocusListener(this);

    /** ***********test**************** */
    /*
     * c.gridx = 3; c.gridy = 0; pDonnees2_.add(lBlanc_,c);
     */

    c.gridx = 1;
    c.gridy = 3;
    pDonnees2_.add(lLong_, c);

    c.gridx = 2;
    c.gridy = 3;
    pDonnees2_.add(tLong_, c);
    tLong_.addFocusListener(this);
    tLong_.setToolTipText("Entrez la longueur du type de bateau");

    c.gridx = 1;
    c.gridy = 4;
    pDonnees2_.add(lLarg_, c);

    c.gridx = 2;
    c.gridy = 4;
    pDonnees2_.add(tLarg_, c);
    tLarg_.addFocusListener(this);
    tLarg_.setToolTipText("Entrez la largeur du type de bateau");

    c.gridx = 0;
    c.gridy = 5;
    pDonnees2_.add(lBlanc_, c);

    /*
     * c.gridx =1; c.gridy =5; pDonnees2_.add(lHoraireDeNavigation_,c);
     */
    c.gridx = 0;
    c.gridy = 6;
    pDonnees2_.add(lHoraireDeNavigation_, c);
    c.gridx = 1;
    c.gridy = 7;
    pDonnees2_.add(lHoraireDebut_, c);

    /*
     * c.gridx =2; c.gridy =7; pDonnees2_.add(dHoraireDebut_,c);
     */

    c.gridx = 2;
    c.gridy = 7;
    pDonnees2_.add(dHoraireDebutHeure_, c);
    dHoraireDebutHeure_.addFocusListener(this);
    dHoraireDebutHeure_.setToolTipText("Entrez l'heure de d�but de navigation");

    c.gridx = 3;
    c.gridy = 7;
    pDonnees2_.add(dHoraireDebutMinute_, c);
    dHoraireDebutMinute_.addFocusListener(this);

    c.gridx = 1;
    c.gridy = 8;
    pDonnees2_.add(lHoraireFin_, c);
    /*
     * c.gridx =2; c.gridy =8; pDonnees2_.add(dHoraireFin_,c);
     */

    c.gridx = 2;
    c.gridy = 8;
    pDonnees2_.add(dHoraireFinHeure_, c);
    dHoraireFinHeure_.addFocusListener(this);
    dHoraireFinHeure_.setToolTipText("Entrez l'heure de fin de navigation");
    c.gridx = 3;
    c.gridy = 8;
    pDonnees2_.add(dHoraireFinMinute_, c);

    dHoraireFinMinute_.addFocusListener(this);

    c.gridx = 0;
    c.gridy = 9;
    pDonnees2_.add(lBlanc2_, c);

    c.gridx = 2;
    c.gridy = 10;
    pDonnees2_.add(lCharge_, c);
    c.gridx = 5;
    c.gridy = 10;
    pDonnees2_.add(lLege_, c);

    c.gridx = 1;
    c.gridy = 11;
    pDonnees2_.add(lTirantDEau_, c);
    c.gridx = 2;
    c.gridy = 11;
    pDonnees2_.add(tChargeT_, c);
    tChargeT_.addFocusListener(this);
    tChargeT_.setToolTipText("Entrez le tirant d'eau Charge");
    c.gridx = 5;
    c.gridy = 11;
    pDonnees2_.add(tLegeT_, c);
    tLegeT_.addFocusListener(this);
    tLegeT_.setToolTipText("Entrez le tirant d'eau L�ge");
    c.gridx = 1;
    c.gridy = 12;
    pDonnees2_.add(lGeneAdmissible_, c);
    c.gridx = 2;
    c.gridy = 12;
    pDonnees2_.add(dChargeHeureG_, c);
    dChargeHeureG_.addFocusListener(this);
    dChargeHeureG_.setToolTipText("Entrez la g�ne admissible Charge");
    c.gridx = 3;
    c.gridy = 12;
    pDonnees2_.add(dChargeMinuteG_, c);
    dChargeMinuteG_.addFocusListener(this);
    c.gridx = 4;
    c.gridy = 12;
    pDonnees2_.add(lBlanc5_, c);

    c.gridx = 5;
    c.gridy = 12;
    pDonnees2_.add(dLegeHeureG_, c);
    dLegeHeureG_.addFocusListener(this);
    dChargeHeureG_.setToolTipText("Entrez la g�ne admissible L�ge");
    c.gridx = 6;
    c.gridy = 12;
    pDonnees2_.add(dLegeMinuteG_, c);
    dLegeMinuteG_.addFocusListener(this);

    // -----------------------------------------------------

    /** **utile pour les manoeuvres** */
    c.gridx = 0;
    c.gridy = 13;
    pDonnees2_.add(lBlanc3_, c);

    c.gridx = 0;
    c.gridy = 14;
    pDonnees2_.add(lVitesseParDefaut_, c);

    c.gridx = 1;
    c.gridy = 15;
    pDonnees2_.add(lVitesseMontant_, c);
    c.gridx = 2;
    c.gridy = 15;
    pDonnees2_.add(tVitesseMontantC_, c);
    tVitesseMontantC_.setToolTipText("Entrez la vitesse Charge dans le sens Montant");
    tVitesseMontantC_.addFocusListener(this);
    c.gridx = 5;
    c.gridy = 15;
    pDonnees2_.add(tVitesseMontantL_, c);
    tVitesseMontantL_.setToolTipText("Entrez la vitesse Lege dans le sens Montant");
    tVitesseMontantL_.addFocusListener(this);
    c.gridx = 1;
    c.gridy = 16;
    pDonnees2_.add(lVitesseDescendant_, c);
    c.gridx = 2;
    c.gridy = 16;
    pDonnees2_.add(tVitesseDescendantC_, c);
    tVitesseDescendantC_.setToolTipText("Entrez la vitesse Charge dans le sens Avalant");
    tVitesseDescendantC_.addFocusListener(this);
    c.gridx = 5;
    c.gridy = 16;
    pDonnees2_.add(tVitesseDescendantL_, c);
    tVitesseDescendantC_.setToolTipText("Entrez la vitesse L�ge dans le sens Avalant");
    tVitesseDescendantL_.addFocusListener(this);
    c.gridx = 0;
    c.gridy = 17;
    pDonnees2_.add(lBlanc4_, c);

    pBoutons_.setLayout(new FlowLayout(FlowLayout.CENTER));
    pBoutons_.add(bAnnuler_);
    pBoutons_.add(bSauver_);
    pBoutons_.add(bAfficher_);
    /* disposition des paneaux */
    getContentPane().add(pTitre_, BorderLayout.NORTH);
    getContentPane().add(pDonnees2_, BorderLayout.CENTER);
    getContentPane().add(pBoutons_, BorderLayout.SOUTH);

    /* intialise les champs avec le num�ro du bateau -1 zi ajout sinon modification du bateau nbat_ */
    initialiseChamps(nBat_);

    /* valide les champs */
    pack();

    /** *reinitialisation des valeurs* */

    setVisible(true);
    imp_.addInternalFrame(this);
    // contraintes horaires -> met des validators sur les champs
    final List dureeList = new ArrayList();
    final List longueurList = new ArrayList();
    // tId_.setValueValidator(BuValueValidator.ID);
    dChargeMinuteG_.setDureeValidator(DureeFieldValidator.creeMax(3600, "Entrez une dur�e de g�ne admissible correct"));
    // dureeList.add(dChargeMinuteG_);
    dHoraireDebutMinute_.setDureeValidator(DureeFieldValidator.creeMax(3600,
        "Entrez un horaire de d�but de navigation correct"));
    dureeList.add(dHoraireDebutMinute_);
    dHoraireDebutHeure_.setDureeValidator(DureeFieldValidator.creeMax(86400,
        "Entrez un horaire de d�but de navigation correct"));
    dureeList.add(dHoraireDebutHeure_);
    dHoraireFinMinute_.setDureeValidator(DureeFieldValidator.creeMax(3600,
        "Entrez un horaire de fin de navigation correct"));
    dureeList.add(dHoraireFinMinute_);
    dHoraireFinHeure_.setDureeValidator(DureeFieldValidator.creeMax(86400,
        "Entrez un horaire de fin de navigation correct"));
    dureeList.add(dHoraireFinHeure_);

    // longueurs
    tLarg_.setLongueurValidator(LongueurFieldValidator.creeMin(0.001));
    longueurList.add(tLarg_);
    tLong_.setLongueurValidator(LongueurFieldValidator.creeMin(0.001));
    longueurList.add(tLong_);
    // TODO a continuer
    // tous les champs a valider sont mis dans un tableau.
    // comme ca pour valider, il suffit de parcourir le tableau
    // et de recuperer les erreurs.
    // voir la m�thode controler_entrees()
    dureesAValider_ = (DureeField[]) dureeList.toArray(new DureeField[dureeList.size()]);
    longueursAValider_ = (LongueurField[]) longueurList.toArray(new LongueurField[longueurList.size()]);
  }

  /**
   * @param _e
   */
  public void actionPerformed(final ActionEvent _e) {
    if (_e.getSource() == bAnnuler_) {
      // quitter la fen�tre
      /*
       * int te=tId_.getText().length(); JOptionPane.showMessageDialog(null,"nb caractere "+te);
       */
      annuler();

      // JOptionPane.showMessageDialog(null,"Bouton Annuler");

    } else if (_e.getSource() == bSauver_) {
      // ajouter ou modifier un bateau
      if (controlerEntrees()) {
        if (modif_) {
          annuler();
        } else {
          initialiseChamps(-1);
        }

      }

      tId_.requestFocus();

    } else if (_e.getSource() == bAfficher_) {
      // afficher les bateaux
      afficherBateaux();
      tId_.requestFocus();
    }

  }

  private void annuler() {
    // fermer la fen�tre et la mettre � null dans l'impl�mentation
    imp_.removeInternalFrame(this);
    imp_.resetFille2ModBateaux();

  }

  DureeField[] dureesAValider_;
  LongueurField[] longueursAValider_;

  /**
   * Cette m�thode a pour but de contr�ler les champs et appelle une fonction addListeBateau pour ajouter un bateau dans
   * la liste. Si une valeur est erron�e, un message est affich� � l'utilisateur
   */
  private boolean controlerEntrees() {
    if (tId_.getText().equals("")) {

      // BuDialogMessage dialog_mess = new BuDialogMessage(imp_.getApp(),imp_.getInformationsSoftware(), "Entrez
      // l'identifiant du type de bateau");
      imp_.affMessage("Entrez l'identifiant du type de bateau");

      return false;
    }
    for (int i = 0; i < dureesAValider_.length; i++) {
      if (!dureesAValider_[i].isValueValid()) {
        imp_.affMessage(dureesAValider_[i].val_.getDescription());
        return false;
      }
    }
    for (int i = 0; i < longueursAValider_.length; i++) {
      if (!longueursAValider_[i].isValueValid()) {
        imp_.affMessage(longueursAValider_[i].val_.getDescription());
        return false;
      }
    }

    if (tChargeT_.getLongueurField() > 0) {
      // if (tChargeT_.val_.isMetreValid(tChargeT_.getLongueurField()))/*tChargeT_.getLongueurField() != 0 /*&&
      // dChargeHeureG_.getDureeField() != 0 && dChargeMinuteG_.getDureeField()!=0)*/ {
      // String nom_ = new String();
      if ((dChargeHeureG_.getDureeField() + dChargeMinuteG_.getDureeField()) == 0) {
        imp_.affMessage("La dur�e de g�ne admissible doit �tre sup�rieure � 0.");
        return false;
      }
      if (tVitesseMontantC_.getLongueurField() == 0) {
        imp_.affMessage("La Vitesse dans le sens montant doit �tre sup�rieure � 0.");
        return false;
      } else if (tVitesseDescendantC_.getLongueurField() == 0) {
        imp_.affMessage("La Vitesse dans le sens avalant doit �tre sup�rieure � 0.");
        return false;
      } else {
        final String nom = tId_.getText() + "C";
        final Sinavi2TypeBateau b = new Sinavi2TypeBateau(nom, tLong_.getLongueurField(), tLarg_.getLongueurField(),/* dHoraireDebut_.getDureeField(),dHoraireFin_.getDureeField() */
        dHoraireDebutHeure_.getDureeField(), dHoraireDebutMinute_.getDureeField(), dHoraireFinHeure_.getDureeField(),
            dHoraireFinMinute_.getDureeField(), tChargeT_.getLongueurField(), dChargeHeureG_.getDureeField(),
            dChargeMinuteG_.getDureeField(), tVitesseMontantC_.getLongueurField(), tVitesseDescendantC_
                .getLongueurField());
        SParametresBateau bat = new SParametresBateau();
        bat = b.bat_;
        addBateau(nom, bat);
      }

    }
    if (tLegeT_.getLongueurField() > 0) {

      if ((dLegeHeureG_.getDureeField() + dLegeMinuteG_.getDureeField()) == 0) {
        imp_.affMessage("La dur�e de g�ne admissible doit �tre sup�rieure � 0.");
        return false;
      }

      if (tVitesseMontantL_.getLongueurField() == 0) {
        imp_.affMessage("La Vitesse dans le sens montant doit �tre sup�rieure � 0.");
        return false;
      } else if (tVitesseDescendantL_.getLongueurField() == 0) {
        imp_.affMessage("La Vitesse dans le sens avalant doit �tre sup�rieure � 0.");
        return false;
      } else {
        final String nom_ = tId_.getText() + "L";
        /*
         * on cr� un SinaviTypeBateau qui modifie les entr�es de l'utilisateur pour qu'elles correspondent aux formats
         * du SParametresBateau de l'IDL.
         */

        final Sinavi2TypeBateau b = new Sinavi2TypeBateau(nom_, tLong_.getLongueurField(), tLarg_.getLongueurField(),
            dHoraireDebutHeure_.getDureeField(), dHoraireDebutMinute_.getDureeField(), dHoraireFinHeure_
                .getDureeField(), dHoraireFinMinute_.getDureeField(), tLegeT_.getLongueurField(), dLegeHeureG_
                .getDureeField(), dLegeMinuteG_.getDureeField(), tVitesseMontantL_.getLongueurField(),
            tVitesseDescendantL_.getLongueurField());
        SParametresBateau bat = new SParametresBateau();
        bat = b.bat_;
        addBateau(nom_, bat);
      }
    }
    return true;
  }

  /*
   * m�thode affiche la cr�ation et appelle la fonction pour ajouter le bateau et la confirme par un message
   */

  private void addBateau(final String _nom, final SParametresBateau _bat) {
    addListeBateau(_bat);
    if (_nom.charAt(_nom.length() - 1) == 'C') {
      imp_.affMessage("Cr�ation du type bateau Charge de nom " + _nom + " r�ussie");
    } else {
      imp_.affMessage("Cr�ation du type bateau L�ge de nom " + _nom + " r�ussie");
    }
  }

  public void addListeBateau(final SParametresBateau _b) {
    /* ajout ou modification d'un bateau */

    /** *ajout d'un bateau */
    /** verifier l'existance du bateau* */
    final int x = rechercherBateau(_b.identification);
    System.out.println("x: " + x);
    if (x == -1) {
      listeBateaux_.add(_b);
    } else {
      System.out.println("suppression du bateau num�ro " + x);
      listeBateaux_.remove(x);
      listeBateaux_.add(x, _b);
    }

  }

  /*
   * recherche pour savoir si le bateau existe d�j� -1 si le bateau n'existe pas num du bateau � modif
   */
  private int rechercherBateau(final String _batCourant) {
    final ListIterator iter = listeBateaux_.listIterator();
    int i = 0;
    boolean trouve = false;
    System.out.println("bateau � chercher : " + _batCourant);
    while (iter.hasNext() && !trouve) {
      SParametresBateau c = new SParametresBateau();
      c = (SParametresBateau) iter.next();
      System.out.println("bateau observ� : " + c.identification);
      if (c.identification.equalsIgnoreCase(_batCourant)) {
        trouve = true;
        System.out.println("rechercher" + i);
        return i;
      } else {
        i++;
      }
    }
    return -1;
  }

  // affiche la fen�tre de la liste des bateaux
  private void afficherBateaux() {

    if (listeBateaux_ != null) {
      imp_.afficherBateaux();
    }

  }

  public void setnBat(final int _bat) {
    nBat_ = _bat;
  }

  public void internalFrameActivated(final InternalFrameEvent e) {
  // TODO Auto-generated method stub

  }

  public void internalFrameClosed(final InternalFrameEvent e) {
    this.closable = true;
    annuler();

  }

  public void internalFrameClosing(final InternalFrameEvent e) {
  // TODO Auto-generated method stub

  }

  public void internalFrameDeactivated(final InternalFrameEvent e) {
  // TODO Auto-generated method stub

  }

  public void internalFrameDeiconified(final InternalFrameEvent e) {
  // TODO Auto-generated method stub

  }

  public void internalFrameIconified(final InternalFrameEvent e) {
  // TODO Auto-generated method stub

  }

  public void internalFrameOpened(final InternalFrameEvent e) {
  // TODO Auto-generated method stub

  }

  /*
   * initialise les champs nbat - 1 champs vierges sinon valeurs du bateau � modifier
   */
  public void initialiseChamps(final int _bat) {
    if (_bat == -1) {
      tId_.setText("");
      tLong_.setLongueurField(0);
      tLarg_.setLongueurField(0);
      dHoraireDebutHeure_.setDureeField(0);
      dHoraireDebutMinute_.setDureeField(0);
      dHoraireFinHeure_.setDureeField(86400);
      dHoraireFinMinute_.setDureeField(0);
      tChargeT_.setLongueurField(0);
      dChargeHeureG_.setDureeField(0);
      dChargeMinuteG_.setDureeField(0);
      tLegeT_.setLongueurField(0);
      dLegeHeureG_.setDureeField(0);
      dLegeMinuteG_.setDureeField(0);

      tVitesseMontantL_.setLongueurField(0);
      tVitesseDescendantL_.setLongueurField(0);

      tVitesseMontantC_.setLongueurField(0);
      tVitesseDescendantC_.setLongueurField(0);

    } else {

      final SParametresBateau batCourant_ = (SParametresBateau) listeBateaux_.get(nBat_);
      tLong_.setLongueurField(batCourant_.longueur);
      tLarg_.setLongueurField(batCourant_.largeur);
      dHoraireDebutHeure_.setDureeField(Sinavi2TypeBateau.determineHeureSeule(batCourant_.debutNavigation));
      dHoraireDebutMinute_.setDureeField(Sinavi2TypeBateau.determineMinuteSeule(batCourant_.debutNavigation));
      dHoraireFinHeure_.setDureeField(Sinavi2TypeBateau.determineHeureSeule(batCourant_.finNavigation));
      dHoraireFinMinute_.setDureeField(Sinavi2TypeBateau.determineMinuteSeule(batCourant_.finNavigation));
      final int nbCar_ = batCourant_.identification.length() - 1;
      String temp;
      temp = batCourant_.identification;
      // on enleve le dernier caract�re indiquant si un bateau est L�ge ou Charge
      temp = temp.substring(0, temp.length() - 1);

      tId_.setText(temp);

      if (batCourant_.identification.charAt(nbCar_) == 'C') {
        tChargeT_.setLongueurField(batCourant_.tirantDeau);
        dChargeHeureG_.setDureeField(Sinavi2TypeBateau.determineHeureSeule(batCourant_.dureeGeneAdmissible));
        dChargeMinuteG_.setDureeField(Sinavi2TypeBateau.determineMinuteSeule(batCourant_.dureeGeneAdmissible));
        tLegeT_.setLongueurField(0);
        dLegeHeureG_.setDureeField(0);
        dLegeMinuteG_.setDureeField(0);

        tVitesseMontantL_.setLongueurField(0);
        tVitesseDescendantL_.setLongueurField(0);

        tVitesseMontantC_.setLongueurField(batCourant_.vitesseMontantParDefaut * 1000);
        tVitesseDescendantC_.setLongueurField(batCourant_.vitesseDescendantParDefaut * 1000);

      } else {
        tLegeT_.setLongueurField(batCourant_.tirantDeau);
        dLegeHeureG_.setDureeField(Sinavi2TypeBateau.determineHeureSeule(batCourant_.dureeGeneAdmissible));
        dLegeMinuteG_.setDureeField(Sinavi2TypeBateau.determineMinuteSeule(batCourant_.dureeGeneAdmissible));
        tChargeT_.setLongueurField(0);
        dChargeHeureG_.setDureeField(0);
        dChargeMinuteG_.setDureeField(0);

        tVitesseMontantC_.setLongueurField(0);
        tVitesseDescendantC_.setLongueurField(0);

        tVitesseMontantL_.setLongueurField(batCourant_.vitesseMontantParDefaut * 1000);
        tVitesseDescendantL_.setLongueurField(batCourant_.vitesseDescendantParDefaut * 1000);

      }

    }

  }

  public void focusGained(final FocusEvent _e) {

  }

  public void focusLost(final FocusEvent _e) {

    if (_e.getSource() == tId_) {
      // validator
      if (tId_.getText().equalsIgnoreCase("")) {
        lId_.setForeground(Color.RED);

      } else {
        lId_.setForeground(CtuluLibSwing.getDefaultLabelForegroundColor());
      }
    }

  }

}
