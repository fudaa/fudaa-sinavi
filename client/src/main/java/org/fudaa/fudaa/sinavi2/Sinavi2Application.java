/*
 * @file         SinaviApplication.java
 * @creation     2001-05-17
 * @modification $Date: 2006-09-19 15:08:58 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.sinavi2;

import com.memoire.bu.BuApplication;

/**
 * ....
 * 
 * @version $Revision: 1.4 $ $Date: 2006-09-19 15:08:58 $ by $Author: deniger $
 * @author Benoît MANEUVRIER Cette classe instancie la fenêtre principale Sinavi2Implementation
 */
public class Sinavi2Application extends BuApplication {

  /**
   * Sinavi2Application
   */
  public Sinavi2Application() {
    super();
    setImplementation(new Sinavi2Implementation());
  }
}
