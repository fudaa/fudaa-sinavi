/*
 * @file         Sinavi2FilleAddModBateaux.java
 * @creation     2005-09-17
 * @modification $Date: 2006-12-07 17:34:47 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.sinavi2;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.ListIterator;

import javax.swing.JComponent;
import javax.swing.border.EmptyBorder;

import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

import com.memoire.bu.*;
import com.memoire.fu.FuLog;

import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.table.CtuluTableExcelWriter;

import org.fudaa.dodico.corba.sinavi2.SParametresBateau;
import org.fudaa.dodico.corba.sinavi2.SResultatHistorique;
import org.fudaa.dodico.corba.sinavi2.SSimulationSinavi2;

import org.fudaa.ebli.graphe.BGraphe;

/**
 * Cette classe a pour but d'afficher la dur�e de parcours d'un ou plusieurs type de bateaux entre deux gares.
 * 
 * @version $Revision: 1.13 $ $Date: 2006-12-07 17:34:47 $ by $Author: clavreul $
 * @author Fatimatou Ka , Beno�t Maneuvrier
 */
public class Sinavi2ResDureeParcours extends BuInternalFrame implements ActionListener {

  public Sinavi2TableauDureeDeParcours tb_;
  public BuTable table_;
  SResultatHistorique[] listeHistorique_;
  public ArrayList listeSimulations_;
  private final BuLabel lTitre_ = new BuLabel("Dur�e de Parcours");
  private final BuLabel lBlanc_ = new BuLabel("    ");
  private final BuLabel lBlanc2_ = new BuLabel("    ");
  private final BuLabel lBlanc3_ = new BuLabel("    ");
  private final BuLabel lBlanc4_ = new BuLabel("    ");
  private final BuLabel lBlanc5_ = new BuLabel("    ");

  private final BuButton bAnnuler_ = new BuButton(Sinavi2Resource.SINAVI2.getIcon("QUITTER"), "Quitter");

  private final BuButton bAfficher_ = new BuButton(Sinavi2Resource.SINAVI2.getIcon("EDITER"), "Afficher");

  private final BuLabel lItineraire_ = new BuLabel("Itin�raire");
  private final BuLabel lGareDeb_ = new BuLabel("Gare Debut");
  private final BuLabel lGareFin_ = new BuLabel("Gare Fin");
  private final BuLabel lSensNav_ = new BuLabel("Sens Navigation");
  private final BuComboBox cGareDeb_ = new BuComboBox();
  private final BuComboBox cGareFin_ = new BuComboBox();
  private final BuComboBox cSensNav_ = new BuComboBox();

  private final BuLabel lBateaux_ = new BuLabel("Bateaux");
  private final BuLabel lListeBateaux_ = new BuLabel("Liste Bateaux");
  // private BuLabel lListeBateauxSel_=new BuLabel("Liste Bateaux Selectionn�s");
  public BuList liListeBateaux_;
  public BuList liListeBateauxSel_;
  private BuScrollPane sliListeBateaux_;
  private final BuLabel lCrenaux_ = new BuLabel("Cr�naux de Navigation");
  private final BuLabel lHoraireDebut_ = new BuLabel("Horaire de D�but");
  private final BuLabel lHoraireFin_ = new BuLabel("Horaire de Fin");
  private final DureeField dHoraireDebutHeure_ = new DureeField(false, false, true, false, false);
  private final DureeField dHoraireDebutMinute_ = new DureeField(false, false, false, true, false);
  private final DureeField dHoraireFinHeure_ = new DureeField(false, false, true, false, false);
  private final DureeField dHoraireFinMinute_ = new DureeField(false, false, false, true, false);

  private final BuLabel ltypeGraphe_ = new BuLabel("Type Graphe");
  private final BuRadioButton rHistogramme_ = new BuRadioButton("Histogramme");
  private final BuRadioButton rCourbe_ = new BuRadioButton("Courbe");

  private final BuLabel lAxeX_ = new BuLabel("Axe des abscisses");
  private final BuRadioButton rTypeBateaux_ = new BuRadioButton("Type de Bateaux");
  private final BuRadioButton rDureeParcours_ = new BuRadioButton("Dur�e de Parcours");

  private final BuLabel lDonneesAAfficher_ = new BuLabel("Donn�es � Afficher");
  private final BuCheckBox cMin_ = new BuCheckBox("Minimum");
  private final BuCheckBox cMoy_ = new BuCheckBox("Moyenne");
  private final BuCheckBox cMax_ = new BuCheckBox("Maximum");
  private final BuCheckBox cSeuil_ = new BuCheckBox("Seuil");// **pas n�cessaire seulement si seuil !=0

  private final BuTextField tSeuil_ = new BuTextField("0");

  private final BuLabel lSeuil_ = new BuLabel("Seuil");

  /** * panel contenant les boutons, il est plac� en bas */

  private final BuPanel pBoutons_ = new BuPanel();
  /**
   * panel contenant les diff�rents champs de la fen�tre
   */
  private final BuPanel pTitre_ = new BuPanel();

  private final BuPanel pDonnees2_ = new BuPanel();
  private GridBagLayout g2;
  private GridBagConstraints c;
  // private BuCommonImplementation _appli;
  public ArrayList donneesGraph_;
  private ArrayList listeBateaux_;
  public Sinavi2Implementation imp_ = null;
  public Sinavi2TableauResDureeParcours t_ = null;
  public double minCourant_ = 0;
  public double moyCourant_ = 0;
  public double maxCourant_ = 0;
  public double maxiFlotte = 0;
  public double moyFlotte = 0;
  public double miniFlotte = 10000000;
  public boolean comparaison_;
  private String[] simulations;
  protected double[] tabMin;// =new double[0];
  protected double[] tabMoy;// =new double[0];
  protected double[] tabMax;// =new double[0];

  // ---------------------------public Sinavi2FilleAddModBateaux (BuCommonImplementation appli_,LinkedList liste_/*,int
  // bat_*/) {
  public Sinavi2ResDureeParcours(final BuCommonImplementation _appli, final ArrayList _listeSimulationsSinavi2,
      final ArrayList _listeBateaux, final Sinavi2TrajetMng _listeTrajets, final ArrayList _listeGares,
      final boolean _comparaison) {
    super("Dur�e de Parcours", true, true, true, false);
    comparaison_ = _comparaison;
    imp_ = (Sinavi2Implementation) _appli.getImplementation();
    if (comparaison_) {
      simulations = new String[imp_.simulationsSel_.length];
      listeSimulations_ = _listeSimulationsSinavi2;
    }
    if (!comparaison_) {
      // System.out.println("num de simulation"+ imp_.getNumeroSimulationCourante());
      listeHistorique_ = new SResultatHistorique[((SSimulationSinavi2) _listeSimulationsSinavi2.get(imp_
          .getNumeroSimulationCourante())).resultatHistorique.length];
      listeHistorique_ = ((SSimulationSinavi2) _listeSimulationsSinavi2.get(imp_.getNumeroSimulationCourante())).resultatHistorique;
    }
    listeBateaux_ = _listeBateaux;
    bAnnuler_.addActionListener(this);
    bAfficher_.addActionListener(this);
    /*******************************************************************************************************************
     * ------- bAddBat_.addActionListener(this); bSupBat_.addActionListener(this); ------
     ******************************************************************************************************************/
    // rCourbe_.addItemListener(this);
    rCourbe_.addActionListener(this);
    // rHistogramme_.addItemListener(this);
    rHistogramme_.addActionListener(this);
    // rDureeParcours_.addItemListener(this);
    rDureeParcours_.addActionListener(this);
    // rTypeBateaux_.addItemListener(this);
    rTypeBateaux_.addActionListener(this);
    ((JComponent) getContentPane()).setBorder(new EmptyBorder(25, 25, 25, 25));
    pTitre_.add(lTitre_, "center");

    g2 = new GridBagLayout();
    pDonnees2_.setLayout(g2);
    c = new GridBagConstraints();
    c.fill = GridBagConstraints.BOTH;

    ListIterator it;
    c.gridx = 1;
    c.gridy = 2;
    pDonnees2_.add(lItineraire_, c);

    it = _listeGares.listIterator();
    while (it.hasNext()) {
      final String s = (String) it.next();
      cGareDeb_.addItem(s);
      cGareFin_.addItem(s);
    }

    c.gridx = 2;
    c.gridy = 2;
    pDonnees2_.add(lGareDeb_, c);

    c.gridx = 3;
    c.gridy = 2;
    pDonnees2_.add(cGareDeb_, c);

    c.gridx = 4;
    c.gridy = 2;
    pDonnees2_.add(lGareFin_, c);

    c.gridx = 5;
    c.gridy = 2;
    pDonnees2_.add(cGareFin_, c);

    c.gridx = 6;
    c.gridy = 2;
    pDonnees2_.add(lSensNav_, c);

    cSensNav_.addItem("les 2");
    cSensNav_.addItem("Montant");
    cSensNav_.addItem("Avalant");
    c.gridx = 7;
    c.gridy = 2;
    pDonnees2_.add(cSensNav_, c);

    c.gridx = 1;
    c.gridy = 3;
    pDonnees2_.add(lBlanc_, c);

    c.gridx = 1;
    c.gridy = 4;
    pDonnees2_.add(lBateaux_, c);

    c.gridx = 2;
    c.gridy = 4;
    pDonnees2_.add(lListeBateaux_, c);

    it = _listeBateaux.listIterator();
    final String[] bateaux = new String[_listeBateaux.size() + 1];
    bateaux[0] = getFlotte();
    int i = 1;
    while (it.hasNext()) {
      bateaux[i] = ((SParametresBateau) it.next()).identification;
      i++;
    }
    liListeBateaux_ = new BuList(bateaux);

    sliListeBateaux_ = new BuScrollPane(liListeBateaux_);
    final Dimension d = new Dimension();
    d.setSize(37, 102);
    sliListeBateaux_.setPreferredSize(d);
    // sliListeBateaux_.revalidate();
    c.gridx = 3;
    c.gridy = 4;
    pDonnees2_.add(sliListeBateaux_, c);
    c.gridx = 1;
    c.gridy = 5;
    pDonnees2_.add(lBlanc2_, c);

    c.gridx = 1;
    c.gridy = 6;
    pDonnees2_.add(lCrenaux_, c);

    c.gridx = 2;
    c.gridy = 6;
    pDonnees2_.add(lHoraireDebut_, c);

    c.gridx = 3;
    c.gridy = 6;
    pDonnees2_.add(dHoraireDebutHeure_, c);

    c.gridx = 4;
    c.gridy = 6;
    pDonnees2_.add(dHoraireDebutMinute_, c);

    c.gridx = 5;
    c.gridy = 6;
    pDonnees2_.add(lHoraireFin_, c);

    c.gridx = 6;
    c.gridy = 6;
    pDonnees2_.add(dHoraireFinHeure_, c);

    c.gridx = 7;
    c.gridy = 6;
    pDonnees2_.add(dHoraireFinMinute_, c);

    dHoraireDebutHeure_.setDureeField(0);
    dHoraireDebutMinute_.setDureeField(0);
    dHoraireFinHeure_.setDureeField(86400);
    dHoraireFinMinute_.setDureeField(0);

    c.gridx = 1;
    c.gridy = 7;
    pDonnees2_.add(lBlanc3_, c);

    c.gridx = 1;
    c.gridy = 8;
    pDonnees2_.add(ltypeGraphe_, c);

    c.gridx = 2;
    c.gridy = 8;
    pDonnees2_.add(rCourbe_, c);
    rCourbe_.setSelected(true);
    c.gridx = 3;
    c.gridy = 8;
    pDonnees2_.add(rHistogramme_, c);
    rHistogramme_.setSelected(false);

    c.gridx = 1;
    c.gridy = 9;
    pDonnees2_.add(lBlanc4_, c);

    c.gridx = 1;
    c.gridy = 10;
    pDonnees2_.add(lDonneesAAfficher_, c);

    c.gridx = 2;
    c.gridy = 10;
    pDonnees2_.add(cMin_, c);

    c.gridx = 3;
    c.gridy = 10;
    pDonnees2_.add(cMoy_, c);

    c.gridx = 4;
    c.gridy = 10;
    pDonnees2_.add(cMax_, c);

    c.gridx = 5;
    c.gridy = 10;
    pDonnees2_.add(cSeuil_, c);

    c.gridx = 6;
    c.gridy = 10;
    pDonnees2_.add(lSeuil_, c);

    c.gridx = 7;
    c.gridy = 10;
    pDonnees2_.add(tSeuil_, c);

    c.gridx = 1;
    c.gridy = 11;
    pDonnees2_.add(lBlanc5_, c);

    c.gridx = 1;
    c.gridy = 12;
    pDonnees2_.add(lAxeX_, c);

    c.gridx = 2;
    c.gridy = 12;
    pDonnees2_.add(rDureeParcours_, c);
    rDureeParcours_.setSelected(false);

    c.gridx = 3;
    c.gridy = 12;
    pDonnees2_.add(rTypeBateaux_, c);
    rTypeBateaux_.setSelected(true);

    pBoutons_.setLayout(new FlowLayout(FlowLayout.CENTER));
    pBoutons_.add(bAnnuler_);

    pBoutons_.add(bAfficher_);
    getContentPane().add(pTitre_, BorderLayout.NORTH);
    getContentPane().add(pDonnees2_, BorderLayout.CENTER);
    getContentPane().add(pBoutons_, BorderLayout.SOUTH);

    pack();

    setVisible(true);
    // addInternalFrameListener(this);
    imp_.addInternalFrame(this);

  }

  private String getFlotte() {
    return "flotte";
  }

  /**
   * V�rifie la validit� des choix de l'utilisateur.
   */
  private boolean verifParametres() {
	  if (cGareDeb_.getSelectedIndex() == cGareFin_.getSelectedIndex()) {
		  imp_.affMessage("Entrez des num�ros de gare diff�rents.");
		  return false;
	  }
	  else if (cGareDeb_.getSelectedIndex()>= cGareFin_.getSelectedIndex()){
		  imp_.affMessage("   Le num�ro de la gare finale doit �tre sup�rieur � la gare de d�but.  \n   Utilisez le sens Montant.");
		  return false;

	  } else if (liListeBateaux_.getSelectedIndex() == -1) {
		  imp_.affMessage("Selectionnez un type de bateau.");
		  return false;
	  } else if (liListeBateaux_.getSelectedIndex() == -1) {
		  imp_.affMessage("Selectionnez un type de bateau.");
		  return false;
	  }
	  return true;
  }

  public void actionPerformed(final ActionEvent _e) {
    if (_e.getSource() == bAnnuler_) {
      annuler();
    } else if (_e.getSource() == bAfficher_) {

      if (verifParametres()) {
        if (t_ != null) {
          t_.annuler();
        }
        t_ = new Sinavi2TableauResDureeParcours(imp_, comparaison_);
        afficherGraphe();
        imp_.activateInternalFrame(imp_.fillegraphe_);
      }
    }

    else if (_e.getSource() == rHistogramme_) {
      rHistogramme_.setSelected(true);
      rCourbe_.setSelected(false);
      rTypeBateaux_.setSelected(true);
      rDureeParcours_.setSelected(false);
    } else if (_e.getSource() == rCourbe_) {
      rHistogramme_.setSelected(false);
      rCourbe_.setSelected(true);
    } else if (_e.getSource() == rDureeParcours_) {
      if (!rHistogramme_.isSelected()) {
        rDureeParcours_.setSelected(true);
        rTypeBateaux_.setSelected(false);
      } else {
        rHistogramme_.setSelected(false);
        rCourbe_.setSelected(true);
        rDureeParcours_.setSelected(false);
      }
    } else if (_e.getSource() == rTypeBateaux_) {
      rTypeBateaux_.setSelected(true);
      rDureeParcours_.setSelected(false);
      rCourbe_.setSelected(true);
      rHistogramme_.setSelected(false);
    }
  }

  private void annuler() {
    imp_.removeInternalFrame(this);
    imp_.resetFille2ModBateaux();

  }

  private void afficherGraphe() {
    final BGraphe graphe = new BGraphe();
    String str = "graphe\n";
    final String deb = "{\n";
    str += deb;
    str += "titre \"Dur�e de Parcours " + cGareDeb_.getSelectedItem() + " -> " + cGareFin_.getSelectedItem();
    if (!comparaison_) {
      if (cSensNav_.getSelectedIndex() == 1) {
        str += " dans le sens montant\"" + CtuluLibString.LINE_SEP;
      } else if (cSensNav_.getSelectedIndex() == 2) {
        str += " dans le sens avalant \"" + CtuluLibString.LINE_SEP;
      } else {
        str += " dans les deux sens de navigation \"" + CtuluLibString.LINE_SEP;
      }
    } else {
      if (cSensNav_.getSelectedIndex() == 1) {
        str += " dans le sens montant ";
      } else if (cSensNav_.getSelectedIndex() == 2) {
        str += " dans le sens avalant ";
      } else {
        str += " dans les deux sens de navigation ";
      }
      str += "pour le type de bateau " + liListeBateaux_.getSelectedValue() + "\"" + CtuluLibString.LINE_SEP;
    }

    str += "legende oui \n";
    str += "animation non \n";
    str += "marges\n";
    str += deb;
    str += "gauche 80\n";
    str += "droite 120\n";
    str += "haut 50\n";
    str += "bas 30\n";
    str += "}\n";
    if (!comparaison_) {

    } else {
      // ListIterator it=donneesGraph_.listIterator();
      tabMin = new double[imp_.simulationsSel_.length];
      tabMoy = new double[imp_.simulationsSel_.length];
      tabMax = new double[imp_.simulationsSel_.length];

      final String bateauSel = liListeBateaux_.getSelectedValue().toString();

      System.out.println("" + donneesGraph_.size());
      for (int i = 0; i < donneesGraph_.size(); i++) {
        // while(it.hasNext()){
        FuLog.debug("taille de " + i + ((ArrayList) donneesGraph_.get(i)).size());
        rechercheMinMoyMax(bateauSel, (ArrayList) donneesGraph_.get(i));
        tabMin[i] = minCourant_;
        tabMoy[i] = moyCourant_;
        tabMax[i] = maxCourant_;
        System.out.println("apres rec :" + tabMin[i] + "<" + tabMoy[i] + "<" + tabMax[i]);
        // i++;
      }

    }
    double x = maxDureeParcours();
    double y = x / 20;
    double z = x + y;
    System.out.println("x --> " + x + "y -->" + y + "z --> " + z);
    // dur�e de parcours en absice
    if (rDureeParcours_.isSelected()) {
      str += "axe\n";
      str += deb;
      str += "titre \"Dur�e de Parcours \" \n";
      str += "unite \"heure\" \n";
      str += "orientation horizontal\n";
      str += "graduations oui\n";
      str += "conversionHM \n";
      str += "minimum  0.0\n";
      str += "maximum  " + z + CtuluLibString.LINE_SEP;
      str += "pas " + y + CtuluLibString.LINE_SEP;
      str += "}\n";
      str += "axe\n";
      str += deb;
      if (!comparaison_) {
        str += "titre \"type de Bateaux\" ";
        str += "unite \"type\" ";
        str += "orientation vertical\n";
        str += "graduations non\n";
        str += "minimum 0\n";
        str += "maximum 20\n";
      } else {
        str += "titre \"simulations\" ";
        str += "unite \"simulation\" ";
        str += "orientation vertical\n";
        str += "graduations oui\n";
        str += "minimum 0\n";
        str += "maximum 10\n";

        str += "etiquettes \n";
        str += deb;
        final int cpt = 1;
        for (int i = 0; i < imp_.simulationsSel_.length; i++) {
          str += cpt + CtuluLibString.LINE_SEP;
          str += "" + imp_.simulationsSel_[i] + CtuluLibString.LINE_SEP;

          // str+="bat1 \n";
          // str+="bat2 \n";
          /*
           * if(liListeBateaux_.getSelectedIndex()==0){ str+=cpt+CtuluLibString.LINE_SEP; str+="flotte\n";
           * rechercheMinMoyMaxFlotte(); }
           */
        }
        str += "}\n";

      }
      // ----
      str += "}\n";

    }
    // dur�e de parcours en ordonn�es
    else {
      x = maxDureeParcours();
      y = x / 20;
      z = x + y;
      str += "axe\n";
      str += deb;
      if (!comparaison_) {
        str += "titre \"type de Bateaux\" ";
        str += "unite \"type\" ";
        str += "orientation horizontal\n";
        str += "graduations non\n";
        str += "minimum 0\n";
        str += "maximum 20\n";
      } else {
        str += "titre \"simulations\" ";
        str += "unite \"simulation\" ";
        str += "orientation horizontal\n";
        str += "graduations oui\n";

        str += "minimum 0\n";
        str += "maximum 10\n";
        // --
        str += "etiquettes \n";
        str += deb;
        final int cpt = 1;
        for (int i = 0; i < imp_.simulationsSel_.length; i++) {
          str += cpt + CtuluLibString.LINE_SEP;
          str += "" + imp_.simulationsSel_[i] + CtuluLibString.LINE_SEP;

        }
        str += "}\n";
      }
      // -----
      str += "}\n";
      str += "axe\n";
      str += deb;
      str += "titre \"Dur�e de Parcours\" \n";
      str += "unite \"heure\" \n";
      str += "orientation vertical\n";
      str += "graduations oui\n";
      str += "conversionHM \n";
      str += "minimum  0.0\n";
      System.out.println(" z " + z + " y " + y);
      str += "maximum  " + z + CtuluLibString.LINE_SEP;
      str += "pas " + y + CtuluLibString.LINE_SEP;
      str += "}";
    }

    // triBateaux();

    if (cMax_.isSelected()) {
      str += "courbe\n";
      str += deb;
      str += "titre \"max\"" + CtuluLibString.LINE_SEP;
      if (rCourbe_.isSelected()) {
        str += "type courbe\n";
      } else {
        str += "type histogramme\n";
      }
      str += "aspect\n";
      str += " {\n";
      if (rHistogramme_.isSelected()) {
        str += "contour.largeur 1 \n";
        str += "surface.couleur FF0000  \n";
        // str+="contour.couleur FFFFFF \n";
        str += "texte.couleur 000000  \n";
      } else {
        str += "contour.couleur FF0000 \n";
      }
      str += "}\n";
      str += " valeurs\n";
      str += "  {\n";
      if (rTypeBateaux_.isSelected()) {
        for (int k = 0; k < tabMax.length; k++) {
          final int h = k + 1;
          System.out.println("max : " + h + "     " + tabMax[k] + CtuluLibString.LINE_SEP);
          str += h + "     " + tabMax[k] + CtuluLibString.LINE_SEP;
        }
      } else {
        for (int k = 0; k < tabMax.length; k++) {
          final int h = k + 1;
          System.out.println("max2 : " + tabMax[k] + " * " + h + CtuluLibString.LINE_SEP);
          str += tabMax[k] + "     " + h + CtuluLibString.LINE_SEP;
        }

      }
      str += "  }\n";
      str += "}\n";
      str += " \n";
    }

    if (cMoy_.isSelected()) {
      str += "courbe\n";
      str += deb;
      str += "titre \"moy\"" + CtuluLibString.LINE_SEP;
      if (rCourbe_.isSelected()) {
        str += "type courbe\n";
      } else {
        str += "type histogramme\n";
      }
      str += "aspect\n";
      str += deb;

      if (rHistogramme_.isSelected()) {
        str += "contour.largeur 1 \n";
        str += "surface.couleur FF8800 \n";
        // str+="contour.couleur FFFFFF \n";
        str += "texte.couleur 000000 \n";
      } else {
        str += "contour.couleur FF8800\n";
      }
      str += "}\n";
      str += "valeurs\n";
      str += "  {\n";

      if (rTypeBateaux_.isSelected()) {
        for (int k = 0; k < tabMoy.length; k++) {
          final int h = k + 1;
          System.out.println("moy : " + h + "     " + tabMoy[k] + CtuluLibString.LINE_SEP);
          str += h + "     " + tabMoy[k] + CtuluLibString.LINE_SEP;
        }
      } else {
        for (int k = 0; k < tabMoy.length; k++) {
          final int h = k + 1;

          str += tabMoy[k] + "     " + h + CtuluLibString.LINE_SEP;
        }
      }
      str += "  }\n";
      str += " }\n";
      str += CtuluLibString.LINE_SEP;
    }

    if (cMin_.isSelected()) {
      str += "courbe\n";
      str += "{";
      str += "titre \"min\" \n";
      if (rCourbe_.isSelected()) {
        str += "type courbe\n";
      } else {
        str += "type histogramme\n";
      }
      str += "aspect\n";
      str += deb;
      if (rHistogramme_.isSelected()) {
        str += "contour.largeur 1 \n";
        str += "surface.couleur 008000 \n";
        str += "contour.couleur 000000 \n";
        str += "texte.couleur 000000 \n";
      } else {
        str += "contour.couleur 008000\n";
      }
      str += "}\n";

      str += "valeurs\n";
      str += deb;

      if (rTypeBateaux_.isSelected()) {
        for (int k = 0; k < tabMin.length; k++) {
          final int h = k + 1;
          System.out.println("min : " + h + "     " + tabMin[k] + CtuluLibString.LINE_SEP);

          str += h + "     " + tabMin[k] + CtuluLibString.LINE_SEP;

        }

      } else {
        for (int k = 0; k < tabMin.length; k++) {
          final int h = k + 1;
          System.out.println("min : " + h + "     " + tabMin[k] + CtuluLibString.LINE_SEP);
          str += tabMin[k] + "     " + h + CtuluLibString.LINE_SEP;

        }

      }
      str += "}\n";
      str += "}\n";
      str += CtuluLibString.LINE_SEP;
    }

    str += "courbe\n";
    str += deb;
    str += "titre \"\"" + CtuluLibString.LINE_SEP;
    str += "type courbe\n";
    str += "aspect\n";
    str += deb;
    str += "contour.couleur FFFFFF\n";
    str += "surface.couleur FFFFFF\n";
    str += "texte.couleur 000000 \n";
    str += "contour.largeur 0\n";
    str += "}\n";
    str += "valeurs\n";
    str += "  {\n";

    final double z1 = z - y / 2;
    if (rTypeBateaux_.isSelected()) {
      if (!comparaison_) {
        for (int k = 0; k < tabMoy.length; k++) {
          final int h = k + 1;
          str += h + "     " + z1 + "\n   etiquette \n \"" + liListeBateaux_.getSelectedValues()[k] + "\" \n ";
        }

      } else {
        for (int k = 0; k < imp_.simulationsSel_.length; k++) {
          final int h = k + 1;
          str += h + "     " + z1 + "\n   etiquette \n \""
              + ((SSimulationSinavi2) listeSimulations_.get(imp_.simulationsSel_[k])).nomSim + "\" \n ";
        }
      }

    } else {
      if (!comparaison_) {
        for (int k = 0; k < tabMoy.length; k++) {
          final int h = k + 1;
          str += z1 + "     " + h + "\n   etiquette \"" + liListeBateaux_.getSelectedValues()[k] + "\" \n ";
        }

      } else {
        for (int k = 0; k < imp_.simulationsSel_.length; k++) {
          final int h = k + 1;
          str += z1 + "     " + h + "\n   etiquette \""
              + ((SSimulationSinavi2) listeSimulations_.get(imp_.simulationsSel_[k])).nomSim + "\" \n ";
        }
      }
    }
    str += "  }\n";
    str += " }\n";

    if (cSeuil_.isSelected() && !tSeuil_.getValue().equals("0")) {

      final String tempSeuil = Sinavi2Lib.convertirHeureEnCentieme(Double.parseDouble(tSeuil_.getText().replace(',',
          '.')));
      str += " contrainte\n";
      str += deb;
      // a mettre le seuil
      if (rDureeParcours_.isSelected()) {

        str += "titre \"Seuil = " + tSeuil_.getText().replace(',', '.') + "\"" + CtuluLibString.LINE_SEP;
        str += "orientation verticale\n";
        str += " type max\n";
        str += " valeur     " + tempSeuil + CtuluLibString.LINE_SEP;
      } else {
        str += "titre \"Seuil  = " + tSeuil_.getText().replace(',', '.') + "\"" + CtuluLibString.LINE_SEP;
        str += " type max\n";
        str += " valeur     " + tempSeuil + CtuluLibString.LINE_SEP;
      }
      str += "}\n";
      str += " \n";

    }

    str += CtuluLibString.LINE_SEP;

    FuLog.debug("cest la fin");
    str += "  } \"" + CtuluLibString.LINE_SEP;
    graphe.setFluxDonnees(new ByteArrayInputStream(str.getBytes()));
    // Sinavi2FilleGraphe f=new Sinavi2FilleGraphe(imp_,imp_.getInformationsDocument(),graphe);
    imp_.affGraphe2(graphe);
    // f.setGraphe(graphe);

  }

  /**
   * Recherche la dur�e max de parcours prends la plus grandes des simulation en cas de comparaisons.
   */
  private double maxDureeParcours() {
    if (!comparaison_) {
      final ListIterator it = donneesGraph_.listIterator();
      double duree = 0;
      while (it.hasNext()) {
        final Sinavi2TypeDureeDeParcours d = (Sinavi2TypeDureeDeParcours) it.next();
        if (duree < d.getDureeParcours()) {
          duree = d.getDureeParcours();
       }
      }
      System.out.println("max dur�e :" + duree);
      return duree;
    }
    double duree = 0;
    FuLog.debug("donnes graphe size max duree parcours :" + ((ArrayList) donneesGraph_.get(0)).size());
    for (int i = 0; i < donneesGraph_.size(); i++) {
      final ListIterator it = ((ArrayList) donneesGraph_.get(i)).listIterator();

      while (it.hasNext()) {
        final Sinavi2TypeDureeDeParcours d = (Sinavi2TypeDureeDeParcours) it.next();
        if (duree < d.getDureeParcours()) {
          duree = d.getDureeParcours();
        }
      }
    }
    System.out.println("max dur�e :" + duree);
    return duree;

  }

  /**
   * recherche la dur�e de parcours min, moy, max pour.
   * 
   * @param ; _type -> un type de bateau donn�
   * @param ; _donnesGraph -> une simulation donn�e
   */

  public void rechercheMinMoyMax(final String _type/* ,double _min,double _moy,double _max */,
      final ArrayList _donneesGraph) {

    final ListIterator it = _donneesGraph.listIterator();
    minCourant_ = 1000000000;
    maxCourant_ = 0;
    moyCourant_ = 0;
    int i = 0;
    while (it.hasNext()) {
      final Sinavi2TypeDureeDeParcours s = (Sinavi2TypeDureeDeParcours) it.next();
      if (s.typeBateau.equals(_type) || _type.equalsIgnoreCase(getFlotte())) {
        if (s.dureeParcours < minCourant_) {
          minCourant_ = s.dureeParcours;
        }
        if (s.dureeParcours > maxCourant_) {
          maxCourant_ = s.dureeParcours;
        }
        moyCourant_ += s.dureeParcours;
        i++;
      }
    }
    moyCourant_ = moyCourant_ / i;
    System.out.println("recherche " + minCourant_ + "<" + moyCourant_ + "<" + maxCourant_);
  }

  /**
   * recherche la dur�e de parcours min, moy, max pour la flotte enti�re sur la simulation seules ou celles en
   * comparaison.
   */

  public void rechercheMinMoyMaxFlotte() {// a modif
    if (!comparaison_) {
      final ListIterator it = donneesGraph_.listIterator();
      miniFlotte = 1000000000;
      maxiFlotte = 0;
      moyFlotte = 0;
      int i = 0;
      while (it.hasNext()) {
        final Sinavi2TypeDureeDeParcours s = (Sinavi2TypeDureeDeParcours) it.next();
        if (s.dureeParcours < miniFlotte) {
          miniFlotte = s.dureeParcours;
        }
        if (s.dureeParcours > maxiFlotte) {
          maxiFlotte = s.dureeParcours;
        }
        moyFlotte += s.dureeParcours;
        i++;

      }
      moyFlotte = moyFlotte / i;
      // System.out.println("recherche "+minCourant_+"<"+moyCourant_+"<"+maxCourant_);
    } else {
      int j = 0;
      FuLog.debug("donn�es graph taille flotte comp  " + donneesGraph_.size());
      for (int i = 0; i < donneesGraph_.size(); i++) {
        final ListIterator it = ((ArrayList) donneesGraph_.get(i)).listIterator();
        while (it.hasNext()) {
          final Sinavi2TypeDureeDeParcours s = (Sinavi2TypeDureeDeParcours) it.next();
          if (s.dureeParcours < miniFlotte) {
            miniFlotte = s.dureeParcours;
          }
          if (s.dureeParcours > maxiFlotte) {
            maxiFlotte = s.dureeParcours;
          }
          moyFlotte += s.dureeParcours;
          j++;
        }
      }
      moyFlotte = moyFlotte / j;
    }

  }

  /**
   * Cette m�those a pour but de retourner l'heure de d�part d'un type de bateau dans un ouvrage, elle donne donc
   * l'heure de sortie du bateau de la gare de fin. Ainsi avec cette dur�e on la soustrait � celle de d�part pour
   * conna�tre la dur�e de parcours. _cpt : ligne dans le his o� on a trouv� l'heure de d�part afin de ne pas tout
   * parcourir et commencer la recherche � partir de cette endroit _avalantMontant : sens du trajet _numeroBateau :
   * num�ro du bateau effectuant le trajet �tudi� _gare : la gare d'arriv�e _listeHistorique : le fichier his dans nos
   * structures : listeHistorique
   */

  private double dureeFin(final int _cpt, final char _avalantMontant, final int _numeroBateau, final int _gare,
      final SResultatHistorique[] _listeHistorique) {
    if (!comparaison_) {

      for (int i = _cpt; i < listeHistorique_.length; i++) {
        final Sinavi2TypeHistorique s = new Sinavi2TypeHistorique(listeHistorique_[i]);
        if (s.avalantMontant_ == _avalantMontant && s.elementAFranchir_.equals(getG()) && s.numeroElement_ == _gare
            && s.numeroBateau_ == _numeroBateau) {
          return s.heureSortie_;
        }

      }
      return -1;
    }
    for (int i = _cpt; i < _listeHistorique.length; i++) {
      final Sinavi2TypeHistorique s = new Sinavi2TypeHistorique(_listeHistorique[i]);
      if (s.avalantMontant_ == _avalantMontant && s.elementAFranchir_.equals(getG()) && s.numeroElement_ == _gare
          && s.numeroBateau_ == _numeroBateau) {
        // System.out.println("+"+_cpt+" i "+i);
        return s.heureSortie_;
      }
    }
    return -1;

  }

  private String getG() {
    return "G";
  }

  /**
   * indique si la simulation d'indice indice ets selectionn�e.
   */
  private boolean simSel(final int _indice) {
    for (int i = 0; i < imp_.simulationsSel_.length; i++) {
      if (imp_.simulationsSel_[i] == _indice) {
        return true;
      }
    }
    return false;
  }

  /**
   * selctionne les donn�es en cas de comparaison on selectionne les donn�es en fonction des param�tres choisis.
   */

  public void selectionDonneesC() {
	  System.out.println("horaires " + dHoraireDebutHeure_.getDureeField() + " " + dHoraireDebutMinute_.getDureeField());
	  donneesGraph_ = new ArrayList();
	  double duree = 0;
	  int cptEtiquette = 0;
	  for (int i = 0; i < listeSimulations_.size(); i++) {
		  System.out.println("simulation :" + i);
		  if (simSel(i)) {
			  final ArrayList donneesGraphSim = new ArrayList();
			  final SResultatHistorique[] listeTempHis = ((SSimulationSinavi2) listeSimulations_.get(i)).resultatHistorique;
			  simulations[cptEtiquette] = ((SSimulationSinavi2) listeSimulations_.get(i)).nomSim;
			  cptEtiquette++;
			  final int gare = Integer.parseInt((String) cGareDeb_.getSelectedItem());
			  final int garef = Integer.parseInt((String) cGareFin_.getSelectedItem());

			  // les deux sens ca
			  if (cSensNav_.getSelectedIndex() == 0) {
				  for (int j = 0; j < listeTempHis.length; j++) {
					  final Sinavi2TypeHistorique s = new Sinavi2TypeHistorique(/* sh */listeTempHis[j]);
					  if (s.avalantMontant_ == 'M' && s.elementAFranchir_.equals(getG()) && s.numeroElement_ == garef) {
						  if ((((SParametresBateau) listeBateaux_.get(s.numeroTypeBateau_ - 1)).identification
								  .equalsIgnoreCase(liListeBateaux_.getSelectedValue().toString()) && liListeBateaux_
								  .getSelectedIndex() != 0)
								  || (liListeBateaux_.getSelectedIndex() == 0)) {
							  if ((s.heureEntree_ % 86400 >= (dHoraireDebutHeure_.getDureeField() + dHoraireDebutMinute_
									  .getDureeField()))
									  && (s.heureEntree_ % 86400 <= (dHoraireFinHeure_.getDureeField() + dHoraireFinMinute_
											  .getDureeField()))) {
								  duree = s.heureEntree_;
								  final double fin = dureeFin(j, s.avalantMontant_, s.numeroBateau_, gare, listeTempHis);
								  if (fin != -1) {
									  final Sinavi2TypeDureeDeParcours d = new Sinavi2TypeDureeDeParcours((int) (fin - duree),
											  ((SParametresBateau) (listeBateaux_.get(s.numeroTypeBateau_ - 1))).identification);
									  donneesGraphSim.add(d);
								  }
							  }
						  }
					  } else if (s.avalantMontant_ == 'A' && s.elementAFranchir_.equals(getG()) && s.numeroElement_ == gare) {
						  if ((((SParametresBateau) listeBateaux_.get(s.numeroTypeBateau_ - 1)).identification
								  .equalsIgnoreCase(liListeBateaux_.getSelectedValue().toString()) && liListeBateaux_
								  .getSelectedIndex() != 0)
								  || (liListeBateaux_.getSelectedIndex() == 0)) {
							  if ((s.heureEntree_ % 86400 >= (dHoraireDebutHeure_.getDureeField() + dHoraireDebutMinute_
									  .getDureeField()))
									  && (s.heureEntree_ % 86400 <= (dHoraireFinHeure_.getDureeField() + dHoraireFinMinute_
											  .getDureeField()))) {
								  duree = s.heureEntree_;
								  final double fin = dureeFin(j, s.avalantMontant_, s.numeroBateau_, garef, listeTempHis);
								  if (fin != -1) {
									  // FuLog.debug("+1");
									  final Sinavi2TypeDureeDeParcours d = new Sinavi2TypeDureeDeParcours((int) (fin - duree),
											  ((SParametresBateau) (listeBateaux_.get(s.numeroTypeBateau_ - 1))).identification);
									  donneesGraphSim.add(d);
								  }
							  }
						  }
					  }
				  }
			  }

			  else if (cSensNav_.getSelectedIndex() == 1) {
				  // cpt=0;
				  for (int j = 0; j < listeTempHis.length; j++) {
					  // while(it.hasNext()){
					  // cpt++;
					  // SResultatHistorique sh=(SResultatHistorique)it.next();
					  final Sinavi2TypeHistorique s = new Sinavi2TypeHistorique(listeTempHis[j]);
					  if (s.avalantMontant_ == 'M' && s.elementAFranchir_.equals(getG()) && s.numeroElement_ == garef) {
						  if ((((SParametresBateau) listeBateaux_.get(s.numeroTypeBateau_ - 1)).identification
								  .equalsIgnoreCase(liListeBateaux_.getSelectedValue().toString()) && liListeBateaux_
								  .getSelectedIndex() != 0)
								  || (liListeBateaux_.getSelectedIndex() == 0)) {
							  if ((s.heureEntree_ % 86400 >= (dHoraireDebutHeure_.getDureeField() + dHoraireDebutMinute_
									  .getDureeField()))
									  && (s.heureEntree_ % 86400 <= (dHoraireFinHeure_.getDureeField() + dHoraireFinMinute_
											  .getDureeField()))) {
								  duree = s.heureEntree_;
								  /*
								   * listeHistorique_=new
								   * SResultatHistorique[((SSimulationSinavi2)listeSimulations_.get(i)).resultatHistorique.length];
								   * listeHistorique_=((SSimulationSinavi2)listeSimulations_.get(i)).resultatHistorique;
								   */
								  final double fin = dureeFin(j, s.avalantMontant_, s.numeroBateau_, gare, listeTempHis);
								  if (fin != -1) {
									  final Sinavi2TypeDureeDeParcours d = new Sinavi2TypeDureeDeParcours((int) (fin - duree),
											  ((SParametresBateau) (listeBateaux_.get(s.numeroTypeBateau_ - 1))).identification);
									  donneesGraphSim.add(d);
								  }
							  }
						  }
					  }
					  // donneesGraph_.add(donneesGraphSim_);
				  }
			  }
			  // avalant
			  else {
				  // cpt=0;
				  for (int j = 0; j < listeTempHis.length; j++) {
					  // while(it.hasNext()){
					  // cpt++;
					  // SResultatHistorique sh=(SResultatHistorique)it.next();
					  final Sinavi2TypeHistorique s = new Sinavi2TypeHistorique(listeTempHis[j]);
					  if (s.avalantMontant_ == 'A' && s.elementAFranchir_.equals(getG()) && s.numeroElement_ == gare) {
						  if ((((SParametresBateau) listeBateaux_.get(s.numeroTypeBateau_ - 1)).identification
								  .equalsIgnoreCase(liListeBateaux_.getSelectedValue().toString()) && liListeBateaux_
								  .getSelectedIndex() != 0)
								  || (liListeBateaux_.getSelectedIndex() == 0)) {
							  if ((s.heureEntree_ % 86400 >= (dHoraireDebutHeure_.getDureeField() + dHoraireDebutMinute_
									  .getDureeField()))
									  && (s.heureEntree_ % 86400 <= (dHoraireFinHeure_.getDureeField() + dHoraireFinMinute_
											  .getDureeField()))) {
								  duree = s.heureEntree_;
								  /*
								   * listeHistorique_=new
								   * SResultatHistorique[((SSimulationSinavi2)listeSimulations_.get(i)).resultatHistorique.length];
								   * listeHistorique_=((SSimulationSinavi2)listeSimulations_.get(i)).resultatHistorique;
								   */
								  final double fin = dureeFin(/* cpt */j, s.avalantMontant_, s.numeroBateau_, garef, listeTempHis);
								  if (fin != -1) {
									  final Sinavi2TypeDureeDeParcours d = new Sinavi2TypeDureeDeParcours((int) (fin - duree),
											  ((SParametresBateau) (listeBateaux_.get(s.numeroTypeBateau_ - 1))).identification);
									  donneesGraphSim.add(d);
								  }
							  }
						  }
					  }
				  }
			  }
			  donneesGraph_.add(donneesGraphSim);
		  }
	  }
  }

  /**
   * Cette m�thode a pour but de trier les donn�es dans l'ordre de sbateaux selectionn�. Elle n'est plus utilis� car on
   * utilise la recherche des donn�es du tableau
   */

  public void triBateaux() {
    String[] listeOrdreBateaux;
    if (liListeBateaux_.getSelectedIndex() != 0) {
      listeOrdreBateaux = new String[liListeBateaux_.getSelectedValues().length];
    } else {
      listeOrdreBateaux = new String[liListeBateaux_.getSelectedValues().length - 1];
    }
    final ListIterator it = donneesGraph_.listIterator();
    int i = 0;
    for (i = 0; i < listeOrdreBateaux.length; i++) {
      listeOrdreBateaux[i] = "";
    }

    while (it.hasNext()) {
      final Sinavi2TypeDureeDeParcours d = (Sinavi2TypeDureeDeParcours) it.next();
      boolean trouve = false;
      i = 0;
      while (!trouve && i < listeOrdreBateaux.length - 1) {
        if (listeOrdreBateaux[i].equalsIgnoreCase(d.typeBateau)) {
          trouve = true;
        } else {
          i++;
        }
      }
      if (!trouve) {
        listeOrdreBateaux[i] = d.typeBateau;
      }
    }
    for (i = 0; i < listeOrdreBateaux.length; i++) {
      if (!listeOrdreBateaux[i].equalsIgnoreCase(liListeBateaux_.getSelectedValues()[i].toString())) {
        boolean trouve = false;
        int j = i + 1;
        while (j < listeOrdreBateaux.length && !trouve) {
          if (!listeOrdreBateaux[j].equalsIgnoreCase(liListeBateaux_.getSelectedValues()[i].toString())) {
            trouve = true;
            echange(i, j);
          } else {
            j++;
          }
        }
      }

    }

  }

  /**
   * M�thode qui �change les donn�es entre deux colonnes des tableaux de donn�es.
   */
  private void echange(final int _i, final int _j) {
    double temp = tabMin[_i];
    tabMin[_i] = tabMin[_j];
    tabMin[_j] = temp;
    temp = tabMoy[_i];
    tabMoy[_i] = tabMoy[_j];
    tabMoy[_j] = temp;
    temp = tabMax[_i];
    tabMax[_i] = tabMax[_j];
    tabMax[_j] = temp;

  }

  /**
   * selection des donn�es mais pas en comparaison.
   */
  public void selectionDonnees() {
    donneesGraph_ = new ArrayList();

    double duree = 0;
    final int gare = Integer.parseInt((String) cGareDeb_.getSelectedItem());
    final int garef = Integer.parseInt((String) cGareFin_.getSelectedItem());
    // les deux sens ca
    if (cSensNav_.getSelectedIndex() == 0) {
      for (int i = 0; i < listeHistorique_.length; i++) {
        final Sinavi2TypeHistorique s = new Sinavi2TypeHistorique(listeHistorique_[i]);
        if (s.avalantMontant_ == 'M' && s.elementAFranchir_.equals(getG()) && s.numeroElement_ == garef) {
          if ((s.heureEntree_ % 86400 >= (dHoraireDebutHeure_.getDureeField() + dHoraireDebutMinute_.getDureeField()))
              && (s.heureEntree_ % 86400 <= (dHoraireFinHeure_.getDureeField() + dHoraireFinMinute_.getDureeField()))) {
            duree = s.heureEntree_;
            final double fin = dureeFin(i/* cpt */, s.avalantMontant_, s.numeroBateau_, gare, null);
            if (fin != -1) {
              final Sinavi2TypeDureeDeParcours d = new Sinavi2TypeDureeDeParcours((int) (fin - duree),
                  ((SParametresBateau) (listeBateaux_.get(s.numeroTypeBateau_ - 1))).identification);
              donneesGraph_.add(d);
            }
          }
        } else if (s.avalantMontant_ == 'A' && s.elementAFranchir_.equals(getG()) && s.numeroElement_ == gare) {
          if ((s.heureEntree_ % 86400 >= (dHoraireDebutHeure_.getDureeField() + dHoraireDebutMinute_.getDureeField()))
              && (s.heureEntree_ % 86400 <= (dHoraireFinHeure_.getDureeField() + dHoraireFinMinute_.getDureeField()))) {
            duree = s.heureEntree_;
            final double fin = dureeFin(i/* cpt */, s.avalantMontant_, s.numeroBateau_, garef, null);
            if (fin != -1) {
              final Sinavi2TypeDureeDeParcours d = new Sinavi2TypeDureeDeParcours((int) (fin - duree),
                  ((SParametresBateau) (listeBateaux_.get(s.numeroTypeBateau_ - 1))).identification);
              donneesGraph_.add(d);
            }
          }
        }
      }
    }

    // montant
    else if (cSensNav_.getSelectedIndex() == 1) {
      // cpt=0;
      for (int i = 0; i < listeHistorique_.length; i++) {
        final Sinavi2TypeHistorique s = new Sinavi2TypeHistorique(listeHistorique_[i]);
        if (s.avalantMontant_ == 'M' && s.elementAFranchir_.equals(getG()) && s.numeroElement_ == garef) {
          if ((s.heureEntree_ % 86400 >= (dHoraireDebutHeure_.getDureeField() + dHoraireDebutMinute_.getDureeField()))
              && (s.heureEntree_ % 86400 <= (dHoraireFinHeure_.getDureeField() + dHoraireFinMinute_.getDureeField()))) {
            duree = s.heureEntree_;
            final double fin = dureeFin(i/* cpt */, s.avalantMontant_, s.numeroBateau_, gare, null);
            if (fin != -1) {
              final Sinavi2TypeDureeDeParcours d = new Sinavi2TypeDureeDeParcours((int) (fin - duree),
                  ((SParametresBateau) (listeBateaux_.get(s.numeroTypeBateau_ - 1))).identification);
              donneesGraph_.add(d);
            }
          }
        }
      }
    }
    // avalant
    else {
      // cpt=0;
      for (int i = 0; i < listeHistorique_.length; i++) {
        final Sinavi2TypeHistorique s = new Sinavi2TypeHistorique(listeHistorique_[i]);
        if (s.avalantMontant_ == 'A' && s.elementAFranchir_.equals(getG()) && s.numeroElement_ == gare) {
          if ((s.heureEntree_ % 86400 >= (dHoraireDebutHeure_.getDureeField() + dHoraireDebutMinute_.getDureeField()))
              && (s.heureEntree_ % 86400 <= (dHoraireFinHeure_.getDureeField() + dHoraireFinMinute_.getDureeField()))) {
            duree = s.heureEntree_;
            final double fin = dureeFin(i/* cpt */, s.avalantMontant_, s.numeroBateau_, garef, null);
            if (fin != -1) {
              final Sinavi2TypeDureeDeParcours d = new Sinavi2TypeDureeDeParcours((int) (fin - duree),
                  ((SParametresBateau) (listeBateaux_.get(s.numeroTypeBateau_ - 1))).identification);
              donneesGraph_.add(d);
            }
          }

        }
      }
    }

  }

  /**
   * Cette classe doit afficher les donn�es correspondant � la recherhe effectu�e.
   */

  public class Sinavi2TableauResDureeParcours extends BuInternalFrame implements ActionListener {

    private final BuLabel lTitre1_ = new BuLabel("Affichage de la dur�e de parcours");

    private final BuButton bAnnuler_ = new BuButton(Sinavi2Resource.SINAVI2.getIcon("QUITTER"), "Quitter");
    private final BuButton bImprimer_ = new BuButton(Sinavi2Resource.SINAVI2.getIcon("IMPRIMER"), "Imprimer");

    /** * panel contenant les boutons, il est plac� en bas. */

    private final BuPanel pBoutons1_ = new BuPanel();
    private final BuPanel pTitre1_ = new BuPanel();
    private final BuPanel pDonnees_ = new BuPanel();

    public Sinavi2TableauDureeDeParcours tb1_;
    public BuScrollPane scrollPane_;
    public BuTable table1_;

    // public boolean comparaison_;
    public Sinavi2TableauResDureeParcours(final BuCommonImplementation _appli, final boolean _comparaison) {
      super("Affichage de la dur�e de parcours", true, true, true, false);
      pTitre1_.add(lTitre1_);
      // comparaison_=_comparaison;
      tableauduree();

      table1_.setRowSelectionAllowed(true);
      table1_.setColumnSelectionAllowed(false);
      table1_.setLocation(20, 20);
      table1_.getColumnModel().getColumn(0).setPreferredWidth(175);
      table1_.getColumnModel().getColumn(1).setPreferredWidth(130);
      table1_.getColumnModel().getColumn(2).setPreferredWidth(140);
      table1_.getColumnModel().getColumn(3).setPreferredWidth(150);
      scrollPane_ = new BuScrollPane(table1_);
      pDonnees_.add(scrollPane_);
      table1_.setAutoscrolls(true);
      // pDonnees_.add(table1_);

      imp_ = (Sinavi2Implementation) _appli.getImplementation();

      bAnnuler_.addActionListener(this);
      bImprimer_.addActionListener(this);
      /*
       * if(!imp2_.isPermettreModif()){ }
       */

      ((JComponent) getContentPane()).setBorder(new EmptyBorder(25, 25, 25, 25));

      pBoutons1_.setLayout(new FlowLayout(FlowLayout.CENTER));
      pBoutons1_.add(bAnnuler_);
      pBoutons1_.add(bImprimer_);
      getContentPane().add(pTitre1_, BorderLayout.NORTH);
      getContentPane().add(pDonnees_, BorderLayout.CENTER);
      getContentPane().add(pBoutons1_, BorderLayout.SOUTH);

      // pack();
      setLocation(40, 40);

      /** *reinitialisation des valeurs* */

      setVisible(true);
    }

    public void tableauduree() {
      int row = 0;
      String s = new String();

      if (tb1_ == null) {
        tb1_ = new Sinavi2TableauDureeDeParcours();
      }
      if (!comparaison_) {
        selectionDonnees();
        // if(liListeBateaux_.getSelectedIndex()!=0 && liListeBateaux_.getS){
        final Object[] bateauxSel = liListeBateaux_.getSelectedValues();
        tabMin = new double[bateauxSel.length];
        tabMoy = new double[bateauxSel.length];
        tabMax = new double[bateauxSel.length];
        for (int i = 0; i < bateauxSel.length; i++) {
          if (!bateauxSel[i].toString().equals("flotte")) {
            rechercheMinMoyMax(bateauxSel[i].toString(), donneesGraph_);
            tb1_.setValueAt(bateauxSel[i].toString(), row, 0);
            tabMin[i] = minCourant_;
            tabMoy[i] = moyCourant_;
            tabMax[i] = maxCourant_;
            s = Sinavi2Lib.determineHeureString((int) (minCourant_ * 3600));
            tb1_.setValueAt(s, row, 1);

            // s=String.valueOf(SinaviLib.determineHeureString.conversionDeuxChiffres(tabMoy[i]));
            s = String.valueOf(Sinavi2Lib.determineHeureString((int) (moyCourant_ * 3600)));
            tb1_.setValueAt(s, row, 2);

            // s=String.valueOf(SinaviLib.determineHeureString.conversionDeuxChiffres(tabMax[i]));
            s = String.valueOf(Sinavi2Lib.determineHeureString((int) (maxCourant_ * 3600)));
            tb1_.setValueAt(s, row, 3);
            row++;
            tb1_.setNbRow(row);

          } else {
            rechercheMinMoyMaxFlotte();
            s = "flotte";
            tb1_.setValueAt(s, row, 0);
            s = String.valueOf(Sinavi2Lib.determineHeureString((int) (miniFlotte * 3600)));
            tb1_.setValueAt(s, row, 1);
            tabMin[i] = miniFlotte;
            tabMoy[i] = moyFlotte;
            tabMax[i] = maxiFlotte;
            // s=String.valueOf(SinaviLib.determineHeureString.conversionDeuxChiffres(tabMoy[i]));
            s = String.valueOf(Sinavi2Lib.determineHeureString((int) (moyFlotte * 3600)));
            tb1_.setValueAt(s, row, 2);

            // s=String.valueOf(SinaviLib.determineHeureString.conversionDeuxChiffres(tabMax[i]));
            s = String.valueOf(Sinavi2Lib.determineHeureString((int) (maxiFlotte * 3600)));
            tb1_.setValueAt(s, row, 3);
            row++;
            tb1_.setNbRow(row);

          }
        }
      } else {
        selectionDonneesC();
        /* double[] */tabMin = new double[imp_.simulationsSel_.length];
        /* double[] */tabMoy = new double[imp_.simulationsSel_.length];
        /* double[] */tabMax = new double[imp_.simulationsSel_.length];

        if (liListeBateaux_.getSelectedIndex() == 0) {
          final String bateauSel = liListeBateaux_.getSelectedValue().toString();
          for (int i = 0; i < donneesGraph_.size(); i++) {
            // while(it.hasNext()){
            FuLog.debug("taille de " + i + ((ArrayList) donneesGraph_.get(i)).size());
            rechercheMinMoyMax(bateauSel, (ArrayList) donneesGraph_.get(i));
            tabMin[i] = minCourant_;
            tabMoy[i] = moyCourant_;
            tabMax[i] = maxCourant_;

            s = ((SSimulationSinavi2) listeSimulations_.get(imp_.simulationsSel_[i])).nomSim + ": " + bateauSel;
            tb1_.setValueAt(s, row, 0);
            s = String.valueOf(Sinavi2Lib.determineHeureString((int) (tabMin[i] * 3600)));
            FuLog.debug(s);
            tb1_.setValueAt(s, row, 1);

            // s=String.valueOf(SinaviLib.determineHeureString.conversionDeuxChiffres(tabMoy[i]));
            s = String.valueOf(Sinavi2Lib.determineHeureString((int) (tabMoy[i] * 3600)));
            tb1_.setValueAt(s, row, 2);

            // s=String.valueOf(SinaviLib.determineHeureString.conversionDeuxChiffres(tabMax[i]));
            s = String.valueOf(Sinavi2Lib.determineHeureString((int) (tabMax[i] * 3600)));
            tb1_.setValueAt(s, row, 3);
            row++;
            tb1_.setNbRow(row);
          }
        } else {

          final String bateauSel = liListeBateaux_.getSelectedValue().toString();
          for (int i = 0; i < donneesGraph_.size(); i++) {
            FuLog.debug("" + ((ArrayList) donneesGraph_.get(i)).size());
            rechercheMinMoyMax(bateauSel, (ArrayList) donneesGraph_.get(i));
            tabMin[i] = minCourant_;
            tabMoy[i] = moyCourant_;
            tabMax[i] = maxCourant_;
            System.out.println("apres rec :" + tabMin[i] + "<" + tabMoy[i] + "<" + tabMax[i]);
            s = ((SSimulationSinavi2) listeSimulations_.get(imp_.simulationsSel_[i])).nomSim + ": " + bateauSel;

            tb1_.setValueAt(s, row, 0);

            // s=String.valueOf(SinaviLib.determineHeureString.conversionDeuxChiffres(tabMin[i]));
            s = String.valueOf(Sinavi2Lib.determineHeureString((int) (tabMin[i] * 3600)));
            FuLog.debug("test : " + s);
            tb1_.setValueAt(s, row, 1);

            // s=String.valueOf(SinaviLib.determineHeureString.conversionDeuxChiffres(tabMoy[i]));
            s = String.valueOf(Sinavi2Lib.determineHeureString((int) (tabMoy[i] * 3600)));
            tb1_.setValueAt(s, row, 2);

            // s=String.valueOf(SinaviLib.determineHeureString.conversionDeuxChiffres(tabMax[i]));
            s = String.valueOf(Sinavi2Lib.determineHeureString((int) (tabMax[i] * 3600)));
            tb1_.setValueAt(s, row, 3);
            row++;
            tb1_.setNbRow(row);
          }
        }
      }
      table1_ = new BuTable(tb1_.data_, tb1_.nomCol());
      table1_.repaint();

    }

    public void annuler() {
      imp_.removeInternalFrame(this);
      imp_.resetFille2AffBateaux();
    }

    public void actionPerformed(final ActionEvent _e) {
      if (_e.getSource() == bAnnuler_) {
        annuler();

      }

      else if (_e.getSource() == bImprimer_) {
        printFile();
        // test.write()
      }

    }

    private void printFile() {
      final File s = imp_.enregistrerXls();
      if (s == null) {
        return;
      }
      final Sinavi2TableauDureeDeParcours tbtemp = new Sinavi2TableauDureeDeParcours();
      tbtemp.data_ = new Object[tb1_.getRowCount() + 2][tb1_.getColumnCount()];
      tbtemp.initNomCol(1);
      if (!comparaison_) {
        tbtemp.setColumnName("Dur�e de parcours des types de bateau s�lectionn�s ", 0);
      } else {
        tbtemp.setColumnName("Comparaison de la dur�e de parours de  " + liListeBateaux_.getSelectedValue().toString(),
            0);
      }
      for (int i = 1; i < tb1_.getColumnCount(); i++) {
        tbtemp.setColumnName(" ", i);
      }

      tbtemp.setNbRow(tb1_.getRowCount() + 2);
      for (int i = 2; i <= (tb1_.getRowCount() + 1); i++) {
        for (int j = 0; j < tb1_.getColumnCount(); j++) {
          tbtemp.data_[i][j] = tb1_.data_[i - 2][j];
        }

      }
      final CtuluTableExcelWriter test = new CtuluTableExcelWriter(tbtemp, s);

      try {
        test.write(null);

      } catch (final RowsExceededException e) {
        e.printStackTrace();
      } catch (final WriteException e) {
        e.printStackTrace();
      } catch (final IOException e) {
        e.printStackTrace();
      }
    }

    public void affMessage(final String _t) {
      new BuDialogMessage(imp_.getApp(), imp_.getInformationsSoftware(), _t).activate();
    }

  }

  public void affMessage(final String _t) {
    new BuDialogMessage(imp_.getApp(), imp_.getInformationsSoftware(), _t).activate();
  }

}
