package org.fudaa.fudaa.sinavi2;

import java.text.NumberFormat;
import java.util.Comparator;
import java.util.List;

import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluLibString;

import org.fudaa.dodico.corba.sinavi2.SDateHeure;
import org.fudaa.dodico.corba.sinavi2.SHeure;
import org.fudaa.dodico.corba.sinavi2.SParametresBateau;

/**
 * @author maneuvrier
 * @version ${Id}
 */
public final class Sinavi2Lib {

  public static class SHeureComparator implements Comparator {

    public int compare(final Object _o1, final Object _o2) {
      final SHeure h1 = (SHeure) _o1;
      final SHeure h2 = (SHeure) _o2;
      int res = h1.heure - h2.heure;
      if (res == 0) {
        res = h1.minutes - h2.minutes;
      }
      return res;
    }

  }
  public static class SDateHeureComparator implements Comparator {
    final SHeureComparator hComp_ = new SHeureComparator();

    public int compare(final Object _o1, final Object _o2) {
      final SDateHeure h1 = (SDateHeure) _o1;
      final SDateHeure h2 = (SDateHeure) _o2;
      int res = h1.date - h2.date;
      if (res == 0) {
        res = hComp_.compare(h1.heure, h2.heure);
      }
      return res;
    }

  }

  public static int getBateau(final List _list, final String _id) {
    if (_id != null && _list != null) {
      for (int i = _list.size() - 1; i >= 0; i--) {
        if (((SParametresBateau) _list.get(i)).identification.equals(_id)) {
          return i;
        }
      }
    }

    return -1;
  }

  private Sinavi2Lib() {

  }

  public static String getS(final String _chFrancais) {
    return Sinavi2Resource.SINAVI2.getString(_chFrancais);
  }

  public static double conversionDeuxChiffres(final double value) {
    final NumberFormat formatter = CtuluLib.getDecimalFormat();
    formatter.setMaximumFractionDigits(2);
    formatter.setMinimumFractionDigits(2);
    final String s = formatter.format(value);
    return Double.parseDouble(s);
  }

  public static String determineHeureString(final int _nbSecondes) {

    /*
     * String s2=""+_nbSecondes; int nbS=Integer.parseInt(s2);
     */
    final int heure = _nbSecondes / 3600;
    final int minute = _nbSecondes / 60 - heure * 60;
    String s = "";
    if (minute > 9) {
      s = heure + CtuluLibString.DOT + minute;
    } else {
      s = heure + ".0" + minute;
    }
    System.out.println("nb seconde :" + _nbSecondes + " -> " + s);
    return s;
  }

  public static String convertirHeureEnCentieme(final double _val) {
    System.out.println("val " + _val);
    final String temp = String.valueOf(_val);
    // temp=temp.replace(',','.');
    System.out.println("temp " + temp);
    final int indice = temp.indexOf('.');
    String min = temp.substring(indice + 1);
    if (min.length() == 1) {
      min = min + "0";
    }
    int minutes = Integer.parseInt(min);
    System.out.println("minutes hm" + minutes);
    minutes = minutes * 100 / 60;
    System.out.println("minutes centieme" + minutes);
    System.out.println("res :" + temp.substring(0, indice) + minutes);
    return temp.substring(0, indice) + CtuluLibString.DOT + minutes;

  }

}
