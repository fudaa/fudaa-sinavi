/*
 * @file         Sinavi2FilleAddModBateaux.java
 * @creation     2005-09-17
 * @modification $Date: 2007-05-04 14:01:06 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.sinavi2;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.util.ArrayList;
import java.util.ListIterator;

import javax.swing.JComponent;
import javax.swing.border.EmptyBorder;
import javax.swing.event.InternalFrameEvent;
import javax.swing.event.InternalFrameListener;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuCommonImplementation;
import com.memoire.bu.BuDialogMessage;
import com.memoire.bu.BuDialogWarning;
import com.memoire.bu.BuInternalFrame;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuTextField;

import org.fudaa.dodico.corba.sinavi2.SParametresBateau;

/**
 * impl�mentation d'une fen�tre interne permettant de saisir les param�tres pour les bateaux
 * 
 * @version $Revision: 1.5 $ $Date: 2007-05-04 14:01:06 $ by $Author: deniger $
 * @author Fatimatou Ka , Beno�t Maneuvrier
 */
public class Sinavi2FilleAddModBateauxold extends BuInternalFrame implements ActionListener, InternalFrameListener,
    FocusListener {

  /** **************************liste des boutons***************************** */

  // private BuCommonInterface _appli;

  private final BuLabel lBlanc_ = new BuLabel("    ");
  private final BuLabel lBlanc2_ = new BuLabel("    ");
  private final BuLabel lBlanc3_ = new BuLabel("    ");
  private final BuLabel lBlanc4_ = new BuLabel("    ");
  private final BuLabel lBlanc5_ = new BuLabel("           ");
  private final BuLabel lTitre_ = new BuLabel("Type de Bateau");

  /** bouton pour annuler */
  private final BuButton bAnnuler_ = new BuButton(Sinavi2Resource.SINAVI2.getIcon("QUITTER"), "Quitter");

  /** sauver */
  private final BuButton bSauver_ = new BuButton(Sinavi2Resource.SINAVI2.getIcon("VALIDER"), "Sauver");

  /** afficher les bateaux */
  private final BuButton bAfficher_ = new BuButton(Sinavi2Resource.SINAVI2.getIcon("EDITER"), "Afficher");

  /** caract�ristiques du bateaux en haut */
  private final BuLabel lId_ = new BuLabel("Identification");
  private final BuTextField tId_ = new BuTextField("");

  private final BuLabel lLong_ = new BuLabel("Longueur");
  private final LongueurField tLong_ = new LongueurField(false, true, true);

  private final BuLabel lLarg_ = new BuLabel("Largeur");
  private final LongueurField tLarg_ = new LongueurField(false, true, true);

  /** horaire de navigation milieu */

  private final BuLabel lHoraireDeNavigation_ = new BuLabel("Horaires de navigation");
  private final BuLabel lHoraireDebut_ = new BuLabel("Horaires de D�but   ");
  private final BuLabel lHoraireFin_ = new BuLabel("Horaires de Fin");

  // private DureeField dHoraireDebut_ = new DureeField(false, false, true, true, false);

  private final DureeField dHoraireDebutHeure_ = new DureeField(false, false, true, false, false);
  private final DureeField dHoraireDebutMinute_ = new DureeField(false, false, false, true, false);

  // private DureeField dHoraireFin_ = new DureeField(false, false, true, true, false);

  private final DureeField dHoraireFinHeure_ = new DureeField(false, false, true, false, false);
  private final DureeField dHoraireFinMinute_ = new DureeField(false, false, false, true, false);

  // -----------------------------------------------------------------------------
  /** **utile pour les vitesses** */
  private final BuLabel lVitesseParDefaut_ = new BuLabel("Vitesses par D�faut de navigation");
  private final BuLabel lVitesseMontant_ = new BuLabel("Vitesse des Montants");
  private final BuLabel lVitesseDescendant_ = new BuLabel("Vitesse des Avalants");
  private final LongueurField tVitesseMontantC_ = new LongueurField(true, true, false);
  private final LongueurField tVitesseMontantL_ = new LongueurField(true, true, false);
  private final LongueurField tVitesseDescendantC_ = new LongueurField(true, true, false);
  private final LongueurField tVitesseDescendantL_ = new LongueurField(true, true, false);

  // ------------------------------------------------------------------------------
  /** charg� l�ges tirant d'eau gene admissible 3 colonnes */

  private final BuLabel lCharge_ = new BuLabel("Charge");

  private final BuLabel lLege_ = new BuLabel("L�ge");

  private final BuLabel lTirantDEau_ = new BuLabel("Tirant d'eau");

  private final BuLabel lGeneAdmissible_ = new BuLabel("G�ne Admissible");

  private final LongueurField tChargeT_ = new LongueurField(false, true, true);

  private final DureeField dChargeHeureG_ = new DureeField(false, false, true, false, false);
  private final DureeField dChargeMinuteG_ = new DureeField(false, false, false, true, false);

  private final LongueurField tLegeT_ = new LongueurField(false, true, true);
  private final DureeField dLegeHeureG_ = new DureeField(false, false, true, false, false);
  private final DureeField dLegeMinuteG_ = new DureeField(false, false, false, true, false);

  /** **gerer la liste des bateaux**** */
  // ---------------------- public LinkedList listeBateaux_;
  public ArrayList listeBateaux_;
  public static int nbBateaux_;

  /** **bateau pour les champs en cas de modifications*** */
  // ---------------------public SParametresBateau batCourant_;
  private int nbat_ = -1;// -1 a lorigine

  /** * panel contenant les boutons, il est plac� en bas */

  private final BuPanel pBoutons_ = new BuPanel();
  /**
   * panel contenant les diff�rents champs de la fen�tre
   */
  private final BuPanel pTitre_ = new BuPanel();

  private final BuPanel pDonnees2_ = new BuPanel();

  /**
   * une instance de SinaviImplementation
   */
  // private BuCommonImplementation app_;
  /**
   * Constructeur
   * 
   * @param _app une instance de SinaviImplementation
   */

  // private BuCommonImplementation _appli;
  public Sinavi2Implementation imp_ = null;

  // ---------------------------public Sinavi2FilleAddModBateaux (BuCommonImplementation appli_,LinkedList liste_/*,int
  // bat_*/) {
  public Sinavi2FilleAddModBateauxold(final BuCommonImplementation _appli, final ArrayList _liste/* ,int bato_ */) {
    // appli_.getApp();
    // JOptionPane.showMessageDialog(null,"Charmout");
    super("Type de Bateaux", true, true, true, false);
    // nbat_=bato_;
    // _appli=appli_;
    /** ************************* nbat_=bat_;************************** */
    listeBateaux_ = _liste;

    // listeBateaux_ = new LinkedList(liste_);
    // liste_=listeBateaux_;/*ajou*/
    // JOptionPane.showMessageDialog(null,"Charmout");

    imp_ = (Sinavi2Implementation) _appli.getImplementation();

    // app_= _app;
    // Ajout des listener aux boutons
    bSauver_.addActionListener(this);
    bAnnuler_.addActionListener(this);
    bAfficher_.addActionListener(this);
    tId_.setToolTipText("Entrez l'identifiant du type de bateau");

    // remplissage de la fenetre

    // setSize(200,400);
    ((JComponent) getContentPane()).setBorder(new EmptyBorder(25, 25, 25, 25));
    // ((JComponent) getContentPane()).setLayout(null);
    /** creation de la fenetre */

    // Container contenu=getContentPane();
    // BuGridBagLayout g=new BuGridBagLayout();
    /*
     * GridBagLayout g1=new GridBagLayout(); contenu.setLayout(g1); GridBagConstraints c1=new GridBagConstraints();
     */
    // c1.fill =c1.BOTH;
    // g1.addLayoutComponent("yes",lHoraireDeNavigation_);
    // pDonnees_.setLayout(g1);
    // lHoraireDebut_.setBounds(50,50,10,20);
    /*
     * GridLayout g1=new GridLayout(3,2,5,2); pDonnees_.setLayout(g1); pDonnees_.add(lId_); pDonnees_.add(tId_);
     * pDonnees_.add(lLong_); pDonnees_.add(tLong_); pDonnees_.add(lLarg_); pDonnees_.add(tLarg_);
     */
    pTitre_.add(lTitre_, "center");

    final GridBagLayout g2 = new GridBagLayout();
    pDonnees2_.setLayout(g2);
    final GridBagConstraints c = new GridBagConstraints();
    c.fill = GridBagConstraints.BOTH;

    /*
     * c.gridx =2; c.gridy =0; pDonnees2_.add(lTitre_);
     */

    c.gridx = 1;
    c.gridy = 2;
    pDonnees2_.add(lId_, c);

    c.gridx = 2;
    c.gridy = 2;
    pDonnees2_.add(tId_, c);
    tId_.addFocusListener(this);

    /** ***********test**************** */
    /*
     * c.gridx = 3; c.gridy = 0; pDonnees2_.add(lBlanc_,c);
     */

    c.gridx = 1;
    c.gridy = 3;
    pDonnees2_.add(lLong_, c);

    c.gridx = 2;
    c.gridy = 3;
    pDonnees2_.add(tLong_, c);
    tLong_.addFocusListener(this);
    c.gridx = 1;
    c.gridy = 4;
    pDonnees2_.add(lLarg_, c);

    c.gridx = 2;
    c.gridy = 4;
    pDonnees2_.add(tLarg_, c);
    tLarg_.addFocusListener(this);

    c.gridx = 0;
    c.gridy = 5;
    pDonnees2_.add(lBlanc_, c);

    /*
     * c.gridx =1; c.gridy =5; pDonnees2_.add(lHoraireDeNavigation_,c);
     */
    c.gridx = 0;
    c.gridy = 6;
    pDonnees2_.add(lHoraireDeNavigation_, c);
    c.gridx = 1;
    c.gridy = 7;
    pDonnees2_.add(lHoraireDebut_, c);

    /*
     * c.gridx =2; c.gridy =7; pDonnees2_.add(dHoraireDebut_,c);
     */

    c.gridx = 2;
    c.gridy = 7;
    pDonnees2_.add(dHoraireDebutHeure_, c);
    dHoraireDebutHeure_.addFocusListener(this);

    c.gridx = 3;
    c.gridy = 7;
    pDonnees2_.add(dHoraireDebutMinute_, c);
    dHoraireDebutMinute_.addFocusListener(this);

    c.gridx = 1;
    c.gridy = 8;
    pDonnees2_.add(lHoraireFin_, c);
    /*
     * c.gridx =2; c.gridy =8; pDonnees2_.add(dHoraireFin_,c);
     */

    c.gridx = 2;
    c.gridy = 8;
    pDonnees2_.add(dHoraireFinHeure_, c);
    dHoraireFinHeure_.addFocusListener(this);
    c.gridx = 3;
    c.gridy = 8;
    pDonnees2_.add(dHoraireFinMinute_, c);

    dHoraireFinMinute_.addFocusListener(this);

    c.gridx = 0;
    c.gridy = 9;
    pDonnees2_.add(lBlanc2_, c);

    c.gridx = 2;
    c.gridy = 10;
    pDonnees2_.add(lCharge_, c);
    c.gridx = 5;
    c.gridy = 10;
    pDonnees2_.add(lLege_, c);

    c.gridx = 1;
    c.gridy = 11;
    pDonnees2_.add(lTirantDEau_, c);
    c.gridx = 2;
    c.gridy = 11;
    pDonnees2_.add(tChargeT_, c);
    tChargeT_.addFocusListener(this);
    c.gridx = 5;
    c.gridy = 11;
    pDonnees2_.add(tLegeT_, c);
    tLegeT_.addFocusListener(this);
    c.gridx = 1;
    c.gridy = 12;
    pDonnees2_.add(lGeneAdmissible_, c);
    c.gridx = 2;
    c.gridy = 12;
    pDonnees2_.add(dChargeHeureG_, c);
    dChargeHeureG_.addFocusListener(this);
    c.gridx = 3;
    c.gridy = 12;
    pDonnees2_.add(dChargeMinuteG_, c);
    dChargeMinuteG_.addFocusListener(this);
    c.gridx = 4;
    c.gridy = 12;
    pDonnees2_.add(lBlanc5_, c);

    c.gridx = 5;
    c.gridy = 12;
    pDonnees2_.add(dLegeHeureG_, c);
    dLegeHeureG_.addFocusListener(this);
    c.gridx = 6;
    c.gridy = 12;
    pDonnees2_.add(dLegeMinuteG_, c);
    dLegeMinuteG_.addFocusListener(this);

    // -----------------------------------------------------

    /** **utile pour les manoeuvres** */
    c.gridx = 0;
    c.gridy = 13;
    pDonnees2_.add(lBlanc3_, c);

    c.gridx = 0;
    c.gridy = 14;
    pDonnees2_.add(lVitesseParDefaut_, c);

    c.gridx = 1;
    c.gridy = 15;
    pDonnees2_.add(lVitesseMontant_, c);
    c.gridx = 2;
    c.gridy = 15;
    pDonnees2_.add(tVitesseMontantC_, c);
    tVitesseMontantC_.addFocusListener(this);
    c.gridx = 5;
    c.gridy = 15;
    pDonnees2_.add(tVitesseMontantL_, c);
    tVitesseMontantL_.addFocusListener(this);
    c.gridx = 1;
    c.gridy = 16;
    pDonnees2_.add(lVitesseDescendant_, c);
    c.gridx = 2;
    c.gridy = 16;
    pDonnees2_.add(tVitesseDescendantC_, c);
    tVitesseDescendantC_.addFocusListener(this);
    c.gridx = 5;
    c.gridy = 16;
    pDonnees2_.add(tVitesseDescendantL_, c);
    tVitesseDescendantL_.addFocusListener(this);
    c.gridx = 0;
    c.gridy = 17;
    pDonnees2_.add(lBlanc4_, c);

    // -------------------------------------------------------

    // pDonnees_.add(lHoraireDeNavigation_);

    // pDonnees_.setLayout(g1);

    // lHoraireDebut_.setBounds(150,150,10,20);
    /*
     * pDonnees_.add(lHoraireDebut_); pDonnees_.add(dHoraireDebut_); pDonnees_.add(lHoraireFin_);
     * pDonnees_.add(dHoraireFin_);
     */
    pBoutons_.setLayout(new FlowLayout(FlowLayout.CENTER));
    pBoutons_.add(bAnnuler_);
    pBoutons_.add(bSauver_);
    pBoutons_.add(bAfficher_);
    // getContentPane().add(pDonnees_, BorderLayout.NORTH);
    getContentPane().add(pTitre_, BorderLayout.NORTH);
    getContentPane().add(pDonnees2_, BorderLayout.CENTER);
    getContentPane().add(pBoutons_, BorderLayout.SOUTH);

    initialise_champs(nbat_);

    pack();

    /** *reinitialisation des valeurs* */

    setVisible(true);
    // addInternalFrameListener(this);
    imp_.addInternalFrame(this);

    /*
     * pDonnees_.add(lHoraireDeNavigation_); pDonnees_.setLayout(new GridLayout(2,2,3,4));
     * pDonnees_.add(lHoraireDebut_); pDonnees_.add(dHoraireDebut_); pDonnees_.add(lHoraireFin_);
     * pDonnees_.add(dHoraireFin_); pBoutons_.setLayout(new FlowLayout(FlowLayout.CENTER)); pBoutons_.add(bAnnuler_);
     * pBoutons_.add(bSauver_); getContentPane().add(pDonnees_, BorderLayout.CENTER); getContentPane().add(pBoutons_,
     * BorderLayout.SOUTH); pack(); setVisible(true); //addInternalFrameListener(this); imp_.addInternalFrame(this);
     */
    /*
     * setVisible(true); this.isEnabled(); this.show();
     */
  }

  /**
   * @param _e
   */
  public void actionPerformed(final ActionEvent _e) {
    if (_e.getSource() == bAnnuler_) {
      /*
       * int te=tId_.getText().length(); JOptionPane.showMessageDialog(null,"nb caractere "+te);
       */
      annuler();

      // JOptionPane.showMessageDialog(null,"Bouton Annuler");

    } else if (_e.getSource() == bSauver_) {
      if (controler_entrees()) {
        // JOptionPane.showMessageDialog(null,"Cr�ation R�ussie");
        initialise_champs(-1);
      }

    } else if (_e.getSource() == bAfficher_) {
      afficher_bateaux();

    }

  }

  private void annuler() {
    imp_.removeInternalFrame(this);
    imp_.resetFille2ModBateaux();

  }

  private boolean controler_entrees() {
    boolean valide = false;
    if (tId_.getText().equals("")) {
      // BuDialogMessage dialog_mess = new BuDialogMessage(imp_.getApp(),imp_.getInformationsSoftware(), "Entrez
      // l'identifiant du type de bateau");
      final BuDialogWarning dialog_mess = new BuDialogWarning(imp_.getApp(), imp_.getInformationsSoftware(),
          "Entrez l'identifiant du type de bateau");
      dialog_mess.activate();
    } else if (tLong_.getLongueurField() == 0.0) {
      // BuDialogMessage dialog_mess = new BuDialogMessage(imp_.getApp(),imp_.getInformationsSoftware(), "Entrez
      // l'identifiant du type de bateau");
      final BuDialogWarning dialog_mess = new BuDialogWarning(imp_.getApp(), imp_.getInformationsSoftware(),
          "Entrez la longueur du bateau en m");
      dialog_mess.activate();
    } else if (tLong_.getLongueurField() < 0.0) {
      // BuDialogMessage dialog_mess = new BuDialogMessage(imp_.getApp(),imp_.getInformationsSoftware(), "Entrez
      // l'identifiant du type de bateau");
      final BuDialogWarning dialog_mess = new BuDialogWarning(imp_.getApp(), imp_.getInformationsSoftware(),
          "Entrez une longueur du bateau en m positive");
      dialog_mess.activate();
    }

    else if (tLarg_.getLongueurField() == 0.0) {
      // BuDialogMessage dialog_mess = new BuDialogMessage(imp_.getApp(),imp_.getInformationsSoftware(), "Entrez
      // l'identifiant du type de bateau");
      final BuDialogWarning dialog_mess = new BuDialogWarning(imp_.getApp(), imp_.getInformationsSoftware(),
          "Entrez la largeur du bateau en m");
      dialog_mess.activate();
    }

    else if (tLarg_.getLongueurField() < 0.0) {
      // BuDialogMessage dialog_mess = new BuDialogMessage(imp_.getApp(),imp_.getInformationsSoftware(), "Entrez
      // l'identifiant du type de bateau");
      final BuDialogWarning dialog_mess = new BuDialogWarning(imp_.getApp(), imp_.getInformationsSoftware(),
          "Entrez la largeur du bateau en m positive");
      dialog_mess.activate();
    }

    /*
     * else if (dHoraireDebut_.getDureeField() < 0 || dHoraireDebut_.getDureeField() > 86400) {//a verifier pour minuit
     * ou undef //BuDialogMessage dialog_mess = new BuDialogMessage(imp_.getApp(),imp_.getInformationsSoftware(),
     * "Entrez l'identifiant du type de bateau"); BuDialogWarning dialog_mess = new
     * BuDialogWarning(imp_.getApp(),imp_.getInformationsSoftware(), "Entrez un horaire de d�but de navigation
     * correct"); dialog_mess.activate(); } else if (dHoraireFin_.getDureeField() < 0 || dHoraireFin_.getDureeField() >
     * 86400){//a verifier pour minuit ou undef //BuDialogMessage dialog_mess = new
     * BuDialogMessage(imp_.getApp(),imp_.getInformationsSoftware(), "Entrez l'identifiant du type de bateau");
     * BuDialogWarning dialog_mess = new BuDialogWarning(imp_.getApp(),imp_.getInformationsSoftware(), "Entrez un
     * horaire de Fin de navigation correct"); dialog_mess.activate(); } else if (dHoraireDebut_.getDureeField() >=
     * dHoraireFin_.getDureeField() ){ //BuDialogMessage dialog_mess = new
     * BuDialogMessage(imp_.getApp(),imp_.getInformationsSoftware(), "Entrez l'identifiant du type de bateau");
     * BuDialogWarning dialog_mess = new BuDialogWarning(imp_.getApp(),imp_.getInformationsSoftware(), "Heure de d�but
     * de navigation > heure de fin de navigation \n --> Incorrect"); dialog_mess.activate(); }
     */

    else if (dHoraireDebutHeure_.getDureeField() < 0 || dHoraireDebutHeure_.getDureeField() > 86400) {// a verifier pour
                                                                                                      // minuit ou undef
      // BuDialogMessage dialog_mess = new BuDialogMessage(imp_.getApp(),imp_.getInformationsSoftware(), "Entrez
      // l'identifiant du type de bateau");
      final BuDialogWarning dialog_mess = new BuDialogWarning(imp_.getApp(), imp_.getInformationsSoftware(),
          "Entrez un horaire de d�but de navigation correct");
      dialog_mess.activate();
    }

    else if (dHoraireFinHeure_.getDureeField() < 0 || dHoraireFinHeure_.getDureeField() > 86400) {// a verifier pour
                                                                                                  // minuit ou undef
      // BuDialogMessage dialog_mess = new BuDialogMessage(imp_.getApp(),imp_.getInformationsSoftware(), "Entrez
      // l'identifiant du type de bateau");
      final BuDialogWarning dialog_mess = new BuDialogWarning(imp_.getApp(), imp_.getInformationsSoftware(),
          "Entrez un horaire de Fin de navigation correct");
      dialog_mess.activate();
    }

    else if (dHoraireDebutMinute_.getDureeField() < 0 || dHoraireDebutMinute_.getDureeField() > 3600) {// a verifier
                                                                                                        // pour minuit
                                                                                                        // ou undef
      // BuDialogMessage dialog_mess = new BuDialogMessage(imp_.getApp(),imp_.getInformationsSoftware(), "Entrez
      // l'identifiant du type de bateau");
      final BuDialogWarning dialog_mess = new BuDialogWarning(imp_.getApp(), imp_.getInformationsSoftware(),
          "Entrez un horaire de d�but de navigation correct");
      dialog_mess.activate();
    }

    else if (dHoraireFinMinute_.getDureeField() < 0 || dHoraireFinMinute_.getDureeField() > 3600) {// a verifier pour
                                                                                                    // minuit ou undef
      // BuDialogMessage dialog_mess = new BuDialogMessage(imp_.getApp(),imp_.getInformationsSoftware(), "Entrez
      // l'identifiant du type de bateau");
      final BuDialogWarning dialog_mess = new BuDialogWarning(imp_.getApp(), imp_.getInformationsSoftware(),
          "Entrez un horaire de Fin de navigation correct");
      dialog_mess.activate();
    } else if (dHoraireDebutHeure_.getDureeField() >= dHoraireFinHeure_.getDureeField()
        && dHoraireDebutMinute_.getDureeField() >= dHoraireFinMinute_.getDureeField()) {
      // BuDialogMessage dialog_mess = new BuDialogMessage(imp_.getApp(),imp_.getInformationsSoftware(), "Entrez
      // l'identifiant du type de bateau");
      final BuDialogWarning dialog_mess = new BuDialogWarning(imp_.getApp(), imp_.getInformationsSoftware(),
          "Heure de d�but de navigation > heure de fin de navigation \n --> Incorrect");
      dialog_mess.activate();
    } else if (dHoraireFinHeure_.getDureeField() == 86400 && dHoraireFinMinute_.getDureeField() > 0) {
      // BuDialogMessage dialog_mess = new BuDialogMessage(imp_.getApp(),imp_.getInformationsSoftware(), "Entrez
      // l'identifiant du type de bateau");
      affMessage("Heure de Fin de navigation sup�rieur � 24h00");

    } else if (dChargeHeureG_.getDureeField() < 0 || dChargeHeureG_.getDureeField() > 86400) {// a verifier pour minuit
                                                                                              // ou undef
      // BuDialogMessage dialog_mess = new BuDialogMessage(imp_.getApp(),imp_.getInformationsSoftware(), "Entrez
      // l'identifiant du type de bateau");
      final BuDialogWarning dialog_mess = new BuDialogWarning(imp_.getApp(), imp_.getInformationsSoftware(),
          "Entrez une dur�e de g�ne admissible correct");
      dialog_mess.activate();
    }

    else if (dLegeHeureG_.getDureeField() < 0 || dLegeHeureG_.getDureeField() > 86400) {// a verifier pour minuit ou
                                                                                        // undef
      // BuDialogMessage dialog_mess = new BuDialogMessage(imp_.getApp(),imp_.getInformationsSoftware(), "Entrez
      // l'identifiant du type de bateau");
      final BuDialogWarning dialog_mess = new BuDialogWarning(imp_.getApp(), imp_.getInformationsSoftware(),
          "Entrez une dur�e de g�ne admissible correct");
      dialog_mess.activate();
    } else if (dChargeMinuteG_.getDureeField() < 0 || dChargeMinuteG_.getDureeField() > 3600) {// a verifier pour
                                                                                                // minuit ou undef
      // BuDialogMessage dialog_mess = new BuDialogMessage(imp_.getApp(),imp_.getInformationsSoftware(), "Entrez
      // l'identifiant du type de bateau");
      final BuDialogWarning dialog_mess = new BuDialogWarning(imp_.getApp(), imp_.getInformationsSoftware(),
          "Entrez une dur�e de g�ne admissible correct");
      dialog_mess.activate();
    } else if (dLegeMinuteG_.getDureeField() < 0 || dLegeMinuteG_.getDureeField() > 3600) {// a verifier pour minuit ou
                                                                                            // undef
      // BuDialogMessage dialog_mess = new BuDialogMessage(imp_.getApp(),imp_.getInformationsSoftware(), "Entrez
      // l'identifiant du type de bateau");
      final BuDialogWarning dialog_mess = new BuDialogWarning(imp_.getApp(), imp_.getInformationsSoftware(),
          "Entrez une dur�e de g�ne admissible correct");
      dialog_mess.activate();
    }

    /*
     * else if(tChargeT_.getLongueurField() !=0.0 && tVitesseMontantC_.getLongueurField()==0){ BuDialogWarning
     * dialog_mess = new BuDialogWarning(imp_.getApp(),imp_.getInformationsSoftware(), "Entrez une vitesse des montants
     * pour le bateau charg�"); dialog_mess.activate(); } else if(tChargeT_.getLongueurField()!=0.0 &&
     * tVitesseDescendantC_.getLongueurField()==0){ BuDialogWarning dialog_mess = new
     * BuDialogWarning(imp_.getApp(),imp_.getInformationsSoftware(), "Entrez une vitesse des montants pour le bateau
     * charg�"); dialog_mess.activate(); } else if(tLegeT_.getLongueurField() !=0.0 &&
     * tVitesseMontantL_.getLongueurField()==0){ BuDialogWarning dialog_mess = new
     * BuDialogWarning(imp_.getApp(),imp_.getInformationsSoftware(), "Entrez une vitesse des montants pour le bateau
     * l�ge"); dialog_mess.activate(); } else if(tLegeT_.getLongueurField()!=0.0 &&
     * tVitesseDescendantL_.getLongueurField()==0){ BuDialogWarning dialog_mess = new
     * BuDialogWarning(imp_.getApp(),imp_.getInformationsSoftware(), "Entrez une vitesse des avalants pour le bateau
     * l�ge"); dialog_mess.activate(); }
     */
    // rajouter les ontroles sur les horaires
    /*
     * else if ((tChargeT_.getLongueurField() == 0 || dChargeG_.getDureeField() == 0) && (tLegeT_.getLongueurField() ==
     * 0 || dLegeG_.getDureeField()==0)){ //BuDialogMessage dialog_mess = new
     * BuDialogMessage(imp_.getApp(),imp_.getInformationsSoftware(), "Entrez l'identifiant du type de bateau");
     * BuDialogWarning dialog_mess = new BuDialogWarning(imp_.getApp(),imp_.getInformationsSoftware(), "Entrez les
     * caract�ristiques pour au moins Charge ou L�ge"); dialog_mess.activate(); }
     */
    else {
      if (tChargeT_.getLongueurField() != 0 /*
                                             * && dChargeHeureG_.getDureeField() != 0 &&
                                             * dChargeMinuteG_.getDureeField()!=0
                                             */) {
        String nom_ = "";
        nom_ = tId_.getText() + "C";

        final SinaviTypeBateau b = new SinaviTypeBateau(nom_, tLong_.getLongueurField(), tLarg_.getLongueurField(),/* dHoraireDebut_.getDureeField(),dHoraireFin_.getDureeField() */
            dHoraireDebutHeure_.getDureeField(), dHoraireDebutMinute_.getDureeField(), dHoraireFinHeure_
                .getDureeField(), dHoraireFinMinute_.getDureeField(), tChargeT_.getLongueurField(), dChargeHeureG_
                .getDureeField(), dChargeMinuteG_.getDureeField(), tVitesseMontantC_.getLongueurField(),
            tVitesseDescendantC_.getLongueurField());
        SParametresBateau bat = new SParametresBateau();
        bat = b.bat_;
        System.out.println("le nom que je vais sauvegarder quil faut sup ---> " + nom_);
        addBateau(nom_, bat);

      }

      if ((tLegeT_.getLongueurField() != 0) /*
                                             * && dLegeHeureG_.getDureeField() != 0 && dLegeMinuteG_.getDureeField() !=
                                             * 0
                                             */) {
        final String nom_ = tId_.getText() + "L";
        final SinaviTypeBateau b = new SinaviTypeBateau(nom_, tLong_.getLongueurField(), tLarg_.getLongueurField(),/* dHoraireDebut_.getDureeField(),dHoraireFin_.getDureeField() */
            dHoraireDebutHeure_.getDureeField(), dHoraireDebutMinute_.getDureeField(), dHoraireFinHeure_
                .getDureeField(), dHoraireFinMinute_.getDureeField(), tLegeT_.getLongueurField(), dLegeHeureG_
                .getDureeField(), dLegeMinuteG_.getDureeField(), tVitesseMontantL_.getLongueurField(),
            tVitesseDescendantL_.getLongueurField());
        SParametresBateau bat = new SParametresBateau();
        bat = b.bat_;
        System.out.println("le nom que je vais sauvegarder quil faut dup ---> " + nom_);
        addBateau(nom_, bat);

      }

      valide = true;

    }

    return valide;

  }

  private void addBateau(final String _nom, final SParametresBateau _bat) {

    // affMessage();
    addListeBateau(_bat);
    // affMessage();
    affMessage("Creation du type bateau Charge de nom " + _nom + "  r�ussi");
  }

  public void addListeBateau(final SParametresBateau _b) {
    /* ajout ou modification d'un bateau */

    /** *ajout d'un bateau */
    // ------------------------------listeBateaux_.addLast(b_);
    // -----------listeBateaux_.add(listeBateaux_.size(),b_);
    /** verifier l'existance du bateau* */
    System.out.println("entre dans addlistebateau");
    final int x = rechercher_bateau(_b.identification);
    System.out.println("x: " + x);
    if (x == -1) {
      listeBateaux_.add(_b);
    } else {
      System.out.println("suppression du bateau num�ro " + x);
      listeBateaux_.remove(x);
      listeBateaux_.add(x, _b);
    }
  }

  private int rechercher_bateau(final String _batcourant) {
    final ListIterator iter = listeBateaux_.listIterator();
    int i = 0;
    boolean trouve = false;
    System.out.println("bateau � chercher : " + _batcourant);
    while (iter.hasNext() && !trouve) {
      SParametresBateau c = new SParametresBateau();
      c = (SParametresBateau) iter.next();
      System.out.println("bateau observ� : " + c.identification);
      if (c.identification.equalsIgnoreCase(_batcourant)) {
        trouve = true;
        System.out.println("rechercher" + i);
        return i;
      } else {
        i++;
      }
    }
    return -1;
  }

  private void afficher_bateaux() {
    /** *boucle sur la lecture des bateaux en affichant seuelment identifiant pour voir si ca marche** */

    // affMessage();
    /*
     * ListIterator iter= listeBateaux_.listIterator(); while (iter.hasNext()){ SParametresBateau c= new
     * SParametresBateau(); c= (SParametresBateau) iter.next(); BuDialogMessage dialog_mess = new
     * BuDialogMessage(imp_.getApp(), imp_.InfoSoftSinavi2_, c.identification); dialog_mess.activate(); }
     */
    if (listeBateaux_ != null) {
      imp_.afficherBateaux();
      // affMessage();
    }
  }

  public void setnbat(final int _bat) {

    nbat_ = _bat;
  }

  public void internalFrameActivated(final InternalFrameEvent e) {
  // TODO Auto-generated method stub

  }

  public void internalFrameClosed(final InternalFrameEvent e) {

  // TODO Auto-generated method stub

  }

  public void internalFrameClosing(final InternalFrameEvent e) {
  // TODO Auto-generated method stub

  }

  public void internalFrameDeactivated(final InternalFrameEvent e) {
  // TODO Auto-generated method stub

  }

  public void internalFrameDeiconified(final InternalFrameEvent e) {
  // TODO Auto-generated method stub

  }

  public void internalFrameIconified(final InternalFrameEvent e) {
  // TODO Auto-generated method stub

  }

  public void internalFrameOpened(final InternalFrameEvent e) {
  // TODO Auto-generated method stub

  }

  public void affMessage(final String _t) {
    final BuDialogMessage dialog_mess = new BuDialogMessage(imp_.getApp(), imp_.getInformationsSoftware(), "" + _t);
    dialog_mess.activate();
  }

  public void initialise_champs(final int _bat) {
    System.out.println("Entr�e dans initialise champs");
    if (_bat == -1) {
      System.out.println("Entr�e dans initialise champs bat-1");
      tId_.setText("");
      tLong_.setLongueurField(0);
      tLarg_.setLongueurField(0);
      // dHoraireDebut_.setDureeField(0);
      dHoraireDebutHeure_.setDureeField(0);
      dHoraireDebutMinute_.setDureeField(0);
      dHoraireFinHeure_.setDureeField(86400);
      dHoraireFinMinute_.setDureeField(0);
      // dHoraireFin_.setDureeField(86400);
      tChargeT_.setLongueurField(0);
      dChargeHeureG_.setDureeField(0);
      dChargeMinuteG_.setDureeField(0);
      tLegeT_.setLongueurField(0);
      dLegeHeureG_.setDureeField(0);
      dLegeMinuteG_.setDureeField(0);
      // ------------------------------------------
      tVitesseMontantL_.setLongueurField(0);
      tVitesseDescendantL_.setLongueurField(0);

      tVitesseMontantC_.setLongueurField(0);
      tVitesseDescendantC_.setLongueurField(0);

      // ----------------------------------------
    } else {
      /** ********modifier****** */
      /* je comprends pas pourquoi ca marche pas */
      final SParametresBateau batCourant_ = (SParametresBateau) listeBateaux_.get(nbat_);

      // tId_.setText(batCourant_.identification);
      // affMessage("" + (batCourant_.getLongueur().getLongueurField() /100));
      tLong_.setLongueurField(batCourant_.longueur);
      // affMessage("" + (batCourant_.getLongueur().getLongueurField() /100));
      tLarg_.setLongueurField(batCourant_.largeur);
      /*
       * dHoraireDebut_.setDureeField(SinaviTypeBateau.determineSeconde(batCourant_.debutNavigation));
       * dHoraireFin_.setDureeField(SinaviTypeBateau.determineSeconde(batCourant_.finNavigation));
       */

      dHoraireDebutHeure_.setDureeField(SinaviTypeBateau.determineHeureSeule(batCourant_.debutNavigation));
      dHoraireDebutMinute_.setDureeField(SinaviTypeBateau.determineMinuteSeule(batCourant_.debutNavigation));
      dHoraireFinHeure_.setDureeField(SinaviTypeBateau.determineHeureSeule(batCourant_.finNavigation));
      dHoraireFinMinute_.setDureeField(SinaviTypeBateau.determineMinuteSeule(batCourant_.finNavigation));
      final int nbCar_ = batCourant_.identification.length() - 1;

      /*
       * char[] id=new char[nbCar_]; for(int i=0;i<nbCar_;i++){ id[i]=batCourant_.identification.charAt(i);
       * System.out.println(batCourant_.identification.charAt(i)); } String test="";
       * test.copyValueOf(id,0,nbCar_); System.out.println(test);
       */
      String temp = "";
      temp = batCourant_.identification;
      System.out.println(temp);
      temp = temp.substring(0, temp.length() - 1);

      tId_.setText(temp);

      if (batCourant_.identification.charAt(nbCar_) == 'C') {
        tChargeT_.setLongueurField(batCourant_.tirantDeau);
        dChargeHeureG_.setDureeField(SinaviTypeBateau.determineHeureSeule(batCourant_.dureeGeneAdmissible));
        dChargeMinuteG_.setDureeField(SinaviTypeBateau.determineMinuteSeule(batCourant_.dureeGeneAdmissible));
        tLegeT_.setLongueurField(0);
        dLegeHeureG_.setDureeField(0);
        dLegeMinuteG_.setDureeField(0);
        // ---------------------------------------
        tVitesseMontantL_.setLongueurField(0);
        tVitesseDescendantL_.setLongueurField(0);

        tVitesseMontantC_.setLongueurField(batCourant_.vitesseMontantParDefaut * 1000);
        tVitesseDescendantC_.setLongueurField(batCourant_.vitesseDescendantParDefaut * 1000);
        // ------------------------------------
      } else {
        tLegeT_.setLongueurField(batCourant_.tirantDeau);
        dLegeHeureG_.setDureeField(SinaviTypeBateau.determineHeureSeule(batCourant_.dureeGeneAdmissible));
        dLegeMinuteG_.setDureeField(SinaviTypeBateau.determineMinuteSeule(batCourant_.dureeGeneAdmissible));
        tChargeT_.setLongueurField(0);
        dChargeHeureG_.setDureeField(0);
        dChargeMinuteG_.setDureeField(0);
        // ------------------------------------------
        tVitesseMontantC_.setLongueurField(0);
        tVitesseDescendantC_.setLongueurField(0);

        tVitesseMontantL_.setLongueurField(batCourant_.vitesseMontantParDefaut * 1000);
        tVitesseDescendantL_.setLongueurField(batCourant_.vitesseDescendantParDefaut * 1000);
        // ------------------------------------------
      }

    }

  }

  public void focusGained(final FocusEvent _e) {

  }

  public void focusLost(final FocusEvent _e) {
    if (_e.getSource() == tId_) {
      if (tId_.getText().equalsIgnoreCase("")) {
        affMessage("Entrez l'identifiant du bateau");
      }
    }

    else if (_e.getSource() == tLong_ && !tId_.getText().equalsIgnoreCase("")) {
      if (tLong_.getLongueurField() <= 0) {// a verifier pour minuit ou undef
        // BuDialogMessage dialog_mess = new BuDialogMessage(imp_.getApp(),imp_.getInformationsSoftware(), "Entrez
        // l'identifiant du type de bateau");
        affMessage("entrez une longueur de bateau positive");
      }

    }

    else if (_e.getSource() == tLarg_ && tLong_.getLongueurField() != 0) {
      if (tLarg_.getLongueurField() <= 0) {// a verifier pour minuit ou undef
        // BuDialogMessage dialog_mess = new BuDialogMessage(imp_.getApp(),imp_.getInformationsSoftware(), "Entrez
        // l'identifiant du type de bateau");
        affMessage("entrez une largeur de bateau positive");
      }
    }

    else if (_e.getSource() == dHoraireDebutHeure_) {
      if ((dHoraireDebutHeure_.getDureeField() < 0 || dHoraireDebutHeure_.getDureeField() > 82800)
          && tLarg_.getLongueurField() != 0) {// a verifier pour minuit ou undef
        // BuDialogMessage dialog_mess = new BuDialogMessage(imp_.getApp(),imp_.getInformationsSoftware(), "Entrez
        // l'identifiant du type de bateau");
        affMessage("Entrez une heure de d�but de navigation correcte \n comprise entre 0 et 23");

      }
    } else if (_e.getSource() == dHoraireFinHeure_) {
      if ((dHoraireFinHeure_.getDureeField() < 0 || dHoraireFinHeure_.getDureeField() > 86400)
          && !((dHoraireDebutHeure_.getDureeField() < 0 || dHoraireDebutHeure_.getDureeField() > 82800) && tLarg_
              .getLongueurField() != 0)) {// a verifier pour minuit ou undef
        // BuDialogMessage dialog_mess = new BuDialogMessage(imp_.getApp(),imp_.getInformationsSoftware(), "Entrez
        // l'identifiant du type de bateau");
        affMessage("Entrez une heure de fin de navigation correcte \n comprise entre 0 et 23");
      }
    } else if (_e.getSource() == dHoraireDebutMinute_) {
      if ((dHoraireDebutMinute_.getDureeField() < 0 || dHoraireDebutMinute_.getDureeField() > 3600)
          && !((dHoraireFinHeure_.getDureeField() < 0 || dHoraireFinHeure_.getDureeField() > 86400) && !((dHoraireDebutHeure_
              .getDureeField() < 0 || dHoraireDebutHeure_.getDureeField() > 82800) && tLarg_.getLongueurField() != 0))) {// a
                                                                                                                          // verifier
                                                                                                                          // pour
                                                                                                                          // minuit
                                                                                                                          // ou
                                                                                                                          // undef
        // BuDialogMessage dialog_mess = new BuDialogMessage(imp_.getApp(),imp_.getInformationsSoftware(), "Entrez
        // l'identifiant du type de bateau");
        affMessage("Entrez un horaire de d�but de navigation correct \n compris entre 0 et 60");
      }
    } else if (_e.getSource() == dHoraireFinMinute_) {
      if ((dHoraireFinMinute_.getDureeField() < 0 || dHoraireFinMinute_.getDureeField() > 3600)
          && !((dHoraireDebutMinute_.getDureeField() < 0 || dHoraireDebutMinute_.getDureeField() > 3600) && !((dHoraireFinHeure_
              .getDureeField() < 0 || dHoraireFinHeure_.getDureeField() > 86400) && !((dHoraireDebutHeure_
              .getDureeField() < 0 || dHoraireDebutHeure_.getDureeField() > 82800) && tLarg_.getLongueurField() != 0)))) {// a
                                                                                                                          // verifier
                                                                                                                          // pour
                                                                                                                          // minuit
                                                                                                                          // ou
                                                                                                                          // undef
        // BuDialogMessage dialog_mess = new BuDialogMessage(imp_.getApp(),imp_.getInformationsSoftware(), "Entrez
        // l'identifiant du type de bateau");
        affMessage("Entrez un horaire de fin de navigation correct \n compris entre 0 et 60");
      }
    } else if (_e.getSource() == tVitesseMontantC_) {
      if (tVitesseMontantC_.getLongueurField() <= 0 && tChargeT_.getLongueurField() > 0) {// a verifier pour minuit ou
                                                                                          // undef
        // BuDialogMessage dialog_mess = new BuDialogMessage(imp_.getApp(),imp_.getInformationsSoftware(), "Entrez
        // l'identifiant du type de bateau");
        affMessage("Entrez une vitesse sup�rieur � 0 km/h");
      }
    } else if (_e.getSource() == tVitesseMontantL_) {
      if (tVitesseMontantL_.getLongueurField() <= 0 && tLegeT_.getLongueurField() > 0) {// a verifier pour minuit ou
                                                                                        // undef
        // BuDialogMessage dialog_mess = new BuDialogMessage(imp_.getApp(),imp_.getInformationsSoftware(), "Entrez
        // l'identifiant du type de bateau");
        affMessage("Entrez une vitesse sup�rieur � 0 km/h");

      }
    } else if (_e.getSource() == tVitesseDescendantC_) {
      if (tVitesseDescendantC_.getLongueurField() <= 0 && tChargeT_.getLongueurField() > 0) {// a verifier pour minuit
                                                                                              // ou undef
        // BuDialogMessage dialog_mess = new BuDialogMessage(imp_.getApp(),imp_.getInformationsSoftware(), "Entrez
        // l'identifiant du type de bateau");
        affMessage("Entrez une vitesse sup�rieur � 0 km/h");
      }
    } else if (_e.getSource() == tVitesseDescendantL_) {
      if (tVitesseDescendantL_.getLongueurField() <= 0 && tLegeT_.getLongueurField() > 0) {// a verifier pour minuit ou
                                                                                            // undef
        // BuDialogMessage dialog_mess = new BuDialogMessage(imp_.getApp(),imp_.getInformationsSoftware(), "Entrez
        // l'identifiant du type de bateau");
        affMessage("Entrez une vitesse sup�rieur � 0 km/h");
      }
    }

    else if (_e.getSource() == dChargeMinuteG_) {
      if (dChargeMinuteG_.getDureeField() > 3600) {// a verifier pour minuit ou undef
        // BuDialogMessage dialog_mess = new BuDialogMessage(imp_.getApp(),imp_.getInformationsSoftware(), "Entrez
        // l'identifiant du type de bateau");
        affMessage("Entrez un horaire de g�ne admissible correct \n compris entre 0 et 60");
      }
    } else if (_e.getSource() == dLegeMinuteG_) {
      if (dLegeMinuteG_.getDureeField() > 3600) {// a verifier pour minuit ou undef
        // BuDialogMessage dialog_mess = new BuDialogMessage(imp_.getApp(),imp_.getInformationsSoftware(), "Entrez
        // l'identifiant du type de bateau");
        affMessage("Entrez un horaire de g�ne admissible correct \n compris entre 0 et 60");
      }
    }
    // ------------------------------------------------------------------------------
    /** charg� l�ges tirant d'eau gene admissible 3 colonnes */

    /*
     * else if(_e.getSource() == tChargeT_){ if (tChargeT_.getLongueurField() <= 0) {//a verifier pour minuit ou undef
     * //BuDialogMessage dialog_mess = new BuDialogMessage(imp_.getApp(),imp_.getInformationsSoftware(), "Entrez
     * l'identifiant du type de bateau"); affMessage("Entrez un tirant d'eau positif"); } } else if(_e.getSource() ==
     * tLegeT_){ if (tLegeT_.getLongueurField() <=0) {//a verifier pour minuit ou undef //BuDialogMessage dialog_mess =
     * new BuDialogMessage(imp_.getApp(),imp_.getInformationsSoftware(), "Entrez l'identifiant du type de bateau");
     * affMessage("Entrez un tirant d'eau positif"); } }
     */

  }

}
