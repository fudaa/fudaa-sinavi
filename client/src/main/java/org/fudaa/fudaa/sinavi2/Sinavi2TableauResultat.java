package org.fudaa.fudaa.sinavi2;

import java.util.List;

import javax.swing.table.AbstractTableModel;

import jxl.write.Label;
import jxl.write.Number;
import jxl.write.WritableCell;

import com.memoire.fu.FuLog;

public class Sinavi2TableauResultat extends AbstractTableModel implements org.fudaa.ctulu.table.CtuluTableModelInterface {
  private int nbrow_ = 0;
  private final String[] NomColonnes_ = { "Elément", "Nb bat Mon", "Nb bat Av", "Nb bat Total", "Nb att Mont",
      "Tot att mont", "Moy att mont", "Max att mont", "Nb att av", "Tot att av", "Moy att av", "Max att av",
      "Nb total bat att", "att total", "Moy att total", "Max att Total" };
  public Object[][] data_ = null;// =new Object[0][0];

  public String[] nomCol() {
    return NomColonnes_;
  }

  public void setNbrow(final int _n) {
    nbrow_ = _n;

  }

  public void init_nomcol(final Object s[][]) {
    for (int i = 0; i < getColumnCount(); i++) {
      data_[0][i] = NomColonnes_[i];
    }

  }

  public int getColumnCount() {

    return NomColonnes_.length;
  }

  public int getRowCount() {
    /*
     * if(data!=null) return data.length; else
     */
    return nbrow_;// data.length;
    // return 0;
  }

  public Object getValueAt(final int _rowIndex, final int _columnIndex) {

    return data_[_rowIndex][_columnIndex];
  }

  public void setValueAt(final Object _value, final int _row, final int _col) {
    // String x=new String(value);
    // x=value;
    final Object[][] temp = new Object[_row + 1][getColumnCount()];
    nbrow_ = _row;
    if (data_ != null) {
      for (int i = 0; i <= _row; i++) {
        if (i == _row) {
          for (int j = 0; j < _col; j++) {
            // temp[i][j]=new Object();
            temp[i][j] = data_[i][j];
          }
        } else {
          for (int j = 0; j < getColumnCount(); j++) {
            // temp[i][j]=new Object();
            temp[i][j] = data_[i][j];
          }
        }
      }
    }
    temp[_row][_col] = _value;
    data_ = new Object[_row + 1][getColumnCount()];
    for (int i = 0; i <= _row; i++) {
      if (i == _row) {
        for (int j = 0; j <= _col; j++) {
          // temp[i][j]=new Object();
          data_[i][j] = temp[i][j];
        }
      } else {
        for (int j = 0; j < getColumnCount(); j++) {
          // temp[i][j]=new Object();
          data_[i][j] = temp[i][j];
        }
      }
    }
    // init_nomcol(data_);
    // data[row][col]=new String(value);
    // nbrow=row+1;
    // fireTableCellUpdated(row, col);
  }

  public int getMaxCol() {
    // TODO Auto-generated method stub
    return nomCol().length;
  }

  public int getMaxRow() {
    // TODO Auto-generated method stub
    return nbrow_;
  }
  
     public int[] getSelectedRows() {
        return null;
    }


  public Object getValue(final int _row, final int _col) {
    // TODO Auto-generated method stub
    return getValueAt(_row, _col);
  }

  public WritableCell getExcelWritable(final int _row, final int _col, int _rowXls, int _colXls) {
    final int r = _row;
    final int c = _col;
    /*
     * if (column_ != null) c = column_[c]; if (row_ != null) { r = row_[r]; }
     */
    final Object o = data_[r][c];
    if (o == null) {
      return null;
    }
    String s = o.toString();
    if (s == null) {
      return null;
    }
    s = s.trim();
    if (s.length() == 0) {
      return null;
    }
    try {
      return new Number(_colXls, _rowXls, Double.parseDouble(s));
    } catch (final NumberFormatException e) {}
    return new Label(_colXls, _rowXls, s);

    // return cell;
  }

  public String getColumnName(final int _i) {
    FuLog.debug("" + NomColonnes_[_i]);
    return NomColonnes_[_i];

  }

public List<String> getComments() {
	// TODO Auto-generated method stub
	return null;
}
}
