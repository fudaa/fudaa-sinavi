package org.fudaa.fudaa.sinavi2;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.ListIterator;
import java.util.Vector;

import javax.swing.JComponent;
import javax.swing.JTable;
import javax.swing.SwingUtilities;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumnModel;

import com.memoire.bu.*;

import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluValueValidator;
import org.fudaa.ctulu.editor.CtuluValueEditorInteger;
import org.fudaa.ctulu.gui.CtuluCellTextRenderer;
import org.fudaa.ctulu.gui.CtuluDialogPanel;
import org.fudaa.ctulu.gui.CtuluLibSwing;
import org.fudaa.ctulu.gui.CtuluListEditorModel;
import org.fudaa.ctulu.gui.CtuluListEditorPanel;
import org.fudaa.dodico.corba.sinavi2.*;
import org.fudaa.dodico.sinavi2.Sinavi2Helper;

/**
 * impl�mentation d'une fen�tre interne permettant de saisir le trajet d'un bateau.
 * 
 * @version $Revision: 1.9 $ $Date: 2007-05-04 14:01:05 $ by $Author: deniger $
 * @author Fatimatou Ka , Benoit Maneuvrier
 */

public class Sinavi2FilleAddModTrajets extends CtuluDialogPanel {

  public static class DateHeureTableModel extends CtuluListEditorModel {

    private final DureeField tfDeterDebutHeure_;
    private final DureeField tfDeterDebutMinute_;
    private final CtuluValueEditorInteger dateEditor_;

    private JTable t_;

    public TableCellEditor getEditorCol0() {
      return dateEditor_.createTableEditorComponent();
    }

    public TableCellEditor getEditorCol1() {
      return tfDeterDebutHeure_.createEditor();
    }

    public TableCellEditor getEditorCol2() {
      return tfDeterDebutMinute_.createEditor();
    }

    public TableCellRenderer getRendererCol1() {
      return tfDeterDebutHeure_.createRenderer();
    }

    public TableCellRenderer getRendererCol2() {
      return tfDeterDebutMinute_.createRenderer();
    }

    public DateHeureTableModel(final SDateHeure[] _values) {
      super(false);
      if (_values != null) {
        super.v_.addAll(Arrays.asList(_values));
      }
      dateEditor_ = new CtuluValueEditorInteger(false);
      final CtuluValueValidator val = new CtuluValueValidator.IntMin(1);
      dateEditor_.setVal(val);
      tfDeterDebutHeure_ = new DureeField(false, false, true, false, false);
      tfDeterDebutHeure_.setDureeValidator(DureeFieldValidator.creeMax(82800, Sinavi2Resource.SINAVI2
          .getString("La valeur doit �tre comprise entre 0 et 23")));
      tfDeterDebutMinute_ = new DureeField(false, false, false, true, false);
      tfDeterDebutMinute_.setDureeValidator(DureeFieldValidator.creeMax(3599, Sinavi2Resource.SINAVI2
          .getString("La valeur doit �tre comprise entre 0 et 59")));
    }

    public boolean isValid() {
      if (t_.isEditing()) {
        t_.getCellEditor().stopCellEditing();
      }
      return v_.size() > 0;
    }

    public void sort() {
      Collections.sort(v_, new Sinavi2Lib.SDateHeureComparator());
      fireTableRowsUpdated(0, v_.size() - 1);
    }

    public SDateHeure[] getResult() {
      final SDateHeure[] r = (SDateHeure[]) v_.toArray(new SDateHeure[v_.size()]);
      Arrays.sort(r, new Sinavi2Lib.SDateHeureComparator());
      return r;
    }

    public boolean isCellEditable(final int _rowIndex, final int _columnIndex) {
      return true;
    }

    private boolean isValide(final Object _o) {
      final SDateHeure h = (SDateHeure) _o;
      return dateEditor_.isValid(new Integer(h.date))
          && tfDeterDebutHeure_.VALUE_DUREE.isValueValid(new Integer(h.heure.heure * 3600))
          && tfDeterDebutMinute_.VALUE_DUREE.isValueValid(new Integer(h.heure.minutes * 60));
    }

    SDateHeure last_;

    public void addElement(final Object _o) {
      last_ = (SDateHeure) _o;
      if (isValide(_o)) {
        super.addElement(_o);
      }
    }

    public boolean canRemove() {
      return v_.size() > 1;
    }

    SDateHeure getDateHeure(final int _i) {
      if (_i >= v_.size()) {
        return null;
      }
      return (SDateHeure) v_.get(_i);
    }

    public String getColumnName(final int _col) {
      if (_col == 0) {
        return "Date";
      }
      if (_col == 1) {
        return "Heure";
      }
      if (_col == 2) {
        return "Minutes";
      }
      return CtuluLibString.EMPTY_STRING;
    }

    public boolean canAdd() {
      return true;
    }

    public Object createNewObject() {
      int lastDate = 1;
      int lastH = 8;
      int lastMin = 0;
      SDateHeure h = last_;
      if (h == null && v_.size() > 0) {
        h = getDateHeure(v_.size() - 1);
      }
      if (h != null) {
        lastDate = h.date;
        lastH = h.heure.heure + 1;
        if (lastH > 23) {
          lastDate++;
          lastH = lastH % 24;
        }
        lastMin = h.heure.minutes;
      }
      return new SDateHeure(lastDate, new SHeure(lastH, lastMin));
    }

    public Object getValueAt(final int _row, final int _col) {
      final SDateHeure h = getDateHeure(_row);
      if (h == null) {
        return CtuluLibString.EMPTY_STRING;
      }
      if (_col == 0) {
        return new Integer(h.date);
      }
      if (_col == 1) {
        return new Integer(h.heure.heure * 3600);
      }
      if (_col == 2) {
        return new Integer(h.heure.minutes * 60);
      }
      return CtuluLibString.EMPTY_STRING;
    }

    public SDateHeure[] getDateHeures() {
      return (SDateHeure[]) v_.toArray(new SDateHeure[v_.size()]);
    }

    public void setValueAt(final Object _aValue, final int _rowIndex, final int _columnIndex) {
      if (_aValue instanceof Integer) {
        final SDateHeure h = getDateHeure(_rowIndex);
        boolean ok = false;
        final int val = ((Integer) _aValue).intValue();
        if (_columnIndex == 0 && dateEditor_.isValid(_aValue)) {
          h.date = val;
          ok = true;
        } else if (_columnIndex == 1 && tfDeterDebutHeure_.VALUE_DUREE.isValueValid(_aValue)) {
          h.heure.heure = val / 3600;
          ok = true;
        } else if (_columnIndex == 2 && tfDeterDebutMinute_.VALUE_DUREE.isValueValid(_aValue)) {
          h.heure.minutes = val / 60;
          ok = true;
        }
        if (ok) {
          last_ = h;
          fireTableCellUpdated(_rowIndex, _columnIndex);
        }

      }
    }

    public int getColumnCount() {
      return 3;
    }

    protected JTable getT() {
      return t_;
    }

    protected void setT(final JTable _t) {
      t_ = _t;
    }

  }

  public static class HeureTableModel extends CtuluListEditorModel {

    private DureeField tfJourDebutHeure_;
    private DureeField tfJourDebutMinute_;

    private JTable t_;

    public HeureTableModel(final SHeure[] _values) {
      super(false);
      if (_values != null) {
        super.v_.addAll(Arrays.asList(_values));
      }
      tfJourDebutHeure_ = new DureeField(false, false, true, false, false);
      tfJourDebutHeure_.setDureeValidator(DureeFieldValidator.creeMax(82800, Sinavi2Resource.SINAVI2
          .getString("La valeur doit �tre comprise entre 0 et 23")));
      tfJourDebutMinute_ = new DureeField(false, false, false, true, false);
      tfJourDebutMinute_.setDureeValidator(DureeFieldValidator.creeMax(3599, Sinavi2Resource.SINAVI2
          .getString("La valeur doit �tre comprise entre 0 et 59")));
    }

    public boolean isValid() {
      if (t_.getCellEditor() != null) {
        t_.getCellEditor().stopCellEditing();
      }
      return v_.size() > 0;
    }

    public int getColumnCount() {
      return 2;
    }

    public TableCellEditor getEditorCol0() {
      return tfJourDebutHeure_.createEditor();
    }

    public TableCellEditor getEditorCol1() {
      return tfJourDebutMinute_.createEditor();
    }

    public TableCellRenderer getRendererCol0() {
      return tfJourDebutHeure_.createRenderer();
    }

    public TableCellRenderer getRendererCol1() {
      return tfJourDebutMinute_.createRenderer();
    }

    public void sort() {
      Collections.sort(v_, new Sinavi2Lib.SHeureComparator());
      fireTableRowsUpdated(0, v_.size() - 1);
    }

    public SHeure[] getResult() {
      final SHeure[] r = (SHeure[]) v_.toArray(new SHeure[v_.size()]);
      Arrays.sort(r, new Sinavi2Lib.SHeureComparator());
      return r;
    }

    public boolean isCellEditable(final int _rowIndex, final int _columnIndex) {
      return true;
    }

    public SHeure[] getDateHeures() {
      return (SHeure[]) v_.toArray(new SHeure[v_.size()]);
    }

    private boolean isValide(final Object _o) {
      final SHeure h = (SHeure) _o;
      return tfJourDebutHeure_.VALUE_DUREE.isValueValid(new Integer(h.heure * 3600))
          && tfJourDebutMinute_.VALUE_DUREE.isValueValid(new Integer(h.minutes * 60));
    }

    SHeure last_;

    public void addElement(final Object _o) {
      last_ = (SHeure) _o;
      if (isValide(_o)) {
        super.addElement(_o);
      }
    }

    public boolean canRemove() {
      return v_.size() > 1;
    }

    SHeure getDateHeure(final int _i) {
      if (_i >= v_.size()) {
        return null;
      }
      return (SHeure) v_.get(_i);
    }

    public String getColumnName(final int _col) {
      if (_col == 0) {
        return "Heure";
      }
      if (_col == 1) {
        return "Minutes";
      }
      return CtuluLibString.EMPTY_STRING;
    }

    public boolean canAdd() {
      return true;
    }

    public Object createNewObject() {
      int lastH = 8;
      int lastMin = 0;
      SHeure h = last_;
      if (h == null && v_.size() > 0) {
        h = getDateHeure(v_.size() - 1);
      }
      if (h != null) {
        lastH = h.heure + 1;
        lastH = lastH % 24;
        lastMin = h.minutes;
      }
      return new SHeure(lastH, lastMin);
    }

    public Object getValueAt(final int _row, final int _col) {
      final SHeure h = getDateHeure(_row);
      if (h == null) {
        return CtuluLibString.EMPTY_STRING;
      }
      if (_col == 0) {
        return new Integer(h.heure * 3600);
      }
      if (_col == 1) {
        return new Integer(h.minutes * 60);
      }
      return CtuluLibString.EMPTY_STRING;
    }

    public void setValueAt(final Object _aValue, final int _rowIndex, final int _columnIndex) {
      if (_aValue instanceof Integer) {
        final SHeure h = getDateHeure(_rowIndex);
        boolean ok = false;
        final int val = ((Integer) _aValue).intValue();
        if (_columnIndex == 0 && tfJourDebutHeure_.VALUE_DUREE.isValueValid(_aValue)) {
          h.heure = val / 3600;
          ok = true;
        } else if (_columnIndex == 1 && tfJourDebutMinute_.VALUE_DUREE.isValueValid(_aValue)) {
          h.minutes = val / 60;
          ok = true;
        }
        if (ok) {
          last_ = h;
          fireTableCellUpdated(_rowIndex, _columnIndex);
        }

      }
    }

    protected JTable getT() {
      return t_;
    }

    protected void setT(final JTable _t) {
      t_ = _t;
    }

  }

  private final GridBagConstraints c_ = new GridBagConstraints();
  private final GridBagLayout layout_ = new GridBagLayout();
  private final BuLabel lblanc_ = new BuLabel("       ");
  private final BuLabel lBlanc2_ = new BuLabel("    ");
  private BuTextField tfNbBateaux_;

  // horaires de generation

  private BuComboBox cbGareArrivee_;

  private BuComboBox cbGareDepart_;

  private BuTextField tfOrdreLoi_;
  // Donn�es communes a toutes les lois
  // Composants pour le panel Loi Erlang
  private BuPanel pnErlang_;
  // composants pour le panel Loi Deterministe
  private BuPanel pnDeterministe_;

  // composants pour le panel de la loi journali�re
  private BuPanel pnJour_;
  private BuComboBox sensTrajet_;

  private DureeField tfErlangDebutHeure_;

  private DureeField tfErlangDebutMinute_;

  private DureeField tfErlangFinHeure_;

  private DureeField tfErlangFinMinute_;

  private BuComboBox cbTypeBateau_;
  private BuComboBox cbTypeLoi_;

  JComponent pnCurrentLoi_;
  public final ArrayList listBiefs_;

  // liste de bateaux
  public ArrayList listeBateau_;

  public final ArrayList listEcluses_;

  // liste de gares
  public final ArrayList listeGares_;

  private boolean isErlang() {
    return cbTypeLoi_.getSelectedIndex() == 0;
  }

  private int getLoiInt() {
    if (isErlang()) {
      return 0;
    }
    if (isDeterministe()) {
      return 1;
    }
    if (isJournaliere()) {
      return 2;
    }
    return -1;
  }

  private boolean isDeterministe() {
    return cbTypeLoi_.getSelectedIndex() == 1;
  }

  private boolean isJournaliere() {
    return cbTypeLoi_.getSelectedIndex() == 2;
  }

  public Sinavi2FilleAddModTrajets(final ArrayList _listeBateaux, final ArrayList _listeGares,
      final ArrayList _listeEcluses, final ArrayList _listeBiefs, final SParametresTrajets _init) {
    // super("Trajets des bateaux", true, true, true, false);
    listeGares_ = _listeGares;
    listEcluses_ = _listeEcluses;
    listBiefs_ = _listeBiefs;
    listeBateau_ = _listeBateaux;
    first_ = _init;
    /** Containtes sur les listes d�roulantes */
    sensTrajet_ = new BuComboBox(new Object[] { Sinavi2Helper.getAvMontantLongDesc(true),
        Sinavi2Helper.getAvMontantLongDesc(false) });
    cbTypeLoi_ = new BuComboBox(new Object[] { "Erlang", "Deterministe", "Journali�re" });
    cbTypeLoi_.addItemListener(new ItemListener() {
      public void itemStateChanged(final java.awt.event.ItemEvent _evt) {
        if (_evt.getStateChange() == ItemEvent.SELECTED) {
          updateLoi();
        }
      }
    });

    /** remplir la liste d�roulante de bateaux* */
    if (listeBateau_ != null) {
    	cbTypeBateau_ = new BuComboBox(listeBateau_.toArray());
    	
    	cbTypeBateau_.setRenderer(new CtuluCellTextRenderer() {
		   	

    		protected void setValue(final Object _value) {
    			super.setValue(((SParametresBateau) _value).identification);
    		}
    	});
    }
    /** remplir la liste d�roulante des gares* */
    if (listeGares_ != null) {
      final Object[] o = listeGares_.toArray();
      cbGareDepart_ = new BuComboBox(o);
      cbGareArrivee_ = new BuComboBox(o);
    }

    /** *Contraintes sur les zones de texte : focus* */
    setBorder(BuBorders.EMPTY3333);
    setLayout(new BuBorderLayout());
    /** panel commun a toutes les lois */
    final BuPanel pnDonnees = new BuPanel();
    pnDonnees.setLayout(layout_);
    c_.fill = GridBagConstraints.BOTH;
    c_.gridx = 1;
    c_.gridy = 0;
    pnDonnees.add(new BuLabel("Parametres  pour la saisie des trajets"), c_);

    c_.gridx = 0;
    c_.gridy = 1;
    pnDonnees.add(lblanc_, c_);

    c_.gridx = 0;
    c_.gridy = 2;
    pnDonnees.add(new BuLabel("Type de Bateau"), c_);

    c_.gridx = 2;
    c_.gridy = 2;
    pnDonnees.add(cbTypeBateau_, c_);

    c_.gridx = 0;
    c_.gridy = 3;
    pnDonnees.add(new BuLabel("Sens du Trajet"), c_);

    c_.gridx = 2;
    c_.gridy = 3;
    pnDonnees.add(sensTrajet_, c_);

    c_.gridx = 0;
    c_.gridy = 4;
    pnDonnees.add(new BuLabel("N� de la gare de d�part"), c_);

    c_.gridx = 2;
    c_.gridy = 4;
    pnDonnees.add(cbGareDepart_, c_);

    c_.gridx = 0;
    c_.gridy = 5;
    pnDonnees.add(new BuLabel("N� de la gare d'Arriv�e"), c_);

    c_.gridx = 2;
    c_.gridy = 5;
    pnDonnees.add(cbGareArrivee_, c_);

    c_.gridx = 0;
    c_.gridy = 6;
    pnDonnees.add(new BuLabel("Type de loi"), c_);

    c_.gridx = 2;
    c_.gridy = 6;
    pnDonnees.add(cbTypeLoi_, c_);
    add(pnDonnees, BorderLayout.NORTH);
    initialiseChamps();
    setVisible(true);
  }

  private void buildDeterministeLoi() {
    if (pnDeterministe_ != null) {
      return;
    }
    pnDeterministe_ = new BuPanel();
    pnDeterministe_.setLayout(new BuBorderLayout());
    SDateHeure[] hs = null;
    if (first_ != null && first_.loiD != null) {
      hs = first_.loiD.dateHeure;
    }
    modelDeterministe_ = new DateHeureTableModel(hs);
    final CtuluListEditorPanel ed = new CtuluListEditorPanel(modelDeterministe_, true, true, true, false, false);
    ed.setPreferredSize(new Dimension(200, 200));
    modelDeterministe_.setT(ed.getTable());
    final TableColumnModel m = ed.getTable().getColumnModel();
    // mise en place des editeurs
    m.getColumn(0).setCellEditor(modelDeterministe_.getEditorCol0());
    m.getColumn(1).setCellEditor(modelDeterministe_.getEditorCol1());
    m.getColumn(2).setCellEditor(modelDeterministe_.getEditorCol2());
    // mise en place des renderers
    m.getColumn(1).setCellRenderer(modelDeterministe_.getRendererCol1());
    m.getColumn(2).setCellRenderer(modelDeterministe_.getRendererCol2());

    pnDeterministe_.add(ed);
  }

  private void buildErlangPn() {
    if (pnErlang_ != null) {
      return;
    }
    pnErlang_ = new BuPanel();
    pnErlang_.setLayout(layout_);
    c_.gridx = 0;
    c_.gridy = 7;
    pnErlang_.add(new BuLabel("Heures de G�n�ration"), c_);
    c_.gridx = 1;
    c_.gridy = 8;
    final BuLabel lErlangHoraireDebut = new BuLabel("Heures de D�but");
    pnErlang_.add(lErlangHoraireDebut, c_);
    c_.gridx = 2;
    c_.gridy = 8;
    tfErlangDebutHeure_ = new DureeField(false, false, true, false, false);
    lErlangHoraireDebut.setLabelFor(tfErlangDebutHeure_);
    pnErlang_.add(tfErlangDebutHeure_, c_);
    tfErlangDebutHeure_.setDureeValidator(DureeFieldValidator.creeMax(82800));

    c_.gridx = 3;
    c_.gridy = 8;
    tfErlangDebutMinute_ = new DureeField(false, false, false, true, false);
    pnErlang_.add(tfErlangDebutMinute_, c_);
    tfErlangDebutMinute_.setDureeValidator(DureeFieldValidator.creeMax(3599));

    c_.gridx = 1;
    c_.gridy = 9;
    final BuLabel lbErlangHoraireFin = new BuLabel("Heures de Fin");
    pnErlang_.add(lbErlangHoraireFin, c_);
    c_.gridx = 2;
    c_.gridy = 9;
    tfErlangFinHeure_ = new DureeField(false, false, true, false, false);
    lbErlangHoraireFin.setLabelFor(tfErlangFinHeure_);
    pnErlang_.add(tfErlangFinHeure_, c_);
    tfErlangFinHeure_.setDureeField(86400);
    DureeFieldValidator.creeMinMax(tfErlangFinHeure_, tfErlangDebutHeure_.getDureeField()
        + tfErlangDebutMinute_.getDureeField(), 86400);
    c_.gridx = 3;
    c_.gridy = 9;
    tfErlangFinMinute_ = new DureeField(false, false, false, true, false);
    pnErlang_.add(tfErlangFinMinute_, c_);
    tfErlangFinMinute_.setDureeValidator(DureeFieldValidator.creeMax(3599));

    c_.gridx = 0;
    c_.gridy = 10;
    pnErlang_.add(lBlanc2_, c_);

    c_.gridx = 0;
    c_.gridy = 11;
    pnErlang_.add(new BuLabel("Ordre de la loi"), c_);
    c_.gridx = 1;
    c_.gridy = 11;
    final CtuluValueEditorInteger ordreLoiEd = new CtuluValueEditorInteger(false);
    ordreLoiEd.setEditable(true);
    ordreLoiEd.setVal(new CtuluValueValidator.IntMin(0));
    tfOrdreLoi_ = (BuTextField) ordreLoiEd.createEditorComponent();
    ordreLoiEd.setValue(new Integer(1), tfOrdreLoi_);
    pnErlang_.add(tfOrdreLoi_, c_);
    c_.gridx = 0;
    c_.gridy = 12;
    pnErlang_.add(new BuLabel("Nombre de Bateaux attendus"), c_);
    c_.gridx = 1;
    c_.gridy = 12;
    tfNbBateaux_ = (BuTextField) ordreLoiEd.createEditorComponent();
    ordreLoiEd.setValue(new Integer(1), tfNbBateaux_);
    pnErlang_.add(tfNbBateaux_, c_);
    final List dureeList = new ArrayList();
    tfErlangDebutMinute_.setDureeValidator(DureeFieldValidator.creeMax(3600,
        "Entrez un horaire de d�but de g�n�ration correct"));
    dureeList.add(tfErlangDebutMinute_);
    tfErlangDebutHeure_.setDureeValidator(DureeFieldValidator.creeMax(86400,
        "Entrez un horaire de d�but de g�n�ration correct"));
    dureeList.add(tfErlangDebutHeure_);
    tfErlangFinMinute_.setDureeValidator(DureeFieldValidator.creeMax(3600,
        "Entrez un horaire de fin de g�n�ration correct"));
    dureeList.add(tfErlangFinMinute_);
    tfErlangFinHeure_.setDureeValidator(DureeFieldValidator.creeMax(86400,
        "Entrez un horaire de fin g�n�ration correct"));
    dureeList.add(tfErlangFinHeure_);
    if (first_ != null && first_.loiE != null) {
      tfOrdreLoi_.setValue(new Integer(first_.loiE.ordreDeLaLoi));
      tfNbBateaux_.setValue(new Integer(first_.loiE.nbreBateauxAttendus));
      tfErlangDebutHeure_.setValueHeure(first_.loiE.heureDebutGeneration.heure);
      tfErlangDebutMinute_.setValueMin(first_.loiE.heureDebutGeneration.minutes);
      tfErlangFinHeure_.setValueHeure(first_.loiE.heureFinGeneration.heure);
      tfErlangFinMinute_.setValueMin(first_.loiE.heureFinGeneration.minutes);
    }

  }

  HeureTableModel modelJourn_;
  DateHeureTableModel modelDeterministe_;

  private void buildPnJournaliere() {
    if (pnJour_ != null) {
      return;
    }
    pnJour_ = new BuPanel();
    pnJour_.setLayout(new BuBorderLayout());
    SHeure[] hs = null;
    if (first_ != null && first_.loiJ != null) {
      hs = first_.loiJ.heureJ;
    }
    modelJourn_ = new HeureTableModel(hs);
    final CtuluListEditorPanel pn = new CtuluListEditorPanel(modelJourn_, true, true, true, false, false);
    pn.setPreferredSize(new Dimension(300, 300));
    final TableColumnModel m = pn.getTable().getColumnModel();
    modelJourn_.setT(pn.getTable());
    // les editeurs
    m.getColumn(0).setCellEditor(modelJourn_.getEditorCol0());
    m.getColumn(1).setCellEditor(modelJourn_.getEditorCol1());
    // les renderer
    m.getColumn(0).setCellRenderer(modelJourn_.getRendererCol0());
    m.getColumn(1).setCellRenderer(modelJourn_.getRendererCol1());
    pnJour_.add(pn);
  }

  public boolean valide() {
    return isParamsValide();
  }

  private boolean isParamsValide() {
    final StringBuffer buf = new StringBuffer();
    final Vector v = BuLib.getAllSubComponents(this);
    final int nb = v.size();
    boolean errorInTxt = false;
    for (int i = 0; i < nb; i++) {
      if (v.get(i) instanceof BuTextField) {
        final BuTextField tf = (BuTextField) v.get(i);
        final boolean error = (tf.getValue() == null);
        tf.setForeground(error ? Color.RED : CtuluLibSwing.getDefaultTextFieldForegroundColor());
        final Object lb = tf.getClientProperty("labeledBy");
        if (lb != null && lb instanceof JComponent) {
          ((JComponent) lb).setForeground(error ? Color.RED : CtuluLibSwing.getDefaultLabelForegroundColor());
        }
        if (error) {
          errorInTxt = true;
        }
      }
    }
    if (errorInTxt) {
      buf.append("Les champs �crits en rouge sont erron�s\n");
    }
    final ArrayList listeBiefs = new ArrayList();
    final ArrayList listeEcluses = new ArrayList();
    if (!rechercheTrajet(Integer.parseInt(cbGareDepart_.getSelectedItem().toString()), Integer.parseInt(cbGareArrivee_
        .getSelectedItem().toString()), Integer.parseInt(cbGareDepart_.getSelectedItem().toString()), null, false,
        false, !isAvalSelected(), false, listeEcluses, listeBiefs)) {
      buf.append("Le trajet saisi est incorrect\n");
    }
    String err = verifBiefs(listeBiefs);
    if (err != null) {
      buf.append(err).append(CtuluLibString.LINE_SEP);
    }
    err = verifEcluses(listeEcluses);
    if (err != null) {
      buf.append(err).append(CtuluLibString.LINE_SEP);
    }

    if (isErlang()) {
      if (tfErlangFinHeure_.getDureeField() + tfErlangFinMinute_.getDureeField() > 86400) {
        buf.append("Une journ�e ne peut d�passer 24h\n");
      }
    } else if (isDeterministe()) {
      if (!modelDeterministe_.isValid()) {
        buf.append(Sinavi2Resource.SINAVI2.getString("Auncune date utilis�e"));
      }
    } else if (isJournaliere() && !modelJourn_.isValid()) {
      buf.append(Sinavi2Resource.SINAVI2.getString("Auncune date utilis�e"));
    }
    if (buf.length() > 0) {
      setErrorText(buf.toString());
      return false;
    }
    cancelErrorText();
    return true;
  }

  SParametresTrajets buildTrajet() {
    final SParametresTrajets trajet = new SParametresTrajets();
    trajet.bateau = ((SParametresBateau) cbTypeBateau_.getSelectedItem()).identification;
    trajet.gareDepart = Integer.parseInt((String) cbGareDepart_.getSelectedItem());
    trajet.gareArrivee = Integer.parseInt((String) cbGareArrivee_.getSelectedItem());
    final Object s = sensTrajet_.getSelectedItem();
    trajet.avalantMontant = Sinavi2Helper.isAval(s.toString());
    trajet.typeDeLoi = getLoiInt();
    if (isErlang()) {
      trajet.loiE = new SParametresLoiE();
      trajet.loiE.heureDebutGeneration = new SHeure(tfErlangDebutHeure_.getHeure(), tfErlangDebutMinute_.getMin());
      trajet.loiE.heureFinGeneration = new SHeure(tfErlangFinHeure_.getHeure(), tfErlangFinMinute_.getMin());
      trajet.loiE.nbreBateauxAttendus = Integer.parseInt(tfNbBateaux_.getText());
      trajet.loiE.ordreDeLaLoi = ((Integer) tfOrdreLoi_.getValue()).intValue();

    } else if (isDeterministe()) {
      trajet.loiD = new SParametresLoiD();
      trajet.loiD.dateHeure = modelDeterministe_.getDateHeures();
    }

    else if (isJournaliere()) {
      trajet.loiJ = new SParametresLoiJ(modelJourn_.getDateHeures());
    }
    return trajet;
  }

  private void rechercheAmontAval(final int _gareCourante, final ArrayList _listeAmontBief,
      final ArrayList _listeAvalBief, final ArrayList _listeAmontEcluse, final ArrayList _listeAvalEcluse,
      final ArrayList _listeEcluses, final ArrayList _listeBiefs) {
    final ListIterator le = _listeEcluses.listIterator();
    final ListIterator lb = _listeBiefs.listIterator();
    while (le.hasNext()) {
      final SParametresEcluse e = (SParametresEcluse) le.next();
      if (e.gareEnAval == _gareCourante) {
        _listeAmontEcluse.add(e);
      }
      if (e.gareEnAmont == _gareCourante) {
        _listeAvalEcluse.add(e);
      }
    }

    while (lb.hasNext()) {
      final SParametresBief b = (SParametresBief) lb.next();
      if (b.gareEnAval == _gareCourante) {
        _listeAmontBief.add(b);
      }
      if (b.gareEnAmont == _gareCourante) {
        _listeAvalBief.add(b);
      }

    }

  }

  /**
   * num�ro de la gare de d�but num�ro de la gare de fin liste des gares �tudi�es bool�en trouve ou bon chemin sens
   * liste des �cluses et liste des biefs la m�thode est appel�e r�cursivement jusq�� temps qu'on ai trouv� si le chemin
   * est bon ou faux.
   */

  private boolean rechercheTrajet(final int _deb, final int _fin, final int _enCours, final int[] _listeGareEtudiees,
      final boolean _trouve, boolean _bonChemin, boolean _montant, final boolean _deuxSens,
      final ArrayList _listeEcluses, final ArrayList _listeBiefs) {
    // le sens est utilis� pour la premiere fois
    // on recherche dans ce sens pour la premiere fois
    // A M ou 2 (pour les autres fois

    System.out.println("rechercher trajet");
    final ArrayList listeAmontBief = new ArrayList();
    final ArrayList listeAvalBief = new ArrayList();
    final ArrayList listeAmontEcluse = new ArrayList();
    final ArrayList listeAvalEcluse = new ArrayList();
    int[] listeGareEtudiees;
    if (_listeGareEtudiees != null) {
      listeGareEtudiees = _listeGareEtudiees;
    } else {
      listeGareEtudiees = new int[0];
    }
    ListIterator it;
    if (_enCours != _fin && !_bonChemin) {
      rechercheAmontAval(_enCours, listeAmontBief, listeAvalBief, listeAmontEcluse, listeAvalEcluse, listEcluses_,
          listBiefs_);
      if (_montant || _deuxSens) {
        if (listeAmontBief.size() > 0) {
          System.out.println("liste bief amont");
          it = listeAmontBief.listIterator();
          while (it.hasNext()) {
            final SParametresBief bief = (SParametresBief) it.next();
            final int garePrec = bief.gareEnAmont;
            if (_enCours == _fin) {
              _listeBiefs.add(bief);
              _bonChemin = true;
            } else if (gareEtudiee(listeGareEtudiees, garePrec)) {
              // System.out.println("on fait rien");
            } else {
              listeGareEtudiees = addGare(listeGareEtudiees, _enCours);
              _listeBiefs.add(bief);
              if (rechercheTrajet(_deb, _fin, garePrec, listeGareEtudiees, _trouve, _bonChemin, false, true,
                  _listeEcluses, _listeBiefs)) {
                _bonChemin = true;
              }
            }
            if (_bonChemin) {
              return true;
            }

          }
        }
        if (listeAmontEcluse.size() > 0 && !_bonChemin) {
          System.out.println("liste ecluse amont");
          listeGareEtudiees = _listeGareEtudiees;
          it = listeAmontEcluse.listIterator();
          while (it.hasNext()) {
            final SParametresEcluse ecluse = (SParametresEcluse) it.next();
            final int garePrec = ecluse.gareEnAmont;
            if (_enCours == _fin) {
              _bonChemin = true;
              _listeEcluses.add(ecluse);
            } else if (gareEtudiee(listeGareEtudiees, garePrec)) {

            } else {
              listeGareEtudiees = addGare(listeGareEtudiees, _enCours);
              _listeEcluses.add(ecluse);
              if (rechercheTrajet(_deb, _fin, garePrec, listeGareEtudiees, _trouve, _bonChemin, false, true,
                  _listeEcluses, _listeBiefs)) {
                _bonChemin = true;
              }
            }
            if (_bonChemin) {
              return true;
            }
          }
        }
      }
      if (!_montant || _deuxSens) {
        if (listeAvalBief.size() > 0 && !_bonChemin) {
          System.out.println("liste bief aval");
          listeGareEtudiees = _listeGareEtudiees;
          it = listeAvalBief.listIterator();
          while (it.hasNext()) {
            final SParametresBief bief = (SParametresBief) it.next();
            final int gareSuiv = bief.gareEnAval;
            if (_enCours == _fin) {
              _bonChemin = true;
              _listeBiefs.add(bief);
            } else if (gareEtudiee(listeGareEtudiees, gareSuiv)) {
              // System.out.println("on fait rien");
            } else {
              _listeBiefs.add(bief);
              listeGareEtudiees = addGare(listeGareEtudiees, _enCours);
              if (rechercheTrajet(_deb, _fin, gareSuiv, listeGareEtudiees, _trouve, _bonChemin, false, true,
                  _listeEcluses, _listeBiefs)) {
                _bonChemin = true;
              }
            }
            if (_bonChemin) {
              return true;
            }
          }
        }

        if (listeAvalEcluse.size() > 0 && !_bonChemin) {
          System.out.println("liste ecluse aval");
          listeGareEtudiees = _listeGareEtudiees;
          it = listeAvalEcluse.listIterator();
          while (it.hasNext()) {
            final SParametresEcluse ecluse = (SParametresEcluse) it.next();
            final int gareSuiv = ecluse.gareEnAval;
            if (_enCours == _fin) {
              _bonChemin = true;
              _listeEcluses.add(ecluse);
            } else if (gareEtudiee(listeGareEtudiees, gareSuiv)) {

            } else {
              _listeEcluses.add(ecluse);
              listeGareEtudiees = addGare(listeGareEtudiees, _enCours);
              if (rechercheTrajet(_deb, _fin, gareSuiv, listeGareEtudiees, _trouve, _bonChemin, false, true,
                  _listeEcluses, _listeBiefs)) {
                _bonChemin = true;
              }
            }
            if (_bonChemin) {
              return true;
            }
          }

        }
      }
    } else {
      System.out.println("chemin = true");
      return true;
    }
    return false;
  }
  SParametresTrajets first_;

  void updateLoi() {
    if (pnCurrentLoi_ != null) {
      remove(pnCurrentLoi_);
    }
    if (isErlang()) {
      buildErlangPn();
      pnCurrentLoi_ = pnErlang_;
    } else if (isDeterministe()) {
      buildDeterministeLoi();
      pnCurrentLoi_ = pnDeterministe_;

    } else if (isJournaliere()) {
      buildPnJournaliere();
      pnCurrentLoi_ = pnJour_;

    }
    add(pnCurrentLoi_, BuBorderLayout.CENTER);
    final Object o = SwingUtilities.getAncestorOfClass(Dialog.class, this);
    if (o != null) {
      ((Dialog) o).pack();
    }
  }

  private String verifBiefs(final ArrayList _listeBiefs) {
    final SParametresBateau bat = (SParametresBateau) listeBateau_.get(cbTypeBateau_.getSelectedIndex());
    if (_listeBiefs != null) {
      final ListIterator it = _listeBiefs.listIterator();
      while (it.hasNext()) {
        final SParametresBief b = (SParametresBief) it.next();
        if (b.hauteur < bat.tirantDeau) {
          return ("Le type de bateau " + bat.identification
              + " a un tirant d'eau trop important pour passer dans le bief " + b.identification);
        }
        if (b.largeur < bat.largeur) {
          return "La largeur du type de bateau " + bat.identification + " est sup�rieur � celle du bief "
              + b.identification;
        }
      }
    }
    return null;
  }

  /*
   * public void focusLost(FocusEvent _e) { processFocusEvent(new FocusEvent(this, FocusEvent.FOCUS_LOST)); if
   * (_e.getSource() == ordreLoi_) { if (ordreLoi_.getText().equalsIgnoreCase("")){ ordreLoi_.setForeground(Color.RED); }
   * else ordreLoi_.setForeground(CtuluLib.getDefaultLabelForegroundColor()); } else if (_e.getSource() == nbrebateaux_) {
   * if (nbrebateaux_.getText().equalsIgnoreCase("")){ nbrebateaux_.setForeground(Color.RED); } else
   * nbrebateaux_.setForeground(CtuluLib.getDefaultLabelForegroundColor()); } else if (_e.getSource() == date_) { if
   * (date_.getText().equalsIgnoreCase("")){ date_.setForeground(Color.RED); } else
   * date_.setForeground(CtuluLib.getDefaultLabelForegroundColor()); } }
   */

  private boolean verifEcluseMultiple(final int _gareAmont, final int _gareAval, final double _tirantDEau,
      final double _largeur, final double _longueur) {
    final ListIterator it = listEcluses_.listIterator();
    while (it.hasNext()) {
      final SParametresEcluse b = (SParametresEcluse) it.next();
      if (b.profondeur >= _tirantDEau && b.largeur >= _largeur && b.gareEnAmont == _gareAmont
          && b.gareEnAval == _gareAval && b.longueur >= _longueur) {
        return true;
      }
    }

    return false;
  }

  private String verifEcluses(final ArrayList _listeEcluses) {
    final SParametresBateau bat = (SParametresBateau) listeBateau_.get(cbTypeBateau_.getSelectedIndex());
    if (_listeEcluses != null) {
      final ListIterator it = _listeEcluses.listIterator();
      while (it.hasNext()) {
        final SParametresEcluse b = (SParametresEcluse) it.next();
        if (b.profondeur < bat.tirantDeau || b.largeur < bat.largeur || b.longueur < bat.longueur) {
          if (!verifEcluseMultiple(b.gareEnAmont, b.gareEnAval, bat.tirantDeau, bat.largeur, bat.longueur)) {
            return "Le bateau " + bat.identification
                + " ne peut stationner dans aucune des �cluses situ�es entre les gares " + b.gareEnAmont + " et "
                + b.gareEnAval + CtuluLibString.DOT;
          }

        }
      }
    }
    return null;
  }

  public int[] addGare(final int[] _liste, final int _gare) {
    int[] temp;
    if (_liste != null) {
      temp = new int[_liste.length + 1];
      for (int i = 0; i < _liste.length; i++) {
        temp[i] = _liste[i];
      }
    } else {
      temp = new int[1];
    }
    temp[temp.length - 1] = _gare;
    return temp;
  }

  public boolean gareEtudiee(final int[] _liste, final int _gare) {
    if (_liste != null) {
      for (int i = 0; i < _liste.length; i++) {
        if (_liste[i] == _gare) {
          return true;
        }
      }
    }
    return false;
  }

  /** M�thode permettant d'initialiser les champs pour la loi erlang. */
  private void initialiseChamps() {
    if (first_ == null) {
      cbTypeLoi_.setSelectedIndex(0);
      updateLoi();
      tfErlangDebutHeure_.setDureeField(0);
      tfErlangDebutMinute_.setDureeField(0);
      tfErlangFinHeure_.setDureeField(86400);
      tfErlangFinMinute_.setDureeField(0);
      tfOrdreLoi_.setText(CtuluLibString.UN);
      tfNbBateaux_.setText(CtuluLibString.UN);
    } else {
      cbTypeBateau_.setSelectedIndex(Sinavi2Lib.getBateau(listeBateau_, first_.bateau));
      sensTrajet_.setSelectedItem(Sinavi2Helper.getAvMontantLongDesc(first_.avalantMontant));
      cbGareDepart_.setSelectedItem(Integer.toString(first_.gareDepart));
      cbGareArrivee_.setSelectedItem(Integer.toString(first_.gareArrivee));
      cbTypeLoi_.setSelectedIndex(first_.typeDeLoi);
      updateLoi();

    }
  }

  public boolean isAvalSelected() {
    return sensTrajet_.getSelectedIndex() == 0;
  }

  public static boolean isParamsOk(final Sinavi2Implementation _impl) {
    if (CtuluLibArray.isEmpty(_impl.listeBateaux_)) {
      _impl.error(Sinavi2Resource.SINAVI2.getString("La liste des bateaux est vide"));
      return false;
    }
    if (CtuluLibArray.isEmpty(_impl.listeGares_)) {
      _impl.error(Sinavi2Resource.SINAVI2.getString("La liste des gares est vide"));
      return false;
    }
    return true;
  }

  public static void addTrajet(final Sinavi2Implementation _impl) {
    if (!isParamsOk(_impl)) {
      return;
    }
    if (_impl.listeTrajets_ == null) {
      _impl.listeTrajets_ = new Sinavi2TrajetMng();
    }
    final Sinavi2TrajetMng mng = _impl.listeTrajets_;
    final Sinavi2FilleAddModTrajets tjts = new Sinavi2FilleAddModTrajets(_impl.listeBateaux_, _impl.listeGares_,
        _impl.listeEcluses_, _impl.listeBiefs_, null);
    if (tjts.afficheModaleOk(_impl.getFrame(), Sinavi2Resource.SINAVI2.getString("Ajouter un trajet"))) {
      mng.add(tjts.buildTrajet());
    }
  }

  public static void modifyTrajet(final Sinavi2Implementation _impl, final int _idx) {
    if (!isParamsOk(_impl)) {
      return;
    }
    if (_impl.listeTrajets_ == null) {
      return;
    }
    final Sinavi2TrajetMng mng = _impl.listeTrajets_;
    final Sinavi2FilleAddModTrajets tjts = new Sinavi2FilleAddModTrajets(_impl.listeBateaux_, _impl.listeGares_,
        _impl.listeEcluses_, _impl.listeBiefs_, mng.getTrajet(_idx));
    if (tjts.afficheModaleOk(_impl.getFrame(), Sinavi2Resource.SINAVI2.getString("Modifier le trajet {0}",
        CtuluLibString.getString(_idx + 1)))) {
      mng.set(_idx, tjts.buildTrajet());
    }
  }

}
