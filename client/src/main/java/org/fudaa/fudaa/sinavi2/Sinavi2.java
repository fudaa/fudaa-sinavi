/*
 * @creation     2005-09-12
 * @modification $Date: 2007-01-19 13:14:32 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.fr
 */
package org.fudaa.fudaa.sinavi2;

import org.fudaa.fudaa.commun.impl.Fudaa;

/**
 * Permet de lancer l'application cliente de SINAVI2.
 * 
 * @version $Revision: 1.6 $ $Date: 2007-01-19 13:14:32 $ by $Author: deniger $
 * @author Beno�t Maneuvrier, Fatimatou Ka
 */
public final class Sinavi2 {

  private Sinavi2() {}

  /**
   * main de Sinavi2 class.
   * 
   * @param _args The command line arguments
   */
  public static void main(final String[] _args) {
    final Fudaa f = new Fudaa();
    f.launch(_args, Sinavi2Implementation.informationsSoftware(), true);
    f.startApp(new Sinavi2Implementation());
  }
}
