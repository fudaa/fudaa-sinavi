package org.fudaa.fudaa.sinavi2;

import java.text.NumberFormat;

import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluLibString;

/**
 * @author maneuvrier
 */
public final class SinaviLib {

  private SinaviLib() {

  }

  public static String getS(final String _chFrancais) {
    return Sinavi2Resource.SINAVI2.getString(_chFrancais);
  }

  public static String deuxChiffresApresVirgule(final String _ch) {
    // INTERDIT et bizarre
    //String s = new String();
    final int x = _ch.length();
    if (_ch.charAt(x - 2) == '.') {
      return  _ch + "0";
    } //else {
      return _ch;
    //}

  }

  public static double conversionDeuxChiffres(final double value) {
    final NumberFormat formatter = CtuluLib.getDecimalFormat();
    formatter.setMaximumFractionDigits(2);
    formatter.setMinimumFractionDigits(2);
    final String s = formatter.format(value);
    return Double.parseDouble(s);
  }

  public static String deuxChiffresApresVirguleFred(final double _value) {
    // il vaut mieux utiliser un decimal format.
    // Faire attention: utiliser des . au lieu des , par commodité ...
    // si cette methode est beaucoup utilise, il vaudrait mieux conserver le
    // fmt dans une variables statique !
    final NumberFormat formatter = CtuluLib.getDecimalFormat();
    formatter.setMaximumFractionDigits(2);
    formatter.setMinimumFractionDigits(2);
    return formatter.format(_value);

  }

  public static String determineHeureString(final int _nbSecondes) {
    /*
     * String s2=""+_nbSecondes; int nbS=Integer.parseInt(s2);
     */
    final int heure = _nbSecondes / 3600;
    final int minute = _nbSecondes / 60 - heure * 60;
    String s = "";
    if (minute > 9) {
      s = heure + CtuluLibString.DOT + minute;
    } else {
      s = heure + ".0" + minute;
    }
    System.out.println("nb seconde :" + _nbSecondes + " -> " + s);
    return s;
  }

}
