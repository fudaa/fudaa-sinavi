package org.fudaa.fudaa.sinavi2;

import org.fudaa.dodico.corba.sinavi2.SParametresCroisements;

/**
 * @author maneuvrier Cette classe pourrait �tre utilis�e pour encapsuler le croisement mais elle n'est pas utilis�e. On
 *         traite directement la structure de l'IDL.
 */
class Sinavi2TypeCroisement {

  // constructeur
  public Sinavi2TypeCroisement() {}

  public Sinavi2TypeCroisement(final String _bief, final String _type1, final String _type2, final boolean _ouiNon) {
    croi_.bief = _bief;
    croi_.type1 = _type1;
    croi_.type2 = _type2;
    croi_.ouiNon = _ouiNon;

  }

  /** ******accesseurs********* */
  public String getBief() {
    return croi_.bief;
  }

  public String getType1() {
    return croi_.type1;
  }

  public String getType2() {
    return croi_.type2;
  }

  public boolean getOuiNon() {
    return croi_.ouiNon;
  }

  /** ***** modifieurs*************** */
  public void setBief(final String _bief) {
    croi_.bief = _bief;
  }

  public void setType1(final String _type1) {
    croi_.type1 = _type1;
  }

  public void setType2(final String _type2) {
    croi_.type2 = _type2;
  }

  public void setOuiNon(final boolean _ouiNon) {
    croi_.ouiNon = _ouiNon;
  }

  public boolean typeCroisementsEquals(final Sinavi2TypeCroisement _c) {
    return (this.getBief().equalsIgnoreCase(_c.getBief()) && this.getType1().equalsIgnoreCase(_c.getType1()) && this
        .getType2().equalsIgnoreCase(_c.getType2()));
  }
  SParametresCroisements croi_;
}
