package org.fudaa.fudaa.sinavi2;

import gnu.trove.TIntArrayList;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.swing.table.AbstractTableModel;

import jxl.write.Label;
import jxl.write.Number;
import jxl.write.WritableCell;

import com.memoire.bu.BuDialogConfirmation;

import org.fudaa.ctulu.CtuluHtmlWriter;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluUI;

import org.fudaa.dodico.corba.sinavi2.SDateHeure;
import org.fudaa.dodico.corba.sinavi2.SHeure;
import org.fudaa.dodico.corba.sinavi2.SParametresBateau;
import org.fudaa.dodico.corba.sinavi2.SParametresLoiD;
import org.fudaa.dodico.corba.sinavi2.SParametresLoiE;
import org.fudaa.dodico.corba.sinavi2.SParametresLoiJ;
import org.fudaa.dodico.corba.sinavi2.SParametresTrajets;

import org.fudaa.dodico.sinavi2.Sinavi2Helper;

/**
 * @author ka Cette classe a pour but d'afficher le tableau des trajets (c'est � dire le parcours d'un bateau) et
 *         d'offrir la possibilit� de l'impression c'est � dire la cr�ation d'un tableau excel d'o� l'impl�mentation de
 *         CtuluTableModelInterface.
 */

public class Sinavi2TrajetMng extends AbstractTableModel implements org.fudaa.ctulu.table.CtuluTableModelInterface {

  private final String erlangName_ = "Erlang";
  private final String[] nomColonnes_ = { getHtmlColName("Bateau"), getHtmlColName("Sens"), getHtmlColName("D�part"),
      getHtmlColName("Arrivee"), getHtmlColName("Type de loi"), getHtmlColName(erlangName_, "D�but"),
      getHtmlColName(erlangName_, "Fin"), getHtmlColName(erlangName_, "Ordre loi"),
      getHtmlColName(erlangName_, "Bateaux attendu"), getHtmlColName("D�terministe"),
      getHtmlColName("Journali�re", "Heure") };

  public Sinavi2TrajetMng(final SParametresTrajets[] _trajets, final List _bateau, final CtuluUI _ui) {
    super();
    setTrajets(_trajets, _bateau, _ui);
  }

  public Sinavi2TrajetMng() {
    super();
    trajets_ = new ArrayList();
  }
  
     public int[] getSelectedRows() {
        return null;
    }


  public SParametresTrajets[] getTrajets() {
    return (SParametresTrajets[]) trajets_.toArray(new SParametresTrajets[trajets_.size()]);
  }

  public SParametresTrajets getTrajet(final int _i) {
    return (SParametresTrajets) trajets_.get(_i);
  }

  public void add(final SParametresTrajets _t) {
    trajets_.add(_t);
    fireTableRowsInserted(size() - 1, size() - 1);
  }

  public static String getHtmlColName(final String _s) {
    return getHtmlColName(_s, "&nbsp;");
  }

  private SParametresTrajets getBateau(final int _idxLine) {
    return (SParametresTrajets) trajets_.get(_idxLine);
  }

  public void supprimerTrajets(final int _x, final Sinavi2Implementation _impl) {
    final SParametresTrajets t = getBateau(_x);
    final BuDialogConfirmation conf = new BuDialogConfirmation(_impl, _impl.getInformationsSoftware(),
        "Voulez vous supprimer le trajet du bateau " + t.bateau + " ???");
    if (conf.activate() == 0) {
      delete(_x);
      _impl.affMessage("Le type de bateau " + t.bateau + " est supprim� !");
    } else {

      _impl.affMessage("Le trajet du type de bateau " + t.bateau + " est conserv� !");
    }
  }

  private void delete(final int[] _idx) {
    if (_idx == null) {
      return;
    }
    CtuluLibArray.remove(trajets_, _idx);
    fireTableRowsDeleted(_idx[0], _idx[_idx.length - 1]);
  }

  private void delete(final int _idx) {
    if (_idx < 0 || _idx >= trajets_.size()) {
      return;
    }
    trajets_.remove(_idx);
    fireTableRowsDeleted(_idx, _idx);
  }

  public void supprimerTrajets(final int[] _x, final Sinavi2Implementation _impl) {
    final BuDialogConfirmation conf = new BuDialogConfirmation(_impl, _impl.getInformationsSoftware(),
        "Voulez vous supprimer les trajets s�lectionn�s ?");
    if (conf.activate() == 0) {
      delete(_x);
      _impl.affMessage("Trajets supprim�s");
    } else {

      _impl.affMessage("Trajets conserv�s");
    }

  }

  public static String getHtmlColName(final String _s, final String _s2) {
    return "<html><body><b>" + _s + "</b><br>" + _s2 + "</body></html>";
  }

  ArrayList trajets_;

  /**
   * nombre de colonnes.
   */
  public int getColumnCount() {

    return nomColonnes_.length;
  }

  /**
   * nombre de lignes.
   */
  public int getRowCount() {
    return trajets_ == null ? 0 : trajets_.size();
  }

  public String getDeterministeDates(final SParametresTrajets _t) {
    final CtuluHtmlWriter writer = createWriter();
    final int nb = _t.loiD.dateHeure.length;
    for (int i = 0; i < nb; i++) {
      final CtuluHtmlWriter.Tag t = writer.addTag("tr");
      writer.addTagAndText("td", Integer.toString(_t.loiD.dateHeure[i].date));
      writer.addTagAndText("td", getHeureMinute(_t.loiD.dateHeure[i]));
      t.close();

    }
    return writer.toString();
  }

  protected CtuluHtmlWriter createWriter() {
    final CtuluHtmlWriter writer = new CtuluHtmlWriter(true, false);
    writer.addStyledTag("table", "border:1px solid black;cellpadding:1px;cellspacing:0px", "border='1'");
    return writer;
  }

  public String getJournaliereHeure(final SParametresTrajets _t) {
    final CtuluHtmlWriter writer = createWriter();
    final int nb = _t.loiJ.heureJ.length;
    for (int i = 0; i < nb; i++) {
      final CtuluHtmlWriter.Tag t = writer.addTag("tr");
      writer.addTagAndText("td", getHeureMinute(_t.loiJ.heureJ[i]));
      t.close();

    }
    return writer.toString();
  }

  public String getHeureMinute(final SDateHeure _dh) {
    return getHeureMinute(_dh.heure);
  }

  public String getHeureMinute(final SHeure _dh) {
    return Sinavi2Helper.format(_dh.heure, _dh.minutes);
  }

  /**
   * retourne la valeur du tableau � la ligne _rowIndex et � la colonne _columnIndex_.
   */
  public Object getValueAt(final int _rowIndex, final int _columnIndex) {
    if (_rowIndex < 0) {
      return CtuluLibString.EMPTY_STRING;
    }
    final SParametresTrajets t = getBateau(_rowIndex);
    switch (_columnIndex) {
    case 0:
      return t.bateau;
    case 1:
      return Sinavi2Helper.getAvMontantLongDesc(t.avalantMontant);
    case 2:
      return new Integer(t.gareDepart);
    case 3:
      return new Integer(t.gareArrivee);
    case 4:
      return Sinavi2Helper.getTypeLoi(t.typeDeLoi);
    case 5:
      if (Sinavi2Helper.isErlang(t)) {
        return getHeureMinute(t.loiE.heureDebutGeneration);
      }
      return CtuluLibString.EMPTY_STRING;
    case 6:
      if (Sinavi2Helper.isErlang(t)) {
        return getHeureMinute(t.loiE.heureFinGeneration);
      }
      return CtuluLibString.EMPTY_STRING;
    case 7:
      if (Sinavi2Helper.isErlang(t)) {
        return new Integer(t.loiE.ordreDeLaLoi);
      }
      return CtuluLibString.EMPTY_STRING;
    case 8:
      if (Sinavi2Helper.isErlang(t)) {
        return new Integer(t.loiE.nbreBateauxAttendus);
      }
      return CtuluLibString.EMPTY_STRING;
    case 9:
      if (Sinavi2Helper.isDeterministe(t)) {
        return getDeterministeDates(t);
      }
      return CtuluLibString.EMPTY_STRING;
    case 10:
      if (Sinavi2Helper.isJournaliere(t)) {
        return getJournaliereHeure(t);
      }
      return CtuluLibString.EMPTY_STRING;
    default:
      return CtuluLibString.EMPTY_STRING;
    }

  }

  /**
   * retourne le nombre de colonnes.
   */
  public int getMaxCol() {
    return getColumnCount();
  }

  /**
   * retourne le nombre de lignes.
   */
  public int getMaxRow() {
    return getRowCount();
  }

  /**
   * retourne la valeur du tableau � la ligne _row et � la colonne _col.
   */

  public Object getValue(final int _row, final int _col) {
    return getValueAt(_row, _col);
  }

  /**
   * retoune un tableau pour le format excel. Celui-ci sera utilis� avec la fonction write (cf classe
   * Sinavi2FilleAffTrajet : actionPerformed -> imprimer
   * 
   * @see org.fudaa.ctulu.table.CtuluTableModelInterface#getExcelWritable(int, int)
   */
  public WritableCell getExcelWritable(final int _row, final int _col, int _rowXls, int _colXls) {
    final int r = _row;
    final int c = _col;
    final Object o = getValue(r, c);
    if (o == null) {
      return null;
    }
    String s = o.toString();
    if (s == null) {
      return null;
    }
    s = s.trim();
    if (s.length() == 0) {
      return null;
    }
    try {
      return new Number(_colXls, _rowXls, Double.parseDouble(s));
    } catch (final NumberFormatException e) {}
    return new Label(_colXls, _rowXls, s);

  }

  /**
   * nom de la colonne de num�ro _i.
   */
  public String getColumnName(final int _i) {
    return nomColonnes_[_i];
  }

  public int size() {
    return trajets_ == null ? 0 : trajets_.size();
  }

  /**
   * @param _name : nom � donner � la colonne
   * @param _i : indice de la colonne � renommer
   */
  public void setColumnName(final String _name, final int _i) {
    nomColonnes_[_i] = _name;
  }

  public void set(final int _i, final SParametresTrajets _t) {
    if (_i >= 0 && _i < size()) {
      trajets_.set(_i, _t);
      fireTableRowsUpdated(_i, _i);
    }
  }

  public void removeBat(final String _name) {
    final int nb = getRowCount();
    final TIntArrayList idx = new TIntArrayList(nb);
    for (int i = 0; i < nb; i++) {
      if (getBateau(i).bateau.equalsIgnoreCase(_name)) {
        idx.add(i);
      }
    }
    delete(idx.toNativeArray());
  }

  public static String getS(final String _s) {
    return Sinavi2Resource.SINAVI2.getString(_s);
  }

  public static String getS(final String _s, final String _v1) {
    return Sinavi2Resource.SINAVI2.getString(_s, _v1);
  }

  public static String getS(final String _s, final String _v1, final String _v2) {
    return Sinavi2Resource.SINAVI2.getString(_s, _v1, _v2);
  }

  public static void addDesc(final SParametresTrajets _t, final StringBuffer _seq) {
    _seq.append(getS("Trajet"));
    if (_t.bateau != null) {
      _seq.append(getS(" du bateau {0}", _t.bateau));
    }
    _seq.append(getS(" reliant les gare {0} et {1}", CtuluLibString.getString(_t.gareDepart), CtuluLibString
        .getString(_t.gareArrivee)));
    _seq.append(getS(" avec une loi {0}", Sinavi2Helper.getTypeLoi(_t.typeDeLoi)));
  }

  public static SParametresTrajets[] controleTrajets(final SParametresTrajets[] _tts, final List _bts, final CtuluUI _ui) {
    if (_tts == null) {
      return _tts;
    }
    final ArrayList res = new ArrayList(_tts.length);
    final StringBuffer error = new StringBuffer(100);
    final int nb = _tts.length;
    final String bateauDefaut = CtuluLibArray.isEmpty(_bts) ? getS("Inconnu")
        : ((SParametresBateau) _bts.get(0)).identification;
    for (int i = 0; i < nb; i++) {
      final SParametresTrajets t = _tts[i];
      boolean ok = true;
      // controle bateau
      ok &= checkBateau(_bts, error, bateauDefaut, t);
      ok &= checkLoi(error, t);
      if (ok) {
        res.add(t);
      }
    }
    if (error.length() > 0) {
      _ui.error(getS("Trajets"), "<html><body>" + error.append("</body></html>").toString(), false);
    }
    return (SParametresTrajets[]) res.toArray(new SParametresTrajets[res.size()]);
  }

  /**
   * @param _error le buffer contenant les erreur
   * @param _t le trajet a tester
   * @return true si a pu etre modifie et est finalement valide
   */
  private static boolean checkLoi(final StringBuffer _error, final SParametresTrajets _t) {
    if (Sinavi2Helper.isDeterministe(_t)) {
      final SParametresLoiD d = _t.loiD;
      if (d == null || CtuluLibArray.isEmpty(d.dateHeure)) {
        _t.loiD = new SParametresLoiD(new SDateHeure[] { new SDateHeure(1, new SHeure(8, 0)) });
        addLoiError(_error, _t, "d�terministe");
      }
    } else if (Sinavi2Helper.isJournaliere(_t)) {
      final SParametresLoiJ j = _t.loiJ;
      if (j == null || CtuluLibArray.isEmpty(j.heureJ)) {
        _t.loiJ = new SParametresLoiJ(new SHeure[] { new SHeure(8, 0) });
        addLoiError(_error, _t, "journali�re");
      }
    } else if (Sinavi2Helper.isErlang(_t)) {
      final SParametresLoiE e = _t.loiE;
      if (e == null) {
        _t.loiE = new SParametresLoiE(new SHeure(8, 0), new SHeure(18, 0), 1, 10);
        addLoiError(_error, _t, "Erlang");
      } else if (e.heureDebutGeneration == null) {
        _t.loiE.heureDebutGeneration = new SHeure(8, 0);
        addLoiError(_error, _t, "Erlang (heure d�but)");
      } else if (e.heureFinGeneration == null) {
        _t.loiE.heureFinGeneration = new SHeure(18, 0);
        addLoiError(_error, _t, "Erlang (heure fin)");
      } else if (e.nbreBateauxAttendus < 0) {
        _t.loiE.nbreBateauxAttendus = 1;
        addLoiError(_error, _t, "Erlang (nombre de bateau)");
      } else if (e.ordreDeLaLoi < 0) {
        _t.loiE.ordreDeLaLoi = 1;
        addLoiError(_error, _t, "Erlang (ordre de la loi)");
      }

    } else {
      _t.typeDeLoi = 0;
      _t.loiE = new SParametresLoiE(new SHeure(8, 0), new SHeure(18, 0), 1, 10);
      _error.append("<p>");
      _error.append(getS("Trajet modifi� car loi inconnu�"));
      _error.append("<ul><li>");
      addDesc(_t, _error);
      _error.append("</li></ul></p>");
    }
    return true;
  }

  private static void addLoiError(final StringBuffer _error, final SParametresTrajets _t, final String _loi) {
    _error.append("<p>");
    _error.append(getS("Trajet modifi� car loi " + _loi + " non sp�cifi�e"));
    _error.append("<ul><li>");
    addDesc(_t, _error);
    _error.append("</li></ul></p>");
  }

  /**
   * @param _bts la liste des bateaux SParametresBateau
   * @param _error le buffer
   * @param _bateauDefault le nom du bateau a utiliser si erreur
   * @param _trajet le trajet a tester
   * @return true si valide ou si a pu etre modifie et est finalement valide
   */
  private static boolean checkBateau(final List _bts, final StringBuffer _error, final String _bateauDefault,
      final SParametresTrajets _trajet) {
    final int idx = Sinavi2Lib.getBateau(_bts, _trajet.bateau);
    if (_trajet.bateau == null || idx < 0) {
      _error.append("<p>");
      if (_trajet.bateau == null) {
        _error.append(getS("Trajet modifi� car bateau non sp�cifi�"));
      } else {
        _error.append(getS("Trajet modifi� car bateau non trouv�"));
      }
      _error.append("<ul><li>");
      addDesc(_trajet, _error);
      _trajet.bateau = _bateauDefault;
      _error.append("</li><li>").append(getS("Bateau utilis�: {0}", _trajet.bateau));
      _error.append("</li></ul></p>");
    }
    return true;
  }

  public final void setTrajets(final SParametresTrajets[] _trajets, final List _bateaux, final CtuluUI _ui) {
    final SParametresTrajets[] tjts = controleTrajets(_trajets, _bateaux, _ui);
    if (trajets_ == null) {
      trajets_ = tjts == null ? new ArrayList(10) : new ArrayList(Arrays.asList(tjts));
    } else {
      trajets_.clear();
      if (tjts != null) {
        trajets_.addAll(Arrays.asList(tjts));
      }
      fireTableDataChanged();
    }

  }

public List<String> getComments() {
	// TODO Auto-generated method stub
	return null;
}
}
