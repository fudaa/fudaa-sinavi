package org.fudaa.fudaa.sinavi2;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import javax.swing.JComponent;
import javax.swing.border.EmptyBorder;
import javax.swing.event.InternalFrameEvent;
import javax.swing.event.InternalFrameListener;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuCommonImplementation;
import com.memoire.bu.BuDialogMessage;
import com.memoire.bu.BuDialogWarning;
import com.memoire.bu.BuInternalFrame;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuTextField;

import org.fudaa.dodico.corba.sinavi2.SParametresGeneration;

/**
 * impl�mentation d'une fen�tre interne permettant de saisir la dur�e de simulation et un nombre quelconque "graine"
 * 
 * @version $Revision: 1.10 $ $Date: 2006-09-19 15:08:58 $ by $Author: deniger $
 * @author Fatimatou Ka , Benoit Maneuvrier
 */

public class Sinavi2FilleInitialiserTirage extends BuInternalFrame implements ActionListener, InternalFrameListener,
    FocusListener {
  private final BuPanel ptitre_ = new BuPanel();
  private final BuPanel pDonnees_ = new BuPanel();
  private final BuLabel lTitre_ = new BuLabel("Initialiser les tirages de lois al�atoires ");
  private final BuLabel lnbJour_ = new BuLabel("Nombre de jours de simulation");
  private final BuTextField nbJour_ = new BuTextField(" ", 3);
  private final BuLabel lgraine_ = new BuLabel("Graine de d�part");
  private final BuTextField graine_ = new BuTextField(" ", 3);

  /** * panel contenant les boutons, il est plac� en bas */

  private final BuPanel pBoutons_ = new BuPanel();

  /** bouton pour annuler */
  private final BuButton bAnnuler_ = new BuButton(Sinavi2Resource.SINAVI2.getIcon("QUITTER"), "Quitter");

  /** sauver */
  private final BuButton bSauver_ = new BuButton(Sinavi2Resource.SINAVI2.getIcon("GENERER"), "Gen�rer");

  /** afficher le nombre de jours saisis et la graine */
  private final BuButton bAfficher_ = new BuButton(Sinavi2Resource.SINAVI2.getIcon("EDITER"), "Afficher");

  // public ArrayList listeInitialise_;
  public Sinavi2Implementation imp_ = null;
  // public IParametresSinavi2 param_;
  public SParametresGeneration gen_;

  public Sinavi2FilleInitialiserTirage(final BuCommonImplementation _appli, final SParametresGeneration _gen) {
    super("Initialiser les tirages de lois al�atoires", true, true, true, false);
    // listeInitialise_=_liste;
    // param_=_param;
    gen_ = _gen;
    // param_=new DParametresSinavi2();
    // param_=_param;

    imp_ = (Sinavi2Implementation) _appli.getImplementation();

    bSauver_.addActionListener(this);
    bAnnuler_.addActionListener(this);
    bAfficher_.addActionListener(this);

    /** Contraintes sur les zones de texte */
    nbJour_.setText("0");
    graine_.setText("0");
    nbJour_.setToolTipText("Entrez un nombre de jour de simulation");
    graine_.setToolTipText("Entrez une graine de d�part");
    /** Focus sur les zones de texte* */
    nbJour_.addFocusListener(this);
    graine_.addFocusListener(this);

    ((JComponent) getContentPane()).setBorder(new EmptyBorder(25, 25, 25, 25));

    ptitre_.add(lTitre_, "center");

    final GridBagLayout g2 = new GridBagLayout();
    pDonnees_.setLayout(g2);
    final GridBagConstraints c = new GridBagConstraints();
    c.fill = GridBagConstraints.BOTH;

    c.gridx = 0;
    c.gridy = 2;
    pDonnees_.add(lnbJour_, c);

    c.gridx = 1;
    c.gridy = 2;
    pDonnees_.add(nbJour_, c);

    c.gridx = 0;
    c.gridy = 3;
    pDonnees_.add(lgraine_, c);

    c.gridx = 1;
    c.gridy = 3;
    pDonnees_.add(graine_, c);
    if (!imp_.isPermettreModif()) {
      bSauver_.setEnabled(false);
    }
    pBoutons_.setLayout(new FlowLayout(FlowLayout.CENTER));
    pBoutons_.add(bAnnuler_);
    pBoutons_.add(bSauver_);
    pBoutons_.add(bAfficher_);

    getContentPane().add(ptitre_, BorderLayout.NORTH);
    getContentPane().add(pDonnees_, BorderLayout.CENTER);
    getContentPane().add(pBoutons_, BorderLayout.SOUTH);

    pack();
    setVisible(true);

    imp_.addInternalFrame(this);
  }

  public void actionPerformed(final ActionEvent _e) {
    if (_e.getSource() == bAnnuler_) {
      annuler();
      // JOptionPane.showMessageDialog(null,"Quitter");

    }

    else if (_e.getSource() == bSauver_) {
      if (controler_param()) {
        // nbJour_.setText("0");
        // graine_.setText("0");
        imp_.genererBateaux();
        annuler();
      }
    }

    else if (_e.getSource() == bAfficher_) {
      affiche();
    }
  }

  private void annuler() {
    imp_.removeInternalFrame(this);
    // imp_.sinavi2filletirage_ =null;
    imp_.resetFilleTirage();

  }

  private boolean controler_param() {
    boolean valide = false;
    if (Integer.parseInt(nbJour_.getText()) < 0) {
      final BuDialogWarning dialog_mess = new BuDialogWarning(imp_.getApp(), imp_.getInformationsSoftware(),
          "Entrez un nombre de jours correct");
      dialog_mess.activate();
    }

    else if (Integer.parseInt(graine_.getText()) < 0) {
      final BuDialogWarning dialog_mess = new BuDialogWarning(imp_.getApp(), imp_.getInformationsSoftware(),
          "Entrez une graine de d�part correcte");
      dialog_mess.activate();
    } else {
      // SParametresGeneration gen_=new SParametresGeneration();
      gen_.dureeSimulation = Integer.parseInt(nbJour_.getText());
      gen_.graine = Integer.parseInt(graine_.getText());
      // affMessage("rentrer");
      // DParametresSinavi2.parametresGeneration(g);
      // param_.parametresGeneration(gen_);
      // affMessage("sortie");
      // listeInitialise_.add(g);
      valide = true;
    }
    return valide;
  }

  /** M�thode permettant d'afficher le nombre de jours de g�n�ration: m�thode test */
  private void affiche() {

    // ListIterator iter=listeInitialise_.listIterator();
    // while(iter.hasNext()){
    // /SParametresGeneration g= new SParametresGeneration();
    // g=(SParametresGeneration)iter.next();
    affMessage("Duree de simulation: " + String.valueOf(gen_.dureeSimulation));
    affMessage("graine: " + String.valueOf(gen_.graine));

  }

  /**
   * Quand on entre dans la zone de texte c'est que l'on entre dans le composant.
   * 
   * @param e
   */
  public void focusGained(final FocusEvent _e) {
    nbJour_.selectAll();
    graine_.selectAll();
    processFocusEvent(new FocusEvent(this, FocusEvent.FOCUS_GAINED));
  }

  /**
   * Quand on sort de la zone de texte c'est que l'on sort du composant.
   * 
   * @param e
   */
  public void focusLost(final FocusEvent _e) {
    processFocusEvent(new FocusEvent(this, FocusEvent.FOCUS_LOST));
  }

  public void affMessage(final String _t) {
    final BuDialogMessage dialog_mess = new BuDialogMessage(imp_.getApp(), imp_.getInformationsSoftware(), "" + _t);
    dialog_mess.activate();
  }

  public void internalFrameActivated(final InternalFrameEvent _e) {
  // TODO Auto-generated method stub

  }

  public void internalFrameClosed(final InternalFrameEvent _e) {
  // TODO Auto-generated method stub

  }

  public void internalFrameClosing(final InternalFrameEvent _e) {
  // TODO Auto-generated method stub

  }

  public void internalFrameDeactivated(final InternalFrameEvent _e) {
  // TODO Auto-generated method stub

  }

  public void internalFrameDeiconified(final InternalFrameEvent _e) {
  // TODO Auto-generated method stub

  }

  public void internalFrameIconified(final InternalFrameEvent _e) {
  // TODO Auto-generated method stub

  }

  public void internalFrameOpened(final InternalFrameEvent _e) {
  // TODO Auto-generated method stub

  }
}
