/*
 * @file         Sinavi2FilleAff�Bateaux.java
 * @creation     2001-05-17
 * @modification $Date: 2007-05-04 14:01:05 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.sinavi2;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.ListIterator;

import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.border.EmptyBorder;
import javax.swing.event.InternalFrameEvent;
import javax.swing.event.InternalFrameListener;

import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuComboBox;
import com.memoire.bu.BuCommonImplementation;
import com.memoire.bu.BuDialogMessage;
import com.memoire.bu.BuInternalFrame;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuScrollPane;
import com.memoire.bu.BuTable;
import com.memoire.fu.FuLog;

import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.table.CtuluTableExcelWriter;

import org.fudaa.dodico.corba.sinavi2.SParametresBateau;
import org.fudaa.dodico.corba.sinavi2.SParametresBief;
import org.fudaa.dodico.corba.sinavi2.SParametresVitesses;

/**
 * impl�mentation d'une fen�tre interne permettant d'afficher les parametres des vitesses sous forme de tableau.
 * 
 * @version $Revision: 1.14 $ $Date: 2007-05-04 14:01:05 $ by $Author: deniger $
 * @author Fatimatou Ka , Beno�t Maneuvrier
 */
public class Sinavi2FilleAffVitesses extends BuInternalFrame implements ActionListener, InternalFrameListener {

  private final BuLabel lTitre_ = new BuLabel("Vitesses des bateaux dans les biefs  ");
  private final BuLabel lFiltre_ = new BuLabel("Filtrer : ");
  private final BuButton bAnnuler_ = new BuButton(Sinavi2Resource.SINAVI2.getIcon("QUITTER"), "Quitter");
  private final BuButton bModifier_ = new BuButton(Sinavi2Resource.SINAVI2.getIcon("MODIFIER"), "Modifier");
  private final BuButton bImprimer_ = new BuButton(Sinavi2Resource.SINAVI2.getIcon("IMPRIMER"), "Imprimer");
  private final BuButton bMiseAJour_ = new BuButton(Sinavi2Resource.SINAVI2.getIcon("MISAJOUR"), "Mise � Jour");
  private final BuButton bVitessesParDefaut_ = new BuButton(Sinavi2Resource.SINAVI2.getIcon("VITESSE"),
      "Vitesses Par D�fauts");
  public ArrayList listeVitesses2_;

  /**
   * bateau pour les champs en cas de modifications.
   */
  public SParametresVitesses vitCourant_;
  // private int nbat_=-1;

  /**
   * panel contenant les boutons, il est plac� en bas.
   */
  private final BuPanel pBoutons_ = new BuPanel();
  /**
   * panel contenant les diff�rents champs de la fen�tre.
   */
  private final BuPanel pTitre_ = new BuPanel();

  private final BuPanel pDonnees2_ = new BuPanel();

  private final BuLabel lTypeBateau_ = new BuLabel("Bateau ");
  private final BuComboBox cTypeBateau_ = new BuComboBox();
  private final BuLabel lBief_ = new BuLabel("Bief ");
  private final BuComboBox cBief_ = new BuComboBox();
  private Sinavi2FilleModVitesses sinavi2fillemodvitesses_;
  public Sinavi2TableauVitesse tb_;
  public BuTable table_;
  public Sinavi2Implementation impl_;

  public Sinavi2FilleAffVitesses(final BuCommonImplementation _appli, final ArrayList _liste2, final int _bief,
      final int _bateau) {
    super("Affichage des vitesses des bateaux dans les biefs", true, true, true, false);
    // pTitre_.add(lTitre_);
    listeVitesses2_ = _liste2;
    impl_ = (Sinavi2Implementation) _appli.getImplementation();
    /* SinaviTableauBateau */
    // tabBateaux_= new BuTableSortModel(tb);
    cBief_.addItem("TOUS");
    if (impl_.listeBiefs_ != null) {
      final ListIterator it = impl_.listeBiefs_.listIterator();
      while (it.hasNext()) {
        final SParametresBief b = (SParametresBief) it.next();
        cBief_.addItem(b.identification);
      }
    }
    cBief_.setSelectedIndex(_bief);
    cTypeBateau_.addItem("TOUS");
    if (impl_.listeBateaux_ != null) {
      final ListIterator itBateau = impl_.listeBateaux_.listIterator();
      while (itBateau.hasNext()) {
        final SParametresBateau b = (SParametresBateau) itBateau.next();
        cTypeBateau_.addItem(b.identification);
      }
    }
    cTypeBateau_.setSelectedIndex(_bateau);
    miseajour(listeVitesses2_);

    // table_.setSize(250,250);
    // table_.setAutoResizeMode(AUTO_RESIZE_ALL_COLUMNS);

    final GridBagLayout g = new GridBagLayout();
    pTitre_.setLayout(g);
    final GridBagConstraints c = new GridBagConstraints();
    c.fill = GridBagConstraints.BOTH;
    c.gridx = 0;
    c.gridy = 2;
    pTitre_.add(lTitre_, c);
    c.gridx = 1;
    c.gridy = 2;

    pTitre_.add(lFiltre_, c);
    c.gridx = 2;
    c.gridy = 2;
    pTitre_.add(lTypeBateau_, c);

    c.gridx = 3;
    c.gridy = 2;
    pTitre_.add(cTypeBateau_, c);

    c.gridx = 2;
    c.gridy = 3;
    pTitre_.add(lBief_, c);

    c.gridx = 3;
    c.gridy = 3;
    pTitre_.add(cBief_, c);

    table_.setRowSelectionAllowed(true);
    // table_.getColumnModel().getColumn(0).setPreferredWidth(300);

    final BuScrollPane tab = new BuScrollPane(table_);

    // pDonnees2_.add(table_);

    pDonnees2_.add(tab);

    bAnnuler_.addActionListener(this);
    bModifier_.addActionListener(this);
    bImprimer_.addActionListener(this);
    bMiseAJour_.addActionListener(this);
    bVitessesParDefaut_.addActionListener(this);
    if (!impl_.isPermettreModif()) {
      bModifier_.setEnabled(false);
      bVitessesParDefaut_.setEnabled(false);

    }
    ((JComponent) getContentPane()).setBorder(new EmptyBorder(25, 25, 25, 25));

    pBoutons_.setLayout(new FlowLayout(FlowLayout.CENTER));
    pBoutons_.add(bAnnuler_);
    pBoutons_.add(bVitessesParDefaut_);
    pBoutons_.add(bModifier_);
    pBoutons_.add(bImprimer_);
    pBoutons_.add(bMiseAJour_);
    getContentPane().add(pTitre_, BorderLayout.NORTH);
    getContentPane().add(pDonnees2_, BorderLayout.CENTER);
    getContentPane().add(pBoutons_, BorderLayout.SOUTH);

    pack();

    setVisible(true);
    addInternalFrameListener(this);
    impl_.addInternalFrame(this);

  }

  public void miseajour(final ArrayList _lstVitesses) {
    if (tb_ == null) {
      tb_ = new Sinavi2TableauVitesse();
    }
    int tabSize = 0;
    if (cTypeBateau_.getSelectedIndex() == 0) {
      tabSize = impl_.listeBateaux_.size();
    } else {
      tabSize = 1;
    }
    if (cBief_.getSelectedIndex() == 0) {
      tabSize = tabSize * impl_.listeBiefs_.size();
    }

    tb_.data_ = new Object[tabSize][tb_.getColumnCount()];
    // tb_.init_nomcol();
    if (_lstVitesses != null) {
      final ListIterator iter = _lstVitesses.listIterator();
      int row = 0;
      while (iter.hasNext()) {

        SParametresVitesses c = new SParametresVitesses();
        c = (SParametresVitesses) iter.next();
        if ((c.bateau.equalsIgnoreCase((String) cTypeBateau_.getSelectedItem()) || cTypeBateau_.getSelectedIndex() == 0)
            && (c.bief.equalsIgnoreCase((String) cBief_.getSelectedItem()) || cBief_.getSelectedIndex() == 0)) {
          tb_.setValueAt(c.bateau, row, 0);
          tb_.setValueAt(c.bief, row, 1);

          String s = "";
          s = (Double.toString(c.vitesseDesMontants));
          tb_.setValueAt(s, row, 2);

          s = (Double.toString(c.vitesseDesAvalants));
          tb_.setValueAt(s, row, 3);

          row++;
          tb_.setNbRow(row);
        }
      }
    }
    table_ = new BuTable(tb_.data_, tb_.getNomColonnes());
    table_.repaint();
  }

  /**
   * @param _e
   */

  public void annuler() {
    impl_.removeInternalFrame(this);
    // sinavi2fillemodvitesses_ =null;
    impl_.resetFille2ModVitesse();
  }

  public void actionPerformed(final ActionEvent _e) {
    if (_e.getSource() == bAnnuler_) {
      annuler();
    } else if (_e.getSource() == bVitessesParDefaut_) {
      vitessesParDefaut(table_.getSelectedRow());
    } else if (_e.getSource() == bModifier_) {

      final int selection = table_.getSelectedRow();
      System.out.println("selection --> " + selection);
      if (selection >= 0) {

        String bateauCourant = "";
        bateauCourant = (String) table_.getValueAt(selection, 0);
        String biefCourant = "";
        biefCourant = (String) table_.getValueAt(selection, 1);

        // imp2_.modifier_vitesse(bateauCourant, biefCourant,tb);
        sinavi2fillemodvitesses_ = new Sinavi2FilleModVitesses(impl_, listeVitesses2_, bateauCourant, biefCourant, tb_);
        sinavi2fillemodvitesses_.setVisible(true);
        // JOptionPane.showMessageDialog(null,"Modification effectu�e");

      } else {
        JOptionPane.showMessageDialog(null, "Selectionnez une ligne");
      }

      /*
       * if(controler_entrees()==true){ JOptionPane.showMessageDialog(null,"Cr�ation R�ussie"); initialise_champs(-1);
       */
    }

    else if (_e.getSource() == bImprimer_) {
      final File s = impl_.enregistrerXls();
      if (s == null) {
        return;
      }
      final Sinavi2TableauManoeuvre tbtemp = new Sinavi2TableauManoeuvre();
      tbtemp.data_ = new Object[tb_.getRowCount() + 2][tb_.getColumnCount()];
      tbtemp.initNomCol(1);
      tbtemp.setColumnName("Liste des Vitesses maximales autoris�es", 0);
      for (int i = 1; i < tb_.getColumnCount(); i++) {
        tbtemp.setColumnName(CtuluLibString.ESPACE, i);
      }

      tbtemp.setNbRow(tb_.getRowCount() + 2);
      for (int i = 2; i <= (tb_.getRowCount() + 1); i++) {
        for (int j = 0; j < tb_.getColumnCount(); j++) {
          tbtemp.data_[i][j] = tb_.data_[i - 2][j];
        }

      }
      final CtuluTableExcelWriter test = new CtuluTableExcelWriter(tbtemp, s);

      try {
        test.write(null);

      } catch (final RowsExceededException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      } catch (final WriteException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      } catch (final IOException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }
    } else if (_e.getSource() == bMiseAJour_) {
      annuler();
      impl_.menuVitesses(cBief_.getSelectedIndex(), cTypeBateau_.getSelectedIndex());
    }
  }

  private void vitessesParDefaut(final int _indice) {
    if (_indice == -1) {
      FuLog.debug("test");
      impl_.listeVitesses_ = new ArrayList();
      listeVitesses2_ = impl_.listeVitesses_;
      final ListIterator itBief = impl_.listeBiefs_.listIterator();
      final ListIterator itBat = impl_.listeBateaux_.listIterator();

      /* remplissage des biefs dans une liste */
      final String[] bief = new String[impl_.listeBiefs_.size()];
      final double[] biefVit = new double[impl_.listeBiefs_.size()];
      int i = 0;
      while (itBief.hasNext()) {
        SParametresBief b = new SParametresBief();
        b = (SParametresBief) itBief.next();
        bief[i] = b.identification;
        biefVit[i] = b.vitesse;
        i++;
      }
      while (itBat.hasNext()) {
        SParametresBateau b = new SParametresBateau();
        b = (SParametresBateau) itBat.next();
        for (int j = 0; j < bief.length; j++) {
          final SParametresVitesses c = new SParametresVitesses();
          c.bateau = b.identification;
          c.bief = bief[j];
          c.vitesseDesMontants = Math.min(b.vitesseMontantParDefaut, biefVit[j]);
          c.vitesseDesAvalants = Math.min(b.vitesseDescendantParDefaut, biefVit[j]);
          listeVitesses2_.add(c);
        }
      }
    } else {
      for (int h = 0; h < table_.getSelectedRowCount(); h++) {
        final String bateauCourant = (String) table_.getValueAt(table_.getSelectedRows()[h], 0);
        final String biefCourant = (String) table_.getValueAt(table_.getSelectedRows()[h], 1);
        boolean trouve = false;

        final ListIterator vit = listeVitesses2_.listIterator();
        while (vit.hasNext() & !trouve) {
          final SParametresVitesses c = (SParametresVitesses) vit.next();
          if (c.bateau.equalsIgnoreCase(bateauCourant) && c.bief.equalsIgnoreCase(biefCourant)) {

            double vitMon = 0, vitAv = 0, vitBief = 0;
            ListIterator it = impl_.listeBateaux_.listIterator();
            while (it.hasNext() & !trouve) {
              final SParametresBateau b = (SParametresBateau) it.next();
              if (b.identification.equalsIgnoreCase(bateauCourant)) {
                vitMon = b.vitesseMontantParDefaut;
                vitAv = b.vitesseDescendantParDefaut;
                trouve = true;
              }
            }
            trouve = false;
            it = impl_.listeBiefs_.listIterator();
            while (it.hasNext() & !trouve) {
              final SParametresBief b = (SParametresBief) it.next();
              if (b.identification.equalsIgnoreCase(biefCourant)) {
                vitBief = b.vitesse;
                trouve = true;
              }
            }
            c.vitesseDesAvalants = Math.min(vitAv, vitBief);
            c.vitesseDesMontants = Math.min(vitMon, vitBief);
            listeVitesses2_.set(table_.getSelectedRows()[h], c);
          }

        }
      }
    }
  }

  public void affMessage(final String _t) {
    new BuDialogMessage(impl_.getApp(), impl_.getInformationsSoftware(), _t).activate();
  }

  public void internalFrameActivated(final InternalFrameEvent _e) {
  // TODO Auto-generated method stub

  }

  public void internalFrameClosed(final InternalFrameEvent _e) {
    this.closable = true;
    System.out.println("fermeture centralis�e des portes");
    annuler();
  }

  public void internalFrameDeactivated(final InternalFrameEvent _e) {
  // TODO Auto-generated method stub

  }

  public void internalFrameDeiconified(final InternalFrameEvent _e) {
  // TODO Auto-generated method stub

  }

  public void internalFrameIconified(final InternalFrameEvent _e) {
  // TODO Auto-generated method stub

  }

  public void internalFrameOpened(final InternalFrameEvent _e) {
  // TODO Auto-generated method stub

  }

  public void internalFrameClosing(final InternalFrameEvent _e) {
  // TODO Auto-generated method stub

  }

}
