/*
 * @file         Sinavi2FilleAddModBateaux.java
 * @creation     2005-09-17
 * @modification $Date: 2007-05-04 14:01:05 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.sinavi2;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.ListIterator;

import javax.swing.JComponent;
import javax.swing.border.EmptyBorder;
import javax.swing.event.InternalFrameEvent;
import javax.swing.event.InternalFrameListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

import com.memoire.bu.*;

import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.table.CtuluTableExcelWriter;

import org.fudaa.dodico.corba.sinavi2.SParametresBateau;
import org.fudaa.dodico.corba.sinavi2.SResultatHistorique;
import org.fudaa.dodico.corba.sinavi2.SSimulationSinavi2;

import org.fudaa.ebli.graphe.BGraphe;

/**
 * Cette classe offre la possibilit� d'�tudier les attentes par type de bateaux. En choisissant diff�rentes options on
 * peut r�aliser le graphique correspondant � nos besoins.
 * 
 * @version $Revision: 1.14 $ $Date: 2007-05-04 14:01:05 $ by $Author: deniger $
 * @author Fatimatou Ka , Beno�t Maneuvrier
 */
public class Sinavi2ResAttentesBateaux extends BuInternalFrame implements ActionListener, InternalFrameListener,
    FocusListener, ListSelectionListener, ItemListener {

  ArrayList listeAttente_;
  private final BuLabel lTitre_ = new BuLabel("Attentes par Type de Bateau");
  private final BuLabel lBlanc_ = new BuLabel("    ");
  private final BuLabel lBlanc2_ = new BuLabel("    ");
  private final BuLabel lBlanc3_ = new BuLabel("    ");
  private final BuLabel lBlanc4_ = new BuLabel("    ");
  private final BuLabel lBlanc5_ = new BuLabel("    ");

  /** bouton pour annuler */
  private final BuButton bAnnuler_ = new BuButton(Sinavi2Resource.SINAVI2.getIcon("QUITTER"), "Quitter");

  /** afficher le graphique */
  private final BuButton bAfficher_ = new BuButton(Sinavi2Resource.SINAVI2.getIcon("EDITER"), "Afficher");

  /** Param�tres d'entr�es pour le graphique* */
  private final BuLabel lItineraire_ = new BuLabel("Itin�raire");
  private final BuLabel lGareDeb_ = new BuLabel("Gare Debut");
  private final BuLabel lGareFin_ = new BuLabel("Gare Fin");
  private final BuLabel lSensNav_ = new BuLabel("Sens Navigation");
  private final BuComboBox cGareDeb_ = new BuComboBox();
  private final BuComboBox cGareFin_ = new BuComboBox();
  private final BuComboBox cSensNav_ = new BuComboBox();

  private final BuLabel lBateau_ = new BuLabel("Bateaux");
  private final BuLabel lListeBateaux_ = new BuLabel("Liste des Bateaux");

  public BuList liListeBateaux_;

  private BuScrollPane sliListeBateaux;
  private final BuLabel lCrenaux_ = new BuLabel("Cr�naux Horaires");
  private final BuLabel lHoraireDebut_ = new BuLabel("Horaires de D�but");
  private final BuLabel lHoraireFin_ = new BuLabel("Horaires de Fin");
  private final DureeField dHoraireDebutHeure_ = new DureeField(false, false, true, false, false);
  private final DureeField dHoraireDebutMinute_ = new DureeField(false, false, false, true, false);
  private final DureeField dHoraireFinHeure_ = new DureeField(false, false, true, false, false);
  private final DureeField dHoraireFinMinute_ = new DureeField(false, false, false, true, false);

  private final BuLabel ltypeGraphe_ = new BuLabel("Type Graphe");
  private final BuRadioButton rHistogramme_ = new BuRadioButton("Histogramme");
  private final BuRadioButton rCourbe_ = new BuRadioButton("Courbe");
  public BuRadioButton rPourcentage_ = new BuRadioButton("Pourcentage");
  public BuRadioButton rCumul_ = new BuRadioButton("Cumul�");

  private final BuLabel lAxeX_ = new BuLabel("Axe des abcisses");
  private final BuRadioButton rDureeX_ = new BuRadioButton("Dur�e d'attente");
  private final BuRadioButton rNbBateauxX_ = new BuRadioButton("Nb bateaux");

  private final BuLabel lDonneesAAfficher_ = new BuLabel("Donn�es � Afficher");

  private final BuCheckBox cSeuil_ = new BuCheckBox("Seuil");// **pas n�cessaire seulement si seuil !=0

  private final BuTextField tSeuil_ = new BuTextField("0");

  private final BuLabel lSeuil_ = new BuLabel("Seuil");

  /** * panel contenant les boutons, il est plac� en bas */

  private final BuPanel pBoutons_ = new BuPanel();
  /**
   * panel contenant les diff�rents champs de la fen�tre
   */
  private final BuPanel pTitre_ = new BuPanel();

  private final BuPanel pDonnees2_ = new BuPanel();
  private GridBagLayout g2;
  private GridBagConstraints c;
  // private BuCommonImplementation _appli;

  private ArrayList donneesGraph_;
  private ArrayList listeBateaux_;
  private SResultatHistorique[] listeHistorique_;
  public ArrayList listeSimulations_;
  public Sinavi2Implementation imp_ = null;
  public double minCourant_ = 0;
  public double moyCourant_ = 0;
  public double maxCourant_ = 0;
  public double maxiFlotte = 0;
  public double moyFlotte = 0;
  public double miniFlotte = 10000000;
  public double[] tabBateaux_;// tableau des dur�es d'attente des bateaux
  public double maxNbBateaux_;// max de bateau sur une tranche
  public double nbBateaux_;// nb de bateau qui parcours le chemin
  public double[] tabNbBateauxDuree_;
  boolean comparaison_;
  public ArrayList listeCoordonnees_;
  public Sinavi2TableauResAttentesBateaux t_ = null;
  private final String[] tabCouleurs = { "FF0000", "00FF00", "888888", "444FFF", "AA1111", "444444", "666666",
      "222222", "484848", "848484" };

  // ---------------------------public Sinavi2FilleAddModBateaux (BuCommonImplementation appli_,LinkedList liste_/*,int
  // bat_*/) {

  public Sinavi2ResAttentesBateaux(final BuCommonImplementation _appli, final ArrayList _listeBateaux,
      final Sinavi2TrajetMng _listeTrajets, final ArrayList _listeGares, final ArrayList _listeBiefs,
      final ArrayList _listeEcluses, final ArrayList _listeSimulations/* _listeHistorique */,
      final boolean _comparaison) {
    super("Attentes par type de Bateau", true, true, true, false);
    comparaison_ = _comparaison;
    imp_ = (Sinavi2Implementation) _appli.getImplementation();
    // listeAttente_=_listeAttente;
    listeBateaux_ = _listeBateaux;
    // listeHistorique_=_listeHistorique;
    listeSimulations_ = _listeSimulations;
    bAnnuler_.addActionListener(this);
    bAfficher_.addActionListener(this);
    /*******************************************************************************************************************
     * ------- bAddBat_.addActionListener(this); bSupBat_.addActionListener(this); ------
     ******************************************************************************************************************/
    rCourbe_.addItemListener(this);
    rCourbe_.addActionListener(this);
    rHistogramme_.addItemListener(this);
    rHistogramme_.addActionListener(this);
    rDureeX_.addActionListener(this);
    rNbBateauxX_.addActionListener(this);
    rPourcentage_.addActionListener(this);
    rCumul_.addActionListener(this);
    ((JComponent) getContentPane()).setBorder(new EmptyBorder(25, 25, 25, 25));
    pTitre_.add(lTitre_, "center");

    g2 = new GridBagLayout();
    pDonnees2_.setLayout(g2);
    c = new GridBagConstraints();
    c.fill = c.BOTH;

    ListIterator it;
    c.gridx = 1;
    c.gridy = 2;
    pDonnees2_.add(lItineraire_, c);

    it = _listeGares.listIterator();
    while (it.hasNext()) {
      final String s = (String) it.next();
      cGareDeb_.addItem(s);
      cGareFin_.addItem(s);
    }

    c.gridx = 2;
    c.gridy = 2;
    pDonnees2_.add(lGareDeb_, c);

    c.gridx = 3;
    c.gridy = 2;
    pDonnees2_.add(cGareDeb_, c);

    c.gridx = 4;
    c.gridy = 2;
    pDonnees2_.add(lGareFin_, c);

    c.gridx = 5;
    c.gridy = 2;
    pDonnees2_.add(cGareFin_, c);

    c.gridx = 6;
    c.gridy = 2;
    pDonnees2_.add(lSensNav_, c);

    cSensNav_.addItem("les 2");
    cSensNav_.addItem("Montant");
    cSensNav_.addItem("Avalant");
    c.gridx = 7;
    c.gridy = 2;
    pDonnees2_.add(cSensNav_, c);

    c.gridx = 1;
    c.gridy = 3;
    pDonnees2_.add(lBlanc_, c);

    c.gridx = 1;
    c.gridy = 4;
    pDonnees2_.add(lBateau_, c);

    c.gridx = 2;
    c.gridy = 4;
    pDonnees2_.add(lListeBateaux_, c);

    final String[] bateaux = new String[_listeBateaux.size() + 1];
    bateaux[0] = "flotte";
    int i = 1;
    it = _listeBateaux.listIterator();
    while (it.hasNext()) {
      bateaux[i] = ((SParametresBateau) it.next()).identification;
      i++;
    }

    liListeBateaux_ = new BuList(bateaux);
    sliListeBateaux = new BuScrollPane(liListeBateaux_);
    c.gridx = 3;
    c.gridy = 4;
    pDonnees2_.add(sliListeBateaux, c);

    c.gridx = 1;
    c.gridy = 5;
    pDonnees2_.add(lBlanc2_, c);

    c.gridx = 1;
    c.gridy = 6;
    pDonnees2_.add(lCrenaux_, c);

    c.gridx = 2;
    c.gridy = 6;
    pDonnees2_.add(lHoraireDebut_, c);

    c.gridx = 3;
    c.gridy = 6;
    pDonnees2_.add(dHoraireDebutHeure_, c);

    c.gridx = 4;
    c.gridy = 6;
    pDonnees2_.add(dHoraireDebutMinute_, c);

    c.gridx = 5;
    c.gridy = 6;
    pDonnees2_.add(lHoraireFin_, c);

    c.gridx = 6;
    c.gridy = 6;
    pDonnees2_.add(dHoraireFinHeure_, c);

    c.gridx = 7;
    c.gridy = 6;
    pDonnees2_.add(dHoraireFinMinute_, c);

    dHoraireDebutHeure_.setDureeField(0);
    dHoraireDebutMinute_.setDureeField(0);
    dHoraireFinHeure_.setDureeField(86400);
    dHoraireFinMinute_.setDureeField(0);

    c.gridx = 1;
    c.gridy = 7;
    pDonnees2_.add(lBlanc3_, c);

    c.gridx = 1;
    c.gridy = 8;
    pDonnees2_.add(ltypeGraphe_, c);

    c.gridx = 2;
    c.gridy = 8;
    pDonnees2_.add(rCourbe_, c);
    rCourbe_.setSelected(true);
    c.gridx = 3;
    c.gridy = 8;
    pDonnees2_.add(rHistogramme_, c);
    rHistogramme_.setSelected(false);

    c.gridx = 2;
    c.gridy = 9;
    pDonnees2_.add(rCumul_, c);
    rCumul_.setSelected(true);
    c.gridx = 3;
    c.gridy = 9;
    pDonnees2_.add(rPourcentage_, c);
    rPourcentage_.setSelected(false);

    c.gridx = 1;
    c.gridy = 10;
    pDonnees2_.add(lBlanc4_, c);

    c.gridx = 1;
    c.gridy = 11;
    pDonnees2_.add(lDonneesAAfficher_, c);

    c.gridx = 2;
    c.gridy = 11;
    pDonnees2_.add(cSeuil_, c);

    c.gridx = 3;
    c.gridy = 11;
    pDonnees2_.add(lSeuil_, c);

    c.gridx = 4;
    c.gridy = 11;
    pDonnees2_.add(tSeuil_, c);

    c.gridx = 1;
    c.gridy = 12;
    pDonnees2_.add(lBlanc5_, c);

    c.gridx = 1;
    c.gridy = 13;
    pDonnees2_.add(lAxeX_, c);

    c.gridx = 2;
    c.gridy = 13;
    pDonnees2_.add(rDureeX_, c);
    rDureeX_.setSelected(true);

    c.gridx = 3;
    c.gridy = 13;
    pDonnees2_.add(rNbBateauxX_, c);

    pBoutons_.setLayout(new FlowLayout(FlowLayout.CENTER));
    pBoutons_.add(bAnnuler_);

    pBoutons_.add(bAfficher_);
    // getContentPane().add(pDonnees_, BorderLayout.NORTH);
    getContentPane().add(pTitre_, BorderLayout.NORTH);
    getContentPane().add(pDonnees2_, BorderLayout.CENTER);
    getContentPane().add(pBoutons_, BorderLayout.SOUTH);

    pack();

    /** *reinitialisation des valeurs* */

    setVisible(true);
    // addInternalFrameListener(this);
    imp_.addInternalFrame(this);

  }

  /**
   * @param _e
   */

  public void actionPerformed(final ActionEvent _e) {
    if (_e.getSource() == bAnnuler_) {
      annuler();
    } else if (_e.getSource() == bAfficher_) {

      if (verifParametres()) {
        afficher_graphe();
        System.out.println("avant la construction du tableau");
        if (t_ != null) {
          t_.annuler();
        }
        t_ = new Sinavi2TableauResAttentesBateaux(imp_, comparaison_);
        System.out.println("apr�s la construction du tableau");
        imp_.activateInternalFrame(imp_.fillegraphe_);
      }

    }

    else if (_e.getSource() == rHistogramme_) {
      System.out.println("clique :rHistogramme_ ");
      rHistogramme_.setSelected(true);
      rCourbe_.setSelected(false);
      /** car les histogrammes sont pas fait dans l'autre sens* */
      rDureeX_.setSelected(true);
      rNbBateauxX_.setSelected(false);
    } else if (_e.getSource() == rCourbe_) {
      System.out.println("clique :rCourbe_ ");
      rHistogramme_.setSelected(false);
      rCourbe_.setSelected(true);
    } else if (_e.getSource() == rPourcentage_) {
      System.out.println("clique :rPourcentage_ ");
      rPourcentage_.setSelected(true);
      rCumul_.setSelected(false);
    } else if (_e.getSource() == rCumul_) {
      System.out.println("clique :rCumul ");
      rCumul_.setSelected(true);
      rPourcentage_.setSelected(false);
    } else if (_e.getSource() == rDureeX_) {
      System.out.println("clique : rDureeX_ ");
      rDureeX_.setSelected(true);
      rNbBateauxX_.setSelected(false);
    } else if (_e.getSource() == rNbBateauxX_) {
      System.out.println("clique : rNbBateauxX_");
      /** car les histogrammes sont pas fait dans l'autre sens* */
      if (!rHistogramme_.isSelected()) {
        rDureeX_.setSelected(false);
        rNbBateauxX_.setSelected(true);

      } else {
        rNbBateauxX_.setSelected(false);
      }
    }

  }

  private void annuler() {
    imp_.removeInternalFrame(this);
    imp_.resetFille2ModBateaux();

  }

  private void afficher_graphe() {

    if (!comparaison_) {
      selectionDonnees(imp_.getNumeroSimulationCourante());
    } else {
      listeCoordonnees_ = new ArrayList();
      for (int i = 0; i < imp_.simulationsSel_.length; i++) {
        selectionDonnees(imp_.simulationsSel_[i]);
        remplirTabNbBateauxDuree();
        listeCoordonnees_.add(tabNbBateauxDuree_);
      }

    }
    // nbBateaux_=tabBateaux_.length;
    maxNbBateaux_ = maxNbBateaux();
    final BGraphe graphe = new BGraphe();
    String str = "graphe\n";
    str += "{\n";
    str += "titre \"Attente de " + liListeBateaux_.getSelectedValue() + " sur le trajet " + cGareDeb_.getSelectedItem()
        + " -> " + cGareFin_.getSelectedItem();
    if (cSensNav_.getSelectedIndex() == 0) {
      str += " dans les deux sens de navigation \" \n";
    } else if (cSensNav_.getSelectedIndex() == 1) {
      str += " dans le sens montant \" \n";
    } else {
      str += " dans le sens avalant \" \n";
    }

    str += "legende oui \n";
    str += "animation non \n";
    str += "marges\n";
    str += "{\n";
    str += "gauche 120\n";
    str += "droite 200\n";
    str += "haut 50\n";
    str += "bas 30\n";
    str += "}\n";
    if (rDureeX_.isSelected()) {
      str += "axe\n";
      str += "{\n";
      str += "titre \"Attentes pour le type " + liListeBateaux_.getSelectedValue() + " \" \n";
      str += "unite \"heure\" \n";
      str += "orientation horizontal \n";
      str += "graduations oui\n";
      str += "conversionHM \n";
      str += "minimum  0.0\n";
      // str+="maximum "+(maxDureeAttente()+(maxDureeAttente()/20))+"\n";
      // double y =(maxDureeAttente()/20);
      str += "maximum  " + ((maxDureeAttente() + (maxDureeAttente() / 20)) / 3600) + "\n";// *
      final double y = ((maxDureeAttente() / 20) / 3600);// *
      str += "pas " + y + "\n";
      str += "}\n";
      str += "axe\n";
      str += "{\n";
      str += "titre \"Nb Bateaux \" \n";
      str += "unite \"Bateau\" \n";
      str += "orientation vertical\n";
      str += "graduations oui\n";
      str += "minimum  0\n";
      // str+="maximum "+(maxNbBateaux_+(maxNbBateaux_ /20))+"\n";
      final double max = nbBateaux_;
      str += "maximum  " + (max + (max / 20)) + "\n";
      int pas = (int) Math.round(max / 20);
      if (pas == 0) {
        pas = 1;
      }
      str += "pas " + pas + "\n";
      str += "}\n";
      if (!comparaison_) {
        if (!rCumul_.isSelected()) {
          str += "courbe\n";
          str += "{\n";
          str += "titre \"Nb de Bateaux\"\n";
          if (rCourbe_.isSelected()) {
            str += "type courbe\n";
          } else {
            str += "type histogramme\n";
          }
          str += "aspect\n";
          str += " {\n";
          if (rHistogramme_.isSelected()) {
            // str+="contour.largeur 1 \n";
            str += "contour.largeur " + (int) y + " \n";
            str += "surface.couleur FF8800 \n";
            str += "texte.couleur 000000 \n";
            str += "contour.couleur 000000 \n";
          } else {
            str += "contour.couleur FF8800\n";
          }
          str += "}\n";
          str += " valeurs\n";
          str += "  {\n";
          for (int cpt = 0; cpt < 20; cpt++) {
            final int h = cpt + 1;
            System.out.println(" nb bat " + h + ": " + tabNbBateauxDuree_[cpt] + " nb bat total = " + nbBateaux_);
            double pourcentage = (tabNbBateauxDuree_[cpt] * 100);// nbBateaux qui attendent ou pas suivant
            pourcentage = conversionDeuxChiffres(pourcentage / nbBateaux_);

            final double h1 = h * y;
            str += h1 + " " + tabNbBateauxDuree_[cpt] + "\n etiquette \n \"" + pourcentage + "\" \n";
            System.out.println("coordonnees : " + h1 + " " + tabNbBateauxDuree_[cpt] + "  " + pourcentage + " % ");
          }
          str += "  }\n";
          str += "}\n";
          str += " \n";
        } else {// cumule
          str += "courbe\n";
          str += "{\n";
          str += "titre \"Nb de Bateaux\"\n";
          if (rCourbe_.isSelected()) {
            str += "type courbe\n";
          } else {
            str += "type histogramme\n";
          }
          str += "aspect\n";
          str += " {\n";
          if (rHistogramme_.isSelected()) {
            // str+="contour.largeur 1 \n";
            str += "contour.largeur " + (int) y + " \n";
            str += "surface.couleur FF8800 \n";
            str += "texte.couleur 000000 \n";
            str += "contour.couleur 000000 \n";
          } else {
            str += "contour.couleur FF8800\n";
          }
          str += "}\n";
          str += " valeurs\n";
          str += "  {\n";
          double pourcentage = 0;
          double nbBat = 0;
          for (int cpt = 0; cpt < 20; cpt++) {
            final int h = cpt + 1;
            nbBat += tabNbBateauxDuree_[cpt];
            /*
             * System.out.println("tabnbbat : " + tabNbBateauxDuree_[cpt] + "nbbat "+nbBat); double
             * pourcentage2=(tabNbBateauxDuree_[cpt]*100);//nbBateaux qui attendent ou pas suivant
             * pourcentage2=conversionDeuxChiffres(pourcentage2/nbBateaux_); System.out.println("pourcentage2 :
             * "+pourcentage2); pourcentage+=pourcentage2; pourcentage=conversionDeuxChiffres(pourcentage);
             */
            pourcentage = nbBat * 100;
            pourcentage = conversionDeuxChiffres(pourcentage / nbBateaux_);
            System.out.println("pourcentage : " + pourcentage);
            // tabNbBateauxDuree_[cpt]/nbBateaux_*100;//nbBateaux qui attendent ou pas suivant
            // str+=h+" " +tabNbBateauxDuree_[cpt]+ "\n etiquette \n \""+pourcentage+" \" \n";
            final double h1 = h * y;
            str += h1 + " " + nbBat + "\n etiquette \n \"" + pourcentage + "\" \n";
            // FuLog.debug("coordonnees : "+h1+ " " + nbBat + " % "+pourcentage );
            System.out.println("coordonnees : " + h1 + " " + nbBat + " % " + pourcentage);

          }
          str += "  }\n";
          str += "}\n";
          str += " \n";
        }
      } else {// si on est en comparaison
        if (!rCumul_.isSelected()) {
          for (int i = 0; i < listeCoordonnees_.size(); i++) {
            str += "courbe\n";
            str += "{\n";
            int numeroSel=imp_.simulationsSel_[i];
            // str+="titre \"Nb de Bateaux\"\n";
            str += "titre \"" + ((SSimulationSinavi2) imp_.listeSimulationsSinavi2_.get(numeroSel)).nomSim + "\"\n";
            if (rCourbe_.isSelected()) {
              str += "type courbe\n";
            } else {
              str += "type histogramme\n";
            }
            str += "aspect\n";
            str += " {\n";
            if (rHistogramme_.isSelected()) {
              str += "contour.couleur 000000 \n";
              str += "contour.largeur " + (int) y + " \n";
              str += "surface.couleur " + tabCouleurs[i] + " \n";
              str += "texte.couleur " + "000000" + " \n";
            } else {
              str += "contour.couleur " + tabCouleurs[i] + " \n";
            }
            str += "}\n";
            str += " valeurs\n";
            str += "  {\n";
            tabNbBateauxDuree_ = (double[]) listeCoordonnees_.get(i);
            for (int cpt = 0; cpt < 20; cpt++) {
              final int h = cpt + 1;
              System.out.println(" nb bat " + h + ": " + tabNbBateauxDuree_[cpt] + " nb bat total = " + nbBateaux_);
              double pourcentage = (tabNbBateauxDuree_[cpt] * 100);// nbBateaux qui attendent ou pas suivant
              pourcentage = conversionDeuxChiffres(pourcentage / nbBateaux_);

              // System.out.println("calcule : "+(tabNbBateauxDuree_[cpt]*100));
              // str+=h+" " +tabNbBateauxDuree_[cpt]+ "\n etiquette \n \""+pourcentage+" \" \n";
              final double h1 = h * y;
              str += h1 + " " + tabNbBateauxDuree_[cpt] + "\n";// + "\n etiquette \n \""+ pourcentage+"\" \n";
              System.out.println("coordonnees : " + h1 + " " + tabNbBateauxDuree_[cpt] + "  " + pourcentage + " % ");
            }
            str += "  }\n";
            str += "}\n";
            str += " \n";
          }
        } else {// cumule
          for (int i = 0; i < listeCoordonnees_.size(); i++) {
            str += "courbe\n";
            str += "{\n";
            int numeroSel=imp_.simulationsSel_[i];
            // str+="titre \"Nb de Bateaux\"\n";
            str += "titre \"" + ((SSimulationSinavi2) imp_.listeSimulationsSinavi2_.get(numeroSel)).nomSim + "\"\n";
            if (rCourbe_.isSelected()) {
              str += "type courbe\n";
            } else {
              str += "type histogramme\n";
            }
            str += "aspect\n";
            str += " {\n";
            if (rHistogramme_.isSelected()) {
              str += "contour.couleur 000000 \n";
              str += "contour.largeur " + (int) y + " \n";
              str += "surface.couleur " + tabCouleurs[i] + " \n";
              str += "texte.couleur " + "000000" + " \n";
            } else {
              str += "contour.couleur " + tabCouleurs[i] + " \n";
            }
            str += "}\n";
            str += " valeurs\n";
            str += "  {\n";
            double pourcentage = 0;
            double nbBat = 0;
            tabNbBateauxDuree_ = (double[]) listeCoordonnees_.get(i);
            for (int cpt = 0; cpt < 20; cpt++) {
              final int h = cpt + 1;
              nbBat += tabNbBateauxDuree_[cpt];

              pourcentage = nbBat * 100;
              pourcentage = conversionDeuxChiffres(pourcentage / nbBateaux_);
              System.out.println("pourcentage : " + pourcentage);
              // tabNbBateauxDuree_[cpt]/nbBateaux_*100;//nbBateaux qui attendent ou pas suivant
              // str+=h+" " +tabNbBateauxDuree_[cpt]+ "\n etiquette \n \""+pourcentage+" \" \n";
              final double h1 = h * y;
              str += h1 + " " + nbBat + "\n ";// "\n etiquette \n \""+ pourcentage+"\" \n";
              // FuLog.debug("coordonnees : "+h1+ " " + nbBat + " % "+pourcentage );
              System.out.println("coordonnees : " + h1 + " " + nbBat + " % " + pourcentage);

            }
            str += "  }\n";
            str += "}\n";
            str += " \n";
          }
        }
      }

      if (cSeuil_.isSelected() && !tSeuil_.getValue().equals("0")) {
        str += " contrainte\n";
        str += "{\n";
        // a mettre le seuil
        final String tempSeuil = Sinavi2Lib.convertirHeureEnCentieme(Double.parseDouble(tSeuil_.getText().replace(',',
            '.')));
        str += "titre \"seuil : " + Double.parseDouble(tSeuil_.getText().replace(',', '.')) + "\"\n";
        str += "orientation verticale\n";
        str += " type max\n";
        str += " valeur     " + tempSeuil + "\n";

        str += "}\n";
        str += " \n";

      }
    } else {// dur�e en ordonn�e
      str += "axe\n";
      str += "{\n";
      str += "titre \"Nb Bateaux \" \n";
      str += "unite \"Bateau\" \n";
      str += "orientation horizontal\n";
      str += "graduations oui\n";
      str += "minimum  0\n";
      // str+="maximum "+maxNbBateaux()+"\n";
      final double max = nbBateaux_;
      str += "maximum  " + (max + (max / 20)) + "\n";
      int pas = (int) Math.round(max / 20);
      if (pas == 0) {
        pas = 1;
      }
      str += "pas " + pas + "\n";
      str += "}\n";
      str += "axe\n";
      str += "{\n";
      str += "titre \"Attentes \" \n";
      str += "unite \"heure\" \n";
      str += "orientation vertical \n";
      str += "conversionHM \n";
      str += "graduations oui\n";
      str += "minimum  0.0\n";
      /*
       * str+="maximum "+(maxDureeAttente()+(maxDureeAttente()/20))+"\n"; double y =(maxDureeAttente()/20);
       */
      str += "maximum  " + ((maxDureeAttente() + (maxDureeAttente() / 20)) / 3600) + "\n";// *
      final double y = ((maxDureeAttente() / 20) / 3600);// *
      str += "pas " + y + "\n";
      str += "}\n";
      if (!comparaison_) {
        if (!rCumul_.isSelected()) {
          str += "courbe\n";
          str += "{\n";
          str += "titre \"Nb de Bateaux\"\n";
          if (rCourbe_.isSelected()) {
            str += "type courbe\n";
          } else {
            str += "type histogramme\n";
          }
          str += "aspect\n";
          str += " {\n";
          if (rHistogramme_.isSelected()) {
            // str+="contour.largeur 1 \n";
            str += "contour.largeur " + (int) (nbBateaux_ / 20) + " \n";
            str += "surface.couleur FF8800 \n";
            str += "texte.couleur 000000 \n";
            str += "contour.couleur 000000 \n";
          } else {
            str += "contour.couleur FF8800\n";
          }
          str += "}\n";
          str += " valeurs\n";
          str += "  {\n";
          for (int cpt = 0; cpt < 20; cpt++) {
            final int h = cpt + 1;
            // double pourcentage=tabNbBateauxDuree_[cpt]/nbBateaux_*100;//nbBateaux qui attendent ou pas suivant
            double pourcentage = (tabNbBateauxDuree_[cpt] * 100);// nbBateaux qui attendent ou pas suivant
            pourcentage = conversionDeuxChiffres(pourcentage / nbBateaux_);
            // str+=tabNbBateauxDuree_[cpt]+" " +h+ "\n etiquette \n \""+ pourcentage+"\" \n";
            final double h1 = h * y;
            str += tabNbBateauxDuree_[cpt] + " " + h1 + "\n etiquette \n \"" + pourcentage + "\" \n";
          }
          str += "  }\n";
          str += "}\n";
          str += " \n";
        } else {// cumule
          str += "courbe\n";
          str += "{\n";
          str += "titre \"Nb de Bateaux\"\n";
          if (rCourbe_.isSelected()) {
            str += "type courbe\n";
          } else {
            str += "type histogramme\n";
          }
          str += "aspect\n";
          str += " {\n";
          if (rHistogramme_.isSelected()) {
            // str+="contour.largeur 1 \n";
            str += "contour.largeur " + (int) (nbBateaux_ / 20) + " \n";
            str += "surface.couleur FF8800 \n";
            str += "texte.couleur 000000 \n";
            str += "contour.couleur 000000 \n";
          } else {
            str += "contour.couleur FF8800\n";
          }
          str += "}\n";
          str += " valeurs\n";
          str += "  {\n";
          double pourcentage = 0;
          double nbBat = 0;
          for (int cpt = 0; cpt < 20; cpt++) {
            final int h = cpt + 1;
            nbBat += tabNbBateauxDuree_[cpt];
            pourcentage = nbBat * 100;
            pourcentage = conversionDeuxChiffres(pourcentage / nbBateaux_);
            final double h1 = h * y;
            str += nbBat + "  " + h1 + "\n etiquette \n \"" + pourcentage + "\" \n";

          }
          str += "  }\n";
          str += "}\n";
          str += " \n";

        }
      } else {// si comparaison
        if (!rCumul_.isSelected()) {
          for (int i = 0; i < listeCoordonnees_.size(); i++) {
        	 // int numero = listeCoordonnees_.;
            str += "courbe\n";
            str += "{\n";
            int numeroSel=imp_.simulationsSel_[i];
            // str+="titre \"Nb de Bateaux\"\n";
            str += "titre \"" + ((SSimulationSinavi2) imp_.listeSimulationsSinavi2_.get(numeroSel)).nomSim + "\"\n";
            if (rCourbe_.isSelected()) {
              str += "type courbe\n";
            } else {
              str += "type histogramme\n";
            }
            str += "aspect\n";
            str += " {\n";
            if (rHistogramme_.isSelected()) {
              // str+="contour.largeur 1 \n";
              str += "contour.largeur " + (int) (nbBateaux_ / 20) + " \n";
              str += "surface.couleur " + tabCouleurs[i] + " \n";
              str += "texte.couleur " + "000000" + " \n";
              str += "contour.couleur 000000 \n";
            } else {
              str += "contour.couleur " + tabCouleurs[i] + " \n";
            }
            str += "}\n";
            str += " valeurs\n";
            str += "  {\n";
            tabNbBateauxDuree_ = (double[]) listeCoordonnees_.get(i);
            for (int cpt = 0; cpt < 20; cpt++) {
              final int h = cpt + 1;
              // double pourcentage=tabNbBateauxDuree_[cpt]/nbBateaux_*100;//nbBateaux qui attendent ou pas suivant
              double pourcentage = (tabNbBateauxDuree_[cpt] * 100);// nbBateaux qui attendent ou pas suivant
              pourcentage = conversionDeuxChiffres(pourcentage / nbBateaux_);
              // str+=tabNbBateauxDuree_[cpt]+" " +h+ "\n etiquette \n \""+ pourcentage+"\" \n";
              final double h1 = h * y;
              str += tabNbBateauxDuree_[cpt] + "\n";// " " +h1+ "\n etiquette \n \""+ pourcentage+"\" \n";
            }
            str += "  }\n";
            str += "}\n";
            str += " \n";
          }
        } else {// cumule
          for (int i = 0; i < listeCoordonnees_.size(); i++) {
            str += "courbe\n";
            str += "{\n";
            int numeroSel=imp_.simulationsSel_[i];
            // str+="titre \"Nb de Bateaux\"\n";
            str += "titre \"" + ((SSimulationSinavi2) imp_.listeSimulationsSinavi2_.get(numeroSel)).nomSim + "\"\n";
            if (rCourbe_.isSelected()) {
              str += "type courbe\n";
            } else {
              str += "type histogramme\n";
            }
            str += "aspect\n";
            str += " {\n";
            if (rHistogramme_.isSelected()) {
              // str+="contour.largeur 1 \n";
              str += "contour.largeur " + (int) (nbBateaux_ / 20) + " \n";
              str += "surface.couleur  " + tabCouleurs[i] + " \n";
              str += "texte.couleur 000000" + " \n";
              str += "contour.couleur 000000 \n ";
            } else {
              str += "contour.couleur " + tabCouleurs[i] + " \n";
            }
            str += "}\n";
            str += " valeurs\n";
            str += "  {\n";
            double pourcentage = 0;
            double nbBat = 0;
            tabNbBateauxDuree_ = (double[]) listeCoordonnees_.get(i);
            for (int cpt = 0; cpt < 20; cpt++) {
              final int h = cpt + 1;
              nbBat += tabNbBateauxDuree_[cpt];
              pourcentage = nbBat * 100;
              pourcentage = conversionDeuxChiffres(pourcentage / nbBateaux_);
              final double h1 = h * y;
              str += nbBat + "  " + h1 + "\n";// "\n etiquette \n \""+ pourcentage+"\" \n";

              /*
               * int h=cpt+1; nbBat+=tabNbBateauxDuree_[cpt];
               * pourcentage+=tabNbBateauxDuree_[cpt]/nbBateaux_*100;//nbBateaux qui attendent ou pas suivant
               * //str+=tabNbBateauxDuree_[cpt]+" " +h+ "\n etiquette \n \""+ pourcentage+"\"\n"; double h1=h*y;
               * str+=tabNbBateauxDuree_[cpt]+" " +h1+ "\n etiquette \n \""+ pourcentage+"\" \n";
               */

            }
            str += "  }\n";
            str += "}\n";
            str += " \n";
          }
        }
      }
      if (cSeuil_.isSelected() && !tSeuil_.getValue().equals("0")) {
        final String tempSeuil = Sinavi2Lib.convertirHeureEnCentieme(Double.parseDouble(tSeuil_.getText().replace(',',
            '.')));
        str += " contrainte\n";
        str += "{\n";
        // a mettre le seuil

        str += "titre \"seuil : max =" + Double.parseDouble(tSeuil_.getText().replace(',', '.')) + "\"\n";
        str += " type max\n";
        str += " valeur     " + tempSeuil + "\n";

        str += "}\n";
        str += " \n";

      }
    }

    // a mettreu ne case seuil a demander encore pour la legnede
    str += "  } \"\n";
    graphe.setFluxDonnees(new ByteArrayInputStream(str.getBytes()));
    // Sinavi2FilleGraphe f=new Sinavi2FilleGraphe(imp_,imp_.getInformationsDocument(),graphe);
    imp_.affGraphe2(graphe);
    // f.setGraphe(graphe);

  }

  public double maxDureeAttente() {// on mettera certainement une variable globale pour ne pas appeler trop de fiosl a
    // fonction

    double maxDuree = 0;
    for (int i = 0; i < tabBateaux_.length; i++) {
      if (maxDuree < tabBateaux_[i]) {
        maxDuree = tabBateaux_[i];
      }

    }
    return maxDuree;
  }

  private double maxNbBateaux() {
    remplirTabNbBateauxDuree();
    nbBateaux_ = 0;
    double nbBatMax = 0;
    for (int i = 0; i < tabNbBateauxDuree_.length; i++) {
      nbBateaux_ += tabNbBateauxDuree_[i];
      if (nbBatMax < tabNbBateauxDuree_[i]) {
        nbBatMax = tabNbBateauxDuree_[i];
      }

    }

    return nbBatMax;

  }

  public void remplirTabNbBateauxDuree() {// a finir verif 0
    tabNbBateauxDuree_ = new double[20];
    System.out.println("max :" + maxDureeAttente());
    final int maxduree = (int) (maxDureeAttente());
    for (int i = 0; i < tabBateaux_.length; i++) {
      int x;
      if (maxduree != 0) {
        x = (int) ((int) tabBateaux_[i] / (maxduree / 20));
      } else {
        x = 0;
      }
      if (x == 20) {
        x = 19;
      }
      System.out.println("bateau " + (int) tabBateaux_[i] + " x : " + x);
      tabNbBateauxDuree_[x]++;

    }
    /**
     * il faut r�aliser une moyenne donc on divise le nombre de bateaux
     */
    for (int i = 0; i < 20; i++) {
      tabNbBateauxDuree_[i] = (tabNbBateauxDuree_[i] / imp_.NB_SIMULATIONS);
    }
  }

  private double dureeAttente(final int _cpt, final char _avalantMontant, final int _numeroBateau, final int _gare) {

    SResultatHistorique sh = listeHistorique_[_cpt];
    Sinavi2TypeHistorique s = new Sinavi2TypeHistorique(sh);
    double dureeAttente = s.attente_;
    for (int j = _cpt + 1; j < listeHistorique_.length; j++) {
      sh = listeHistorique_[j];
      s = new Sinavi2TypeHistorique(sh);

      if (s.avalantMontant_ == _avalantMontant && s.elementAFranchir_.equals("G") && s.numeroElement_ == _gare
          && s.numeroBateau_ == _numeroBateau) {
        dureeAttente += s.attente_;
        return dureeAttente;
      } else if (s.numeroBateau_ == _numeroBateau) {
        dureeAttente += s.attente_;
      }

    }
    return -1;

  }

  public void selectionDonnees(final int _numSim) {
    donneesGraph_ = new ArrayList();
    tabBateaux_ = new double[1];
    listeHistorique_ = ((SSimulationSinavi2) listeSimulations_.get(_numSim)).resultatHistorique;

    final int gare = Integer.parseInt((String) cGareDeb_.getSelectedItem());
    final int garef = Integer.parseInt((String) cGareFin_.getSelectedItem());
    final String typeBateau = liListeBateaux_.getSelectedValue().toString();
    int cpt = 0;
    int nbbat = 0;
    // les deux sens ca
    if (cSensNav_.getSelectedIndex() == 0) {
      for (int j = 0; j < listeHistorique_.length; j++) {
        final SResultatHistorique sh = listeHistorique_[j];
        cpt++;
        final Sinavi2TypeHistorique s = new Sinavi2TypeHistorique(sh);
        if (s.avalantMontant_ == 'M'
            && s.elementAFranchir_.equals("G")
            && s.numeroElement_ == garef
            && (((SParametresBateau) (listeBateaux_.get(s.numeroTypeBateau_ - 1))).identification
                .equalsIgnoreCase(typeBateau) || liListeBateaux_.getSelectedIndex() == 0)) {
          if ((s.heureEntree_ % 86400 >= (dHoraireDebutHeure_.getDureeField() + dHoraireDebutMinute_.getDureeField()))
              && (s.heureEntree_ % 86400 <= (dHoraireFinHeure_.getDureeField() + dHoraireFinMinute_.getDureeField()))) {
            final double attente = dureeAttente(cpt, s.avalantMontant_, s.numeroBateau_, gare);
            if (attente != -1) {
              final double[] tabBateauxTemp = new double[tabBateaux_.length + 1];
              for (int i = 0; i < tabBateaux_.length; i++) {
                tabBateauxTemp[i] = tabBateaux_[i];
              }
              tabBateauxTemp[nbbat] = attente;
              tabBateaux_ = tabBateauxTemp;
              nbbat++;
            }
          }
        } else if (s.avalantMontant_ == 'A'
            && s.elementAFranchir_.equals("G")
            && s.numeroElement_ == gare
            && (((SParametresBateau) (listeBateaux_.get(s.numeroTypeBateau_ - 1))).identification
                .equalsIgnoreCase(typeBateau) || liListeBateaux_.getSelectedIndex() == 0)) {
          if ((s.heureEntree_ % 86400 >= (dHoraireDebutHeure_.getDureeField() + dHoraireDebutMinute_.getDureeField()))
              && (s.heureEntree_ % 86400 <= (dHoraireFinHeure_.getDureeField() + dHoraireFinMinute_.getDureeField()))) {
            final double attente = dureeAttente(cpt, s.avalantMontant_, s.numeroBateau_, garef);
            if (attente != -1) {
              final double[] tabBateauxTemp = new double[tabBateaux_.length + 1];
              for (int i = 0; i < tabBateaux_.length; i++) {
                tabBateauxTemp[i] = tabBateaux_[i];
              }
              tabBateauxTemp[nbbat] = attente;
              tabBateaux_ = tabBateauxTemp;
              nbbat++;
            }
          }
        }
      }
    }

    // montant
    else if (cSensNav_.getSelectedIndex() == 1) {
      cpt = 0;
      for (int j = 0; j < listeHistorique_.length; j++) {
        cpt++;
        final SResultatHistorique sh = listeHistorique_[j];
        final Sinavi2TypeHistorique s = new Sinavi2TypeHistorique(sh);
        if (s.avalantMontant_ == 'M'
            && s.elementAFranchir_.equals("G")
            && s.numeroElement_ == garef
            && (((SParametresBateau) (listeBateaux_.get(s.numeroTypeBateau_ - 1))).identification
                .equalsIgnoreCase(typeBateau) || liListeBateaux_.getSelectedIndex() == 0)) {
          if ((s.heureEntree_ % 86400 >= (dHoraireDebutHeure_.getDureeField() + dHoraireDebutMinute_.getDureeField()))
              && (s.heureEntree_ % 86400 <= (dHoraireFinHeure_.getDureeField() + dHoraireFinMinute_.getDureeField()))) {
            final double attente = dureeAttente(cpt, s.avalantMontant_, s.numeroBateau_, gare);
            if (attente != -1) {
              final double[] tabBateauxTemp = new double[tabBateaux_.length + 1];
              for (int i = 0; i < tabBateaux_.length; i++) {
                tabBateauxTemp[i] = tabBateaux_[i];
              }
              tabBateauxTemp[nbbat] = attente;
              tabBateaux_ = tabBateauxTemp;
              nbbat++;
            }
          }

        }
      }
    }
    // avalant
    else {
      cpt = 0;
      for (int j = 0; j < listeHistorique_.length; j++) {
        cpt++;
        final SResultatHistorique sh = listeHistorique_[j];
        final Sinavi2TypeHistorique s = new Sinavi2TypeHistorique(sh);
        if (s.avalantMontant_ == 'A'
            && s.elementAFranchir_.equals("G")
            && s.numeroElement_ == gare
            && (((SParametresBateau) (listeBateaux_.get(s.numeroTypeBateau_ - 1))).identification
                .equalsIgnoreCase(typeBateau) || liListeBateaux_.getSelectedIndex() == 0)) {
          if ((s.heureEntree_ % 86400 >= (dHoraireDebutHeure_.getDureeField() + dHoraireDebutMinute_.getDureeField()))
              && (s.heureEntree_ % 86400 <= (dHoraireFinHeure_.getDureeField() + dHoraireFinMinute_.getDureeField()))) {
            final double attente = dureeAttente(cpt, s.avalantMontant_, s.numeroBateau_, garef);
            if (attente != -1) {
              final double[] tabBateauxTemp = new double[tabBateaux_.length + 1];
              for (int i = 0; i < tabBateaux_.length; i++) {
                tabBateauxTemp[i] = tabBateaux_[i];
              }
              tabBateauxTemp[nbbat] = attente;
              tabBateaux_ = tabBateauxTemp;
              nbbat++;
            }
          }
        }
      }
    }

  }

  private boolean simSel(final int indice) {
    for (int i = 0; i < imp_.simulationsSel_.length; i++) {
      if (imp_.simulationsSel_[i] == indice) {
        return true;
      }
    }
    return false;
  }

  private boolean verifParametres() {
	  if (cGareDeb_.getSelectedIndex() == cGareFin_.getSelectedIndex()) {
		  imp_.affMessage("Entrez des num�ros de gare diff�rents.");
		  return false;
	  }
	  else if (cGareDeb_.getSelectedIndex()>= cGareFin_.getSelectedIndex()){
		  imp_.affMessage("   Le num�ro de la gare finale doit �tre sup�rieur � la gare de d�but.  \n   Utilisez le sens Montant.");
		  return false;


	  } else if (liListeBateaux_.getSelectedIndex() == -1) {
		  imp_.affMessage("Selectionnez un type de bateau.");
		  return false;
	  } else if (liListeBateaux_.getSelectedIndex() == -1) {
		  imp_.affMessage("Selectionnez un type de bateau.");
		  return false;
	  }
	  return true;
  }

  public class Sinavi2TableauResAttentesBateaux extends BuInternalFrame implements ActionListener,
      InternalFrameListener {

    /**
     * tableau***** minimum moyenne maximum
     */

    private final BuLabel lTitre1_ = new BuLabel("Affichage du temps d'attentes par bateaux");

    private final BuButton bAnnuler_ = new BuButton(Sinavi2Resource.SINAVI2.getIcon("QUITTER"), "Quitter");
    private final BuButton bImprimer_ = new BuButton(Sinavi2Resource.SINAVI2.getIcon("IMPRIMER"), "Imprimer");

    /** * panel contenant les boutons, il est plac� en bas */

    private final BuPanel pBoutons1_ = new BuPanel();
    private final BuPanel pTitre1_ = new BuPanel();
    private final BuPanel pDonnees_ = new BuPanel();
    public BuScrollPane scrollPane_;
    public Sinavi2TableauAttentesBateaux tb1_;
    public BuTable table1_;
    public Sinavi2Implementation imp2_ = null;

    // public boolean comparaison_;
    public Sinavi2TableauResAttentesBateaux(final BuCommonImplementation _appli, final boolean _comparaison) {
      super("Affichage du temps d'attentes par bateaux", true, true, true, false);
      pTitre1_.add(lTitre1_);
      // comparaison_=_comparaison;
      tableauAttentesBateaux();

      table1_.setRowSelectionAllowed(true);
      table1_.setColumnSelectionAllowed(false);

      table1_.getColumnModel().getColumn(0).setPreferredWidth(100);
      table1_.getColumnModel().getColumn(1).setPreferredWidth(150);
      table1_.getColumnModel().getColumn(2).setPreferredWidth(150);
      table1_.getColumnModel().getColumn(3).setPreferredWidth(125);
      scrollPane_ = new BuScrollPane(table1_);
      pDonnees_.add(scrollPane_);
      table1_.setAutoscrolls(true);

      // pDonnees_.add(table1_);

      imp2_ = (Sinavi2Implementation) _appli.getImplementation();

      bAnnuler_.addActionListener(this);
      bImprimer_.addActionListener(this);

      ((JComponent) getContentPane()).setBorder(new EmptyBorder(25, 25, 25, 25));

      pBoutons1_.setLayout(new FlowLayout(FlowLayout.CENTER));
      pBoutons1_.add(bAnnuler_);
      pBoutons1_.add(bImprimer_);
      getContentPane().add(pTitre1_, BorderLayout.NORTH);
      getContentPane().add(pDonnees_, BorderLayout.CENTER);
      getContentPane().add(pBoutons1_, BorderLayout.SOUTH);

      // pack();
      setLocation(20, 20);

      /** *reinitialisation des valeurs* */

      setVisible(true);
      addInternalFrameListener(this);
      imp2_.addInternalFrame(this);

    }

    public void tableauAttentesBateaux() {
      int row = 0;
      //String s = new String();
      final double y = (maxDureeAttente() / 20);
      if (tb1_ == null) {
        tb1_ = new Sinavi2TableauAttentesBateaux();
      }
      if (!comparaison_ && !rPourcentage_.isSelected()) {
        selectionDonnees(imp_.getNumeroSimulationCourante());
        final Object bateauxSel = liListeBateaux_.getSelectedValue();
        double pourcentage = 0;
        double nbBat = 0;
        // for (int i=0;i<bateauxSel.length; i++){
        for (int cpt = 0; cpt < 20; cpt++) {

          final int h = cpt + 1;
          nbBat += tabNbBateauxDuree_[cpt];
          if (cpt == 0) {
           String s = bateauxSel.toString();
            tb1_.setValueAt(s, row, 0);
            pourcentage = nbBat * 100;
            pourcentage = conversionDeuxChiffres(pourcentage / nbBateaux_);
            System.out.println("pourcentage : " + pourcentage);
            final double h1 = h * y;
            System.out.println("coordonnees : " + h1 + " " + nbBat + " % " + pourcentage);

            s = String.valueOf(pourcentage);
            tb1_.setValueAt(s, row, 1);

            s = String.valueOf(Sinavi2Lib.determineHeureString((int) h1));
            tb1_.setValueAt(s, row, 2);

            s = "" + nbBat;
            tb1_.setValueAt(s, row, 3);
            row++;
            tb1_.setNbRow(row);
          } else {
            String s = " ";
            tb1_.setValueAt(s, row, 0);
            pourcentage = nbBat * 100;
            pourcentage = conversionDeuxChiffres(pourcentage / nbBateaux_);
            System.out.println("pourcentage : " + pourcentage);
            final double h1 = h * y;
            System.out.println("coordonnees : " + h1 + " " + nbBat + " % " + pourcentage);

            s = String.valueOf(pourcentage);
            tb1_.setValueAt(s, row, 1);

            s = String.valueOf(Sinavi2Lib.determineHeureString((int) h1));
            tb1_.setValueAt(s, row, 2);

            s = "" + nbBat;
            tb1_.setValueAt(s, row, 3);

            row++;
            tb1_.setNbRow(row);
          }
        }

      } else if (!comparaison_ && !rCumul_.isSelected()) {
        selectionDonnees(imp_.getNumeroSimulationCourante());
        final Object bateauxSel = liListeBateaux_.getSelectedValue();
        // double nbBat=0;
        for (int cpt = 0; cpt < 20; cpt++) {
          final int h = cpt + 1;
          // nbBat+=tabNbBateauxDuree_[cpt];
          if (cpt == 0) {
         String   s = bateauxSel.toString();
            tb1_.setValueAt(s, row, 0);
            System.out.println(" nb bat " + h + ": " + tabNbBateauxDuree_[cpt] + " nb bat total = " + nbBateaux_);
            double pourcentage = (tabNbBateauxDuree_[cpt] * 100);// nbBateaux qui attendent ou pas suivant
            pourcentage = conversionDeuxChiffres(pourcentage / nbBateaux_);

            final double h1 = h * y;
            s = String.valueOf(pourcentage);
            tb1_.setValueAt(s, row, 1);

            s = String.valueOf(Sinavi2Lib.determineHeureString((int) h1));
            tb1_.setValueAt(s, row, 2);

            s = "" + tabNbBateauxDuree_[cpt];
            tb1_.setValueAt(s, row, 3);
            row++;
            tb1_.setNbRow(row);
          } else {
            String  s = " ";
            tb1_.setValueAt(s, row, 0);
            System.out.println(" nb bat " + h + ": " + tabNbBateauxDuree_[cpt] + " nb bat total = " + nbBateaux_);
            double pourcentage = (tabNbBateauxDuree_[cpt] * 100);// nbBateaux qui attendent ou pas suivant
            pourcentage = conversionDeuxChiffres(pourcentage / nbBateaux_);

            final double h1 = h * y;
            s = String.valueOf(pourcentage);
            tb1_.setValueAt(s, row, 1);

            s = String.valueOf(Sinavi2Lib.determineHeureString((int) h1));
            tb1_.setValueAt(s, row, 2);

            s = "" + tabNbBateauxDuree_[cpt];
            tb1_.setValueAt(s, row, 3);

            row++;
            tb1_.setNbRow(row);
          }
        }
      } else if (comparaison_ && !rCumul_.isSelected()) {
        listeCoordonnees_ = new ArrayList();
        for (int i = 0; i < imp_.simulationsSel_.length; i++) {
          selectionDonnees(imp_.simulationsSel_[i]);
          remplirTabNbBateauxDuree();
          listeCoordonnees_.add(tabNbBateauxDuree_);
        }

        for (int i = 0; i < listeCoordonnees_.size(); i++) {
          tabNbBateauxDuree_ = (double[]) listeCoordonnees_.get(i);
          // double nbBat=0;
          for (int cpt = 0; cpt < 20; cpt++) {
            final int h = cpt + 1;
            // nbBat+=tabNbBateauxDuree_[cpt];
            System.out.println(" nb bat " + h + ": " + tabNbBateauxDuree_[cpt] + " nb bat total = " + nbBateaux_);
            double pourcentage = (tabNbBateauxDuree_[cpt] * 100);// nbBateaux qui attendent ou pas suivant
            pourcentage = conversionDeuxChiffres(pourcentage / nbBateaux_);
            final double h1 = h * y;
            if (cpt == 0) {
              String  s = ((SSimulationSinavi2) listeSimulations_.get(imp_.simulationsSel_[i])).nomSim + ": "
                  + liListeBateaux_.getSelectedValue().toString();
              tb1_.setValueAt(s, row, 0);

              s = String.valueOf(pourcentage);

              tb1_.setValueAt(s, row, 1);

              s = String.valueOf(Sinavi2Lib.determineHeureString((int) h1));
              tb1_.setValueAt(s, row, 2);

              s = "" + tabNbBateauxDuree_[cpt];
              tb1_.setValueAt(s, row, 3);

              row++;
              tb1_.setNbRow(row);
            } else {

              String s = "";
              tb1_.setValueAt(s, row, 0);

              s = String.valueOf(pourcentage);

              tb1_.setValueAt(s, row, 1);

              s = String.valueOf(Sinavi2Lib.determineHeureString((int) h1));
              tb1_.setValueAt(s, row, 2);

              s = "" + tabNbBateauxDuree_[cpt];
              tb1_.setValueAt(s, row, 3);

              row++;
              tb1_.setNbRow(row);
            }
          }

        }
      } else if (comparaison_ && !rPourcentage_.isSelected()) {
        listeCoordonnees_ = new ArrayList();
        for (int i = 0; i < imp_.simulationsSel_.length; i++) {
          selectionDonnees(imp_.simulationsSel_[i]);
          remplirTabNbBateauxDuree();
          listeCoordonnees_.add(tabNbBateauxDuree_);
        }
        for (int i = 0; i < listeCoordonnees_.size(); i++) {
          tabNbBateauxDuree_ = (double[]) listeCoordonnees_.get(i);
          double pourcentage = 0;
          double nbBat = 0;

          for (int cpt = 0; cpt < 20; cpt++) {
            final int h = cpt + 1;
            nbBat += tabNbBateauxDuree_[cpt];
            // nbBat =tabNbBateauxDuree_[cpt];

            pourcentage = nbBat * 100;
            pourcentage = conversionDeuxChiffres(pourcentage / nbBateaux_);
            System.out.println("pourcentage : " + pourcentage);
            final double h1 = h * y;
            if (cpt == 0) {
              String  s = ((SSimulationSinavi2) listeSimulations_.get(imp_.simulationsSel_[i])).nomSim + ": "
                  + liListeBateaux_.getSelectedValue().toString();
              tb1_.setValueAt(s, row, 0);

              s = String.valueOf(pourcentage);
              tb1_.setValueAt(s, row, 1);

              s = String.valueOf(Sinavi2Lib.determineHeureString((int) h1));
              tb1_.setValueAt(s, row, 2);

              s = "" + nbBat;
              tb1_.setValueAt(s, row, 3);

              row++;
              tb1_.setNbRow(row);
            } else {
              String    s = "";
              tb1_.setValueAt(s, row, 0);

              s = String.valueOf(pourcentage);
              tb1_.setValueAt(s, row, 1);

              s = String.valueOf(Sinavi2Lib.determineHeureString((int) h1));
              tb1_.setValueAt(s, row, 2);

              s = "" + nbBat;
              tb1_.setValueAt(s, row, 3);
              row++;
              tb1_.setNbRow(row);
            }
          }

        }
      }

      table1_ = new BuTable(tb1_.data_, tb1_.nomCol());
      table1_.repaint();

    }

    /**
     * @param _e
     */

    public void annuler() {
      imp2_.removeInternalFrame(this);
      imp2_.resetFille2AffBateaux();
    }

    public void actionPerformed(final ActionEvent _e) {
      if (_e.getSource() == bAnnuler_) {
        annuler();

      }

      else if (_e.getSource() == bImprimer_) {
        printInFile();

      }

    }

    /**
     * on fait choisir un fichier � l'aide d'un chooser on cr� un tableau temporaire tbtemp. o� : on ajoute une ligne
     * avec le titre et du blanc pour les autres cellules de la ligne on ajoute les colonnes sur la ligne suivante on
     * ajoute enfin les donn�es on cr� le fichier excel on write le bateau
     */
    private void printInFile() {

      final File s = imp2_.enregistrerXls();
      if (s == null) {
        return;
      }
      final Sinavi2TableauAttentesBateaux tbtemp = new Sinavi2TableauAttentesBateaux();
      tbtemp.data_ = new Object[tb1_.getRowCount() + 2][tb1_.getColumnCount()];
      tbtemp.initNomCol(1);
      tbtemp.setColumnName(" Attente du type de bateau  " + liListeBateaux_.getSelectedValue().toString(), 0);
      for (int i = 1; i < tb1_.getColumnCount(); i++) {
        tbtemp.setColumnName(" ", i);
      }

      tbtemp.setNbRow(tb1_.getRowCount() + 2);
      for (int i = 2; i <= (tb1_.getRowCount() + 1); i++) {
        for (int j = 0; j < tb1_.getColumnCount(); j++) {
          tbtemp.data_[i][j] = tb1_.data_[i - 2][j];
        }

      }
      final CtuluTableExcelWriter test = new CtuluTableExcelWriter(tbtemp, s);

      try {
        test.write(null);

      } catch (final RowsExceededException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      } catch (final WriteException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      } catch (final IOException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }
    }

    public void affMessage(final String _t) {
      final BuDialogMessage dialog_mess = new BuDialogMessage(imp_.getApp(), imp_.getInformationsSoftware(), "" + _t);
      dialog_mess.activate();
    }

    public void internalFrameActivated(final InternalFrameEvent _e) {
    // TODO Auto-generated method stub

    }

    public void internalFrameClosed(final InternalFrameEvent _e) {
      this.closable = true;

    }

    public void internalFrameDeactivated(final InternalFrameEvent _e) {
    // TODO Auto-generated method stub

    }

    public void internalFrameDeiconified(final InternalFrameEvent _e) {
    // TODO Auto-generated method stub

    }

    public void internalFrameIconified(final InternalFrameEvent _e) {
    // TODO Auto-generated method stub

    }

    public void internalFrameOpened(final InternalFrameEvent _e) {
    // TODO Auto-generated method stub

    }

    public void internalFrameClosing(final InternalFrameEvent _e) {
    // TODO Auto-generated method stub

    }

  }

  public void internalFrameActivated(final InternalFrameEvent e) {
  // TODO Auto-generated method stub

  }

  public void internalFrameClosed(final InternalFrameEvent e) {

  // TODO Auto-generated method stub

  }

  public void internalFrameClosing(final InternalFrameEvent e) {
  // TODO Auto-generated method stub

  }

  public void internalFrameDeactivated(final InternalFrameEvent e) {
  // TODO Auto-generated method stub

  }

  public void internalFrameDeiconified(final InternalFrameEvent e) {
  // TODO Auto-generated method stub

  }

  public void internalFrameIconified(final InternalFrameEvent e) {
  // TODO Auto-generated method stub

  }

  public void internalFrameOpened(final InternalFrameEvent e) {
  // TODO Auto-generated method stub

  }

  public void affMessage(final String _t) {
    final BuDialogMessage dialog_mess = new BuDialogMessage(imp_.getApp(), imp_.getInformationsSoftware(), "" + _t);
    dialog_mess.activate();
  }

  public void focusGained(final FocusEvent _e) {

  }

  public void focusLost(final FocusEvent _e) {

  }

  public void valueChanged(final ListSelectionEvent _e) {
  // TODO Auto-generated method stub

  }

  public void itemStateChanged(final ItemEvent _e) {
  // TODO Auto-generated method stub

  }

  public double conversionDeuxChiffres(final double value) {
    final NumberFormat formatter = CtuluLib.getDecimalFormat();
    formatter.setMaximumFractionDigits(2);
    formatter.setMinimumFractionDigits(2);
    final String s = formatter.format(value);
    return Double.parseDouble(s);
  }

}
