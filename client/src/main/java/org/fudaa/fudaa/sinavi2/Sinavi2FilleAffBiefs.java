/*
 * @file         Sinavi2FilleAff�Bateaux.java
 * @creation     2001-05-17
 * @modification $Date: 2007-05-04 14:01:06 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */

package org.fudaa.fudaa.sinavi2;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.ListIterator;

import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.border.EmptyBorder;
import javax.swing.event.InternalFrameEvent;
import javax.swing.event.InternalFrameListener;

import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuCommonImplementation;
import com.memoire.bu.BuDialogMessage;
import com.memoire.bu.BuInternalFrame;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuScrollPane;
import com.memoire.bu.BuTable;
import com.memoire.bu.BuTableSortModel;

import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.table.CtuluTableExcelWriter;

import org.fudaa.dodico.corba.sinavi2.SParametresBief;

/**
 * impl�mentation d'une fen�tre interne permettant d'afficher les param�tres des biefs sous forme de tableau. Cette
 * fen�tre regroupe toutes les fonctionnalit�s li�es aux biefs ajouter/Modifier/Supprimer/Imprimer/Indisponibilit�s cf.
 * Sinavi2FilleAffBateaux
 * 
 * @version $Revision: 1.14 $ $Date: 2007-05-04 14:01:06 $ by $Author: deniger $
 * @author Fatimatou Ka, Beno�t Maneuvrier
 */
public class Sinavi2FilleAffBiefs extends BuInternalFrame implements ActionListener, InternalFrameListener {

  private final BuLabel lTitre_ = new BuLabel("Affichage des biefs");

  /** bouton pour annuler */
  private final BuButton bAnnuler_ = new BuButton(Sinavi2Resource.SINAVI2.getIcon("QUITTER"), "Quitter");

  private final BuButton bIndisponibilites_ = new BuButton(Sinavi2Resource.SINAVI2.getIcon(""), "Indisponibilit�s");
  /** bouton pour annuler */
  private final BuButton bAjouter_ = new BuButton(Sinavi2Resource.SINAVI2.getIcon("AJOUTER"), "Ajouter");

  /** sauver */
  private final BuButton bModifier_ = new BuButton(Sinavi2Resource.SINAVI2.getIcon("MODIFIER"), "Modifier");

  private final BuButton bSupprimer_ = new BuButton(Sinavi2Resource.SINAVI2.getIcon("SUPPRIMER"), "Suppimer");

  private final BuButton bImprimer_ = new BuButton(Sinavi2Resource.SINAVI2.getIcon("IMPRIMER"), "Imprimer");

  private final BuButton bMiseAJour_ = new BuButton(Sinavi2Resource.SINAVI2.getIcon("MISAJOUR"), "Mise � Jour");

  public ArrayList listeBiefs2_;

  /** **bateau pour les champs en cas de modifications*** */
  public SParametresBief biefCourant_;

  private final BuPanel pBoutons_ = new BuPanel();

  private final BuPanel pTitre_ = new BuPanel();

  private final BuPanel pDonnees2_ = new BuPanel();

  private BuScrollPane scrollPane_;
  public Sinavi2TableauBief tb_;
  public BuTable table_;
  public BuTableSortModel tabBiefs_;
  public Sinavi2Implementation impl_ = null;

  /**
   * Constructeur de la fen�tre d'affichage des biefs
   * 
   * @param _appli : instance de sinavi2implementation
   * @param _liste2 : liste des biefs
   */
  public Sinavi2FilleAffBiefs(final BuCommonImplementation _appli, final ArrayList _liste2) {
    super("Affichage des Types de Biefs", true, true, true, false);
    pTitre_.add(lTitre_);
    listeBiefs2_ = _liste2;

    miseAJour(listeBiefs2_);
    table_.setRowSelectionAllowed(true);
    table_.setColumnSelectionAllowed(false);
    scrollPane_ = new BuScrollPane(table_);
    pDonnees2_.add(scrollPane_);

    table_.setAutoscrolls(true);

    impl_ = (Sinavi2Implementation) _appli.getImplementation();

    bAnnuler_.addActionListener(this);
    bIndisponibilites_.addActionListener(this);
    bAjouter_.addActionListener(this);
    bModifier_.addActionListener(this);
    bSupprimer_.addActionListener(this);
    bImprimer_.addActionListener(this);
    bMiseAJour_.addActionListener(this);
    if (!impl_.isPermettreModif()) {
      bAjouter_.setEnabled(false);
      bModifier_.setEnabled(false);
      bSupprimer_.setEnabled(false);

    }

    ((JComponent) getContentPane()).setBorder(new EmptyBorder(25, 25, 25, 25));

    pBoutons_.setLayout(new FlowLayout(FlowLayout.CENTER));
    pBoutons_.add(bAnnuler_);
    pBoutons_.add(bIndisponibilites_);
    pBoutons_.add(bAjouter_);
    pBoutons_.add(bModifier_);
    pBoutons_.add(bSupprimer_);
    pBoutons_.add(bImprimer_);
    pBoutons_.add(bMiseAJour_);
    getContentPane().add(pTitre_, BorderLayout.NORTH);
    getContentPane().add(pDonnees2_, BorderLayout.CENTER);
    getContentPane().add(pBoutons_, BorderLayout.SOUTH);

    pack();

    /** *reinitialisation des valeurs* */

    setVisible(true);
    addInternalFrameListener(this);
    impl_.addInternalFrame(this);

  }

  public void miseAJour(final ArrayList _lstBiefs) {

    if (tb_ == null) {
      tb_ = new Sinavi2TableauBief();
    }
    if (_lstBiefs != null) {
      final ListIterator iter = _lstBiefs.listIterator();
      int row = 0;
      while (iter.hasNext()) {

        SParametresBief c = new SParametresBief();
        c = (SParametresBief) iter.next();
        tb_.setValueAt(c.identification, row, 0);

        String s =  (Double.toString(c.longueur));
        tb_.setValueAt(s, row, 1);

        s = (Double.toString(c.largeur));
        tb_.setValueAt(s, row, 2);

        s = (Double.toString(c.hauteur));
        tb_.setValueAt(s, row, 3);

        s = (Double.toString(c.vitesse));
        tb_.setValueAt(s, row, 4);

        row++;
        tb_.setNbRow(row);

      }
    }
    table_ = new BuTable(tb_.data_, tb_.nomCol());
    table_.repaint();
  }

  public void annuler() {

    impl_.removeInternalFrame(this);
    impl_.resetFille2AffBiefs();
  }

  public void actionPerformed(final ActionEvent _e) {
    if (_e.getSource() == bAnnuler_) {
      annuler();
    } else if (_e.getSource() == bAjouter_) {
      impl_.ajouterBief(-1, false);

    } else if (_e.getSource() == bModifier_) {

      final int selection = table_.getSelectedRow();
      if (selection >= 0) {
        impl_.ajouterBief(selection, true);
      } else {
        JOptionPane.showMessageDialog(null, "Selectionnez une ligne");
      }

    } else if (_e.getSource() == bSupprimer_) {
      final int selection = table_.getSelectedRow();
      if (selection >= 0) {
        impl_.supprimerBiefs(selection);
      } else {
        impl_.message("Selectionnez une ligne");
      }

    } else if (_e.getSource() == bImprimer_) {
      final File f = impl_.enregistrerXls();
      if (f == null) {
        impl_.affMessage("Nom de Fichier incorrect");
        return;
      }
      final Sinavi2TableauBief tbtemp = new Sinavi2TableauBief();
      tbtemp.data_ = new Object[tb_.getRowCount() + 2][tb_.getColumnCount()];
      tbtemp.initNomCol(1);
      tbtemp.setColumnName("Liste des Biefs", 0);
      for (int i = 1; i < tb_.getColumnCount(); i++) {
        tbtemp.setColumnName(CtuluLibString.ESPACE, i);
      }
      tbtemp.setNbRow(tb_.getRowCount() + 2);
      for (int i = 2; i <= (tb_.getRowCount() + 1); i++) {
        for (int j = 0; j < tb_.getColumnCount(); j++) {
          tbtemp.data_[i][j] = tb_.data_[i - 2][j];
        }

      }
      final CtuluTableExcelWriter test = new CtuluTableExcelWriter(tbtemp, f);

      try {
        test.write(null);

      } catch (final RowsExceededException e) {
        e.printStackTrace();
      } catch (final WriteException e) {
        e.printStackTrace();
      } catch (final IOException e) {
        e.printStackTrace();
      }
    } else if (_e.getSource() == bMiseAJour_) {
      annuler();
      impl_.afficherBiefs();
    } else if (_e.getSource() == bIndisponibilites_) {
      annuler();
      final int selection = table_.getSelectedRow();
      if (selection >= 0) {
        annuler();
        impl_.addModIndisponibilites("bie:" + table_.getValueAt(selection, 0).toString(), 3);
      } else {
        impl_.addModIndisponibilites("", 3);
      }
    }
  }

  public void affMessage(final String _t) {
    new BuDialogMessage(impl_.getApp(), impl_.getInformationsSoftware(), "" + _t).activate();
  }

  public void internalFrameActivated(final InternalFrameEvent _e) {

  }

  public void internalFrameClosed(final InternalFrameEvent _e) {
    impl_.resetFille2AffBiefs();
  }

  public void internalFrameDeactivated(final InternalFrameEvent _e) {

  }

  public void internalFrameDeiconified(final InternalFrameEvent _e) {

  }

  public void internalFrameIconified(final InternalFrameEvent _e) {

  }

  public void internalFrameOpened(final InternalFrameEvent _e) {}

  public void internalFrameClosing(final InternalFrameEvent _e) {}

}
