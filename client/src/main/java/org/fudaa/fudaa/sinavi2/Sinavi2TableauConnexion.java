package org.fudaa.fudaa.sinavi2;

import java.util.List;

import javax.swing.table.AbstractTableModel;

import jxl.write.Label;
import jxl.write.Number;
import jxl.write.WritableCell;

/**
 * @author maneuvrier Cette classe a pour but d'afficher le tableaux des connexions entre les gares et d'offrir la
 *         possibilit� de l'impression c'est � dire la cr�ation d'un tableau excel d'o� l'impl�mentation de
 *         CtuluTableModelInterface. cf. Sinavi2TableauBateau
 */
public class Sinavi2TableauConnexion extends AbstractTableModel implements org.fudaa.ctulu.table.CtuluTableModelInterface {
  private int nbRow_ = 0;
  public Object[][] data_;// =new Object[15][15];
  private final String[] nomColonnes_ = { "El�ment", "Gare en Amont", "Gare en Aval" };

  public String[] getNomColonnes() {
    return nomColonnes_;
  }

  public void setNbRow(final int x) {
    nbRow_ = x;
  }

  public int getNbRow() {
    return nbRow_;
  }
  
  public int[] getSelectedRows() {
       return null;
    }

  public int getColumnCount() {
    return nomColonnes_.length;
  }

  public int getRowCount() {
    // return nbbateau*nbbief+1;
    return nbRow_;
  }

  public void initNomCol(final int _row) {
    for (int i = 0; i < getColumnCount(); i++) {
      data_[_row][i] = nomColonnes_[i];
    }

  }

  public Object getValueAt(final int _rowIndex, final int _columnIndex) {

    return data_[_rowIndex][_columnIndex];
  }

  public void setValueAt(final Object _value, final int _row, final int _col) {
    final Object x = _value;
    System.out.println(x.toString());
    data_[_row][_col] = new Object();

    data_[_row][_col] = x;

    // fireTableCellUpdated(row, col);

  }

  public int getMaxCol() {
    // TODO Auto-generated method stub
    return nomColonnes_.length;
  }

  public int getMaxRow() {
    // TODO Auto-generated method stub
    return nbRow_;
  }

  public Object getValue(final int _row, final int _col) {
    // TODO Auto-generated method stub
    return getValueAt(_row, _col);
  }

  public WritableCell getExcelWritable(final int _row, final int _col, int _rowXls, int _colXls) {
    final int r = _row;
    final int c = _col;
    /*
     * if (column_ != null) c = column_[c]; if (row_ != null) { r = row_[r]; }
     */
    final Object o = data_[r][c];
    if (o == null) {
      return null;
    }
    String s = o.toString();
    if (s == null) {
      return null;
    }
    s = s.trim();
    if (s.length() == 0) {
      return null;
    }
    try {
      return new Number(_colXls, _rowXls, Double.parseDouble(s));
    } catch (final NumberFormatException e) {}
    return new Label(_colXls, _rowXls, s);

    // return cell;
  }

  public void setColumnName(final String _name, final int _i) {
    nomColonnes_[_i] = _name;

  }

  public String getColumnName(final int _i) {
    return nomColonnes_[_i];

  }

public List<String> getComments() {
	// TODO Auto-generated method stub
	return null;
}

}
