/*
 * @creation     2001-05-17
 * @modification $Date: 2007-05-04 14:01:05 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.sinavi2;

import java.awt.Color;
import java.awt.Component;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.io.File;
import java.util.ArrayList;
import java.util.ListIterator;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JScrollPane;

import com.memoire.bu.BuCommonImplementation;
import com.memoire.bu.BuFileChooser;
import com.memoire.bu.BuFileFilter;
import com.memoire.bu.BuInternalFrame;
import com.memoire.bu.BuMenuItem;
import com.memoire.bu.BuPopupMenu;
import com.memoire.dja.DjaBox;
import com.memoire.dja.DjaCircle;
import com.memoire.dja.DjaDiamond;
import com.memoire.dja.DjaDirectArrow;
import com.memoire.dja.DjaGrid;
import com.memoire.dja.DjaLink;
import com.memoire.dja.DjaLoadSavePng;
import com.memoire.dja.DjaObject;
import com.memoire.dja.DjaVector;
import com.memoire.fu.FuLog;

import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.CtuluLibString;

import org.fudaa.dodico.corba.sinavi2.SParametresBief;
import org.fudaa.dodico.corba.sinavi2.SParametresEcluse;
import org.fudaa.dodico.corba.sinavi2.SParametresTrajets;

/**
 * Cette classe a pour but d'afficher le sch�ma du r�seau fluvial.
 */

class Sinavi2Reseau extends BuInternalFrame implements MouseListener, ActionListener, MouseMotionListener {
  public static String[] EXTENSIONS;
  public static final String GARE_CLEF = "gareKey";
  public static final String ECLUSE_CLEF = "ecluseKey";
  public static final String BIEF_CLEF = "biefKey";
  private int elemAMod = -1; // �l�ment � modifier
  private SParametresBief biefCourant_;
  private SParametresEcluse ecluseCourant_;
  private BuPopupMenu menu;
  private BuPopupMenu menu2;
  private BuMenuItem affBateaux;
  private BuMenuItem affBiefs;
  private BuMenuItem affEcluses;
  private BuMenuItem affControles;
  private BuMenuItem affTrajets;
  private BuMenuItem affCroisements;
  private BuMenuItem affTrematages;
  private BuMenuItem affVitesses;
  private BuMenuItem affManoeuvres;
  private BuMenuItem affConnexions;
  private BuMenuItem enregReseau;
  private BuMenuItem majReseau;

  private BuMenuItem modEcluse;
  private BuMenuItem modEcluseConnexion;
  private BuMenuItem supEcluse;
  private BuMenuItem modBief;
  private BuMenuItem modBiefConnexion;
  private BuMenuItem modBiefTrematages;
  private BuMenuItem modBiefCroisements;

  private BuMenuItem supBief;

  protected ArrayList listeGares_ = new ArrayList();
  protected ArrayList listeEcluses_ = new ArrayList();
  protected ArrayList listeBiefs_ = new ArrayList();
  protected ArrayList listeG_;
  protected ArrayList listeE_;
  protected ArrayList listeB_;
  protected DjaGrid grid2_;
  protected DjaVector vect_;
  protected Rectangle[] tabObjetsE_;
  protected Rectangle[] tabObjetsG_;
  protected DjaDirectArrow[] tabObjetsB_;
  public Sinavi2Implementation imp_;

  public Sinavi2Reseau(final BuCommonImplementation appli_, final ArrayList _listeBateaux, final ArrayList _listeBiefs,
      final ArrayList _listeEcluses, final ArrayList _listeGares, final SParametresTrajets _trajet) {
    imp_ = (Sinavi2Implementation) appli_.getImplementation();
    if (verifier_connexions(_listeBiefs, _listeEcluses, _listeGares)) {
      vect_ = new DjaVector();
      grid2_ = new DjaGrid();
      menu = new BuPopupMenu();
      modEcluse = new BuMenuItem("Modifier Ecluse");
      modBief = new BuMenuItem("Modifier Bief");
      modEcluseConnexion = new BuMenuItem("Modifier Connexion Ecluse");
      modBiefConnexion = new BuMenuItem("Modifier Connexion Bief");
      supBief = new BuMenuItem("Supprimer Bief");
      supEcluse = new BuMenuItem("Supprimer Ecluse");
      modBiefTrematages = new BuMenuItem("Modifier R�gles Trematages");
      modBiefCroisements = new BuMenuItem("Modifier R�gles Croisements");
      affBateaux = new BuMenuItem("Afficher Bateaux");
      affBiefs = new BuMenuItem("Afficher Biefs");
      affEcluses = new BuMenuItem("Afficher Ecluses");
      affControles = new BuMenuItem("Afficher Controles");
      affTrajets = new BuMenuItem("Afficher Trajets");
      affCroisements = new BuMenuItem("Afficher Croisements");
      affTrematages = new BuMenuItem("Afficher Trematages");
      affVitesses = new BuMenuItem("Afficher Vitesses");
      affManoeuvres = new BuMenuItem("Afficher Manoeuvres");
      affConnexions = new BuMenuItem("Afficher Connexions");
      enregReseau = new BuMenuItem("Imprimer Palette");
      majReseau = new BuMenuItem("MAJ Palette");
      // modBief.addMouseListener(this);
      modBief.addActionListener(this);
      modEcluse.addActionListener(this);
      modEcluseConnexion.addActionListener(this);
      modBiefConnexion.addActionListener(this);
      supBief.addActionListener(this);
      supEcluse.addActionListener(this);
      modBiefTrematages.addActionListener(this);
      modBiefCroisements.addActionListener(this);
      affBateaux.addActionListener(this);
      affBiefs.addActionListener(this);
      affEcluses.addActionListener(this);
      affControles.addActionListener(this);
      affTrajets.addActionListener(this);
      affCroisements.addActionListener(this);
      affTrematages.addActionListener(this);
      affVitesses.addActionListener(this);
      affManoeuvres.addActionListener(this);
      affConnexions.addActionListener(this);
      enregReseau.addActionListener(this);
      majReseau.addActionListener(this);
      menu.add(affBateaux);
      menu.add(affBiefs);
      menu.add(affEcluses);
      menu.add(affControles);
      menu.add(affTrajets);
      menu.add(affCroisements);
      menu.add(affTrematages);
      menu.add(affVitesses);
      menu.add(affManoeuvres);
      menu.add(affConnexions);
      menu.add(enregReseau);
      menu.add(majReseau);
      final DjaBox info = new DjaBox("AMONT ---> AVAL \n -->: bief \n o    : gare \n<>  : ecluse");
      info.setHeight(55);
      info.setWidth(150);
      info.setColor(Color.red);
      grid2_.add(info);
      // getContentPane().add(grid_);
      setTitle("Repr�sentation du r�seau");
      creationListeObjet(_listeBiefs, _listeEcluses, _listeGares);
      // creationListeObjet(listeB_,listeE_,listeG_);
      dispositionListeObjet();
      // DjaCircle test=new DjaCircle("1");
      listeG_ = _listeGares;
      listeE_ = _listeEcluses;
      listeB_ = _listeBiefs;
      ListIterator it2 = listeGares_.listIterator();
      while (it2.hasNext()) {
        grid2_.add((DjaCircle) it2.next());
      }
      it2 = listeEcluses_.listIterator();
      while (it2.hasNext()) {
        grid2_.add((DjaDiamond) it2.next());
        grid2_.add((DjaDirectArrow) it2.next());
        grid2_.add((DjaDirectArrow) it2.next());
      }
      it2 = listeBiefs_.listIterator();
      while (it2.hasNext()) {
        grid2_.add((DjaDirectArrow) it2.next());
      }
      // grid2_=new DjaGrid(vect_);

      translationObjets();
      rapprocherEcluses();
      ajustePosition();
      if (_trajet != null) {
        afficherTrajetCouleur(_trajet);
      }

      grid2_.repaint();
      grid2_.setVisible(true);
      // DjaLayoutTree t=new DjaLayoutTree();
      // t.layout(grid_);
      final JScrollPane scrollPane = new JScrollPane(grid2_);
      getContentPane().add(scrollPane);
      grid2_.addMouseListener(this);
      grid2_.addMouseMotionListener(this);

      // getContentPane().add(grid2_);
      setTitle("R�seau Sinavi");
      setBounds(10, 20, 300, 200);
      addMouseListener(this);
      supprimeIntersections();
      System.out.println("nombre de gares : " + listeGares_.size() + " ecluses" + listeEcluses_.size() / 3 + " biefs "
          + listeBiefs_.size());
    }
  }

  private void ajustePosition() {
    // ajuste la position des ancres.
    final ListIterator it = listeGares_.listIterator();
    while (it.hasNext()) {
      final DjaCircle g = (DjaCircle) it.next();
      final DjaLink[] tabPrec = g.getEndConnections();
      if (tabPrec.length == 1) {
        tabPrec[0].setEndPosition(6);
      } else if (tabPrec.length == 2) {
        if (tabPrec[0].getBeginObject().getY() < tabPrec[1].getBeginObject().getY()) {
          tabPrec[0].setEndPosition(7);
          tabPrec[1].setEndPosition(5);
        } else {
          tabPrec[0].setEndPosition(5);
          tabPrec[1].setEndPosition(7);
        }

      } else if (tabPrec.length == 3) {
        if (tabPrec[0].getBeginObject().getY() < tabPrec[1].getBeginObject().getY()) {
          if (tabPrec[1].getBeginObject().getY() < tabPrec[2].getBeginObject().getY()) {
            tabPrec[0].setEndPosition(7);
            tabPrec[1].setEndPosition(6);
            tabPrec[2].setEndPosition(5);
          } else if (tabPrec[0].getBeginObject().getY() < tabPrec[2].getBeginObject().getY()) {
            tabPrec[2].setEndPosition(7);
            tabPrec[0].setEndPosition(6);
            tabPrec[1].setEndPosition(5);
          } else {
            tabPrec[0].setEndPosition(7);
            tabPrec[2].setEndPosition(6);
            tabPrec[1].setEndPosition(5);
          }
        } else if (tabPrec[1].getBeginObject().getY() < tabPrec[2].getBeginObject().getY()) {
          if (tabPrec[2].getBeginObject().getY() < tabPrec[0].getBeginObject().getY()) {
            tabPrec[1].setEndPosition(7);
            tabPrec[2].setEndPosition(6);
            tabPrec[0].setEndPosition(5);
          } else if (tabPrec[2].getBeginObject().getY() < tabPrec[1].getBeginObject().getY()) {
            tabPrec[2].setEndPosition(7);
            tabPrec[1].setEndPosition(6);
            tabPrec[0].setEndPosition(5);
          } else {
            tabPrec[1].setEndPosition(7);
            tabPrec[0].setEndPosition(6);
            tabPrec[2].setEndPosition(5);
          }
        }
      }

      final DjaLink[] tabSuiv = g.getBeginConnections();
      if (tabSuiv.length == 1) {
        tabSuiv[0].setBeginPosition(2);
      } else if (tabSuiv.length == 2) {
        if (tabSuiv[0].getEndObject().getY() < tabSuiv[1].getEndObject().getY()) {
          tabSuiv[0].setBeginPosition(1);
          tabSuiv[1].setBeginPosition(3);
        } else {
          tabSuiv[0].setBeginPosition(3);
          tabSuiv[1].setBeginPosition(1);
        }

      } else if (tabSuiv.length == 3) {
        if (tabSuiv[0].getEndObject().getY() < tabSuiv[1].getEndObject().getY()) {
          if (tabSuiv[1].getEndObject().getY() < tabSuiv[2].getEndObject().getY()) {
            tabSuiv[0].setBeginPosition(1);
            tabSuiv[1].setBeginPosition(2);
            tabSuiv[2].setBeginPosition(3);
          } else if (tabSuiv[0].getEndObject().getY() < tabSuiv[2].getEndObject().getY()) {
            tabSuiv[2].setBeginPosition(1);
            tabSuiv[0].setBeginPosition(2);
            tabSuiv[1].setBeginPosition(3);
          } else {
            tabSuiv[0].setBeginPosition(1);
            tabSuiv[2].setBeginPosition(2);
            tabSuiv[1].setBeginPosition(3);
          }
        } else if (tabSuiv[1].getEndObject().getY() < tabSuiv[2].getEndObject().getY()) {
          if (tabSuiv[2].getEndObject().getY() < tabSuiv[0].getEndObject().getY()) {
            tabSuiv[1].setBeginPosition(1);
            tabSuiv[2].setBeginPosition(2);
            tabSuiv[0].setBeginPosition(3);
          } else if (tabSuiv[2].getEndObject().getY() < tabSuiv[1].getEndObject().getY()) {
            tabSuiv[2].setBeginPosition(1);
            tabSuiv[1].setBeginPosition(2);
            tabSuiv[0].setBeginPosition(3);
          } else {
            tabSuiv[1].setBeginPosition(1);
            tabSuiv[0].setBeginPosition(2);
            tabSuiv[2].setBeginPosition(3);
          }
        }
      }

    }

  }

  private void rapprocherEcluses() {
    final ListIterator it = listeEcluses_.listIterator();
    while (it.hasNext()) {
      FuLog.debug("rapprocher ecluse");
      final DjaDiamond e = (DjaDiamond) it.next();
      final DjaDirectArrow d = (DjaDirectArrow) it.next();// entrant
      final DjaDirectArrow d2 = (DjaDirectArrow) it.next();// dsortant
      final DjaCircle garePrec = (DjaCircle) d.getBeginObject();
      final DjaCircle gareSuiv = (DjaCircle) d2.getEndObject();
      final DjaLink[] tab = (DjaLink[]) garePrec.getBeginConnections();
      final ArrayList lecl = new ArrayList();
      final int[] ordonnees = new int[tab.length];
      // ordonn�es
      FuLog.debug("taille tab:" + tab.length);
      for (int i = 0; i < tab.length; i++) {
        // FuLog.debug(tab[i].getEndObject().getProperty(ECLUSE_CLEF));
        lecl.add(tab[i].getEndObject());
        ordonnees[i] = (tab[i].getEndObject()).getY();
      }
      // tri des ecluses de la plus haute � la plus basse
      for (int i = 0; i < ordonnees.length; i++) {
        int min = ordonnees[i];
        for (int j = i; j < ordonnees.length; j++) {
          if (ordonnees[j] < min) {
            final DjaDiamond temp = (DjaDiamond) lecl.get(i);
            lecl.set(i, lecl.get(j));
            lecl.set(j, temp);
            min = ordonnees[j];

          }
        }
      }
      // changement des coordonn�es
      if (lecl.size() == 1) {
        final ListIterator it2 = lecl.listIterator();
        final DjaObject temp = (DjaObject) it2.next();
        final ListIterator it3 = listeEcluses_.listIterator();
        boolean trouve = false;
        int cpt = 0;
        while (it3.hasNext() && !trouve) {
          if (((DjaObject) it3.next()).equals(temp)) {
            trouve = true;
            FuLog.debug("test trouve");

          } else {
            it3.next();
            it3.next();
            cpt += 3;
          }
        }
        temp.setY(garePrec.getY());
        if (trouve) {
          listeEcluses_.set(cpt, temp);
        }
      } else if (lecl.size() == 2) {
        final ListIterator it2 = lecl.listIterator();
        DjaObject temp = (DjaObject) it2.next();
        ListIterator it3 = listeEcluses_.listIterator();
        boolean trouve = false;
        int cpt = 0;
        while (it3.hasNext() && !trouve) {
          if (((DjaObject) it3.next()).equals(temp)) {
            trouve = true;
            FuLog.debug("test trouve");

          } else {
            it3.next();
            it3.next();
            cpt += 3;
          }
        }
        temp.setY(garePrec.getY() - 40);
        if (trouve) {
          listeEcluses_.set(cpt, temp);
        }
        temp = (DjaObject) it2.next();
        it3 = listeEcluses_.listIterator();
        trouve = false;
        cpt = 0;
        while (it3.hasNext() && !trouve) {
          if (((DjaObject) it3.next()).equals(temp)) {
            trouve = true;

          } else {
            it3.next();
            it3.next();
            cpt += 3;
          }
        }
        temp.setY(garePrec.getY() + 40);
        if (trouve) {
          listeEcluses_.set(cpt, temp);
        }
      } else if (lecl.size() == 3) {
        final ListIterator it2 = lecl.listIterator();
        DjaObject temp = (DjaObject) it2.next();
        ListIterator it3 = listeEcluses_.listIterator();
        boolean trouve = false;
        int cpt = 0;
        while (it3.hasNext() && !trouve) {
          if (((DjaObject) it3.next()).equals(temp)) {
            trouve = true;

          } else {
            it3.next();
            it3.next();
            cpt += 3;
          }
        }
        temp.setY(garePrec.getY() - 40);
        if (trouve) {
          listeEcluses_.set(cpt, temp);
        }
        temp = (DjaObject) it2.next();
        it3 = listeEcluses_.listIterator();
        trouve = false;
        cpt = 0;
        while (it3.hasNext() && !trouve) {
          if (((DjaObject) it3.next()).equals(temp)) {
            trouve = true;

          } else {
            it3.next();
            it3.next();
            cpt += 3;
          }
        }
        temp.setY(garePrec.getY());
        if (trouve) {
          listeEcluses_.set(cpt, temp);
        }

        temp = (DjaObject) it2.next();
        it3 = listeEcluses_.listIterator();
        trouve = false;
        cpt = 0;
        while (it3.hasNext() && !trouve) {
          if (((DjaObject) it3.next()).equals(temp)) {
            trouve = true;

          } else {
            it3.next();
            it3.next();
            cpt += 3;
          }
        }
        temp.setY(garePrec.getY() + 40);
        if (trouve) {
          listeEcluses_.set(cpt, temp);
        }
      } else if (lecl.size() == 4) {
        final ListIterator it2 = lecl.listIterator();
        DjaObject temp = (DjaObject) it2.next();
        ListIterator it3 = listeEcluses_.listIterator();
        boolean trouve = false;
        int cpt = 0;
        while (it3.hasNext() && !trouve) {
          if (((DjaObject) it3.next()).equals(temp)) {
            trouve = true;

          } else {
            it3.next();
            it3.next();
            cpt += 3;
          }
        }
        temp.setY(garePrec.getY() - 80);
        if (trouve) {
          listeEcluses_.set(cpt, temp);
        }
        temp = (DjaObject) it2.next();
        it3 = listeEcluses_.listIterator();
        trouve = false;
        cpt = 0;
        while (it3.hasNext() && !trouve) {
          if (((DjaObject) it3.next()).equals(temp)) {
            trouve = true;

          } else {
            it3.next();
            it3.next();
            cpt += 3;
          }
        }

        temp.setY(garePrec.getY() - 40);
        if (trouve) {
          listeEcluses_.set(cpt, temp);
        }
        temp = (DjaObject) it2.next();

        it3 = listeEcluses_.listIterator();
        trouve = false;
        cpt = 0;
        while (it3.hasNext() && !trouve) {
          if (((DjaObject) it3.next()).equals(temp)) {
            trouve = true;

          } else {
            it3.next();
            it3.next();
            cpt += 3;
          }
        }
        temp.setY(garePrec.getY());

        if (trouve) {
          listeEcluses_.set(cpt, temp);
        }
        temp = (DjaObject) it2.next();

        it3 = listeEcluses_.listIterator();
        trouve = false;
        cpt = 0;
        while (it3.hasNext() && !trouve) {
          if (((DjaObject) it3.next()).equals(temp)) {
            trouve = true;

          } else {
            it3.next();
            it3.next();
            cpt += 3;
          }
        }
        temp.setY(garePrec.getY() + 40);
        if (trouve) {
          listeEcluses_.set(cpt, temp);
        }
      } else if (lecl.size() == 5) {
        final ListIterator it2 = lecl.listIterator();
        DjaObject temp = (DjaObject) it2.next();
        ListIterator it3 = listeEcluses_.listIterator();
        boolean trouve = false;
        int cpt = 0;
        while (it3.hasNext() && !trouve) {
          if (((DjaObject) it3.next()).equals(temp)) {
            trouve = true;

          } else {
            it3.next();
            it3.next();
            cpt += 3;
          }
        }
        temp.setY(garePrec.getY() - 80);
        if (trouve) {
          listeEcluses_.set(cpt, temp);
        }
        temp = (DjaObject) it2.next();
        it3 = listeEcluses_.listIterator();
        trouve = false;
        cpt = 0;
        while (it3.hasNext() && !trouve) {
          if (((DjaObject) it3.next()).equals(temp)) {
            trouve = true;

          } else {
            it3.next();
            it3.next();
            cpt += 3;
          }
        }
        temp.setY(garePrec.getY() - 40);
        if (trouve) {
          listeEcluses_.set(cpt, temp);
        }
        temp = (DjaObject) it2.next();
        it3 = listeEcluses_.listIterator();
        trouve = false;
        cpt = 0;
        while (it3.hasNext() && !trouve) {
          if (((DjaObject) it3.next()).equals(temp)) {
            trouve = true;

          } else {
            it3.next();
            it3.next();
            cpt += 3;
          }
        }
        temp.setY(garePrec.getY());
        if (trouve) {
          listeEcluses_.set(cpt, temp);
        }
        temp = (DjaObject) it2.next();
        it3 = listeEcluses_.listIterator();
        trouve = false;
        cpt = 0;
        while (it3.hasNext() && !trouve) {
          if (((DjaObject) it3.next()).equals(temp)) {
            trouve = true;

          } else {
            it3.next();
            it3.next();
            cpt += 3;
          }
        }
        temp.setY(garePrec.getY() + 40);
        if (trouve) {
          listeEcluses_.set(cpt, temp);
        }
        temp = (DjaObject) it2.next();
        it3 = listeEcluses_.listIterator();
        trouve = false;
        cpt = 0;
        while (it3.hasNext() && !trouve) {
          if (((DjaObject) it3.next()).equals(temp)) {
            trouve = true;

          } else {
            it3.next();
            it3.next();
            cpt += 3;
          }
        }
        temp.setY(garePrec.getY() + 80);
        if (trouve) {
          listeEcluses_.set(cpt, temp);
        }
      }

    }

  }

  private void afficherTrajetCouleur(final SParametresTrajets _trajet) {
    System.out.println("afficher trajet couleur");
    final boolean trouve = false;
    DjaCircle deb = null, fin = null;
    final ListIterator it = listeGares_.listIterator();
    int x = 0;
    while (it.hasNext() && x != 2) {
      final DjaCircle d = (DjaCircle) it.next();
      if (d.getData(GARE_CLEF).equals("" + _trajet.gareDepart)) {
        deb = d;
        x++;
      }
      if (d.getData(GARE_CLEF).equals("" + _trajet.gareArrivee)) {
        fin = d;
        x++;
      }
    }
    final ArrayList _listeGareEtudiees = new ArrayList();

    rechercheTrajet(deb, fin, deb, _listeGareEtudiees, true, trouve);

  }

  // a appeler avec gare encours=deb au debut
  private boolean rechercheTrajet(final DjaCircle _deb, final DjaCircle _fin, final DjaCircle _enCours,
      final ArrayList _listeGareEtudiees, final boolean _trouve, boolean _bonChemin) {
    System.out.println("rechercher trajet");
    final ArrayList listeAmontBief = new ArrayList();
    final ArrayList listeAvalBief = new ArrayList();
    final ArrayList listeAmontEcluse = new ArrayList();
    final ArrayList listeAvalEcluse = new ArrayList();
    ArrayList listeGareEtudiees = _listeGareEtudiees;
    ListIterator it;
    if (!_enCours.equals(_fin) && !_bonChemin) {
      rechercheAmontAval(_enCours, listeAmontBief, listeAvalBief, listeAmontEcluse, listeAvalEcluse, listeEcluses_,
          listeBiefs_);
      if (listeAmontBief.size() > 0) {
        System.out.println("liste bief amont");
        it = listeAmontBief.listIterator();
        while (it.hasNext()) {
          final DjaDirectArrow bief = (DjaDirectArrow) it.next();
          final DjaCircle garePrec = (DjaCircle) bief.getBeginObject();
          if (_enCours == _fin) {
            _bonChemin = true;
          } else if (listeGareEtudiees.contains(garePrec)) {
            // System.out.println("on fait rien");
          } else {
            listeGareEtudiees.add(_enCours);
            if (rechercheTrajet(_deb, _fin, garePrec, listeGareEtudiees, _trouve, _bonChemin)) {
              _enCours.setColor(Color.blue);
              _bonChemin = true;
            }
          }
          if (_bonChemin) {
            _enCours.setColor(Color.blue);
            System.out.println("colorie bleu");
            _enCours.setForeground(Color.blue);
            bief.setColor(Color.blue);
            bief.setForeground(Color.blue);
            return true;
          }
          /*
           * if(listeGareEtudiees.contains(garePrec)){ System.out.println("on fait rien"); } else if(garePrec==_fin)
           * _bonChemin=true; else{ listeGareEtudiees.add(_enCours);
           * rechercheTrajet(_deb,_fin,garePrec,listeGareEtudiees,_trouve,_bonChemin); if(_bonChemin)
           * _enCours.setColor(Color.blue); }
           */
        }
      }
      if (listeAvalBief.size() > 0 && !_bonChemin) {
        System.out.println("liste bief aval");
        listeGareEtudiees = _listeGareEtudiees;

        it = listeAvalBief.listIterator();
        while (it.hasNext()) {
          final DjaDirectArrow bief = (DjaDirectArrow) it.next();
          final DjaCircle gareSuiv = (DjaCircle) bief.getEndObject();
          /*
           * if(listeGareEtudiees.contains(gareSuiv)){ System.out.println("on fait rien"); } else if(gareSuiv==_fin)
           * _bonChemin=true; else{ listeGareEtudiees.add(_enCours);
           * rechercheTrajet(_deb,_fin,gareSuiv,listeGareEtudiees,_trouve,_bonChemin); if(_bonChemin)
           * _enCours.setColor(Color.blue); }
           */
          if (_enCours == _fin) {
            _bonChemin = true;
          } else if (listeGareEtudiees.contains(gareSuiv)) {
            System.out.println("on fait rien");
          } else {
            listeGareEtudiees.add(_enCours);
            if (rechercheTrajet(_deb, _fin, gareSuiv, listeGareEtudiees, _trouve, _bonChemin)) {
              _enCours.setColor(Color.blue);
              _bonChemin = true;
            }
          }
          if (_bonChemin) {
            _enCours.setColor(Color.blue);
            _enCours.setForeground(Color.blue);
            System.out.println("colorie bleu");
            bief.setColor(Color.blue);
            bief.setForeground(Color.blue);
            return true;
          }
        }
      }
      if (listeAmontEcluse.size() > 0 && !_bonChemin) {
        System.out.println("liste ecluse amont");
        listeGareEtudiees = _listeGareEtudiees;
        it = listeAmontEcluse.listIterator();
        while (it.hasNext()) {
          final DjaDiamond ecluse = (DjaDiamond) it.next();
          final DjaDirectArrow d = (DjaDirectArrow) it.next();// entrant
          final DjaDirectArrow d2 = (DjaDirectArrow) it.next();// dsortant
          final DjaCircle garePrec = (DjaCircle) d.getBeginObject();
          if (_enCours == _fin) {
            _bonChemin = true;
          } else if (listeGareEtudiees.contains(garePrec)) {
            System.out.println("on fait rien");
          } else {
            listeGareEtudiees.add(_enCours);
            if (rechercheTrajet(_deb, _fin, garePrec, listeGareEtudiees, _trouve, _bonChemin)) {
              _enCours.setColor(Color.blue);
              _bonChemin = true;
            }
          }
          if (_bonChemin) {
            _enCours.setColor(Color.blue);
            _enCours.setForeground(Color.blue);
            ecluse.setColor(Color.blue);
            ecluse.setForeground(Color.blue);
            d.setColor(Color.blue);
            d.setForeground(Color.blue);
            d2.setColor(Color.blue);
            d2.setForeground(Color.blue);
            System.out.println("colorie bleu");
            return true;
          }
        }
      }
      if (listeAvalEcluse.size() > 0 && !_bonChemin) {
        System.out.println("liste ecluse aval");
        listeGareEtudiees = _listeGareEtudiees;
        listeGareEtudiees = _listeGareEtudiees;
        it = listeAvalEcluse.listIterator();
        while (it.hasNext()) {
          final DjaDiamond ecluse = (DjaDiamond) it.next();
          final DjaDirectArrow d = (DjaDirectArrow) it.next();// entrant
          final DjaDirectArrow d2 = (DjaDirectArrow) it.next();// dsortant
          final DjaCircle gareSuiv = (DjaCircle) d2.getEndObject();
          if (_enCours == _fin) {
            _bonChemin = true;
          } else if (listeGareEtudiees.contains(gareSuiv)) {
            System.out.println("on fait rien");
          } else {
            listeGareEtudiees.add(_enCours);
            if (rechercheTrajet(_deb, _fin, gareSuiv, listeGareEtudiees, _trouve, _bonChemin)) {
              _enCours.setColor(Color.blue);
              _bonChemin = true;
            }
          }
          if (_bonChemin) {
            _enCours.setColor(Color.blue);
            _enCours.setForeground(Color.blue);
            ecluse.setColor(Color.blue);
            ecluse.setForeground(Color.blue);
            d.setColor(Color.blue);
            d.setForeground(Color.blue);
            d2.setColor(Color.blue);
            d2.setForeground(Color.blue);
            System.out.println("colorie bleu");
            return true;
          }
        }
        /*
         * if(listeGareEtudiees.contains(gareSuiv)){ System.out.println("on fait rien"); } else if(gareSuiv==_fin)
         * _bonChemin=true; else{ listeGareEtudiees.add(_enCours);
         * rechercheTrajet(_deb,_fin,gareSuiv,listeGareEtudiees,_trouve,_bonChemin); if(_bonChemin)
         * _enCours.setColor(Color.blue); }
         */
      }
    } else {
      System.out.println("chemin = true");
      _bonChemin = true;
      _enCours.setColor(Color.blue);
      _enCours.setForeground(Color.blue);
      return true;
    }
    return false;
  }

  /**
   * Cette fonction a pour but de d�placer le mod�le elle met le plus haut �l�ment au pixel. 100,100
   */

  private void translationObjets() {
    final int minX = rechercheMinX();
    final int minY = rechercheMinY();
    ListIterator it = listeGares_.listIterator();
    while (it.hasNext()) {
      final DjaCircle c = (DjaCircle) it.next();
      c.setX(c.getX() + 100 - minX);
      c.setY(c.getY() + 100 - minY);

    }
    it = listeEcluses_.listIterator();
    while (it.hasNext()) {
      final DjaDiamond c = (DjaDiamond) it.next();
      it.next();
      it.next();
      c.setX(c.getX() + 100 - minX);
      c.setY(c.getY() + 100 - minY);

    }
  }

  private int rechercheMinY() {
    int y = 0;
    ListIterator it = listeGares_.listIterator();
    while (it.hasNext()) {
      final DjaCircle c = (DjaCircle) it.next();
      final int y1 = c.getY();
      if (y > y1) {
        y = y1;
      }
    }
    it = listeEcluses_.listIterator();
    while (it.hasNext()) {
      final DjaDiamond c = (DjaDiamond) it.next();
      it.next();
      it.next();
      final int y1 = c.getY();
      if (y > y1) {
        y = y1;
      }
    }
    // System.out.println("YYYYYYYYYY "+y);
    return y;
  }

  private int rechercheMinX() {
    int x = 0;
    ListIterator it = listeGares_.listIterator();
    while (it.hasNext()) {
      final DjaCircle c = (DjaCircle) it.next();
      final int x1 = c.getX();
      if (x > x1) {
        x = x1;
      }
    }
    it = listeEcluses_.listIterator();
    while (it.hasNext()) {
      final DjaDiamond c = (DjaDiamond) it.next();
      it.next();
      it.next();
      final int x1 = c.getX();
      if (x > x1) {
        x = x1;
      }
    }
    // System.out.println("XXXXXXXXXXXXXx"+x);
    return x;

  }

  /**
   * Cette grosse m�thode a pour but de disposer les objets. On regarde combien il ya d'�cluses et de bief en amont et
   * en vaval afin de placer les �lements. On doit donc �tudier chaque cas. On se limite � 5 �cluses et 3 bief par gare.
   */

  private void dispositionListeObjet() {
    final ArrayList listeGaresATraiter = new ArrayList(listeGares_);
    final ArrayList listeEclusesATraiter = new ArrayList(listeEcluses_);
    final ArrayList listeBiefsATraiter = new ArrayList(listeBiefs_);
    /*
     * ArrayList listeAmont = null; ArrayList listeAval=null;
     */
    final DjaCircle d = (DjaCircle) listeGaresATraiter.get(0);// debut d ela recherche de larboresecnce
    /*
     * d.setX(700); d.setY(300);
     */
    /*******************************************************************************************************************
     * d.setX(350); d.setY(150);
     ******************************************************************************************************************/
    dispositionListeObjetRecursive(listeGaresATraiter, listeEclusesATraiter, listeBiefsATraiter, d/*
                                                                                                   * (DjaCircle)
                                                                                                   * listeGaresATraiter.get(0)
                                                                                                   */);
    /*
     * if(listeGaresATraiter.size()>0){ rechercheAmontAval((DjaCircle) listeGaresATraiter.get(0), listeAmont,listeAval,
     * listeEclusesATraiter, listeBiefsATraiter); }
     */
  }

  private void dispositionListeObjetRecursive(final ArrayList _listeGaresATraiter,
      final ArrayList _listeEclusesATraiter, final ArrayList _listeBiefsATraiter, final DjaCircle _GareEnCours) {
    System.out.println("recursivit�" + _GareEnCours.getData("gareKey"));

    ListIterator it;
    int tailleListeAmont = 0;
    int tailleListeAval = 0;
    /*******************************************************************************************************************
     * ArrayList listeAmontBief = null; ArrayList listeAvalBief=null; ArrayList listeAmontEcluse = null; ArrayList
     * listeAvalEcluse=null; ArrayList listeAmontBief2 = null; ArrayList listeAvalBief2=null; ArrayList
     * listeAmontEcluse2 = null; ArrayList listeAvalEcluse2=null;
     ******************************************************************************************************************/
    final ArrayList listeAmontBief = new ArrayList();
    final ArrayList listeAvalBief = new ArrayList();
    final ArrayList listeAmontEcluse = new ArrayList();
    final ArrayList listeAvalEcluse = new ArrayList();
    ArrayList listeAmontBief2 = new ArrayList();
    ArrayList listeAvalBief2 = new ArrayList();
    ArrayList listeAmontEcluse2 = new ArrayList();
    ArrayList listeAvalEcluse2 = new ArrayList();

    if (_GareEnCours != null && _listeGaresATraiter.size() != 0) {
      // if(_listeGaresATraiter.size()>0){
      // rechercheAmontAval(_GareEnCours, listeAmontBief,listeAvalBief,listeAmontEcluse,listeAvalEcluse,
      // _listeEclusesATraiter, _listeBiefsATraiter);
      // System.out.println("premier recherche");
      rechercheAmontAval(_GareEnCours, listeAmontBief, listeAvalBief, listeAmontEcluse, listeAvalEcluse, listeEcluses_,
          listeBiefs_);
      // System.out.println("fin premier recherche");

      // traitement de la liste en amont

      if (listeAmontBief != null) {
        tailleListeAmont = listeAmontBief.size();
      }
      if (listeAmontEcluse != null) {
        tailleListeAmont += listeAmontEcluse.size() / 3;
      }
      if (listeAvalBief != null) {
        tailleListeAval = listeAvalBief.size();
      }
      if (listeAvalEcluse != null) {
        tailleListeAval += listeAvalEcluse.size() / 3;
      }
      if (tailleListeAmont == 0 && tailleListeAval == 0) {
        System.out.println("taille de la liste en amont = 0 --------------> on fait rien");
        if (_listeGaresATraiter.contains(_GareEnCours)) {
          // //grid_.add(_GareEnCours);

          vect_.addElement(_GareEnCours);
          _listeGaresATraiter.remove(_GareEnCours);
          System.out.println("ajout gare fin: " + _GareEnCours.getData("gareKey"));
        }
      }

      else if (tailleListeAmont == 1) {
        System.out.println("taille de la liste en amont =1");
        // on place l'element en arriere au point 6
        if (listeAmontBief.size() == 1) {
          // aEntrer les positions
          it = listeAmontBief.listIterator();
          final DjaDirectArrow bief = (DjaDirectArrow) it.next();
          bief.setEndPosition(6);
          final DjaCircle garePrec = (DjaCircle) bief.getBeginObject();
          listeAmontBief2 = new ArrayList();
          listeAvalBief2 = new ArrayList();
          listeAmontEcluse2 = new ArrayList();
          listeAvalEcluse2 = new ArrayList();
          rechercheAmontAval(garePrec, listeAmontBief2, listeAvalBief2, listeAmontEcluse2, listeAvalEcluse2,
              listeEcluses_, listeBiefs_);

          if (_listeGaresATraiter.contains(garePrec)) {
            garePrec.setX(_GareEnCours.getX() - 200/** * */
            );
            if ((listeAvalBief2.size() + listeAvalEcluse2.size() / 3) > 1) {
              garePrec.setY(_GareEnCours.getY() + 80);
              bief.setBeginPosition(1);

            }

            else {
              garePrec.setY(_GareEnCours.getY());
              bief.setBeginPosition(2);
            }
          }
          if (_listeGaresATraiter.contains(_GareEnCours)) {
            System.out.println("ajout gare amont " + _GareEnCours.getData("gareKey"));
            // //grid_.add(_GareEnCours);
            vect_.addElement(_GareEnCours);
            _listeGaresATraiter.remove(_GareEnCours);
          }
          if (_listeBiefsATraiter.contains(bief)) {
            System.out.println("ajout bief amont " + bief.getData("biefKey"));
            bief.setEndPosition(6);
            // //grid_.add(bief);
            vect_.addElement(bief);
            _listeBiefsATraiter.remove(bief);
          }

          if (_listeGaresATraiter.contains(garePrec)) {
            dispositionListeObjetRecursive(_listeGaresATraiter, _listeEclusesATraiter, _listeBiefsATraiter, garePrec);
          }
        } else {// /listeAmontEcluse==1
          it = listeAmontEcluse.listIterator();
          final DjaDiamond ecluse = (DjaDiamond) it.next();
          final DjaDirectArrow d = (DjaDirectArrow) it.next();// entrant
          final DjaDirectArrow d2 = (DjaDirectArrow) it.next();// dsortant

          d2.setEndPosition(6);// *********************************
          // ecluse.getEndConnections()[0].setEndPosition(6);

          final DjaCircle garePrec = (DjaCircle) d.getBeginObject();
          d.setBeginPosition(2);// *********************************
          // DjaCircle garePrec=(DjaCircle)ecluse.getBeginConnections()[0].getBeginObject();
          if (_listeGaresATraiter.contains(garePrec)) {
            garePrec.setX(_GareEnCours.getX() - 200);
            garePrec.setY(_GareEnCours.getY());
          }
          if (_listeGaresATraiter.contains(_GareEnCours)) {
            System.out.println("ajout gare amont " + _GareEnCours.getData("gareKey"));
            // //grid_.add(_GareEnCours);
            vect_.addElement(_GareEnCours);

            _listeGaresATraiter.remove(_GareEnCours);
          }
          if (_listeEclusesATraiter.contains(ecluse)) {
            ecluse.setX(_GareEnCours.getX() - 120);
            ecluse.setY(_GareEnCours.getY());

            System.out.println("ajout ecluse amont : " + ecluse.getData("ecluseKey"));
            // //grid_.add(ecluse);
            vect_.addElement(ecluse);
            // //grid_.add(d2);
            vect_.addElement(d2);
            // //grid_.add(d);//**
            vect_.addElement(d);
            _listeEclusesATraiter.remove(ecluse);
          }

          if (_listeGaresATraiter.contains(garePrec)) {
            dispositionListeObjetRecursive(_listeGaresATraiter, _listeEclusesATraiter, _listeBiefsATraiter, garePrec);
          }
        }

      } else if (tailleListeAmont == 2) {

        System.out.println("taille de la liste en amont =2");
        // on place l'element en arriere au point 6
        if (listeAmontBief.size() == 2) {
          it = listeAmontBief.listIterator();
          final DjaDirectArrow bief = (DjaDirectArrow) it.next();
          bief.setEndPosition(7);
          // bief.setBeginPosition(3);
          final DjaCircle garePrec = (DjaCircle) bief.getBeginObject();
          listeAmontBief2 = new ArrayList();
          listeAvalBief2 = new ArrayList();
          listeAmontEcluse2 = new ArrayList();
          listeAvalEcluse2 = new ArrayList();
          rechercheAmontAval(garePrec, listeAmontBief2, listeAvalBief2, listeAmontEcluse2, listeAvalEcluse2,
              listeEcluses_, listeBiefs_);

          final DjaDirectArrow bief2 = (DjaDirectArrow) it.next();
          bief2.setEndPosition(5);
          // bief2.setBeginPosition(1);
          final DjaCircle garePrec2 = (DjaCircle) bief2.getBeginObject();
          listeAmontBief2 = new ArrayList();
          listeAvalBief2 = new ArrayList();
          listeAmontEcluse2 = new ArrayList();
          listeAvalEcluse2 = new ArrayList();
          rechercheAmontAval(garePrec2, listeAmontBief2, listeAvalBief2, listeAmontEcluse2, listeAvalEcluse2,
              listeEcluses_, listeBiefs_);

          if (_listeGaresATraiter.contains(_GareEnCours)) {
            vect_.addElement(_GareEnCours);
            System.out.println("ajout gare amont " + _GareEnCours.getData("gareKey"));
            _listeGaresATraiter.remove(_GareEnCours);
          }

          if (_listeGaresATraiter.contains(garePrec)) {
            // System.out.println("taurait pas du passer la gros con" + garePrec.getX() +"-"+garePrec.getY());
            garePrec.setX(_GareEnCours.getX() - 200/** * */
            );
            garePrec.setY(_GareEnCours.getY() - 80);
            /***********************************************************************************************************
             * if((listeAvalBief2.size()+listeAvalEcluse2.size())>1){ // garePrec.setY(_GareEnCours.getY()+80);
             * bief.setBeginPosition(1); } else{ //garePrec.setY(_GareEnCours.getY()); bief.setBeginPosition(2); }
             **********************************************************************************************************/

            // //grid_.add(_GareEnCours);
          }

          if (_listeGaresATraiter.contains(garePrec2)) {
            garePrec2.setX(_GareEnCours.getX() - 200/** * */
            );
            garePrec2.setY(_GareEnCours.getY() + 80);
            /*
             * if((listeAvalBief2.size()+listeAvalEcluse2.size())>1) garePrec.setY(_GareEnCours.getY()+80); else
             * garePrec.setY(_GareEnCours.getY());
             */

            if ((listeAvalBief2.size() + listeAvalEcluse2.size() / 3) > 1) {
              // garePrec2.setY(_GareEnCours.getY()+80);
              bief2.setBeginPosition(1);
            }

            else {
              // garePrec2.setY(_GareEnCours.getY());
              bief2.setBeginPosition(2);
            }
          }

          if (_listeBiefsATraiter.contains(bief)) {
            // //grid_.add(bief);
            vect_.addElement(bief);
            System.out.println("ajout bief amont " + bief.getData("biefKey"));

            _listeBiefsATraiter.remove(bief);
          }

          if (_listeBiefsATraiter.contains(bief2)) {
            // //grid_.add(bief2);
            vect_.addElement(bief2);
            System.out.println("ajout bief amont " + bief2.getData("biefKey"));
            _listeBiefsATraiter.remove(bief2);
          }

          if (_listeGaresATraiter.contains(garePrec)) {// System.out.println("ca passe du con");
            dispositionListeObjetRecursive(_listeGaresATraiter, _listeEclusesATraiter, _listeBiefsATraiter, garePrec);
          }
          if (_listeGaresATraiter.contains(garePrec2)) {// System.out.println("ca passe c normal");
            dispositionListeObjetRecursive(_listeGaresATraiter, _listeEclusesATraiter, _listeBiefsATraiter, garePrec2);
          }
        }
        if (listeAmontEcluse.size() / 3 == 2) {
          it = listeAmontEcluse.listIterator();
          final DjaDiamond ecluse = (DjaDiamond) it.next();
          final DjaDirectArrow d = (DjaDirectArrow) it.next();// entrant
          final DjaDirectArrow d2 = (DjaDirectArrow) it.next();// dsortant

          d2.setEndPosition(7);
          // ecluse.getEndConnections()[0].setEndPosition(6);

          final DjaCircle garePrec = (DjaCircle) d.getBeginObject();

          // DjaCircle garePrec=(DjaCircle)ecluse.getBeginConnections()[0].getBeginObject();

          final DjaDiamond ecluse2 = (DjaDiamond) it.next();
          final DjaDirectArrow dd = (DjaDirectArrow) it.next();// entrant
          final DjaDirectArrow dd2 = (DjaDirectArrow) it.next();// dsortant

          // dd2.setEndPosition(7);
          // ecluse.getEndConnections()[0].setEndPosition(6);

          if (_listeGaresATraiter.contains(_GareEnCours)) {
            // //grid_.add(_GareEnCours);
            vect_.addElement(_GareEnCours);
            _listeGaresATraiter.remove(_GareEnCours);
            System.out.println("ajout gare amont " + _GareEnCours.getData("gareKey"));

          }

          if (_listeGaresATraiter.contains(garePrec)) {
            listeAmontBief2 = new ArrayList();
            listeAvalBief2 = new ArrayList();
            listeAmontEcluse2 = new ArrayList();
            listeAvalEcluse2 = new ArrayList();
            rechercheAmontAval(garePrec, listeAmontBief2, listeAvalBief2, listeAmontEcluse2, listeAvalEcluse2,
                listeEcluses_, listeBiefs_);

            if ((listeAvalBief2.size() + listeAvalEcluse2.size() / 3) > 1) {
              // garePrec.setY(_GareEnCours.getY()+80);
              d.setBeginPosition(1);
            }

            else {
              // garePrec.setY(_GareEnCours.getY());
              d.setBeginPosition(2);
            }
            garePrec.setX(_GareEnCours.getX() - 200);
            garePrec.setY(_GareEnCours.getY());

          }

          if (_listeEclusesATraiter.contains(ecluse)) {

            ecluse.setX(_GareEnCours.getX() - 120);
            ecluse.setY(_GareEnCours.getY() - 40);// 80-->40
            garePrec.setX(_GareEnCours.getX() - 200);// --
            garePrec.setY(_GareEnCours.getY());// --

            // //grid_.add(ecluse);
            vect_.addElement(ecluse);
            System.out.println("ajout ecluse amont : " + ecluse.getData("ecluseKey"));
            // //grid_.add(d2);
            vect_.addElement(d2);
            // //grid_.add(d);//**
            vect_.addElement(d);
            _listeEclusesATraiter.remove(ecluse);
          }
          if (_listeEclusesATraiter.contains(ecluse2)) {
            dd.setBeginPosition(3);
            dd2.setEndPosition(5);

            ecluse2.setX(_GareEnCours.getX() - 120);
            ecluse2.setY(_GareEnCours.getY() + 40);// 80-->40
            garePrec.setX(_GareEnCours.getX() - 200);// --
            garePrec.setY(_GareEnCours.getY());// --
            System.out.println("ajout ecluse amont " + ecluse2.getData("ecluseKey"));
            // //grid_.add(ecluse2);
            vect_.addElement(ecluse2);
            // //grid_.add(dd2);
            vect_.addElement(dd2);
            // //grid_.add(dd);//**
            vect_.addElement(dd);
            _listeEclusesATraiter.remove(ecluse2);
          }

          if (_listeGaresATraiter.contains(garePrec)) {
            dispositionListeObjetRecursive(_listeGaresATraiter, _listeEclusesATraiter, _listeBiefsATraiter, garePrec);
          }

        }
        if (listeAmontBief.size() == 1 && listeAmontEcluse.size() / 3 == 1) {// cas exceptionnel je pense ; non fait
          // encore
        }

      } else if (tailleListeAmont == 3) {// logiquement 3 biefs ou 3 �cluses

        System.out.println("taille de la liste en amont =3");
        // on place l'element en arriere au point 6
        if (listeAmontBief.size() == 3) {
          it = listeAmontBief.listIterator();
          final DjaDirectArrow bief = (DjaDirectArrow) it.next();
          bief.setEndPosition(7);
          final DjaCircle garePrec = (DjaCircle) bief.getBeginObject();
          listeAmontBief2 = new ArrayList();
          listeAvalBief2 = new ArrayList();
          listeAmontEcluse2 = new ArrayList();
          listeAvalEcluse2 = new ArrayList();
          rechercheAmontAval(garePrec, listeAmontBief2, listeAvalBief2, listeAmontEcluse2, listeAvalEcluse2,
              listeEcluses_, listeBiefs_);
          if (_listeGaresATraiter.contains(garePrec)) {
            garePrec.setX(_GareEnCours.getX() - 200/** * */
            );
            garePrec.setY(_GareEnCours.getY() - 80);// sert a rien je sais pu pkoi

            if ((listeAvalBief2.size() + listeAvalEcluse2.size() / 3) > 1) {
              // garePrec.setY(_GareEnCours.getY()-80);
              bief.setBeginPosition(1);
            }

            else {
              // garePrec.setY(_GareEnCours.getY());
              bief.setBeginPosition(2);
            }
          }
          final DjaDirectArrow bief2 = (DjaDirectArrow) it.next();
          bief2.setEndPosition(6);
          final DjaCircle garePrec2 = (DjaCircle) bief2.getBeginObject();
          listeAmontBief2 = new ArrayList();
          listeAvalBief2 = new ArrayList();
          listeAmontEcluse2 = new ArrayList();
          listeAvalEcluse2 = new ArrayList();
          rechercheAmontAval(garePrec2, listeAmontBief2, listeAvalBief2, listeAmontEcluse2, listeAvalEcluse2,
              listeEcluses_, listeBiefs_);
          if (_listeGaresATraiter.contains(garePrec2)) {
            garePrec2.setX(_GareEnCours.getX() - 200/** * */
            );
            garePrec2.setY(_GareEnCours.getY());
            /*
             * if((listeAvalBief2.size()+listeAvalEcluse2.size())>1) garePrec.setY(_GareEnCours.getY()+80); else
             * garePrec.setY(_GareEnCours.getY());
             */

            if ((listeAvalBief2.size() + listeAvalEcluse2.size() / 3) > 1) {
              // garePrec2.setY(_GareEnCours.getY()+80);
              bief2.setBeginPosition(1);
            }

            else {
              // garePrec2.setY(_GareEnCours.getY());
              bief2.setBeginPosition(2);
            }
          }
          final DjaDirectArrow bief3 = (DjaDirectArrow) it.next();
          bief3.setEndPosition(5);
          final DjaCircle garePrec3 = (DjaCircle) bief3.getBeginObject();
          listeAmontBief2 = new ArrayList();
          listeAvalBief2 = new ArrayList();
          listeAmontEcluse2 = new ArrayList();
          listeAvalEcluse2 = new ArrayList();
          rechercheAmontAval(garePrec3, listeAmontBief2, listeAvalBief2, listeAmontEcluse2, listeAvalEcluse2,
              listeEcluses_, listeBiefs_);
          if (_listeGaresATraiter.contains(garePrec3)) {
            garePrec3.setX(_GareEnCours.getX() - 200/** * */
            );
            garePrec3.setY(_GareEnCours.getY() + 80);
            /*
             * if((listeAvalBief2.size()+listeAvalEcluse2.size())>1) garePrec.setY(_GareEnCours.getY()+80); else
             * garePrec.setY(_GareEnCours.getY());
             */

            if ((listeAvalBief2.size() + listeAvalEcluse2.size() / 3) > 1) {
              // garePrec2.setY(_GareEnCours.getY()+80);
              bief2.setBeginPosition(1);
            }

            else {
              // garePrec2.setY(_GareEnCours.getY());
              bief2.setBeginPosition(2);
            }
          }

          if (_listeGaresATraiter.contains(_GareEnCours)) {
            System.out.println("ajout gare amont " + _GareEnCours.getData("gareKey"));
            // //grid_.add(_GareEnCours);
            vect_.addElement(_GareEnCours);
            _listeGaresATraiter.remove(_GareEnCours);
          }
          if (_listeBiefsATraiter.contains(bief)) {
            System.out.println("ajout bief amont " + bief.getData("biefKey"));
            // //grid_.add(bief);
            vect_.addElement(bief);
            _listeBiefsATraiter.remove(bief);
          }
          if (_listeBiefsATraiter.contains(bief2)) {
            System.out.println("ajout bief amont " + bief2.getData("biefKey"));
            // //grid_.add(bief2);
            vect_.addElement(bief2);
            _listeBiefsATraiter.remove(bief2);
          }
          if (_listeBiefsATraiter.contains(bief3)) {
            System.out.println("ajout bief amont " + bief3.getData("biefKey"));
            _listeBiefsATraiter.remove(bief3);
            // //grid_.add(bief3);
            vect_.addElement(bief3);
          }

          if (_listeGaresATraiter.contains(garePrec)) {
            dispositionListeObjetRecursive(_listeGaresATraiter, _listeEclusesATraiter, _listeBiefsATraiter, garePrec);
          }
          if (_listeGaresATraiter.contains(garePrec2)) {
            dispositionListeObjetRecursive(_listeGaresATraiter, _listeEclusesATraiter, _listeBiefsATraiter, garePrec2);
          }
          if (_listeGaresATraiter.contains(garePrec3)) {
            dispositionListeObjetRecursive(_listeGaresATraiter, _listeEclusesATraiter, _listeBiefsATraiter, garePrec3);
          }
        }
        if (listeAmontEcluse.size() / 3 == 3) {

          it = listeAmontEcluse.listIterator();
          final DjaDiamond ecluse = (DjaDiamond) it.next();
          final DjaDirectArrow d = (DjaDirectArrow) it.next();// entrant
          final DjaDirectArrow d2 = (DjaDirectArrow) it.next();// dsortant

          d2.setEndPosition(7);
          // ecluse.getEndConnections()[0].setEndPosition(6);

          final DjaCircle garePrec = (DjaCircle) d.getBeginObject();
          listeAmontBief2 = new ArrayList();
          listeAvalBief2 = new ArrayList();
          listeAmontEcluse2 = new ArrayList();
          listeAvalEcluse2 = new ArrayList();
          rechercheAmontAval(garePrec, listeAmontBief2, listeAvalBief2, listeAmontEcluse2, listeAvalEcluse2,
              listeEcluses_, listeBiefs_);
          if (_listeGaresATraiter.contains(garePrec)) {

            if ((listeAvalBief2.size() + listeAvalEcluse2.size() / 3) > 1) {
              // garePrec.setY(_GareEnCours.getY()+80);
              d.setBeginPosition(1);
            }

            else {
              // garePrec.setY(_GareEnCours.getY());
              d.setBeginPosition(2);
            }

            // DjaCircle garePrec=(DjaCircle)ecluse.getBeginConnections()[0].getBeginObject();
            if (_listeEclusesATraiter.contains(ecluse)) {
              ecluse.setX(_GareEnCours.getX() - 120);
              ecluse.setY(_GareEnCours.getY() - 40);// 80-->40

              garePrec.setX(_GareEnCours.getX() - 200);
              garePrec.setY(_GareEnCours.getY());
            }
          }
          final DjaDiamond ecluse2 = (DjaDiamond) it.next();
          final DjaDirectArrow dd = (DjaDirectArrow) it.next();// entrant
          final DjaDirectArrow dd2 = (DjaDirectArrow) it.next();// dsortant

          // ecluse.getEndConnections()[0].setEndPosition(6);

          final DjaDiamond ecluse3 = (DjaDiamond) it.next();
          final DjaDirectArrow ddd = (DjaDirectArrow) it.next();// entrant
          final DjaDirectArrow ddd2 = (DjaDirectArrow) it.next();// dsortant

          // ecluse.getEndConnections()[0].setEndPosition(6);

          if (_listeGaresATraiter.contains(_GareEnCours)) {
            System.out.println("ajout gare amont " + _GareEnCours.getData("gareKey"));
            // //grid_.add(_GareEnCours);
            vect_.addElement(_GareEnCours);
            _listeGaresATraiter.remove(_GareEnCours);
          }
          if (_listeEclusesATraiter.contains(ecluse)) {
            System.out.println("ajout ecluse amont " + ecluse.getData("ecluseKey"));
            // //grid_.add(ecluse);
            vect_.addElement(ecluse);
            // //grid_.add(d2);
            vect_.addElement(d2);
            // //grid_.add(d);//**
            vect_.addElement(d);
            _listeEclusesATraiter.remove(ecluse);
          }
          if (_listeEclusesATraiter.contains(ecluse2)) {
            dd.setBeginPosition(2);
            dd2.setEndPosition(6);

            ecluse2.setX(_GareEnCours.getX() - 120);
            ecluse2.setY(_GareEnCours.getY());
            garePrec.setX(_GareEnCours.getX() - 200);
            garePrec.setY(_GareEnCours.getY());

            System.out.println("ajout ecluse amont " + ecluse2.getData("ecluseKey"));
            // //grid_.add(ecluse2);
            vect_.addElement(ecluse2);
            // //grid_.add(dd2);
            vect_.addElement(dd2);
            // //grid_.add(dd);//**
            vect_.addElement(dd);
            _listeEclusesATraiter.remove(ecluse2);
          }
          if (_listeEclusesATraiter.contains(ecluse3)) {
            ddd.setBeginPosition(3);
            ddd2.setEndPosition(5);
            garePrec.setX(_GareEnCours.getX() - 200);
            garePrec.setY(_GareEnCours.getY());

            ecluse3.setX(_GareEnCours.getX() - 120);
            ecluse3.setY(_GareEnCours.getY() + 40);// 80-->40
            System.out.println("ajout ecluse amont " + ecluse3.getData("ecluseKey"));
            // //grid_.add(ecluse3);
            vect_.addElement(ecluse3);
            // //grid_.add(ddd2);
            vect_.addElement(ddd2);
            // //grid_.add(ddd);//**
            vect_.addElement(ddd);
            _listeEclusesATraiter.remove(ecluse3);
          }

          if (_listeGaresATraiter.contains(garePrec)) {
            dispositionListeObjetRecursive(_listeGaresATraiter, _listeEclusesATraiter, _listeBiefsATraiter, garePrec);
          }

        }
        if (listeAmontBief.size() == 1 && listeAmontEcluse.size() / 3 == 2) {// cas exceptionnel je pense ; non fait
          // encore

        }
        if (listeAmontBief.size() == 2 && listeAmontEcluse.size() / 3 == 1) {// cas exceptionnel je pense ; non fait
          // encore
        }

      } else if (tailleListeAmont == 4) {// on va regarder que les �cluses

        if (listeAmontEcluse.size() / 3 == 4) {

          it = listeAmontEcluse.listIterator();
          final DjaDiamond ecluse = (DjaDiamond) it.next();
          final DjaDirectArrow d = (DjaDirectArrow) it.next();// entrant
          final DjaDirectArrow d2 = (DjaDirectArrow) it.next();// dsortant

          d2.setEndPosition(7);
          // ecluse.getEndConnections()[0].setEndPosition(6);

          final DjaCircle garePrec = (DjaCircle) d.getBeginObject();
          listeAmontBief2 = new ArrayList();
          listeAvalBief2 = new ArrayList();
          listeAmontEcluse2 = new ArrayList();
          listeAvalEcluse2 = new ArrayList();
          rechercheAmontAval(garePrec, listeAmontBief2, listeAvalBief2, listeAmontEcluse2, listeAvalEcluse2,
              listeEcluses_, listeBiefs_);
          if (_listeGaresATraiter.contains(garePrec)) {

            if ((listeAvalBief2.size() + listeAvalEcluse2.size() / 3) > 1) {
              // garePrec.setY(_GareEnCours.getY()+80);
              d.setBeginPosition(1);
            }

            else {
              // garePrec.setY(_GareEnCours.getY());
              d.setBeginPosition(2);
            }

            // DjaCircle garePrec=(DjaCircle)ecluse.getBeginConnections()[0].getBeginObject();
            if (_listeEclusesATraiter.contains(ecluse)) {
              ecluse.setX(_GareEnCours.getX() - 120);
              ecluse.setY(_GareEnCours.getY() - 40);// 80-->40

              garePrec.setX(_GareEnCours.getX() - 200);
              garePrec.setY(_GareEnCours.getY());
            }
          }
          final DjaDiamond ecluse2 = (DjaDiamond) it.next();
          final DjaDirectArrow dd = (DjaDirectArrow) it.next();// entrant
          final DjaDirectArrow dd2 = (DjaDirectArrow) it.next();// dsortant

          // ecluse.getEndConnections()[0].setEndPosition(6);

          final DjaDiamond ecluse3 = (DjaDiamond) it.next();
          final DjaDirectArrow ddd = (DjaDirectArrow) it.next();// entrant
          final DjaDirectArrow ddd2 = (DjaDirectArrow) it.next();// dsortant

          final DjaDiamond ecluse4 = (DjaDiamond) it.next();
          final DjaDirectArrow dddd = (DjaDirectArrow) it.next();// entrant
          final DjaDirectArrow dddd2 = (DjaDirectArrow) it.next();// dsortant

          // ecluse.getEndConnections()[0].setEndPosition(6);

          if (_listeGaresATraiter.contains(_GareEnCours)) {
            System.out.println("ajout gare amont " + _GareEnCours.getData("gareKey"));
            // //grid_.add(_GareEnCours);
            vect_.addElement(_GareEnCours);
            _listeGaresATraiter.remove(_GareEnCours);
          }
          if (_listeEclusesATraiter.contains(ecluse)) {
            System.out.println("ajout ecluse amont " + ecluse.getData("ecluseKey"));
            // //grid_.add(ecluse);
            vect_.addElement(ecluse);
            // //grid_.add(d2);
            vect_.addElement(d2);
            // //grid_.add(d);//**
            vect_.addElement(d);
            _listeEclusesATraiter.remove(ecluse);
          }
          if (_listeEclusesATraiter.contains(ecluse2)) {
            dd.setBeginPosition(2);
            dd2.setEndPosition(6);

            ecluse2.setX(_GareEnCours.getX() - 120);
            ecluse2.setY(_GareEnCours.getY());
            garePrec.setX(_GareEnCours.getX() - 200);
            garePrec.setY(_GareEnCours.getY());

            System.out.println("ajout ecluse amont " + ecluse2.getData("ecluseKey"));
            // //grid_.add(ecluse2);
            vect_.addElement(ecluse2);
            // //grid_.add(dd2);
            vect_.addElement(dd2);
            // //grid_.add(dd);//**
            vect_.addElement(dd);
            _listeEclusesATraiter.remove(ecluse2);
          }
          if (_listeEclusesATraiter.contains(ecluse3)) {
            ddd.setBeginPosition(3);
            ddd2.setEndPosition(5);
            garePrec.setX(_GareEnCours.getX() - 200);
            garePrec.setY(_GareEnCours.getY());

            ecluse3.setX(_GareEnCours.getX() - 120);
            ecluse3.setY(_GareEnCours.getY() + 40);// 80-->40
            System.out.println("ajout ecluse amont " + ecluse3.getData("ecluseKey"));
            // //grid_.add(ecluse3);
            vect_.addElement(ecluse3);
            // //grid_.add(ddd2);
            vect_.addElement(ddd2);
            // //grid_.add(ddd);//**
            vect_.addElement(ddd);
            _listeEclusesATraiter.remove(ecluse3);
          }
          if (_listeEclusesATraiter.contains(ecluse4)) {
            dddd.setBeginPosition(0);
            dddd2.setEndPosition(0);
            garePrec.setX(_GareEnCours.getX() - 200);
            garePrec.setY(_GareEnCours.getY());

            ecluse4.setX(_GareEnCours.getX() - 120);
            ecluse4.setY(_GareEnCours.getY() - 80);// 80-->40
            System.out.println("ajout ecluse amont " + ecluse3.getData("ecluseKey"));
            // //grid_.add(ecluse3);
            vect_.addElement(ecluse4);
            // //grid_.add(ddd2);
            vect_.addElement(dddd2);
            // //grid_.add(ddd);//**
            vect_.addElement(dddd);
            _listeEclusesATraiter.remove(ecluse4);
          }

          if (_listeGaresATraiter.contains(garePrec)) {
            dispositionListeObjetRecursive(_listeGaresATraiter, _listeEclusesATraiter, _listeBiefsATraiter, garePrec);
          }

        }

      } else if (tailleListeAmont == 5) {// on va regarder que les �cluses

        if (listeAmontEcluse.size() / 3 == 5) {

          it = listeAmontEcluse.listIterator();
          final DjaDiamond ecluse = (DjaDiamond) it.next();
          final DjaDirectArrow d = (DjaDirectArrow) it.next();// entrant
          final DjaDirectArrow d2 = (DjaDirectArrow) it.next();// dsortant

          d2.setEndPosition(7);
          // ecluse.getEndConnections()[0].setEndPosition(6);

          final DjaCircle garePrec = (DjaCircle) d.getBeginObject();
          listeAmontBief2 = new ArrayList();
          listeAvalBief2 = new ArrayList();
          listeAmontEcluse2 = new ArrayList();
          listeAvalEcluse2 = new ArrayList();
          rechercheAmontAval(garePrec, listeAmontBief2, listeAvalBief2, listeAmontEcluse2, listeAvalEcluse2,
              listeEcluses_, listeBiefs_);
          if (_listeGaresATraiter.contains(garePrec)) {

            if ((listeAvalBief2.size() + listeAvalEcluse2.size() / 3) > 1) {
              // garePrec.setY(_GareEnCours.getY()+80);
              d.setBeginPosition(1);
            }

            else {
              // garePrec.setY(_GareEnCours.getY());
              d.setBeginPosition(2);
            }

            // DjaCircle garePrec=(DjaCircle)ecluse.getBeginConnections()[0].getBeginObject();
            if (_listeEclusesATraiter.contains(ecluse)) {
              ecluse.setX(_GareEnCours.getX() - 120);
              ecluse.setY(_GareEnCours.getY() - 40);// 80-->40

              garePrec.setX(_GareEnCours.getX() - 200);
              garePrec.setY(_GareEnCours.getY());
            }
          }
          final DjaDiamond ecluse2 = (DjaDiamond) it.next();
          final DjaDirectArrow dd = (DjaDirectArrow) it.next();// entrant
          final DjaDirectArrow dd2 = (DjaDirectArrow) it.next();// dsortant

          // ecluse.getEndConnections()[0].setEndPosition(6);

          final DjaDiamond ecluse3 = (DjaDiamond) it.next();
          final DjaDirectArrow ddd = (DjaDirectArrow) it.next();// entrant
          final DjaDirectArrow ddd2 = (DjaDirectArrow) it.next();// dsortant

          final DjaDiamond ecluse4 = (DjaDiamond) it.next();
          final DjaDirectArrow dddd = (DjaDirectArrow) it.next();// entrant
          final DjaDirectArrow dddd2 = (DjaDirectArrow) it.next();// dsortant

          final DjaDiamond ecluse5 = (DjaDiamond) it.next();
          final DjaDirectArrow ddddd = (DjaDirectArrow) it.next();// entrant
          final DjaDirectArrow ddddd2 = (DjaDirectArrow) it.next();// dsortant

          // ecluse.getEndConnections()[0].setEndPosition(6);

          if (_listeGaresATraiter.contains(_GareEnCours)) {
            System.out.println("ajout gare amont " + _GareEnCours.getData("gareKey"));
            // //grid_.add(_GareEnCours);
            vect_.addElement(_GareEnCours);
            _listeGaresATraiter.remove(_GareEnCours);
          }
          if (_listeEclusesATraiter.contains(ecluse)) {
            System.out.println("ajout ecluse amont " + ecluse.getData("ecluseKey"));
            // //grid_.add(ecluse);
            vect_.addElement(ecluse);
            // //grid_.add(d2);
            vect_.addElement(d2);
            // //grid_.add(d);//**
            vect_.addElement(d);
            _listeEclusesATraiter.remove(ecluse);
          }
          if (_listeEclusesATraiter.contains(ecluse2)) {
            dd.setBeginPosition(2);
            dd2.setEndPosition(6);

            ecluse2.setX(_GareEnCours.getX() - 120);
            ecluse2.setY(_GareEnCours.getY());
            garePrec.setX(_GareEnCours.getX() - 200);
            garePrec.setY(_GareEnCours.getY());

            System.out.println("ajout ecluse amont " + ecluse2.getData("ecluseKey"));
            // //grid_.add(ecluse2);
            vect_.addElement(ecluse2);
            // //grid_.add(dd2);
            vect_.addElement(dd2);
            // //grid_.add(dd);//**
            vect_.addElement(dd);
            _listeEclusesATraiter.remove(ecluse2);
          }
          if (_listeEclusesATraiter.contains(ecluse3)) {
            ddd.setBeginPosition(3);
            ddd2.setEndPosition(5);
            garePrec.setX(_GareEnCours.getX() - 200);
            garePrec.setY(_GareEnCours.getY());

            ecluse3.setX(_GareEnCours.getX() - 120);
            ecluse3.setY(_GareEnCours.getY() + 40);// 80-->40
            System.out.println("ajout ecluse amont " + ecluse3.getData("ecluseKey"));
            // //grid_.add(ecluse3);
            vect_.addElement(ecluse3);
            // //grid_.add(ddd2);
            vect_.addElement(ddd2);
            // //grid_.add(ddd);//**
            vect_.addElement(ddd);
            _listeEclusesATraiter.remove(ecluse3);
          }
          if (_listeEclusesATraiter.contains(ecluse4)) {
            dddd.setBeginPosition(0);
            dddd2.setEndPosition(0);
            garePrec.setX(_GareEnCours.getX() - 200);
            garePrec.setY(_GareEnCours.getY());

            ecluse4.setX(_GareEnCours.getX() - 120);
            ecluse4.setY(_GareEnCours.getY() - 80);// 80-->40
            System.out.println("ajout ecluse amont " + ecluse3.getData("ecluseKey"));
            // //grid_.add(ecluse3);
            vect_.addElement(ecluse4);
            // //grid_.add(ddd2);
            vect_.addElement(dddd2);
            // //grid_.add(ddd);//**
            vect_.addElement(dddd);
            _listeEclusesATraiter.remove(ecluse4);
          }
          if (_listeEclusesATraiter.contains(ecluse5)) {
            ddddd.setBeginPosition(4);
            ddddd2.setEndPosition(4);
            garePrec.setX(_GareEnCours.getX() - 200);
            garePrec.setY(_GareEnCours.getY());

            ecluse5.setX(_GareEnCours.getX() - 120);
            ecluse5.setY(_GareEnCours.getY() + 80);// 80-->40
            System.out.println("ajout ecluse amont " + ecluse3.getData("ecluseKey"));
            // //grid_.add(ecluse3);
            vect_.addElement(ecluse5);
            // //grid_.add(ddd2);
            vect_.addElement(ddddd2);
            // //grid_.add(ddd);//**
            vect_.addElement(ddddd);
            _listeEclusesATraiter.remove(ecluse5);
          }

          if (_listeGaresATraiter.contains(garePrec)) {
            dispositionListeObjetRecursive(_listeGaresATraiter, _listeEclusesATraiter, _listeBiefsATraiter, garePrec);
          }

        }

      } else {
        System.out.println("Ca fait beaucoup tout de m�me >5");// il faudra peut etre le gfaire pour montrer les
        // erreurs de lutilisateur
      }

      if (tailleListeAval == 1) {
        System.out.println("taille de la liste en aval =1");
        // on place l'element en arriere au point 6
        if (listeAvalBief.size() == 1) {
          // aEntrer les positions
          it = listeAvalBief.listIterator();
          final DjaDirectArrow bief = (DjaDirectArrow) it.next();
          final DjaCircle gareSuiv = (DjaCircle) bief.getEndObject();
          listeAmontBief2 = new ArrayList();
          listeAvalBief2 = new ArrayList();
          listeAmontEcluse2 = new ArrayList();
          listeAvalEcluse2 = new ArrayList();
          rechercheAmontAval(gareSuiv, listeAmontBief2, listeAvalBief2, listeAmontEcluse2, listeAvalEcluse2,
              listeEcluses_, listeBiefs_);
          if (_listeGaresATraiter.contains(gareSuiv)) {
            gareSuiv.setX(_GareEnCours.getX() + 200/** * */
            );
            if ((listeAmontBief2.size() + listeAmontEcluse2.size() / 3) > 1) {
              gareSuiv.setY(_GareEnCours.getY() + 80);
              bief.setEndPosition(7);
              FuLog.debug("gare suiv 1 bief :" + listeAmontBief2.size() + "," + listeAmontEcluse2.size() / 3);
            }

            else {
              gareSuiv.setY(_GareEnCours.getY());
              bief.setEndPosition(6);
            }
          }

          if (_listeGaresATraiter.contains(_GareEnCours)) {

            // //grid_.add(_GareEnCours);
            vect_.addElement(_GareEnCours);
            System.out.println("ajout gare aval " + _GareEnCours.getData("gareKey"));

            _listeGaresATraiter.remove(_GareEnCours);
          }

          if (_listeBiefsATraiter.contains(bief)) {
            bief.setBeginPosition(2);
            System.out.println("ajout bief aval " + bief.getData("biefKey"));
            // //grid_.add(bief);
            vect_.addElement(bief);
            _listeBiefsATraiter.remove(bief);

          }
          if (_listeGaresATraiter.contains(gareSuiv)) {
            dispositionListeObjetRecursive(_listeGaresATraiter, _listeEclusesATraiter, _listeBiefsATraiter, gareSuiv);
          }
        } else {// /listeAvalEcluse==1
          it = listeAvalEcluse.listIterator();
          final DjaDiamond ecluse = (DjaDiamond) it.next();
          final DjaDirectArrow d = (DjaDirectArrow) it.next();// entrant
          final DjaDirectArrow d2 = (DjaDirectArrow) it.next();// dsortant

          // d2.setBeginPosition(2);
          d.setBeginPosition(2);
          // ecluse.getEndConnections()[0].setEndPosition(6);

          final DjaCircle gareSuiv = (DjaCircle) d2.getEndObject();
          d2.setEndPosition(6);
          // DjaCircle garePrec=(DjaCircle)ecluse.getBeginConnections()[0].getBeginObject();

          if (_listeGaresATraiter.contains(gareSuiv)) {
            gareSuiv.setX(_GareEnCours.getX() + 200);
            gareSuiv.setY(_GareEnCours.getY());
          }

          if (_listeGaresATraiter.contains(_GareEnCours)) {
            System.out.println("ajout gare aval " + _GareEnCours.getData("gareKey"));
            // //grid_.add(_GareEnCours);
            vect_.addElement(_GareEnCours);
            _listeGaresATraiter.remove(_GareEnCours);
          }
          if (_listeEclusesATraiter.contains(ecluse)) {
            ecluse.setX(_GareEnCours.getX() + 80);
            ecluse.setY(_GareEnCours.getY());
            System.out.println("ajout ecluse aval " + ecluse.getData("ecluseKey"));
            vect_.addElement(ecluse);
            // //grid_.add(ecluse);
            vect_.addElement(d2);
            // //grid_.add(d2);
            vect_.addElement(d);
            // //grid_.add(d);//**
            _listeEclusesATraiter.remove(ecluse);
          }
          if (_listeGaresATraiter.contains(gareSuiv)) {
            dispositionListeObjetRecursive(_listeGaresATraiter, _listeEclusesATraiter, _listeBiefsATraiter, gareSuiv);
          }
        }

      } else if (tailleListeAval == 2) {

        System.out.println("taille de la liste en aval =2");
        // on place l'element en arriere au point 6
        if (listeAvalBief.size() == 2) {
          it = listeAvalBief.listIterator();
          final DjaDirectArrow bief = (DjaDirectArrow) it.next();
          bief.setBeginPosition(1);
          bief.setEndPosition(6);
          final DjaCircle gareSuiv = (DjaCircle) bief.getEndObject();
          listeAmontBief2 = new ArrayList();
          listeAvalBief2 = new ArrayList();
          listeAmontEcluse2 = new ArrayList();
          listeAvalEcluse2 = new ArrayList();
          rechercheAmontAval(gareSuiv, listeAmontBief2, listeAvalBief2, listeAmontEcluse2, listeAvalEcluse2,
              listeEcluses_, listeBiefs_);
          if (_listeGaresATraiter.contains(gareSuiv)) {

            gareSuiv.setX(_GareEnCours.getX() + 200/** * */
            );
            gareSuiv.setY(_GareEnCours.getY() - 80);
          }
          /*
           * if((listeAmontBief2.size()+listeAmontEcluse2.size()/3)>1){ //garePrec.setY(_GareEnCours.getY()+80);
           * bief.setEndPosition(7); } else{ // garePrec.setY(_GareEnCours.getY()); bief.setEndPosition(6); }
           */
          /*************************************************************************************************************
           * if((listeAvalBief2.size()+listeAvalEcluse2.size())>1){ //garePrec.setY(_GareEnCours.getY()+80);
           * bief.setBeginPosition(7); } else{ // garePrec.setY(_GareEnCours.getY()); bief.setBeginPosition(6); }
           ************************************************************************************************************/

          final DjaDirectArrow bief2 = (DjaDirectArrow) it.next();
          bief2.setBeginPosition(3);
          bief2.setEndPosition(5);

          final DjaCircle gareSuiv2 = (DjaCircle) bief2.getEndObject();
          listeAmontBief2 = new ArrayList();
          listeAvalBief2 = new ArrayList();
          listeAmontEcluse2 = new ArrayList();
          listeAvalEcluse2 = new ArrayList();
          rechercheAmontAval(gareSuiv2, listeAmontBief2, listeAvalBief2, listeAmontEcluse2, listeAvalEcluse2,
              listeEcluses_, listeBiefs_);
          if (_listeGaresATraiter.contains(gareSuiv2)) {
            gareSuiv2.setX(_GareEnCours.getX() + 200/** * */
            );
            gareSuiv2.setY(_GareEnCours.getY() + 80);

            /***********************************************************************************************************
             * if((listeAmontBief2.size()+listeAmontEcluse2.size())>1){ gareSuiv2.setY(_GareEnCours.getY()+80);
             * bief2.setEndPosition(1); } else{ gareSuiv2.setY(_GareEnCours.getY()); bief2.setEndPosition(2); }
             **********************************************************************************************************/

          }
          /*
           * if((listeAvalBief2.size()+listeAvalEcluse2.size())>1) garePrec.setY(_GareEnCours.getY()+80); else
           * garePrec.setY(_GareEnCours.getY());
           */

          if (_listeGaresATraiter.contains(_GareEnCours)) {
            System.out.println("ajout gare aval " + _GareEnCours.getData("gareKey"));
            // //grid_.add(_GareEnCours);
            vect_.addElement(_GareEnCours);
            _listeGaresATraiter.remove(_GareEnCours);
          }
          if (_listeBiefsATraiter.contains(bief)) {
            System.out.println("ajout bief aval " + bief.getData("biefKey"));
            // //grid_.add(bief);
            vect_.addElement(bief);
            _listeBiefsATraiter.remove(bief);
          }
          if (_listeBiefsATraiter.contains(bief2)) {
            System.out.println("ajout bief aval " + bief2.getData("biefKey"));
            // //grid_.add(bief2);
            vect_.addElement(bief2);
            _listeBiefsATraiter.remove(bief2);
          }

          if (_listeGaresATraiter.contains(gareSuiv)) {
            dispositionListeObjetRecursive(_listeGaresATraiter, _listeEclusesATraiter, _listeBiefsATraiter, gareSuiv);
          }
          if (_listeGaresATraiter.contains(gareSuiv2)) {
            dispositionListeObjetRecursive(_listeGaresATraiter, _listeEclusesATraiter, _listeBiefsATraiter, gareSuiv2);
          }
        }
        if (listeAvalEcluse.size() / 3 == 2) {
          it = listeAvalEcluse.listIterator();
          final DjaDiamond ecluse = (DjaDiamond) it.next();
          final DjaDirectArrow d = (DjaDirectArrow) it.next();// entrant
          final DjaDirectArrow d2 = (DjaDirectArrow) it.next();// dsortant

          d2.setEndPosition(7);
          d.setBeginPosition(1);
          // ecluse.getEndConnections()[0].setEndPosition(6);

          final DjaCircle gareSuiv = (DjaCircle) d2.getEndObject();
          listeAmontBief2 = new ArrayList();
          listeAvalBief2 = new ArrayList();
          listeAmontEcluse2 = new ArrayList();
          listeAvalEcluse2 = new ArrayList();
          rechercheAmontAval(gareSuiv, listeAmontBief2, listeAvalBief2, listeAmontEcluse2, listeAvalEcluse2,
              listeEcluses_, listeBiefs_);
          /*
           * if((listeAvalBief2.size()+listeAvalEcluse2.size())>1){ garePrec.setY(_GareEnCours.getY()+80);
           * d.setBeginPosition(1); } else{ garePrec.setY(_GareEnCours.getY()); d.setBeginPosition(2); }
           */

          // DjaCircle garePrec=(DjaCircle)ecluse.getBeginConnections()[0].getBeginObject();
          final DjaDiamond ecluse2 = (DjaDiamond) it.next();
          final DjaDirectArrow dd = (DjaDirectArrow) it.next();// entrant
          final DjaDirectArrow dd2 = (DjaDirectArrow) it.next();// dsortant

          if (_listeGaresATraiter.contains(_GareEnCours)) {
            System.out.println("ajout gare aval " + _GareEnCours.getData("gareKey"));
            // //grid_.add(_GareEnCours);
            vect_.addElement(_GareEnCours);

            _listeGaresATraiter.remove(_GareEnCours);
          }
          if (_listeEclusesATraiter.contains(ecluse)) {
            ecluse.setX(_GareEnCours.getX() + 80);
            ecluse.setY(_GareEnCours.getY() - 40);// 80-->40
            gareSuiv.setX(_GareEnCours.getX() + 200);
            gareSuiv.setY(_GareEnCours.getY());
            System.out.println("ajout ecluse aval " + ecluse.getData("ecluseKey"));
            // //grid_.add(ecluse);
            vect_.addElement(ecluse);
            // //grid_.add(d2);
            vect_.addElement(d2);
            // //grid_.add(d);//**
            vect_.addElement(d);
            _listeEclusesATraiter.remove(ecluse);
          }
          if (_listeEclusesATraiter.contains(ecluse2)) {
            dd.setBeginPosition(3);
            dd2.setEndPosition(5);

            ecluse2.setX(_GareEnCours.getX() + 80);
            ecluse2.setY(_GareEnCours.getY() + 40);// 80-->40

            System.out.println("ajout ecluse aval " + ecluse2.getData("ecluseKey"));
            // //grid_.add(ecluse2);
            vect_.addElement(ecluse2);
            // //grid_.add(dd2);
            vect_.addElement(dd2);
            // //grid_.add(dd);//**
            vect_.addElement(dd);
            _listeEclusesATraiter.remove(ecluse2);

          }

          if (_listeGaresATraiter.contains(gareSuiv)) {
            dispositionListeObjetRecursive(_listeGaresATraiter, _listeEclusesATraiter, _listeBiefsATraiter, gareSuiv);
          }

        }
        if (listeAvalBief.size() == 1 && listeAvalEcluse.size() / 3 == 1) {// cas exceptionnel je pense ; non fait
          // encore
        }

      } else if (tailleListeAval == 3) {// logiquement 3 biefs ou 3 �cluses

        System.out.println("taille de la liste en aval =3");
        // on place l'element en arriere au point 6
        if (listeAvalBief.size() == 3) {
          it = listeAvalBief.listIterator();
          final DjaDirectArrow bief = (DjaDirectArrow) it.next();
          bief.setBeginPosition(1);
          final DjaCircle gareSuiv = (DjaCircle) bief.getEndObject();
          listeAmontBief2 = new ArrayList();
          listeAvalBief2 = new ArrayList();
          listeAmontEcluse2 = new ArrayList();
          listeAvalEcluse2 = new ArrayList();
          rechercheAmontAval(gareSuiv, listeAmontBief2, listeAvalBief2, listeAmontEcluse2, listeAvalEcluse2,
              listeEcluses_, listeBiefs_);

          if (_listeGaresATraiter.contains(gareSuiv)) {
            gareSuiv.setX(_GareEnCours.getX() + 200/** * */
            );
            gareSuiv.setY(_GareEnCours.getY() - 80);// sert a rien je sais pu pkoi
          }
          if ((listeAmontBief2.size() + listeAmontEcluse2.size() / 3) > 1) {
            // garePrec.setY(_GareEnCours.getY()-80);
            bief.setEndPosition(7);
          }

          else {
            // garePrec.setY(_GareEnCours.getY());
            bief.setEndPosition(6);
          }

          final DjaDirectArrow bief2 = (DjaDirectArrow) it.next();
          bief2.setBeginPosition(2);
          final DjaCircle gareSuiv2 = (DjaCircle) bief2.getEndObject();
          listeAmontBief2 = new ArrayList();
          listeAvalBief2 = new ArrayList();
          listeAmontEcluse2 = new ArrayList();
          listeAvalEcluse2 = new ArrayList();
          rechercheAmontAval(gareSuiv2, listeAmontBief2, listeAvalBief2, listeAmontEcluse2, listeAvalEcluse2,
              listeEcluses_, listeBiefs_);
          if (_listeGaresATraiter.contains(gareSuiv2)) {
            gareSuiv2.setX(_GareEnCours.getX() + 200/** * */
            );
            gareSuiv2.setY(_GareEnCours.getY());
          }
          /*
           * if((listeAvalBief2.size()+listeAvalEcluse2.size())>1) garePrec.setY(_GareEnCours.getY()+80); else
           * garePrec.setY(_GareEnCours.getY());
           */

          /*
           * if((listeAmontBief2.size()+listeAmontEcluse2.size())>1){ //garePrec2.setY(_GareEnCours.getY()+80);
           * bief2.setEndPosition(7); } else{
           */
          // garePrec2.setY(_GareEnCours.getY());
          bief2.setEndPosition(6);
          // }

          final DjaDirectArrow bief3 = (DjaDirectArrow) it.next();
          bief3.setBeginPosition(3);
          final DjaCircle gareSuiv3 = (DjaCircle) bief3.getEndObject();
          listeAmontBief2 = new ArrayList();
          listeAvalBief2 = new ArrayList();
          listeAmontEcluse2 = new ArrayList();
          listeAvalEcluse2 = new ArrayList();
          rechercheAmontAval(gareSuiv3, listeAmontBief2, listeAvalBief2, listeAmontEcluse2, listeAvalEcluse2,
              listeEcluses_, listeBiefs_);
          if (_listeGaresATraiter.contains(gareSuiv)) {
            gareSuiv3.setX(_GareEnCours.getX() + 200/** * */
            );
            gareSuiv3.setY(_GareEnCours.getY() + 80);
          }
          /*
           * if((listeAvalBief2.size()+listeAvalEcluse2.size())>1) garePrec.setY(_GareEnCours.getY()+80); else
           * garePrec.setY(_GareEnCours.getY());
           */

          /*
           * if((listeAvalBief2.size()+listeAvalEcluse2.size())>1){ //garePrec2.setY(_GareEnCours.getY()+80);
           */
          bief3.setEndPosition(5);
          /*
           * } else{ //garePrec2.setY(_GareEnCours.getY()); bief2.setEndPosition(6); }
           */

          if (_listeGaresATraiter.contains(_GareEnCours)) {
            System.out.println("ajout gare aval " + _GareEnCours.getData("gareKey"));
            // //grid_.add(_GareEnCours);
            vect_.addElement(_GareEnCours);
            _listeGaresATraiter.remove(_GareEnCours);
          }
          if (_listeBiefsATraiter.contains(bief)) {
            System.out.println("ajout bief aval " + bief.getData("biefKey"));
            // //grid_.add(bief);
            vect_.addElement(bief);
            _listeBiefsATraiter.remove(bief);
          }
          if (_listeBiefsATraiter.contains(bief2)) {
            System.out.println("ajout bief aval " + bief2.getData("biefKey"));
            // //grid_.add(bief2);
            vect_.addElement(bief2);
            _listeBiefsATraiter.remove(bief2);
          }
          if (_listeBiefsATraiter.contains(bief3)) {
            System.out.println("ajout bief aval " + bief3.getData("biefKey"));
            // //grid_.add(bief3);
            vect_.addElement(bief3);
            _listeBiefsATraiter.remove(bief3);
          }

          if (_listeGaresATraiter.contains(gareSuiv)) {
            dispositionListeObjetRecursive(_listeGaresATraiter, _listeEclusesATraiter, _listeBiefsATraiter, gareSuiv);
          }
          if (_listeGaresATraiter.contains(gareSuiv2)) {
            dispositionListeObjetRecursive(_listeGaresATraiter, _listeEclusesATraiter, _listeBiefsATraiter, gareSuiv2);
          }
          if (_listeGaresATraiter.contains(gareSuiv3)) {
            dispositionListeObjetRecursive(_listeGaresATraiter, _listeEclusesATraiter, _listeBiefsATraiter, gareSuiv3);
          }
        }
        if (listeAvalEcluse.size() / 3 == 3) {
          it = listeAvalEcluse.listIterator();
          final DjaDiamond ecluse = (DjaDiamond) it.next();
          final DjaDirectArrow d = (DjaDirectArrow) it.next();// entrant
          final DjaDirectArrow d2 = (DjaDirectArrow) it.next();// dsortant

          d2.setEndPosition(7);
          // ecluse.getEndConnections()[0].setEndPosition(6);
          d.setBeginPosition(1);
          final DjaCircle gareSuiv = (DjaCircle) d2.getEndObject();
          listeAmontBief2 = new ArrayList();
          listeAvalBief2 = new ArrayList();
          listeAmontEcluse2 = new ArrayList();
          listeAvalEcluse2 = new ArrayList();
          rechercheAmontAval(gareSuiv, listeAmontBief2, listeAvalBief2, listeAmontEcluse2, listeAvalEcluse2,
              listeEcluses_, listeBiefs_);
          /*
           * if((listeAvalBief2.size()+listeAvalEcluse2.size())>1){ // garePrec.setY(_GareEnCours.getY()+80);
           * d.setBeginPosition(1); } else{ // garePrec.setY(_GareEnCours.getY()); d.setBeginPosition(2); }
           */

          // DjaCircle garePrec=(DjaCircle)ecluse.getBeginConnections()[0].getBeginObject();
          final DjaDiamond ecluse2 = (DjaDiamond) it.next();
          final DjaDirectArrow dd = (DjaDirectArrow) it.next();// entrant
          final DjaDirectArrow dd2 = (DjaDirectArrow) it.next();// dsortant

          // ecluse.getEndConnections()[0].setEndPosition(6);

          final DjaDiamond ecluse3 = (DjaDiamond) it.next();
          final DjaDirectArrow ddd = (DjaDirectArrow) it.next();// entrant
          final DjaDirectArrow ddd2 = (DjaDirectArrow) it.next();// dsortant

          // ecluse.getEndConnections()[0].setEndPosition(6);

          if (_listeGaresATraiter.contains(_GareEnCours)) {
            System.out.println("ajout gare aval " + _GareEnCours.getData("gareKey"));
            // //grid_.add(_GareEnCours);

            vect_.addElement(_GareEnCours);
            _listeGaresATraiter.remove(_GareEnCours);
          }
          if (_listeEclusesATraiter.contains(ecluse)) {

            ecluse.setX(_GareEnCours.getX() + 80);
            ecluse.setY(_GareEnCours.getY() - 40);// 80-->40
            gareSuiv.setX(_GareEnCours.getX() + 200);
            gareSuiv.setY(_GareEnCours.getY());

            System.out.println("ajout ecluse aval " + ecluse.getData("ecluseKey"));
            // //grid_.add(ecluse);

            vect_.addElement(ecluse);
            // //grid_.add(d2);
            vect_.addElement(d2);
            // //grid_.add(d);//**
            vect_.addElement(d);
            _listeEclusesATraiter.remove(ecluse);
          }
          if (_listeEclusesATraiter.contains(ecluse2)) {

            dd.setBeginPosition(2);
            dd2.setEndPosition(6);

            ecluse2.setX(_GareEnCours.getX() + 80);
            ecluse2.setY(_GareEnCours.getY());
            gareSuiv.setX(_GareEnCours.getX() + 200);// --
            gareSuiv.setY(_GareEnCours.getY());// --

            System.out.println("ajout ecluse aval " + ecluse2.getData("ecluseKey"));
            // //grid_.add(ecluse2);
            vect_.addElement(ecluse2);
            // //grid_.add(dd2);
            vect_.addElement(dd2);
            // //grid_.add(dd);//**
            vect_.addElement(dd);
            _listeEclusesATraiter.remove(ecluse2);
          }
          if (_listeEclusesATraiter.contains(ecluse3)) {

            ddd.setBeginPosition(3);
            ddd2.setEndPosition(5);

            ecluse3.setX(_GareEnCours.getX() + 80);
            ecluse3.setY(_GareEnCours.getY() + 40);// 80-->40
            gareSuiv.setX(_GareEnCours.getX() + 200);// --
            gareSuiv.setY(_GareEnCours.getY());// --
            System.out.println("ajout ecluse aval " + ecluse.getData("ecluseKey"));
            // //grid_.add(ecluse3);
            vect_.addElement(ecluse3);
            // //grid_.add(ddd2);
            vect_.addElement(ddd2);
            // //grid_.add(ddd);//**
            vect_.addElement(ddd);
            _listeEclusesATraiter.remove(ecluse3);
          }

          if (_listeGaresATraiter.contains(gareSuiv)) {
            dispositionListeObjetRecursive(_listeGaresATraiter, _listeEclusesATraiter, _listeBiefsATraiter, gareSuiv);
          }

        }
        if (listeAvalBief.size() == 1 && listeAvalEcluse.size() / 3 == 2) {// cas exceptionnel je pense ; non fait
          // encore
        }
        if (listeAvalBief.size() == 2 && listeAvalEcluse.size() / 3 == 1) {// cas exceptionnel je pense ; non fait
          // encore
        }

      }
      /*
       * else System.out.println("Ca fait beaucoup tout de m�me >3 ou tr�s peu 0");//
       */

      else if (tailleListeAval == 4) {// logiquement 4 �cluses

        System.out.println("taille de la liste en aval =4");
        if (listeAvalEcluse.size() / 3 == 4) {
          it = listeAvalEcluse.listIterator();
          final DjaDiamond ecluse = (DjaDiamond) it.next();
          final DjaDirectArrow d = (DjaDirectArrow) it.next();// entrant
          final DjaDirectArrow d2 = (DjaDirectArrow) it.next();// dsortant

          d2.setEndPosition(7);
          // ecluse.getEndConnections()[0].setEndPosition(6);
          d.setBeginPosition(1);
          final DjaCircle gareSuiv = (DjaCircle) d2.getEndObject();
          listeAmontBief2 = new ArrayList();
          listeAvalBief2 = new ArrayList();
          listeAmontEcluse2 = new ArrayList();
          listeAvalEcluse2 = new ArrayList();
          rechercheAmontAval(gareSuiv, listeAmontBief2, listeAvalBief2, listeAmontEcluse2, listeAvalEcluse2,
              listeEcluses_, listeBiefs_);
          /*
           * if((listeAvalBief2.size()+listeAvalEcluse2.size())>1){ // garePrec.setY(_GareEnCours.getY()+80);
           * d.setBeginPosition(1); } else{ // garePrec.setY(_GareEnCours.getY()); d.setBeginPosition(2); }
           */

          // DjaCircle garePrec=(DjaCircle)ecluse.getBeginConnections()[0].getBeginObject();
          final DjaDiamond ecluse2 = (DjaDiamond) it.next();
          final DjaDirectArrow dd = (DjaDirectArrow) it.next();// entrant
          final DjaDirectArrow dd2 = (DjaDirectArrow) it.next();// dsortant

          // ecluse.getEndConnections()[0].setEndPosition(6);

          final DjaDiamond ecluse3 = (DjaDiamond) it.next();
          final DjaDirectArrow ddd = (DjaDirectArrow) it.next();// entrant
          final DjaDirectArrow ddd2 = (DjaDirectArrow) it.next();// dsortant

          final DjaDiamond ecluse4 = (DjaDiamond) it.next();
          final DjaDirectArrow dddd = (DjaDirectArrow) it.next();// entrant
          final DjaDirectArrow dddd2 = (DjaDirectArrow) it.next();// dsortant

          // ecluse.getEndConnections()[0].setEndPosition(6);

          if (_listeGaresATraiter.contains(_GareEnCours)) {
            System.out.println("ajout gare aval " + _GareEnCours.getData("gareKey"));
            // //grid_.add(_GareEnCours);

            vect_.addElement(_GareEnCours);
            _listeGaresATraiter.remove(_GareEnCours);
          }
          if (_listeEclusesATraiter.contains(ecluse)) {

            ecluse.setX(_GareEnCours.getX() + 80);
            ecluse.setY(_GareEnCours.getY() - 40);// 80-->40
            gareSuiv.setX(_GareEnCours.getX() + 200);
            gareSuiv.setY(_GareEnCours.getY());

            System.out.println("ajout ecluse aval " + ecluse.getData("ecluseKey"));
            // //grid_.add(ecluse);

            vect_.addElement(ecluse);
            // //grid_.add(d2);
            vect_.addElement(d2);
            // //grid_.add(d);//**
            vect_.addElement(d);
            _listeEclusesATraiter.remove(ecluse);
          }
          if (_listeEclusesATraiter.contains(ecluse2)) {

            dd.setBeginPosition(2);
            dd2.setEndPosition(6);

            ecluse2.setX(_GareEnCours.getX() + 80);
            ecluse2.setY(_GareEnCours.getY());
            gareSuiv.setX(_GareEnCours.getX() + 200);// --
            gareSuiv.setY(_GareEnCours.getY());// --

            System.out.println("ajout ecluse aval " + ecluse2.getData("ecluseKey"));
            // //grid_.add(ecluse2);
            vect_.addElement(ecluse2);
            // //grid_.add(dd2);
            vect_.addElement(dd2);
            // //grid_.add(dd);//**
            vect_.addElement(dd);
            _listeEclusesATraiter.remove(ecluse2);
          }
          if (_listeEclusesATraiter.contains(ecluse3)) {

            ddd.setBeginPosition(3);
            ddd2.setEndPosition(5);

            ecluse3.setX(_GareEnCours.getX() + 80);
            ecluse3.setY(_GareEnCours.getY() + 40);// 80-->40
            gareSuiv.setX(_GareEnCours.getX() + 200);// --
            gareSuiv.setY(_GareEnCours.getY());// --
            System.out.println("ajout ecluse aval " + ecluse.getData("ecluseKey"));
            // //grid_.add(ecluse3);
            vect_.addElement(ecluse3);
            // //grid_.add(ddd2);
            vect_.addElement(ddd2);
            // //grid_.add(ddd);//**
            vect_.addElement(ddd);
            _listeEclusesATraiter.remove(ecluse3);
          }
          if (_listeEclusesATraiter.contains(ecluse4)) {

            dddd.setBeginPosition(0);
            dddd2.setEndPosition(0);

            ecluse4.setX(_GareEnCours.getX() + 80);
            ecluse4.setY(_GareEnCours.getY() + 80);// 80-->40
            gareSuiv.setX(_GareEnCours.getX() + 200);// --
            gareSuiv.setY(_GareEnCours.getY());// --
            System.out.println("ajout ecluse aval " + ecluse.getData("ecluseKey"));
            // //grid_.add(ecluse3);
            vect_.addElement(ecluse4);
            // //grid_.add(ddd2);
            vect_.addElement(dddd2);
            // //grid_.add(ddd);//**
            vect_.addElement(dddd);
            _listeEclusesATraiter.remove(ecluse4);
          }

          if (_listeGaresATraiter.contains(gareSuiv)) {
            dispositionListeObjetRecursive(_listeGaresATraiter, _listeEclusesATraiter, _listeBiefsATraiter, gareSuiv);
          }

        }

      } else if (tailleListeAval == 5) {// logiquement 4 �cluses

        System.out.println("taille de la liste en aval =5");
        if (listeAvalEcluse.size() / 3 == 5) {
          it = listeAvalEcluse.listIterator();
          final DjaDiamond ecluse = (DjaDiamond) it.next();
          final DjaDirectArrow d = (DjaDirectArrow) it.next();// entrant
          final DjaDirectArrow d2 = (DjaDirectArrow) it.next();// dsortant

          d2.setEndPosition(7);
          // ecluse.getEndConnections()[0].setEndPosition(6);
          d.setBeginPosition(1);
          final DjaCircle gareSuiv = (DjaCircle) d2.getEndObject();
          listeAmontBief2 = new ArrayList();
          listeAvalBief2 = new ArrayList();
          listeAmontEcluse2 = new ArrayList();
          listeAvalEcluse2 = new ArrayList();
          rechercheAmontAval(gareSuiv, listeAmontBief2, listeAvalBief2, listeAmontEcluse2, listeAvalEcluse2,
              listeEcluses_, listeBiefs_);
          /*
           * if((listeAvalBief2.size()+listeAvalEcluse2.size())>1){ // garePrec.setY(_GareEnCours.getY()+80);
           * d.setBeginPosition(1); } else{ // garePrec.setY(_GareEnCours.getY()); d.setBeginPosition(2); }
           */

          // DjaCircle garePrec=(DjaCircle)ecluse.getBeginConnections()[0].getBeginObject();
          final DjaDiamond ecluse2 = (DjaDiamond) it.next();
          final DjaDirectArrow dd = (DjaDirectArrow) it.next();// entrant
          final DjaDirectArrow dd2 = (DjaDirectArrow) it.next();// dsortant

          // ecluse.getEndConnections()[0].setEndPosition(6);

          final DjaDiamond ecluse3 = (DjaDiamond) it.next();
          final DjaDirectArrow ddd = (DjaDirectArrow) it.next();// entrant
          final DjaDirectArrow ddd2 = (DjaDirectArrow) it.next();// dsortant

          final DjaDiamond ecluse4 = (DjaDiamond) it.next();
          final DjaDirectArrow dddd = (DjaDirectArrow) it.next();// entrant
          final DjaDirectArrow dddd2 = (DjaDirectArrow) it.next();// dsortant

          final DjaDiamond ecluse5 = (DjaDiamond) it.next();
          final DjaDirectArrow ddddd = (DjaDirectArrow) it.next();// entrant
          final DjaDirectArrow ddddd2 = (DjaDirectArrow) it.next();// dsortant

          // ecluse.getEndConnections()[0].setEndPosition(6);

          if (_listeGaresATraiter.contains(_GareEnCours)) {
            System.out.println("ajout gare aval " + _GareEnCours.getData("gareKey"));
            // //grid_.add(_GareEnCours);

            vect_.addElement(_GareEnCours);
            _listeGaresATraiter.remove(_GareEnCours);
          }
          if (_listeEclusesATraiter.contains(ecluse)) {

            ecluse.setX(_GareEnCours.getX() + 80);
            ecluse.setY(_GareEnCours.getY() - 40);// 80-->40
            gareSuiv.setX(_GareEnCours.getX() + 200);
            gareSuiv.setY(_GareEnCours.getY());

            System.out.println("ajout ecluse aval " + ecluse.getData("ecluseKey"));
            // //grid_.add(ecluse);

            vect_.addElement(ecluse);
            // //grid_.add(d2);
            vect_.addElement(d2);
            // //grid_.add(d);//**
            vect_.addElement(d);
            _listeEclusesATraiter.remove(ecluse);
          }
          if (_listeEclusesATraiter.contains(ecluse2)) {

            dd.setBeginPosition(2);
            dd2.setEndPosition(6);

            ecluse2.setX(_GareEnCours.getX() + 80);
            ecluse2.setY(_GareEnCours.getY());
            gareSuiv.setX(_GareEnCours.getX() + 200);// --
            gareSuiv.setY(_GareEnCours.getY());// --

            System.out.println("ajout ecluse aval " + ecluse2.getData("ecluseKey"));
            // //grid_.add(ecluse2);
            vect_.addElement(ecluse2);
            // //grid_.add(dd2);
            vect_.addElement(dd2);
            // //grid_.add(dd);//**
            vect_.addElement(dd);
            _listeEclusesATraiter.remove(ecluse2);
          }
          if (_listeEclusesATraiter.contains(ecluse3)) {

            ddd.setBeginPosition(3);
            ddd2.setEndPosition(5);

            ecluse3.setX(_GareEnCours.getX() + 80);
            ecluse3.setY(_GareEnCours.getY() + 40);// 80-->40
            gareSuiv.setX(_GareEnCours.getX() + 200);// --
            gareSuiv.setY(_GareEnCours.getY());// --
            System.out.println("ajout ecluse aval " + ecluse.getData("ecluseKey"));
            // //grid_.add(ecluse3);
            vect_.addElement(ecluse3);
            // //grid_.add(ddd2);
            vect_.addElement(ddd2);
            // //grid_.add(ddd);//**
            vect_.addElement(ddd);
            _listeEclusesATraiter.remove(ecluse3);
          }
          if (_listeEclusesATraiter.contains(ecluse4)) {

            dddd.setBeginPosition(0);
            dddd2.setEndPosition(0);

            ecluse4.setX(_GareEnCours.getX() + 80);
            ecluse4.setY(_GareEnCours.getY() - 80);// 80-->40
            gareSuiv.setX(_GareEnCours.getX() + 200);// --
            gareSuiv.setY(_GareEnCours.getY());// --
            System.out.println("ajout ecluse aval " + ecluse.getData("ecluseKey"));
            // //grid_.add(ecluse3);
            vect_.addElement(ecluse4);
            // //grid_.add(ddd2);
            vect_.addElement(dddd2);
            // //grid_.add(ddd);//**
            vect_.addElement(dddd);
            _listeEclusesATraiter.remove(ecluse4);
          }
          if (_listeEclusesATraiter.contains(ecluse5)) {

            ddddd.setBeginPosition(4);
            ddddd2.setEndPosition(4);

            ecluse5.setX(_GareEnCours.getX() + 80);
            ecluse5.setY(_GareEnCours.getY() + 80);// 80-->40
            gareSuiv.setX(_GareEnCours.getX() + 200);// --
            gareSuiv.setY(_GareEnCours.getY());// --
            System.out.println("ajout ecluse aval " + ecluse.getData("ecluseKey"));
            // //grid_.add(ecluse3);
            vect_.addElement(ecluse5);
            // //grid_.add(ddd2);
            vect_.addElement(ddddd2);
            // //grid_.add(ddd);//**
            vect_.addElement(ddddd);
            _listeEclusesATraiter.remove(ecluse5);
          }

          if (_listeGaresATraiter.contains(gareSuiv)) {
            dispositionListeObjetRecursive(_listeGaresATraiter, _listeEclusesATraiter, _listeBiefsATraiter, gareSuiv);
          }

        }

      }

    } else {
      System.out.println("Les objets sont dispos�s");
      // PSSSSS : il faudra rajouter les cas exceptionnels pour montrer les erreurs comises par l'utilisateur
    }
  }

  private void rechercheAmontAval(final DjaCircle _gareCourante, final ArrayList _listeAmontBief,
      final ArrayList _listeAvalBief, final ArrayList _listeAmontEcluse, final ArrayList _listeAvalEcluse,
      final ArrayList _listeEcluses, final ArrayList _listeBiefs) {
    System.out.println("recherche amont aval");
    final ListIterator le = _listeEcluses.listIterator();
    final ListIterator lb = _listeBiefs.listIterator();
    /*******************************************************************************************************************
     * _listeAmontBief=new ArrayList(); _listeAvalBief=new ArrayList(); _listeAmontEcluse=new ArrayList();
     * _listeAvalEcluse=new ArrayList();
     ******************************************************************************************************************/
    /*
     * _listeAmontBief=null; _listeAvalBief=null; _listeAmontEcluse=null; _listeAvalEcluse=null;
     */

    // liste des �cluses
    while (le.hasNext()) {
      final DjaDiamond eDja = (DjaDiamond) le.next();
      final DjaDirectArrow d = (DjaDirectArrow) le.next();// dentrant
      final DjaDirectArrow d2 = (DjaDirectArrow) le.next();// dsortant
      // SParametresEcluse eObj=(SParametresEcluse) eDja.getData("ecluseKey");
      /*****************************************************************************************************************
       * if(eDja.getEndConnections()[0].getEndObject().equals(_gareCourante)){ _listeAmontEcluse.add(eDja); }
       ****************************************************************************************************************/
      if (d2.getEndObject().equals(_gareCourante)) {

        _listeAmontEcluse.add(eDja);
        _listeAmontEcluse.add(d);
        _listeAmontEcluse.add(d2);

      }

      /*****************************************************************************************************************
       * if(eDja.getBeginConnections()[0].getBeginObject()==_gareCourante){ _listeAvalEcluse.add(eDja); }
       ****************************************************************************************************************/
      if (d.getBeginObject().equals(_gareCourante)) {

        _listeAvalEcluse.add(eDja);
        _listeAvalEcluse.add(d);
        _listeAvalEcluse.add(d2);

      }
    }
    // liste des biefs
    while (lb.hasNext()) {
      final DjaDirectArrow aDja = (DjaDirectArrow) lb.next();
      // System.out.println("longueu " +aDja.getEndObject().getEndConnections().length);
      // if(aDja.getEndConnections()[0].getEndObject().equals(_gareCourante)){
      if (aDja.getEndObject().equals(_gareCourante)) {
        // _listeAmontBief.add(aDja.getBeginObject());//GARE PRECEDENTE DU BIEF
        _listeAmontBief.add(aDja);// ARC
        FuLog.debug("amont bief1 " + aDja.getEndO());

      }
      if (aDja.getBeginObject().equals(_gareCourante)) {
        // if(aDja.getBeginConnections()[0].getBeginObject()==_gareCourante){
        _listeAvalBief.add(aDja);
      }

    }

  }

  /**
   * on cr� la liste des objets dja pour dessiner le r�seau.
   */
  private void creationListeObjet(final ArrayList _listeBiefs, final ArrayList _listeEcluses,
      final ArrayList _listeGares) {
    /*
     * creation de la liste des gares on met le num�ro de la gare en Text on ajoute une cl� gareKey de valeur le num�ro
     * de la gare
     */
    ListIterator it;
    String s;
    // if(_listeGares.size()!=0){
    it = _listeGares.listIterator();

    while (it.hasNext()) {
      s = new String(it.next().toString());
      final DjaCircle g = new DjaCircle(s);
      g.setHeight(40);
      // g.setText("test",1);
      // g.putProperty("gareKey",s);
      g.putData("gareKey", s);
      // System.out.println("ggggggggggg"+g.getData("gareKey"));
      // grid_.add(g);
      listeGares_.add(g);
      /*****************************************************************************************************************
       * g.addText("00h00m00"); g.addText("00m00");
       ****************************************************************************************************************/
    }
    // }
    /**
     * On cr� la liste des �cluses On ajoute les connexion en cherchant la gare pr�c�dente ou suivante gr�ce � la
     * m�thode rechercheDjaGare le trait pr�c�dent et li� a la gare en amont et au Diamond tandis que le trait suivant
     * est li� � la fin du Diamond et � la gare avalante.
     */

    if (_listeEcluses != null && _listeEcluses.size() != 0) {
      it = _listeEcluses.listIterator();
      while (it.hasNext()) {
        final SParametresEcluse e = (SParametresEcluse) it.next();
        s = e.identification;

        final DjaDiamond g = new DjaDiamond(s);
        g.putData("ecluseKey", e);// s->e
        final DjaDirectArrow d = new DjaDirectArrow();
        d.setEndType(0);
        d.setBeginObject(rechercheDjaGare("" + e.gareEnAmont));
        d.setEndObject(g);
        d.setEndPosition(6);
        // d.setColor(Color.white);
        final DjaDirectArrow d2 = new DjaDirectArrow();
        d2.setEndType(0);
        d2.setBeginObject(g);
        d2.setBeginPosition(2);
        d2.setEndObject(rechercheDjaGare("" + e.gareEnAval));
        // d2.setColor(Color.white);
        // grid_.add(g);

        // grid_.add(d);
        // grid_.add(d2);

        listeEcluses_.add(g);
        listeEcluses_.add(d);
        listeEcluses_.add(d2);
      }
    }

    /**
     * cr�ation de la liste de biefs, on proc�de comme pour les �cluses on recherche les gares adjacentes pour fixer les
     * connexions
     */

    if (_listeBiefs != null && _listeBiefs.size() != 0) {
      it = _listeBiefs.listIterator();
      while (it.hasNext()) {
        final SParametresBief b = (SParametresBief) it.next();
        s = b.identification;
        final DjaDirectArrow g = new DjaDirectArrow(s);

        g.putData("biefKey", b);// s->b
        g.setBeginObject(rechercheDjaGare("" + b.gareEnAmont));
        // g.setBegin(rechercheDjaGare(""+b.gareEnAmont),3,3 );
        g.setEndObject(rechercheDjaGare("" + b.gareEnAval));
        // grid_.add(g);
        listeBiefs_.add(g);
      }
    }
    /*
     * else{ BuDialogMessage dialog_mess = new BuDialogMessage(imp_, imp_.getInformationsSoftware(), "Il faut r�aliser
     * au minimum une connexion"); dialog_mess.activate(); }
     */

  }

  /**
   * recherche de la gare en donnant son nom on parcours la liste des DjaCircle et on retourne la gare pour mettre les
   * liens.
   */

  private DjaCircle rechercheDjaGare(final String s) {
    final ListIterator l = listeGares_.listIterator();
    boolean trouve = false;
    DjaCircle s2 = null;
    while (l.hasNext() && !trouve) {
      s2 = (DjaCircle) l.next();
      if (s.equals(s2.getData("gareKey"))) {
        trouve = true;

      }

    }
    return s2;
  }

  /**
   * retourne l'indice de la gare s en parcourant la listeG.
   */

  private int rechercheIndiceGare(final String s) {
    final ListIterator l = listeG_.listIterator();
    boolean trouve = false;
    String s2 = "";
    int x = -1;
    while (l.hasNext() && !trouve) {
      s2 = (l.next().toString());
      if (s.equals(s2)) {
        trouve = true;
        x = l.nextIndex() - 1;
      }

    }
    return x;
  }

  /**
   * on met les coordonn�es du rectangle entourant les objets afin de d�finir des zones pour afficher les informations
   * et d�finir des zones de click.
   */

  private void miseAJourTabObjets() {
    tabObjetsE_ = new Rectangle[listeEcluses_.size() / 3];
    tabObjetsG_ = new Rectangle[listeGares_.size()];
    tabObjetsB_ = new DjaDirectArrow[listeBiefs_.size()];
    ListIterator it;
    int i = 0;
    it = listeGares_.listIterator();
    while (it.hasNext()) {
      final DjaCircle d = (DjaCircle) it.next();
      tabObjetsG_[i] = d.getBounds();
      i++;
    }
    it = listeBiefs_.listIterator();
    i = 0;
    while (it.hasNext()) {
      final DjaDirectArrow d = (DjaDirectArrow) it.next();
      tabObjetsB_[i] = d;
      i++;
    }

    int j = 0;
    it = listeEcluses_.listIterator();
    while (it.hasNext()) {
      final DjaDiamond d = (DjaDiamond) it.next();
      /* tabObjetsB_[i]=(DjaDirectArrow) */it.next();
      i++;
      /* //tabObjetsB_[i]=(DjaDirectArrow) */it.next();
      tabObjetsE_[j] = d.getBounds();
      i++;
      j++;
    }
  }

  /**
   * Cette m�thode a pour but de supprimer les intersections on regarde les bounds afin de bouger les objets pour
   * emp�cher les intersections et on les mets � jour � chaque d�placement. On bouge verticalement tous les objets pour
   * le supprimer. Cette m�thode est appel� r�cursivement jusqu'� temps qu'il n'y est plus d'intersection (boole�n
   * modif).
   */

  private void supprimeIntersections() {
    boolean modif = false;
    miseAJourTabObjets();

    ListIterator it;
    // verifie intersection ecluse ecluse
    // verifie intersection biefs
    for (int i = 0; i < tabObjetsB_.length; i++) {
      // bief bief
      for (int j = i + 1; j < tabObjetsB_.length; j++) {
        if (tabObjetsB_[i].getBounds().intersects(tabObjetsB_[j].getBounds())) {
          modif = true;

          System.out.println("bief*bief");
          System.out.println("intersection entre " + i + " et " + j);
          final DjaDirectArrow d1 = tabObjetsB_[i];
          final DjaDirectArrow d2 = tabObjetsB_[j];
          final DjaCircle g1 = (DjaCircle) d1.getBeginObject();
          final DjaCircle g2 = (DjaCircle) d2.getBeginObject();
          if (g1.getY() < g2.getY()) {
            final ArrayList listtemp1 = new ArrayList();
            final ArrayList listtemp2 = new ArrayList();
            g1.setY(g1.getY() - 40);
            ArrayList listeAmontBief = new ArrayList();
            ArrayList listeAvalBief = new ArrayList();
            ArrayList listeAmontEcluse = new ArrayList();
            ArrayList listeAvalEcluse = new ArrayList();
            rechercheAmontAval(g1, listeAmontBief, listeAvalBief, listeAmontEcluse, listeAvalEcluse, listeEcluses_,
                listeBiefs_);
            it = listeAvalBief.listIterator();
            while (it.hasNext()) {
              final DjaDirectArrow dtemp = (DjaDirectArrow) it.next();
              final DjaCircle ctemp = (DjaCircle) dtemp.getEndObject();
              ctemp.setY(ctemp.getY() - 40);
              listtemp1.add(ctemp);
            }
            g2.setY(g2.getY() + 40);
            listeAmontBief = new ArrayList();
            listeAvalBief = new ArrayList();
            listeAmontEcluse = new ArrayList();
            listeAvalEcluse = new ArrayList();
            rechercheAmontAval(g2, listeAmontBief, listeAvalBief, listeAmontEcluse, listeAvalEcluse, listeEcluses_,
                listeBiefs_);
            it = listeAvalBief.listIterator();
            while (it.hasNext()) {
              final DjaDirectArrow dtemp = (DjaDirectArrow) it.next();
              final DjaCircle ctemp = (DjaCircle) dtemp.getEndObject();
              ctemp.setY(ctemp.getY() + 40);
              listtemp2.add(ctemp);
            }

            it = listeGares_.listIterator();
            while (it.hasNext()) {
              final DjaCircle g = (DjaCircle) it.next();
              if (g.getY() <= g1.getY() + 40) {
                if (!listtemp1.contains(g) && !listtemp2.contains(g)) {
                  g.setY(g.getY() - 40);
                }
              } else if (!listtemp1.contains(g) && !listtemp2.contains(g)) {
                g.setY(g.getY() + 40);
              }
            }
            it = listeEcluses_.listIterator();
            while (it.hasNext()) {
              final DjaDiamond g = (DjaDiamond) it.next();
              it.next();
              it.next();
              if (g.getY() <= g1.getY() + 40) {
                if (!listtemp1.contains(g) && !listtemp2.contains(g)) {
                  g.setY(g.getY() - 40);
                }
              } else if (!listtemp1.contains(g) && !listtemp2.contains(g)) {
                g.setY(g.getY() + 40);
              }
            }
            g1.setY(g1.getY() + 40);
            g2.setY(g2.getY() - 40);
          } else {
            g1.setY(g1.getY() + 40);
            ArrayList listeAmontBief = new ArrayList();
            ArrayList listeAvalBief = new ArrayList();
            ArrayList listeAmontEcluse = new ArrayList();
            ArrayList listeAvalEcluse = new ArrayList();
            final ArrayList listtemp1 = new ArrayList();
            final ArrayList listtemp2 = new ArrayList();
            rechercheAmontAval(g1, listeAmontBief, listeAvalBief, listeAmontEcluse, listeAvalEcluse, listeEcluses_,
                listeBiefs_);
            it = listeAvalBief.listIterator();
            while (it.hasNext()) {
              final DjaDirectArrow dtemp = (DjaDirectArrow) it.next();
              final DjaCircle ctemp = (DjaCircle) dtemp.getEndObject();
              ctemp.setY(ctemp.getY() + 40);
              listtemp1.add(ctemp);
            }
            g2.setY(g2.getY() - 40);
            listeAmontBief = new ArrayList();
            listeAvalBief = new ArrayList();
            listeAmontEcluse = new ArrayList();
            listeAvalEcluse = new ArrayList();
            rechercheAmontAval(g2, listeAmontBief, listeAvalBief, listeAmontEcluse, listeAvalEcluse, listeEcluses_,
                listeBiefs_);
            it = listeAvalBief.listIterator();
            while (it.hasNext()) {
              final DjaDirectArrow dtemp = (DjaDirectArrow) it.next();
              final DjaCircle ctemp = (DjaCircle) dtemp.getEndObject();
              ctemp.setY(ctemp.getY() - 40);
              listtemp2.add(ctemp);
            }

            it = listeGares_.listIterator();
            while (it.hasNext()) {
              final DjaCircle g = (DjaCircle) it.next();
              if (g.getY() < g1.getY() - 40) {
                if (!listtemp1.contains(g) && !listtemp2.contains(g)) {
                  g.setY(g.getY() - 40);
                }
              } else // if(g.getY()>g1.getY()+40)
              if (!listtemp1.contains(g) && !listtemp2.contains(g)) {
                g.setY(g.getY() + 40);
              }
            }
            it = listeEcluses_.listIterator();
            while (it.hasNext()) {
              final DjaDiamond g = (DjaDiamond) it.next();
              it.next();
              it.next();
              if (g.getY() < g1.getY() - 40) {
                if (!listtemp1.contains(g) && !listtemp2.contains(g)) {
                  g.setY(g.getY() - 40);
                }
              } else // if(g.getY()>g1.getY()+40)
              if (!listtemp1.contains(g) && !listtemp2.contains(g)) {
                g.setY(g.getY() + 40);
              }
            }

            g1.setY(g1.getY() - 40);
            g2.setY(g2.getY() + 40);
          }
          miseAJourTabObjets();
        }
      }

    }
    for (int i = 0; i < tabObjetsE_.length; i++) {
      // ecluse ecluse
      for (int j = i + 1; j < tabObjetsE_.length; j++) {
        if (tabObjetsE_[i].intersects(tabObjetsE_[j])) {
          modif = true;
          System.out.println("ecluse * ecluse");
          System.out.println("intersection entre " + i + " et " + j);
          final DjaDiamond e1 = (DjaDiamond) listeEcluses_.get(i * 3);
          final DjaDirectArrow de1 = (DjaDirectArrow) listeEcluses_.get(i * 3 + 1);// dentrant

          final DjaCircle g1 = (DjaCircle) de1.getBeginObject();
          if (g1.getY() < e1.getY()) {
            e1.setY(e1.getY() - 40);
            it = listeGares_.listIterator();
            while (it.hasNext()) {
              final DjaCircle g = (DjaCircle) it.next();
              if (g.getY() < e1.getY() + 40) {
                g.setY(g.getY() - 40);
              } else {
                // if(g.getY()>e1.getY()+40)
                g.setY(g.getY() + 40);
              }
            }
            it = listeEcluses_.listIterator();
            while (it.hasNext()) {
              final DjaDiamond g = (DjaDiamond) it.next();
              it.next();
              it.next();
              if (g.getY() < e1.getY() + 40) {
                g.setY(g.getY() - 40);
              } else {
                // if(g.getY()>e1.getY()+40)
                g.setY(g.getY() + 40);
              }
            }
            e1.setY(e1.getY() + 40);
          } else if (g1.getY() > e1.getY()) {
            e1.setY(e1.getY() + 40);
            it = listeGares_.listIterator();
            while (it.hasNext()) {
              final DjaCircle g = (DjaCircle) it.next();
              if (g.getY() < e1.getY() - 40) {
                g.setY(g.getY() + 40);
              } else {
                // if(g.getY()>e1.getY()-40)
                g.setY(g.getY() - 40);
              }
            }
            it = listeEcluses_.listIterator();
            while (it.hasNext()) {
              final DjaDiamond g = (DjaDiamond) it.next();
              it.next();
              it.next();
              if (g.getY() < e1.getY() - 40) {
                g.setY(g.getY() + 40);
              } else {
                // if(g.getY()>e1.getY()-40)
                g.setY(g.getY() - 40);
              }
            }
            e1.setY(e1.getY() - 40);
          }

        }
        miseAJourTabObjets();
      }
      // ecluse gare
      for (int j = 0; j < tabObjetsG_.length; j++) {
        if (tabObjetsE_[i].intersects(tabObjetsG_[j])) {
          modif = true;
          System.out.println("ecluse * gare");
          System.out.println("intersection entre " + i + " et " + j);
          final DjaDiamond e1 = (DjaDiamond) listeEcluses_.get(i * 3);
          final DjaDirectArrow de1 = (DjaDirectArrow) listeEcluses_.get(i * 3 + 1);// dentrant
          final DjaCircle g1 = (DjaCircle) de1.getBeginObject();
          if (g1.getY() < e1.getY()) {
            e1.setY(e1.getY() - 40);
            it = listeGares_.listIterator();
            while (it.hasNext()) {
              final DjaCircle g = (DjaCircle) it.next();
              if (g.getY() < e1.getY() + 40) {
                g.setY(g.getY() - 40);
              } else {
                // if(g.getY()>e1.getY()+40)
                g.setY(g.getY() + 40);
              }
            }
            it = listeEcluses_.listIterator();
            while (it.hasNext()) {
              final DjaDiamond g = (DjaDiamond) it.next();
              it.next();
              it.next();
              if (g.getY() < e1.getY() + 40) {
                g.setY(g.getY() - 40);
              } else {
                // if(g.getY()>e1.getY()+40)
                g.setY(g.getY() + 40);
              }
            }
            e1.setY(e1.getY() + 40);
          } else if (g1.getY() > e1.getY()) {
            e1.setY(e1.getY() + 40);
            it = listeGares_.listIterator();
            while (it.hasNext()) {
              final DjaCircle g = (DjaCircle) it.next();
              if (g.getY() < e1.getY() - 40) {
                g.setY(g.getY() + 40);
              } else {
                // if(g.getY()>e1.getY()-40)
                g.setY(g.getY() - 40);
              }
            }
            it = listeEcluses_.listIterator();
            while (it.hasNext()) {
              final DjaDiamond g = (DjaDiamond) it.next();
              it.next();
              it.next();
              if (g.getY() < e1.getY() - 40) {
                g.setY(g.getY() + 40);
              } else {
                // if(g.getY()>e1.getY()-40)
                g.setY(g.getY() - 40);
              }
            }
            e1.setY(e1.getY() - 40);
          }
          miseAJourTabObjets();
        }
      }
    }

    // gare gare
    for (int i = 0; i < tabObjetsG_.length; i++) {
      for (int j = i + 1; j < tabObjetsG_.length; j++) {
        if (tabObjetsG_[i].intersects(tabObjetsG_[j])) {
          modif = true;
          // System.out.println("gare * gare");
          // System.out.println("intersection entre "+i+" et "+j);
          DjaCircle g1 = (DjaCircle) listeGares_.get(i);
          ArrayList listeAmontBief = new ArrayList();
          ArrayList listeAvalBief = new ArrayList();
          ArrayList listeAmontEcluse = new ArrayList();
          ArrayList listeAvalEcluse = new ArrayList();
          DjaCircle g2 = null;
          rechercheAmontAval(g1, listeAmontBief, listeAvalBief, listeAmontEcluse, listeAvalEcluse, listeEcluses_,
              listeBiefs_);
          if (listeAmontBief.size() > 0) {
            final ListIterator it1 = listeAmontBief.listIterator();
            final DjaDirectArrow e = (DjaDirectArrow) it1.next();
            g2 = (DjaCircle) e.getBeginObject();

          } else if (listeAvalBief.size() > 0) {
            final ListIterator it1 = listeAvalBief.listIterator();
            final DjaDirectArrow e = (DjaDirectArrow) it1.next();
            g2 = (DjaCircle) e.getEndObject();

          }
          if (g2 == null) {
            continue;
            // DjaDirectArrow l=(DjaDirectArrow) g1.getEndConnections()[0];
          }

          // if(g1.getBeginConnections()[0].getY()<g1.getY()){
          if (g2.getY() == g1.getY()) {
            g1 = (DjaCircle) listeGares_.get(j);
            listeAmontBief = new ArrayList();
            listeAvalBief = new ArrayList();
            listeAmontEcluse = new ArrayList();
            listeAvalEcluse = new ArrayList();
            g2 = null;
            rechercheAmontAval(g1, listeAmontBief, listeAvalBief, listeAmontEcluse, listeAvalEcluse, listeEcluses_,
                listeBiefs_);
            if (listeAmontBief.size() > 0) {
              FuLog.debug("taille bief :" + listeAmontBief.size());
              final ListIterator it1 = listeAmontBief.listIterator();
              final DjaDirectArrow e = (DjaDirectArrow) it1.next();
              g2 = (DjaCircle) e.getBeginObject();

            } else if (listeAvalBief.size() > 0) {
              final ListIterator it1 = listeAvalBief.listIterator();
              final DjaDirectArrow e = (DjaDirectArrow) it1.next();
              g2 = (DjaCircle) e.getEndObject();

            }
          }
          final int yg1 = g1.getY();
          final int yg2 = g2.getY();
          if (/* g2.getY() */yg2 < /* g1.getY() */yg1) {
            // int yg1=g1.getY();
            // g1.setY(g1.getY()-40);
            it = listeGares_.listIterator();
            while (it.hasNext()) {
              final DjaCircle g = (DjaCircle) it.next();
              if (g.getY() < /* g1.getY() */yg1/* +40 */) {
                g.setY(g.getY() - 40);
              } else {
                // if(g.getY()>e1.getY()+40)
                g.setY(g.getY() + 40);
              }
            }
            it = listeEcluses_.listIterator();
            while (it.hasNext()) {
              final DjaDiamond g = (DjaDiamond) it.next();
              it.next();
              it.next();
              if (g.getY() < /* g1.getY()+40 */yg1) {
                g.setY(g.getY() - 40);
              } else {
                // if(g.getY()>g1.getY()+40)
                g.setY(g.getY() + 40);
              }
            }
            // g1.setY(g1.getY()+40);
            g1.setY(yg1 - 40);
          } else if (/* g2.getY() */yg2 > /* g1.getY() */yg1) {
            // g1.setY(g1.getY()+40);
            it = listeGares_.listIterator();
            while (it.hasNext()) {
              final DjaCircle g = (DjaCircle) it.next();
              if (g.getY() <= /* g1.getY()-40 */yg1) {
                g.setY(g.getY() - 40);
              } else {
                // if(g.getY()>e1.getY()-40)
                g.setY(g.getY() + 40);
              }
            }
            it = listeEcluses_.listIterator();
            while (it.hasNext()) {
              final DjaDiamond g = (DjaDiamond) it.next();
              it.next();
              it.next();
              if (g.getY() <= /* g1.getY()-40 */yg1) {
                g.setY(g.getY() - 40);
              } else {
                // if(g.getY()>e1.getY()-40)
                g.setY(g.getY() + 40);
              }
            }
            // g1.setY(g1.getY()-40);
            g1.setY(yg1 + 40);
          }
          miseAJourTabObjets();
          /*************************************************************************************************************
           * i=tabObjetsB_.length; j=tabObjetsB_.length;
           ************************************************************************************************************/
        }
      }

    }

    if (modif) {
      // System.out.println("21 seconde");
      supprimeIntersections();
    }

  }

  /*
   * ArrayList listeGaresAVerifier=listeGares_; ArrayList listeBiefsAVerifier=listeBiefs_; ArrayList
   * listeEclusesAVerifier=listeEcluses_; ListIterator it; int i=0; it=listeGaresAVerifier.listIterator();
   * while(it.hasNext()){ i++; DjaCircle d=(DjaCircle)it.next(); }
   */

  public void actionPerformed(final ActionEvent _e) {
    if (_e.getSource() == enregReseau) {
      cmdEnregistrerSous();
    }
    if (_e.getSource() == majReseau) {
      imp_.removeInternalFrame(this);
      imp_.menuPalette(null);
    }
    if (_e.getSource() == modEcluse) {
      System.out.println("num�ro ecluse " + elemAMod);
      imp_.ajouterEcluse(elemAMod, true);

    }
    if (_e.getSource() == modBief) {
      System.out.println("num�ro bief " + elemAMod);
      imp_.ajouterBief(elemAMod, true);
    }

    if (_e.getSource() == modBiefConnexion) {

      imp_.sinavi2filleaddmodconnexions_ = new Sinavi2FilleAddModConnexion(imp_, imp_.listeGares_, imp_.listeBiefs_,
          imp_.listeEcluses_);
      imp_.sinavi2filleaddmodconnexions_.initialiseChamps(biefCourant_.identification, "bie:");
      imp_.sinavi2filleaddmodconnexions_.tfNomvoie_.setEnabled(false);
      imp_.sinavi2filleaddmodconnexions_.setVisible(true);
    }
    if (_e.getSource() == modEcluseConnexion) {

      imp_.sinavi2filleaddmodconnexions_ = new Sinavi2FilleAddModConnexion(imp_, imp_.listeGares_, imp_.listeBiefs_,
          imp_.listeEcluses_);
      imp_.sinavi2filleaddmodconnexions_.initialiseChamps(ecluseCourant_.identification, "ecl:");
      imp_.sinavi2filleaddmodconnexions_.tfNomvoie_.setEnabled(false);
      imp_.sinavi2filleaddmodconnexions_.setVisible(true);
    }
    if (_e.getSource() == supBief) {

      imp_.supprimerBiefs(imp_.listeBiefs_.indexOf(biefCourant_));
    }
    if (_e.getSource() == supEcluse) {

      imp_.supprimerBiefs(imp_.listeEcluses_.indexOf(ecluseCourant_));
    }

    if (_e.getSource() == affBateaux) {
      imp_.afficherBateaux();
    }
    if (_e.getSource() == affBiefs) {
      imp_.afficherBiefs();
    }
    if (_e.getSource() == affEcluses) {
      imp_.afficherEcluses();
    }
    if (_e.getSource() == affControles) {
      imp_.controler();
    }
    if (_e.getSource() == affTrajets) {
      imp_.afficherTrajets();
    }
    if (_e.getSource() == affCroisements) {
      imp_.menuCroisements("");
    }
    if (_e.getSource() == affTrematages) {
      imp_.menuTrematages("");
    }
    if (_e.getSource() == affVitesses) {
      imp_.menuVitesses(0, 0);
    }
    if (_e.getSource() == affManoeuvres) {
      imp_.menuManoeuvres(0, 0);
    }
    if (_e.getSource() == affConnexions) {
      imp_.afficherConnexions();
    }
    if (_e.getSource() == modBiefTrematages) {
      imp_.menuTrematages(biefCourant_.identification);
    }
    if (_e.getSource() == modBiefCroisements) {
      imp_.menuCroisements(biefCourant_.identification);
    }

  }

  /**
   * Cette m�thode g�re tous les clics de souris.
   */

  public void mouseClicked(final MouseEvent _e) {
    int i = 0;
    final int x = _e.getX();
    final int y = _e.getY();
    DjaObject obj;

    final int bouton = _e.getButton();

    switch (bouton) {
    case 1:
      System.out.println("bouton 1 coordonn�es : " + x + CtuluLibString.VIR + y);
      /*
       * if(_e.getSource().equals(modEcluse)){ imp_.ajouter_ecluse(elemAMod); } if(_e.getSource().equals(modBief)){
       * imp_.ajouter_bief(elemAMod); }
       */
      break;
    case 3:
      System.out.println("bouton 2 coordonn�es : " + x + CtuluLibString.VIR + y);

      boolean trouve = false;
      ListIterator it;
      while (!trouve) {
        it = listeBiefs_.listIterator();
        while (it.hasNext() && !trouve) {
          final DjaDirectArrow d = (DjaDirectArrow) it.next();
          if (d.contains(x, y)) {
            // if(r.equals(d.getBounds())){
            obj = d;
            trouve = true;
            final SParametresBief b = (SParametresBief) d.getData(this.BIEF_CLEF);
            biefCourant_ = b;
            menu2 = new BuPopupMenu();

            menu2.add(modBief);
            menu2.add(modBiefConnexion);
            menu2.add(supBief);
            menu2.add(modBiefTrematages);
            menu2.add(modBiefCroisements);
            final ListIterator it2 = listeB_.listIterator();
            // boolean trouve2=false;
            // while(it2.hasNext() && !trouve){
            elemAMod = i;
            // }
            menu2.show(grid2_, x, y);

            // menu.remove(modBief);
          }
          i++;
        }
        i = 0;
        it = listeEcluses_.listIterator();
        while (it.hasNext() && !trouve) {
          final DjaDiamond d = (DjaDiamond) it.next();

          if (d.contains(x, y)) {
            // if(r.equals(d.getBounds())){
            obj = d;
            trouve = true;
            final SParametresEcluse e = (SParametresEcluse) d.getData(this.ECLUSE_CLEF);
            ecluseCourant_ = e;
            menu2 = new BuPopupMenu();
            menu2.add(modEcluse);
            menu2.add(modEcluseConnexion);
            menu2.add(supEcluse);
            elemAMod = i;
            menu2.show(grid2_, x, y);
            // menu.remove(modEcluse);
          }
          it.next();
          it.next();
          i++;
        }
        if (!trouve) {
          menu.show(grid2_, x, y);
          trouve = true;
        }
      }

      break;
    case 2:
      System.out.println("bouton 3 coordonn�es : " + x + CtuluLibString.VIR + y);
      final ListIterator it2 = listeBiefs_.listIterator();
      i = 0;
      while (it2.hasNext()) {
        final DjaDirectArrow d = (DjaDirectArrow) it2.next();
        final Rectangle r = d.getBounds();
        System.out.println("rect " + i + r.height + " " + r.y);
        i++;
      }

      break;
    }

  }

  /**
   * on met dans les _tabX les surfaces des �l�ments X. / private void rechercheTableauBounds(Rectangle[] _tabG,
   * Rectangle[] _tabE, Rectangle[] _tabB) { ListIterator it; int i=0; it=listeGares_.listIterator();
   * while(it.hasNext()){ DjaCircle d=(DjaCircle)it.next(); _tabG[i]=d.getBounds(); i++; }
   * it=listeBiefs_.listIterator(); i=0; while(it.hasNext()){ DjaDirectArrow d=(DjaDirectArrow)it.next();
   * _tabB[i]=d.getBounds(); i++; } i=0; it=listeEcluses_.listIterator(); while(it.hasNext()){ DjaDiamond
   * d=(DjaDiamond)it.next(); it.next(); it.next(); _tabE[i]=d.getBounds(); i++; } } /** affiche les informations qd on
   * d�place la souris les infos sont affich�s en HTML. On place les coordonn�es de la souris dans x et y et on
   * recherche l'�l�ment qui contient ses coordonn�es. On affiche alors ces informations dans une bulle.
   */

  public void mouseMoved(final MouseEvent _e) {
    final int x = _e.getX();
    final int y = _e.getY();
    final Component l = grid2_.getComponentAt(x, y);

    DjaObject obj;
    /*
     * Rectangle[] tabG=new Rectangle[listeGares_.size()]; Rectangle[] tabE=new Rectangle[listeEcluses_.size()];
     * Rectangle[] tabB=new Rectangle[listeBiefs_.size()]; rechercheTableauBounds(tabG,tabE,tabB);
     */
    boolean trouve = false;
    ListIterator it;
    while (!trouve) {
      it = listeGares_.listIterator();
      while (it.hasNext()) {
        final DjaCircle d = (DjaCircle) it.next();
        if (d.contains(x, y)) {
          // if(r.equals(d.getBounds())){
          obj = d;
          // System.out.println("cle gare "+d.getData("keyGare").toString());
          trouve = true;
          grid2_.setToolTipText("gare " + d.getData(this.GARE_CLEF));
        }
      }
      it = listeBiefs_.listIterator();
      while (it.hasNext()) {
        final DjaDirectArrow d = (DjaDirectArrow) it.next();
        if (d.contains(x, y)) {
          // if(r.equals(d.getBounds())){
          obj = d;
          trouve = true;
          final SParametresBief b = (SParametresBief) d.getData(this.BIEF_CLEF);
          grid2_.setToolTipText("<HTML><center><b>bief</b></center>" + "<i>nom : </i>" + b.identification + ""
              + "<BR><i>lgr : </i>" + b.longueur + "m</BR>" + "<BR><i>lar : </i>" + b.largeur + "m</BR>"
              + "<BR><i>htr : </i>" + b.hauteur + "m</BR>" + "<BR><i>vit : </i>" + b.vitesse + "km/s</BR>" + "</HTML>"/*
                                                                                                                       * <BR>nom :
                                                                                                                       * b.identification<\BR> /*
                                                                                                                       * +"\n
                                                                                                                       * lgr :
                                                                                                                       * "+b.longueur+
                                                                                                                       * "\n
                                                                                                                       * lar :
                                                                                                                       * "+b.largeur+
                                                                                                                       * "\n
                                                                                                                       * htr :
                                                                                                                       * "+b.hauteur+
                                                                                                                       * "\n
                                                                                                                       * vit :
                                                                                                                       * "+b.vitesse
                                                                                                                       */
          );

          /*
           * grid2_.setToolTipText("bief :\n"+" nom : "+ b.identification+"\n lgr : "+b.longueur+ "\n lar : "+b.largeur+
           * "\n htr : "+b.hauteur+ "\n vit : "+b.vitesse );
           */
        }
      }

      it = listeEcluses_.listIterator();
      while (it.hasNext()) {
        final DjaDiamond d = (DjaDiamond) it.next();
        if (d.contains(x, y)) {
          // if(r.equals(d.getBounds())){
          obj = d;
          trouve = true;
          final SParametresEcluse e = (SParametresEcluse) d.getData(ECLUSE_CLEF);
          grid2_.setToolTipText("<HTML><center><b>ecluse</b></center>" + "<i>nom : </i>" + e.identification + ""
              + "<BR><i>lgr : </i>" + e.longueur + "m</BR>" + "<BR><i>lar : </i>" + e.largeur + "m</BR>"
              + "<BR><i>pdr : </i>" + e.profondeur + "m</BR>" + "<BR><i>Bas M : </i>" + e.dureeBassineeMontante
              + "min/s</BR>" + "<BR><i>Bas S : </i>" + e.dureeBassineeDescendante + "min/s</BR>"
              + "<BR><i>Man E : </i>" + e.dureeManoeuvresEnEntree + "min/s</BR>" + "<BR><i>Man S : </i>"
              + e.dureeManoeuvresEnSortie + "min/s</BR>" + "</HTML>");

          /*
           * grid2_.setToolTipText("ecluse :\n"+" nom : "+ e.identification+ "\n lgr : "+e.longueur+ "\n lar :
           * "+e.largeur+ "\n pfr : "+e.profondeur+ "\n Bas M : "+e.dureeBassineeMontante+ "\n Bas D :
           * "+e.dureeBassineeDescendante+ "\n Man E : "+e.dureeManoeuvresEnEntr�e+ "\n Man S :
           * "+e.dureeManoeuvresEnSortie );
           */

        }
        it.next();
        it.next();
      }
      if (!trouve) {
        trouve = true;
        grid2_.setToolTipText("");
      }
    }

  }

  public void mouseEntered(final MouseEvent _e) {
  /*
   * int x=_e.getX(); int y=_e.getY(); Component l=grid2_.getComponentAt(x,y); Rectangle r=l.getBounds(); DjaObject obj;
   * Rectangle[] tabG=new Rectangle[listeGares_.size()]; Rectangle[] tabE=new Rectangle[listeEcluses_.size()];
   * Rectangle[] tabB=new Rectangle[listeBiefs_.size()]; rechercheTableauBounds(tabG,tabE,tabB); boolean trouve=false;
   * ListIterator it; while(!trouve){ it=listeGares_.listIterator(); while(it.hasNext()){ DjaCircle
   * d=(DjaCircle)it.next(); if(d.getBounds().contains(x,y)){ //if(r.equals(d.getBounds())){ obj=d; trouve=true;
   * grid2_.setToolTipText("gare"+(String) d.getData("keyGare")); } } it=listeBiefs_.listIterator();
   * while(it.hasNext()){ DjaDirectArrow d=(DjaDirectArrow)it.next(); if(d.getBounds().contains(x,y)){
   * //if(r.equals(d.getBounds())){ obj=d; trouve=true; grid2_.setToolTipText("bief"+(String) d.getData("keyBief")); } }
   * it=listeEcluses_.listIterator(); while(it.hasNext()){ DjaDiamond d=(DjaDiamond)it.next();
   * if(d.getBounds().contains(x,y)){ //if(r.equals(d.getBounds())){ obj=d; trouve=true; grid2_.setToolTipText("ecluse"+
   * (String) d.getData("keyEcluse")); } it.next(); it.next(); } trouve=true; }
   */
  }

  public void mouseExited(final MouseEvent _e) {

  }

  public void mousePressed(final MouseEvent _e) {

  }

  public void mouseReleased(final MouseEvent _e) {

  }

  public void creationListeObjets(final int gareDebut, final int GareFin) {

  }

  public void mouseDragged(final MouseEvent _e) {

  }

  private boolean verifier_connexions(final ArrayList _listeBiefs, final ArrayList _listeEcluses,
      final ArrayList _listeGares) {
    // aie si listbief est null cela plante
//    if ((_listeBiefs != null || _listeBiefs.size() != 0) && (_listeEcluses != null || _listeEcluses.size() != 0)) {
    //voulait-on faire cela
      if (!CtuluLibArray.isEmpty(listeBiefs_) && CtuluLibArray.isEmpty(listeEcluses_)) {
      System.out.println("test");
      final ListIterator it = _listeGares.listIterator();
      System.out.println("test1");
      ArrayList listeAmontBief = null;
      ArrayList listeAvalBief = null;
      ArrayList listeAmontEcluse = null;
      ArrayList listeAvalEcluse = null;
      while (it.hasNext()) {
        final String x = (String) it.next();
        listeAmontBief = new ArrayList();
        listeAvalBief = new ArrayList();
        listeAmontEcluse = new ArrayList();
        listeAvalEcluse = new ArrayList();
        rechercheAmontAval2(x, listeAmontBief, listeAvalBief, listeAmontEcluse, listeAvalEcluse, _listeEcluses,
            _listeBiefs);
        System.out.println("liste amont bief " + listeAmontBief.size() + " avalb " + listeAvalBief.size() + " amonte "
            + listeAmontEcluse.size() + "avale" + listeAvalEcluse.size());
        if ((listeAmontBief.size() > 0 && listeAmontEcluse.size() > 0)
            || (listeAvalBief.size() > 0 && listeAvalEcluse.size() > 0)) {
          imp_.affMessage("Il ne peut y avoir des biefs et des �cluses en amont d'une \n seule gare. V�rifiez la gare "
              + x);
          return false;
        }
      }
      return true;
    } // else {
    return true;
    // }

  }

  private void rechercheAmontAval2(final String _gareCourante, final ArrayList _listeAmontBief,
      final ArrayList _listeAvalBief, final ArrayList _listeAmontEcluse, final ArrayList _listeAvalEcluse,
      final ArrayList _listeEcluses, final ArrayList _listeBiefs) {
    System.out.println("recherche amont aval");
    final ListIterator le = _listeEcluses.listIterator();
    final ListIterator lb = _listeBiefs.listIterator();

    // liste des �cluses
    while (le.hasNext()) {
      System.out.println("eee");
      final SParametresEcluse e = (SParametresEcluse) le.next();

      if (("" + e.gareEnAval).equals(_gareCourante)) {
        _listeAmontEcluse.add(e);
      }
      if (("" + e.gareEnAmont).equals(_gareCourante)) {

        _listeAvalEcluse.add(e);
      }
    }
    // liste des biefs
    while (lb.hasNext()) {
      System.out.println("bbb");
      final SParametresBief b = (SParametresBief) lb.next();
      // System.out.println("longueu " +aDja.getEndObject().getEndConnections().length);
      // if(aDja.getEndConnections()[0].getEndObject().equals(_gareCourante)){
      if (("" + b.gareEnAval).equals(_gareCourante)) {
        // _listeAmontBief.add(aDja.getBeginObject());//GARE PRECEDENTE DU BIEF
        _listeAmontBief.add(b);// ARC

      }
      if (("" + b.gareEnAmont).equals(_gareCourante)) {
        // if(aDja.getBeginConnections()[0].getBeginObject()==_gareCourante){
        _listeAvalBief.add(b);
      }

    }

  }

  public void cmdEnregistrerSous() {
    final DjaLoadSavePng i = new DjaLoadSavePng();
    final BuFileChooser chooser = new BuFileChooser();
    chooser.setFileFilter(new BuFileFilter("png", "Fichiers : " + ".Png,.png"));
    final int returnVal = chooser.showOpenDialog((JFrame) imp_.getApp());
    if (returnVal == JFileChooser.APPROVE_OPTION) {
      String filename = chooser.getSelectedFile().getAbsolutePath();
      if ((!filename.substring(filename.length() - 4).equalsIgnoreCase(".png"))
          && (!filename.substring(filename.length() - 4).equalsIgnoreCase(".Gif"))) {
        filename = filename + ".png";
      }
      final File f = new File(filename);

      try {

        i.save(f, vect_);
        imp_.affMessage("Le r�seau a �t� enregistr� avec succ�s.");
      } catch (final Exception e) {
        // TODO Auto-generated catch block
        imp_.affMessage("Une erreur inopin�e s'est produite.");

        e.printStackTrace();
      }

    }

  }

}