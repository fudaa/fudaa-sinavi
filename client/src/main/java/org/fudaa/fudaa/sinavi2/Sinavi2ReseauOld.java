package org.fudaa.fudaa.sinavi2;

import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.ListIterator;

import javax.swing.JScrollPane;

import com.memoire.bu.BuInternalFrame;
import com.memoire.dja.DjaArcArrow;
import com.memoire.dja.DjaCircle;
import com.memoire.dja.DjaDiamond;
import com.memoire.dja.DjaDirectArrow;
import com.memoire.dja.DjaGrid;

import org.fudaa.ctulu.CtuluLibString;

import org.fudaa.dodico.corba.sinavi2.SParametresBief;
import org.fudaa.dodico.corba.sinavi2.SParametresEcluse;

class Sinavi2ReseauOld extends BuInternalFrame implements MouseListener, ActionListener {
  protected ArrayList listeGares_ = new ArrayList();
  protected ArrayList listeEcluses_ = new ArrayList();
  protected ArrayList listeBiefs_ = new ArrayList();
  protected ArrayList listeG_;
  protected ArrayList listeE_;
  protected ArrayList listeB_;
  protected DjaGrid grid_;

  public Sinavi2ReseauOld(final ArrayList _listeBateaux, final ArrayList _listeBiefs, final ArrayList _listeEcluses,
      final ArrayList _listeGares, final ArrayList _listeTrajets) {
    grid_ = new DjaGrid();
    // getContentPane().add(grid_);
    setTitle("Repr�sentation du r�seau");
    creationListeObjet(_listeBiefs, _listeEcluses, _listeGares, _listeTrajets);
    dispositionListeObjet();
    // DjaCircle test=new DjaCircle("1");
    listeG_ = _listeBiefs;
    listeE_ = _listeEcluses;
    listeB_ = _listeBiefs;

    /*
     * DjaText t=new DjaText(); t.setText("pos0"); t.setPosition(0); test.addText(t); DjaText t1=new DjaText();
     * t1.setText("pos1"); t1.setPosition(1); test.addText(t1);
     */
    // d.add(test);
    grid_.repaint();
    grid_.setVisible(true);
    // DjaLayoutTree t=new DjaLayoutTree();
    // t.layout(grid_);
    final JScrollPane scrollPane = new JScrollPane(grid_);

    getContentPane().add(scrollPane);
    // getContentPane().add(grid_);
    setTitle("R�seau Sinavi");
    setBounds(10, 20, 300, 200);
    addMouseListener(this);
    System.out.println("nombre de gares : " + listeGares_.size() + " ecluses" + listeEcluses_.size() / 3 + " biefs "
        + listeBiefs_.size());
  }

  private void dispositionListeObjet() {
    final ArrayList listeGaresATraiter = new ArrayList(listeGares_);
    final ArrayList listeEclusesATraiter = new ArrayList(listeEcluses_);
    final ArrayList listeBiefsATraiter = new ArrayList(listeBiefs_);
    final DjaCircle d = (DjaCircle) listeGaresATraiter.get(0);// debut d ela recherche de larboresecnce
    /*
     * d.setX(700); d.setY(300);
     */
    d.setX(350);
    d.setY(150);
    dispositionListeObjetRecursive(listeGaresATraiter, listeEclusesATraiter, listeBiefsATraiter,
        (DjaCircle) listeGaresATraiter.get(0));
    /*
     * if(listeGaresATraiter.size()>0){ rechercheAmontAval((DjaCircle) listeGaresATraiter.get(0), listeAmont,listeAval,
     * listeEclusesATraiter, listeBiefsATraiter); }
     */
  }

  private void dispositionListeObjetRecursive(final ArrayList _listeGaresATraiter,
      final ArrayList _listeEclusesATraiter, final ArrayList _listeBiefsATraiter, final DjaCircle _GareEnCours) {
    System.out.println("recusivit�");
    ListIterator it;
    int tailleListeAmont = 0;
    int tailleListeAval = 0;
    /*******************************************************************************************************************
     * ArrayList listeAmontBief = null; ArrayList listeAvalBief=null; ArrayList listeAmontEcluse = null; ArrayList
     * listeAvalEcluse=null; ArrayList listeAmontBief2 = null; ArrayList listeAvalBief2=null; ArrayList
     * listeAmontEcluse2 = null; ArrayList listeAvalEcluse2=null;
     ******************************************************************************************************************/
    final ArrayList listeAmontBief = new ArrayList();
    final ArrayList listeAvalBief = new ArrayList();
    final ArrayList listeAmontEcluse = new ArrayList();
    final ArrayList listeAvalEcluse = new ArrayList();
    final ArrayList listeAmontBief2 = new ArrayList();
    final ArrayList listeAvalBief2 = new ArrayList();
    final ArrayList listeAmontEcluse2 = new ArrayList();
    final ArrayList listeAvalEcluse2 = new ArrayList();

    if (_GareEnCours != null && _listeGaresATraiter.size() != 0) {
      // if(_listeGaresATraiter.size()>0){
      // rechercheAmontAval(_GareEnCours, listeAmontBief,listeAvalBief,listeAmontEcluse,listeAvalEcluse,
      // _listeEclusesATraiter, _listeBiefsATraiter);
      System.out.println("premier recherche");
      rechercheAmontAval(_GareEnCours, listeAmontBief, listeAvalBief, listeAmontEcluse, listeAvalEcluse, listeEcluses_,
          listeBiefs_);
      System.out.println("fin premier recherche");

      // traitement de la liste en amont

      if (listeAmontBief != null) {
        tailleListeAmont = listeAmontBief.size();
      }
      if (listeAmontEcluse != null) {
        tailleListeAmont += listeAmontEcluse.size() / 3;
      }
      if (listeAvalBief != null) {
        tailleListeAval = listeAvalBief.size();
      }
      if (listeAvalEcluse != null) {
        tailleListeAval += listeAvalEcluse.size() / 3;
      }
      if (tailleListeAmont == 0 && tailleListeAval == 0) {
        System.out.println("taille de la liste en amont = 0 --------------> on fait rien");
        if (_listeGaresATraiter.contains(_GareEnCours)) {
          grid_.add(_GareEnCours);
        }
        _listeGaresATraiter.remove(_GareEnCours);
        System.out.println("ajout gare : " + _GareEnCours.getText());
      }

      else if (tailleListeAmont == 1) {
        System.out.println("taille de la liste en amont =1");
        // on place l'element en arriere au point 6
        if (listeAmontBief.size() == 1) {
          // aEntrer les positions
          it = listeAmontBief.listIterator();
          final DjaArcArrow bief = (DjaArcArrow) it.next();
          bief.setEndPosition(6);
          final DjaCircle garePrec = (DjaCircle) bief.getBeginObject();
          rechercheAmontAval(garePrec, listeAmontBief2, listeAvalBief2, listeAmontEcluse2, listeAvalEcluse2,
              listeEcluses_, listeBiefs_);
          garePrec.setX(_GareEnCours.getX() - 80);
          if ((listeAvalBief2.size() + listeAvalEcluse2.size()) > 1) {
            garePrec.setY(_GareEnCours.getY() + 80);
            bief.setBeginPosition(1);
          }

          else {
            garePrec.setY(_GareEnCours.getY());
            bief.setBeginPosition(2);
          }
          if (_listeGaresATraiter.contains(_GareEnCours)) {
            grid_.add(_GareEnCours);
            _listeGaresATraiter.remove(_GareEnCours);
          }
          if (_listeBiefsATraiter.contains(bief)) {
            grid_.add(bief);
            _listeBiefsATraiter.remove(bief);
          }

          if (_listeGaresATraiter.contains(garePrec)) {
            dispositionListeObjetRecursive(_listeGaresATraiter, _listeEclusesATraiter, _listeBiefsATraiter, garePrec);
          }
        } else {// /listeAmontEcluse==1
          it = listeAmontEcluse.listIterator();
          final DjaDiamond ecluse = (DjaDiamond) it.next();
          final DjaDirectArrow d = (DjaDirectArrow) it.next();// entrant
          final DjaDirectArrow d2 = (DjaDirectArrow) it.next();// dsortant

          d2.setEndPosition(6);
          // ecluse.getEndConnections()[0].setEndPosition(6);

          final DjaCircle garePrec = (DjaCircle) d.getBeginObject();
          d.setBeginPosition(2);
          // DjaCircle garePrec=(DjaCircle)ecluse.getBeginConnections()[0].getBeginObject();
          ecluse.setX(_GareEnCours.getX() - 120);
          ecluse.setY(_GareEnCours.getY());
          garePrec.setX(_GareEnCours.getX() - 200);
          garePrec.setY(_GareEnCours.getY());
          if (_listeGaresATraiter.contains(_GareEnCours)) {
            grid_.add(_GareEnCours);
            System.out.println("ajout gare : " + _GareEnCours.getText());
            _listeGaresATraiter.remove(_GareEnCours);
          }
          if (_listeEclusesATraiter.contains(ecluse)) {
            System.out.println("ajout ecluse : " + ecluse.getText());
            grid_.add(ecluse);
            grid_.add(d2);
            grid_.add(d);// **
            _listeEclusesATraiter.remove(ecluse);
          }

          if (_listeGaresATraiter.contains(garePrec)) {
            dispositionListeObjetRecursive(_listeGaresATraiter, _listeEclusesATraiter, _listeBiefsATraiter, garePrec);
          }
        }

      } else if (tailleListeAmont == 2) {

        System.out.println("taille de la liste en amont =2");
        // on place l'element en arriere au point 6
        if (listeAmontBief.size() == 2) {
          it = listeAmontBief.listIterator();
          final DjaArcArrow bief = (DjaArcArrow) it.next();
          bief.setEndPosition(7);
          final DjaCircle garePrec = (DjaCircle) bief.getBeginObject();
          rechercheAmontAval(garePrec, listeAmontBief2, listeAvalBief2, listeAmontEcluse2, listeAvalEcluse2,
              listeEcluses_, listeBiefs_);
          garePrec.setX(_GareEnCours.getX() - 80);
          garePrec.setY(_GareEnCours.getY() - 80);

          if ((listeAvalBief2.size() + listeAvalEcluse2.size()) > 1) {
            garePrec.setY(_GareEnCours.getY() + 80);
            bief.setBeginPosition(1);
          }

          else {
            garePrec.setY(_GareEnCours.getY());
            bief.setBeginPosition(2);
          }

          final DjaArcArrow bief2 = (DjaArcArrow) it.next();
          bief2.setEndPosition(5);
          final DjaCircle garePrec2 = (DjaCircle) bief2.getBeginObject();
          rechercheAmontAval(garePrec2, listeAmontBief2, listeAvalBief2, listeAmontEcluse2, listeAvalEcluse2,
              listeEcluses_, listeBiefs_);
          garePrec2.setX(_GareEnCours.getX() - 80);
          garePrec2.setY(_GareEnCours.getY() + 80);
          /*
           * if((listeAvalBief2.size()+listeAvalEcluse2.size())>1) garePrec.setY(_GareEnCours.getY()+80); else
           * garePrec.setY(_GareEnCours.getY());
           */

          if ((listeAvalBief2.size() + listeAvalEcluse2.size()) > 1) {
            garePrec2.setY(_GareEnCours.getY() + 80);
            bief2.setBeginPosition(1);
          }

          else {
            garePrec2.setY(_GareEnCours.getY());
            bief2.setBeginPosition(2);
          }

          if (_listeGaresATraiter.contains(_GareEnCours)) {
            grid_.add(_GareEnCours);
            System.out.println("ajout gare : " + _GareEnCours.getText());
            _listeGaresATraiter.remove(_GareEnCours);
          }
          if (_listeBiefsATraiter.contains(bief)) {
            grid_.add(bief);
            System.out.println("ajout bief : " + bief.getText());
            _listeBiefsATraiter.remove(bief);
          }

          if (_listeBiefsATraiter.contains(bief2)) {
            grid_.add(bief2);
            System.out.println("ajout bief : " + bief2.getText());
            _listeBiefsATraiter.remove(bief2);
          }

          if (_listeGaresATraiter.contains(garePrec)) {
            dispositionListeObjetRecursive(_listeGaresATraiter, _listeEclusesATraiter, _listeBiefsATraiter, garePrec);
          }
          if (_listeGaresATraiter.contains(garePrec2)) {
            dispositionListeObjetRecursive(_listeGaresATraiter, _listeEclusesATraiter, _listeBiefsATraiter, garePrec2);
          }
        }
        if (listeAmontEcluse.size() / 3 == 2) {
          it = listeAmontEcluse.listIterator();
          final DjaDiamond ecluse = (DjaDiamond) it.next();
          final DjaDirectArrow d = (DjaDirectArrow) it.next();// entrant
          final DjaDirectArrow d2 = (DjaDirectArrow) it.next();// dsortant

          d2.setEndPosition(7);
          // ecluse.getEndConnections()[0].setEndPosition(6);

          final DjaCircle garePrec = (DjaCircle) d.getBeginObject();
          rechercheAmontAval(garePrec, listeAmontBief2, listeAvalBief2, listeAmontEcluse2, listeAvalEcluse2,
              listeEcluses_, listeBiefs_);
          if ((listeAvalBief2.size() + listeAvalEcluse2.size()) > 1) {
            garePrec.setY(_GareEnCours.getY() + 80);
            d.setBeginPosition(1);
          }

          else {
            garePrec.setY(_GareEnCours.getY());
            d.setBeginPosition(2);
          }

          // DjaCircle garePrec=(DjaCircle)ecluse.getBeginConnections()[0].getBeginObject();
          ecluse.setX(_GareEnCours.getX() - 120);
          ecluse.setY(_GareEnCours.getY() - 80);
          garePrec.setX(_GareEnCours.getX() - 200);
          garePrec.setY(_GareEnCours.getY());

          final DjaDiamond ecluse2 = (DjaDiamond) it.next();
          final DjaDirectArrow dd = (DjaDirectArrow) it.next();// entrant
          final DjaDirectArrow dd2 = (DjaDirectArrow) it.next();// dsortant

          dd2.setEndPosition(7);
          // ecluse.getEndConnections()[0].setEndPosition(6);

          dd.setBeginPosition(3);
          dd2.setEndPosition(5);

          ecluse2.setX(_GareEnCours.getX() - 120);
          ecluse2.setY(_GareEnCours.getY() + 80);

          if (_listeGaresATraiter.contains(_GareEnCours)) {
            grid_.add(_GareEnCours);
            _listeGaresATraiter.remove(_GareEnCours);
            System.out.println("ajout gare : " + _GareEnCours.getText());
          }
          if (_listeEclusesATraiter.contains(ecluse)) {
            grid_.add(ecluse);
            System.out.println("ajout ecluse : " + ecluse.getText(0));
            grid_.add(d2);
            grid_.add(d);// **
            _listeEclusesATraiter.remove(ecluse);
          }
          if (_listeEclusesATraiter.contains(ecluse2)) {
            grid_.add(ecluse2);
            grid_.add(dd2);
            grid_.add(dd);// **
            _listeEclusesATraiter.remove(ecluse2);
          }

          if (_listeGaresATraiter.contains(garePrec)) {
            dispositionListeObjetRecursive(_listeGaresATraiter, _listeEclusesATraiter, _listeBiefsATraiter, garePrec);
          }

        }
        if (listeAmontBief.size() == 1 && listeAmontEcluse.size() == 1) {// cas exceptionnel je pense ; non fait encore
        }

      } else if (tailleListeAmont == 3) {// logiquement 3 biefs ou 3 �cluses

        System.out.println("taille de la liste en amont =3");
        // on place l'element en arriere au point 6
        if (listeAmontBief.size() == 3) {
          it = listeAmontBief.listIterator();
          final DjaArcArrow bief = (DjaArcArrow) it.next();
          bief.setEndPosition(7);
          final DjaCircle garePrec = (DjaCircle) bief.getBeginObject();
          rechercheAmontAval(garePrec, listeAmontBief2, listeAvalBief2, listeAmontEcluse2, listeAvalEcluse2,
              listeEcluses_, listeBiefs_);
          garePrec.setX(_GareEnCours.getX() - 80);
          garePrec.setY(_GareEnCours.getY() - 80);// sert a rien je sais pu pkoi

          if ((listeAvalBief2.size() + listeAvalEcluse2.size()) > 1) {
            // garePrec.setY(_GareEnCours.getY()-80);
            bief.setBeginPosition(1);
          }

          else {
            // garePrec.setY(_GareEnCours.getY());
            bief.setBeginPosition(2);
          }

          final DjaArcArrow bief2 = (DjaArcArrow) it.next();
          bief2.setEndPosition(6);
          final DjaCircle garePrec2 = (DjaCircle) bief2.getBeginObject();
          rechercheAmontAval(garePrec2, listeAmontBief2, listeAvalBief2, listeAmontEcluse2, listeAvalEcluse2,
              listeEcluses_, listeBiefs_);
          garePrec2.setX(_GareEnCours.getX() - 80);
          garePrec2.setY(_GareEnCours.getY());
          /*
           * if((listeAvalBief2.size()+listeAvalEcluse2.size())>1) garePrec.setY(_GareEnCours.getY()+80); else
           * garePrec.setY(_GareEnCours.getY());
           */

          if ((listeAvalBief2.size() + listeAvalEcluse2.size()) > 1) {
            // garePrec2.setY(_GareEnCours.getY()+80);
            bief2.setBeginPosition(1);
          }

          else {
            // garePrec2.setY(_GareEnCours.getY());
            bief2.setBeginPosition(2);
          }

          final DjaArcArrow bief3 = (DjaArcArrow) it.next();
          bief3.setEndPosition(5);
          final DjaCircle garePrec3 = (DjaCircle) bief3.getBeginObject();
          rechercheAmontAval(garePrec3, listeAmontBief2, listeAvalBief2, listeAmontEcluse2, listeAvalEcluse2,
              listeEcluses_, listeBiefs_);
          garePrec3.setX(_GareEnCours.getX() - 80);
          garePrec3.setY(_GareEnCours.getY() + 80);
          /*
           * if((listeAvalBief2.size()+listeAvalEcluse2.size())>1) garePrec.setY(_GareEnCours.getY()+80); else
           * garePrec.setY(_GareEnCours.getY());
           */

          if ((listeAvalBief2.size() + listeAvalEcluse2.size()) > 1) {
            // garePrec2.setY(_GareEnCours.getY()+80);
            bief2.setBeginPosition(1);
          }

          else {
            // garePrec2.setY(_GareEnCours.getY());
            bief2.setBeginPosition(2);
          }

          if (_listeGaresATraiter.contains(_GareEnCours)) {
            grid_.add(_GareEnCours);
            _listeGaresATraiter.remove(_GareEnCours);
          }
          if (_listeBiefsATraiter.contains(bief)) {
            grid_.add(bief);
            _listeBiefsATraiter.remove(bief);
          }
          if (_listeBiefsATraiter.contains(bief2)) {
            grid_.add(bief2);
            _listeBiefsATraiter.remove(bief2);
          }
          if (_listeBiefsATraiter.contains(bief3)) {
            _listeBiefsATraiter.remove(bief3);
            grid_.add(bief3);
          }

          if (_listeGaresATraiter.contains(garePrec)) {
            dispositionListeObjetRecursive(_listeGaresATraiter, _listeEclusesATraiter, _listeBiefsATraiter, garePrec);
          }
          if (_listeGaresATraiter.contains(garePrec2)) {
            dispositionListeObjetRecursive(_listeGaresATraiter, _listeEclusesATraiter, _listeBiefsATraiter, garePrec2);
          }
          if (_listeGaresATraiter.contains(garePrec3)) {
            dispositionListeObjetRecursive(_listeGaresATraiter, _listeEclusesATraiter, _listeBiefsATraiter, garePrec3);
          }
        }
        if (listeAmontEcluse.size() / 3 == 3) {
          System.out.println("amont �cluse 33333333333333333333");
          it = listeAmontEcluse.listIterator();
          final DjaDiamond ecluse = (DjaDiamond) it.next();
          final DjaDirectArrow d = (DjaDirectArrow) it.next();// entrant
          final DjaDirectArrow d2 = (DjaDirectArrow) it.next();// dsortant

          d2.setEndPosition(7);
          // ecluse.getEndConnections()[0].setEndPosition(6);

          final DjaCircle garePrec = (DjaCircle) d.getBeginObject();
          rechercheAmontAval(garePrec, listeAmontBief2, listeAvalBief2, listeAmontEcluse2, listeAvalEcluse2,
              listeEcluses_, listeBiefs_);
          if ((listeAvalBief2.size() + listeAvalEcluse2.size()) > 1) {
            // garePrec.setY(_GareEnCours.getY()+80);
            d.setBeginPosition(1);
          }

          else {
            // garePrec.setY(_GareEnCours.getY());
            d.setBeginPosition(2);
          }

          // DjaCircle garePrec=(DjaCircle)ecluse.getBeginConnections()[0].getBeginObject();
          ecluse.setX(_GareEnCours.getX() - 120);
          ecluse.setY(_GareEnCours.getY() - 80);
          garePrec.setX(_GareEnCours.getX() - 200);
          garePrec.setY(_GareEnCours.getY());

          final DjaDiamond ecluse2 = (DjaDiamond) it.next();
          final DjaDirectArrow dd = (DjaDirectArrow) it.next();// entrant
          final DjaDirectArrow dd2 = (DjaDirectArrow) it.next();// dsortant

          // ecluse.getEndConnections()[0].setEndPosition(6);

          dd.setBeginPosition(2);
          dd2.setEndPosition(6);

          ecluse2.setX(_GareEnCours.getX() - 120);
          ecluse2.setY(_GareEnCours.getY());

          final DjaDiamond ecluse3 = (DjaDiamond) it.next();
          final DjaDirectArrow ddd = (DjaDirectArrow) it.next();// entrant
          final DjaDirectArrow ddd2 = (DjaDirectArrow) it.next();// dsortant

          // ecluse.getEndConnections()[0].setEndPosition(6);

          ddd.setBeginPosition(3);
          ddd2.setEndPosition(5);

          ecluse3.setX(_GareEnCours.getX() - 120);
          ecluse3.setY(_GareEnCours.getY() + 80);

          if (_listeGaresATraiter.contains(_GareEnCours)) {
            System.out.println("ajout gare " + _GareEnCours.getX() + "-" + _GareEnCours.getY());
            grid_.add(_GareEnCours);
            _listeGaresATraiter.remove(_GareEnCours);
          }
          if (_listeEclusesATraiter.contains(ecluse)) {
            System.out.println("ajout ecluse " + ecluse.getX() + "-" + ecluse.getY());
            grid_.add(ecluse);
            grid_.add(d2);
            grid_.add(d);// **
            _listeEclusesATraiter.remove(ecluse);
          }
          if (_listeEclusesATraiter.contains(ecluse2)) {
            System.out.println("ajout ecluse2 " + ecluse2.getX() + "-" + ecluse2.getY());
            grid_.add(ecluse2);
            grid_.add(dd2);
            grid_.add(dd);// **
            _listeEclusesATraiter.remove(ecluse2);
          }
          if (_listeEclusesATraiter.contains(ecluse3)) {
            System.out.println("ajout ecluse3 " + ecluse3.getX() + "-" + ecluse3.getY());
            grid_.add(ecluse3);
            grid_.add(ddd2);
            grid_.add(ddd);// **
            _listeEclusesATraiter.remove(ecluse3);
          }

          if (_listeGaresATraiter.contains(garePrec)) {
            dispositionListeObjetRecursive(_listeGaresATraiter, _listeEclusesATraiter, _listeBiefsATraiter, garePrec);
          }

        }
        if (listeAmontBief.size() == 1 && listeAmontEcluse.size() == 2) {// cas exceptionnel je pense ; non fait encore

        }
        if (listeAmontBief.size() == 2 && listeAmontEcluse.size() == 1) {// cas exceptionnel je pense ; non fait encore
        }

      } else {
        System.out.println("Ca fait beaucoup tout de m�me >3");// il faudra peut etre le gfaire pour montrer les
                                                                // erreurs de lutilisateur
      }

      // traitement de la liste en aval

      /*
       * if(listeAvalBief!=null) tailleListeAval=listeAvalBief.size(); if(listeAvalEcluse!=null)
       * tailleListeAval+=listeAvalEcluse.size()/2;
       */
      /*****************************************************************************************************************
       * if(tailleListeAval==0){ System.out.println("taille de la liste en aval = 0 --------------> on fait rien");
       * if(!_listeGaresATraiter.contains(_GareEnCours)){ grid_.add(_GareEnCours);
       * _listeGaresATraiter.remove(_GareEnCours); } }
       ****************************************************************************************************************/

      if (tailleListeAval == 1) {
        System.out.println("taille de la liste en aval =1");
        // on place l'element en arriere au point 6
        if (listeAvalBief.size() == 1) {
          // aEntrer les positions
          it = listeAvalBief.listIterator();
          final DjaArcArrow bief = (DjaArcArrow) it.next();
          bief.setBeginPosition(2);
          final DjaCircle gareSuiv = (DjaCircle) bief.getEndObject();
          rechercheAmontAval(gareSuiv, listeAmontBief2, listeAvalBief2, listeAmontEcluse2, listeAvalEcluse2,
              listeEcluses_, listeBiefs_);
          gareSuiv.setX(_GareEnCours.getX() + 80);
          if ((listeAmontBief2.size() + listeAmontEcluse2.size()) > 1) {
            gareSuiv.setY(_GareEnCours.getY() + 80);
            bief.setEndPosition(7);
          }

          else {
            gareSuiv.setY(_GareEnCours.getY());
            bief.setEndPosition(6);
          }
          System.out.println("AVANNNNNNNNNNNNNNT AJOUT");

          if (_listeGaresATraiter.contains(_GareEnCours)) {

            grid_.add(_GareEnCours);
            System.out.println("ajout gare : " + _GareEnCours.getText());
            System.out.println("ajout gare0 : " + _GareEnCours.getText(0));

            _listeGaresATraiter.remove(_GareEnCours);
          }

          if (_listeBiefsATraiter.contains(bief)) {
            grid_.add(bief);
            _listeBiefsATraiter.remove(bief);

          }
          if (_listeGaresATraiter.contains(gareSuiv)) {
            dispositionListeObjetRecursive(_listeGaresATraiter, _listeEclusesATraiter, _listeBiefsATraiter, gareSuiv);
          }
        } else {// /listeAvalEcluse==1
          it = listeAvalEcluse.listIterator();
          final DjaDiamond ecluse = (DjaDiamond) it.next();
          final DjaDirectArrow d = (DjaDirectArrow) it.next();// entrant
          final DjaDirectArrow d2 = (DjaDirectArrow) it.next();// dsortant

          // d2.setBeginPosition(2);
          d.setBeginPosition(2);
          // ecluse.getEndConnections()[0].setEndPosition(6);

          final DjaCircle gareSuiv = (DjaCircle) d2.getEndObject();
          d2.setEndPosition(6);
          // DjaCircle garePrec=(DjaCircle)ecluse.getBeginConnections()[0].getBeginObject();
          ecluse.setX(_GareEnCours.getX() + 80);
          ecluse.setY(_GareEnCours.getY());
          gareSuiv.setX(_GareEnCours.getX() + 200);
          gareSuiv.setY(_GareEnCours.getY());
          System.out.println("ajout de gare");
          if (_listeGaresATraiter.contains(_GareEnCours)) {
            grid_.add(_GareEnCours);
            _listeGaresATraiter.remove(_GareEnCours);
          }
          if (_listeEclusesATraiter.contains(ecluse)) {
            grid_.add(ecluse);
            grid_.add(d2);
            grid_.add(d);// **
            _listeEclusesATraiter.remove(ecluse);
          }
          if (_listeGaresATraiter.contains(gareSuiv)) {
            dispositionListeObjetRecursive(_listeGaresATraiter, _listeEclusesATraiter, _listeBiefsATraiter, gareSuiv);
          }
        }

      } else if (tailleListeAval == 2) {

        System.out.println("taille de la liste en aval =2");
        // on place l'element en arriere au point 6
        if (listeAvalBief.size() == 2) {
          it = listeAvalBief.listIterator();
          final DjaArcArrow bief = (DjaArcArrow) it.next();
          bief.setBeginPosition(1);
          final DjaCircle gareSuiv = (DjaCircle) bief.getEndObject();
          rechercheAmontAval(gareSuiv, listeAmontBief2, listeAvalBief2, listeAmontEcluse2, listeAvalEcluse2,
              listeEcluses_, listeBiefs_);
          gareSuiv.setX(_GareEnCours.getX() + 80);
          gareSuiv.setY(_GareEnCours.getY() - 80);

          if ((listeAvalBief2.size() + listeAvalEcluse2.size()) > 1) {
            // garePrec.setY(_GareEnCours.getY()+80);
            bief.setBeginPosition(7);
          }

          else {
            // garePrec.setY(_GareEnCours.getY());
            bief.setBeginPosition(6);
          }

          final DjaArcArrow bief2 = (DjaArcArrow) it.next();
          bief2.setBeginPosition(3);
          final DjaCircle gareSuiv2 = (DjaCircle) bief2.getEndObject();
          rechercheAmontAval(gareSuiv2, listeAmontBief2, listeAvalBief2, listeAmontEcluse2, listeAvalEcluse2,
              listeEcluses_, listeBiefs_);
          gareSuiv2.setX(_GareEnCours.getX() + 80);
          gareSuiv2.setY(_GareEnCours.getY() + 80);
          /*
           * if((listeAvalBief2.size()+listeAvalEcluse2.size())>1) garePrec.setY(_GareEnCours.getY()+80); else
           * garePrec.setY(_GareEnCours.getY());
           */

          if ((listeAmontBief2.size() + listeAmontEcluse2.size()) > 1) {
            gareSuiv2.setY(_GareEnCours.getY() + 80);
            bief2.setEndPosition(1);
          }

          else {
            gareSuiv2.setY(_GareEnCours.getY());
            bief2.setEndPosition(2);
          }

          if (_listeGaresATraiter.contains(_GareEnCours)) {
            grid_.add(_GareEnCours);
            _listeGaresATraiter.remove(_GareEnCours);
          }
          if (_listeBiefsATraiter.contains(bief)) {
            grid_.add(bief);
            _listeBiefsATraiter.remove(bief);
          }
          if (_listeBiefsATraiter.contains(bief2)) {
            grid_.add(bief2);
            _listeBiefsATraiter.remove(bief2);
          }

          if (_listeGaresATraiter.contains(gareSuiv)) {
            dispositionListeObjetRecursive(_listeGaresATraiter, _listeEclusesATraiter, _listeBiefsATraiter, gareSuiv);
          }
          if (_listeGaresATraiter.contains(gareSuiv2)) {
            dispositionListeObjetRecursive(_listeGaresATraiter, _listeEclusesATraiter, _listeBiefsATraiter, gareSuiv2);
          }
        }
        if (listeAvalEcluse.size() / 3 == 2) {
          it = listeAvalEcluse.listIterator();
          final DjaDiamond ecluse = (DjaDiamond) it.next();
          final DjaDirectArrow d = (DjaDirectArrow) it.next();// entrant
          final DjaDirectArrow d2 = (DjaDirectArrow) it.next();// dsortant

          d2.setEndPosition(7);
          d.setBeginPosition(1);
          // ecluse.getEndConnections()[0].setEndPosition(6);

          final DjaCircle gareSuiv = (DjaCircle) d2.getEndObject();
          rechercheAmontAval(gareSuiv, listeAmontBief2, listeAvalBief2, listeAmontEcluse2, listeAvalEcluse2,
              listeEcluses_, listeBiefs_);
          /*
           * if((listeAvalBief2.size()+listeAvalEcluse2.size())>1){ garePrec.setY(_GareEnCours.getY()+80);
           * d.setBeginPosition(1); } else{ garePrec.setY(_GareEnCours.getY()); d.setBeginPosition(2); }
           */

          // DjaCircle garePrec=(DjaCircle)ecluse.getBeginConnections()[0].getBeginObject();
          ecluse.setX(_GareEnCours.getX() + 120);
          ecluse.setY(_GareEnCours.getY() - 80);
          gareSuiv.setX(_GareEnCours.getX() + 200);
          gareSuiv.setY(_GareEnCours.getY());

          final DjaDiamond ecluse2 = (DjaDiamond) it.next();
          final DjaDirectArrow dd = (DjaDirectArrow) it.next();// entrant
          final DjaDirectArrow dd2 = (DjaDirectArrow) it.next();// dsortant

          dd.setBeginPosition(3);
          dd2.setEndPosition(5);

          ecluse2.setX(_GareEnCours.getX() + 120);
          ecluse2.setY(_GareEnCours.getY() + 80);
          System.out.println("avant jaout gare");
          if (_listeGaresATraiter.contains(_GareEnCours)) {
            grid_.add(_GareEnCours);
            System.out.println("apres jaout gare");
            _listeGaresATraiter.remove(_GareEnCours);
          }
          if (_listeEclusesATraiter.contains(ecluse)) {
            grid_.add(ecluse);
            grid_.add(d2);
            grid_.add(d);// **
            _listeEclusesATraiter.remove(ecluse);
          }
          if (_listeEclusesATraiter.contains(ecluse2)) {
            grid_.add(ecluse2);
            grid_.add(dd2);
            grid_.add(dd);// **
            _listeEclusesATraiter.remove(ecluse2);

          }

          if (_listeGaresATraiter.contains(gareSuiv)) {
            dispositionListeObjetRecursive(_listeGaresATraiter, _listeEclusesATraiter, _listeBiefsATraiter, gareSuiv);
          }

        }
        if (listeAvalBief.size() == 1 && listeAvalEcluse.size() == 1) {// cas exceptionnel je pense ; non fait encore
        }

      } else if (tailleListeAval == 3) {// logiquement 3 biefs ou 3 �cluses

        System.out.println("taille de la liste en aval =3");
        // on place l'element en arriere au point 6
        if (listeAvalBief.size() == 3) {
          it = listeAvalBief.listIterator();
          final DjaArcArrow bief = (DjaArcArrow) it.next();
          bief.setBeginPosition(1);
          final DjaCircle gareSuiv = (DjaCircle) bief.getEndObject();
          rechercheAmontAval(gareSuiv, listeAmontBief2, listeAvalBief2, listeAmontEcluse2, listeAvalEcluse2,
              listeEcluses_, listeBiefs_);
          gareSuiv.setX(_GareEnCours.getX() - 80);
          gareSuiv.setY(_GareEnCours.getY() - 80);// sert a rien je sais pu pkoi

          if ((listeAmontBief2.size() + listeAmontEcluse2.size()) > 1) {
            // garePrec.setY(_GareEnCours.getY()-80);
            bief.setEndPosition(7);
          }

          else {
            // garePrec.setY(_GareEnCours.getY());
            bief.setEndPosition(6);
          }

          final DjaArcArrow bief2 = (DjaArcArrow) it.next();
          bief2.setBeginPosition(2);
          final DjaCircle gareSuiv2 = (DjaCircle) bief2.getEndObject();
          rechercheAmontAval(gareSuiv2, listeAmontBief2, listeAvalBief2, listeAmontEcluse2, listeAvalEcluse2,
              listeEcluses_, listeBiefs_);
          gareSuiv2.setX(_GareEnCours.getX() + 80);
          gareSuiv2.setY(_GareEnCours.getY());
          /*
           * if((listeAvalBief2.size()+listeAvalEcluse2.size())>1) garePrec.setY(_GareEnCours.getY()+80); else
           * garePrec.setY(_GareEnCours.getY());
           */

          /*
           * if((listeAmontBief2.size()+listeAmontEcluse2.size())>1){ //garePrec2.setY(_GareEnCours.getY()+80);
           * bief2.setEndPosition(7); } else{
           */
          // garePrec2.setY(_GareEnCours.getY());
          bief2.setEndPosition(6);
          // }

          final DjaArcArrow bief3 = (DjaArcArrow) it.next();
          bief3.setBeginPosition(3);
          final DjaCircle gareSuiv3 = (DjaCircle) bief3.getEndObject();
          rechercheAmontAval(gareSuiv3, listeAmontBief2, listeAvalBief2, listeAmontEcluse2, listeAvalEcluse2,
              listeEcluses_, listeBiefs_);
          gareSuiv3.setX(_GareEnCours.getX() + 80);
          gareSuiv3.setY(_GareEnCours.getY() + 80);
          /*
           * if((listeAvalBief2.size()+listeAvalEcluse2.size())>1) garePrec.setY(_GareEnCours.getY()+80); else
           * garePrec.setY(_GareEnCours.getY());
           */

          /*
           * if((listeAvalBief2.size()+listeAvalEcluse2.size())>1){ //garePrec2.setY(_GareEnCours.getY()+80);
           */
          bief3.setEndPosition(5);
          /*
           * } else{ //garePrec2.setY(_GareEnCours.getY()); bief2.setEndPosition(6); }
           */

          if (_listeGaresATraiter.contains(_GareEnCours)) {
            grid_.add(_GareEnCours);
            _listeGaresATraiter.remove(_GareEnCours);
          }
          if (_listeBiefsATraiter.contains(bief)) {
            grid_.add(bief);
            _listeBiefsATraiter.remove(bief);
          }
          if (_listeBiefsATraiter.contains(bief2)) {
            grid_.add(bief2);
            _listeBiefsATraiter.remove(bief2);
          }
          if (_listeBiefsATraiter.contains(bief3)) {
            grid_.add(bief3);
            _listeBiefsATraiter.remove(bief3);
          }

          if (_listeGaresATraiter.contains(gareSuiv)) {
            dispositionListeObjetRecursive(_listeGaresATraiter, _listeEclusesATraiter, _listeBiefsATraiter, gareSuiv);
          }
          if (_listeGaresATraiter.contains(gareSuiv2)) {
            dispositionListeObjetRecursive(_listeGaresATraiter, _listeEclusesATraiter, _listeBiefsATraiter, gareSuiv2);
          }
          if (_listeGaresATraiter.contains(gareSuiv3)) {
            dispositionListeObjetRecursive(_listeGaresATraiter, _listeEclusesATraiter, _listeBiefsATraiter, gareSuiv3);
          }
        }
        if (listeAvalEcluse.size() / 3 == 3) {
          it = listeAvalEcluse.listIterator();
          final DjaDiamond ecluse = (DjaDiamond) it.next();
          final DjaDirectArrow d = (DjaDirectArrow) it.next();// entrant
          final DjaDirectArrow d2 = (DjaDirectArrow) it.next();// dsortant

          d2.setEndPosition(7);
          // ecluse.getEndConnections()[0].setEndPosition(6);
          d.setBeginPosition(1);
          final DjaCircle gareSuiv = (DjaCircle) d2.getEndObject();
          rechercheAmontAval(gareSuiv, listeAmontBief2, listeAvalBief2, listeAmontEcluse2, listeAvalEcluse2,
              listeEcluses_, listeBiefs_);
          /*
           * if((listeAvalBief2.size()+listeAvalEcluse2.size())>1){ // garePrec.setY(_GareEnCours.getY()+80);
           * d.setBeginPosition(1); } else{ // garePrec.setY(_GareEnCours.getY()); d.setBeginPosition(2); }
           */

          // DjaCircle garePrec=(DjaCircle)ecluse.getBeginConnections()[0].getBeginObject();
          ecluse.setX(_GareEnCours.getX() + 120);
          ecluse.setY(_GareEnCours.getY() - 80);
          gareSuiv.setX(_GareEnCours.getX() + 200);
          gareSuiv.setY(_GareEnCours.getY());

          final DjaDiamond ecluse2 = (DjaDiamond) it.next();
          final DjaDirectArrow dd = (DjaDirectArrow) it.next();// entrant
          final DjaDirectArrow dd2 = (DjaDirectArrow) it.next();// dsortant

          // ecluse.getEndConnections()[0].setEndPosition(6);

          dd.setBeginPosition(2);
          dd2.setEndPosition(6);

          ecluse2.setX(_GareEnCours.getX() + 120);
          ecluse2.setY(_GareEnCours.getY());

          final DjaDiamond ecluse3 = (DjaDiamond) it.next();
          final DjaDirectArrow ddd = (DjaDirectArrow) it.next();// entrant
          final DjaDirectArrow ddd2 = (DjaDirectArrow) it.next();// dsortant

          // ecluse.getEndConnections()[0].setEndPosition(6);

          ddd.setBeginPosition(3);
          ddd2.setEndPosition(5);

          ecluse3.setX(_GareEnCours.getX() + 120);
          ecluse3.setY(_GareEnCours.getY() + 80);

          if (_listeGaresATraiter.contains(_GareEnCours)) {
            System.out.println("ajout gare aval 3");
            grid_.add(_GareEnCours);
            _listeGaresATraiter.remove(_GareEnCours);
          }
          if (_listeEclusesATraiter.contains(ecluse)) {
            System.out.println("ajout ecluse aval 3");
            grid_.add(ecluse);
            grid_.add(d2);
            grid_.add(d);// **
            _listeEclusesATraiter.remove(ecluse);
          }
          if (_listeEclusesATraiter.contains(ecluse2)) {
            System.out.println("ajout ecluse 2 aval 3");
            grid_.add(ecluse2);
            grid_.add(dd2);
            grid_.add(dd);// **
            _listeEclusesATraiter.remove(ecluse2);
          }
          if (_listeEclusesATraiter.contains(ecluse3)) {
            System.out.println("ajout ecluse3 aval 3");
            grid_.add(ecluse3);
            grid_.add(ddd2);
            grid_.add(ddd);// **
            _listeEclusesATraiter.remove(ecluse3);
          }

          if (_listeGaresATraiter.contains(gareSuiv)) {
            dispositionListeObjetRecursive(_listeGaresATraiter, _listeEclusesATraiter, _listeBiefsATraiter, gareSuiv);
          }

        }
        if (listeAvalBief.size() == 1 && listeAvalEcluse.size() == 2) {// cas exceptionnel je pense ; non fait encore
        }
        if (listeAvalBief.size() == 2 && listeAvalEcluse.size() == 1) {// cas exceptionnel je pense ; non fait encore
        }

      } else {
        System.out.println("Ca fait beaucoup tout de m�me >3 ou tr�s peu 0");//
      }

    } else {
      System.out.println("Les objets sont dispos�s");
      // PSSSSS : il faudra rajouter les cas exceptionnels pour montrer les erreurs comises par l'utilisateur
    }
  }

  private void rechercheAmontAval(final DjaCircle _gareCourante, final ArrayList _listeAmontBief,
      final ArrayList _listeAvalBief, final ArrayList _listeAmontEcluse, final ArrayList _listeAvalEcluse,
      final ArrayList _listeEcluses, final ArrayList _listeBiefs) {
    System.out.println("recherche amont aval");
    final ListIterator le = _listeEcluses.listIterator();
    final ListIterator lb = _listeBiefs.listIterator();
    /*******************************************************************************************************************
     * _listeAmontBief=new ArrayList(); _listeAvalBief=new ArrayList(); _listeAmontEcluse=new ArrayList();
     * _listeAvalEcluse=new ArrayList();
     ******************************************************************************************************************/
    /*
     * _listeAmontBief=null; _listeAvalBief=null; _listeAmontEcluse=null; _listeAvalEcluse=null;
     */

    // liste des �cluses
    while (le.hasNext()) {
      final DjaDiamond eDja = (DjaDiamond) le.next();
      final DjaDirectArrow d = (DjaDirectArrow) le.next();// dentrant
      final DjaDirectArrow d2 = (DjaDirectArrow) le.next();// dsortant
      // SParametresEcluse eObj=(SParametresEcluse) eDja.getData("ecluseKey");
      /*****************************************************************************************************************
       * if(eDja.getEndConnections()[0].getEndObject().equals(_gareCourante)){ _listeAmontEcluse.add(eDja); }
       ****************************************************************************************************************/
      if (d2.getEndObject().equals(_gareCourante)) {
        System.out.println("d2 egal gare amon");
        _listeAmontEcluse.add(eDja);
        _listeAmontEcluse.add(d);
        _listeAmontEcluse.add(d2);
        System.out.println("taille amont " + _listeAmontEcluse.size());
      }

      /*****************************************************************************************************************
       * if(eDja.getBeginConnections()[0].getBeginObject()==_gareCourante){ _listeAvalEcluse.add(eDja); }
       ****************************************************************************************************************/
      if (d.getBeginObject().equals(_gareCourante)) {
        System.out.println("d egal gare aval");
        _listeAvalEcluse.add(eDja);
        _listeAvalEcluse.add(d);
        _listeAvalEcluse.add(d2);
        System.out.println("taille amont " + _listeAmontEcluse.size());

      }
    }
    // liste des biefs
    while (lb.hasNext()) {
      final DjaArcArrow aDja = (DjaArcArrow) lb.next();
      // System.out.println("longueu " +aDja.getEndObject().getEndConnections().length);
      // if(aDja.getEndConnections()[0].getEndObject().equals(_gareCourante)){
      if (aDja.getEndObject().equals(_gareCourante)) {
        // _listeAmontBief.add(aDja.getBeginObject());//GARE PRECEDENTE DU BIEF
        _listeAmontBief.add(aDja);// ARC

      }
      if (aDja.getBeginObject().equals(_gareCourante)) {
        // if(aDja.getBeginConnections()[0].getBeginObject()==_gareCourante){
        _listeAvalBief.add(aDja);
      }

    }

  }

  private void creationListeObjet(final ArrayList _listeBiefs, final ArrayList _listeEcluses_,
      final ArrayList _listeGares, final ArrayList _listeTrajets) {
    // creation de la liste des gares
    ListIterator it = _listeGares.listIterator();
    String s;
    while (it.hasNext()) {
      s = (it.next().toString());
      final DjaCircle g = new DjaCircle(s);
      g.setHeight(40);
      g.putData("gareKey", s);
      // grid_.add(g);
      listeGares_.add(g);
    }
    it = _listeEcluses_.listIterator();
    while (it.hasNext()) {
      final SParametresEcluse e = (SParametresEcluse) it.next();
      s = e.identification;
      final DjaDiamond g = new DjaDiamond(s);
      g.putData("ecluseKey", e);
      final DjaDirectArrow d = new DjaDirectArrow();
      d.setBeginObject(rechercheDjaGare("" + e.gareEnAmont));

      d.setEndObject(g);
      d.setEndPosition(6);

      final DjaDirectArrow d2 = new DjaDirectArrow();
      d2.setBeginObject(g);
      d2.setBeginPosition(2);
      d2.setEndObject(rechercheDjaGare("" + e.gareEnAval));
      // grid_.add(g);

      // grid_.add(d);
      // grid_.add(d2);
      listeEcluses_.add(g);
      listeEcluses_.add(d);
      listeEcluses_.add(d2);
    }
    it = _listeBiefs.listIterator();
    while (it.hasNext()) {
      final SParametresBief b = (SParametresBief) it.next();
      s = b.identification;
      final DjaArcArrow g = new DjaArcArrow(s);
      g.putData("biefKey", b);
      g.setBeginObject(rechercheDjaGare("" + b.gareEnAmont));
      // g.setBegin(rechercheDjaGare(""+b.gareEnAmont),3,3 );
      g.setEndObject(rechercheDjaGare("" + b.gareEnAval));
      // grid_.add(g);
      listeBiefs_.add(g);
    }
  }

  private DjaCircle rechercheDjaGare(final String s) {
    final ListIterator l = listeGares_.listIterator();
    boolean trouve = false;
    DjaCircle s2 = null;
    while (l.hasNext() && !trouve) {
      s2 = (DjaCircle) l.next();
      if (s.equals(s2.getData("gareKey"))) {
        trouve = true;

      }

    }
    return s2;
  }

  private int rechercheIndiceGare(final String s) {
    final ListIterator l = listeG_.listIterator();
    boolean trouve = false;
    String s2 = "";
    int x = -1;
    while (l.hasNext() && !trouve) {
      s2 = (l.next().toString());
      if (s.equals(s2)) {
        trouve = true;
        x = l.nextIndex() - 1;
      }

    }
    return x;
  }

  public void mouseClicked(final MouseEvent _e) {
    final int x = _e.getX();
    final int y = _e.getY();
    final int bouton = _e.getButton();
    switch (bouton) {
    case 1:
      System.out.println("bouton 1 coordonn�es : " + x + CtuluLibString.VIR + y);
      break;
    case 2:
      System.out.println("bouton 2 coordonn�es : " + x + CtuluLibString.VIR + y);
      break;
    case 3:
      System.out.println("bouton 3 coordonn�es : " + x + CtuluLibString.VIR + y);
      break;
    }

  }

  public void mouseEntered(final MouseEvent _e) {

  }

  public void mouseExited(final MouseEvent _e) {

  }

  public void mousePressed(final MouseEvent _e) {

  }

  public void mouseReleased(final MouseEvent _e) {

  }

  public void creationListeObjets(final int gareDebut, final int GareFin) {

  }

}