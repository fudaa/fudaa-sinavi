package org.fudaa.fudaa.sinavi2;

import org.fudaa.ctulu.CtuluLibString;

import org.fudaa.dodico.corba.sinavi2.SParametresBateau;

/**
 * @author maneuvrier Le type Sinavi2TypeBateau a �t� cr�� afin d'encapsuler les donn�es entr�es par l'utilisateur et de
 *         les adapter au type SParametresBateau, structure corba utilis�e pour la communication Fortran
 */

class Sinavi2TypeBateau {

  public Sinavi2TypeBateau() {}

  /**
   * @param _id ; identifiant du type de bateau non null, il se termine par C pour Charge (avec marchandises) et L pour
   *          Lege (� vide)
   * @param _longueur : non null
   * @param _largeur : non null
   * @param _debNavHeure : horaire de d�but de navigation (heure) max 24
   * @param _debNavMinute : horaire de d�but de navigation (minute) max 60
   * @param _finNavHeure : horaire de fin de navigation (heure) max 24
   * @param _finNavMinute : horaire de d�but de navigation (minute) max 60
   * @param _tirantDEau : tirant d'eau en m > 0 hauteur d'enfoncement du bateau dans l'eau
   * @param _geneHeure : g�ne admissible (heure)
   * @param _geneMinute : g�ne admissible (minute) la g�ne admissible est utilis�e pour exprimer des priorit�s entre les
   *          types de bateau
   * @param _vitesseMontant : vitesse moyenne dans le sens montant : --> km/h
   * @param _vitesseDescendant : vitesse moyenne dans le sens avalant : --> km/h
   */
  public Sinavi2TypeBateau(final String _id, final double _longueur, final double _largeur, /* int hdebnav, int hfinnav */
  final int _debNavHeure, final int _debNavMinute, final int _finNavHeure, final int _finNavMinute,
      final double _tirantDEau, final int _geneHeure, final int _geneMinute, final double _vitesseMontant,
      final double _vitesseDescendant) {

    bat_ = new SParametresBateau();
    bat_.identification = _id;
    bat_.longueur = _longueur;
    bat_.largeur = _largeur;
    final int hdebnav = _debNavHeure + _debNavMinute;
    final int hfinnav = _finNavHeure + _finNavMinute;
    bat_.debutNavigation = determineHeure(hdebnav);
    bat_.finNavigation = determineHeure(hfinnav);
    bat_.tirantDeau = _tirantDEau;
    final int hgene = _geneHeure + _geneMinute;
    bat_.dureeGeneAdmissible = determineHeure(hgene);
    arrondir(bat_.debutNavigation, 2);// inutil
    arrondir(bat_.finNavigation, 2);// inutil
    arrondir(bat_.dureeGeneAdmissible, 2); // inutil
    // ---------------------------------------

    bat_.vitesseMontantParDefaut = _vitesseMontant / 1000;
    bat_.vitesseDescendantParDefaut = _vitesseDescendant / 1000;

  }

  /**
   * retourne en seconde l'heure entr�e en param�tre en oubliant les minutes.
   * 
   * @param _heure : de la forme hh,mm
   * @return l'heure en seconde
   */
  public static int determineHeureSeule(final double _heure) {
    final int m = (int) (_heure * 100);
    return m / 100 * 3600;
    //return heu;
  }

  /**
   * retourne en seconde les minutes entr�es en param�tre en oubliant les heures.
   * 
   * @param _heure : de la forme hh,mm
   * @return le nombre de minutes en seconde
   */
  public static int determineMinuteSeule(final double _heure) {
    final int m = (int) (_heure * 100);
    return  m % 100;
    //return min;
  }

  /**
   * convertit en heure minutes un nombre de secondes : hh.mm.
   * 
   * @param _nbSecondes
   * @return en heure minute
   */
  public static double determineHeure(final int _nbSecondes) {
    final int heure = _nbSecondes / 3600;
    final int minute = _nbSecondes / 60 - heure * 60;
    String s;
    if (minute > 9) {
      s = new String(heure + CtuluLibString.DOT + minute);
    } else {
      s = new String(heure + ".0" + minute);
    }
    final Double h = new Double(s);
    return h.doubleValue();
  }

  /**
   * convertit une heure en seconde.
   * 
   * @param _heure de la forme hh,mm
   * @return le nombre de seconde
   */
  public static int determineSeconde(final double _heure) {
    final int m = (int) (_heure * 100);
    final int min = m % 100 * 60;
    final int heu = m / 100 * 3600;
    return heu + min;
  }

  // accesseurs
  public String getIdentification() {
    return bat_.identification;
  }

  public double getLongueur() {
    return bat_.longueur;
  }

  public double getLargeur() {
    return bat_.largeur;
  }

  public double getDebutNavigation() {
    return bat_.debutNavigation;
  }

  public double getFinNavigation() {
    return bat_.finNavigation;
  }

  public double getDureeGeneAdmissible() {
    return bat_.dureeGeneAdmissible;
  }

  public double getTirantDeau() {
    return bat_.tirantDeau;
  }

  public double getVitesseMontantParDefaut() {
    return bat_.vitesseMontantParDefaut;
  }

  public double getVitesseDescendantParDefaut() {
    return bat_.vitesseDescendantParDefaut;
  }

  // modifieurs
  public void setIdentification(final String _id) {
    bat_.identification = _id;
  }

  public void setLongueur(final double _longueur) {
    bat_.longueur = _longueur;
  }

  public void setLargeur(final double _largeur) {
    bat_.largeur = _largeur;
  }

  public void setDebutNavigation(final double _h_deb_nav) {
    bat_.debutNavigation = _h_deb_nav;
  }

  public void setFinNavigation(final double _h_fin_nav) {
    bat_.finNavigation = _h_fin_nav;
  }

  public void setDureeGeneAdmissible(final double _gene) {
    bat_.dureeGeneAdmissible = _gene;
  }

  public void setTirantDeau(final double _tirant_deau) {
    bat_.tirantDeau = _tirant_deau;
  }

  public void setVitesseMontantParDefaut(final double _vitesseMontantParDefaut) {
    bat_.vitesseMontantParDefaut = _vitesseMontantParDefaut;
  }

  public void setVitesseDescendantParDefaut(final double _vitesseDescendantParDefaut) {
    bat_.vitesseDescendantParDefaut = _vitesseDescendantParDefaut;
  }

  /**
   * arrondir un nombre.
   * 
   * @param _a : nombre � arrondir
   * @param _nbChiffres : nombre de chiffres apr�s la virgule
   */

  public void arrondir(double _a, final int _nbChiffres) {
    for (int i = 0; i < _nbChiffres; i++) {
      _a *= 10;
    }
    _a = (int) (_a + .5);
    for (int i = 0; i < _nbChiffres; i++) {
      _a /= 10;
    }
  }

  /**
   * comparaison de typeBateau.
   * 
   * @param _b bateau � comparer avec this
   * @return la valeur boul�enne de la comparaison vrai si �gaux et faux dans le cas contraire
   */
  public boolean typeBateauEquals(final Sinavi2TypeBateau _b) {
    return (this.getDureeGeneAdmissible() == _b.getDureeGeneAdmissible()
        && this.getDebutNavigation() == _b.getDebutNavigation() && this.getFinNavigation() == _b.getFinNavigation()
        && this.getIdentification() == _b.getIdentification() && this.getLargeur() == _b.getLargeur()
        && this.getLongueur() == _b.getLongueur() && this.getTirantDeau() == _b.getTirantDeau());
  }

  SParametresBateau bat_; // le SParametresBateau qu'on va mettre dans la liste des bateaux
}
