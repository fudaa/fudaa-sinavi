package org.fudaa.fudaa.sinavi2;

import org.fudaa.dodico.corba.sinavi2.SResultatHistorique;

public class SinaviTypeHistorique {

  /** constructeur sans param�tres* */
  public SinaviTypeHistorique() {}

  /**
   * @param numero bateau dans la simulation
   * @param numero du type de bateau
   * @param sens de navigation
   * @param �l�m�nt � franchir
   * @param num�ro de l'�l�m�nt � franchir
   * @param heure d'entr�e dans l'�l�m�nt
   * @param heure de sortie dans l'�l�ment
   * @param attente
   */

  public SinaviTypeHistorique(final SResultatHistorique _hist) {
    this.numeroBateau_ = _hist.numeroBateau;
    this.numeroTypeBateau_ = _hist.numeroTypeBateau;
    this.avalantMontant_ = avalantMontant(_hist.avalantMontant);
    this.elementAFranchir_ = _hist.elementAFranchir;
    this.numeroElement_ = _hist.numeroElement;
    this.heureEntree_ = _hist.heureEntree;
    this.heureSortie_ = _hist.heureSortie;
    this.attente_ = _hist.attente;
  }

  /** constructeur avec param�tres* */
  public static char avalantMontant(final boolean _avalantMontant) {
    if (_avalantMontant) {
      return 'A';
    } else {
      return 'M';
    }

  }

  /** accesseurs* */
  public char getAvalantMontant() {
    return avalantMontant_;
  }

  public void setAvalantMontant(final char _avalantMontant) {
    avalantMontant_ = _avalantMontant;
  }

  public int getNumeroBateau() {
    return numeroBateau_;
  }

  public void SetNumeroBateau(final int _numeroBateau) {
    numeroBateau_ = _numeroBateau;
  }

  public int getNumeroTypeBateau() {
    return numeroTypeBateau_;
  }

  public void SetNumeroTypeBateau(final int _numeroTypeBateau) {
    numeroBateau_ = _numeroTypeBateau;
  }

  public String getElementAFranchir() {
    return elementAFranchir_;
  }

  public void SetElementAFranchir(final String _elementAFranchir) {
    elementAFranchir_ = _elementAFranchir;
  }

  public int getNumeroElement() {
    return numeroElement_;
  }

  public void SetNumeroElement(final int _numeroElement) {
    numeroElement_ = _numeroElement;
  }

  public double getHeureEntree() {
    return heureEntree_;
  }

  public void SetHeureEntree(final double _heureEntree) {
    heureEntree_ = _heureEntree;
  }

  public double getHeureSortie() {
    return heureSortie_;
  }

  public void SetHeureSortie(final double _heureSortie) {
    heureSortie_ = _heureSortie;
  }

  public double getAttente() {
    return attente_;
  }

  public void SetAttente(final double _attente) {
    attente_ = _attente;
  }

  int numeroBateau_;
  int numeroTypeBateau_;
  char avalantMontant_; // true pour avalant
  String elementAFranchir_;
  int numeroElement_;
  double heureEntree_;
  double heureSortie_;
  double attente_;

}
