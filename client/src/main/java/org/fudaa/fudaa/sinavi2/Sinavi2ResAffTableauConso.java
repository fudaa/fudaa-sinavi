package org.fudaa.fudaa.sinavi2;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.ListIterator;

import javax.swing.JComponent;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;
import javax.swing.event.InternalFrameEvent;
import javax.swing.event.InternalFrameListener;

import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuCommonImplementation;
import com.memoire.bu.BuDialogMessage;
import com.memoire.bu.BuInternalFrame;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuScrollPane;
import com.memoire.bu.BuTable;

import org.fudaa.ctulu.table.CtuluTableExcelWriter;

import org.fudaa.dodico.corba.sinavi2.SSimulationSinavi2;

/**
 * impl�mentation d'une fen�tre interne permettant d'afficher les bassin�es et les fausses bassin�es sous forme de
 * tableau.
 * 
 * @version $Revision: 1.7 $ $Date: 2006-09-19 15:08:57 $ by $Author: deniger $
 * @author Fatimatou Ka, Beno�t Maneuvrier
 */
public class Sinavi2ResAffTableauConso extends BuInternalFrame implements ActionListener, InternalFrameListener {

  private final BuLabel lTitre_ = new BuLabel("Tableau r�capitulatif de la consommation d'eau");

  private final BuButton bAnnuler_ = new BuButton(Sinavi2Resource.SINAVI2.getIcon("QUITTER"), "Quitter");

  private final BuButton bImprimer_ = new BuButton(Sinavi2Resource.SINAVI2.getIcon("IMPRIMER"), "Imprimer");

  /** * panel contenant les boutons, il est plac� en bas. */

  private final BuPanel pBoutons_ = new BuPanel();
  /**
   * panel contenant les diff�rents champs de la fen�tre.
   */
  private final BuPanel pTitre_ = new BuPanel();

  private final BuPanel pDonnees2_ = new BuPanel();

  public ArrayList listeCons_;
  public ArrayList listeCoordonnees_;
  public boolean comparaison_;
  private BuScrollPane scrollPane_;
  public Sinavi2TableauConsommation tb_;
  public BuTable table_;
  public Sinavi2Implementation imp2_;
  public ArrayList listeSimulations_;
  public int[] simulationsSel_;

  public Sinavi2ResAffTableauConso(final BuCommonImplementation _appli, final ArrayList _listeCons,
      final ArrayList _listeSimulationsSinavi2, final boolean _comparaison, final int[] _simulationsSel) {
    super("Tableau r�capitulatif de la consommation d'eau", true, true, true, false);
    comparaison_ = _comparaison;
    listeCons_ = _listeCons;
    listeSimulations_ = _listeSimulationsSinavi2;
    simulationsSel_ = _simulationsSel;
    pTitre_.add(lTitre_);
    tableauConsommation();

    table_.setSize(250, 250);
    table_.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
    table_.setRowSelectionAllowed(true);
    table_.setColumnSelectionAllowed(false);
    scrollPane_ = new BuScrollPane(table_);
    pDonnees2_.add(scrollPane_);

    table_.setAutoscrolls(true);

    imp2_ = (Sinavi2Implementation) _appli.getImplementation();

    bAnnuler_.addActionListener(this);
    bImprimer_.addActionListener(this);

    ((JComponent) getContentPane()).setBorder(new EmptyBorder(25, 25, 25, 25));

    pBoutons_.setLayout(new FlowLayout(FlowLayout.CENTER));
    pBoutons_.add(bAnnuler_);
    pBoutons_.add(bImprimer_);
    getContentPane().add(pTitre_, BorderLayout.NORTH);
    getContentPane().add(pDonnees2_, BorderLayout.CENTER);
    getContentPane().add(pBoutons_, BorderLayout.SOUTH);

    pack();

    /** *reinitialisation des valeurs* */

    setVisible(true);
    addInternalFrameListener(this);
    imp2_.addInternalFrame(this);

  }

  public void tableauConsommation() {
    int row = 0;
    if (tb_ == null) {
      tb_ = new Sinavi2TableauConsommation();
    }
    System.out.println(" taille de liste cons dans le tableau avant toute chose " + listeCons_.size());
    if (!comparaison_) {
      final ListIterator it = ((ArrayList) listeCons_.get(0)).listIterator();
      while (it.hasNext()) {
        final Sinavi2TypeConsommation cons = (Sinavi2TypeConsommation) it.next();
        row = fillTableau(row, cons.ecluse_, cons);
      }
    } else {
      for (int h = 0; h < /* imp2_.simulationsSel.length */listeCons_.size(); h++) {
        final ListIterator it = ((ArrayList) listeCons_.get(h)).listIterator();
        System.out.println("valeur de la liste iterator " + it);
        String s = null;
        while (it.hasNext()) {
          final Sinavi2TypeConsommation cons = (Sinavi2TypeConsommation) it.next();
          s = ((SSimulationSinavi2) listeSimulations_.get(simulationsSel_[h])).nomSim + " :" + cons.ecluse_;
          row = fillTableau(row, s, cons);
        }
      }

    }

    table_ = new BuTable(tb_.data_, tb_.nomCol());
    table_.repaint();
  }

  private int fillTableau(int _row, String _s, final Sinavi2TypeConsommation _cons) {
    tb_.setValueAt(_s, _row, 0);

    _s = String.valueOf(_cons.getNbBassineesMontantes());
    tb_.setValueAt(_s, _row, 1);

    _s = String.valueOf(_cons.getNbBassineesAvalantes());
    tb_.setValueAt(_s, _row, 2);

    _s = String.valueOf(_cons.getNbTotalBassinees());
    tb_.setValueAt(_s, _row, 3);

    _s = String.valueOf(_cons.getNbFaussesBassineesMontantes());
    tb_.setValueAt(_s, _row, 4);

    _s = String.valueOf(_cons.getNbFaussesBassineesAvalantes());
    tb_.setValueAt(_s, _row, 5);

    _s = String.valueOf(_cons.getNbTotalFaussesBassinees());
    tb_.setValueAt(_s, _row, 6);

    _s = String.valueOf((int) _cons.getConsommationDEau());
    tb_.setValueAt(_s, _row, 7);

    _row++;
    tb_.setNbRow(_row);
    return _row;
  }

  public void annuler() {

    imp2_.removeInternalFrame(this);
    // imp2_.ResetResAffTableauConso();
  }

  public void actionPerformed(final ActionEvent _e) {
    if (_e.getSource() == bAnnuler_) {
      annuler();

    }

    else if (_e.getSource() == bImprimer_) {

      printInFile();

    }

  }

  /**
   * on fait choisir un fichier � l'aide d'un chooser. on cr� un tableau temporaire tbtemp o� : on ajoute une ligne avec
   * le titre et du blanc pour les autres cellules de la ligne on ajoute les colonnes sur la ligne suivante on ajoute
   * enfin les donn�es on cr� le fichier excel on write le bateau
   */
  private void printInFile() {
    final File f = imp2_.enregistrerXls();
    if (f == null) {
      return;
    }
    final Sinavi2TableauConsommation tbtemp = new Sinavi2TableauConsommation();
    tbtemp.data_ = new Object[tb_.getRowCount() + 2][tb_.getColumnCount()];
    tbtemp.initNomCol(1);
    tbtemp.setColumnName("Tableau r�capitulatif de la consommation d'eau", 0);
    for (int i = 1; i < tb_.getColumnCount(); i++) {
      tbtemp.setColumnName(" ", i);
    }

    tbtemp.setNbRow(tb_.getRowCount() + 2);
    for (int i = 2; i <= (tb_.getRowCount() + 1); i++) {
      for (int j = 0; j < tb_.getColumnCount(); j++) {
        tbtemp.data_[i][j] = tb_.data_[i - 2][j];
      }

    }
    final CtuluTableExcelWriter test = new CtuluTableExcelWriter(tbtemp, f);

    try {
      test.write(null);

    } catch (final RowsExceededException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    } catch (final WriteException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    } catch (final IOException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
  }

  public void affMessage(final String _t) {
    new BuDialogMessage(imp2_.getApp(), imp2_.getInformationsSoftware(), _t).activate();
  }

  public void internalFrameActivated(final InternalFrameEvent _e) {
  // TODO Auto-generated method stub

  }

  public void internalFrameClosed(final InternalFrameEvent _e) {
    this.closable = true;

  }

  public void internalFrameDeactivated(final InternalFrameEvent _e) {
  // TODO Auto-generated method stub

  }

  public void internalFrameDeiconified(final InternalFrameEvent _e) {
  // TODO Auto-generated method stub

  }

  public void internalFrameIconified(final InternalFrameEvent _e) {
  // TODO Auto-generated method stub

  }

  public void internalFrameOpened(final InternalFrameEvent _e) {
  // TODO Auto-generated method stub

  }

  public void internalFrameClosing(final InternalFrameEvent _e) {
  // TODO Auto-generated method stub

  }

}
