/*
 * @file         Sinavi2FilleAff�Bateaux.java
 * @creation     2001-05-17
 * @modification $Date: 2007-05-04 14:01:06 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.sinavi2;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.ListIterator;

import javax.swing.JComponent;
import javax.swing.border.EmptyBorder;
import javax.swing.event.InternalFrameEvent;
import javax.swing.event.InternalFrameListener;

import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuCommonImplementation;
import com.memoire.bu.BuDialogMessage;
import com.memoire.bu.BuInternalFrame;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuScrollPane;
import com.memoire.bu.BuTable;
import com.memoire.bu.BuTableSortModel;

import org.fudaa.ctulu.table.CtuluTableExcelWriter;

import org.fudaa.dodico.corba.sinavi2.SParametresEcluse;

import org.fudaa.dodico.sinavi2.Sinavi2Helper;

/**
 * impl�mentation d'une fen�tre interne permettant d'afficher les param�tres des ecluses sous forme de tableau cf.
 * Sinavi2FilleAffEcluses
 * 
 * @version $Revision: 1.15 $ $Date: 2007-05-04 14:01:06 $ by $Author: deniger $
 * @author Fatimatou Ka, Beno�t Maneuvrier
 */
public class Sinavi2FilleAffEcluses extends BuInternalFrame implements ActionListener, InternalFrameListener {

  private static final int AUTO_RESIZE_ALL_COLUMNS = 0;

  /*********************************************************************************************************************
   * tableau***** identification longueur largeur navigation debut fin type charg� ou l�ge tirant d'eau gene admissible
   */

  // private BuTable tabBateaux_ =new BuTable();
  private final BuLabel lTitre_ = new BuLabel("Affichage des types de ecluse");

  /** bouton pour annuler */
  private final BuButton bAnnuler_ = new BuButton(Sinavi2Resource.SINAVI2.getIcon("QUITTER"), "Quitter");

  private final BuButton bDdisponibilites_ = new BuButton(Sinavi2Resource.SINAVI2.getIcon(""), "Indisponibilit�s");
  /** bouton pour annuler */
  private final BuButton bAjouter_ = new BuButton(Sinavi2Resource.SINAVI2.getIcon("AJOUTER"), "Ajouter");

  /** sauver */
  private final BuButton bModifier_ = new BuButton(Sinavi2Resource.SINAVI2.getIcon("MODIFIER"), "Modifier");

  private final BuButton bSupprimer_ = new BuButton(Sinavi2Resource.SINAVI2.getIcon("SUPPRIMER"), "Suppimer");

  private final BuButton bImprimer_ = new BuButton(Sinavi2Resource.SINAVI2.getIcon("IMPRIMER"), "Imprimer");

  private final BuButton bMiseAJour_ = new BuButton(Sinavi2Resource.SINAVI2.getIcon("MISAJOUR"), "Mise � Jour");

  /** **gerer la liste des ecluses**** */
  // ---------------------------------public LinkedList listeBateaux2_;
  public ArrayList listeEcluses2_;
  // public static int nbBateaux_;

  /** **bateau pour les champs en cas de modifications*** */
  public SParametresEcluse ecluseCourant_;
  // private int nbat_=-1;

  /** * panel contenant les boutons, il est plac� en bas */

  private final BuPanel pBoutons_ = new BuPanel();
  /**
   * panel contenant les diff�rents champs de la fen�tre
   */
  private final BuPanel pTitre_ = new BuPanel();

  private final BuPanel pDonnees2_ = new BuPanel();

  /**
   * une instance de SinaviImplementation
   */
  // private BuCommonImplementation app_;
  /**
   * Constructeur
   * 
   * @param _app une instance de SinaviImplementation
   */

  // private BuCommonImplementation _appli;
  // public JTable tabBateaux_;
  private BuScrollPane scrollPane_;
  public Sinavi2TableauEcluse tb_;
  public BuTable table_;
  public BuTableSortModel tabEcluses_;
  public Sinavi2Implementation imp2_ = null;

  public Sinavi2FilleAffEcluses(final BuCommonImplementation _appli, final ArrayList _liste2) {
    super("Affichage des Types d'�cluses", true, true, true, false);
    pTitre_.add(lTitre_);
    listeEcluses2_ = _liste2;
    /* SinaviTableauBateau */
    // tabBateaux_= new BuTableSortModel(tb);
    miseajour(listeEcluses2_);

    table_.setSize(250, 250);
    table_.setAutoResizeMode(AUTO_RESIZE_ALL_COLUMNS);
    table_.setRowSelectionAllowed(true);
    table_.setColumnSelectionAllowed(false);

    scrollPane_ = new BuScrollPane(table_);
    pDonnees2_.add(scrollPane_);
    // scrollPane.repaint();
    table_.setAutoscrolls(true);
    // pDonnees2_.add(table_);

    imp2_ = (Sinavi2Implementation) _appli.getImplementation();

    bAnnuler_.addActionListener(this);
    bDdisponibilites_.addActionListener(this);
    bAjouter_.addActionListener(this);
    bModifier_.addActionListener(this);
    bSupprimer_.addActionListener(this);
    bImprimer_.addActionListener(this);
    bMiseAJour_.addActionListener(this);

    if (!imp2_.isPermettreModif()) {
      bAjouter_.setEnabled(false);
      bModifier_.setEnabled(false);
      bSupprimer_.setEnabled(false);

    }
    ((JComponent) getContentPane()).setBorder(new EmptyBorder(25, 25, 25, 25));

    pBoutons_.setLayout(new FlowLayout(FlowLayout.CENTER));
    pBoutons_.add(bAnnuler_);
    pBoutons_.add(bDdisponibilites_);
    pBoutons_.add(bAjouter_);
    pBoutons_.add(bModifier_);
    pBoutons_.add(bSupprimer_);
    pBoutons_.add(bImprimer_);
    pBoutons_.add(bMiseAJour_);
    getContentPane().add(pTitre_, BorderLayout.NORTH);
    getContentPane().add(pDonnees2_, BorderLayout.CENTER);
    getContentPane().add(pBoutons_, BorderLayout.SOUTH);

    pack();

    /** *reinitialisation des valeurs* */

    setVisible(true);
    addInternalFrameListener(this);
    imp2_.addInternalFrame(this);

  }

  public void miseajour(final ArrayList lstEcluses_) {

    if (tb_ == null) {
      tb_ = new Sinavi2TableauEcluse();
    }
    if (lstEcluses_ != null) {
      final ListIterator iter = lstEcluses_.listIterator();
      int row = 0;
      while (iter.hasNext()) {

        SParametresEcluse c = new SParametresEcluse();
        c = (SParametresEcluse) iter.next();
        tb_.setValueAt(c.identification, row, 0);

        String s = "";
        s = (Double.toString(c.longueur));
        tb_.setValueAt(s, row, 1);

        s = (Double.toString(c.largeur));
        tb_.setValueAt(s, row, 2);

        s = (Double.toString(c.profondeur));
        tb_.setValueAt(s, row, 3);

        s = (Double.toString(c.hauteurChuteDEau));
        tb_.setValueAt(s, row, 4);

        s = (Double.toString(c.dureeBassineeMontante));
        s = Sinavi2Helper.en2Chiffres(s);
        tb_.setValueAt(s, row, 5);

        s = (Double.toString(c.dureeBassineeDescendante));
        s = Sinavi2Helper.en2Chiffres(s);
        tb_.setValueAt(s, row, 6);

        s = (Double.toString(c.dureeManoeuvresEnEntree));
        s = Sinavi2Helper.en2Chiffres(s);
        tb_.setValueAt(s, row, 7);

        s = (Double.toString(c.dureeManoeuvresEnSortie));
        s = Sinavi2Helper.en2Chiffres(s);
        tb_.setValueAt(s, row, 8);

        row++;
        tb_.setNbRow(row);

      }
    }
    table_ = new BuTable(tb_.data_, tb_.nomCol());
    table_.repaint();
  }

  /**
   * @param _e
   */

  public void annuler() {

    imp2_.removeInternalFrame(this);
    // imp2_.sinavi2filleaffecluses_ =null;
    imp2_.resetFille2AffEcluses();
  }

  public void actionPerformed(final ActionEvent _e) {
    if (_e.getSource() == bAnnuler_) {
      // j'arrive pas � faire le bouton annuler
      /*
       * int te=tId_.getText().length(); JOptionPane.showMessageDialog(null,"nb caractere "+te);
       */
      // this.setVisible(false);
      annuler();
      // JOptionPane.showMessageDialog(null,"Bouton Annuler");

    } else if (_e.getSource() == bAjouter_) {
      imp2_.ajouterEcluse(-1, false);

      /*
       * if(controler_entrees()==true){ JOptionPane.showMessageDialog(null,"Cr�ation R�ussie"); initialise_champs(-1);
       */
    } else if (_e.getSource() == bModifier_) {

      final int selection = table_.getSelectedRow();
      if (selection >= 0) {
        imp2_.ajouterEcluse(selection, true);
      } else {
        imp2_.message("Selectionnez une ligne");
        /*
         * if(controler_entrees()==true){ JOptionPane.showMessageDialog(null,"Cr�ation R�ussie"); initialise_champs(-1);
         */
      }
    } else if (_e.getSource() == bSupprimer_) {
      final int selection = table_.getSelectedRow();
      if (selection >= 0) {
        imp2_.supprimerEcluses(selection);
      } else {
        imp2_.message("Selectionnez une ligne");
      }
    } else if (_e.getSource() == bImprimer_) {
      final File f = imp2_.enregistrerXls();
      if (f == null) {
        imp2_.affMessage("Nom de Fichier incorrect");
        return;
      }
      final Sinavi2TableauEcluse tbtemp = new Sinavi2TableauEcluse();
      tbtemp.data_ = new Object[tb_.getRowCount() + 2][tb_.getColumnCount()];
      tbtemp.initNomCol(1);
      tbtemp.setColumnName("Liste des Ecluses", 0);
      for (int i = 1; i < tb_.getColumnCount(); i++) {
        tbtemp.setColumnName(" ", i);
      }
      tbtemp.setNbRow(tb_.getRowCount() + 2);
      for (int i = 2; i <= (tb_.getRowCount() + 1); i++) {
        for (int j = 0; j < (tb_.getColumnCount()); j++) {
          if (j == 5 || j == 6 || j == 7 || j == 8) {
            tbtemp.data_[i][j] = tb_.getValue(i - 2, j).toString().replace('.', 'm');
          } else {
            tbtemp.data_[i][j] = tb_.data_[i - 2][j];
          }
        }

      }
      final CtuluTableExcelWriter test = new CtuluTableExcelWriter(tbtemp, f);
      try {
        test.write(null);

      } catch (final RowsExceededException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      } catch (final WriteException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      } catch (final IOException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }
    } else if (_e.getSource() == bMiseAJour_) {
      annuler();
      imp2_.afficherEcluses();

    } else if (_e.getSource() == bDdisponibilites_) {
      annuler();
      final int selection = table_.getSelectedRow();
      if (selection >= 0) {
        annuler();
        imp2_.addModIndisponibilites("ecl:" + table_.getValueAt(selection, 0).toString(), 3);
      } else {
        imp2_.addModIndisponibilites("", 3);
      }
    }
  }

  public void affMessage(final String _t) {
    new BuDialogMessage(imp2_.getApp(), imp2_.getInformationsSoftware(), _t).activate();
  }

  public void internalFrameActivated(final InternalFrameEvent _e) {
  // TODO Auto-generated method stub

  }

  public void internalFrameClosed(final InternalFrameEvent _e) {
    this.closable = true;
    System.out.println("fermeture centralis�e des portes");
    annuler();

  }

  public void internalFrameDeactivated(final InternalFrameEvent _e) {
  // TODO Auto-generated method stub

  }

  public void internalFrameDeiconified(final InternalFrameEvent _e) {
  // TODO Auto-generated method stub

  }

  public void internalFrameIconified(final InternalFrameEvent _e) {
  // TODO Auto-generated method stub

  }

  public void internalFrameOpened(final InternalFrameEvent _e) {
  // TODO Auto-generated method stub

  }

  public void internalFrameClosing(final InternalFrameEvent _e) {
  // TODO Auto-generated method stub

  }

}
