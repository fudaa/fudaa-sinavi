/*
 * @file         Sinavi2FilleAff�Bateaux.java
 * @creation     2001-05-17
 * @modification $Date: 2007-05-04 14:01:06 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.sinavi2;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.ListIterator;

import javax.swing.JComponent;
import javax.swing.border.EmptyBorder;
import javax.swing.event.InternalFrameEvent;
import javax.swing.event.InternalFrameListener;

import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuCommonImplementation;
import com.memoire.bu.BuDialogMessage;
import com.memoire.bu.BuInternalFrame;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuScrollPane;
import com.memoire.bu.BuTable;

import org.fudaa.ctulu.table.CtuluTableExcelWriter;

import org.fudaa.dodico.corba.sinavi2.SParametresBief;
import org.fudaa.dodico.corba.sinavi2.SParametresEcluse;
import org.fudaa.dodico.corba.sinavi2.SParametresVitesses;

/**
 * impl�mentation d'une fen�tre interne permettant d'afficher les connexions entre les �l�ments du r�seau.
 * 
 * @version $Revision: 1.16 $ $Date: 2007-05-04 14:01:06 $ by $Author: deniger $
 * @author Fatimatou Ka , Ben�it Maneuvrier
 */
public class Sinavi2FilleAffConnexions extends BuInternalFrame implements ActionListener, InternalFrameListener {

  private final BuLabel lTitre_ = new BuLabel("Param�tres de connexion du R�seau");

  /** bouton pour annuler. */
  private final BuButton bAnnuler_ = new BuButton(Sinavi2Resource.SINAVI2.getIcon("QUITTER"), "Quitter");

  private final BuButton bModifier_ = new BuButton(Sinavi2Resource.SINAVI2.getIcon("MODIFIER"), "Modifier");

  private final BuButton bImprimer_ = new BuButton(Sinavi2Resource.SINAVI2.getIcon("IMPRIMER"), "Imprimer");

  private final BuButton bMiseAJour_ = new BuButton(Sinavi2Resource.SINAVI2.getIcon("MISAJOUR"), "Mise � Jour");
  private final BuButton bPalette_ = new BuButton(Sinavi2Resource.SINAVI2.getIcon("PALETTE"), "Visualiser Palette");

  public ArrayList listeBiefs_;
  public ArrayList listeEcluses_;
  public ArrayList listeGares_;
  // public static int nbBateaux_;

  /**
   * bateau pour les champs en cas de modifications.
   */
  public SParametresVitesses vitCourant_;
  // private int nbat_=-1;

  /** * panel contenant les boutons, il est plac� en bas. */

  private final BuPanel pBoutons_ = new BuPanel();
  /**
   * panel contenant les diff�rents champs de la fen�tre.
   */
  private final BuPanel pTitre_ = new BuPanel();

  private final BuPanel pDonnees2_ = new BuPanel();

  private Sinavi2FilleAddModConnexion sinavi2filleaddmodconnexions_;
  public Sinavi2TableauConnexion tb_;
  public BuTable table_;
  public Sinavi2Implementation imp2_;

  public Sinavi2FilleAffConnexions(final BuCommonImplementation _appli, final ArrayList _listeBiefs,
      final ArrayList _listeEcluses, final ArrayList _listeGares) {
    super("Affichage des connexions du r�seau", true, true, true, false);
    // affMessage("pd");
    imp2_ = (Sinavi2Implementation) _appli.getImplementation();
    pTitre_.add(lTitre_);
    listeBiefs_ = _listeBiefs;
    listeEcluses_ = _listeEcluses;
    listeGares_ = _listeGares;
    miseajour(listeBiefs_, listeEcluses_);

    // table_.setSize(250,250);
    // table_.setAutoResizeMode(AUTO_RESIZE_ALL_COLUMNS);
    table_.setRowSelectionAllowed(true);

    pDonnees2_.add(new BuScrollPane(table_));

    bAnnuler_.addActionListener(this);
    bModifier_.addActionListener(this);
    bImprimer_.addActionListener(this);
    bMiseAJour_.addActionListener(this);
    if (!imp2_.isPermettreModif()) {

      bModifier_.setEnabled(false);

    }
    bPalette_.addActionListener(this);
    ((JComponent) getContentPane()).setBorder(new EmptyBorder(25, 25, 25, 25));

    pBoutons_.setLayout(new FlowLayout(FlowLayout.CENTER));
    pBoutons_.add(bAnnuler_);
    pBoutons_.add(bModifier_);
    pBoutons_.add(bImprimer_);
    pBoutons_.add(bMiseAJour_);
    pBoutons_.add(bPalette_);
    getContentPane().add(pTitre_, BorderLayout.NORTH);
    getContentPane().add(pDonnees2_, BorderLayout.CENTER);
    getContentPane().add(pBoutons_, BorderLayout.SOUTH);

    pack();

    setVisible(true);
    addInternalFrameListener(this);
    imp2_.addInternalFrame(this);

  }

  /**
   * On cr� un tableau de la taille de la liste des biefs. Additionn�e � celle de la liste des �cluses On insert dans le
   * tableau les connexions des biefs puis les connexions des �cluses
   */
  public void miseajour(final ArrayList _lstBiefs, final ArrayList _lstEcluses) {
    if (tb_ == null) {
      tb_ = new Sinavi2TableauConnexion();
    }
    tb_.data_ = new Object[_lstBiefs.size() + _lstEcluses.size()][tb_.getColumnCount()];
    // tb_.init_nomcol();
    int row = 0;
    if (_lstBiefs != null) {
      final ListIterator iter = _lstBiefs.listIterator();

      while (iter.hasNext()) {
        SParametresBief c = new SParametresBief();
        c = (SParametresBief) iter.next();
        final String nom = c.identification;
        final String prec = "bie:";
        tb_.setValueAt(concat(nom, prec), row, 0);
        tb_.setValueAt("" + c.gareEnAmont, row, 1);
        tb_.setValueAt("" + c.gareEnAval, row, 2);
        row++;
        tb_.setNbRow(row);

      }
    }
    if (_lstEcluses != null) {
      final ListIterator iter = _lstEcluses.listIterator();
      while (iter.hasNext()) {
        SParametresEcluse c = new SParametresEcluse();
        c = (SParametresEcluse) iter.next();
        final String nom = c.identification;
        final String prec = "ecl:";
        tb_.setValueAt(concat(nom, prec), row, 0);
        tb_.setValueAt("" + c.gareEnAmont, row, 1);
        tb_.setValueAt("" + c.gareEnAval, row, 2);
        row++;
        tb_.setNbRow(row);

      }
    }
    table_ = new BuTable(tb_.data_, tb_.getNomColonnes());
    table_.repaint();
    if ((_lstEcluses != null && _lstEcluses.size() != 0) || (_lstBiefs != null && _lstBiefs.size() != 0)) {
      verifierConnexions();
    }
  }

  /**
   * Cette fonction a pour but d ev�rifier la validit� des connexions. Il ne peut y avoir � la fois un bief et une
   * �cluse partant ou arrivant de la m�me gare
   */

  private void verifierConnexions() {
    final ListIterator it = listeGares_.listIterator();
    ArrayList listeAmontBief = null;
    ArrayList listeAvalBief = null;
    ArrayList listeAmontEcluse = null;
    ArrayList listeAvalEcluse = null;
    while (it.hasNext()) {
      final String x = (String) it.next();
      listeAmontBief = new ArrayList();
      listeAvalBief = new ArrayList();
      listeAmontEcluse = new ArrayList();
      listeAvalEcluse = new ArrayList();
      rechercheAmontAval(x, listeAmontBief, listeAvalBief, listeAmontEcluse, listeAvalEcluse, listeEcluses_,
          listeBiefs_);
      if ((listeAmontBief.size() > 0 && listeAmontEcluse.size() > 0)
          || (listeAvalBief.size() > 0 && listeAvalEcluse.size() > 0)) {
        imp2_
            .affMessage("Il ne peut y avoir des biefs et des �cluses en amont \n ou en aval d'une seule gare. \n V�rifiez la gare "
                + x);

      }
    }

  }

  /**
   * @param _e
   */

  public void annuler() {
    imp2_.removeInternalFrame(this);
    // imp2_.sinavi2filleaffconnexions_ =null;
    imp2_.resetFille2AffConnexion();
  }

  public void actionPerformed(final ActionEvent _e) {
    if (_e.getSource() == bAnnuler_) {
      annuler();
    }

    else if (_e.getSource() == bModifier_) {

      final int selection = table_.getSelectedRow();
      System.out.println("selection --> " + selection);
      if (selection > 0) {
//fred inutile de faire new String
        String elemCourant = "";
        elemCourant = (String) table_.getValueAt(selection, 0);
        final String precCourant = elemCourant.substring(0, 4);
        final String elem = elemCourant.substring(4);
        System.out.println("elemmmmmmmmmmment � modifier : " + elem);
        // imp2_.modifier_vitesse(bateauCourant, biefCourant,tb);
        sinavi2filleaddmodconnexions_ = new Sinavi2FilleAddModConnexion(imp2_,/* imp2_. */listeGares_, listeBiefs_,
            listeEcluses_);
        sinavi2filleaddmodconnexions_.initialiseChamps(elem, precCourant);
        sinavi2filleaddmodconnexions_.tfNomvoie_.setEnabled(false);
        sinavi2filleaddmodconnexions_.setVisible(true);
        // affMessage("Modification effectu�e");

      } else {
        affMessage("Selectionnez une ligne");
      }

      /*
       * if(controler_entrees()==true){ JOptionPane.showMessageDialog(null,"Cr�ation R�ussie"); initialise_champs(-1);
       */
    }

    else if (_e.getSource() == bImprimer_) {

      final File f = imp2_.enregistrerXls();
      if (f == null) {
        imp2_.affMessage("Nom de Fichier incorrect");
        return;
      }
      final Sinavi2TableauConnexion tbtemp = new Sinavi2TableauConnexion();
      tbtemp.data_ = new Object[tb_.getRowCount() + 2][tb_.getColumnCount()];
      tbtemp.initNomCol(1);
      tbtemp.setColumnName("Liste des Connexions", 0);
      for (int i = 1; i < tb_.getColumnCount(); i++) {
        tbtemp.setColumnName(" ", i);
      }
      tbtemp.setNbRow(tb_.getRowCount() + 2);
      for (int i = 2; i <= (tb_.getRowCount() + 1); i++) {
        for (int j = 0; j < tb_.getColumnCount(); j++) {

          tbtemp.data_[i][j] = tb_.data_[i - 2][j];
        }

      }
      final CtuluTableExcelWriter test = new CtuluTableExcelWriter(tbtemp, f);
      try {
        test.write(null);

      } catch (final RowsExceededException e) {
        e.printStackTrace();
      } catch (final WriteException e) {
        e.printStackTrace();
      } catch (final IOException e) {
        e.printStackTrace();
      }
    } else if (_e.getSource() == bMiseAJour_) {
      annuler();
      imp2_.afficherConnexions();
      // miseajour(listeBateaux2_);

      /*
       * if(controler_entrees()==true){ JOptionPane.showMessageDialog(null,"Cr�ation R�ussie"); initialise_champs(-1);
       */
    } else if (_e.getSource() == bPalette_) {

      imp2_.menuPalette(null);

    }
  }

  /**
   * M�thode permettant de concat�ner le type de l'�l�m�nt( bief ou ecluse ) et le nom.
   */
  public static String concat(final String _nom, final String _type) {
    String s = null;
    if (_type.equalsIgnoreCase("bie:")) {
      s = "bie:" + _nom;
    } else if (_type.equalsIgnoreCase("ecl:")) {
      s = "ecl:" + _nom;
    }
    return s;
  }

  /*
   * a virer
   */
  public void affMessage(final String _t) {
    new BuDialogMessage(imp2_.getApp(), imp2_.getInformationsSoftware(), _t).activate();
  }

  public void internalFrameActivated(final InternalFrameEvent _e) {

  }

  public void internalFrameClosed(final InternalFrameEvent _e) {
    imp2_.resetFille2AffConnexion();
  }

  public void internalFrameDeactivated(final InternalFrameEvent _e) {

  }

  public void internalFrameDeiconified(final InternalFrameEvent _e) {

  }

  public void internalFrameIconified(final InternalFrameEvent _e) {

  }

  public void internalFrameOpened(final InternalFrameEvent _e) {

  }

  public void internalFrameClosing(final InternalFrameEvent _e) {}

  /**
   * Cette fonction recherche les listes des biefs, des �cluses en amont et en aval de la gare courante dans les listes
   * d'�cluses et de biefs pass� en param�tres.
   */

  private void rechercheAmontAval(final String _gareCourante, final ArrayList _listeAmontBief,
      final ArrayList _listeAvalBief, final ArrayList _listeAmontEcluse, final ArrayList _listeAvalEcluse,
      final ArrayList _listeEcluses, final ArrayList _listeBiefs) {
    System.out.println("recherche amont aval");
    final ListIterator le = _listeEcluses.listIterator();
    final ListIterator lb = _listeBiefs.listIterator();

    // liste des �cluses
    while (le.hasNext()) {

      final SParametresEcluse e = (SParametresEcluse) le.next();

      if (("" + e.gareEnAval).equals(_gareCourante)) {
        _listeAmontEcluse.add(e);
      }
      if (("" + e.gareEnAmont).equals(_gareCourante)) {

        _listeAvalEcluse.add(e);
      }
    }
    // liste des biefs
    while (lb.hasNext()) {

      final SParametresBief b = (SParametresBief) lb.next();
      if (("" + b.gareEnAval).equals(_gareCourante)) {
        _listeAmontBief.add(b);// ARC

      }
      if (("" + b.gareEnAmont).equals(_gareCourante)) {
        _listeAvalBief.add(b);
      }

    }

  }

}
