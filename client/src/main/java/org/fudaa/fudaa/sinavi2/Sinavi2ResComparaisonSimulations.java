package org.fudaa.fudaa.sinavi2;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.util.ArrayList;

import javax.swing.JComponent;
import javax.swing.border.EmptyBorder;
import javax.swing.event.InternalFrameEvent;
import javax.swing.event.InternalFrameListener;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuCommonImplementation;
import com.memoire.bu.BuDialogMessage;
import com.memoire.bu.BuInternalFrame;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuList;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuScrollPane;

import org.fudaa.dodico.corba.sinavi2.SSimulationSinavi2;

public class Sinavi2ResComparaisonSimulations extends BuInternalFrame implements ActionListener, InternalFrameListener {
  private final BuLabel lTitre_ = new BuLabel("Comparaisons de Simulation");
  private final BuButton bAnnuler_ = new BuButton(Sinavi2Resource.SINAVI2.getIcon("QUITTER"), "Quitter");
  private final BuButton bDureeParcours_ = new BuButton(Sinavi2Resource.SINAVI2.getIcon("DUREEPARCOURS"), "Dur�e de P");
  private final BuButton bAttenteElement_ = new BuButton(Sinavi2Resource.SINAVI2.getIcon("ATTENTEELEMENTS"),
      "Att El�ments");
  private final BuButton bAttenteBateau_ = new BuButton(Sinavi2Resource.SINAVI2.getIcon("ATTENTEBATEAU"), "Att Bateaux");
  private final BuButton bConsoDEau_ = new BuButton(Sinavi2Resource.SINAVI2.getIcon("CONSODEAU"), "Conso d'eau");
  private final BuButton bTableauConsoDEau_ = new BuButton(Sinavi2Resource.SINAVI2.getIcon("TABLEAUCONSODEAU"),
      "Tab Conso d'eau");
  public ArrayList listeSimulations_;
  private final BuPanel pBoutons_ = new BuPanel();
  private final BuPanel pTitre_ = new BuPanel();
  private final BuPanel pDonnees2_ = new BuPanel();
  private final BuLabel lListeSimulations_ = new BuLabel("Liste de Simulations");
  private BuList liListeSimulations_;// =new BuList();
  private BuScrollPane sliListeSimulations_;
  private Sinavi2Implementation imp_;
  

  public Sinavi2ResComparaisonSimulations(final BuCommonImplementation _appli, final ArrayList _listeSimulations) {
    super("Comparaison des Simulations", true, true, true, false);
    listeSimulations_ = _listeSimulations;
    imp_ = (Sinavi2Implementation) _appli.getImplementation();
    
    bAnnuler_.addActionListener(this);
    bDureeParcours_.addActionListener(this);
    bAttenteBateau_.addActionListener(this);
    bAttenteElement_.addActionListener(this);
    bConsoDEau_.addActionListener(this);
    bTableauConsoDEau_.addActionListener(this);
    pTitre_.add(lTitre_);
    pDonnees2_.add(lListeSimulations_);
    sliListeSimulations_ = new BuScrollPane(liListeSimulations_);
    final String[] simulation = new String[listeSimulations_.size()];
    for (int i = 0; i < listeSimulations_.size() - 1; i++) {

      /*
       * ArrayList temp=(ArrayList)listeSimulations_.get(i); simulation[i]=temp.get(0).toString();
       */
      simulation[i] = ((SSimulationSinavi2) listeSimulations_.get(i)).nomSim;
    }
    liListeSimulations_ = new BuList(simulation);
    sliListeSimulations_ = new BuScrollPane(liListeSimulations_);

    pDonnees2_.add(sliListeSimulations_);
    ((JComponent) getContentPane()).setBorder(new EmptyBorder(25, 25, 25, 25));
    pBoutons_.setLayout(new FlowLayout(FlowLayout.CENTER));
    pBoutons_.add(bAnnuler_);
    pBoutons_.add(bDureeParcours_);
    pBoutons_.add(bAttenteBateau_);
    pBoutons_.add(bAttenteElement_);
    pBoutons_.add(bConsoDEau_);
    pBoutons_.add(bTableauConsoDEau_);
    getContentPane().add(pTitre_, BorderLayout.NORTH);
    getContentPane().add(pDonnees2_, BorderLayout.CENTER);
    getContentPane().add(pBoutons_, BorderLayout.SOUTH);
    pack();
  }

  public void annuler() {

    imp_.removeInternalFrame(this);

    // imp_.ResetFille2AffBateaux(); //a faire a ajouter la fenetre dans sinavi2implementatino
  }

  public void actionPerformed(final ActionEvent _e) {
	  final BuDialogMessage indispo_ = new BuDialogMessage(imp_.getApp(), imp_.InfoSoftSinavi2_,
	    "Non disponible pour le moment !");
	  if (_e.getSource() == bAnnuler_) {
		  annuler();

	  } else if (_e.getSource() == bDureeParcours_) {
		  mAJSimulationSel();
		  System.out.println("nb :" + imp_.simulationsSel_.length);
		  imp_.resDureeParcoursC();
	  } else if (_e.getSource() == bAttenteBateau_) {
		  mAJSimulationSel();
		  imp_.resAttentesBateauxC();
	  } else if (_e.getSource() == bAttenteElement_) {
		  mAJSimulationSel();
		  imp_.resAttentesElementsC();
	  } else if (_e.getSource() == bConsoDEau_) {

		  indispo_.activate(); // a modifier lorsque les consommations seront disponibles
		  //mAJSimulationSel();
		  //imp_.resConsommationC();
	  } else if (_e.getSource() == bTableauConsoDEau_) {
		  indispo_.activate(); // a modifier lorsque les consommations seront disponibles
		  
		  //mAJSimulationSel();
		  //imp_.ResAffTableauRecapitulatif(true);
	  }
  }

  private void mAJSimulationSel() {
    imp_.simulationsSel_ = liListeSimulations_.getSelectedIndices();
  }

  public void internalFrameActivated(final InternalFrameEvent _e) {
  // TODO Auto-generated method stub

  }

  public void internalFrameClosed(final InternalFrameEvent _e) {
  // TODO Auto-generated method stub

  }

  public void internalFrameClosing(final InternalFrameEvent _e) {
  // TODO Auto-generated method stub

  }

  public void internalFrameDeactivated(final InternalFrameEvent _e) {
  // TODO Auto-generated method stub

  }

  public void internalFrameDeiconified(final InternalFrameEvent _e) {
  // TODO Auto-generated method stub

  }

  public void internalFrameIconified(final InternalFrameEvent _e) {
  // TODO Auto-generated method stub

  }

  public void internalFrameOpened(final InternalFrameEvent _e) {
  // TODO Auto-generated method stub

  }

  public void focusGained(final FocusEvent _e) {
  // TODO Auto-generated method stub

  }

  public void focusLost(final FocusEvent _e) {
  // TODO Auto-generated method stub

  }

}
