package org.fudaa.fudaa.sinavi2;

import java.util.List;

import javax.swing.table.AbstractTableModel;

import jxl.write.Label;
import jxl.write.Number;
import jxl.write.WritableCell;

import com.memoire.fu.FuLog;

/**
 * @author ka Cette classe a pour but d'afficher le tableau des trajets (c'est � dire le parcours d'un bateau) et
 *         d'offrir la possibilit� de l'impression c'est � dire la cr�ation d'un tableau excel d'o� l'impl�mentation de
 *         CtuluTableModelInterface.
 */

public class Sinavi2TableauTrajet extends AbstractTableModel implements org.fudaa.ctulu.table.CtuluTableModelInterface {

  /*********************************************************************************************************************
   * tableau***** nom du type de bateau sesns du trajet gare de d�part gare d'arriv�e Loi E: heure de d�but de
   * g�n�ration, heure de fin de g�n�ration, ordre de la loi, nombre de bateaux attendus Loi D: date , heures de
   * g�n�ration LoiJ :heure de g�n�ration
   */
  private int nbRow_ = 1;// 1
  private final String[] nomColonnes_ = { "Bateau", "Sens", "D�part", "Arrivee", "Type de loi", "Erlang", "Erlang",
      "Erlang", "Erlang", "Deter ", " ministe", " Journali�re" };

  public Object[][] data_ = new Object[1][getColumnCount()];

  /**
   * @return le tableau contenant le nom des colonnes
   */
  public String[] nomCol() {
    return nomColonnes_;
  }

  /**
   * On met � jour le nombre de lignes gr�ce � cette m�thode
   * 
   * @param _n : nombre de ligne
   */
  public void setNbrow(final int _n) {
    nbRow_ = _n;
  }

  /**
   * @param _row ligne o� on veut ins�rer les noms des colonnes
   */
  public void initNomCol(final int _row) {
    for (int i = 0; i < getColumnCount(); i++) {
      data_[_row][i] = nomColonnes_[i];
    }

  }

  public void initNomcol() {

    data_[0][5] = "D�but";
    data_[0][6] = "Fin";
    data_[0][7] = "Ordre loi";
    data_[0][8] = "Bateaux attendus";
    data_[0][9] = "Date";
    data_[0][10] = "Heures";
    data_[0][11] = "Heure";

  }

  /**
   * nombre de colonnes
   */
  public int getColumnCount() {

    return nomColonnes_.length;
  }

  /**
   * nombre de lignes
   */
  public int getRowCount() {
    return nbRow_;
  }

  /**
   * retourne la valeur du tableau � la ligne _rowIndex et � la colonne _columnIndex_
   */
  public Object getValueAt(final int _rowIndex, final int _columnIndex) {

    return data_[_rowIndex][_columnIndex];
  }

  /**
   * Cette fonction ne fait pas qu'entrer une valeur elle copie les valeurs du tableau et agrandit la valeur data Il
   * faut entrer les valeurs dans l'ordre. Cette fonction n'est pas optimis�e, et si le tableau est grand elle consomme
   * beaucoup de ressources.
   * 
   * @see javax.swing.table.TableModel#setValueAt(java.lang.Object, int, int)
   */
  public void setValueAt(final Object value, final int row, final int col) {
    final Object[][] temp = new Object[row + 1][getColumnCount()];
    if (data_ != null) {
      for (int i = 1; i <= row; i++) {
        if (i == row) {
          for (int j = 0; j < col; j++) {
            FuLog.debug("i :" + i + " j:" + j);
            temp[i][j] = data_[i][j];
          }
        } else {
          for (int j = 0; j < getColumnCount(); j++) {
            // temp[i][j]=new Object();
            temp[i][j] = data_[i][j];
          }
        }
      }
    }
    temp[row][col] = value;
    data_ = new Object[row + 1][getColumnCount()];
    for (int i = 1; i <= row; i++) {
      if (i == row) {
        for (int j = 0; j <= col; j++) {
          data_[i][j] = temp[i][j];
        }
      } else {
        for (int j = 0; j < getColumnCount(); j++) {
          data_[i][j] = temp[i][j];
        }
      }
    }

  }

  /**
   * retourne le nombre de colonnes
   */
  public int getMaxCol() {
    // TODO Auto-generated method stub
    return nomCol().length;
  }

  /**
   * retourne le nombre de ligne
   */
  public int getMaxRow() {
    // TODO Auto-generated method stub
    return nbRow_;
  }

  /**
   * retourne la valeur du tableau � la ligne _row et � la colonne _col
   * 
   * @see org.fudaa.ctulu.table.CtuluTableModelInterface#getValue(int, int)
   */

  public Object getValue(final int _row, final int _col) {
    // TODO Auto-generated method stub
    return getValueAt(_row, _col);
  }
  
     public int[] getSelectedRows() {
        return null;
    }


  /**
   * retoune un tableau pour le format excel Celui-ci sera utilis� avec la fonction write (cf classe
   * Sinavi2FilleAffTrajet : actionPerformed -> imprimer
   * 
   * @see org.fudaa.ctulu.table.CtuluTableModelInterface#getExcelWritable(int, int)
   */
  public WritableCell getExcelWritable(final int _row, final int _col, int _rowXls, int _colXls) {
    final int r = _row;
    final int c = _col;
    final Object o = data_[r][c];
    if (o == null) {
      return null;
    }
    String s = o.toString();
    if (s == null) {
      return null;
    }
    s = s.trim();
    if (s.length() == 0) {
      return null;
    }
    try {
      return new Number(_colXls, _rowXls, Double.parseDouble(s));
    } catch (final NumberFormatException e) {}
    return new Label(_colXls, _rowXls, s);

  }

  /**
   * nom de la colonne de num�ro _i.
   * 
   * @see javax.swing.table.TableModel#getColumnName(int)
   */
  public String getColumnName(final int _i) {
    return nomColonnes_[_i];

  }

  /**
   * @param _name : nom � donner � la colonne
   * @param _i : indice de la colonne � renommer
   */
  public void setColumnName(final String _name, final int _i) {
    nomColonnes_[_i] = _name;

  }

public List<String> getComments() {
	// TODO Auto-generated method stub
	return null;
}
}
