/*
 *  @creation     12 d�c. 2005
 *  @modification $Date: 2007-02-02 11:22:28 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.sinavi2;

import org.fudaa.ctulu.CtuluLibString;

/**
 * Classe a utiliser pour valider les champs duree field.
 * 
 * @author Fred Deniger
 * @version $Id: DureeFieldValidator.java,v 1.12 2007-02-02 11:22:28 deniger Exp $
 */
public abstract class DureeFieldValidator {

  public DureeFieldValidator() {
    super();
  }

  /**
   * @return description courte du controle attendu
   */
  public abstract String getDescription();

  /**
   * @param _sec les secondes a verifier
   * @return true si valide les m�thodes is...Valid v�rifie la valdit� de la valeur getDescription retourne la
   *         description de l'erreur empechant la validation du champ
   */
  public abstract boolean isSecondeValid(int _sec);

  // pour cr�er un validateur avec une valeur minimale
  public static DureeFieldValidator creeMin(final int _minValue) {
    return new DureeFieldValidator() {

      public boolean isSecondeValid(final int _sec) {
        // System.out.println("sec "+_sec+" is sup a "+_minValue+" = "+(_sec >= _minValue));
        return _sec >= _minValue;
      }

      public String getDescription() {
        return Sinavi2Resource.SINAVI2.getString("La valeur doit �tre sup�rieure � {0}", CtuluLibString
            .getString(_minValue));
      }

    };
  }

  // pour cr�er un validator avec une valeur maximale
  public static DureeFieldValidator creeMax(final int _maxValue) {
    return new DureeFieldValidator() {

      public boolean isSecondeValid(final int _sec) {
        return _sec <= _maxValue;
      }

      public String getDescription() {
        return Sinavi2Resource.SINAVI2.getString("La valeur doit �tre positive et inf�rieure � {0}", CtuluLibString
            .getString(_maxValue));
      }

    };
  }

  public static DureeFieldValidator creeMax(final int _maxValue, final String _desc) {
    return new DureeFieldValidator() {

      public boolean isSecondeValid(final int _sec) {
        return _sec <= _maxValue;
      }

      public String getDescription() {
        return _desc;
      }

    };
  }

  // pour cr�er un validateur compris entre deux valeurs
  public static DureeFieldValidator creeMinMax(final DureeField _duree, final int _minValue, final int _maxValue) {
    return new DureeFieldValidator() {

      public boolean isSecondeValid(final int _sec) {
        return _sec <= _maxValue && _sec >= _minValue;
      }

      public String getDescription() {
        return Sinavi2Resource.SINAVI2.getString("La valeur doit �tre comprise entre {0} et {1}",
             CtuluLibString.getString(_minValue) , CtuluLibString.getString(_maxValue));
      }

    };
  }

}
