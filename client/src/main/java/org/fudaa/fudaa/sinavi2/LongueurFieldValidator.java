/*
 *  @creation     12 d�c. 2005
 *  @modification $Date: 2006-09-19 15:08:58 $
 *  @license      GNU General Public License 2
 *  @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 *  @mail         fudaa-devel@lists.sourceforge.net
 */
package org.fudaa.fudaa.sinavi2;

/**
 * Classe a utiliser pour valider les champs duree field.
 * 
 * @author Fred Deniger
 * @version $Id: LongueurFieldValidator.java,v 1.5 2006-09-19 15:08:58 deniger Exp $
 */
public abstract class LongueurFieldValidator {

  public LongueurFieldValidator() {
    super();
  }

  /**
   * @return description courte du controle attendu
   */
  public abstract String getDescription();

  /**
   * @param _sec les secondes a verifier
   * @return true si valide
   */
  public abstract boolean isMetreValid(double _metre);

  // pour cr�er une valeur minimum
  public static LongueurFieldValidator creeMin(final double _minValue) {
    return new LongueurFieldValidator() {
      // valeur sup�rieur au minimum
      public boolean isMetreValid(final double _metre) {
        return _metre >= _minValue;
      }

      // description si la valeur ets inf�rieure
      public String getDescription() {
        return Sinavi2Resource.SINAVI2.getString("La valeur doit �tre sup�rieure � " + _minValue);
      }

    };
  }

  // pour cr�er une valeur maximale
  public static LongueurFieldValidator creeMax(final double _maxValue) {
    return new LongueurFieldValidator() {
      // valeur inf�rieure au max
      public boolean isMetreValid(final double _metre) {
        return _metre <= _maxValue;
      }

      // description si la valaure est sup�rieure � la valeure maximale
      public String getDescription() {
        return Sinavi2Resource.SINAVI2.getString("La valeur doit �tre positive et inf�rieure � " + _maxValue);
      }

    };
  }

  // max + description de l'erreur
  public static LongueurFieldValidator creeMax(final double _maxValue, final String _desc) {
    return new LongueurFieldValidator() {

      public boolean isMetreValid(final double _metre) {
        return _metre <= _maxValue;
      }

      public String getDescription() {
        return _desc;
      }

    };
  }

  // valeure comprise entre un minimum et un maximum
  public static LongueurFieldValidator creeMinMax(final LongueurField _Longueur, final double _minValue,
      final double _maxValue) {
    return new LongueurFieldValidator() {
      // valeur correcte
      public boolean isMetreValid(final double _metre) {
        return _metre <= _maxValue && _metre >= _minValue;
      }

      // description de l'erreur
      public String getDescription() {
        return Sinavi2Resource.SINAVI2
            .getString("La valeur doit �tre comprise entre " + _minValue + " et " + _maxValue);
      }

    };
  }

}
