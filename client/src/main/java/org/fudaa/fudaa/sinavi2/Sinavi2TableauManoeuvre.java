package org.fudaa.fudaa.sinavi2;

import java.util.List;

import javax.swing.table.AbstractTableModel;

import jxl.write.Label;
import jxl.write.Number;
import jxl.write.WritableCell;

import com.memoire.fu.FuLog;

/**
 * @author maneuvrier Cette classe a pour but d'afficher le tableau des manoeuvres et d'offrir la possibilit� de
 *         l'impression c'est � dire la cr�ation d'un tableau excel d'o� l'impl�mentation de CtuluTableModelInterface.
 *         cf. Sinavi2TableauVitesses
 */
public class Sinavi2TableauManoeuvre extends AbstractTableModel implements org.fudaa.ctulu.table.CtuluTableModelInterface {
  private int nbRow_ = 0;
  private int nbBateau_ = 0;
  private int nbEcluse_ = 0;
  public Object[][] data_;
  private final String[] nomColonnes_ = { "Ecluse", "Bateau", "Entr�e", "Sortie" };

  public String[] getNomColonnes() {
    return nomColonnes_;
  }

  public void setNbRow(final int x) {
    nbRow_ = x;
  }

  public int getNbRow() {
    return nbRow_;
  }

  public void setNbBateau(final int _n) {
    nbBateau_ = _n;

  }

  public int getNbBateau() {
    return nbBateau_;

  }

  public void setNbEcluse(final int _n) {
    nbEcluse_ = _n;
  }

  public int getNbEcluse() {
    return nbEcluse_;

  }

  public void addEcluse() {
    nbEcluse_++;
  }

  public void addBateau() {
    nbBateau_++;
  }

  public int getColumnCount() {

    return nomColonnes_.length;
  }

  public int getRowCount() {
    // return nbbateau*nbecluse+1;
    return nbRow_;
  }

  public void initNomCol(final int _row) {
    for (int i = 0; i < getColumnCount(); i++) {
      data_[_row][i] = nomColonnes_[i];
    }

  }

  public Object getValueAt(final int _rowIndex, final int _columnIndex) {

    return data_[_rowIndex][_columnIndex];
  }
  
     public int[] getSelectedRows() {
        return null;
    }


  public void setValueAt(final Object _value, final int _row, final int _col) {
    final Object x = _value;
    nbRow_ = _row;
    System.out.println(x.toString());
    data_[_row][_col] = new Object();

    data_[_row][_col] = x;

  }

  public int getMaxCol() {
    // TODO Auto-generated method stub
    return nomColonnes_.length;
  }

  public int getMaxRow() {
    // TODO Auto-generated method stub
    return nbRow_;
  }

  public Object getValue(final int _row, final int _col) {
    // TODO Auto-generated method stub
    return getValueAt(_row, _col);
  }

  public WritableCell getExcelWritable(final int _row, final int _col, int _rowXls, int _colXls) {
    final int r = _row;
    final int c = _col;
    final Object o = data_[r][c];
    if (o == null) {
      return null;
    }
    String s = o.toString();
    if (s == null) {
      return null;
    }
    s = s.trim();
    if (s.length() == 0) {
      return null;
    }
    try {
      return new Number(_colXls, _rowXls, Double.parseDouble(s));
    } catch (final NumberFormatException e) {}
    return new Label(_colXls, _rowXls, s);
  }

  public String getColumnName(final int i) {
    FuLog.debug("" + nomColonnes_[i]);
    return nomColonnes_[i];

  }

  public void setColumnName(final String _name, final int _i) {
    nomColonnes_[_i] = _name;

  }

public List<String> getComments() {
	// TODO Auto-generated method stub
	return null;
}
}
