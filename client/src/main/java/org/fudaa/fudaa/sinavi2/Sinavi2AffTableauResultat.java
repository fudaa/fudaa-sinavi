package org.fudaa.fudaa.sinavi2;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.ListIterator;

import javax.swing.JComponent;
import javax.swing.border.EmptyBorder;
import javax.swing.event.InternalFrameEvent;
import javax.swing.event.InternalFrameListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuCommonImplementation;
import com.memoire.bu.BuInternalFrame;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuList;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuRadioButton;
import com.memoire.bu.BuScrollPane;
import com.memoire.bu.BuTable;
import com.memoire.fu.FuLog;

import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.table.CtuluTableExcelWriter;
import org.fudaa.ctulu.table.CtuluTableModelInterface;

import org.fudaa.dodico.corba.sinavi2.SParametresBateau;
import org.fudaa.dodico.corba.sinavi2.SParametresBief;
import org.fudaa.dodico.corba.sinavi2.SParametresEcluse;

public class Sinavi2AffTableauResultat extends BuInternalFrame implements ActionListener, InternalFrameListener,
    ItemListener, FocusListener, ListSelectionListener {

  private final BuLabel lTitre_ = new BuLabel("Choix des donn�es � afficher pour les attentes");
  private final BuLabel lBlanc_ = new BuLabel("    ");
  private final BuLabel lBlanc2_ = new BuLabel("    ");
  private final BuLabel lBlanc3_ = new BuLabel("    ");
  private final BuLabel lBlanc4_ = new BuLabel("    ");
  private final BuLabel lchoix_ = new BuLabel("      ");
  public BuRadioButton rBateaux_ = new BuRadioButton("Type de bateaux");
  public BuRadioButton rElements_ = new BuRadioButton("El�ments");
  private final BuLabel lSens_ = new BuLabel("Sens Navigation");
  public BuRadioButton cMontant_ = new BuRadioButton("Montant");
  public BuRadioButton cAvalant_ = new BuRadioButton("Avalant");
  public BuRadioButton cTotal_ = new BuRadioButton("Les deux");
  // private BuLabel lBateaux_ =new BuLabel("Bateaux");
  private final BuLabel lListe_ = new BuLabel("Listes ");

  public BuList liListeBateaux_;

  public BuScrollPane sliListeBateaux_;
  // private BuLabel lElements_ =new BuLabel("Elements");
  // private BuLabel lListeElements_=new BuLabel("Liste des Bateaux");

  public BuList liListeElements_;

  private BuScrollPane sliListeElements_;
  private final BuButton bAnnuler_ = new BuButton(Sinavi2Resource.SINAVI2.getIcon("QUITTER"), "Quitter");
  private final BuButton bAfficher_ = new BuButton(Sinavi2Resource.SINAVI2.getIcon("AFFICHER"), "Afficher");
  private final BuButton bImprimer_ = new BuButton(Sinavi2Resource.SINAVI2.getIcon("IMPRIMER"), "Imprimer");
  /**
   * panel contenant les boutons, il est plac� en bas.
   */

  private final BuPanel pBoutons_ = new BuPanel();
  /**
   * panel contenant les diff�rents champs de la fen�tre.
   */
  private final BuPanel pTitre_ = new BuPanel();

  private final BuPanel pDonnees2_ = new BuPanel();
  /*
   * private BuPanel pBateaux_=new BuPanel(); private BuPanel pElements_=new BuPanel();
   */

  public ArrayList listeAttentes_;
  public ArrayList listeBateaux_;
  public ArrayList listeBiefs_;
  public ArrayList listeEcluses_;
  public ArrayList donnees_;

  private GridBagLayout g2_;
  private GridBagConstraints c_;
  public Sinavi2Implementation imp_ = null;
  public Sinavi2ResTableauResultat t_;

  public Sinavi2AffTableauResultat(final BuCommonImplementation _appli, final ArrayList _listeAttentes,
      final ArrayList _listeBateaux, final ArrayList _listeBiefs, final ArrayList _listeEcluses) {
    super("choix des donn�es � afficher pour les attentes", true, true, true, false);

    listeAttentes_ = _listeAttentes;
    listeBateaux_ = _listeBateaux;
    listeEcluses_ = _listeEcluses;
    listeBiefs_ = _listeBiefs;
    imp_ = (Sinavi2Implementation) _appli.getImplementation();

    bAnnuler_.addActionListener(this);
    bAfficher_.addActionListener(this);

    /** ajout des �l�ments pour le sens de navigation* */

    rBateaux_.addActionListener(this);
    rElements_.addActionListener(this);
    rBateaux_.addItemListener(this);
    rElements_.addItemListener(this);
    cMontant_.addActionListener(this);
    cMontant_.addItemListener(this);
    cAvalant_.addActionListener(this);
    cAvalant_.addItemListener(this);
    cTotal_.addActionListener(this);
    cTotal_.addItemListener(this);

    final String[] bateaux = new String[_listeBateaux.size() + 1];
    bateaux[0] = "Tous";
    /** * ajout des differents types de bateaux* */
    for (int i = 1; i < listeBateaux_.size() + 1; i++) {
      bateaux[i] = ((SParametresBateau) listeBateaux_.get(i - 1)).identification;
    }
    liListeBateaux_ = new BuList(bateaux);
    sliListeBateaux_ = new BuScrollPane(liListeBateaux_);

    /** ajout des �l�ments ** */
    final String[] elements = new String[_listeBiefs.size() + _listeEcluses.size() + 1];
    elements[0] = "Tous";
    for (int i = 1; i < listeBiefs_.size() + 1; i++) {
      elements[i] = "bie:" + ((SParametresBief) listeBiefs_.get(i - 1)).identification;
    }

    for (int i = 0; i < listeEcluses_.size(); i++) {
      elements[i + listeBiefs_.size() + 1] = "ecl:" + ((SParametresEcluse) listeEcluses_.get(i)).identification;
    }
    liListeElements_ = new BuList(elements);
    sliListeElements_ = new BuScrollPane(liListeElements_);

    ((JComponent) getContentPane()).setBorder(new EmptyBorder(25, 25, 25, 25));
    pTitre_.add(lTitre_, "center");

    g2_ = new GridBagLayout();
    pDonnees2_.setLayout(g2_);
    c_ = new GridBagConstraints();

    c_.fill = GridBagConstraints.BOTH;
    c_.gridx = 2;
    c_.gridy = 2;
    pDonnees2_.add(lchoix_, c_);

    c_.gridx = 3;
    c_.gridy = 2;
    pDonnees2_.add(rBateaux_, c_);
    rBateaux_.setSelected(true);

    c_.gridx = 4;
    c_.gridy = 2;
    pDonnees2_.add(lBlanc3_, c_);

    c_.gridx = 5;
    c_.gridy = 2;
    pDonnees2_.add(rElements_, c_);
    rElements_.setSelected(false);

    c_.gridx = 2;
    c_.gridy = 3;
    pDonnees2_.add(lBlanc_, c_);

    c_.gridx = 2;
    c_.gridy = 4;
    pDonnees2_.add(lListe_, c_);

    /*
     * c.gridx =3; c.gridy =4; pDonnees2_.add(lBateaux_,c);
     */

    c_.gridx = 3;
    c_.gridy = 4;
    // sliListeBateaux_.setPreferredSize(sliListeBateaux_.getPreferredSize());
    final Dimension d1 = new Dimension();
    d1.setSize(37, 102);
    sliListeBateaux_.setPreferredSize(d1);
    sliListeBateaux_.revalidate();
    pDonnees2_.add(sliListeBateaux_, c_);

    /*
     * c.gridx =5; c.gridy =4; pDonnees2_.add(lElements_,c);
     */

    c_.gridx = 5;
    c_.gridy = 4;
    // sliListeElements.setPreferredSize(sliListeElements.getPreferredSize());
    final Dimension d = new Dimension();
    d.setSize(37, 102);
    sliListeElements_.setPreferredSize(d);
    sliListeElements_.revalidate();
    pDonnees2_.add(sliListeElements_, c_);

    c_.gridx = 2;
    c_.gridy = 5;
    pDonnees2_.add(lBlanc2_, c_);

    c_.gridx = 2;
    c_.gridy = 6;
    pDonnees2_.add(lSens_, c_);

    c_.gridx = 3;
    c_.gridy = 6;
    cAvalant_.setSelected(true);
    pDonnees2_.add(cAvalant_, c_);

    c_.gridx = 4;
    c_.gridy = 6;
    cMontant_.setSelected(false);
    pDonnees2_.add(cMontant_, c_);

    c_.gridx = 5;
    c_.gridy = 6;
    cTotal_.setSelected(false);
    pDonnees2_.add(cTotal_, c_);

    c_.gridx = 2;
    c_.gridy = 7;
    pDonnees2_.add(lBlanc4_, c_);

    pBoutons_.setLayout(new FlowLayout(FlowLayout.CENTER));
    pBoutons_.add(bAnnuler_);
    // pBoutons_.add(bImprimer_);
    pBoutons_.add(bAfficher_);
    getContentPane().add(pTitre_, BorderLayout.NORTH);
    getContentPane().add(pDonnees2_, BorderLayout.CENTER);
    getContentPane().add(pBoutons_, BorderLayout.SOUTH);

    pack();
    setVisible(true);

    imp_.addInternalFrame(this);

  }

  private void annuler() {
    imp_.removeInternalFrame(this);
    imp_.resetFilleTableauResultat();

  }

  public void actionPerformed(final ActionEvent _e) {
    if (_e.getSource() == bAnnuler_) {
      annuler();
    } else if (_e.getSource() == rBateaux_) {
      rBateaux_.setSelected(true);
      rElements_.setSelected(false);

    } else if (_e.getSource() == rElements_) {
      rElements_.setSelected(true);
      rBateaux_.setSelected(false);

    } else if (_e.getSource() == cMontant_) {
      cMontant_.setSelected(true);
      cAvalant_.setSelected(false);
      cTotal_.setSelected(false);

    }

    else if (_e.getSource() == cAvalant_) {
      cAvalant_.setSelected(true);
      cMontant_.setSelected(false);
      cTotal_.setSelected(false);

    } else if (_e.getSource() == cTotal_) {
      cTotal_.setSelected(true);
      cAvalant_.setSelected(false);
      cMontant_.setSelected(false);

    } else if (_e.getSource() == bAfficher_) {

      if (rBateaux_.isSelected()) {
        if (liListeBateaux_.getSelectedIndex() == 0 || liListeBateaux_.getSelectedIndex() == -1) {
          imp_.affMessage("Selectionnez un type de bateau");
        } else if (liListeElements_.getSelectedIndex() == -1) {
          imp_.affMessage("Selectionnez un ou plusieurs �l�m�nts");
        } else if (liListeBateaux_.getSelectedValues().length != 1) {
          imp_.affMessage("S�lectionnez un seul type de bateau");
        } else {
          if (t_ != null) {
            t_.annuler();
          }
          t_ = new Sinavi2ResTableauResultat(imp_);
        }
      }

      else if (rElements_.isSelected()) {
        if (liListeElements_.getSelectedIndex() == 0 || liListeElements_.getSelectedIndex() == -1) {
          imp_.affMessage("Selectionnez un seul �l�ment");
        } else if (liListeBateaux_.getSelectedIndex() == -1) {
          imp_.affMessage("Selectionnez un ou plusieurs types de bateau");
        } else if (liListeElements_.getSelectedValues().length != 1) {
          imp_.affMessage("S�lectionnez un seul �l�ment");
        } else {
          if (t_ != null) {
            t_.annuler();
          }
          t_ = new Sinavi2ResTableauResultat(imp_);
        }

      }
    } else if (_e.getSource() == bImprimer_) {

    }

  }

  /** m�thode permettand de mettre dans la liste donnees_ les donn�es qui ont �t� s�lectionn�.* */
  public void selectionDonnees(final int _numSim) {
    donnees_ = new ArrayList();
    // ListIterator it =((ArrayList)listeAttentes_.get(_numSim)).listIterator();
    ListIterator it;
    if (rBateaux_.isSelected()) {
      final Object bateau = liListeBateaux_.getSelectedValue();
      final Object[] elementsSelection = liListeElements_.getSelectedValues();
      // ArrayList listeElements =new ArrayList();
      if (liListeElements_.getSelectedIndex() == 0) {
        for (int i = 0; i < listeBiefs_.size(); i++) {
          boolean ajoute = false;
          it = ((ArrayList) listeAttentes_.get(_numSim)).listIterator();
          final String bief = ((SParametresBief) listeBiefs_.get(i)).identification;
          while (it.hasNext()) {
            final Sinavi2TypeAttente s = (Sinavi2TypeAttente) it.next();
            if (/* listeBateaux.contains(s.typeBateaux_) */bief.equalsIgnoreCase(s.element_)
                && s.typeBateaux_.equalsIgnoreCase(bateau.toString())) {
              donnees_.add(s);
              ajoute = true;
            }
          }
          if (!ajoute) {
            final Sinavi2TypeAttente n = new Sinavi2TypeAttente();
            n.setTypeBateaux(bateau.toString());
            n.setTypeElement('B');
            n.setElement(bief);
            n.setDureeMoyAvalantAtt(0);
            n.setDureeMoyMontantAtt(0);
            n.setDureeMoyTotalAtt(0);
            n.setDureeMoyTtlAvalantAtt(0);
            n.setDureeMoyTtlMontantAtt(0);
            n.setDureeMoyTtlTotalAtt(0);
            donnees_.add(n);
          }
        }
        for (int j = 0; j < listeEcluses_.size(); j++) {
          boolean ajoute = false;
          it = ((ArrayList) listeAttentes_.get(_numSim)).listIterator();
          final String ecluse = ((SParametresEcluse) listeEcluses_.get(j)).identification;
          while (it.hasNext()) {
            final Sinavi2TypeAttente s = (Sinavi2TypeAttente) it.next();
            if (/* listeBateaux.contains(s.typeBateaux_) */ecluse.equalsIgnoreCase(s.element_)
                && s.typeBateaux_.equalsIgnoreCase(bateau.toString())) {
              donnees_.add(s);
              ajoute = true;
            }
          }
          if (!ajoute) {
            final Sinavi2TypeAttente n = new Sinavi2TypeAttente();
            n.setTypeBateaux(bateau.toString());
            n.setTypeElement('O');
            n.setElement(ecluse);
            n.setDureeMoyAvalantAtt(0);
            n.setDureeMoyMontantAtt(0);
            n.setDureeMoyTotalAtt(0);
            n.setDureeMoyTtlAvalantAtt(0);
            n.setDureeMoyTtlMontantAtt(0);
            n.setDureeMoyTtlTotalAtt(0);
            donnees_.add(n);
          }
        }

      } else {
        for (int i = 0; i < elementsSelection.length; i++) {
          boolean ajoute = false;
          final String elem = ((String) elementsSelection[i]).substring(4);
          final String prec = ((String) elementsSelection[i]).substring(0, 4);
          // listeElements.add(elem);
          it = ((ArrayList) listeAttentes_.get(_numSim)).listIterator();
          while (it.hasNext()) {
            final Sinavi2TypeAttente s = (Sinavi2TypeAttente) it.next();
            if (/* listeElements.contains(s.element_) */elem.equalsIgnoreCase(s.element_)
                && s.typeBateaux_.equalsIgnoreCase(bateau.toString())) {
              donnees_.add(s);
              ajoute = true;
            }
          }
          if (!ajoute) {
            final Sinavi2TypeAttente n = new Sinavi2TypeAttente();
            n.setDureeMoyAvalantAtt(0);
            n.setDureeMoyMontantAtt(0);
            n.setDureeMoyTotalAtt(0);
            n.setDureeMoyTtlAvalantAtt(0);
            n.setDureeMoyTtlMontantAtt(0);
            n.setDureeMoyTtlTotalAtt(0);
            if (prec.equalsIgnoreCase("bie:")) {
              n.setTypeElement('B');
              n.setElement(elem);
            } else if (prec.equalsIgnoreCase("ecl:")) {
              n.setTypeElement('O');
              n.setElement(elem);
            }
            n.setTypeBateaux(bateau.toString());
            donnees_.add(n);
          }
        }
      }
    }

    else if (rElements_.isSelected()) {
      // ListIterator it =((ArrayList)listeAttentes_.get(_numSim)).listIterator();
      final String element = liListeElements_.getSelectedValue().toString().substring(4);
      final String prec = liListeElements_.getSelectedValue().toString().substring(0, 4);
      if (liListeBateaux_.getSelectedIndex() == 0) {
        System.out.println("je veux tous les bateaux");
        for (int i = 0; i < listeBateaux_.size(); i++) {
          boolean ajoute = false;
          it = ((ArrayList) listeAttentes_.get(_numSim)).listIterator();
          final String bateau = ((SParametresBateau) listeBateaux_.get(i)).identification;
          System.out.println("valeur de bateau " + bateau);
          while (it.hasNext()) {
            final Sinavi2TypeAttente s = (Sinavi2TypeAttente) it.next();
            if (/* listeBateaux.contains(s.typeBateaux_) */bateau.equalsIgnoreCase(s.typeBateaux_)
                && s.element_.equalsIgnoreCase(element)) {
              donnees_.add(s);
              ajoute = true;
            }
          }
          if (!ajoute) {
            final Sinavi2TypeAttente n = new Sinavi2TypeAttente();
            n.setDureeMoyAvalantAtt(0);
            n.setDureeMoyMontantAtt(0);
            n.setDureeMoyTotalAtt(0);
            n.setDureeMoyTtlAvalantAtt(0);
            n.setDureeMoyTtlMontantAtt(0);
            n.setDureeMoyTtlTotalAtt(0);
            n.setTypeBateaux(bateau);
            if (prec.equalsIgnoreCase("bie:")) {
              n.setTypeElement('B');
              n.setElement(element);
            } else if (prec.equalsIgnoreCase("ecl:")) {
              n.setTypeElement('O');
              n.setElement(element);
            }
            donnees_.add(n);
          }
        }
      } else {
        // ArrayList listeBateaux =new ArrayList();
        final Object[] bateauxSelection = liListeBateaux_.getSelectedValues();
        for (int i = 0; i < bateauxSelection.length; i++) {
          boolean ajoute = false;
          // listeBateaux.add(bateauxSelection[i].toString());
          it = ((ArrayList) listeAttentes_.get(_numSim)).listIterator();
          while (it.hasNext()) {
            final Sinavi2TypeAttente s = (Sinavi2TypeAttente) it.next();
            if (/* listeBateaux.contains(s.typeBateaux_) */bateauxSelection[i].toString().equalsIgnoreCase(
                s.typeBateaux_)
                && s.element_.equalsIgnoreCase(element)) {
              donnees_.add(s);
              ajoute = true;
            }
          }
          if (!ajoute) {
            final Sinavi2TypeAttente n = new Sinavi2TypeAttente();
            n.setTypeBateaux(bateauxSelection[i].toString());
            n.setDureeMoyAvalantAtt(0);
            n.setDureeMoyMontantAtt(0);
            n.setDureeMoyTotalAtt(0);
            n.setDureeMoyTtlAvalantAtt(0);
            n.setDureeMoyTtlMontantAtt(0);
            n.setDureeMoyTtlTotalAtt(0);
            if (prec.equalsIgnoreCase("bie:")) {
              n.setTypeElement('B');
              n.setElement(element);
            } else if (prec.equalsIgnoreCase("ecl:")) {
              n.setTypeElement('O');
              n.setElement(element);
            }
            donnees_.add(n);
          }

        }
      }
    }
    it = donnees_.listIterator();
    while (it.hasNext()) {
      final Sinavi2TypeAttente s = (Sinavi2TypeAttente) it.next();
      s.calculeTotaux();
      s.calculeMoy();
      s.calculePourcentage(true, false);
      s.calculePourcentage(false, true);
      s.calculePourcentage(true, true);
      s.reguleNbBateaux();
      s.ConversionDouble();
    }

  }
  public class Sinavi2ResTableauResultat extends BuInternalFrame implements ActionListener, InternalFrameListener {

    private static final int AUTO_RESIZE_ALL_COLUMNS = 0;
    private BuLabel lTitre1_;
    private final BuButton bAnnuler_ = new BuButton(Sinavi2Resource.SINAVI2.getIcon("QUITTER"), "Quitter");
    private final BuButton bImprimer_ = new BuButton(Sinavi2Resource.SINAVI2.getIcon("IMPRIMER"), "Imprimer");
    private final BuButton bMiseAjour_ = new BuButton(Sinavi2Resource.SINAVI2.getIcon("MiSE A JOUR"), "Mise a jour");
    /** * panel contenant les boutons, il est plac� en bas */

    private final BuPanel pBoutons1_ = new BuPanel();
    private final BuPanel pTitre1_ = new BuPanel();
    private final BuPanel pDonnees_ = new BuPanel();

    public Sinavi2TableauResultatMontant tb1_;
    public Sinavi2TableauResultatAvalant tb2_;
    public Sinavi2TableauResultatTotal tb3_;
    public BuScrollPane scrollPane_;
    public BuTable table_;

    // public boolean comparaison_;
    public Sinavi2ResTableauResultat(final BuCommonImplementation _appli) {
      super("Attente ", true, true, true, false);
      if (rBateaux_.isSelected()) {
        if (cMontant_.isSelected()) {
          lTitre1_ = new BuLabel("Attente du  type  " + liListeBateaux_.getSelectedValue().toString() + " "
              + " en Montant");
        } else if (cAvalant_.isSelected()) {
          lTitre1_ = new BuLabel("Attente du  type  " + liListeBateaux_.getSelectedValue().toString() + " "
              + "en Avalant");
        } else if (cTotal_.isSelected()) {
          lTitre1_ = new BuLabel("Attente du  type  " + liListeBateaux_.getSelectedValue().toString() + " "
              + "dans les deux sens");
        }
        pTitre1_.add(lTitre1_);
      }

      else {
        if (cMontant_.isSelected()) {
          lTitre1_ = new BuLabel("Attente de l'�l�m�nt  " + liListeElements_.getSelectedValue().toString()
              + " en Montant");
        } else if (cAvalant_.isSelected()) {
          lTitre1_ = new BuLabel("Attente de l'�l�ment  " + liListeElements_.getSelectedValue().toString()
              + " en Avalant");
        } else {
          lTitre1_ = new BuLabel("Attente de l'�l�ment  " + liListeElements_.getSelectedValue().toString()
              + " dans les deux sens");
        }
        pTitre1_.add(lTitre1_);
      }
      tableau();

      table_.setRowSelectionAllowed(true);
      table_.setColumnSelectionAllowed(false);
      table_.setAutoResizeMode(AUTO_RESIZE_ALL_COLUMNS);
      scrollPane_ = new BuScrollPane(table_);

      pDonnees_.add(scrollPane_);
      table_.setAutoscrolls(true);

      bAnnuler_.addActionListener(this);
      bImprimer_.addActionListener(this);
      bMiseAjour_.addActionListener(this);
      /*
       * if(!imp2_.isPermettreModif()){ }
       */

      ((JComponent) getContentPane()).setBorder(new EmptyBorder(25, 25, 25, 25));

      pBoutons1_.setLayout(new FlowLayout(FlowLayout.CENTER));
      pBoutons1_.add(bAnnuler_);
      pBoutons1_.add(bImprimer_);
      pBoutons1_.add(bMiseAjour_);
      getContentPane().add(pTitre1_, BorderLayout.NORTH);
      getContentPane().add(pDonnees_, BorderLayout.CENTER);
      getContentPane().add(pBoutons1_, BorderLayout.SOUTH);

      pack();
      setLocation(20, 20);

      /** *reinitialisation des valeurs* */

      setVisible(true);
      addInternalFrameListener(this);
      imp_.addInternalFrame(this);

    }

    /** m�thode permettant d'afficher le tableau* */
    public void tableau() {
      /*
       * System.out.println("Numero de simulation s�l�ctionn�e" + imp_.getNumeroSimulationCourante());
       * System.out.println("taille de liste Attentes" + listeAttentes_.size());
       */
      selectionDonnees(0);
      int row = 0;
      String chaine = "";
      final ListIterator it = donnees_.listIterator();
      /** * cas o� le bonton radio montant a �t� s�lectionn�* */
      if (cMontant_.isSelected()) {
        if (tb1_ == null) {
          tb1_ = new Sinavi2TableauResultatMontant();
        }
        int totalBateauxMontant = 0;
        int totalNbBateauxAttendant = 0;
        double totalAttente = 0;
        double TotalMoyAttente;
        double TotalMoyAttenteTotalite;
        double TotalMaxAttente = 0;
        double pourcentageBateauxAttendant;
        while (it.hasNext()) {
          final Sinavi2TypeAttente s = (Sinavi2TypeAttente) it.next();
          /**
           * s.calculeTotaux(); s.calculeMoy(); s.calculePourcentage(true,false); s.reguleNbBateaux();
           * s.ConversionDouble();
           */
          if (rBateaux_.isSelected()) {
            if (s.typeElement_ == 'B') {
              chaine = "bie:" + s.element_;
            } else if (s.typeElement_ == 'O') {
              chaine = "ecl:" + s.element_;
            }
            tb1_.setValueAt(chaine, row, 0);
          } else if (rElements_.isSelected()) {
            tb1_.setValueAt(s.typeBateaux_, row, 0);
          }

          totalBateauxMontant = totalBateauxMontant + s.nbBatMontant_;
          chaine = "" + s.nbBatMontant_;
          tb1_.setValueAt(chaine, row, 1);

          totalNbBateauxAttendant = totalNbBateauxAttendant + s.nbBatMontantAtt_;
          chaine = "" + s.nbBatMontantAtt_;
          tb1_.setValueAt(chaine, row, 2);

          chaine = "" + s.pourcentageMontantAtt_;
          tb1_.setValueAt(chaine, row, 3);

          totalAttente = totalAttente + s.dureeTtlMontantAtt_;
          chaine = ConversionHeureMinute(s.dureeTtlMontantAtt_);
          tb1_.setValueAt(chaine, row, 4);

          chaine = ConversionHeureMinute(s.dureeMoyMontantAtt_);
          tb1_.setValueAt(chaine, row, 5);

          chaine = ConversionHeureMinute(s.dureeMoyTtlMontantAtt_);
          tb1_.setValueAt(chaine, row, 6);

          if (s.dureeMaxMontantAtt_ > TotalMaxAttente) {
            TotalMaxAttente = s.dureeMaxMontantAtt_;
          }
          chaine = ConversionHeureMinute(s.dureeMaxMontantAtt_);
          tb1_.setValueAt(chaine, row, 7);

          row++;
          tb1_.setNbRow(row);
        }
        if (!it.hasNext()) {

          chaine = "Totaux";
          tb1_.setValueAt(chaine, row, 0);

          chaine = "" + totalBateauxMontant;
          tb1_.setValueAt(chaine, row, 1);

          chaine = "" + totalNbBateauxAttendant;
          tb1_.setValueAt(chaine, row, 2);

          pourcentageBateauxAttendant = totalNbBateauxAttendant * 100;
          pourcentageBateauxAttendant = pourcentageBateauxAttendant / totalBateauxMontant;
          final Double d = new Double(pourcentageBateauxAttendant);
          if (d.isNaN()) {
            pourcentageBateauxAttendant = 0;
          } else {
            pourcentageBateauxAttendant = Sinavi2Lib.conversionDeuxChiffres(pourcentageBateauxAttendant);
          }
          chaine = "" + pourcentageBateauxAttendant;
          tb1_.setValueAt(chaine, row, 3);

          chaine = ConversionHeureMinute(totalAttente);
          tb1_.setValueAt(chaine, row, 4);

          TotalMoyAttente = totalAttente / totalNbBateauxAttendant;
          chaine = ConversionHeureMinute(TotalMoyAttente);
          tb1_.setValueAt(chaine, row, 5);

          TotalMoyAttenteTotalite = totalAttente / totalBateauxMontant;
          chaine = ConversionHeureMinute(TotalMoyAttenteTotalite);
          tb1_.setValueAt(chaine, row, 6);

          chaine = ConversionHeureMinute(TotalMaxAttente);
          tb1_.setValueAt(chaine, row, 7);

          row++;
          tb1_.setNbRow(row);

        }
        table_ = new BuTable(tb1_.data_, tb1_.nomCol());
        table_.repaint();

      }
      /** cas o� le bonton radio avalant a �t� s�lectionn�* */
      else if (cAvalant_.isSelected()) {
        if (tb2_ == null) {
          tb2_ = new Sinavi2TableauResultatAvalant();
        }
        int totalBateaux = 0;
        int totalNbBateauxAttendant = 0;
        double totalAttente = 0;
        double TotalMoyAttente;
        double TotalMoyAttenteTotalite;
        double TotalMaxAttente = 0;
        double pourcentageBateauxAttendant;
        while (it.hasNext()) {
          final Sinavi2TypeAttente s = (Sinavi2TypeAttente) it.next();
          /**
           * s.calculeTotaux(); s.calculeMoy(); s.calculePourcentage(false,true); s.reguleNbBateaux();
           * s.ConversionDouble();
           */

          if (rBateaux_.isSelected()) {
            if (s.typeElement_ == 'B') {
              chaine = "bie:" + s.element_;
            } else if (s.typeElement_ == 'O') {
              chaine = "ecl:" + s.element_;
            }
            tb2_.setValueAt(chaine, row, 0);
          } else if (rElements_.isSelected()) {
            tb2_.setValueAt(s.typeBateaux_, row, 0);
          }

          totalBateaux = totalBateaux + s.nbBatAvalant_;
          chaine = CtuluLibString.getString(s.nbBatAvalant_);
          tb2_.setValueAt(chaine, row, 1);

          totalNbBateauxAttendant = totalNbBateauxAttendant + s.nbBatAvalantAtt_;
          chaine = "" + s.nbBatAvalantAtt_;
          tb2_.setValueAt(chaine, row, 2);

          chaine = "" + s.pourcentageAvalantAtt_;
          tb2_.setValueAt(chaine, row, 3);

          totalAttente = totalAttente + s.dureeTtlAvalantAtt_;
          chaine = ConversionHeureMinute(s.dureeTtlAvalantAtt_);
          tb2_.setValueAt(chaine, row, 4);

          chaine = "" + s.pourcentageAvalantAtt_;
          tb2_.setValueAt(chaine, row, 5);

          chaine = ConversionHeureMinute(s.dureeMoyAvalantAtt_);
          tb2_.setValueAt(chaine, row, 5);

          chaine = ConversionHeureMinute(s.dureeMoyTtlAvalantAtt_);
          tb2_.setValueAt(chaine, row, 6);

          if (s.dureeMaxAvalantAtt_ > TotalMaxAttente) {
            TotalMaxAttente = s.dureeMaxAvalantAtt_;
          }
          chaine = ConversionHeureMinute(s.dureeMaxAvalantAtt_);
          tb2_.setValueAt(chaine, row, 7);

          row++;
          tb2_.setNbRow(row);
        }
        if (!it.hasNext()) {

          chaine = "Totaux";
          tb2_.setValueAt(chaine, row, 0);

          chaine = "" + totalBateaux;
          tb2_.setValueAt(chaine, row, 1);

          chaine = "" + totalNbBateauxAttendant;
          tb2_.setValueAt(chaine, row, 2);

          pourcentageBateauxAttendant = totalNbBateauxAttendant * 100;
          pourcentageBateauxAttendant = pourcentageBateauxAttendant / totalBateaux;
          final Double d = new Double(pourcentageBateauxAttendant);
          if (d.isNaN()) {
            pourcentageBateauxAttendant = 0;
          } else {
            pourcentageBateauxAttendant = Sinavi2Lib.conversionDeuxChiffres(pourcentageBateauxAttendant);
          }
          chaine = "" + pourcentageBateauxAttendant;
          tb2_.setValueAt(chaine, row, 3);

          chaine = ConversionHeureMinute(totalAttente);
          tb2_.setValueAt(chaine, row, 4);

          TotalMoyAttente = totalAttente / totalNbBateauxAttendant;
          chaine = ConversionHeureMinute(TotalMoyAttente);
          tb2_.setValueAt(chaine, row, 5);

          TotalMoyAttenteTotalite = totalAttente / totalBateaux;
          chaine = ConversionHeureMinute(TotalMoyAttenteTotalite);
          tb2_.setValueAt(chaine, row, 6);

          chaine = ConversionHeureMinute(TotalMaxAttente);
          tb2_.setValueAt(chaine, row, 7);

          row++;
          tb2_.setNbRow(row);

        }
        table_ = new BuTable(tb2_.data_, tb2_.nomCol());
        table_.repaint();

      }

      /** cas o� le bouton radio Total a �t� s�lectionn�* */
      else if (cTotal_.isSelected()) {
        if (tb3_ == null) {
          tb3_ = new Sinavi2TableauResultatTotal();
        }
        int totalBateaux = 0, totalBateauxMontant = 0, totalBateauxAvalant = 0;
        int totalNbBateauxAtt = 0, totalNbBateauxAttMontant = 0, totalNbBateauxAttAvalant = 0;
        double totalAttente = 0, totalAttenteMontant = 0, totalAttenteAvalant = 0;
        double totalMoyAtt, totalMoyAttTotalite, totalMoyAttMontant, totalMoyAttMontantTotalite, totalMoyAttAvalant, totalMoyAttAvalantTotalite;
        double totalMaxAtt = 0, totalMaxAttMontant = 0, totalMaxAttAvalant = 0;
        double pourcentageAttMontant, pourcentageAttAvalant, pourcentageAttTotal;
        while (it.hasNext()) {
          final Sinavi2TypeAttente s = (Sinavi2TypeAttente) it.next();

          /*************************************************************************************************************
           * s.calculeTotaux(); s.calculeMoy(); s.calculePourcentage(true,false); s.calculePourcentage(false, true);
           * s.calculePourcentage(true, true); s.reguleNbBateaux();
           ************************************************************************************************************/
          if (rBateaux_.isSelected()) {
            if (s.typeElement_ == 'B') {
              chaine = "bie:" + s.element_;
            } else if (s.typeElement_ == 'O') {
              chaine = "ecl:" + s.element_;
            }
            tb3_.setValueAt(chaine, row, 0);
          } else if (rElements_.isSelected()) {
            tb3_.setValueAt(s.typeBateaux_, row, 0);
          }

          totalBateauxMontant = totalBateauxMontant + s.nbBatMontant_;
          chaine = "" + s.nbBatMontant_;
          tb3_.setValueAt(chaine, row, 1);

          totalBateauxAvalant = totalBateauxAvalant + s.nbBatAvalant_;
          chaine = "" + s.nbBatAvalant_;
          tb3_.setValueAt(chaine, row, 2);

          totalBateaux = totalBateaux + s.nbBatTotal_;
          chaine = "" + s.nbBatTotal_;
          tb3_.setValueAt(chaine, row, 3);

          totalNbBateauxAttMontant = totalNbBateauxAttMontant + s.nbBatMontantAtt_;
          chaine = "" + s.nbBatMontantAtt_;
          tb3_.setValueAt(chaine, row, 4);

          chaine = "" + s.pourcentageMontantAtt_;
          tb3_.setValueAt(chaine, row, 5);

          totalAttenteMontant = totalAttenteMontant + s.dureeTtlMontantAtt_;
          chaine = ConversionHeureMinute(s.dureeTtlMontantAtt_);
          tb3_.setValueAt(chaine, row, 6);

          chaine = ConversionHeureMinute(s.dureeMoyMontantAtt_);
          tb3_.setValueAt(chaine, row, 7);

          chaine = ConversionHeureMinute(s.dureeMoyTtlMontantAtt_);
          tb3_.setValueAt(chaine, row, 8);

          if (s.dureeMaxMontantAtt_ > totalMaxAttMontant) {
            totalMaxAttMontant = s.dureeMaxMontantAtt_;
          }
          chaine = ConversionHeureMinute(s.dureeMaxMontantAtt_);
          tb3_.setValueAt(chaine, row, 9);

          totalNbBateauxAttAvalant = totalNbBateauxAttAvalant + s.nbBatAvalantAtt_;
          chaine = "" + s.nbBatAvalantAtt_;
          tb3_.setValueAt(chaine, row, 10);

          chaine = "" + s.pourcentageAvalantAtt_;
          tb3_.setValueAt(chaine, row, 11);

          totalAttenteAvalant = totalAttenteAvalant + s.dureeTtlAvalantAtt_;
          chaine = ConversionHeureMinute(s.dureeTtlAvalantAtt_);
          tb3_.setValueAt(chaine, row, 12);

          chaine = ConversionHeureMinute(s.dureeMoyAvalantAtt_);
          tb3_.setValueAt(chaine, row, 13);

          chaine = ConversionHeureMinute(s.dureeMoyTtlAvalantAtt_);
          tb3_.setValueAt(chaine, row, 14);

          if (s.dureeMaxAvalantAtt_ > totalMaxAttAvalant) {
            totalMaxAttAvalant = s.dureeMaxAvalantAtt_;
          }
          chaine = ConversionHeureMinute(s.dureeMaxAvalantAtt_);
          tb3_.setValueAt(chaine, row, 15);

          totalNbBateauxAtt = totalNbBateauxAtt + s.nbBatTotalAtt_;
          chaine = "" + s.nbBatTotalAtt_;
          tb3_.setValueAt(chaine, row, 16);

          chaine = "" + s.pourcentageTotalAtt_;
          tb3_.setValueAt(chaine, row, 17);

          totalAttente = totalAttente + s.dureeTtlTotalAtt_;
          chaine = ConversionHeureMinute(s.dureeTtlTotalAtt_);
          tb3_.setValueAt(chaine, row, 18);

          chaine = ConversionHeureMinute(s.dureeMoyTotalAtt_);
          tb3_.setValueAt(chaine, row, 19);

          chaine = ConversionHeureMinute(s.dureeMoyTtlTotalAtt_);
          tb3_.setValueAt(chaine, row, 20);

          if (s.dureeMaxTotalAtt_ > totalMaxAtt) {
            totalMaxAtt = s.dureeMaxTotalAtt_;
          }
          chaine = ConversionHeureMinute(s.dureeMaxTotalAtt_);
          tb3_.setValueAt(chaine, row, 21);

          row++;
          tb3_.setNbRow(row);

        }
        if (!it.hasNext()) {
          chaine = "Totaux";
          tb3_.setValueAt(chaine, row, 0);

          chaine = "" + totalBateauxMontant;
          tb3_.setValueAt(chaine, row, 1);

          chaine = "" + totalBateauxAvalant;
          tb3_.setValueAt(chaine, row, 2);

          chaine = "" + totalBateaux;
          tb3_.setValueAt(chaine, row, 3);

          chaine = "" + totalNbBateauxAttMontant;
          tb3_.setValueAt(chaine, row, 4);

          pourcentageAttMontant = totalNbBateauxAttMontant * 100;
          pourcentageAttMontant = pourcentageAttMontant / totalBateauxMontant;
          Double d = new Double(pourcentageAttMontant);
          if (d.isNaN()) {
            pourcentageAttMontant = 0;
          } else {
            pourcentageAttMontant = Sinavi2Lib.conversionDeuxChiffres(pourcentageAttMontant);
          }
          chaine = "" + pourcentageAttMontant;
          tb3_.setValueAt(chaine, row, 5);

          chaine = ConversionHeureMinute(totalAttenteMontant);
          tb3_.setValueAt(chaine, row, 6);

          totalMoyAttMontant = totalAttenteMontant / totalNbBateauxAttMontant;
          chaine = ConversionHeureMinute(totalMoyAttMontant);
          tb3_.setValueAt(chaine, row, 7);

          totalMoyAttMontantTotalite = totalAttenteMontant / totalBateauxMontant;
          chaine = ConversionHeureMinute(totalMoyAttMontantTotalite);
          tb3_.setValueAt(chaine, row, 8);

          chaine = ConversionHeureMinute(totalMaxAttMontant);
          tb3_.setValueAt(chaine, row, 9);

          chaine = "" + totalNbBateauxAttAvalant;
          tb3_.setValueAt(chaine, row, 10);

          pourcentageAttAvalant = totalNbBateauxAttAvalant * 100;
          pourcentageAttAvalant = pourcentageAttAvalant / totalBateauxAvalant;
          d = new Double(pourcentageAttAvalant);
          if (d.isNaN()) {
            pourcentageAttAvalant = 0;
          } else {
            pourcentageAttAvalant = Sinavi2Lib.conversionDeuxChiffres(pourcentageAttAvalant);
          }
          chaine = "" + pourcentageAttAvalant;
          tb3_.setValueAt(chaine, row, 11);

          chaine = ConversionHeureMinute(totalAttenteAvalant);
          tb3_.setValueAt(chaine, row, 12);

          totalMoyAttAvalant = totalAttenteAvalant / totalNbBateauxAttAvalant;
          chaine = ConversionHeureMinute(totalMoyAttAvalant);
          tb3_.setValueAt(chaine, row, 13);

          totalMoyAttAvalantTotalite = totalAttenteAvalant / totalBateauxAvalant;
          chaine = ConversionHeureMinute(totalMoyAttAvalantTotalite);
          tb3_.setValueAt(chaine, row, 14);

          chaine = ConversionHeureMinute(totalMaxAttAvalant);
          tb3_.setValueAt(chaine, row, 15);

          chaine = "" + totalNbBateauxAtt;
          tb3_.setValueAt(chaine, row, 16);

          pourcentageAttTotal = totalNbBateauxAtt * 100;
          pourcentageAttTotal = pourcentageAttTotal / totalBateaux;
          d = new Double(pourcentageAttTotal);
          if (d.isNaN()) {
            pourcentageAttTotal = 0;
          } else {
            pourcentageAttTotal = Sinavi2Lib.conversionDeuxChiffres(pourcentageAttTotal);
          }
          chaine = "" + pourcentageAttTotal;
          tb3_.setValueAt(chaine, row, 17);

          chaine = ConversionHeureMinute(totalAttente);
          tb3_.setValueAt(chaine, row, 18);

          totalMoyAtt = totalAttente / totalNbBateauxAtt;
          chaine = ConversionHeureMinute(totalMoyAtt);
          tb3_.setValueAt(chaine, row, 19);

          totalMoyAttTotalite = totalAttente / totalBateaux;
          chaine = ConversionHeureMinute(totalMoyAttTotalite);
          tb3_.setValueAt(chaine, row, 20);

          chaine = ConversionHeureMinute(totalMaxAtt);
          tb3_.setValueAt(chaine, row, 21);

          row++;
          tb3_.setNbRow(row);
        }

        table_ = new BuTable(tb3_.data_, tb3_.nomCol());
        table_.repaint();

      }
      /*
       * table_ = new BuTable(tb_.data_,tb_.nomCol() ); table_.repaint();
       */

    }

    /** m�thode permettantde calculer les heures , les minutes et les secondes � partir des secondes* */
    public String ConversionHeureMinute(final double s) {
      String chaine = null;
      int heure, minute, seconde;
      heure = (int) (s / 3600);
      final int tempMinute = (int) (s - heure * 3600);
      minute = tempMinute / 60;
      seconde = tempMinute - minute * 60;
      chaine = concatHeure(heure, minute, seconde);
      return chaine;
    }

    /**
     * m�thode permettant d'afficher les heures , les minutes , les secondes sous le format : HHhMMmSS avec HH:les
     * heures, MM:les minutes,SS: les secondes*
     */
    public String concatHeure(final int _heure, final int _minute, final int _seconde) {
      String s;
      if (_heure > 9) {
        if (_minute > 9) {
          if (_seconde > 9) {
            s = new String(_heure + "h" + _minute + "m" + _seconde);
          } else {
            s = new String(_heure + "h" + _minute + "m0" + _seconde);
          }
        } else {
          if (_seconde > 9) {
            s = new String(_heure + "h0" + _minute + "m" + _seconde);
          } else {
            s = new String(_heure + "h0" + _minute + "m0" + _seconde);
          }
        }
      } else {
        if (_minute > 9) {
          if (_seconde > 9) {
            s = new String("0" + _heure + "h" + _minute + "m" + _seconde);
          } else {
            s = new String("0" + _heure + "h" + _minute + "m0" + _seconde);
          }
        } else {
          if (_seconde > 9) {
            s = new String("0" + _heure + "h0" + _minute + "m" + _seconde);
          } else {
            s = new String("0" + _heure + "h0" + _minute + "m0" + _seconde);
          }
        }
      }
      return s;
    }

    /**
     * @param _e
     */

    public void annuler() {
      imp_.resetFilleTableauResultat();
    }

    public void actionPerformed(final ActionEvent _e) {
      if (_e.getSource() == bAnnuler_) {
        annuler();

      } else if (_e.getSource() == bMiseAjour_) {
        /*
         * if(t_!=null) t_.annuler(); t_ = new Sinavi2ResTableauResultat(imp_);
         */
        if (rBateaux_.isSelected()) {
          if (liListeBateaux_.getSelectedIndex() == 0 || liListeBateaux_.getSelectedIndex() == -1) {
            imp_.affMessage("Selectionnez un type de bateau");
          } else if (liListeElements_.getSelectedIndex() == -1) {
            imp_.affMessage("Selectionnez un ou plusieurs �l�m�nts");
          } else if (liListeBateaux_.getSelectedValues().length != 1) {
            imp_.affMessage("S�lectionnez un seul type de bateau");
          } else {
            if (t_ != null) {
              t_.annuler();
            }
            t_ = new Sinavi2ResTableauResultat(imp_);
          }
        }

        else if (rElements_.isSelected()) {
          if (liListeElements_.getSelectedIndex() == 0 || liListeElements_.getSelectedIndex() == -1) {
            imp_.affMessage("Selectionnez un seul �l�ment");
          } else if (liListeBateaux_.getSelectedIndex() == -1) {
            imp_.affMessage("Selectionnez un ou plusieurs types de bateau");
          } else if (liListeElements_.getSelectedValues().length != 1) {
            imp_.affMessage("S�lectionnez un seul �l�ment");
          } else {
            if (t_ != null) {
              t_.annuler();
            }
            t_ = new Sinavi2ResTableauResultat(imp_);
          }

        }
      }
      /** m�thode permettant d'enregistrer sous format Xls les �l�ments du tableau * */
      else if (_e.getSource() == bImprimer_) {
        final File f = imp_.enregistrerXls();
        if (f == null) {
          imp_.affMessage("Nom de Fichier incorrect");
          return;
        }
        imprimer(f);

      }

    }

    private void imprimer(final File _dest) {
      CtuluTableModelInterface model = null;
      if (cMontant_.isSelected()) {
        final Sinavi2TableauResultatMontant tbtemp = new Sinavi2TableauResultatMontant();
        model = tbtemp;
        tbtemp.data_ = new Object[tb1_.getRowCount() + 1][tb1_.getColumnCount()];
        FuLog.debug("nb row + " + tb1_.getRowCount());
        tbtemp.initNomCol(0);
        tbtemp.setNbRow(tb1_.getRowCount() + 1);
        for (int i = 1; i <= tb1_.getRowCount(); i++) {
          for (int j = 0; j < tb1_.getColumnCount(); j++) {
            tbtemp.data_[i][j] = tb1_.data_[i - 1][j];
          }
        }

      }

      else if (cAvalant_.isSelected()) {
        final Sinavi2TableauResultatAvalant tbtemp = new Sinavi2TableauResultatAvalant();
        model = tbtemp;
        tbtemp.data_ = new Object[tb2_.getRowCount() + 1][tb2_.getColumnCount()];
        FuLog.debug("nb row + " + tb2_.getRowCount());
        tbtemp.initNomCol(0);
        tbtemp.setNbRow(tb2_.getRowCount() + 1);
        for (int i = 1; i <= tb2_.getRowCount(); i++) {
          for (int j = 0; j < tb2_.getColumnCount(); j++) {

            tbtemp.data_[i][j] = tb2_.data_[i - 1][j];
          }

        }

      } else if (cTotal_.isSelected()) {
        final Sinavi2TableauResultatTotal tbtemp = new Sinavi2TableauResultatTotal();
        model = tbtemp;
        tbtemp.data_ = new Object[tb3_.getRowCount() + 1][tb3_.getColumnCount()];
        FuLog.debug("nb row + " + tb3_.getRowCount());
        tbtemp.initNomCol(0);
        tbtemp.setNbRow(tb3_.getRowCount() + 1);
        for (int i = 1; i <= tb3_.getRowCount(); i++) {
          for (int j = 0; j < tb3_.getColumnCount(); j++) {
            tbtemp.data_[i][j] = tb3_.data_[i - 1][j];
          }
        }

      }
      if (model != null) {
        final CtuluTableExcelWriter test = new CtuluTableExcelWriter(model, _dest);

        try {
          test.write(null);

        } catch (final RowsExceededException _err) {
          FuLog.error(_err);
        } catch (final WriteException _err) {
          FuLog.error(_err);
        } catch (final IOException _err) {
          FuLog.error(_err);
        }
      }
    }

    public void internalFrameActivated(final InternalFrameEvent _e) {
    // TODO Auto-generated method stub

    }

    public void internalFrameClosed(final InternalFrameEvent _e) {
      this.closable = true;
      System.out.println("fermeture centralis�e des portes");
      annuler();

    }

    public void internalFrameDeactivated(final InternalFrameEvent _e) {

    }

    public void internalFrameDeiconified(final InternalFrameEvent _e) {

    }

    public void internalFrameIconified(final InternalFrameEvent _e) {

    }

    public void internalFrameOpened(final InternalFrameEvent _e) {

    }

    public void internalFrameClosing(final InternalFrameEvent _e) {

    }

  }

  public void internalFrameActivated(final InternalFrameEvent e) {

  }

  public void internalFrameClosed(final InternalFrameEvent e) {
    this.closable = true;
    imp_.resetFilleTableauResultat();

  }

  public void internalFrameClosing(final InternalFrameEvent e) {

  }

  public void internalFrameDeactivated(final InternalFrameEvent e) {

  }

  public void internalFrameDeiconified(final InternalFrameEvent e) {

  }

  public void internalFrameIconified(final InternalFrameEvent e) {

  }

  public void internalFrameOpened(final InternalFrameEvent e) {

  }

  public void itemStateChanged(final ItemEvent e) {

  }

  public void focusGained(final FocusEvent e) {

  }

  public void focusLost(final FocusEvent e) {

  }

  public void valueChanged(final ListSelectionEvent e) {}

}
