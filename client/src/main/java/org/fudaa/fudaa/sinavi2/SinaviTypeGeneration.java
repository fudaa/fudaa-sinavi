package org.fudaa.fudaa.sinavi2;

import org.fudaa.dodico.corba.sinavi2.SResultatGenerationBateau;

class SinaviTypeGeneration {
  public SinaviTypeGeneration() {}

  public SinaviTypeGeneration(final SResultatGenerationBateau _gen) {
    this.jour_ = _gen.jour;
    this.heureDepart_ = concatHeure(_gen.heure, _gen.minute, _gen.seconde);
    this.avalantMontant_ = avalantMontant(_gen.avalantMontant);
    this.gareArrivee_ = _gen.numeroGareSortie;
    this.gareDepart_ = _gen.numeroGareDepart;
    this.typeBateau_ = _gen.numeroTypeBateau;
  }

  /*
   * public Component getEditeur(){ return new double(true,false,true); }
   */

  public static char avalantMontant(final boolean _avalantMontant) {
    if (_avalantMontant) {
      return 'A';
    } else {
      return 'M';
    }

  }

  public static String concatHeure(final int _heure, final int _minute, final int _seconde) {
    String s;
    if (_heure > 9) {
      if (_minute > 9) {
        if (_seconde > 9) {
          s = new String(_heure + "h" + _minute + "m" + _seconde);
        } else {
          s = new String(_heure + "h" + _minute + "m0" + _seconde);
        }
      } else {
        if (_seconde > 9) {
          s = new String(_heure + "h0" + _minute + "m" + _seconde);
        } else {
          s = new String(_heure + "h0" + _minute + "m0" + _seconde);
        }
      }
    } else {
      if (_minute > 9) {
        if (_seconde > 9) {
          s = new String("0" + _heure + "h" + _minute + "m" + _seconde);
        } else {
          s = new String("0" + _heure + "h" + _minute + "m0" + _seconde);
        }
      } else {
        if (_seconde > 9) {
          s = new String("0" + _heure + "h0" + _minute + "m" + _seconde);
        } else {
          s = new String("0" + _heure + "h0" + _minute + "m0" + _seconde);
        }
      }
    }
    return s;
  }

  public char getAvalantMontant() {
    return avalantMontant_;
  }

  public void setAvalantMontant(final char _avalantMontant) {
    avalantMontant_ = _avalantMontant;
  }

  public int getGareArrivee() {
    return gareArrivee_;
  }

  public void setGareArrivee(final int _gareArrivee) {
    gareArrivee_ = _gareArrivee;
  }

  public int getGareDepart() {
    return gareDepart_;
  }

  public void setGareDepart(final int _gareDepart) {
    gareDepart_ = _gareDepart;
  }

  public String getHeureDepart() {
    return heureDepart_;
  }

  public void setHeureDepart(final String _heureDepart) {
    heureDepart_ = _heureDepart;
  }

  public int getJour() {
    return jour_;
  }

  public void setJour(final int _jour) {
    jour_ = _jour;
  }

  public int getTypeBateau() {
    return typeBateau_;
  }

  public void setTypeBateau(final int _typeBateau) {
    typeBateau_ = _typeBateau;
  }

  // constructeur de recopie m�thode

  // accesseurs

  /*
   * fonctions recherche du bateau dans la liste de bateaux concatener l'heure
   */

  int jour_;
  String heureDepart_;
  char avalantMontant_;// true pour avalant
  int typeBateau_;
  int gareDepart_;
  int gareArrivee_;
}
