package org.fudaa.fudaa.sinavi2;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.ListIterator;

import javax.swing.JComponent;
import javax.swing.border.EmptyBorder;
import javax.swing.event.InternalFrameEvent;
import javax.swing.event.InternalFrameListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

import com.memoire.bu.*;
import com.memoire.fu.FuLog;

import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.table.CtuluTableExcelWriter;
import org.fudaa.ctulu.table.CtuluTableModelInterface;

import org.fudaa.dodico.corba.sinavi2.SParametresBateau;
import org.fudaa.dodico.corba.sinavi2.SParametresEcluse;
import org.fudaa.dodico.corba.sinavi2.SResultatConsommationDEau;
import org.fudaa.dodico.corba.sinavi2.SSimulationSinavi2;

import org.fudaa.ebli.graphe.BGraphe;

/**
 * impl�mentation d'une fen�tre interne permettant de saisir les param�tres pour le calcul de la consommation d'eau.
 * 
 * @version $Revision: 1.8 $ $Date: 2006-09-19 15:08:58 $ by $Author: deniger $
 * @author Fatimatou Ka , Beno�t Maneuvrier
 */

public class Sinavi2ResConsommationDEau extends BuInternalFrame implements ActionListener, InternalFrameListener,
    FocusListener, ListSelectionListener, ItemListener {

  private final BuLabel lTitre_ = new BuLabel("Consommation d'eau");
  private final BuLabel lBlanc_ = new BuLabel("    ");
  private final BuLabel lBlanc2_ = new BuLabel("    ");
  private final BuLabel lBlanc3_ = new BuLabel("    ");
  private final BuLabel lBlanc4_ = new BuLabel("    ");
  private final BuLabel lBlanc5_ = new BuLabel("    ");

  /** bouton pour annuler. */
  private final BuButton bAnnuler_ = new BuButton(Sinavi2Resource.SINAVI2.getIcon("QUITTER"), "Quitter");

  /** afficher le graphique. */
  private final BuButton bAfficher_ = new BuButton(Sinavi2Resource.SINAVI2.getIcon("EDITER"), "Afficher");

  /** Param�tres d'entr�es pour le graphique. */
  private final BuLabel lItineraire_ = new BuLabel("Itin�raire");
  private final BuLabel lSensNav_ = new BuLabel("Sens Navigation");
  public BuComboBox cSensNav_ = new BuComboBox();

  private final BuLabel lEcluse_ = new BuLabel("Ecluses");
  private final BuLabel lListeEcluses_ = new BuLabel("Liste des �cluses");

  public BuList liListeEcluses_;

  private BuScrollPane sliListeEcluses_;
  /*
   * private BuLabel lCrenaux_ =new BuLabel("Cr�naux Horaires"); private BuLabel lHoraireDebut_ = new BuLabel("Horaires
   * de D�but"); private BuLabel lHoraireFin_ = new BuLabel("Horaires de Fin"); private DureeField dHoraireDebutHeure_ =
   * new DureeField(false,false,true,false,false); private DureeField dHoraireDebutMinute_ = new
   * DureeField(false,false,false,true,false); private DureeField dHoraireFinHeure_ = new
   * DureeField(false,false,true,false,false); private DureeField dHoraireFinMinute_ = new
   * DureeField(false,false,false,true,false);
   */

  private final BuLabel ltypeGraphe_ = new BuLabel("Type Graphe");
  private final BuRadioButton rHistogramme_ = new BuRadioButton("Histogramme");
  private final BuRadioButton rCourbe_ = new BuRadioButton("Courbe");

  private final BuLabel lAxeX_ = new BuLabel("Axe des abcisses");
  private final BuRadioButton rEcluseX_ = new BuRadioButton("Ecluses");
  private final BuRadioButton rConsoX_ = new BuRadioButton("consommation");
  private final BuRadioButton rBassinX_ = new BuRadioButton("Bassin�es");
  private final BuRadioButton rRemplissageX_ = new BuRadioButton("Taux de Remplissage");

  private final BuLabel lDonneesAAfficher_ = new BuLabel("Donn�es � Afficher");

  private final BuCheckBox cSeuil_ = new BuCheckBox("Seuil");

  private final BuTextField tSeuil_ = new BuTextField(" 0         ");
  public BuRadioButton rConsom_ = new BuRadioButton("Consommation");
  public BuRadioButton rBassinees_ = new BuRadioButton("Bassin�es");
  public BuRadioButton rRemplissage_ = new BuRadioButton("Taux de remplissage");
  public Sinavi2TableauResConsommationDEau t_;

  /** * panel contenant les boutons, il est plac� en bas. */

  private final BuPanel pBoutons_ = new BuPanel();
  /**
   * panel contenant les diff�rents champs de la fen�tre.
   */
  private final BuPanel pTitre_ = new BuPanel();

  private final BuPanel pDonnees2_ = new BuPanel();
  private GridBagLayout g2;

  public ArrayList donneesGraph_;
  public ArrayList listeEcluses_;
  public ArrayList listeSimulations_;
  public ArrayList listeConsommation_;
  public Sinavi2Implementation imp_;
  public ArrayList listeCons_;
  public ArrayList listeBateaux_;
  public ArrayList listeRemplissage_;
  boolean comparaison_;
  public ArrayList listeCoordonnees_;

  public Sinavi2ResConsommationDEau(final BuCommonImplementation _appli, final ArrayList _listeEcluses,
      final ArrayList _listeSimulationsSinavi2, final ArrayList _listeCons, final ArrayList _listeBateaux,
      final ArrayList _listeConsommation, final boolean _comparaison) {
    super("Consommation d'eau", true, true, true, false);
    comparaison_ = _comparaison;

    imp_ = (Sinavi2Implementation) _appli.getImplementation();
    listeCons_ = _listeCons;
    listeConsommation_ = _listeConsommation;
    listeEcluses_ = _listeEcluses;
    listeBateaux_ = _listeBateaux;
    if (comparaison_) {
      listeSimulations_ = _listeSimulationsSinavi2;
    }
    /** *** contraintes sur les boutons et boutons radios***** */
    bAnnuler_.addActionListener(this);
    bAfficher_.addActionListener(this);
    rCourbe_.addItemListener(this);
    rCourbe_.addActionListener(this);
    rConsom_.addItemListener(this);
    rConsom_.addActionListener(this);
    rRemplissage_.addItemListener(this);
    rRemplissage_.addActionListener(this);
    rBassinees_.addItemListener(this);
    rBassinees_.addActionListener(this);
    rHistogramme_.addItemListener(this);
    rHistogramme_.addActionListener(this);
    rEcluseX_.addActionListener(this);
    rEcluseX_.addItemListener(this);
    rConsoX_.addActionListener(this);
    rConsoX_.addItemListener(this);
    rBassinX_.addActionListener(this);
    rBassinX_.addItemListener(this);
    rRemplissageX_.addActionListener(this);
    rRemplissageX_.addItemListener(this);
    cSeuil_.addActionListener(this);

    ((JComponent) getContentPane()).setBorder(new EmptyBorder(25, 25, 25, 25));
    pTitre_.add(lTitre_, "center");
    g2 = new GridBagLayout();
    pDonnees2_.setLayout(g2);
    final GridBagConstraints c = new GridBagConstraints();
    c.fill = GridBagConstraints.BOTH;

    ListIterator it;
    c.gridx = 1;
    c.gridy = 2;
    pDonnees2_.add(lItineraire_, c);

    c.gridx = 2;
    c.gridy = 2;
    pDonnees2_.add(lSensNav_, c);

    cSensNav_.addItem("les 2");
    cSensNav_.addItem("Montant");
    cSensNav_.addItem("Avalant");
    c.gridx = 3;
    c.gridy = 2;
    pDonnees2_.add(cSensNav_, c);

    c.gridx = 1;
    c.gridy = 3;
    pDonnees2_.add(lBlanc_, c);

    c.gridx = 1;
    c.gridy = 4;
    pDonnees2_.add(lEcluse_, c);

    c.gridx = 2;
    c.gridy = 4;
    pDonnees2_.add(lListeEcluses_, c);

    final String[] ecluses = new String[_listeEcluses.size()];
    int i = 0;
    it = _listeEcluses.listIterator();
    while (it.hasNext()) {
      ecluses[i] = ((SParametresEcluse) it.next()).identification;
      i++;
    }

    liListeEcluses_ = new BuList(ecluses);
    sliListeEcluses_ = new BuScrollPane(liListeEcluses_);
    c.gridx = 3;
    c.gridy = 4;
    final Dimension d = new Dimension();
    d.setSize(37, 102);
    sliListeEcluses_.setPreferredSize(d);
    sliListeEcluses_.revalidate();
    pDonnees2_.add(sliListeEcluses_, c);

    c.gridx = 1;
    c.gridy = 5;
    pDonnees2_.add(lBlanc2_, c);

    /*
     * c.gridx =1; c.gridy =6; pDonnees2_.add(lCrenaux_,c); c.gridx =2; c.gridy =6; pDonnees2_.add(lHoraireDebut_,c);
     * c.gridx =3; c.gridy =6; pDonnees2_.add(dHoraireDebutHeure_,c); c.gridx =4; c.gridy =6;
     * pDonnees2_.add(dHoraireDebutMinute_,c); c.gridx =5; c.gridy =6; pDonnees2_.add(lHoraireFin_,c); c.gridx =6;
     * c.gridy =6; pDonnees2_.add(dHoraireFinHeure_,c); c.gridx =7; c.gridy =6; pDonnees2_.add(dHoraireFinMinute_,c);
     * dHoraireDebutHeure_.setDureeField(0); dHoraireDebutMinute_.setDureeField(0);
     * dHoraireFinHeure_.setDureeField(86400); dHoraireFinMinute_.setDureeField(0);
     */

    c.gridx = 1;
    c.gridy = 6;
    pDonnees2_.add(lBlanc3_, c);

    c.gridx = 1;
    c.gridy = 7;
    pDonnees2_.add(ltypeGraphe_, c);

    c.gridx = 2;
    c.gridy = 7;
    pDonnees2_.add(rCourbe_, c);
    rCourbe_.setSelected(true);
    c.gridx = 3;
    c.gridy = 7;
    pDonnees2_.add(rHistogramme_, c);
    rHistogramme_.setSelected(false);

    c.gridx = 1;
    c.gridy = 9;
    pDonnees2_.add(lBlanc4_, c);

    c.gridx = 1;
    c.gridy = 10;
    pDonnees2_.add(lDonneesAAfficher_, c);

    c.gridx = 2;
    c.gridy = 10;
    pDonnees2_.add(rConsom_, c);
    rConsom_.setSelected(true);

    c.gridx = 3;
    c.gridy = 10;
    pDonnees2_.add(rBassinees_, c);
    rBassinees_.setSelected(false);

    c.gridx = 4;
    c.gridy = 10;
    pDonnees2_.add(rRemplissage_, c);
    rRemplissage_.setSelected(false);
    c.gridx = 5;
    c.gridy = 10;
    pDonnees2_.add(cSeuil_, c);

    /*
     * c.gridx =6; c.gridy =10; pDonnees2_.add(lSeuil_,c);
     */

    /*
     * c.gridx =6; c.gridy =10; pDonnees2_.add(lBlanc6_,c);
     */

    c.gridx = 6;
    c.gridy = 10;
    pDonnees2_.add(tSeuil_, c);

    c.gridx = 1;
    c.gridy = 11;
    pDonnees2_.add(lBlanc5_, c);

    c.gridx = 1;
    c.gridy = 12;
    pDonnees2_.add(lAxeX_, c);

    c.gridx = 2;
    c.gridy = 12;
    pDonnees2_.add(rEcluseX_, c);
    rEcluseX_.setSelected(true);

    c.gridx = 3;
    c.gridy = 12;
    pDonnees2_.add(rConsoX_, c);
    rConsoX_.setSelected(false);

    c.gridx = 4;
    c.gridy = 12;
    pDonnees2_.add(rBassinX_, c);
    rBassinX_.setSelected(false);

    c.gridx = 5;
    c.gridy = 12;
    pDonnees2_.add(rRemplissageX_, c);
    rRemplissageX_.setSelected(false);

    pBoutons_.setLayout(new FlowLayout(FlowLayout.CENTER));
    pBoutons_.add(bAnnuler_);

    pBoutons_.add(bAfficher_);
    getContentPane().add(pTitre_, BorderLayout.NORTH);
    getContentPane().add(pDonnees2_, BorderLayout.CENTER);
    getContentPane().add(pBoutons_, BorderLayout.SOUTH);

    pack();
    setVisible(true);
    imp_.addInternalFrame(this);
  }

  public void actionPerformed(final ActionEvent _e) {
    if (_e.getSource() == bAnnuler_) {
      annuler();
    } else if (_e.getSource() == bAfficher_) {
      afficherGraphe();
      System.out.println("avant la construction du tableau");
      if (t_ != null) {
        t_.annuler();
      }
      t_ = new Sinavi2TableauResConsommationDEau(imp_, comparaison_);
      System.out.println("apr�s la construction du tableau");
      imp_.activateInternalFrame(imp_.fillegraphe_);
    }

    else if (_e.getSource() == rHistogramme_) {
      rHistogramme_.setSelected(true);
      rCourbe_.setSelected(false);
      /** car les histogrammes sont pas fait dans l'autre sens* */
      rEcluseX_.setSelected(true);
      rConsoX_.setSelected(false);
      rBassinX_.setSelected(false);
      rRemplissageX_.setSelected(false);
    } else if (_e.getSource() == rCourbe_) {
      rHistogramme_.setSelected(false);
      rCourbe_.setSelected(true);
    } else if (_e.getSource() == rConsom_) {
      rConsom_.setSelected(true);
      rRemplissage_.setSelected(false);
      rBassinees_.setSelected(false);
      rEcluseX_.setSelected(true);
      rConsoX_.setSelected(true);
      rRemplissageX_.setSelected(false);
      rBassinX_.setSelected(false);
      if (rEcluseX_.isSelected()) {
        rConsoX_.setSelected(false);
      } else if (rConsoX_.isSelected()) {
        rEcluseX_.setSelected(false);
      }
      if (rRemplissageX_.isSelected() || rBassinX_.isSelected()) {
        imp_.affMessage("Selectionnez Consommation dans abscisses");
      }

    }

    else if (_e.getSource() == rRemplissage_) {
      rEcluseX_.setSelected(true);
      rRemplissage_.setSelected(true);
      rRemplissageX_.setSelected(false);
      rBassinX_.setSelected(false);
      rConsoX_.setSelected(false);
      rConsom_.setSelected(false);
      rBassinees_.setSelected(false);
      if (rEcluseX_.isSelected()) {
        rRemplissageX_.setSelected(false);
      } else if (rRemplissageX_.isSelected()) {
        rEcluseX_.setSelected(false);
      }
      if (rConsoX_.isSelected() || rBassinX_.isSelected()) {
        imp_.affMessage("Selectionnez Taux de Remplissage dans abscisses");
      }
    } else if (_e.getSource() == rBassinees_) {
      rEcluseX_.setSelected(true);
      rBassinX_.setSelected(true);
      rConsoX_.setSelected(false);
      rRemplissageX_.setSelected(false);
      rConsom_.setSelected(false);
      rRemplissage_.setSelected(false);
      rBassinees_.setSelected(true);
      if (rEcluseX_.isSelected()) {
        rBassinX_.setSelected(false);
      } else if (rBassinX_.isSelected()) {
        rEcluseX_.setSelected(false);
      }
      if (rConsoX_.isSelected() || rRemplissageX_.isSelected()) {
        imp_.affMessage("Selectionnez Taux de Bassinees dans abscisses");
      }
    } else if (_e.getSource() == rEcluseX_) {
      rEcluseX_.setSelected(true);
      if (rConsom_.isSelected()) {
        rRemplissage_.setSelected(false);
        rBassinees_.setSelected(false);
      } else if (rBassinees_.isSelected()) {
        rConsom_.setSelected(false);
        rRemplissage_.setSelected(false);
      } else if (rRemplissage_.isSelected()) {
        rConsom_.setSelected(false);
        rBassinees_.setSelected(false);
      }
      rRemplissage_.setSelected(true);
      rBassinX_.setSelected(false);
      rRemplissageX_.setSelected(false);
      rConsoX_.setSelected(false);

    } else if (_e.getSource() == rBassinX_) {
      rBassinX_.setSelected(true);
      rBassinees_.setSelected(true);
      rEcluseX_.setSelected(false);
      rConsoX_.setSelected(false);
      rConsom_.setSelected(false);
      rRemplissage_.setSelected(false);
      rRemplissageX_.setSelected(false);
      if (rConsom_.isSelected() || rRemplissage_.isSelected()) {
        imp_.affMessage("Selectionnez Bassin�es dans donn�es � afficher");
      }
      rHistogramme_.setSelected(false);
      rCourbe_.setSelected(true);

    } else if (_e.getSource() == rRemplissageX_) {
      rRemplissageX_.setSelected(true);
      rRemplissage_.setSelected(true);
      rConsom_.setSelected(false);
      rBassinees_.setSelected(false);
      rEcluseX_.setSelected(false);
      rBassinX_.setSelected(false);
      rConsoX_.setSelected(false);
      if (rConsom_.isSelected() || rBassinees_.isSelected()) {
        imp_.affMessage("Selectionnez taux de de remplissage dans donn�es � afficher");
      }
      /** * les histogrammes ne sont pas faits dans l'autre sens* */
      rHistogramme_.setSelected(false);
      rCourbe_.setSelected(true);

    } else if (_e.getSource() == rConsoX_) {
      rConsoX_.setSelected(true);
      rRemplissageX_.setSelected(false);
      rEcluseX_.setSelected(false);
      rBassinX_.setSelected(false);
      rConsom_.setSelected(true);
      rRemplissage_.setSelected(false);
      rBassinees_.setSelected(false);
      if (rRemplissage_.isSelected() || rBassinees_.isSelected()) {
        imp_.affMessage("Selectionnez Consommation dans donn�es � afficher");
      }
      /** car les histogrammes ne sont pas fait dans l'autre sens* */
      if (!rHistogramme_.isSelected()) {
        rEcluseX_.setSelected(false);
        rConsoX_.setSelected(true);

      }
    }

  }

  private void annuler() {
    imp_.removeInternalFrame(this);
    // imp_.ResetFille2ModBateaux();

  }

  /**
   * m�thode permettant de v�rifier si l'utilisateur a bien choisi une �cluse avant d'afficher le graphe.
   */
  private boolean verifParametres() {
    if (liListeEcluses_.getSelectedIndex() == -1) {
      imp_.affMessage("Selectionnez au moins une �cluse");
      return false;
    }
    return true;
  }

  private void afficherGraphe() {
    if (verifParametres()) {
      final BGraphe graphe = new BGraphe();
      String str = "graphe\n";
      final String deb = "{\n";
      str += deb;
      if (rConsom_.isSelected() && !comparaison_) {
        str += "titre \"Consommation d'eau ";
      } else if (rConsom_.isSelected() && comparaison_) {
        str += "titre \"Consommation d'eau de " + liListeEcluses_.getSelectedValue().toString();
      } else if (rBassinees_.isSelected() && !comparaison_) {
        str += "titre \"Bassin�es et fausse bassin�es ";
      } else if (rBassinees_.isSelected() && comparaison_) {
        str += "titre \"Bassin�es et fausse bassin�es de  " + liListeEcluses_.getSelectedValue().toString();
      } else if (rRemplissage_.isSelected() && !comparaison_) {
        str += "titre \"Taux de remplissage  ";
      } else if (rRemplissage_.isSelected() && comparaison_) {
        str += "titre \"Taux de remplissage de " + liListeEcluses_.getSelectedValue().toString();
      }
      if (cSensNav_.getSelectedIndex() == 0) {
        str += " dans les deux sens de navigation \"+ \n";
      } else if (cSensNav_.getSelectedIndex() == 1) {
        str += "en Montant \"+ \n";
      } else {
        str += " en Avalant \"+ \n";
      }
      str += "legende oui \n";
      str += "animation non \n";
      str += "marges\n";
      str += deb;
      str += "gauche 80\n";
      str += "droite 120\n";
      str += "haut 50\n";
      str += "bas 30\n";
      str += getEnd();

      final Object[] eclusesSel = liListeEcluses_.getSelectedValues();

      final String max15 = "maximum  15\n";
      final String min0 = "minimum  0\n";
      final String tab = "     ";
      if (rConsom_.isSelected()) {

        if (!comparaison_) {
          selectionDonneesConsommationBassinee(0);
        } else {
          listeCoordonnees_ = new ArrayList();
          for (int i = 0; i < imp_.simulationsSel_.length; i++) {
            donneesGraph_ = new ArrayList();
            selectionDonneesConsommationBassinee(i);
            listeCoordonnees_.add(donneesGraph_);
          }

        }
        if (rEcluseX_.isSelected()) {
          str += "axe\n";
          str += deb;
          str += "titre \"Types d'Elements\" ";
          str += "unite \"Element\" ";
          str += "orientation horizontal\n";
          str += "graduations non\n";
          str += min0;
          str += max15;
          str += getEnd();
          str += "axe\n";
          str += deb;
          str += "titre \"Consommation d'eau \" \n";
          str += "unite \"m3\" \n";
          str += "orientation vertical\n";
          str += "graduations oui\n";
          str += min0;
          final int maxcons = (int) maxConsommation();
          str += "maximum  " + (maxcons + maxcons / 20) + " \n";
          final int y = maxcons / 20;
          str += "pas " + y + CtuluLibString.LINE_SEP_SIMPLE;
          str += getEnd();
          str += "courbe\n";
          str += deb;
          str += "titre \"Consommation d'eau\"\n";
        } else if (rConsoX_.isSelected()) {
          str += "axe\n";
          str += deb;
          str += "titre \"Consommation d'eau \" \n";
          str += "unite \"m3\" \n";
          str += "orientation horizontal\n";
          str += "graduations oui\n";
          str += min0;
          final int maxcons = (int) maxConsommation();
          str += "maximum  " + (maxcons + maxcons / 20) + " \n";
          final int y = (int) maxcons / 20;
          str += "pas " + y + CtuluLibString.LINE_SEP_SIMPLE;
          str += getEnd();
          str += "axe\n";
          str += deb;
          str += "titre \"Types d'Elements\" ";
          str += "unite \"Element\" ";
          str += "orientation vertical\n";
          str += "graduations non\n";
          str += min0;
          str += max15;
          str += getEnd();
          str += "courbe\n";
          str += deb;
          str += "titre \"Consommation d'eau\"\n";
        }
        if (rCourbe_.isSelected()) {
          str += "type courbe\n";
        } else {
          str += "type histogramme\n";
        }
        str += "aspect\n";
        str += " {\n";
        if (rHistogramme_.isSelected()) {
          str += "contour.largeur 1 \n";
          str += "surface.couleur FF8800 \n";
          str += "texte.couleur FF8800 \n";
        } else {
          str += "contour.couleur FF8800\n";
        }
        str += getEnd();
        str += " valeurs\n";
        str += "  {\n";
        if (rEcluseX_.isSelected()) {
          if (!comparaison_) {
            final ListIterator it = donneesGraph_.listIterator();
            int h = 1;
            while (it.hasNext()) {
              final Sinavi2TypeConsommation cons = (Sinavi2TypeConsommation) it.next();
              /** deux sens s�l�ctionn�s* */
              if (cSensNav_.getSelectedIndex() == 0) {
                str += h + CtuluLibString.ESPACE + cons.getConsommationDEau() + CtuluLibString.LINE_SEP_SIMPLE;
                h++;
              }
              /** * sens montant s�l�ctionn�* */
              else if (cSensNav_.getSelectedIndex() == 0) {
                str += h + CtuluLibString.ESPACE + cons.getConsommationDEauMontant() + CtuluLibString.LINE_SEP_SIMPLE;
                h++;
              } else {
                str += h + CtuluLibString.ESPACE + cons.getConsommationDEauAvalant() + CtuluLibString.LINE_SEP_SIMPLE;
                h++;
              }
            }
          } else {
            int h = 1;
            while (h <= listeCoordonnees_.size()) {
              System.out.println("att : " + h);
              final ListIterator it = ((ArrayList) listeCoordonnees_.get(h - 1)).listIterator();
              while (it.hasNext()) {
                final Sinavi2TypeConsommation cons = (Sinavi2TypeConsommation) it.next();
                /** deux sens s�l�ctionn�s* */
                if (cSensNav_.getSelectedIndex() == 0) {
                  str += h + CtuluLibString.ESPACE + cons.getConsommationDEau() + CtuluLibString.LINE_SEP_SIMPLE;
                  h++;
                }
                /** * sens montant s�l�ctionn�* */
                else if (cSensNav_.getSelectedIndex() == 0) {
                  str += h + CtuluLibString.ESPACE + cons.getConsommationDEauMontant() + CtuluLibString.LINE_SEP_SIMPLE;
                  h++;
                } else {
                  str += h + CtuluLibString.ESPACE + cons.getConsommationDEauAvalant() + CtuluLibString.LINE_SEP_SIMPLE;
                  h++;
                }
              }
            }

          }
        } else if (rConsoX_.isSelected()) {
          if (!comparaison_) {
            final ListIterator it = donneesGraph_.listIterator();
            int h = 1;
            while (it.hasNext()) {
              final Sinavi2TypeConsommation cons = (Sinavi2TypeConsommation) it.next();
              /** deux sens s�l�ctionn�s* */
              if (cSensNav_.getSelectedIndex() == 0) {
                str += cons.getConsommationDEau() + "  " + h + CtuluLibString.LINE_SEP_SIMPLE;
                h++;
              }
              /** * sens montant s�l�ctionn�* */
              else if (cSensNav_.getSelectedIndex() == 0) {
                str += cons.getConsommationDEauMontant() + "   " + h + CtuluLibString.LINE_SEP_SIMPLE;
                h++;
              } else {
                str += cons.getConsommationDEauAvalant() + "  " + h + CtuluLibString.LINE_SEP_SIMPLE;
                h++;
              }
            }
          } else {
            int h = 1;
            while (h <= listeCoordonnees_.size()) {
              System.out.println("att : " + h);
              final ListIterator it = ((ArrayList) listeCoordonnees_.get(h - 1)).listIterator();
              while (it.hasNext()) {
                System.out.println("consommation : " + h);
                final Sinavi2TypeConsommation cons = (Sinavi2TypeConsommation) it.next();
                /** deux sens s�l�ctionn�s* */
                if (cSensNav_.getSelectedIndex() == 0) {
                  str += cons.getConsommationDEau() + "  " + h + CtuluLibString.LINE_SEP_SIMPLE;
                  h++;
                }
                /** * sens montant s�l�ctionn�* */
                else if (cSensNav_.getSelectedIndex() == 0) {
                  str += cons.getConsommationDEauMontant() + "  " + h + CtuluLibString.LINE_SEP_SIMPLE;
                  h++;
                } else {
                  str += cons.getConsommationDEauAvalant() + "  " + h + CtuluLibString.LINE_SEP_SIMPLE;
                  h++;
                }
              }
            }

          }
        }

        str += "  }\n";
        str += getEnd();
        str += "courbe\n";
        str += deb;
        str += "titre \"\"\n";
        str += "type courbe\n";
        str += "aspect\n";
        str += deb;
        str += "contour.couleur FFFFFF\n";
        str += "surface.couleur 888888\n";
        str += "texte.couleur 000000 \n";
        str += "contour.largeur 0\n";
        str += getEnd();
        str += "valeurs\n";
        str += "  {\n";

        final double z1 = maxConsommation() + maxConsommation() / 40;
        if (rEcluseX_.isSelected()) {
          if (!comparaison_) {
            for (int k = 0; k < eclusesSel.length; k++) {
              final int h = k + 1;
              System.out.println("test etiquette");
              str += h + tab + z1 + "\n   etiquette \n \"" + eclusesSel[h - 1].toString() + "\" \n ";
            }
          } else {
            for (int k = 0; k < imp_.simulationsSel_.length; k++) {
              final int h = k + 1;
              System.out.println("t-est etiquette");
              str += h + tab + z1 + "\n   etiquette \n \""
                  + ((SSimulationSinavi2) imp_.listeSimulationsSinavi2_.get(imp_.simulationsSel_[k])).nomSim + "\" \n ";
            }
          }
        } else if (rConsoX_.isSelected()) {
          if (!comparaison_) {
            for (int k = 0; k < eclusesSel.length; k++) {
              final int h = k + 1;
              System.out.println("test etiquette");
              str += z1 + tab + h + "\n   etiquette \n \"" + eclusesSel[h - 1].toString() + "\" \n ";
            }
          } else {
            for (int k = 0; k < imp_.simulationsSel_.length; k++) {
              final int h = k + 1;
              System.out.println("t-est etiquette");
              str += z1 + tab + h + "\n   etiquette \n \""
                  + ((SSimulationSinavi2) imp_.listeSimulationsSinavi2_.get(imp_.simulationsSel_[k])).nomSim + "\" \n ";
            }
          }
        }
        str += "  }\n";
        str += " }\n";

        if (cSeuil_.isSelected() && !tSeuil_.getValue().equals("0")) {
          System.out.println("je suis dans seuil");
          str += " contrainte\n";
          str += deb;
          // a mettre le seuil
          if (rConsoX_.isSelected()) {
            System.out.println("je suis dans �cluse");
            str += "titre \"Seuil = " + Double.parseDouble(tSeuil_.getText().replace(',', '.')) + "\"\n";
            str += "orientation verticale\n";
            str += " type max\n";
            str += " valeur     " + Double.parseDouble(tSeuil_.getText().replace(',', '.'))
                + CtuluLibString.LINE_SEP_SIMPLE;
          } else if (rEcluseX_.isSelected()) {
            str += "titre \"Seuil  = " + Double.parseDouble(tSeuil_.getText().replace(',', '.')) + "\"\n";
            str += " type max\n";
            str += " valeur     " + Double.parseDouble(tSeuil_.getText().replace(',', '.'))
                + CtuluLibString.LINE_SEP_SIMPLE;
          }
          str += getEnd();
          str += " \n";

        }
        str += "  } \"\n";

      }
      if (rBassinees_.isSelected()) {

        if (!comparaison_) {
          selectionDonneesConsommationBassinee(0);
        } else {
          listeCoordonnees_ = new ArrayList();
          for (int i = 0; i < imp_.simulationsSel_.length/* listeCons_.size() */; i++) {
            donneesGraph_ = new ArrayList();
            selectionDonneesConsommationBassinee(i);
            listeCoordonnees_.add(donneesGraph_);
          }
        }
        if (rEcluseX_.isSelected()) {
          str += "axe\n";
          str += deb;
          str += "titre \"Types d'Elements\" ";
          str += "unite \"Element\" ";
          str += "orientation horizontal\n";
          str += "graduations non\n";
          str += min0;
          str += max15;
          str += getEnd();
          str += "axe\n";
          str += deb;
          str += "titre \"Nombre de bassin�es total \" \n";
          str += "unite \"entier\" \n";
          str += "orientation vertical\n";
          str += "graduations oui\n";
          str += min0;
          final double maxbas = maxBassinee();
          str += "maximum  " + (maxbas + maxbas / 20) + " \n";
          final double y = maxbas / 20;
          str += "pas " + y + CtuluLibString.LINE_SEP_SIMPLE;
          str += getEnd();
          str += "courbe\n";
          str += deb;
          str += "titre \"Nb de bassin�es total\"\n";
        } else if (rBassinX_.isSelected()) {
          str += "axe\n";
          str += deb;
          str += "titre \"Nb de bassinees total \" \n";
          str += "unite \"entier\" \n";
          str += "orientation horizontal\n";
          str += "graduations oui\n";
          str += min0;
          final double maxbas = maxBassinee();
          str += "maximum  " + (maxbas + maxbas / 20) + " \n";
          final double y = maxbas / 20;
          str += "pas " + y + CtuluLibString.LINE_SEP_SIMPLE;
          str += getEnd();
          str += "axe\n";
          str += deb;
          str += "titre \"Types d'Elements\" ";
          str += "unite \"Element\" ";
          str += "orientation vertical\n";
          str += "graduations non\n";
          str += min0;
          str += max15;
          str += getEnd();
          str += "courbe\n";
          str += deb;
          str += "titre \"Nb bassin�es\"\n";
        }
        if (rCourbe_.isSelected()) {
          str += "type courbe\n";
        } else {
          str += "type histogramme\n";
        }
        str += "aspect\n";
        str += " {\n";
        if (rHistogramme_.isSelected()) {
          str += "contour.largeur 1 \n";
          str += "surface.couleur FF8800 \n";
          str += "texte.couleur FF8800 \n";
        } else {
          str += "contour.couleur FF8800\n";
        }
        str += getEnd();
        str += " valeurs\n";
        str += "  {\n";
        if (rEcluseX_.isSelected()) {
          if (!comparaison_) {
            final ListIterator it = donneesGraph_.listIterator();
            int h = 1;
            int total = 0;
            while (it.hasNext()) {
              System.out.println("bassinee : " + h);
              final Sinavi2TypeConsommation cons = (Sinavi2TypeConsommation) it.next();

              /** deux sens s�l�ctionn�s* */
              if (cSensNav_.getSelectedIndex() == 0) {
                total = cons.getNbTotalBassinees() + cons.getNbTotalFaussesBassinees();
                str += h + CtuluLibString.ESPACE + total + CtuluLibString.LINE_SEP_SIMPLE;
              }

              /** sens montant s�l�ectionn�* */
              else if (cSensNav_.getSelectedIndex() == 1) {
                total = cons.getNbBassineesMontantes() + cons.getNbFaussesBassineesMontantes();
                str += h + CtuluLibString.ESPACE + total + CtuluLibString.LINE_SEP_SIMPLE;

              }

              /** sens avalant s�l�ctionn�* */
              else {
                total = cons.getNbBassineesAvalantes() + cons.getNbFaussesBassineesAvalantes();
                str += h + CtuluLibString.ESPACE + total + CtuluLibString.LINE_SEP_SIMPLE;

              }
              h++;

            }

          } else {
            int h = 1;
            while (h <= listeCoordonnees_.size()) {
              System.out.println("att : " + h);
              int total = 0;
              final ListIterator it = ((ArrayList) listeCoordonnees_.get(h - 1)).listIterator();
              while (it.hasNext()) {
                System.out.println("bassinee : " + h);
                final Sinavi2TypeConsommation cons = (Sinavi2TypeConsommation) it.next();

                /** deux sens s�l�ctionn�s* */
                if (cSensNav_.getSelectedIndex() == 0) {
                  total = cons.getNbTotalBassinees() + cons.getNbTotalFaussesBassinees();
                  str += h + CtuluLibString.ESPACE + total + CtuluLibString.LINE_SEP_SIMPLE;
                }

                /** sens montant s�l�ectionn�* */
                else if (cSensNav_.getSelectedIndex() == 1) {
                  total = cons.getNbBassineesMontantes() + cons.getNbFaussesBassineesMontantes();
                  str += h + CtuluLibString.ESPACE + total + CtuluLibString.LINE_SEP_SIMPLE;

                }
                /** sens avalant s�l�ctionn�* */
                else {
                  total = cons.getNbBassineesAvalantes() + cons.getNbFaussesBassineesAvalantes();
                  str += h + CtuluLibString.ESPACE + total + CtuluLibString.LINE_SEP_SIMPLE;

                }
                h++;
              }
            }

          }
          str += "  }\n";
          str += getEnd();
          str += "courbe\n";
          str += deb;
          str += "titre \"Nb de fausses bassin�es\"\n";
          if (rCourbe_.isSelected()) {
            str += "type courbe\n";
          } else {
            str += "type histogramme\n";
          }
          str += "aspect\n";
          str += " {\n";
          if (rHistogramme_.isSelected()) {
            str += "contour.largeur 1 \n";
            str += "surface.couleur 444FFF \n";
            // str+="contour.couleur FFFFFF \n";
            str += "texte.couleur 444FFF \n";
          } else {
            str += "contour.couleur 444FFF\n";
          }
          str += getEnd();
          str += " valeurs\n";
          str += "  {\n";

          if (!comparaison_) {
            final ListIterator it = donneesGraph_.listIterator();
            int h = 1;
            while (it.hasNext()) {
              System.out.println("bassinee : " + h);
              final Sinavi2TypeConsommation cons = (Sinavi2TypeConsommation) it.next();
              cons.calculeTauxFaussesBassinees();
              /** deux sens s�l�ctionn�s* */
              if (cSensNav_.getSelectedIndex() == 0) {

                str += h + CtuluLibString.ESPACE + cons.getNbTotalFaussesBassinees() + "\n etiquette \n \""
                    + cons.getTauxFaussesBassineesTotal() + CtuluLibString.LINE_SEP_SIMPLE;
              }

              /** sens montant s�l�ectionn�* */
              else if (cSensNav_.getSelectedIndex() == 1) {

                str += h + CtuluLibString.ESPACE + cons.getNbFaussesBassineesMontantes() + "\n etiquette \n \""
                    + cons.getTauxFaussesBassineesMontantes() + CtuluLibString.LINE_SEP_SIMPLE;

              }

              /** sens avalant s�l�ctionn�* */
              else {

                str += h + CtuluLibString.ESPACE + cons.getNbFaussesBassineesAvalantes() + "\n etiquette \n \""
                    + cons.getTauxFaussesBassineesAvalantes() + CtuluLibString.LINE_SEP_SIMPLE;

              }

              h++;
            }

          } else {
            int h = 1;
            while (h <= listeCoordonnees_.size()) {
              System.out.println("att : " + h);
              final ListIterator it = ((ArrayList) listeCoordonnees_.get(h - 1)).listIterator();
              while (it.hasNext()) {
                System.out.println("bassinee : " + h);
                final Sinavi2TypeConsommation cons = (Sinavi2TypeConsommation) it.next();
                cons.calculeTauxFaussesBassinees();
                /** deux sens s�l�ctionn�s* */
                if (cSensNav_.getSelectedIndex() == 0) {

                  str += h + CtuluLibString.ESPACE + cons.getNbTotalFaussesBassinees() + "\n etiquette \n \""
                      + cons.getTauxFaussesBassineesTotal() + CtuluLibString.LINE_SEP_SIMPLE;

                }

                /** sens montant s�l�ectionn�* */
                else if (cSensNav_.getSelectedIndex() == 1) {

                  str += h + CtuluLibString.ESPACE + cons.getNbFaussesBassineesMontantes() + "\n etiquette \n \""
                      + cons.getTauxFaussesBassineesMontantes() + CtuluLibString.LINE_SEP_SIMPLE;
                }

                /** sens avalant s�l�ctionn�* */
                else {

                  str += h + CtuluLibString.ESPACE + cons.getNbFaussesBassineesAvalantes() + "\n etiquette \n \""
                      + cons.getTauxFaussesBassineesAvalantes() + CtuluLibString.LINE_SEP_SIMPLE;
                }

                h++;
              }
            }

          }
        } else if (rBassinX_.isSelected()) {
          int total = 0;
          if (!comparaison_) {
            final ListIterator it = donneesGraph_.listIterator();
            int h = 1;
            while (it.hasNext()) {
              System.out.println("bassinee : " + h);
              final Sinavi2TypeConsommation cons = (Sinavi2TypeConsommation) it.next();

              /** deux sens s�l�ctionn�s* */
              if (cSensNav_.getSelectedIndex() == 0) {
                total = cons.getNbTotalBassinees() + cons.getNbTotalFaussesBassinees();
                str += total + CtuluLibString.ESPACE + h + CtuluLibString.LINE_SEP_SIMPLE;
              }

              /** sens montant s�l�ectionn�* */
              else if (cSensNav_.getSelectedIndex() == 1) {
                total = cons.getNbBassineesMontantes() + cons.getNbFaussesBassineesMontantes();
                str += total + CtuluLibString.ESPACE + h + CtuluLibString.LINE_SEP_SIMPLE;

              }

              /** sens avalant s�l�ctionn�* */
              else {
                total = cons.getNbBassineesAvalantes() + cons.getNbFaussesBassineesAvalantes();
                str += total + CtuluLibString.ESPACE + h + CtuluLibString.LINE_SEP_SIMPLE;

              }
              h++;

            }

          } else {
            int h = 1;
            while (h <= listeCoordonnees_.size()) {
              System.out.println("att : " + h);
              int t = 0;
              final ListIterator it = ((ArrayList) listeCoordonnees_.get(h - 1)).listIterator();
              while (it.hasNext()) {
                System.out.println("bassinee : " + h);
                final Sinavi2TypeConsommation cons = (Sinavi2TypeConsommation) it.next();

                /** deux sens s�l�ctionn�s* */
                if (cSensNav_.getSelectedIndex() == 0) {
                  t = cons.getNbTotalBassinees() + cons.getNbTotalFaussesBassinees();
                  str += t + CtuluLibString.ESPACE + h + CtuluLibString.LINE_SEP_SIMPLE;
                }

                /** sens montant s�l�ectionn�* */
                else if (cSensNav_.getSelectedIndex() == 1) {
                  t = cons.getNbFaussesBassineesMontantes() + cons.getNbBassineesMontantes();
                  str += t + CtuluLibString.ESPACE + h + CtuluLibString.LINE_SEP_SIMPLE;

                }
                /** sens avalant s�l�ctionn�* */
                else {
                  t = cons.getNbBassineesAvalantes() + cons.getNbFaussesBassineesAvalantes();
                  str += t + CtuluLibString.ESPACE + h + CtuluLibString.LINE_SEP_SIMPLE;

                }
                h++;
              }
            }

          }
          str += "  }\n";
          str += getEnd();
          str += "courbe\n";
          str += deb;
          str += "titre \"Nb de fausses bassin�es\"\n";
          if (rCourbe_.isSelected()) {
            str += "type courbe\n";
          } else {
            str += "type histogramme\n";
          }
          str += "aspect\n";
          str += " {\n";
          if (rHistogramme_.isSelected()) {
            str += "contour.largeur 1 \n";
            str += "surface.couleur 444FFF \n";
            str += "texte.couleur 444FFF \n";
          } else {
            str += "contour.couleur 444FFF\n";
          }
          str += getEnd();
          str += " valeurs\n";
          str += "  {\n";

          if (!comparaison_) {
            final ListIterator it = donneesGraph_.listIterator();
            int h = 1;
            while (it.hasNext()) {
              System.out.println("bassinee : " + h);
              final Sinavi2TypeConsommation cons = (Sinavi2TypeConsommation) it.next();

              /** deux sens s�l�ctionn�s* */
              if (cSensNav_.getSelectedIndex() == 0) {

                str += cons.getNbTotalFaussesBassinees() + CtuluLibString.ESPACE + h + "\n etiquette \n \""
                    + cons.getTauxFaussesBassineesTotal() + CtuluLibString.LINE_SEP_SIMPLE;
              }

              /** sens montant s�l�ectionn�* */
              else if (cSensNav_.getSelectedIndex() == 1) {

                str += cons.getNbFaussesBassineesMontantes() + CtuluLibString.ESPACE + h + "\n etiquette \n \""
                    + cons.getTauxFaussesBassineesMontantes() + CtuluLibString.LINE_SEP_SIMPLE;

              }

              /** sens avalant s�l�ctionn�* */
              else {

                str += cons.getNbFaussesBassineesAvalantes() + CtuluLibString.ESPACE + h + "\n etiquette \n \""
                    + cons.getTauxFaussesBassineesAvalantes() + CtuluLibString.LINE_SEP_SIMPLE;

              }

              h++;
            }

          } else {
            int h = 1;
            while (h <= listeCoordonnees_.size()) {
              System.out.println("att : " + h);
              final ListIterator it = ((ArrayList) listeCoordonnees_.get(h - 1)).listIterator();
              while (it.hasNext()) {
                System.out.println("bassinee : " + h);
                final Sinavi2TypeConsommation cons = (Sinavi2TypeConsommation) it.next();

                /** deux sens s�l�ctionn�s* */
                if (cSensNav_.getSelectedIndex() == 0) {

                  str += cons.getNbTotalFaussesBassinees() + CtuluLibString.ESPACE + h + "\n etiquette \n \""
                      + cons.getTauxFaussesBassineesTotal() + CtuluLibString.LINE_SEP_SIMPLE;

                }

                /** sens montant s�l�ectionn�* */
                else if (cSensNav_.getSelectedIndex() == 1) {

                  str += cons.getNbFaussesBassineesMontantes() + CtuluLibString.ESPACE + h + "\n etiquette \n \""
                      + cons.getTauxFaussesBassineesMontantes() + CtuluLibString.LINE_SEP_SIMPLE;
                }

                /** sens avalant s�l�ctionn�* */
                else {

                  str += cons.getNbFaussesBassineesAvalantes() + CtuluLibString.ESPACE + h + "\n etiquette \n \""
                      + cons.getTauxFaussesBassineesAvalantes() + CtuluLibString.LINE_SEP_SIMPLE;
                }

                h++;
              }
            }

          }
        }
        str += "  }\n";
        str += getEnd();
        str += "courbe\n";
        str += deb;
        str += "titre \"\"\n";
        str += "type courbe\n";
        str += "aspect\n";
        str += deb;
        str += "contour.couleur FFFFFF\n";
        str += "surface.couleur FFFFFF\n";
        str += "texte.couleur 000000 \n";
        str += "contour.largeur 0\n";
        str += getEnd();
        str += "valeurs\n";
        str += "  {\n";
        final double max = maxBassinee();
        System.out.println("valeur de max " + max);
        final double z = max + max / 40;
        System.out.println("valeur de z1************************************************ " + z);
        if (rEcluseX_.isSelected()) {
          if (!comparaison_) {
            for (int k = 0; k < eclusesSel.length; k++) {
              final int h = k + 1;
              System.out.println("test etiquette");
              str += h + tab + z + "\n   etiquette \n \"" + eclusesSel[h - 1].toString() + "\" \n ";
            }
          } else {
            for (int k = 0; k < imp_.simulationsSel_.length; k++) {
              final int h = k + 1;
              System.out.println("t-est etiquette");
              str += h + tab + z + "\n   etiquette \n \""
                  + ((SSimulationSinavi2) imp_.listeSimulationsSinavi2_.get(imp_.simulationsSel_[k])).nomSim + "\" \n ";
            }
          }
        } else if (rBassinX_.isSelected()) {
          if (!comparaison_) {
            for (int k = 0; k < eclusesSel.length; k++) {
              final int h = k + 1;
              System.out.println("test etiquette");
              str += z + tab + h + "\n   etiquette \n \"" + eclusesSel[h - 1].toString() + "\" \n ";
            }
          } else {
            for (int k = 0; k < imp_.simulationsSel_.length; k++) {
              final int h = k + 1;
              System.out.println("t-est etiquette");
              str += z + tab + h + "\n   etiquette \n \""
                  + ((SSimulationSinavi2) imp_.listeSimulationsSinavi2_.get(imp_.simulationsSel_[k])).nomSim + "\" \n ";
            }
          }
        }
        str += "  }\n";
        str += " }\n";

        if (cSeuil_.isSelected() && !tSeuil_.getValue().equals("0")) {
          str += " contrainte\n";
          str += deb;
          // a mettre le seuil
          if (rBassinX_.isSelected()) {
            str += "titre \"Seuil = " + Double.parseDouble(tSeuil_.getText().replace(',', '.')) + "\"\n";
            str += "orientation verticale\n";
            str += " type max\n";
            str += " valeur     " + Double.parseDouble(tSeuil_.getText().replace(',', '.'))
                + CtuluLibString.LINE_SEP_SIMPLE;
          } else if (rEcluseX_.isSelected()) {
            str += "titre \"Seuil  = " + Double.parseDouble(tSeuil_.getText().replace(',', '.')) + "\"\n";
            str += " type max\n";
            str += " valeur     " + Double.parseDouble(tSeuil_.getText().replaceAll(",", CtuluLibString.DOT))
                + CtuluLibString.LINE_SEP_SIMPLE;
          }
          str += getEnd();
          str += " \n";
        }
        str += "  } \"\n";

      }

      if (rRemplissage_.isSelected()) {

        if (!comparaison_) {
          selectionDonneesRemplissage(0);
        } else {
          listeCoordonnees_ = new ArrayList();
          for (int i = 0; i < imp_.simulationsSel_.length; i++) {
            donneesGraph_ = new ArrayList();
            selectionDonneesRemplissage(i);
            listeCoordonnees_.add(donneesGraph_);

          }

        }
        if (rEcluseX_.isSelected()) {
          str += "axe\n";
          str += deb;
          str += "titre \"Types d'Elements\" ";
          str += "unite \"Element\" ";
          str += "orientation horizontal\n";
          str += "graduations non\n";
          str += min0;
          str += max15;
          str += getEnd();
          str += "axe\n";
          str += deb;
          str += "titre \"Taux de remplissage \" \n";
          str += "unite \"m3\" \n";
          str += "orientation vertical\n";
          str += "graduations oui\n";
          str += min0;
          final double maxremplissage = maxRemplissage();
          str += "maximum  " + (maxremplissage + maxremplissage / 20) + " \n";
          System.out.println("maaaaaaaaax remplissage " + maxremplissage);
          final double y = maxremplissage / 20;
          str += "pas " + y + "%" + CtuluLibString.LINE_SEP_SIMPLE;
          str += getEnd();
          str += "courbe\n";
          str += deb;
          str += "titre \"Taux de remplissage\"\n";
        } else if (rRemplissageX_.isSelected()) {
          str += "axe\n";
          str += deb;
          str += "titre \"Taux de remplissage \" \n";
          str += "unite \"%\" \n";
          str += "orientation horizontal\n";
          str += "graduations oui\n";
          str += min0;
          final double maxremplissage = maxRemplissage();
          str += "maximum  " + (maxremplissage + maxremplissage / 20) + " \n";
          System.out.println("maaaaaaaaax remplissage " + maxremplissage);
          final double y = maxremplissage / 20;
          str += "pas " + y + "%" + CtuluLibString.LINE_SEP_SIMPLE;
          str += getEnd();
          str += "axe\n";
          str += deb;
          str += "titre \"Types d'Elements\" ";
          str += "unite \"Element\" ";
          str += "orientation vertical\n";
          str += "graduations non\n";
          str += min0;
          str += max15;
          str += getEnd();
          str += "courbe\n";
          str += deb;
          str += "titre \"Taux de remplissage\"\n";
        }
        if (rCourbe_.isSelected()) {
          str += "type courbe\n";
        } else {
          str += "type histogramme\n";
        }
        str += "aspect\n";
        str += " {\n";
        if (rHistogramme_.isSelected()) {
          str += "contour.largeur 1 \n";
          str += "surface.couleur FF8800 \n";
          str += "texte.couleur FF8800 \n";
        } else {
          str += "contour.couleur FF8800\n";
        }
        str += getEnd();
        str += " valeurs\n";
        str += "  {\n";
        if (rEcluseX_.isSelected()) {
          if (!comparaison_) {
            final ListIterator it = donneesGraph_.listIterator();
            int h = 1;
            while (it.hasNext()) {
              System.out.println("consommation : " + h);
              String chaine = new String();
              final Sinavi2TypeConsommation cons = (Sinavi2TypeConsommation) it.next();

              /** deux sens s�l�ctionn�s* */
              if (cSensNav_.getSelectedIndex() == 0) {
                chaine = cons.getTauxRemplissageTotal() + "";
                str += h + CtuluLibString.ESPACE + chaine + CtuluLibString.LINE_SEP_SIMPLE;
                h++;
              }

              /** * sens montant s�l�ctionn�* */
              else if (cSensNav_.getSelectedIndex() == 0) {
                chaine = cons.getTauxRemplissageMontant() + "";
                str += h + CtuluLibString.ESPACE + chaine + CtuluLibString.LINE_SEP_SIMPLE;
                h++;
              } else {
                chaine = cons.getTauxRemplissageAvalant() + "";
                str += h + CtuluLibString.ESPACE + chaine + CtuluLibString.LINE_SEP_SIMPLE;
                h++;
              }
            }
          } else {
            int h = 1;
            while (h <= listeCoordonnees_.size()) {
              System.out.println("att : " + h);
              final ListIterator it = ((ArrayList) listeCoordonnees_.get(h - 1)).listIterator();
              while (it.hasNext()) {
                System.out.println("consommation : " + h);
                String chaine = new String();
                final Sinavi2TypeConsommation cons = (Sinavi2TypeConsommation) it.next();

                /** deux sens s�l�ctionn�s* */
                if (cSensNav_.getSelectedIndex() == 0) {
                  chaine = cons.getTauxRemplissageTotal() + "";
                  str += h + CtuluLibString.ESPACE + chaine + CtuluLibString.LINE_SEP_SIMPLE;
                  h++;
                }

                /** * sens montant s�l�ctionn�* */
                else if (cSensNav_.getSelectedIndex() == 0) {
                  chaine = cons.getTauxRemplissageMontant() + "";
                  str += h + CtuluLibString.ESPACE + chaine + CtuluLibString.LINE_SEP_SIMPLE;
                  h++;
                }

                else {
                  chaine = cons.getTauxRemplissageAvalant() + CtuluLibString.ESPACE;
                  str += h + CtuluLibString.ESPACE + chaine + CtuluLibString.LINE_SEP_SIMPLE;
                  h++;
                }
              }

            }

          }
        } else if (rRemplissage_.isSelected()) {
          if (!comparaison_) {
            final ListIterator it = donneesGraph_.listIterator();
            int h = 1;
            while (it.hasNext()) {
              System.out.println("consommation : " + h);
              String chaine = new String();
              final Sinavi2TypeConsommation cons = (Sinavi2TypeConsommation) it.next();

              /** deux sens s�l�ctionn�s* */
              if (cSensNav_.getSelectedIndex() == 0) {
                chaine = cons.getTauxRemplissageTotal() + "";
                str += chaine + CtuluLibString.ESPACE + h + CtuluLibString.LINE_SEP_SIMPLE;
                h++;
              }

              /** * sens montant s�l�ctionn�* */
              else if (cSensNav_.getSelectedIndex() == 0) {
                chaine = cons.getTauxRemplissageMontant() + "";
                str += chaine + CtuluLibString.ESPACE + h + CtuluLibString.LINE_SEP_SIMPLE;
                h++;
              } else {
                chaine = cons.getTauxRemplissageAvalant() + "";
                str += chaine + CtuluLibString.ESPACE + h + CtuluLibString.LINE_SEP_SIMPLE;
                h++;
              }
            }
          } else {
            int h = 1;
            while (h <= listeCoordonnees_.size()) {
              System.out.println("att : " + h);
              final ListIterator it = ((ArrayList) listeCoordonnees_.get(h - 1)).listIterator();
              while (it.hasNext()) {
                System.out.println("consommation : " + h);
                String chaine = new String();
                final Sinavi2TypeConsommation cons = (Sinavi2TypeConsommation) it.next();

                /** deux sens s�l�ctionn�s* */
                if (cSensNav_.getSelectedIndex() == 0) {
                  chaine = cons.getTauxRemplissageTotal() + "";
                  str += chaine + CtuluLibString.ESPACE + h + CtuluLibString.LINE_SEP_SIMPLE;
                  h++;
                }

                /** * sens montant s�l�ctionn�* */
                else if (cSensNav_.getSelectedIndex() == 0) {
                  chaine = cons.getTauxRemplissageMontant() + "";
                  str += chaine + CtuluLibString.ESPACE + h + CtuluLibString.LINE_SEP_SIMPLE;
                  h++;
                }

                else {
                  chaine = cons.getTauxRemplissageAvalant() + CtuluLibString.ESPACE;
                  str += chaine + CtuluLibString.ESPACE + h + CtuluLibString.LINE_SEP_SIMPLE;
                  h++;
                }
              }

            }

          }
        }
        str += "  }\n";
        str += getEnd();
        str += "courbe\n";
        str += deb;
        str += "titre \"\"\n";
        str += "type courbe\n";
        str += "aspect\n";
        str += deb;
        str += "contour.couleur FFFFFF\n";
        // str+="contour.couleur 888888\n";
        str += "surface.couleur FFFFFF\n";
        // str+="surface.couleur 888888\n";
        str += "texte.couleur 000000 \n";
        str += "contour.largeur 0\n";
        str += getEnd();
        str += "valeurs\n";
        str += "  {\n";

        final double z1 = maxRemplissage() + maxRemplissage() / 40;
        if (rEcluseX_.isSelected()) {
          if (!comparaison_) {
            for (int k = 0; k < eclusesSel.length; k++) {
              final int h = k + 1;
              System.out.println("test etiquette");
              str += h + tab + z1 + "\n   etiquette \n \"" + eclusesSel[h - 1].toString() + "\" \n ";
            }
          } else {
            for (int k = 0; k < imp_.simulationsSel_.length; k++) {
              final int h = k + 1;
              System.out.println("t-est etiquette");
              str += h + tab + z1 + "\n   etiquette \n \""
                  + ((SSimulationSinavi2) imp_.listeSimulationsSinavi2_.get(imp_.simulationsSel_[k])).nomSim + "\" \n ";
            }
          }
        } else if (rRemplissageX_.isSelected()) {
          if (!comparaison_) {
            for (int k = 0; k < eclusesSel.length; k++) {
              final int h = k + 1;
              str += z1 + tab + h + "\n   etiquette \n \"" + eclusesSel[h - 1].toString() + "\" \n ";
            }
          } else {
            for (int k = 0; k < imp_.simulationsSel_.length; k++) {
              final int h = k + 1;
              System.out.println("t-est etiquette");
              str += z1 + tab + h + "\n   etiquette \n \""
                  + ((SSimulationSinavi2) imp_.listeSimulationsSinavi2_.get(imp_.simulationsSel_[k])).nomSim + "\" \n ";
            }
          }
        }
        str += "  }\n";
        str += " }\n";
        if (cSeuil_.isSelected() && !tSeuil_.getValue().equals("0")) {
          str += " contrainte\n";
          str += deb;
          // a mettre le seuil
          if (rRemplissageX_.isSelected()) {

            str += "titre \"Seuil = " + Double.parseDouble(tSeuil_.getText().replace(',', '.')) + "\"\n";
            str += "orientation verticale\n";
            str += " type max\n";
            str += " valeur     " + Double.parseDouble(tSeuil_.getText().replace(',', '.'))
                + CtuluLibString.LINE_SEP_SIMPLE;
          } else if (rEcluseX_.isSelected()) {
            str += "titre \"Seuil  = " + Double.parseDouble(tSeuil_.getText().replace(',', '.')) + "\"\n";
            str += " type max\n";
            str += " valeur     " + Double.parseDouble(tSeuil_.getText().replace(',', '.'))
                + CtuluLibString.LINE_SEP_SIMPLE;
          }
          str += getEnd();
          str += " \n";
        }
        str += "  } \"\n";
      }

      str += "  }\n";
      str += " }\n";

      graphe.setFluxDonnees(new ByteArrayInputStream(str.getBytes()));
      imp_.affGraphe2(graphe);
    }
  }

  private String getEnd() {
    return "}\n";
  }

  /**
   * ** m�thode permettant de mettre dans la liste donneesGraph les donn�es soit de la consommation d'eau soit des
   * fausses bassin�es � afficher dans le tableau ou sur le graphe.
   */
  public void selectionDonneesConsommationBassinee(final int _numSim) {
    Object[] eclusesSel;
    ListIterator iter;
    donneesGraph_ = new ArrayList();
    if (!comparaison_) {
      // iter=listeCons_.listIterator();
      iter = ((ArrayList) listeCons_.get(_numSim)).listIterator();
      eclusesSel = liListeEcluses_.getSelectedValues();
    } else {
      eclusesSel = new Object[1];
      eclusesSel[0] = liListeEcluses_.getSelectedValue();
      iter = ((ArrayList) (listeCons_.get(_numSim))).listIterator();
    }
    while (iter.hasNext()) {
      Sinavi2TypeConsommation cons = new Sinavi2TypeConsommation();
      cons = (Sinavi2TypeConsommation) iter.next();
      for (int i = 0; i < eclusesSel.length; i++) {
        if (eclusesSel[i].toString().equalsIgnoreCase(cons.ecluse_)) {
          donneesGraph_.add(cons);
        }
      }
    }
  }

  /**
   * m�thode permettant de mettre dans la liste donneesGraph les donn�es concernant le taux de remplissage � afficher
   * dans le tableau ou sur le graphe.
   */
  public void selectionDonneesRemplissage(final int _numSim) {
    Object[] eclusesSel;
    ListIterator iter;
    donneesGraph_ = new ArrayList();
    if (!comparaison_) {
      DonneesRemplissage();
      iter = listeRemplissage_.listIterator();
      eclusesSel = liListeEcluses_.getSelectedValues();
    } else {
      DonneesRemplissageC();
      eclusesSel = new Object[1];
      eclusesSel[0] = liListeEcluses_.getSelectedValue();
      iter = ((ArrayList) (listeRemplissage_.get(_numSim))).listIterator();
    }
    while (iter.hasNext()) {
      final Sinavi2TypeConsommation cons = (Sinavi2TypeConsommation) iter.next();
      for (int i = 0; i < eclusesSel.length; i++) {
        if (eclusesSel[i].toString().equalsIgnoreCase(cons.ecluse_)) {
          donneesGraph_.add(cons);
        }
      }
    }
  }

  private boolean simSel(final int indice) {
    for (int i = 0; i < imp_.simulationsSel_.length; i++) {
      if (imp_.simulationsSel_[i] == indice) {
        return true;
      }
    }
    return false;
  }

  /**
   * m�thode permettant de mettre dans listeRemplissage les donn�es calcul�es du taux de remplissage dans le cas d'une
   * comparaison de simulation.
   */
  public void DonneesRemplissageC() {
    listeRemplissage_ = new ArrayList();
    for (int r = 0; r < imp_.simulationsSel_.length; r++) {
      // if(simSel(r)){
      final ArrayList RempliTemp = new ArrayList();
      final SParametresEcluse[] tempEcluses = ((SSimulationSinavi2) listeSimulations_.get(r)).parametresEcluse;
      final SParametresBateau[] tempBateaux = ((SSimulationSinavi2) listeSimulations_.get(r)).parametresBateau;
      final SResultatConsommationDEau[] tempConso = ((SSimulationSinavi2) listeSimulations_.get(r)).resultatConsommationDEau;

      for (int i = 1; i < tempEcluses.length + 1; i++) {
        final ArrayList listetemp = new ArrayList();
        final Sinavi2TypeConsommation cons2 = new Sinavi2TypeConsommation();
        double remplissageAvalant = 0;
        double remplissageMontant = 0;
        double tauxRemplissageAvalant = 0;
        double tauxRemplissageMontant = 0;

        for (int j = 0; j < tempConso.length; j++) {
          if (i == tempConso[j].numeroEcluse && tempConso[j].typeBateau != 0) {
            listetemp.add(tempConso[j]);
          }
        }

        cons2.listeNombreBateauxAvalant_ = new int[tempBateaux.length];
        cons2.listeTypesBateaux_ = new int[tempBateaux.length];
        cons2.listeNombreBateauxMontant_ = new int[tempBateaux.length];

        for (int d = 1; d < tempBateaux.length + 1; d++) {
          for (int f = 0; f < listetemp.size(); f++) {
            if (d == ((SResultatConsommationDEau) listetemp.get(f)).typeBateau) {
              final SResultatConsommationDEau cons3 = ((SResultatConsommationDEau) listetemp.get(f));
              final Sinavi2TypeConsommationDEau cons = new Sinavi2TypeConsommationDEau(cons3);
              final int total = cons.getNbBateaux() / imp_.NB_SIMULATIONS;
              if (cons.avalantMontant_ == 'A') {
                cons2.listeNombreBateauxAvalant_[d - 1] = cons2.listeNombreBateauxAvalant_[d - 1] + total;
              } else {
                cons2.listeNombreBateauxMontant_[d - 1] = cons2.listeNombreBateauxMontant_[d - 1] + total;
              }
            }
          }
          cons2.listeTypesBateaux_[d - 1] = d;
        }
        for (int a = 0; a < cons2.listeTypesBateaux_.length; a++) {
          int longueur = (int) tempBateaux[a].longueur;

          if ((longueur % 5) == 0) {
            longueur = longueur / 5;
          } else {
            longueur = longueur / 5 + 1;
          }
          remplissageAvalant = remplissageAvalant
              + (tempBateaux[a].largeur * cons2.listeNombreBateauxAvalant_[a] * longueur);
          remplissageMontant = remplissageMontant
              + (tempBateaux[a].largeur * cons2.listeNombreBateauxMontant_[a] * longueur);
        }

        tauxRemplissageAvalant = remplissageAvalant / (tempEcluses[i - 1].longueur * tempEcluses[i - 1].largeur) * 100;
        tauxRemplissageMontant = remplissageMontant / (tempEcluses[i - 1].longueur * tempEcluses[i - 1].largeur) * 100;
        cons2.setEcluse(tempEcluses[i - 1].identification);
        cons2.setTauxRemplissageAvalant(Sinavi2Lib.conversionDeuxChiffres(tauxRemplissageAvalant));
        cons2.setTauxRemplissageMontant(Sinavi2Lib.conversionDeuxChiffres(tauxRemplissageMontant));
        cons2.setTauxRemplissageTotal(cons2.tauxRemplissageAvalant_ + cons2.tauxRemplissageMontant_);
        RempliTemp.add(cons2);
      }
      listeRemplissage_.add(RempliTemp);
    }

  }

  /**
   * m�thode permettant de mettre dans listeRemplissage les donn�es calcul�es du taux de remplissage dans le cas d'une
   * seule simulation.
   */

  public void DonneesRemplissage() {
    listeRemplissage_ = new ArrayList();
    for (int i = 1; i < listeEcluses_.size() + 1; i++) {
      final ArrayList listetemp = new ArrayList();
      final Sinavi2TypeConsommation cons2 = new Sinavi2TypeConsommation();
      double remplissageAvalant = 0;
      double remplissageMontant = 0;
      double tauxRemplissageAvalant = 0;
      double tauxRemplissageMontant = 0;

      for (int j = 0; j < listeConsommation_.size(); j++) {
        if (i == ((SResultatConsommationDEau) listeConsommation_.get(j)).numeroEcluse
            && (((SResultatConsommationDEau) listeConsommation_.get(j)).typeBateau != 0)) {
          listetemp.add(listeConsommation_.get(j));
        }
      }

      cons2.listeNombreBateauxAvalant_ = new int[listeBateaux_.size()];
      cons2.listeTypesBateaux_ = new int[listeBateaux_.size()];
      cons2.listeNombreBateauxMontant_ = new int[listeBateaux_.size()];

      for (int d = 1; d < listeBateaux_.size() + 1; d++) {
        for (int f = 0; f < listetemp.size(); f++) {
          if (d == ((SResultatConsommationDEau) listetemp.get(f)).typeBateau) {
            final SResultatConsommationDEau cons3 = ((SResultatConsommationDEau) listetemp.get(f));
            final Sinavi2TypeConsommationDEau cons = new Sinavi2TypeConsommationDEau(cons3);
            final int total = cons.getNbBateaux() / imp_.NB_SIMULATIONS;
            if (cons.avalantMontant_ == 'A') {
              cons2.listeNombreBateauxAvalant_[d - 1] = cons2.listeNombreBateauxAvalant_[d - 1] + total;
            } else {
              cons2.listeNombreBateauxMontant_[d - 1] = cons2.listeNombreBateauxMontant_[d - 1] + total;
            }
          }
        }
        cons2.listeTypesBateaux_[d - 1] = d;
      }

      for (int a = 0; a < cons2.listeTypesBateaux_.length; a++) {
        int longueur = (int) ((SParametresBateau) listeBateaux_.get(a)).longueur;
        System.out.println("longueur avant  " + "******************************" + longueur);
        if ((longueur % 5) == 0) {
          longueur = longueur / 5;
        } else {
          longueur = longueur / 5 + 1;
        }
        System.out.println("apr�s  " + "******************************" + longueur);
        remplissageAvalant = remplissageAvalant
            + (((SParametresBateau) listeBateaux_.get(a)).largeur * cons2.listeNombreBateauxAvalant_[a] * longueur);
        remplissageMontant = remplissageMontant
            + (((SParametresBateau) listeBateaux_.get(a)).largeur * cons2.listeNombreBateauxMontant_[a] * longueur);
      }

      tauxRemplissageAvalant = remplissageAvalant
          / (((SParametresEcluse) listeEcluses_.get(i - 1)).longueur * ((SParametresEcluse) listeEcluses_.get(i - 1)).largeur)
          * 100;
      tauxRemplissageMontant = remplissageMontant
          / (((SParametresEcluse) listeEcluses_.get(i - 1)).longueur * ((SParametresEcluse) listeEcluses_.get(i - 1)).largeur)
          * 100;
      cons2.setEcluse(((SParametresEcluse) listeEcluses_.get(i - 1)).identification);
      cons2.setTauxRemplissageAvalant(Sinavi2Lib.conversionDeuxChiffres(tauxRemplissageAvalant));
      cons2.setTauxRemplissageMontant(Sinavi2Lib.conversionDeuxChiffres(tauxRemplissageMontant));
      cons2.setTauxRemplissageTotal(cons2.tauxRemplissageAvalant_ + cons2.tauxRemplissageMontant_);
      listeRemplissage_.add(cons2);
    }

  }

  /** ** m�thode permettant de calculer la consommation maximum. ** */
  public double maxConsommation() {
    int consommation = 0;

    if (!comparaison_) {
      // ListIterator iter=listeCons_.listIterator();
      final ListIterator iter = ((ArrayList) listeCons_.get(0)).listIterator();
      while (iter.hasNext()) {
        final Sinavi2TypeConsommation conso = new Sinavi2TypeConsommation((Sinavi2TypeConsommation) iter.next());
        if (conso.consommationDEau_ > consommation) {
          consommation = (int) conso.consommationDEau_;
        }
      }

    } else {
      for (int i = 0; i < listeCons_.size(); i++) {
        final ListIterator iter = ((ArrayList) (listeCons_.get(i))).listIterator();
        while (iter.hasNext()) {
          final Sinavi2TypeConsommation conso = new Sinavi2TypeConsommation((Sinavi2TypeConsommation) iter.next());
          if (conso.consommationDEau_ > consommation) {
            consommation = (int) conso.consommationDEau_;
          }

        }
      }
    }
    return consommation;
  }

  /** ** m�thode permettant de calculer le taux de remplissage maximum. ** */
  public double maxRemplissage() {
    double maxRemplissage = 0;
    if (!comparaison_) {
      final ListIterator iter = listeRemplissage_.listIterator();
      while (iter.hasNext()) {
        final Sinavi2TypeConsommation conso = new Sinavi2TypeConsommation((Sinavi2TypeConsommation) iter.next());
        if (conso.tauxRemplissageTotal_ > maxRemplissage) {
          maxRemplissage = conso.tauxRemplissageTotal_;
        }
      }

    } else {
      for (int i = 0; i < listeRemplissage_.size(); i++) {
        final ListIterator iter = ((ArrayList) (listeRemplissage_.get(i))).listIterator();
        while (iter.hasNext()) {
          final Sinavi2TypeConsommation conso = new Sinavi2TypeConsommation((Sinavi2TypeConsommation) iter.next());
          if (conso.tauxRemplissageTotal_ > maxRemplissage) {
            maxRemplissage = conso.tauxRemplissageTotal_;
          }

        }
      }
    }
    return maxRemplissage;
  }

  /** ** m�thode permettant de calculer le nombre de bassin�es maximum. ** */
  public int maxBassinee() {
    int bassinees = 0;
    if (!comparaison_) {
      final ListIterator iter = ((ArrayList) listeCons_.get(0)).listIterator();
      while (iter.hasNext()) {
        final Sinavi2TypeConsommation conso = new Sinavi2TypeConsommation((Sinavi2TypeConsommation) iter.next());
        final int total = conso.nbTotalBassinees_ + conso.nbTotalFaussesBassinees_;
        if (total > bassinees) {
          bassinees = total;
        }
      }
      return bassinees;
    }
    for (int i = 0; i < listeCons_.size(); i++) {
      final ListIterator iter = ((ArrayList) (listeCons_.get(i))).listIterator();
      while (iter.hasNext()) {
        final Sinavi2TypeConsommation conso = new Sinavi2TypeConsommation((Sinavi2TypeConsommation) iter.next());
        final int total = conso.nbTotalBassinees_ + conso.nbTotalFaussesBassinees_;
        if (total > bassinees) {
          bassinees = total;
        }
      }
    }

    return bassinees;
  }

  /**
   * * classe permettant l'affichage soit de la consommation d'eau, soit des fausses bassin�es, soit du taux de
   * remplissage. ***
   */
  public class Sinavi2TableauResConsommationDEau extends BuInternalFrame implements ActionListener,
      InternalFrameListener {

    private final BuLabel lTitre1_ = new BuLabel("Consommation D'eau");

    private final BuButton bAnnuler_ = new BuButton(Sinavi2Resource.SINAVI2.getIcon("QUITTER"), "Quitter");
    private final BuButton bImprimer_ = new BuButton(Sinavi2Resource.SINAVI2.getIcon("IMPRIMER"), "Imprimer");

    /** * panel contenant les boutons, il est plac� en bas. */

    private final BuPanel pBoutons1_ = new BuPanel();
    private final BuPanel pTitre1_ = new BuPanel();
    private final BuPanel pDonnees_ = new BuPanel();
    public BuScrollPane scrollPane_;
    public Sinavi2TableauConsommationDEau tb1_;
    public Sinavi2TableauBassinee tb2_;
    public Sinavi2TableauRemplissage tb3_;
    public BuTable table1_;
    public BuTable table2_;
    public BuTable table3_;
    public Sinavi2Implementation imp2_;

    // public boolean comparaison_;
    public Sinavi2TableauResConsommationDEau(final BuCommonImplementation _appli, final boolean _comparaison) {
      super("Affichage de la consommation d'eau par �cluse", true, true, true, false);
      pTitre1_.add(lTitre1_);
      // comparaison_=_comparaison;

      if (rConsom_.isSelected()) {

        tableauConsommation();
        table1_.setRowSelectionAllowed(true);
        table1_.setColumnSelectionAllowed(false);

        table1_.getColumnModel().getColumn(0).setPreferredWidth(100);
        table1_.getColumnModel().getColumn(1).setPreferredWidth(100);

        scrollPane_ = new BuScrollPane(table1_);
        pDonnees_.add(scrollPane_);
        table1_.setAutoscrolls(true);
      } else if (rBassinees_.isSelected()) {

        tableauBassinee();
        table2_.setRowSelectionAllowed(true);
        table2_.setColumnSelectionAllowed(false);

        table2_.getColumnModel().getColumn(0).setPreferredWidth(100);
        table2_.getColumnModel().getColumn(1).setPreferredWidth(100);
        table2_.getColumnModel().getColumn(2).setPreferredWidth(125);

        scrollPane_ = new BuScrollPane(table2_);
        pDonnees_.add(scrollPane_);
        table2_.setAutoscrolls(true);
      } else if (rRemplissage_.isSelected()) {
        // DonneesRemplissage();
        tableauRemplissage();
        table3_.setRowSelectionAllowed(true);
        table3_.setColumnSelectionAllowed(false);

        table3_.getColumnModel().getColumn(0).setPreferredWidth(100);
        table3_.getColumnModel().getColumn(1).setPreferredWidth(150);
        scrollPane_ = new BuScrollPane(table3_);
        pDonnees_.add(scrollPane_);
        table3_.setAutoscrolls(true);
      }
      imp2_ = (Sinavi2Implementation) _appli.getImplementation();

      bAnnuler_.addActionListener(this);
      bImprimer_.addActionListener(this);

      ((JComponent) getContentPane()).setBorder(new EmptyBorder(25, 25, 25, 25));

      pBoutons1_.setLayout(new FlowLayout(FlowLayout.CENTER));
      pBoutons1_.add(bAnnuler_);
      pBoutons1_.add(bImprimer_);
      getContentPane().add(pTitre1_, BorderLayout.NORTH);
      getContentPane().add(pDonnees_, BorderLayout.CENTER);
      getContentPane().add(pBoutons1_, BorderLayout.SOUTH);

      // pack();
      setLocation(20, 20);

      /** *reinitialisation des valeurs* */

      setVisible(true);
      addInternalFrameListener(this);
      imp2_.addInternalFrame(this);

    }

    /**
     * tableau permettant d'afficher la consommation des �cluses s�lectionn�s dans le cas d'une seule simulation ou la
     * consommation d'eau d'une seule �cluse dans le cas d'une comparaison de simulation.
     */
    public void tableauConsommation() {
      int row = 0;
      String s = new String();
      if (tb1_ == null) {
        tb1_ = new Sinavi2TableauConsommationDEau();
      }
      if (!comparaison_) {
        selectionDonneesConsommationBassinee(0);
      } else {
        listeCoordonnees_ = new ArrayList();
        for (int i = 0; i < imp_.simulationsSel_.length; i++) {
          donneesGraph_ = new ArrayList();
          selectionDonneesConsommationBassinee(i);
          listeCoordonnees_.add(donneesGraph_);
        }

      }
      if (!comparaison_) {
        final ListIterator iter = donneesGraph_.listIterator();
        while (iter.hasNext()) {
          final Sinavi2TypeConsommation cons = new Sinavi2TypeConsommation((Sinavi2TypeConsommation) iter.next());

          s = cons.getEcluse();
          tb1_.setValueAt(s, row, 0);

          /** * deux sens consid�r�s** */
          if (cSensNav_.getSelectedIndex() == 0) {
            s = "" + (int) cons.getConsommationDEau();
            tb1_.setValueAt(s, row, 1);
          }

          /** * sens montant consid�r�** */
          else if (cSensNav_.getSelectedIndex() == 1) {
            s = "" + (int) cons.getConsommationDEauMontant();
            tb1_.setValueAt(s, row, 1);
          }

          /** *sens avalant consid�r�** */
          else {
            s = "" + (int) cons.getConsommationDEauAvalant();
            tb1_.setValueAt(s, row, 1);
          }
          row++;
          tb1_.setNbRow(row);
        }

      }

      else if (comparaison_) {
        for (int h = 0; h < listeCoordonnees_.size(); h++) {
          final ListIterator it2 = ((ArrayList) listeCoordonnees_.get(h)).listIterator();
          while (it2.hasNext()) {

            final Sinavi2TypeConsommation cons = new Sinavi2TypeConsommation((Sinavi2TypeConsommation) it2.next());
            s = ((SSimulationSinavi2) listeSimulations_.get(imp_.simulationsSel_[h])).nomSim + " : " + cons.getEcluse();
            tb1_.setValueAt(s, row, 0);

            /** * deux sens consid�r�s** */
            if (cSensNav_.getSelectedIndex() == 0) {
              s = "" + (int) cons.getConsommationDEau();
              tb1_.setValueAt(s, row, 1);
            }

            /** * sens montant consid�r�** */
            else if (cSensNav_.getSelectedIndex() == 1) {
              s = "" + (int) cons.getConsommationDEauMontant();
              tb1_.setValueAt(s, row, 1);
            }

            /** *sens avalant consid�r�** */
            else {
              s = "" + (int) cons.getConsommationDEauAvalant();
              tb1_.setValueAt(s, row, 1);
            }
            row++;
            tb1_.setNbRow(row);
          }
        }
      }

      table1_ = new BuTable(tb1_.data_, tb1_.nomCol());
      table1_.repaint();

    }

    /**
     * tableau permettant d'afficher le total(bassin�es et fausses), le nombre de fausses bassin�es, le pourcentage des
     * fausses bassin�es des �cluses s�lectionn�es.
     */
    public void tableauBassinee() {
      int row = 0;
      String s = new String();
      if (tb2_ == null) {
        tb2_ = new Sinavi2TableauBassinee();
      }
      if (!comparaison_) {
        selectionDonneesConsommationBassinee(0);
      } else {
        listeCoordonnees_ = new ArrayList();
        for (int i = 0; i < imp_.simulationsSel_.length; i++) {
          donneesGraph_ = new ArrayList();
          selectionDonneesConsommationBassinee(i);
          listeCoordonnees_.add(donneesGraph_);
        }

      }
      if (!comparaison_) {
        final ListIterator iter = donneesGraph_.listIterator();
        while (iter.hasNext()) {
          final Sinavi2TypeConsommation cons = new Sinavi2TypeConsommation((Sinavi2TypeConsommation) iter.next());
          cons.calculeTauxFaussesBassinees();
          s = cons.getEcluse();
          tb2_.setValueAt(s, row, 0);

          /** dans les deux sens* */
          if (cSensNav_.getSelectedIndex() == 0) {
            s = "" + cons.getNbTotalBassinees();
            tb2_.setValueAt(s, row, 1);
            s = "" + cons.getNbTotalFaussesBassinees();
            tb2_.setValueAt(s, row, 2);
            s = "" + cons.getTauxFaussesBassineesTotal();
            tb2_.setValueAt(s, row, 3);
            s = (cons.getNbTotalBassinees() + cons.getNbTotalFaussesBassinees()) + "";
            tb2_.setValueAt(s, row, 4);
          }

          /** sens montant* */
          else if (cSensNav_.getSelectedIndex() == 1) {
            s = "" + cons.getNbBassineesMontantes();
            tb2_.setValueAt(s, row, 1);
            s = "" + cons.getNbFaussesBassineesMontantes();
            tb2_.setValueAt(s, row, 2);
            s = "" + cons.getTauxFaussesBassineesMontantes();
            tb2_.setValueAt(s, row, 3);
            s = (cons.getNbBassineesMontantes() + cons.getNbFaussesBassineesMontantes()) + "";
            tb2_.setValueAt(s, row, 4);
          }

          else {
            s = "" + cons.getNbBassineesAvalantes();
            tb2_.setValueAt(s, row, 1);
            s = "" + cons.getNbFaussesBassineesAvalantes();
            tb2_.setValueAt(s, row, 2);
            s = "" + cons.getTauxFaussesBassineesAvalantes();
            tb2_.setValueAt(s, row, 3);
            s = (cons.getNbBassineesAvalantes() + cons.getNbFaussesBassineesAvalantes()) + "";
            tb2_.setValueAt(s, row, 4);
          }

          row++;
          tb2_.setNbRow(row);
        }
      } else {

        for (int h = 0; h < listeCoordonnees_.size(); h++) {
          final ListIterator it2 = ((ArrayList) listeCoordonnees_.get(h)).listIterator();
          while (it2.hasNext()) {
            final Sinavi2TypeConsommation cons = new Sinavi2TypeConsommation((Sinavi2TypeConsommation) it2.next());
            cons.calculeTauxFaussesBassinees();
            s = ((SSimulationSinavi2) listeSimulations_.get(imp_.simulationsSel_[h])).nomSim + " : " + cons.getEcluse();
            tb2_.setValueAt(s, row, 0);

            /** dans les deux sens* */
            if (cSensNav_.getSelectedIndex() == 0) {
              s = "" + cons.getNbTotalBassinees();
              System.out.println("valeur de bassin�e*******************" + " : " + s);
              tb2_.setValueAt(s, row, 1);
              s = "" + cons.getNbTotalFaussesBassinees();
              System.out.println("valeur de fausse bassin�e**********************" + " :" + s);
              tb2_.setValueAt(s, row, 2);
              s = "" + cons.tauxFaussesBassineesTotal_;
              tb2_.setValueAt(s, row, 3);
              s = (cons.getNbTotalBassinees() + cons.getNbTotalFaussesBassinees()) + "";
              tb2_.setValueAt(s, row, 4);

            }

            /** sens monatant* */
            if (cSensNav_.getSelectedIndex() == 1) {
              s = "" + cons.getNbBassineesMontantes();
              tb2_.setValueAt(s, row, 1);
              s = "" + cons.getNbFaussesBassineesMontantes();
              tb2_.setValueAt(s, row, 2);
              s = "" + cons.tauxFaussesBassineesMontantes_;
              tb2_.setValueAt(s, row, 3);
              s = (cons.getNbBassineesMontantes() + cons.getNbFaussesBassineesMontantes()) + "";
              tb2_.setValueAt(s, row, 4);
            }

            else {
              s = "" + cons.getNbBassineesAvalantes();
              tb2_.setValueAt(s, row, 1);
              s = "" + cons.getNbFaussesBassineesAvalantes();
              tb2_.setValueAt(s, row, 2);
              s = "" + cons.tauxFaussesBassineesAvalantes_;
              tb2_.setValueAt(s, row, 3);
              s = (cons.getNbBassineesAvalantes() + cons.getNbFaussesBassineesAvalantes()) + "";
              tb2_.setValueAt(s, row, 4);
            }
            row++;
            tb2_.setNbRow(row);
          }
        }
      }

      table2_ = new BuTable(tb2_.data_, tb2_.nomCol());
      table2_.repaint();

    }

    /**
     * tableau permattant d'afficher le taux de remplissage pour une �cluse s�lectionn�e dans le cas d'une comparaison
     * de simulation et des �cluses s�lectionn�es dans le cas d'une seule simulation.
     */
    public void tableauRemplissage() {
      int row = 0;
      String s = new String();
      if (tb3_ == null) {
        tb3_ = new Sinavi2TableauRemplissage();
      }
      if (!comparaison_) {
        selectionDonneesRemplissage(0);
      } else {
        listeCoordonnees_ = new ArrayList();
        for (int i = 0; i < imp_.simulationsSel_.length; i++) {
          donneesGraph_ = new ArrayList();
          selectionDonneesRemplissage(i);
          listeCoordonnees_.add(donneesGraph_);
        }

      }
      if (!comparaison_) {
        final ListIterator iter = donneesGraph_.listIterator();
        while (iter.hasNext()) {
          final Sinavi2TypeConsommation cons = new Sinavi2TypeConsommation((Sinavi2TypeConsommation) iter.next());
          s = cons.getEcluse();
          tb3_.setValueAt(s, row, 0);

          /** dans les deux sens* */
          if (cSensNav_.getSelectedIndex() == 0) {
            s = "" + Sinavi2Lib.conversionDeuxChiffres(cons.getTauxRemplissageTotal());
            tb3_.setValueAt(s, row, 1);

          }

          /** sens montant* */
          else if (cSensNav_.getSelectedIndex() == 1) {
            s = "" + Sinavi2Lib.conversionDeuxChiffres(cons.getTauxRemplissageMontant());
            tb3_.setValueAt(s, row, 1);

          }

          else {
            s = "" + Sinavi2Lib.conversionDeuxChiffres(cons.getTauxRemplissageAvalant());
            tb3_.setValueAt(s, row, 1);

          }

          row++;
          tb3_.setNbRow(row);
        }
      } else {
        for (int h = 0; h < listeCoordonnees_.size(); h++) {
          final ListIterator it2 = ((ArrayList) listeCoordonnees_.get(h)).listIterator();
          while (it2.hasNext()) {
            final Sinavi2TypeConsommation cons = new Sinavi2TypeConsommation((Sinavi2TypeConsommation) it2.next());
            s = ((SSimulationSinavi2) listeSimulations_.get(imp_.simulationsSel_[h])).nomSim + " : " + cons.getEcluse();
            tb3_.setValueAt(s, row, 0);

            /** dans les deux sens* */
            if (cSensNav_.getSelectedIndex() == 0) {
              s = "" + Sinavi2Lib.conversionDeuxChiffres(cons.getTauxRemplissageTotal());
              tb3_.setValueAt(s, row, 1);

            }

            /** sens montant* */
            else if (cSensNav_.getSelectedIndex() == 1) {
              s = "" + Sinavi2Lib.conversionDeuxChiffres(cons.getTauxRemplissageMontant());
              tb3_.setValueAt(s, row, 1);

            }

            else {
              s = "" + Sinavi2Lib.conversionDeuxChiffres(cons.getTauxRemplissageAvalant());
              tb3_.setValueAt(s, row, 1);

            }
            row++;
            tb3_.setNbRow(row);
          }
        }
      }
      table3_ = new BuTable(tb3_.data_, tb3_.nomCol());
      table3_.repaint();

    }

    /**
     * @param _e
     */

    public void annuler() {
      imp2_.removeInternalFrame(this);
      imp2_.resetFille2AffBateaux();
    }

    public void actionPerformed(final ActionEvent _e) {
      if (_e.getSource() == bAnnuler_) {
        annuler();

      }

      else if (_e.getSource() == bImprimer_) {
        printFile();

      }

    }

    private void printFile() {
      final File s = imp2_.enregistrerXls();
      if (s == null) {
        return;
      }
      CtuluTableModelInterface model = null;
      if (rConsom_.isSelected()) {
        final Sinavi2TableauConsommationDEau tbtemp = new Sinavi2TableauConsommationDEau();
        model = tbtemp;
        tbtemp.data_ = new Object[tb1_.getRowCount() + 1][tb1_.getColumnCount()];
        FuLog.debug("nb row + " + tb1_.getRowCount());
        tbtemp.initNomCol(1);
        if (!comparaison_) {
          tbtemp.setColumnName("Consommation d'eau", 0);
        } else {
          tbtemp.setColumnName("Consomation d'eau de l'�cluse " + liListeEcluses_.getSelectedValue().toString(), 0);
        }
        for (int i = 1; i < tb1_.getColumnCount(); i++) {
          tbtemp.setColumnName(" ", i);
        }
        tbtemp.setNbRow(tb1_.getRowCount() + 2);
        for (int i = 2; i <= (tb1_.getRowCount() + 1); i++) {
          for (int j = 0; j < tb1_.getColumnCount(); j++) {
            tbtemp.data_[i][j] = tb1_.data_[i - 2][j];
          }
        }

      } else if (rBassinees_.isSelected()) {
        final Sinavi2TableauBassinee tbtemp = new Sinavi2TableauBassinee();
        model = tbtemp;
        tbtemp.data_ = new Object[tb2_.getRowCount() + 1][tb2_.getColumnCount()];
        tbtemp.initNomCol(1);
        if (!comparaison_) {
          tbtemp.setColumnName("Fausses Bassin�es", 0);
        } else {
          tbtemp.setColumnName("Fausses Bassin�es de l'�cluse " + liListeEcluses_.getSelectedValue().toString(), 0);
        }
        for (int i = 1; i < tb2_.getColumnCount(); i++) {
          tbtemp.setColumnName(" ", i);
        }

        tbtemp.setNbRow(tb2_.getRowCount() + 2);
        for (int i = 2; i <= (tb2_.getRowCount() + 1); i++) {
          for (int j = 0; j < tb2_.getColumnCount(); j++) {
            tbtemp.data_[i][j] = tb2_.data_[i - 2][j];
          }

        }

      } else if (rRemplissage_.isSelected()) {
        final Sinavi2TableauRemplissage tbtemp = new Sinavi2TableauRemplissage();
        tbtemp.data_ = new Object[tb3_.getRowCount() + 1][tb3_.getColumnCount()];
        FuLog.debug("nb row + " + tb3_.getRowCount());
        tbtemp.initNomCol(1);
        if (!comparaison_) {
          tbtemp.setColumnName("Taux de remplissage", 0);
        } else {
          tbtemp.setColumnName("Taux de remplissage de l'�cluse " + liListeEcluses_.getSelectedValue().toString(), 0);
        }
        for (int i = 1; i < tb3_.getColumnCount(); i++) {
          tbtemp.setColumnName(" ", i);
        }

        tbtemp.setNbRow(tb3_.getRowCount() + 2);
        for (int i = 2; i <= (tb3_.getRowCount() + 1); i++) {
          for (int j = 0; j < tb3_.getColumnCount(); j++) {
            tbtemp.data_[i][j] = tb3_.data_[i - 2][j];
          }

        }

      }
      final CtuluTableExcelWriter test = new CtuluTableExcelWriter(model, s);
      try {
        test.write(null);

      } catch (final RowsExceededException e) {
        e.printStackTrace();
      } catch (final WriteException e) {
        e.printStackTrace();
      } catch (final IOException e) {
        e.printStackTrace();
      }

    }

    public void affMessage(final String _t) {
      new BuDialogMessage(imp_.getApp(), imp_.getInformationsSoftware(), _t).activate();
    }

    public void internalFrameActivated(final InternalFrameEvent _e) {
    // TODO Auto-generated method stub

    }

    public void internalFrameClosed(final InternalFrameEvent _e) {
      this.closable = true;

    }

    public void internalFrameDeactivated(final InternalFrameEvent _e) {
    // TODO Auto-generated method stub

    }

    public void internalFrameDeiconified(final InternalFrameEvent _e) {
    // TODO Auto-generated method stub

    }

    public void internalFrameIconified(final InternalFrameEvent _e) {
    // TODO Auto-generated method stub

    }

    public void internalFrameOpened(final InternalFrameEvent _e) {
    // TODO Auto-generated method stub

    }

    public void focusGained(final FocusEvent e) {
    // TODO Auto-generated method stub

    }

    public void focusLost(final FocusEvent e) {
    // TODO Auto-generated method stub

    }

    public void valueChanged(final ListSelectionEvent e) {
    // TODO Auto-generated method stub

    }

    public void itemStateChanged(final ItemEvent e) {
    // TODO Auto-generated method stub

    }

    public void internalFrameClosing(final InternalFrameEvent e) {
    // TODO Auto-generated method stub

    }

  } // fin de la classe permettant l'affichage des tableaux

  public void focusGained(final FocusEvent _e) {
    // tSeuil_.selectAll();
    processFocusEvent(new FocusEvent(this, FocusEvent.FOCUS_GAINED));
  }

  public void internalFrameActivated(final InternalFrameEvent e) {
  // TODO Auto-generated method stub

  }

  public void internalFrameClosed(final InternalFrameEvent e) {
  // TODO Auto-generated method stub

  }

  public void internalFrameClosing(final InternalFrameEvent e) {
  // TODO Auto-generated method stub

  }

  public void focusLost(final FocusEvent _e) {
    processFocusEvent(new FocusEvent(this, FocusEvent.FOCUS_LOST));
  }

  public void internalFrameDeactivated(final InternalFrameEvent e) {
  // TODO Auto-generated method stub

  }

  public void internalFrameDeiconified(final InternalFrameEvent e) {
  // TODO Auto-generated method stub

  }

  public void internalFrameIconified(final InternalFrameEvent e) {
  // TODO Auto-generated method stub

  }

  public void internalFrameOpened(final InternalFrameEvent e) {
  // TODO Auto-generated method stub

  }

  public void valueChanged(final ListSelectionEvent e) {
  // TODO Auto-generated method stub

  }

  public void itemStateChanged(final ItemEvent e) {
  // TODO Auto-generated method stub

  }

}
