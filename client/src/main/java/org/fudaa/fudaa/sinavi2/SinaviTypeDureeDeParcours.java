package org.fudaa.fudaa.sinavi2;

import org.fudaa.ctulu.CtuluLibString;

public class SinaviTypeDureeDeParcours {
  public SinaviTypeDureeDeParcours() {

  }

  public SinaviTypeDureeDeParcours(final int _duree, final String _typeBateau) {
    setDureeParcours(_duree);
    // dureeParcours=determineHeure(_duree);
    typeBateau = _typeBateau;
  }

  public static double determineHeure(final int _nbSecondes) {
    /*
     * String s2=""+_nbSecondes; int nbS=Integer.parseInt(s2);
     */
    final int heure = _nbSecondes / 3600;
    final int minute = _nbSecondes / 60 - heure * 60;
    String s = "";
    if (minute > 9) {
      s = heure + CtuluLibString.DOT + minute;
    } else {
      s = heure + ".0" + minute;
    }
    System.out.println("nb seconde :" + _nbSecondes + " -> " + s);
    final Double h = new Double(s);

    return h.doubleValue();
  }

  public static String determineHeureString(final int _nbSecondes) {
    /*
     * String s2=""+_nbSecondes; int nbS=Integer.parseInt(s2);
     */
    final int heure = _nbSecondes / 3600;
    final int minute = _nbSecondes / 60 - heure * 60;
    String s = "";
    if (minute > 9) {
      s = heure + CtuluLibString.DOT + minute;
    } else {
      s = heure + ".0" + minute;
    }
    System.out.println("nb seconde :" + _nbSecondes + " -> " + s);
    return s;
  }

  public double getDureeParcours() {
    return dureeParcours;
  }

  public void setDureeParcours(final double _dureeParcours) {
    dureeParcours = _dureeParcours / 3600;
    // dureeParcours = determineHeure((int) _dureeParcours);
  }

  public String getTypeBateau() {
    return typeBateau;
  }

  public void setTypeBateau(final String _typeBateau) {
    typeBateau = _typeBateau;
  }

  double dureeParcours;
  String typeBateau;
}
