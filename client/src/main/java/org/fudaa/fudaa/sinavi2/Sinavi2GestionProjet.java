/*
 * @file         SinaviGestionProjet.java
 * @creation     2001-05-17
 * @modification $Date: 2006-09-19 15:08:57 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.sinavi2;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.swing.JFileChooser;

import com.memoire.bu.BuAssistant;
import com.memoire.bu.BuCommonImplementation;
import com.memoire.bu.BuFileChooser;
import com.memoire.bu.BuTaskOperation;

/**
 * Gestion du projet.
 * 
 * @version $Revision: 1.7 $ $Date: 2006-09-19 15:08:57 $ by $Author: deniger $
 * @author Fatimatou Ka, Beno�t Maneuvrier
 */
public class Sinavi2GestionProjet {

  private static BuCommonImplementation app_;
  // private SinaviReseauFrame sinavireseauframe_;
  private File fichier_;

  public Sinavi2GestionProjet(final BuCommonImplementation _app) {
    app_ = _app;
    fichier_ = null;
  }

  /*
   * public void setSinaviReseauFrame(SinaviReseauFrame _srf){ sinavireseauframe_ = _srf; } public SinaviReseauFrame
   * getSinaviReseauFrame(){ return sinavireseauframe_; }
   */
  public String getNomFichier() {
    if (fichier_ == null) {
      return null;
    }
    return fichier_.getAbsolutePath();
  }

  public void enregistre() {
    new BuTaskOperation(app_, "Enregistre") {

      public void act() {
        enregistreSous(false);
      }
    }.start();
  }

  public void enregistreSous() {
    new BuTaskOperation(app_, "Enregistre sous ...") {

      public void act() {
        enregistreSous(true);
      }
    }.start();
  }

  public void ouvrir() {
    new BuTaskOperation(app_, "Ouverture") {

      public void act() {
        ouvrir(null);
      }
    }.start();
  }

  void ouvrir(String _nomFichier) {
    try {
      if (_nomFichier == null) {
        final File fichier = openSinavi2File();
        if (fichier == null) {
          ((Sinavi2Implementation) app_).glassStop_.setVisible(false);
          return;
        }
        _nomFichier = fichier.getAbsolutePath();
        fichier_ = fichier;
      } else {
        fichier_ = new File(_nomFichier);
      }
      final DataInputStream FluxLu = new DataInputStream(new BufferedInputStream(new FileInputStream(_nomFichier)));
      // initialisation d'ETUDE_SINAVI
      byte[] binaireEtudeSinavi = new byte[FluxLu.readInt()];
      final int nombre_octets = binaireEtudeSinavi.length;
      final int nombre_car = nombre_octets / 50;
      int index = 0;
      for (int i = 0; i < 50; i++) {
        FluxLu.read(binaireEtudeSinavi, index, nombre_car);
        index += nombre_car;
        app_.getMainPanel().setProgression(i);
      }
      FluxLu.read(binaireEtudeSinavi, index, nombre_octets - index);
      app_.getMainPanel().setProgression(50);
      // Sinavi2Implementation.ETUDE_SINAVI2 = DEtudeNavigationFluviale
      // .creeEtudeNavigationFluviale(binaireEtudeSinavi);
      app_.getMainPanel().setProgression(90);
      binaireEtudeSinavi = null;
      // initialisation de la fen�tre contenant le r�seau fluviale (DJA)
      // initDjaFrameFromString(Sinavi2Implementation.ETUDE_SINAVI2.descriptionGraphiqueDja());
      FluxLu.close();
      app_.getMainPanel().setProgression(100);
      app_.getMainPanel().setProgression(0);
    } catch (final IOException ioe) {
      System.err.println("$$$ " + ioe);
    }
    final String test = getNomFichier();
    if (test != null) {
      // if (getSinaviReseauFrame() != null)
      // SinaviImplementation.sinavireseauframe_ = getSinaviReseauFrame();
      ((Sinavi2Implementation) app_).activerCommandesSimulation(true);
      Sinavi2Implementation.assistant_.changeAttitude(BuAssistant.ATTENTE, "Etude Ouverte.\nA vous de jouer...");
      // BuDialogMessage dialog_mess = new BuDialogMessage(app_,SinaviImplementation.InfoSoftSinavi_, "Etude ouverte.");
      // dialog_mess.activate();
      app_.setTitle(Sinavi2Implementation.InfoSoftSinavi2_.name + " " + Sinavi2Implementation.InfoSoftSinavi2_.version
          + " " + getNomFichier());
    } else {
      Sinavi2Implementation.assistant_.changeAttitude(BuAssistant.PAROLE, "Ouverture annul�e");
    }
    ((Sinavi2Implementation) app_).glassStop_.setVisible(false);
  }

  void enregistreSous(final boolean _demande) {
    try {
      File fichier_intermediaire = null;
      if ((fichier_ == null) || (_demande)) {
        fichier_intermediaire = saveSinaviFile();
        if (fichier_intermediaire == null) {
          ((Sinavi2Implementation) app_).glassStop_.setVisible(false);
          return;
        }
        fichier_ = fichier_intermediaire;
      }
      final DataOutputStream FluxEcrit = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(fichier_)));
      // String stringDja = djaFrameToString();
      // Sinavi2Implementation.ETUDE_SINAVI2.descriptionGraphiqueDja(stringDja);
      /*
       * byte[] binaireEtudeSinavi = Sinavi2Implementation.ETUDE_SINAVI2.toByteArray(); int nombre_octets =
       * binaireEtudeSinavi.length; int nombre_car = nombre_octets / 100; int index = 0;
       * FluxEcrit.writeInt(binaireEtudeSinavi.length); for (int i = 0; i < 100; i++) {
       * FluxEcrit.write(binaireEtudeSinavi, index, nombre_car); index += nombre_car;
       * app_.getMainPanel().setProgression(i); } FluxEcrit.write(binaireEtudeSinavi, index, nombre_octets - index);
       * app_.getMainPanel().setProgression(100); app_.getMainPanel().setProgression(0); binaireEtudeSinavi = null;
       */
      FluxEcrit.close();
      Sinavi2Implementation.assistant_.changeAttitude(BuAssistant.ATTENTE, "Enregistrement Termin�");
      ((Sinavi2Implementation) app_).glassStop_.setVisible(false);
      app_.setTitle(Sinavi2Implementation.InfoSoftSinavi2_.name + " " + Sinavi2Implementation.InfoSoftSinavi2_.version
          + " " + getNomFichier());
    } catch (final IOException ex) {
      ex.printStackTrace();
    }
  }

  /*
   * private String djaFrameToString(){ if (sinavireseauframe_ == null) { BuDialogMessage dialog_mess = new
   * BuDialogMessage(app_, SinaviImplementation .informationsSoftware(), "Pas de description du r�seau");
   * dialog_mess.activate(); return new String(""); } ByteArrayOutputStream byte_array_output_stream = new
   * ByteArrayOutputStream(); // Object[] infosDja = new Object[3]; try {
   * DjaSaver.saveAsText(sinavireseauframe_.getGrid().getObjects(), byte_array_output_stream); } catch (Exception ioe) {
   * System.err.println("$$$ " + ioe); } return byte_array_output_stream.toString(); } private void
   * initDjaFrameFromString(String _stringDja){ if (_stringDja == null) { //System.out.println("Erreur de chaine de
   * description Dja"); return; } if (_stringDja.equals("")) { //System.out.println("Erreur de chaine de description
   * Dja"); return; } try { byte[] buffer = _stringDja.getBytes(); ByteArrayInputStream byte_array_input_stream = new
   * ByteArrayInputStream(buffer); DjaGridInteractive grille = new DjaGridInteractive(true, DjaLoader
   * .loadAsText(byte_array_input_stream)); sinavireseauframe_ = new SinaviReseauFrame(app_, "CONNEXION DU RESEAU",
   * grille); grille.addMouseListener(new SinaviReseauMouseAdapter(sinavireseauframe_, app_));
   * grille.addGridListener(new SinaviReseauGridAdapter()); } catch (Exception ex) { System.err.println("$$$ " + ex);
   * ex.printStackTrace(); } }
   */
  private static File openSinavi2File() {
    final String[] ext = { "nom" };
    File res = null;
    final BuFileChooser chooser = new BuFileChooser();
    chooser.setFileFilter(new javax.swing.filechooser.FileFilter() {

      public boolean accept(final java.io.File f) {
        boolean retour = f.isDirectory();
        final String nom = f.getName().toLowerCase();
        for (int i = 0; i < ext.length; i++) {
          if (nom.endsWith(ext[i])) {
            retour = true;
          }
        }
        return retour;
      }

      public String getDescription() {
        String texte = "";
        for (int i = 0; i < ext.length; i++) {
          texte += " *." + ext[i];
        }
        return texte;
      }
    });
    final int returnVal = chooser.showOpenDialog(app_.getFrame());
    String filename = null;
    if (returnVal == JFileChooser.APPROVE_OPTION) {
      filename = chooser.getSelectedFile().getPath();
    }
    if (filename == null) {
      return null;
    }
    res = new File(filename);
    return res;
  }

  private static File saveSinaviFile() {
    final String[] ext = { "nom" };
    File res = null;
    final BuFileChooser chooser = new BuFileChooser();
    chooser.setFileFilter(new javax.swing.filechooser.FileFilter() {

      public boolean accept(final java.io.File f) {
        boolean retour = f.isDirectory();
        final String nom = f.getName().toLowerCase();
        for (int i = 0; i < ext.length; i++) {
          if (nom.endsWith(ext[i])) {
            retour = true;
          }
        }
        return retour;
      }

      public String getDescription() {
        String texte = "";
        for (int i = 0; i < ext.length; i++) {
          texte += " *." + ext[i];
        }
        return texte;
      }
    });
    final int returnVal = chooser.showSaveDialog(app_.getFrame());
    String filename = null;
    if (returnVal == JFileChooser.APPROVE_OPTION) {
      filename = chooser.getSelectedFile().getPath();
    }
    if (filename == null) {
      return null;
    }
    boolean test = false;
    for (int i = 0; i < ext.length; i++) {
      if (filename.endsWith(ext[i])) {
        test = true;
      }
    }
    if (!test) {
      filename = filename + ".nom";
    }
    res = new File(filename);
    return res;
  }

  public static File saveHtmlFile() {
    final String[] ext = { "html", "HTML", "htm", "HTM" };
    File res = null;
    final BuFileChooser chooser = new BuFileChooser();
    chooser.setFileFilter(new javax.swing.filechooser.FileFilter() {

      public boolean accept(final java.io.File f) {
        boolean retour = f.isDirectory();
        final String nom = f.getName().toLowerCase();
        for (int i = 0; i < ext.length; i++) {
          if (nom.endsWith(ext[i])) {
            retour = true;
          }
        }
        return retour;
      }

      public String getDescription() {
        String texte = "";
        for (int i = 0; i < ext.length; i++) {
          texte += " *." + ext[i];
        }
        return texte;
      }
    });
    final int returnVal = chooser.showSaveDialog(app_.getFrame());
    String filename = null;
    if (returnVal == JFileChooser.APPROVE_OPTION) {
      filename = chooser.getSelectedFile().getPath();
    }
    if (filename == null) {
      return null;
    }
    boolean test = false;
    for (int i = 0; i < ext.length; i++) {
      if (filename.endsWith(ext[i])) {
        test = true;
      }
    }
    if (!test) {
      filename = filename + ".html";
    }
    res = new File(filename);
    return res;
  }

  public static File saveCsvFile() {
    final String[] ext = { "csv", "CSV" };
    File res = null;
    final BuFileChooser chooser = new BuFileChooser();
    chooser.setFileFilter(new javax.swing.filechooser.FileFilter() {

      public boolean accept(final java.io.File f) {
        boolean retour = f.isDirectory();
        final String nom = f.getName().toLowerCase();
        for (int i = 0; i < ext.length; i++) {
          if (nom.endsWith(ext[i])) {
            retour = true;
          }
        }
        return retour;
      }

      public String getDescription() {
        String texte = "";
        for (int i = 0; i < ext.length; i++) {
          texte += " *." + ext[i];
        }
        return texte;
      }
    });
    final int returnVal = chooser.showSaveDialog(app_.getFrame());
    String filename = null;
    if (returnVal == JFileChooser.APPROVE_OPTION) {
      filename = chooser.getSelectedFile().getPath();
    }
    if (filename == null) {
      return null;
    }
    boolean test = false;
    for (int i = 0; i < ext.length; i++) {
      if (filename.endsWith(ext[i])) {
        test = true;
      }
    }
    if (!test) {
      filename = filename + ".csv";
    }
    res = new File(filename);
    return res;
  }

  public static File saveGifFile() {
    final String[] ext = { "gif", "GIF" };
    File res = null;
    final BuFileChooser chooser = new BuFileChooser();
    chooser.setFileFilter(new javax.swing.filechooser.FileFilter() {

      public boolean accept(final java.io.File f) {
        boolean retour = f.isDirectory();
        final String nom = f.getName().toLowerCase();
        for (int i = 0; i < ext.length; i++) {
          if (nom.endsWith(ext[i])) {
            retour = true;
          }
        }
        return retour;
      }

      public String getDescription() {
        String texte = "";
        for (int i = 0; i < ext.length; i++) {
          texte += " *." + ext[i];
        }
        return texte;
      }
    });
    final int returnVal = chooser.showSaveDialog(app_.getFrame());
    String filename = null;
    if (returnVal == JFileChooser.APPROVE_OPTION) {
      filename = chooser.getSelectedFile().getPath();
    }
    if (filename == null) {
      return null;
    }
    boolean test = false;
    for (int i = 0; i < ext.length; i++) {
      if (filename.endsWith(ext[i])) {
        test = true;
      }
    }
    if (!test) {
      filename = filename + ".gif";
    }
    res = new File(filename);
    return res;
  }
}
