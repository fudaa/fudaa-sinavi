package org.fudaa.fudaa.sinavi2;

public class SinaviTypeConsommation {

  /** * constructeur sans param�tre* */
  public SinaviTypeConsommation() {

  }

  /** * constructeur avec param�tres* */
  public SinaviTypeConsommation(final SinaviTypeConsommation _cons) {
    numecluse_ = _cons.numecluse_;
    ecluse_ = _cons.ecluse_;
    nbBassineesMontantes_ = _cons.nbBassineesMontantes_;
    nbBassineesAvalantes_ = _cons.nbBassineesAvalantes_;
    nbTotalBassinees_ = _cons.nbTotalBassinees_;
    nbFaussesBassineesMontantes_ = _cons.nbFaussesBassineesMontantes_;
    nbFaussesBassineesAvalantes_ = _cons.nbFaussesBassineesAvalantes_;
    nbTotalFaussesBassinees_ = _cons.nbTotalFaussesBassinees_;
    consommationDEauAvalant_ = _cons.consommationDEauAvalant_;
    consommationDEauMontant_ = _cons.consommationDEauMontant_;
    consommationDEau_ = _cons.consommationDEau_;
    listeTypesBateaux_ = _cons.listeTypesBateaux_;
    listeNombreBateauxMontant_ = _cons.listeNombreBateauxMontant_;
    listeNombreBateauxAvalant_ = _cons.listeNombreBateauxAvalant_;
    tauxRemplissageAvalant_ = _cons.tauxRemplissageAvalant_;
    tauxRemplissageMontant_ = _cons.tauxRemplissageMontant_;
    tauxRemplissageTotal_ = _cons.tauxRemplissageTotal_;

  }

  public int getNumeroEcluse() {
    return numecluse_;
  }

  public void setNumeroEcluse() {

  }

  public String getEcluse() {
    return ecluse_;
  }

  public void setEcluse(final String _ecluse) {
    ecluse_ = _ecluse;
  }

  public int getNbBassineesMontantes() {
    return nbBassineesMontantes_;
  }

  public void setNbBassineesMontantes(final int _nbBassineesMontantes) {
    nbBassineesMontantes_ = _nbBassineesMontantes;
  }

  public int getNbBassineesAvalantes() {
    return nbBassineesAvalantes_;
  }

  public void setNbBassineesAvalantes(final int _nbBassineesAvalantes) {
    nbBassineesAvalantes_ = _nbBassineesAvalantes;
  }

  public int getNbTotalBassinees() {
    return nbTotalBassinees_;
  }

  public void setNbTotalBassinees(final int _nbTotalBassinees) {
    nbTotalBassinees_ = _nbTotalBassinees;
  }

  public int getNbFaussesBassineesMontantes() {
    return nbFaussesBassineesMontantes_;
  }

  public void setNbFaussesBassineesMontantes(final int _nbFaussesBassineesMontantes) {
    nbFaussesBassineesMontantes_ = _nbFaussesBassineesMontantes;
  }

  public int getNbFaussesBassineesAvalantes() {
    return nbFaussesBassineesAvalantes_;
  }

  public void setNbFaussesBassineesAvalantes(final int _nbFaussesBassineesAvalantes) {
    nbFaussesBassineesAvalantes_ = _nbFaussesBassineesAvalantes;
  }

  public int getNbTotalFaussesBassinees() {
    return nbTotalFaussesBassinees_;
  }

  public void setNbTotalFaussesBassinees(final int _nbTotalFaussesBassinees) {
    nbTotalFaussesBassinees_ = _nbTotalFaussesBassinees;
  }

  public double getConsommationDEauAvalant() {
    return consommationDEauAvalant_;
  }

  public void setConsommationDEauAvalant(final double _consommationDEauAvalant) {
    consommationDEauAvalant_ = _consommationDEauAvalant;
  }

  public double getConsommationDEauMontant() {
    return consommationDEauMontant_;
  }

  public void setConsommationDEauMontant(final double _consommationDEauMontant) {
    consommationDEauMontant_ = _consommationDEauMontant;
  }

  public double getConsommationDEau() {
    return consommationDEau_;
  }

  public void setConsommationDEau(final double _consommationDEau) {
    consommationDEau_ = _consommationDEau;
  }

  public double getTauxRemplissageAvalant() {
    return tauxRemplissageAvalant_;
  }

  public void setTauxRemplissageAvalant(final double _tauxRemplissageAvalant) {
    tauxRemplissageAvalant_ = _tauxRemplissageAvalant;
  }

  public double getTauxRemplissageMontant() {
    return tauxRemplissageAvalant_;
  }

  public void setTauxRemplissageMontant(final double _tauxRemplissageMontant) {
    tauxRemplissageMontant_ = _tauxRemplissageMontant;
  }

  public double getTauxRemplissageTotal() {
    return tauxRemplissageTotal_;
  }

  public void setTauxRemplissageTotal(final double _tauxRemplissageTotal) {
    tauxRemplissageTotal_ = _tauxRemplissageTotal;
  }

  public void calculeTotauxBassinee() {
    nbTotalBassinees_ = nbBassineesMontantes_ + nbBassineesAvalantes_;
  }

  public void calculeTotauxFaussesBassinees() {
    nbTotalFaussesBassinees_ = nbFaussesBassineesMontantes_ + nbFaussesBassineesAvalantes_;
  }

  int numecluse_;
  String ecluse_;
  int nbBassineesMontantes_;
  int nbBassineesAvalantes_;
  int nbTotalBassinees_;
  int nbFaussesBassineesMontantes_;
  int nbFaussesBassineesAvalantes_;
  int nbTotalFaussesBassinees_;
  double consommationDEau_;
  double consommationDEauAvalant_;
  double consommationDEauMontant_;
  int[] listeTypesBateaux_; // tableau des types de bateau
  int[] listeNombreBateauxAvalant_;
  int[] listeNombreBateauxMontant_;// tableau du nombre de bateaux par type en avalant int []
                                    // listeNombreBateauxMontant_; // tableau du nombre de bateaux par type en montant
  double tauxRemplissageAvalant_;
  double tauxRemplissageMontant_;
  double tauxRemplissageTotal_;

}
