package org.fudaa.fudaa.sinavi2;

import org.fudaa.dodico.corba.sinavi2.SDateHeure;
import org.fudaa.dodico.corba.sinavi2.SHeure;
import org.fudaa.dodico.corba.sinavi2.SParametresLoiD;
import org.fudaa.dodico.corba.sinavi2.SParametresLoiE;
import org.fudaa.dodico.corba.sinavi2.SParametresLoiJ;
import org.fudaa.dodico.corba.sinavi2.SParametresTrajets;

public class SinaviTypeTrajet {

  SParametresTrajets trajet_;
  SParametresLoiE erlang_;
  SParametresLoiD deter_;
  SParametresLoiJ jour_;

  // constructeur sans param�tres
  public SinaviTypeTrajet() {}

  /**
   * @param nom du bateau
   * @param numeroGareDepart
   * @param numeroGareArrivee
   * @param typeLoi
   */
  public SinaviTypeTrajet(final String _bat, final boolean _sens, final int _numeroGareDepart,
      final int _numeroGareArrivee, final int _typeLoi) {

    trajet_ = new SParametresTrajets();
    trajet_.bateau = _bat;
    trajet_.avalantMontant = _sens;
    trajet_.gareDepart = _numeroGareDepart;
    trajet_.gareArrivee = _numeroGareArrivee;
    trajet_.typeDeLoi = _typeLoi;
  }

  /** Constructeur pour la loi Erlang */
  public SinaviTypeTrajet(final String _bat, final boolean _sens, final int _numeroGareDepart,
      final int _numeroGareArrivee, final int _typeLoi, final int _heuredeb, final int _mindeb, final int _heurefin,
      final int _minfin, final int _ordreloi, final int _nbrebateaux) {
    trajet_ = new SParametresTrajets();
    trajet_.bateau = _bat;
    trajet_.avalantMontant = _sens;
    trajet_.gareDepart = _numeroGareDepart;
    trajet_.gareArrivee = _numeroGareArrivee;
    trajet_.typeDeLoi = _typeLoi;
    erlang_ = new SParametresLoiE();
    final SHeure hdeb_ = new SHeure();
    final SHeure hfin_ = new SHeure();
    hdeb_.heure = determineHeureSeule(_heuredeb);
    hdeb_.minutes = determineMinuteSeule(_mindeb);
    hfin_.heure = determineHeureSeule(_heurefin);
    hfin_.minutes = determineMinuteSeule(_minfin);
    erlang_.heureDebutGeneration = hdeb_;
    erlang_.heureFinGeneration = hfin_;
    erlang_.ordreDeLaLoi = _ordreloi;
    erlang_.nbreBateauxAttendus = _nbrebateaux;
    trajet_.loiE = erlang_;
  }

  /** Constructeur pour la loi deterministe */
  public SinaviTypeTrajet(final String _bat, final boolean _sens, final int _numeroGareDepart,
      final int _numeroGareArrivee, final int _typeLoi, final int _heure, final int _minute, final int _date) {
    trajet_ = new SParametresTrajets();
    trajet_.bateau = _bat;
    trajet_.avalantMontant = _sens;
    trajet_.gareDepart = _numeroGareDepart;
    trajet_.gareArrivee = _numeroGareArrivee;
    trajet_.typeDeLoi = _typeLoi;
    deter_ = new SParametresLoiD();
    final SDateHeure[] dateHeures = new SDateHeure[1];
    final SDateHeure dateHeure = new SDateHeure();
    final SHeure h_ = new SHeure();
    h_.heure = determineHeureSeule(_heure);
    h_.minutes = determineMinuteSeule(_minute);
    // dateHeures[0].date=date;
    // dateHeures[0].heure=h_;
    dateHeure.date = _date;
    dateHeure.heure = h_;
    dateHeures[0] = dateHeure;
    deter_.dateHeure = dateHeures;
    trajet_.loiD = deter_;
  }

  /** Constructeur pour la loi journali�re */
  public SinaviTypeTrajet(final String _bat, final boolean _sens, final int _numeroGareDepart,
      final int _numeroGareArrivee, final int _typeLoi, final int _heure, final int _minute) {
    trajet_ = new SParametresTrajets();
    trajet_.bateau = _bat;
    trajet_.avalantMontant = _sens;
    trajet_.gareDepart = _numeroGareDepart;
    trajet_.gareArrivee = _numeroGareArrivee;
    trajet_.typeDeLoi = _typeLoi;
    jour_ = new SParametresLoiJ();
    final SHeure h_ = new SHeure();
    final SHeure[] heures = new SHeure[1];
    h_.heure = determineHeureSeule(_heure);
    h_.minutes = determineMinuteSeule(_minute);
    heures[0] = h_;
    jour_.heureJ = heures;
    trajet_.loiJ = jour_;
  }

  public static int determineHeureSeule(final int _nbSecondes) {
    return _nbSecondes / 3600;
  }

  public static int determineMinuteSeule(final int _nbSecondes) {
    return _nbSecondes / 60;
  }

  // constructeur de recopie m�thode

  // accesseurs
  /** ************accesseuuuuuuuuuurrrrrrrrrrrrrrrrrrsssssssssssssss */
  public String getbateau() {
    return trajet_.bateau;
  }

  public boolean getSens() {
    return trajet_.avalantMontant;
  }

  public int getNumeroGareDepart() {
    return trajet_.gareDepart;
  }

  public int getNumeroGareArrivee() {
    return trajet_.gareArrivee;
  }

  public int getTypeLoi() {
    return trajet_.typeDeLoi;
  }

  // modifieurs
  public void setbateau(final String _bat) {
    trajet_.bateau = _bat;
  }

  public void setSens(final boolean _sens) {
    trajet_.avalantMontant = _sens;
  }

  public void setNumeroGareDepart(final int _gareDepart) {
    trajet_.gareDepart = _gareDepart;
  }

  public void setNumeroGareArrivee(final int _gareArrivee) {
    trajet_.gareArrivee = _gareArrivee;
  }

  public void setTypeLoi(final int _loi) {
    trajet_.typeDeLoi = _loi;
  }

  /*
   * double creeComponent(boolean _useK,boolean _useM,boolean _useL){ LongueurField field=new
   * LongueurField(_useK,_useM,_useL); field.setValueValidator(new BuValueValidator() { public boolean
   * isValueValid(Object _value) { boolean r=(_value instanceof LongueurField); if(r){ double
   * l=((LongueurField)_value).getLongueurField(); return l>0 && l<1E6; } // TODO Auto-generated method stub return
   * false; } }); return field; }
   */

  public void arrondir(double _a, final int _nbChiffres) {
    for (int i = 0; i < _nbChiffres; i++) {
      _a *= 10;
    }
    _a = (int) (_a + .5);
    for (int i = 0; i < _nbChiffres; i++) {
      _a /= 10;
    }
  }

  /** *********m�thodes utiles********************** */

  public void dessineMoi() {

  }

  public boolean typeTrajetEgale(final SinaviTypeTrajet _traj) {
    return (this.getbateau() == _traj.getbateau() && this.getSens() == _traj.getSens()
        && this.getNumeroGareDepart() == _traj.getNumeroGareDepart()
        && this.getNumeroGareArrivee() == _traj.getNumeroGareArrivee() && this.getTypeLoi() == _traj.getTypeLoi()
        );
  }

}
