package org.fudaa.fudaa.sinavi2;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.util.ArrayList;

import javax.swing.JComponent;
import javax.swing.border.EmptyBorder;
import javax.swing.event.InternalFrameEvent;
import javax.swing.event.InternalFrameListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import com.memoire.bu.*;
import com.memoire.fu.FuLog;

import org.fudaa.dodico.corba.sinavi2.SSimulationSinavi2;

public class Sinavi2ResGestionSimulations extends BuInternalFrame implements ActionListener, InternalFrameListener,
    ListSelectionListener, FocusListener {
  private int lastIndexSel_;
  private String lastCommentaire_;
  private final BuLabel lTitre_ = new BuLabel("Gestion des Simulations");
  private final BuLabel lCommentaire_ = new BuLabel("Commentaires");
  private final BuTextField tCommentaire_ = new BuTextField();
  private final BuLabel _ = new BuLabel("Gestion des Simulations");
  private final BuButton bAnnuler_ = new BuButton(Sinavi2Resource.SINAVI2.getIcon("QUITTER"), "Quitter");
  private final BuButton bRenommer_ = new BuButton(Sinavi2Resource.SINAVI2.getIcon("RENOMMER"), "Renommer");
  private final BuButton bSupprimer_ = new BuButton(Sinavi2Resource.SINAVI2.getIcon("SUPPRIMER"), "Supprimer");
  private final BuButton bMiseAJour_ = new BuButton(Sinavi2Resource.SINAVI2.getIcon("SELECTIONNER"), "Mise � Jour");
  public ArrayList listeSimulations_;
  private final BuPanel pBoutons_ = new BuPanel();
  private final BuPanel pTitre_ = new BuPanel();
  private final BuPanel pDonnees2_ = new BuPanel();
  private final BuLabel lListeSimulations_ = new BuLabel("Liste de Simulations");
  private BuList liListeSimulations_;// =new BuList();
  private BuScrollPane sliListeSimulations_;
  private Sinavi2Implementation imp_;

  public Sinavi2ResGestionSimulations(final BuCommonImplementation _appli, final ArrayList _listeSimulations) {
    super("Gestion des Simulations", true, true, true, false);
    listeSimulations_ = _listeSimulations;
    imp_ = (Sinavi2Implementation) _appli.getImplementation();
    bAnnuler_.addActionListener(this);
    bRenommer_.addActionListener(this);
    bSupprimer_.addActionListener(this);
    bMiseAJour_.addActionListener(this);
    tCommentaire_.addActionListener(this);
    tCommentaire_.addFocusListener(this);
    pTitre_.add(lTitre_);
    pDonnees2_.add(lListeSimulations_);

    sliListeSimulations_ = new BuScrollPane(liListeSimulations_);

    mAJSimulations();
    liListeSimulations_.addListSelectionListener(this);
    sliListeSimulations_ = new BuScrollPane(liListeSimulations_);
    sliListeSimulations_.setPreferredSize(new Dimension(200, 136));
    pDonnees2_.add(sliListeSimulations_);
    pDonnees2_.add(lCommentaire_);
    // tCommentaire_.setPreferredSize(new Dimension(100,85));
    tCommentaire_.setText("commentaires");
    final BuScrollPane scCom_ = new BuScrollPane(tCommentaire_);
    scCom_.setPreferredSize(new Dimension(200, 50));
    pDonnees2_.add(scCom_);

    ((JComponent) getContentPane()).setBorder(new EmptyBorder(25, 25, 25, 25));
    pBoutons_.setLayout(new FlowLayout(FlowLayout.CENTER));
    pBoutons_.add(bAnnuler_);
    pBoutons_.add(bRenommer_);
    pBoutons_.add(bSupprimer_);
    pBoutons_.add(bMiseAJour_);
    getContentPane().add(pTitre_, BorderLayout.NORTH);
    getContentPane().add(pDonnees2_, BorderLayout.CENTER);
    getContentPane().add(pBoutons_, BorderLayout.SOUTH);
    pack();
    setLastIndexSel(listeSimulations_.size() - 1);
  }

  private void mAJSimulations() {
    final String[] simulation = new String[listeSimulations_.size()];
    FuLog.debug("taille liste simulation : " + listeSimulations_.size());

    for (int i = 0; i < listeSimulations_.size(); i++) {

      final SSimulationSinavi2 temp = (SSimulationSinavi2) listeSimulations_.get(i);
      if (!temp.nomSim.equalsIgnoreCase("")) {
        simulation[i] = temp.nomSim;
      } else {
        simulation[i] = "Future Simulation";
        temp.nomSim = "Future Simulation";
      }

    }

    /*
     * for(int i=0;i<listeSimulations_.size();i++){ ArrayList temp=(ArrayList)listeSimulations_.get(i);
     * simulation[i]=temp.get(0).toString(); }
     */
    liListeSimulations_ = new BuList(simulation);
  }

  public void annuler() {

    imp_.removeInternalFrame(this);

    // imp_.ResetFille2AffBateaux(); //a faire a ajouter la fenetre dans sinavi2implementatino
  }

  public void actionPerformed(final ActionEvent _e) {
    if (_e.getSource() == bAnnuler_) {
      annuler();

    }
    if (_e.getSource() == tCommentaire_) {
      /*
       * if(liListeSimulations_.getSelectedIndex()!=-1){ modifCom=liListeSimulations_.getSelectedIndex(); }
       */

    }

    else if (_e.getSource() == bRenommer_) {
      if (liListeSimulations_.getSelectedIndex() != listeSimulations_.size() - 1) {
        final int x = liListeSimulations_.getSelectedIndex();
        final BuDialogInput nomSim = new BuDialogInput(imp_, Sinavi2Implementation.informationsSoftware(),
            "Nom de la simulation", "Entrez un nom de simulation", "" + liListeSimulations_.getSelectedValue());
        /***************************************************************************************************************
         * nomSim.activate(); while(nomSim.getValue().equalsIgnoreCase("")){ nomSim.activate(); }
         **************************************************************************************************************/
        if (nomSim.activate() == 0 && !nomSim.getValue().equalsIgnoreCase("")) {
          // liListeSimulations_.setSelectedValue(nomSim.getValue(),true);
          // ((ArrayList)listeSimulations_.get(x)).set(0,nomSim.getValue());
          ((SSimulationSinavi2) listeSimulations_.get(x)).nomSim = nomSim.getValue();
        }
        mAJSimulations();
        annuler();
        imp_.resGestionSimulation();
      } else {
        imp_.affMessage("Vous ne pouvez renommer une simulation non lanc�e.");
      }
    } else if (_e.getSource() == bSupprimer_) {
      if (liListeSimulations_.getSelectedValue().toString().equalsIgnoreCase("Future Simulation")) {
        imp_.affMessage("La future simulation ne peut �tre supprim�e.");
      } else if (liListeSimulations_.getSelectedIndex() < imp_.listeSimulationsSinavi2_.size() - 1) {
        final int index = liListeSimulations_.getSelectedIndex();

        if (index < imp_.getNumeroSimulationCourante()) {
          imp_.setSimulationCourante(imp_.getNumeroSimulationCourante() - 1);
        }
        listeSimulations_.remove(index);
        imp_.affMessage("Simulation supprim�e");
        annuler();
        imp_.resGestionSimulation();
      }

    }

    else if (_e.getSource() == bMiseAJour_) {
      // mAJSimulations();
      if (liListeSimulations_.getSelectedIndex() != -1) {
        /*
         * System.out.println("testbat "+((SSimulationSinavi2)listeSimulations_.get(0)).parametresBateau[0].largeur);
         * System.out.println("testbat "+((SSimulationSinavi2)listeSimulations_.get(1)).parametresBateau[0].largeur);
         * System.out.println("testbat "+((SSimulationSinavi2)listeSimulations_.get(2)).parametresBateau[0].largeur);
         */
        // imp_.setCommentaire((String) tCommentaire_.getValue());
        // imp_.nomSimCourant_=(String) liListeSimulations_.getSelectedValue();
        ((SSimulationSinavi2) listeSimulations_.get(liListeSimulations_.getSelectedIndex())).nomSim = liListeSimulations_
            .getSelectedValue().toString();

        // ((SSimulationSinavi2)listeSimulations_.get(liListeSimulations_.getSelectedIndex())).commentaire=tCommentaire_.getValue().toString();
        FuLog.debug("" + liListeSimulations_.getSelectedIndex());
        imp_.setSimulationCourante(liListeSimulations_.getSelectedIndex());
        if (liListeSimulations_.getSelectedIndex() < listeSimulations_.size() - 1) {
          /*System.out.println(" on ne peut pas modifier getselindex " + liListeSimulations_.getSelectedIndex() + " simcou"
              + imp_.getNumeroSimulationCourante() + " taille " + listeSimulations_.size());
          imp_.affMessage("On ne peut pas modifier une simulation d�j� lanc�e.\n"+"Pour effectuer une nouvelle simulation,\n"+"s�lectionnez \"Future simulation\".");*/
        	imp_.simulationCourante_=liListeSimulations_.getSelectedIndex();
        	imp_.setPermettreModif(false);
        	System.out.println("nom de la simulation : " +" "+imp_.nomSimCourant_+"\n" );
        	System.out.print("nb de jour : " + imp_.gen_.dureeSimulation +"\n");
        	System.out.println("graine = " + imp_.gen_.graine);
          
        } else {
          System.out.println(" on peut  modifier getselindex " + liListeSimulations_.getSelectedIndex() + " simcou"
              + imp_.getNumeroSimulationCourante() + " taille " + listeSimulations_.size());
          imp_.setPermettreModif(true);
        }
        // imp_.simulationCourante_=liListeSimulations_.getSelectedIndex();
        annuler();
      } /*else {		// Il n'y a pas de "else" possible !!
        imp_.affMessage("selectionnez une simulation afin d'en observer son contenu");
      }*/

    }
  }

  public void internalFrameActivated(final InternalFrameEvent _e) {
  // TODO Auto-generated method stub

  }

  public void internalFrameClosed(final InternalFrameEvent _e) {
    this.closable = true;
    annuler();

  }

  public void internalFrameClosing(final InternalFrameEvent _e) {
  // TODO Auto-generated method stub

  }

  public void internalFrameDeactivated(final InternalFrameEvent _e) {
  // TODO Auto-generated method stub

  }

  public void internalFrameDeiconified(final InternalFrameEvent _e) {
  // TODO Auto-generated method stub

  }

  public void internalFrameIconified(final InternalFrameEvent _e) {
  // TODO Auto-generated method stub

  }

  public void internalFrameOpened(final InternalFrameEvent _e) {
  // TODO Auto-generated method stub

  }

  public void focusGained(final FocusEvent _e) {
    // TODO Auto-generated method stub
    if (_e.getSource() == tCommentaire_) {
      Sinavi2Implementation.assistant_
          .changeAttitude(
              BuAssistant.PAROLE,
              "Pour sauvegarder un \n commentaire, selectionnez \n la simulation, remplissez \n le commentaire puis cliquez \n une fois sur la simulation \n dans la liste.");
    }

  }

  public void focusLost(final FocusEvent _e) {
    if (_e.getSource() == tCommentaire_) {

      setLastIndexSel(liListeSimulations_.getSelectedIndex());
      FuLog.debug("commentaire : " + tCommentaire_.getText() + " " + tCommentaire_.getValue());
      ((SSimulationSinavi2) listeSimulations_.get(getLastIndexSel())).commentaire = tCommentaire_.getText();

      FuLog.debug("perd focus");
    }

  }

  public void valueChanged(final ListSelectionEvent _e) {
    FuLog.debug("change value");
    if (_e.getSource() == liListeSimulations_) {
      FuLog.debug("change value modif");
      // ((SSimulationSinavi2)listeSimulations_.get(getLastIndexSel())).commentaire=tCommentaire_.getText();
      tCommentaire_
          .setValue(((SSimulationSinavi2) listeSimulations_.get(liListeSimulations_.getSelectedIndex())).commentaire);
      // setLastIndexSel( liListeSimulations_.getSelectedIndex());
    }

  }

  public int getLastIndexSel() {
    return lastIndexSel_;
  }

  public void setLastIndexSel(final int _lastIndexSel) {
    lastIndexSel_ = _lastIndexSel;
  }

  public String getLastCommentaire() {
    return lastCommentaire_;
  }

  public void setLastCommentaire(final String _lastCommentaire) {
    lastCommentaire_ = _lastCommentaire;
  }

}
