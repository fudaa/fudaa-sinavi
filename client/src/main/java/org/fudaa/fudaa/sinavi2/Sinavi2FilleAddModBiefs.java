/*
 * @file         SinaviFilleChoixCalcul.java
 * @creation     2001-05-17
 * @modification $Date: 2006-12-20 16:13:19 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.sinavi2;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

import javax.swing.JComponent;
import javax.swing.border.EmptyBorder;
import javax.swing.event.InternalFrameEvent;
import javax.swing.event.InternalFrameListener;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuCommonImplementation;
import com.memoire.bu.BuInternalFrame;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuTextField;

import org.fudaa.ctulu.gui.CtuluLibSwing;

import org.fudaa.dodico.corba.sinavi2.SParametresBief;

/**
 * impl�mentation d'une fen�tre interne permettant de saisir ou modifier les param�tres des biefs cette classe
 * fonctionne comme Sinavi2AddModBateaux
 * 
 * @version $Revision: 1.14 $ $Date: 2006-12-20 16:13:19 $ by $Author: deniger $
 * @author Fatimatou Ka, Beno�t Maneuvrier
 */
public class Sinavi2FilleAddModBiefs extends BuInternalFrame implements ActionListener, InternalFrameListener,
    FocusListener {

  private final BuLabel lTitre_ = new BuLabel("Bief");
  /** bouton pour annuler */
  private final BuButton bAnnuler_ = new BuButton(Sinavi2Resource.SINAVI2.getIcon("QUITTER"), "Quitter");
  /** ajouter ou modifier le type de bief */
  private final BuButton bSauver_ = new BuButton(Sinavi2Resource.SINAVI2.getIcon("VALIDER"), "Sauver");
  /** afficher les biefs */
  private final BuButton bAfficher_ = new BuButton(Sinavi2Resource.SINAVI2.getIcon("EDITER"), "Afficher");

  /** validator des champs LongueurField */
  LongueurField[] longueursAValider_;

  /** caract�ristiques du biefs en haut */
  private final BuLabel lId_ = new BuLabel("Identification");
  private final BuTextField tId_ = new BuTextField("", 15);

  private final BuLabel lLong_ = new BuLabel("Longueur");
  private final LongueurField tLong_ = new LongueurField(false, true, false);

  private final BuLabel lLarg_ = new BuLabel("Largeur");
  private final LongueurField tLarg_ = new LongueurField(false, true, false);

  private final BuLabel lHaut_ = new BuLabel("Hauteur d'eau");
  private final LongueurField tHaut_ = new LongueurField(false, true, true);

  private final BuLabel lVit_ = new BuLabel("Vitesse");
  private final LongueurField tVit_ = new LongueurField(true, true, false);

  /** **gerer la liste des biefs**** */

  public ArrayList listeBiefs_;
  /** **num�ro de bief pour initialiser les champs en cas de modifications*** */

  private int nBief_ = -1;// -1 a lorigine
  /** * panel contenant les boutons, il est plac� en bas */
  private final BuPanel pBoutons_ = new BuPanel();
  /**
   * panel contenant les diff�rents champs de la fen�tre
   */
  private final BuPanel pTitre_ = new BuPanel();

  private final BuPanel pDonnees2_ = new BuPanel();
  /**
   * Constructeur
   * 
   * @param _appli une instance de SinaviImplementation
   * @param _liste liste des biefs
   * @param _modif true on est en modification
   */
  protected boolean modif_;

  /**
   * une instance de SinaviImplementation
   */
  public Sinavi2Implementation imp_ = null;

  public Sinavi2FilleAddModBiefs(final BuCommonImplementation _appli, final ArrayList _liste, final boolean _modif) {
    super("Ajouter/Modifier un bief", true, true, true, false);
    modif_ = _modif;
    listeBiefs_ = _liste;
    imp_ = (Sinavi2Implementation) _appli.getImplementation();

    bSauver_.addActionListener(this);
    bAnnuler_.addActionListener(this);
    bAfficher_.addActionListener(this);
    tId_.setToolTipText("Entrez l'identifiant du bief");

    ((JComponent) getContentPane()).setBorder(new EmptyBorder(25, 25, 25, 25));

    /** creation de la fenetre */

    pTitre_.add(lTitre_, "center");
    final GridLayout g1 = new GridLayout(5, 2);
    pDonnees2_.setLayout(g1);

    pDonnees2_.add(lId_);
    pDonnees2_.add(tId_);
    tId_.addFocusListener(this);
    pDonnees2_.add(lLong_);
    pDonnees2_.add(tLong_);
    tLong_.addFocusListener(this);
    tLong_.setToolTipText("Entrez la longueur du bief");
    pDonnees2_.add(lLarg_);
    pDonnees2_.add(tLarg_);
    tLarg_.setToolTipText("Entrez la largeur du bief");
    tLarg_.addFocusListener(this);
    pDonnees2_.add(lHaut_);
    pDonnees2_.add(tHaut_);
    tHaut_.setToolTipText("Entrez la hauteur du bief");
    tHaut_.addFocusListener(this);
    pDonnees2_.add(lVit_);
    pDonnees2_.add(tVit_);
    tVit_.addFocusListener(this);
    tVit_.setToolTipText("Entrez la vitesse maximale autoris�e dans le bief");

    pBoutons_.setLayout(new FlowLayout(FlowLayout.CENTER));
    pBoutons_.add(bAnnuler_);
    pBoutons_.add(bSauver_);

    pBoutons_.add(bAfficher_);
    // getContentPane().add(pDonnees_, BorderLayout.NORTH);
    getContentPane().add(pTitre_, BorderLayout.NORTH);
    getContentPane().add(pDonnees2_, BorderLayout.CENTER);
    getContentPane().add(pBoutons_, BorderLayout.SOUTH);

    initialiseChamps(nBief_);

    pack();

    final List longueurList = new ArrayList();
    /** *reinitialisation des valeurs* */
    tLong_.setLongueurValidator(LongueurFieldValidator.creeMin(1));
    longueurList.add(tLong_);
    tLarg_.setLongueurValidator(LongueurFieldValidator.creeMin(1));
    longueurList.add(tLarg_);
    tHaut_.setLongueurValidator(LongueurFieldValidator.creeMin(1));
    longueurList.add(tHaut_);
    tVit_.setLongueurValidator(LongueurFieldValidator.creeMin(1));
    longueurList.add(tVit_);
    longueursAValider_ = (LongueurField[]) longueurList.toArray(new LongueurField[longueurList.size()]);
    setVisible(true);

    // addInternalFrameListener(this);
    imp_.addInternalFrame(this);

  }

  /**
   * @param _e
   */
  public void actionPerformed(final ActionEvent _e) {
    if (_e.getSource() == bAnnuler_) {
      annuler();
    }

    else if (_e.getSource() == bSauver_) {
      if (controlerEntrees()) {
        if (modif_) {
          annuler();
        } else {
          initialiseChamps(-1);
        }

      }
      tId_.requestFocus();

    } else if (_e.getSource() == bAfficher_) {
      afficherBiefs();
    }
  }

  private void annuler() {
    imp_.removeInternalFrame(this);
    imp_.resetFille2ModBiefs();

  }

  private boolean controlerEntrees() {

    if (tId_.getText().equals("")) {
      imp_.affMessage("Entrez l'identifiant du type de bief");
      return false;
    }

    for (int i = 0; i < longueursAValider_.length; i++) {
      if (!longueursAValider_[i].isValueValid()) {
        imp_.affMessage(longueursAValider_[i].val_.getDescription());
        return false;
      }
    }

    final Sinavi2TypeBief b = new Sinavi2TypeBief(tId_.getText(), tLong_.getLongueurField(), tLarg_.getLongueurField(),
        tHaut_.getLongueurField(), tVit_.getLongueurField());
    SParametresBief bief = new SParametresBief();
    bief = b.bief_;
    addBief(b.getIdentification(), bief);
    return true;

  }

  private void addBief(final String _nom, final SParametresBief _bief) {

    addListeBief(_bief);
    imp_.affMessage("Creation du bief de nom " + _nom + " r�ussie.");
  }

  /**
   * @param _b bief � ajouter on ajoute le bief en �crasant l'ancien s'il existe d�j� sinon on l'ajoute en fin de liste
   */
  public void addListeBief(final SParametresBief _b) {
    final int x = rechercherBief(_b.identification);
    System.out.println("x: " + x);
    if (x == -1) {
      imp_.definirIndisponibilites(_b, null);
      listeBiefs_.add(_b);
    } else {

      _b.gareEnAmont = ((SParametresBief) listeBiefs_.get(x)).gareEnAmont;
      _b.gareEnAval = ((SParametresBief) listeBiefs_.get(x)).gareEnAval;
      listeBiefs_.remove(x);
      imp_.definirIndisponibilites(_b, null);
      listeBiefs_.add(x, _b);
    }
  }

  /**
   * @param _biefCourant nom du bief � chercher
   * @return l'indice du bief
   */

  private int rechercherBief(final String _biefCourant) {
    final ListIterator iter = listeBiefs_.listIterator();
    int i = 0;
    boolean trouve = false;
    while (iter.hasNext() && !trouve) {
      SParametresBief c = new SParametresBief();
      c = (SParametresBief) iter.next();
      if (c.identification.equalsIgnoreCase(_biefCourant)) {
        trouve = true;
        return i;
      } else {
        i++;
      }
    }
    return -1;
  }

  /**
   * affiche l'�cran des biefs
   */
  private void afficherBiefs() {
    if (listeBiefs_ != null) {
      imp_.afficherBiefs();
    }
  }

  public void setNBief(final int _bief) {
    nBief_ = _bief;
  }

  public void internalFrameActivated(final InternalFrameEvent e) {
  // TODO Auto-generated method stub

  }

  public void internalFrameClosed(final InternalFrameEvent e) {

  // TODO Auto-generated method stub

  }

  public void internalFrameClosing(final InternalFrameEvent e) {
  // TODO Auto-generated method stub

  }

  public void internalFrameDeactivated(final InternalFrameEvent e) {
  // TODO Auto-generated method stub

  }

  public void internalFrameDeiconified(final InternalFrameEvent e) {
  // TODO Auto-generated method stub

  }

  public void internalFrameIconified(final InternalFrameEvent e) {
  // TODO Auto-generated method stub

  }

  public void internalFrameOpened(final InternalFrameEvent e) {
  // TODO Auto-generated method stub

  }

  /**
   * @param _bief -1 si ajout initialisation des champs num�ro du bief dans la liste si modifications initialisation des
   *          champs avec les valeurs du bief
   */

  public void initialiseChamps(final int _bief) {
    System.out.println("Entr�e dans initialise champs");
    if (_bief == -1) {
      System.out.println("Entr�e dans initialise champs bief -1");
      tId_.setText("");
      tLong_.setLongueurField(0);
      tLarg_.setLongueurField(0);
      tHaut_.setLongueurField(0);
      tVit_.setLongueurField(0);

    } else {

      final SParametresBief biefCourant_ = (SParametresBief) listeBiefs_.get(nBief_);
      tId_.setText(biefCourant_.identification);
      tLong_.setLongueurField(biefCourant_.longueur);
      tLarg_.setLongueurField(biefCourant_.largeur);
      tHaut_.setLongueurField(biefCourant_.hauteur);
      tVit_.setLongueurField(biefCourant_.vitesse * 1000);

    }

  }

  public void focusGained(final FocusEvent _e) {

  }

  /**
   * V�rification des champs en cas de perte de focus
   */
  public void focusLost(final FocusEvent _e) {
    if (_e.getSource() == tId_) {
      if (tId_.getText().equalsIgnoreCase("")) {
        lId_.setForeground(Color.RED);

      } else {
        lId_.setForeground(CtuluLibSwing.getDefaultLabelForegroundColor());
      }

    }
  }

}
