package org.fudaa.fudaa.sinavi2;

/**
 * @author ka Le type Sinavi2TypeConsommation a �t� cr�� afin de selectionner des donn�es dans le fichier *.cons pour
 *         r�aliser des graphes,des tableaux et faire des calculs et notamment des %
 */

public class Sinavi2TypeConsommation {

  /** * constructeur sans param�tre* */
  public Sinavi2TypeConsommation() {}

  /**
   * @param numero de l'�cluse
   * @param nom de l'�cluse
   * @param nombre de bassin�es en amont
   * @param nombre de bassin�es en aval
   * @param nombre de bassin�es dans les deux sens (en aval et en amont)
   * @param nombre de fausses bassin�es en amont
   * @param nombre de fausses bassin�es en aval
   * @param nombre de fausses bassin�es dans les deux sens (en aval et en amont)
   * @param pourcentage de fausses bassin�es /(nbre de bassin�es et fausses bassin�es) en amont
   * @param pourcentage de fausses bassin�es /(nbre de bassin�es et fausses bassin�es) en aval
   * @param pourcentage de fausses bassin�es /(nbre de bassin�es et fausses bassin�es) dans les deux sens(en amont et en
   *          aval)
   * @param consommation en amont
   * @param consommation en aval
   * @param consommation dans les deux sens(amont et aval)
   * @param tableau des types de bateau
   * @param tableau du nombre de bateaux par type en avalant
   * @param tableau du nombre de bateaux par type en montant
   * @param taux de remplissage en amont
   * @param taux de remplissage en aval
   * @param taux de remplissage dans les deux sens (en amont et en aval)
   */

  public Sinavi2TypeConsommation(final Sinavi2TypeConsommation _cons) {
    numecluse_ = _cons.numecluse_;
    ecluse_ = _cons.ecluse_;
    nbBassineesMontantes_ = _cons.nbBassineesMontantes_;
    nbBassineesAvalantes_ = _cons.nbBassineesAvalantes_;
    nbTotalBassinees_ = _cons.nbTotalBassinees_;
    nbFaussesBassineesMontantes_ = _cons.nbFaussesBassineesMontantes_;
    nbFaussesBassineesAvalantes_ = _cons.nbFaussesBassineesAvalantes_;
    nbTotalFaussesBassinees_ = _cons.nbTotalFaussesBassinees_;
    tauxFaussesBassineesMontantes_ = _cons.tauxFaussesBassineesMontantes_;
    tauxFaussesBassineesAvalantes_ = _cons.tauxFaussesBassineesAvalantes_;
    tauxFaussesBassineesTotal_ = _cons.tauxFaussesBassineesTotal_;
    consommationDEauAvalant_ = _cons.consommationDEauAvalant_;
    consommationDEauMontant_ = _cons.consommationDEauMontant_;
    consommationDEau_ = _cons.consommationDEau_;
    listeTypesBateaux_ = _cons.listeTypesBateaux_;
    listeNombreBateauxMontant_ = _cons.listeNombreBateauxMontant_;
    listeNombreBateauxAvalant_ = _cons.listeNombreBateauxAvalant_;
    tauxRemplissageAvalant_ = _cons.tauxRemplissageAvalant_;
    tauxRemplissageMontant_ = _cons.tauxRemplissageMontant_;
    tauxRemplissageTotal_ = _cons.tauxRemplissageTotal_;

  }

  /** ****************accesseurs************************************** */
  public int getNumeroEcluse() {
    return numecluse_;
  }

  public void setNumeroEcluse() {

  }

  public String getEcluse() {
    return ecluse_;
  }

  public void setEcluse(final String _ecluse) {
    ecluse_ = _ecluse;
  }

  public int getNbBassineesMontantes() {
    return nbBassineesMontantes_;
  }

  public void setNbBassineesMontantes(final int _nbBassineesMontantes) {
    nbBassineesMontantes_ = _nbBassineesMontantes;
  }

  public int getNbBassineesAvalantes() {
    return nbBassineesAvalantes_;
  }

  public void setNbBassineesAvalantes(final int _nbBassineesAvalantes) {
    nbBassineesAvalantes_ = _nbBassineesAvalantes;
  }

  public int getNbTotalBassinees() {
    return nbTotalBassinees_;
  }

  public void setNbTotalBassinees(final int _nbTotalBassinees) {
    nbTotalBassinees_ = _nbTotalBassinees;
  }

  public int getNbFaussesBassineesMontantes() {
    return nbFaussesBassineesMontantes_;
  }

  public void setNbFaussesBassineesMontantes(final int _nbFaussesBassineesMontantes) {
    nbFaussesBassineesMontantes_ = _nbFaussesBassineesMontantes;
  }

  public int getNbFaussesBassineesAvalantes() {
    return nbFaussesBassineesAvalantes_;
  }

  public void setNbFaussesBassineesAvalantes(final int _nbFaussesBassineesAvalantes) {
    nbFaussesBassineesAvalantes_ = _nbFaussesBassineesAvalantes;
  }

  public int getNbTotalFaussesBassinees() {
    return nbTotalFaussesBassinees_;
  }

  public void setNbTotalFaussesBassinees(final int _nbTotalFaussesBassinees) {
    nbTotalFaussesBassinees_ = _nbTotalFaussesBassinees;
  }

  public double getTauxFaussesBassineesAvalantes() {
    return tauxFaussesBassineesAvalantes_;
  }

  public void setTauxFaussesBassineesAvalantes(final double _tauxFaussesBassineesAvalantes) {
    tauxFaussesBassineesAvalantes_ = _tauxFaussesBassineesAvalantes;
  }

  public double getTauxFaussesBassineesMontantes() {
    return tauxFaussesBassineesMontantes_;
  }

  public void setTauxFaussesBassineesMontantes(final double _tauxFaussesBassineesMontantes) {
    tauxFaussesBassineesMontantes_ = _tauxFaussesBassineesMontantes;
  }

  public double getTauxFaussesBassineesTotal() {
    return tauxFaussesBassineesTotal_;
  }

  public void setTauxFaussesBassineesTotal(final double _tauxFaussesBassineesTotal) {
    tauxFaussesBassineesTotal_ = _tauxFaussesBassineesTotal;
  }

  public double getConsommationDEauAvalant() {
    return consommationDEauAvalant_;
  }

  public void setConsommationDEauAvalant(final double _consommationDEauAvalant) {
    consommationDEauAvalant_ = _consommationDEauAvalant;
  }

  public double getConsommationDEauMontant() {
    return consommationDEauMontant_;
  }

  public void setConsommationDEauMontant(final double _consommationDEauMontant) {
    consommationDEauMontant_ = _consommationDEauMontant;
  }

  public double getConsommationDEau() {
    return consommationDEau_;
  }

  public void setConsommationDEau(final double _consommationDEau) {
    consommationDEau_ = _consommationDEau;
  }

  public double getTauxRemplissageAvalant() {
    return tauxRemplissageAvalant_;
  }

  public void setTauxRemplissageAvalant(final double _tauxRemplissageAvalant) {
    tauxRemplissageAvalant_ = _tauxRemplissageAvalant;
  }

  public double getTauxRemplissageMontant() {
    return tauxRemplissageAvalant_;
  }

  public void setTauxRemplissageMontant(final double _tauxRemplissageMontant) {
    tauxRemplissageMontant_ = _tauxRemplissageMontant;
  }

  public double getTauxRemplissageTotal() {
    return tauxRemplissageTotal_;
  }

  public void setTauxRemplissageTotal(final double _tauxRemplissageTotal) {
    tauxRemplissageTotal_ = _tauxRemplissageTotal;
  }

  public void calculeTotauxBassinee() {
    nbTotalBassinees_ = nbBassineesMontantes_ + nbBassineesAvalantes_;
  }

  public void calculeTotauxFaussesBassinees() {
    nbTotalFaussesBassinees_ = nbFaussesBassineesMontantes_ + nbFaussesBassineesAvalantes_;
  }

  /** * M�thode permettant de calculer les taux de fausses bassin�es en amont, en aval et dans les deux sens** */
  public void calculeTauxFaussesBassinees() {
    double totalM = 0, totalA = 0, total = 0;
    tauxFaussesBassineesMontantes_ = nbFaussesBassineesMontantes_ * 100;
    totalM = nbFaussesBassineesMontantes_ + nbBassineesMontantes_;
    tauxFaussesBassineesMontantes_ = tauxFaussesBassineesMontantes_ / totalM;
    Double d = new Double(tauxFaussesBassineesMontantes_);
    if (d.isNaN()) {
      tauxFaussesBassineesMontantes_ = 0;
    } else {
      tauxFaussesBassineesMontantes_ = Sinavi2Lib.conversionDeuxChiffres(tauxFaussesBassineesMontantes_);
    }

    tauxFaussesBassineesAvalantes_ = nbFaussesBassineesAvalantes_ * 100;
    totalA = nbFaussesBassineesAvalantes_ + nbBassineesAvalantes_;
    tauxFaussesBassineesAvalantes_ = tauxFaussesBassineesAvalantes_ / totalA;
    d = new Double(tauxFaussesBassineesAvalantes_);
    if (d.isNaN()) {
      tauxFaussesBassineesAvalantes_ = 0;
    } else {
      tauxFaussesBassineesAvalantes_ = Sinavi2Lib.conversionDeuxChiffres(tauxFaussesBassineesAvalantes_);
    }

    tauxFaussesBassineesTotal_ = nbTotalFaussesBassinees_ * 100;
    total = nbTotalFaussesBassinees_ + nbTotalBassinees_;
    tauxFaussesBassineesTotal_ = tauxFaussesBassineesTotal_ / total;
    d = new Double(tauxFaussesBassineesTotal_);
    if (d.isNaN()) {
      tauxFaussesBassineesTotal_ = 0;
    } else {
      tauxFaussesBassineesTotal_ = Sinavi2Lib.conversionDeuxChiffres(tauxFaussesBassineesTotal_);
    }
  }

  int numecluse_; // num�ro de l'�cluse
  String ecluse_; // nom de l'�cluse
  int nbBassineesMontantes_; // nombre de bassin�es en amont de l'�cluse
  int nbBassineesAvalantes_; // nombre de bassin�es en aval de l'�cluse
  int nbTotalBassinees_; // nombre de bassin�es en amont et en aval
  int nbFaussesBassineesMontantes_; // nombre de fausses bassin�es en amont
  int nbFaussesBassineesAvalantes_; // nombre de fausses bassin�es en aval
  int nbTotalFaussesBassinees_; // nombre de fausses bassin�es en amont et en aval
  double tauxFaussesBassineesMontantes_; // pourcentage de fausses bassin�es en amont (/nombre de bassin�es et nombre
                                          // de fausses bassin�es en amont)
  double tauxFaussesBassineesAvalantes_; // pourcentage de fausses bassin�es en aval (/nombre de bassin�es et nombre de
                                          // fausses bassin�es en aval)
  double tauxFaussesBassineesTotal_; // pourcentage de fausses bassin�es en amont et aval (/nombre de bassin�es et
                                      // nombre de fausses bassin�es en amont et aval)
  double consommationDEau_; // consommation d'eau en amont et en aval
  double consommationDEauAvalant_;// consommation d'eau en aval
  double consommationDEauMontant_;// consommation d'eau en amont
  int[] listeTypesBateaux_; // tableau des types de bateau
  int[] listeNombreBateauxAvalant_;// tableau du nombre de bateaux par type en avalant
  int[] listeNombreBateauxMontant_;// tableau du nombre de bateaux par type en montant;
  double tauxRemplissageAvalant_; // taux de remplissage en aval
  double tauxRemplissageMontant_; // taux de remplissage en amont
  double tauxRemplissageTotal_; // taux de remplissage en amont et en aval

}
