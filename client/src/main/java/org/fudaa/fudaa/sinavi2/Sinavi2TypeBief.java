package org.fudaa.fudaa.sinavi2;

import org.fudaa.dodico.corba.sinavi2.SParametresBief;

/**
 * @author maneuvrier cf Sinavi2TypeBateau Le type Sinavi2TypeBief a �t� cr�� afin d'encapsuler les donn�es entr�es par
 *         l'utilisateur et de les adapter au type SParametresBief, structure corba utilis�e pour la communication
 *         Fortran
 */

class Sinavi2TypeBief {

  // constructeur
  public Sinavi2TypeBief() {}

  /**
   * Constructeur de Type Bief
   * 
   * @param _id : identifiant du bief non null
   * @param _longueur : longueur en m
   * @param _largeur : largeur en m
   * @param _hauteur : hauteur en m
   * @param _vitesse : vitesse en km/H maximale autoris�e dans le bief, on ne pourra en aucun cas la d�passer
   */
  public Sinavi2TypeBief(final String _id, final double _longueur, final double _largeur, final double _hauteur,
      final double _vitesse) {

    bief_ = new SParametresBief();
    bief_.identification = _id;
    bief_.longueur = _longueur;
    bief_.largeur = _largeur;
    bief_.hauteur = _hauteur;
    bief_.vitesse = _vitesse / 1000;
    bief_.gareEnAmont = 0;
    bief_.gareEnAval = 0;
  }

  // accesseurs
  public String getIdentification() {
    return bief_.identification;
  }

  public double getLongueur() {
    return bief_.longueur;
  }

  public double getLargeur() {
    return bief_.largeur;
  }

  public double getHauteur() {
    return bief_.hauteur;
  }

  public double getVitesse() {
    return bief_.vitesse;
  }

  public int getGareEnAmont() {
    return bief_.gareEnAmont;
  }

  public int getGareEnAval() {
    return bief_.gareEnAval;
  }

  // modifieurs
  public void setIdentification(final String _id) {
    bief_.identification = _id;
  }

  public void setLongueur(final double _longueur) {
    bief_.longueur = _longueur;
  }

  public void setLargeur(final double _largeur) {
    bief_.largeur = _largeur;
  }

  public void setHauteur(final double _hauteur) {
    bief_.hauteur = _hauteur;
  }

  public void setVitesse(final double _vitesse) {
    bief_.vitesse = _vitesse;
  }

  public void setGareEnAmont(final int _gare) {
    bief_.gareEnAmont = _gare;
  }

  public void setGareEnAval(final int _gare) {
    bief_.gareEnAval = _gare;
  }

  public boolean typeBiefEquals(final Sinavi2TypeBief _b) {
    return (this.getGareEnAmont() == _b.getGareEnAmont() && this.getGareEnAval() == _b.getGareEnAval()
        && this.getHauteur() == _b.getHauteur() && this.getIdentification() == _b.getIdentification()
        && this.getLargeur() == _b.getLargeur() && this.getLongueur() == _b.getLongueur() && this.getVitesse() == _b
        .getVitesse());
  }

  SParametresBief bief_;
}
