/*
 * @creation     2001-05-17
 * @modification $Date: 2007-06-04 16:57:12 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */

package org.fudaa.fudaa.sinavi2;

import java.awt.Container;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

import com.memoire.bu.*;
import com.memoire.fu.FuLib;
import com.memoire.fu.FuLog;

import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluLibArray;
import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluTaskDelegate;
import org.fudaa.ctulu.ProgressionInterface;

import org.fudaa.dodico.corba.objet.IConnexion;
import org.fudaa.dodico.corba.objet.IPersonne;
import org.fudaa.dodico.corba.sinavi2.*;

import org.fudaa.dodico.sinavi2.DCalculSinavi2;
import org.fudaa.dodico.sinavi2.DParametresSinavi2;
import org.fudaa.dodico.sinavi2.Sinavi2Readers;
import org.fudaa.dodico.sinavi2.Sinavi2Writers;

import org.fudaa.ebli.graphe.BGraphe;

import org.fudaa.fudaa.commun.FudaaAstucesAbstract;
import org.fudaa.fudaa.commun.FudaaLib;
import org.fudaa.fudaa.commun.dodico.FudaaDodicoTacheConnexion;
import org.fudaa.fudaa.commun.dodico.FudaaImplementation;
import org.fudaa.fudaa.commun.projet.FudaaFiltreFichier;
import org.fudaa.fudaa.commun.projet.FudaaProjet;

/**
 * Implementation de l'application.
 * 
 * @version $Id: Sinavi2Implementation.java,v 1.35 2007-06-04 16:57:12 clavreul Exp $
 * @version $Id: Sinavi2Implementation.java,v 1.35 2007-06-04 16:57:12 clavreul Exp $
 * @author Beno�t Maneuvrier, Fatimatou Ka
 */
public class Sinavi2Implementation extends FudaaImplementation {
  /**
   * assistant du bureau.
   */
  protected static BuAssistant assistant_;
  protected static BuInformationsDocument InfoDocsSinavi2_ = new BuInformationsDocument();
  /* informations sur sinavi2 */
  protected static BuInformationsSoftware InfoSoftSinavi2_ = new BuInformationsSoftware();
  // public static Sinavi2GestionProjet sinavi2gestionprojet_;
  /**
   * Etude Sinavi initialis�e dans la fonction creer() ou ouvrir()
   */
  /*
   * public static IEtudeNavigationFluviale ETUDE_SINAVI2 = null; public static IServiceSinavi SERVICE_SINAVI2 = null;
   */
  protected static FudaaProjet projet_;
  protected final static int RECENT_COUNT = 10;
  public static IConnexion CONNEXION_SINAVI2;

  // public final static String LOCAL_UPDATE = ".";
  public final static int NB_SIMULATIONS = 6;

  public static IPersonne PERSONNE;
  public static ICalculSinavi2 SERVEUR_SINAVI2;
  static {
    InfoSoftSinavi2_.name = "Sinavi2";
    InfoSoftSinavi2_.version = "0.70";
    InfoSoftSinavi2_.date = "4-juin-2007";
    InfoSoftSinavi2_.rights = "Tous droits r�serv�s. CETMEF (c)2005";
    InfoSoftSinavi2_.contact = "alain.chambreuil@equipement.gouv.fr";
    InfoSoftSinavi2_.license = "GPL2";
    InfoSoftSinavi2_.languages = "fr";
    InfoSoftSinavi2_.http = "http://www.cetmef.equipement.gouv.fr/";
    InfoSoftSinavi2_.update = "";
    InfoSoftSinavi2_.man = "";
    InfoSoftSinavi2_.logo = Sinavi2Resource.SINAVI2.getIcon("sinavi2logo");
    InfoSoftSinavi2_.banner = Sinavi2Resource.SINAVI2.getIcon("sinavi2pres2");
    InfoSoftSinavi2_.authors = new String[] { "Beno�t MANEUVRIER & Fatimatou KA" };
    InfoSoftSinavi2_.contributors = new String[] { "Equipes Dodico, Ebli et Fudaa" };
    InfoSoftSinavi2_.documentors = new String[] {};
    InfoSoftSinavi2_.testers = new String[] { "Alain POURPLANCHE", "Nicolas CLAVREUL", "Alain CHAMBREUIL" };
    InfoDocsSinavi2_.name = "Etude";
    InfoDocsSinavi2_.version = "0.70";
    InfoDocsSinavi2_.organization = "CETMEF";
    InfoDocsSinavi2_.author = System.getProperty("user.name");
    InfoDocsSinavi2_.contact = InfoDocsSinavi2_.author + "@equipement.gouv.fr";
    InfoDocsSinavi2_.date = FuLib.date();
    BuPrinter.INFO_LOG = InfoSoftSinavi2_;
    BuPrinter.INFO_DOC = InfoDocsSinavi2_;
  }

  /**
   * Informations concernant l'application.
   */
  public static BuInformationsSoftware informationsSoftware() {
    return InfoSoftSinavi2_;
  }
  private String nomPrj_;
  /**
   * fen�tre affichant l'aide.
   */
  protected BuHelpFrame aide_;
  protected Sinavi2FilleGraphe fillegraphe_;
  /**
   * composant permettant de stopper l'interactivite de Sinavi2.
   */
  protected BuGlassPaneStop glassStop_;
  protected Sinavi2ResAffTableauResultat sinavi2afftableauresultat_;
  protected Sinavi2FilleAddModBateaux sinavi2filleaddmodbateaux_;
  protected Sinavi2FilleAddModBiefs sinavi2filleaddmodbiefs_;
  protected Sinavi2FilleAddModConnexion sinavi2filleaddmodconnexions_;
  protected Sinavi2FilleAddModEcluses sinavi2filleaddmodecluses_;
  protected Sinavi2FilleAddModIndisponibilites sinavi2filleaddmodindisponibilites_;
  protected Sinavi2FilleAffBateaux sinavi2filleaffbateaux_;
  protected Sinavi2FilleAffBiefs sinavi2filleaffbiefs_;
  protected Sinavi2FilleAffConnexions sinavi2filleaffconnexions_;
  protected Sinavi2FilleAffControles sinavi2filleaffcontroles_;
  protected Sinavi2FilleAffCroisements sinavi2filleaffcroisements_;
  protected Sinavi2FilleAffEcluses sinavi2filleaffecluses_;
  protected Sinavi2FilleAffGeneration sinavi2filleaffgeneration_;
  protected Sinavi2FilleAffHistorique sinavi2filleaffhistorique_;
  protected Sinavi2FilleAffIndisponibilites sinavi2filleaffindisponibilites_;
  protected Sinavi2FilleAffManoeuvres sinavi2filleaffmanoeuvres_;
  protected Sinavi2FilleAffTrajets sinavi2filleafftrajets_;
  protected Sinavi2FilleAffTrematages sinavi2filleafftrematages_;
  protected Sinavi2FilleAffVitesses sinavi2filleaffvitesses_;
  protected Sinavi2FilleModManoeuvres sinavi2fillemodmanoeuvres_;
  protected Sinavi2FilleModVitesses sinavi2fillemodvitesses_;
  protected Sinavi2FilleInitialiserTirage sinavi2filletirage_;
  protected Sinavi2ResAffTableauConso sinavi2resafftableauconso_;

  /**
   * visualiseur des taches en cours.
   */
  protected BuTaskView taches_;

  // public SParametresGeneration gen_;
  public CtuluAnalyze an_ = new CtuluAnalyze();

  public String commentaireCourant_;
  public SParametresGeneration gen_;
  public ArrayList listeAttentes_;
  // liste courantes
  public ArrayList listeBateaux_;
  public ArrayList listeBiefs_;
  public ArrayList listeCons_;
  public ArrayList listeConsommation_;
  public ArrayList listeCroisements_;
  public ArrayList listeEcluses_;
  public ArrayList listeGares_;
  public ArrayList listeGeneration_;
  public ArrayList listeHistorique_;
  public ArrayList listeManoeuvres_;
  // protected Sinavi2AffTableauResultat sinavi2afftableauresultat_;
  // public ArrayList listesBateaux_;
  // public ArrayList listesBiefs_;
  // public ArrayList listesCroisements_;

  // public ArrayList listesEcluses_;
  public ArrayList listeSimulationsSinavi2_ = new ArrayList();
  // public ArrayList listesManoeuvres_;
  // public ArrayList listesTrajets_;
  // public ArrayList listesTrematages_;
  // public ArrayList listesVitesses_;
  public Sinavi2TrajetMng listeTrajets_;
  public ArrayList listeTrematages_;
  public ArrayList listeVitesses_;
  public String nomSimCourant_ = "";

  public IParametresSinavi2 param_;// param_ = new DParametresSinavi2();

  public SSimulationSinavi2 paramCourant_;

  public boolean permettreModif_;
  public IResultatsSinavi2 res_;
  public SResultatConsommationDEau[] resConsoDEau_;

  // public ArrayList listeInitialise_;
  public int simulationCourante_;// indice de la simulation en cours
  // public int nouvelleSimulation_;

  public int[] simulationsSel_;

  private String[] createDialogOptions() {
    return new String[] { "Ignorer", "Renommer", "Ecraser" };
  }

  private ArrayList createList(final Object[] _o) {
    return _o == null ? new ArrayList(20) : new ArrayList(Arrays.asList(_o));
  }

  private void deleteBateau(final int x) {
    final String batcourant = ((SParametresBateau) listeBateaux_.get(x)).identification;
    listeBateaux_.remove(x);
    affMessage("Le type de bateau " + batcourant + " est supprim� !");
    // automatisation des suppressions en cascade trajet, croisements,trematages,vitesses,manoeuvres
    /** trajets* */
    if (listeTrajets_ != null) {
      listeTrajets_.removeBat(batcourant);
    }

    /** croisements* */
    if (listeCroisements_ != null) {
      final ListIterator it = listeCroisements_.listIterator();
      int indice = 0;
      int[] tabIndice = new int[0];
      while (it.hasNext()) {
        final SParametresCroisements t = (SParametresCroisements) it.next();
        if (t.type1.equalsIgnoreCase(batcourant) || t.type2.equalsIgnoreCase(batcourant)) {
          final int[] temp = new int[tabIndice.length + 1];
          for (int k = 0; k < tabIndice.length; k++) {
            temp[k] = tabIndice[k];
          }
          temp[temp.length - 1] = indice;
          tabIndice = temp;
        }
        indice++;
      }
      for (int k = 0; k < tabIndice.length; k++) {
        listeCroisements_.remove(tabIndice[k] - k);
      }
    }

    /** trematages* */
    if (listeTrematages_ != null) {
      final ListIterator it = listeTrematages_.listIterator();
      int indice = 0;
      int[] tabIndice = new int[0];
      while (it.hasNext()) {
        final SParametresTrematages t = (SParametresTrematages) it.next();
        if (t.type1.equalsIgnoreCase(batcourant) || t.type2.equalsIgnoreCase(batcourant)) {
          final int[] temp = new int[tabIndice.length + 1];
          for (int k = 0; k < tabIndice.length; k++) {
            temp[k] = tabIndice[k];
          }
          temp[temp.length - 1] = indice;
          tabIndice = temp;
        }
        indice++;
      }
      for (int k = 0; k < tabIndice.length; k++) {
        listeTrematages_.remove(tabIndice[k] - k);
      }
    }
    /** vitesses* */
    if (listeVitesses_ != null) {
      final ListIterator it = listeVitesses_.listIterator();
      int indice = 0;
      int[] tabIndice = new int[0];
      while (it.hasNext()) {
        final SParametresVitesses t = (SParametresVitesses) it.next();
        if (t.bateau.equalsIgnoreCase(batcourant)) {
          final int[] temp = new int[tabIndice.length + 1];
          for (int k = 0; k < tabIndice.length; k++) {
            temp[k] = tabIndice[k];
          }
          temp[temp.length - 1] = indice;
          tabIndice = temp;
        }
        indice++;
      }
      for (int k = 0; k < tabIndice.length; k++) {
        listeVitesses_.remove(tabIndice[k] - k);
      }
    }
    /** manoeuvres* */
    if (listeManoeuvres_ != null) {
      final ListIterator it = listeManoeuvres_.listIterator();
      int indice = 0;
      int[] tabIndice = new int[0];
      while (it.hasNext()) {
        final SParametresManoeuvres t = (SParametresManoeuvres) it.next();
        if (t.type.equalsIgnoreCase(batcourant)) {
          final int[] temp = new int[tabIndice.length + 1];
          for (int k = 0; k < tabIndice.length; k++) {
            temp[k] = tabIndice[k];
          }
          temp[temp.length - 1] = indice;
          tabIndice = temp;
        }
        indice++;
      }
      for (int k = 0; k < tabIndice.length; k++) {
        listeManoeuvres_.remove(tabIndice[k] - k);
      }
    }
  }

  private void exporterAll() {
    final File f = ouvrirFileChooser("Exporter projet sinavi", new String[] { "bie", "ouv", "cnx", "vites", "croist",
        "tremat", "man", "trajet", "nav" }, true);
    if (f == null) {
      return;
    }
    String nom = nomPrj_;
    File dir = f;
    if (!f.isDirectory()) {
      nom = CtuluLibFile.getSansExtension(f.getName());
      dir = f.getParentFile();
    }
    if (nom == null || dir == null) {
      return;
    }
    final File dirExp = dir;
    final String nomExp = nom;
    final CtuluTaskDelegate task = createTask("export");
    final ProgressionInterface prog = task.getStateReceiver();
    task.start(new Runnable() {

      public void run() {
        genererFichiers(dirExp, nomExp, prog);
      }
    });

  }

  /**
   * m�thode permettant d'exporter un fichier.nav.
   */
  private void exporterBateau() {
    exporterBateau(false);
  }

  /** m�thode permettant d'exporter un fichier.nav2. * */
  private void exporterBateau(final boolean _v2) {
    if (CtuluLibArray.isEmpty(listeBateaux_)) {
      affMessage(getNoBateauMsg());
    } else {
      final BuFileChooser chooser = new BuFileChooser();
      final int returnVal = chooser.showOpenDialog((JFrame) getApp());

      if (returnVal == JFileChooser.APPROVE_OPTION) {
        File file = chooser.getSelectedFile();
        final String ext = _v2 ? ".nav2" : ".nav";
        final String name = file.getAbsolutePath();
        if (!name.endsWith(ext)) {
          file = new File(name + ext);
        }
        if (_v2) {
          Sinavi2Writers.doBateauxV2((SParametresBateau[]) listeBateaux_.toArray(new SParametresBateau[listeBateaux_
              .size()]), file, null, an_);
        } else {
          Sinavi2Writers.doBateaux((SParametresBateau[]) listeBateaux_.toArray(new SParametresBateau[listeBateaux_
              .size()]), file, null, an_);
        }
        affMessage(getExportSuccess(file.getName()));
      }
    }
  }

  /** m�thode permettant d'exporter un fichier.nav2. * */
  private void exporterBateau2() {
    exporterBateau(true);
  }

  /** m�thode permettant d'exporter un fichier.bie. * */
  private void exporterBief() {
    if (listeBiefs_ != null) {
      if (listeBiefs_.size() != 0) {
        final BuFileChooser chooser = new BuFileChooser();
        final int returnVal = chooser.showOpenDialog((JFrame) getApp());

        if (returnVal == JFileChooser.APPROVE_OPTION) {
          String filename = chooser.getSelectedFile().getAbsolutePath();
          System.out.println(filename);
          final DParametresSinavi2 param2_ = new DParametresSinavi2();
          if (!filename.substring(filename.length() - 4).equalsIgnoreCase(".bie")) {
            filename = filename + ".bie";
          }

          final File _bie = new File(filename);
          param2_.parametresBief((SParametresBief[]) listeBiefs_.toArray(new SParametresBief[listeBiefs_.size()]));
          Sinavi2Writers.doBiefsExport(param2_.parametresBief(), _bie, null, an_);
          affMessage(getExportSuccess(filename));
        }
      } else {
        affMessage("Il n'y a pas de biefs dans la base.");
      }
    } else {
      affMessage("Il n'y a pas de biefs dans la base.");
    }
  }

  /** m�thode permettant d'exporter un fichier .ouv. */
  private void exporterEcluse() {
    if (listeEcluses_ != null) {
      if (listeEcluses_.size() != 0) {
        final BuFileChooser chooser = new BuFileChooser();
        final int returnVal = chooser.showOpenDialog((JFrame) getApp());

        if (returnVal == JFileChooser.APPROVE_OPTION) {
          String filename = chooser.getSelectedFile().getAbsolutePath();
          System.out.println(filename);
          final DParametresSinavi2 param2_ = new DParametresSinavi2();
          if (!filename.substring(filename.length() - 4).equalsIgnoreCase(".ouv")) {
            filename = filename + ".ouv";
          }

          final File _ouv = new File(filename);
          param2_.parametresEcluse((SParametresEcluse[]) listeEcluses_.toArray(new SParametresEcluse[listeEcluses_
              .size()]));
          Sinavi2Writers.doEclusesExport(param2_.parametresEcluse(), _ouv, null, an_);
          affMessage(getExportSuccess(filename));
        }
      } else {
        affMessage("Il n'y a pas d'�cluses dans la base.");
      }
    } else {
      affMessage("Il n'y a pas d'�cluses dans la base.");
    }
  }

  /**
   * m�thode permettant d'exporter un fichier .ouv2 .
   */
  private void exporterEcluse2() {
    if (listeEcluses_ != null) {
      if (listeEcluses_.size() != 0) {
        final BuFileChooser chooser = new BuFileChooser();
        final int returnVal = chooser.showOpenDialog((JFrame) getApp());

        if (returnVal == JFileChooser.APPROVE_OPTION) {
          String filename = chooser.getSelectedFile().getAbsolutePath();
          System.out.println(filename);
          final DParametresSinavi2 param2_ = new DParametresSinavi2();
          if (!filename.substring(filename.length() - 5).equalsIgnoreCase(".ouv2")) {
            filename = filename + ".ouv2";
          }

          final File _ouv2 = new File(filename);
          param2_.parametresEcluse((SParametresEcluse[]) listeEcluses_.toArray(new SParametresEcluse[listeEcluses_
              .size()]));
          Sinavi2Writers.doEclusesExportV2(param2_.parametresEcluse(), _ouv2, null, an_);
          affMessage(getExportSuccess(filename));
        }
      } else {
        affMessage("Il n'y a pas d'�cluses dans la base.");
      }
    } else {
      affMessage("Il n'y a pas d'�cluses dans la base.");
    }
  }

  /** * m�thode permettant d'exporter un fichier.res. */
  private void exporterReseau() {
    if (listeEcluses_ != null) {
      if (listeEcluses_.size() != 0) {
        final BuFileChooser chooser = new BuFileChooser();
        final int returnVal = chooser.showOpenDialog((JFrame) getApp());

        if (returnVal == JFileChooser.APPROVE_OPTION) {
          String filename = chooser.getSelectedFile().getAbsolutePath();
          if (!filename.substring(filename.length() - 4).equalsIgnoreCase(".res")) {
            filename = filename + ".res";
          }
          final File _res = new File(filename);
          final DParametresSinavi2 param2_ = new DParametresSinavi2();
          param2_.parametresEcluse((SParametresEcluse[]) listeEcluses_.toArray(new SParametresEcluse[listeEcluses_
              .size()]));
          param2_.parametresBief((SParametresBief[]) listeBiefs_.toArray(new SParametresBief[listeBiefs_.size()]));
          Sinavi2Writers.doReseauExport(param2_.parametresEcluse(), param2_.parametresBief(), _res, null, an_);
          affMessage(getExportSuccess(filename));
        }
      } else {
        affMessage("Un r�seau doit comporter au moins un bief et une �cluse.");
      }
    } else {
      affMessage("Un r�seau doit comporter au moins un bief et une �cluse.");
    }
  }

  private String getExportSuccess(final String _filename) {
    return "Le fichier " + _filename + " est export�.";
  }

  private String getNoBateauMsg() {
    return "Il n'y a pas de bateaux dans la base.";
  }

  private String getStrIdent() {
    return " identification ";
  }

  private void lancer() {
    nomPrj_ = ((SSimulationSinavi2) listeSimulationsSinavi2_.get(0)).nomPrj;
    if (listeBateaux_.size() == 0) {
      affMessage("Il n'y a pas de bateaux dans la base");
    } else if (listeVitesses_.size() == 0) {
      affMessage("Il n'y a pas de bateaux dans la base");
    } else if (listeTrajets_.size() == 0) {
      affMessage("Il n'y a pas de trajets dans la base");
    } else if (listeTrematages_.size() == 0) {
      affMessage("Vous n'avez pas indiqu� les r�gles de tr�matage");
    } else if (listeCroisements_.size() == 0) {
      affMessage("Vous n'avez pas indiqu� les r�gles de croisements");
    } else if (listeGares_.size() == 0) {
      affMessage("Le r�seau n'est pas d�fini");
    } else if (listeManoeuvres_.size() == 0) {
      affMessage("Vous n'avez pas indiqu� les temps de manoeuvres");
    } else if (getNumeroSimulationCourante() != listeSimulationsSinavi2_.size() - 1) {
      affMessage("S�lectionnez la \"future simulation\" et n'oubliez pas de v�rifier vos param�tres ");
    } else {
      calculer();
    }

  }

  /**
   * ajouter un index pour donner le bateaux en cours et mettre � blanc dans la classe fille pour nouveau bateaux
   * rajouter un parametre et uen condition.
   */

  private void modifierBateaux() {
    // test();
    if (listeBateaux_ != null) {
      if (listeBateaux_.size() != 0) {
        final int j = listeBateaux_.size();
        final String[] s = new String[j];
        final ListIterator iter = listeBateaux_.listIterator();
        int i = 0;
        while (iter.hasNext()) {
          SParametresBateau c = new SParametresBateau();
          c = (SParametresBateau) iter.next();
          s[i] = c.identification;
          i++;
        }
        final BuDialogChoice b = new BuDialogChoice(getApp(), InfoSoftSinavi2_, "Type de Bateaux � Modifier",
            "Choisissez :", s);
        if (b.activate() == 0) {
          final String batcourant = b.getValue();
          final int x = rechercherBateau(batcourant);
          if (sinavi2filleaddmodbateaux_ == null) {
            sinavi2filleaddmodbateaux_ = new Sinavi2FilleAddModBateaux(this, listeBateaux_, true);
          }
          sinavi2filleaddmodbateaux_.setnBat(x);
          sinavi2filleaddmodbateaux_.initialiseChamps(x);
          addInternalFrame(sinavi2filleaddmodbateaux_);
        } else {
          affMessage("Aucune modification effectu�e");
        }
      } else {
        final BuDialogMessage dialog_mess = new BuDialogMessage(getApp(), InfoSoftSinavi2_,
            "Il n'y aucun type de bateaux dans votre projet !");
        dialog_mess.activate();
      }
    } else {
      final BuDialogMessage dialog_mess = new BuDialogMessage(getApp(), InfoSoftSinavi2_,
          "Il n'y aucun type de bateaux dans votre projet !");
      dialog_mess.activate();
    }

  }

  private void modifierBiefs() {
    // test();
    if (listeBiefs_ != null && listeBiefs_.size() != 0) {
      final int j = listeBiefs_.size();
      /*
       * String nb=""+j; JOptionPane.showMessageDialog(null,nb);
       */

      final String[] s = new String[j];
      // test();
      // ={"bat1","bat2"};

      final ListIterator iter = listeBiefs_.listIterator();
      int i = 0;
      while (iter.hasNext()) {
        SParametresBief c = new SParametresBief();
        c = (SParametresBief) iter.next();
        s[i] = c.identification;
        i++;
        /*
         * Sinavi2TypeBateau c= new Sinavi2TypeBateau(); c= (Sinavi2TypeBateau) iter.next(); s[i]=(String)c.getId();
         * i++;
         */

      }
      // test();
      final BuDialogChoice b = new BuDialogChoice(getApp(), InfoSoftSinavi2_, "Type de Biefs � Modifier",
          "Choisissez :", s);
      // test();
      // if(b.getDefaultCloseOperation()!=0){
      // b.activate();

      if (b.activate() == 0) {
        final String biefcourant = b.getValue();
        final int x = rechercherBief(biefcourant);
        if (sinavi2filleaddmodbiefs_ == null) {
          sinavi2filleaddmodbiefs_ = new Sinavi2FilleAddModBiefs(this, listeBiefs_, true);
        }
        sinavi2filleaddmodbiefs_.setNBief(x);
        sinavi2filleaddmodbiefs_.initialiseChamps(x);
        addInternalFrame(sinavi2filleaddmodbiefs_);
        ajouterBief(x, true);
      } else {
        affMessage("Aucune modification effectu�e");
      }

    } else {
      final BuDialogMessage dialog_mess = new BuDialogMessage(getApp(), InfoSoftSinavi2_,
          "Il n'y aucun type de biefs dans votre projet !");
      dialog_mess.activate();

    }

  }

  /* declaration des menu du menu des donn�es du mod�le */

  private void modifierEcluses() {
    // test();
    if (listeEcluses_ != null && listeEcluses_.size() != 0) {
      final int j = listeEcluses_.size();
      /*
       * String nb=""+j; JOptionPane.showMessageDialog(null,nb);
       */

      final String[] s = new String[j];
      // test();
      // ={"bat1","bat2"};

      final ListIterator iter = listeEcluses_.listIterator();
      int i = 0;
      while (iter.hasNext()) {
        SParametresEcluse c = new SParametresEcluse();
        c = (SParametresEcluse) iter.next();
        s[i] = c.identification;
        i++;
        /*
         * Sinavi2TypeBateau c= new Sinavi2TypeBateau(); c= (Sinavi2TypeBateau) iter.next(); s[i]=(String)c.getId();
         * i++;
         */

      }
      // test();
      final BuDialogChoice b = new BuDialogChoice(getApp(), InfoSoftSinavi2_, "Type d'�cluses � Modifier",
          "Choisissez :", s);
      // test();
      // if(b.getDefaultCloseOperation()!=0){
      // b.activate();

      if (b.activate() == 0) {
        final String eclusecourant = b.getValue();
        final int x = rechercherEcluse(eclusecourant);
        if (sinavi2filleaddmodecluses_ == null) {
          sinavi2filleaddmodecluses_ = new Sinavi2FilleAddModEcluses(this, listeEcluses_, true);
        }
        sinavi2filleaddmodecluses_.setnEcluse(x);
        sinavi2filleaddmodecluses_.initialiseChamps(x);
        addInternalFrame(sinavi2filleaddmodecluses_);
        ajouterEcluse(x, true);
      } else {
        affMessage("Aucune modification effectu�e");
      }

    } else {
      final BuDialogMessage dialog_mess = new BuDialogMessage(getApp(), InfoSoftSinavi2_,
          "Il n'y a aucune �cluse dans votre projet !");
      dialog_mess.activate();

    }

  }

  /* declaration des menu du menu fonctionnement */

  /*
   * private int rechercher_bateau(String batcourant) { ListIterator iter= listeBateaux_.listIterator(); int i=0; while
   * (iter.hasNext()){ SParametresBateau c=new SParametresBateau(); c=(SParametresBateau) iter.next(); if(
   * c.identification == batcourant){ System.out.println("rechercher"+i); return i; } else i++; } return -1; }
   */
  private int rechercherBateau(final String batcourant) {
    final ListIterator iter = listeBateaux_.listIterator();
    int i = 0;
    while (iter.hasNext()) {
      SParametresBateau c = new SParametresBateau();
      c = (SParametresBateau) iter.next();
      if (c.identification.equalsIgnoreCase(batcourant)) {
        System.out.println("rechercher" + i);
        return i;
      } else {
        i++;
      }

    }
    return -1;
  }

  private int rechercherBief(final String _biefcourant) {
    for (int i = listeBiefs_.size() - 1; i >= 0; i--) {
      final SParametresBief c = (SParametresBief) listeBiefs_.get(i);
      if (c.identification.equalsIgnoreCase(_biefcourant)) {
        return i;
      }
    }
    return -1;
  }

  private int rechercherEcluse(final String _eclusecourant) {
    for (int i = listeEcluses_.size() - 1; i >= 0; i--) {
      final SParametresEcluse c = (SParametresEcluse) listeEcluses_.get(i);
      if (c.identification.equalsIgnoreCase(_eclusecourant)) {
        return i;
      }
    }
    return -1;

  }

  /*
   * public void remplirListes(final IParametresSinavi2 _params) { listeBateaux_ =
   * createList(_params.parametresBateau()); listeBiefs_ = createList(_params.parametresBief()); listeEcluses_ =
   * createList(_params.parametresEcluse()); listeCroisements_ = createList(_params.parametresCroisement());
   * listeTrematages_ = createList(_params.parametresTrematage()); listeVitesses_ =
   * createList(_params.parametresVitesse()); listeManoeuvres_ = createList(_params.parametresManoeuvre()); if
   * (listeTrajets_ == null) { listeTrajets_ = new Sinavi2TrajetMng(_params.parametresTrajet(), listeBateaux_, this); }
   * else { listeTrajets_.setTrajets(_params.parametresTrajet(), listeBateaux_, this); } }
   */
  // methode utilisee dans majSimulationCourante()
  private void remplirListes(final SSimulationSinavi2 _params) {
    listeBateaux_ = createList(_params.parametresBateau);
    listeBiefs_ = createList(_params.parametresBief);
    listeEcluses_ = createList(_params.parametresEcluse);
    listeCroisements_ = createList(_params.parametresCroisement);
    listeTrematages_ = createList(_params.parametresTrematage);
    listeVitesses_ = createList(_params.parametresVitesse);
    listeManoeuvres_ = createList(_params.parametresManoeuvre);
    listeHistorique_ = createList(_params.resultatHistorique);
    listeGeneration_ = createList(_params.resultatGenerationBateau);// nc : curieux que cette ligne ait ete absente
    if (listeTrajets_ == null) {
      listeTrajets_ = new Sinavi2TrajetMng(_params.parametresTrajet, listeBateaux_, this);
    } else {
      listeTrajets_.setTrajets(_params.parametresTrajet, listeBateaux_, this);
    }
  }

  private void remplirParametres() {
    try {
      param_ = IParametresSinavi2Helper.narrow(SERVEUR_SINAVI2.parametres(CONNEXION_SINAVI2));
    } catch (final NullPointerException e) {
      System.err.println("Pas de connexion serveur => local");
      affMessage("Vous n'etes pas connect� � un serveur SINAVI \nVous ne pourrez pas lancer de simulation");
    }
    if (listeBateaux_ != null) {
      param_.parametresBateau((SParametresBateau[]) listeBateaux_.toArray(new SParametresBateau[listeBateaux_.size()]));
    }
    if (listeBiefs_ != null) {
      param_.parametresBief((SParametresBief[]) listeBiefs_.toArray(new SParametresBief[listeBiefs_.size()]));
    }
    if (listeEcluses_ != null) {
      param_.parametresEcluse((SParametresEcluse[]) listeEcluses_.toArray(new SParametresEcluse[listeEcluses_.size()]));
    }
    if (listeVitesses_ != null) {
      param_.parametresVitesse((SParametresVitesses[]) listeVitesses_.toArray(new SParametresVitesses[listeVitesses_
          .size()]));
    }
    if (listeCroisements_ != null) {
      param_.parametresCroisement((SParametresCroisements[]) listeCroisements_
          .toArray(new SParametresCroisements[listeCroisements_.size()]));
    }
    if (listeTrematages_ != null) {
      param_.parametresTrematage((SParametresTrematages[]) listeTrematages_
          .toArray(new SParametresTrematages[listeTrematages_.size()]));
    }
    if (listeManoeuvres_ != null) {
      param_.parametresManoeuvre((SParametresManoeuvres[]) listeManoeuvres_
          .toArray(new SParametresManoeuvres[listeManoeuvres_.size()]));
    }
    if (listeTrajets_ != null) {
      param_.parametresTrajet(listeTrajets_.getTrajets());
    }
    param_.nomPrj(CtuluLibFile.getSansExtension(new File(nomPrj_).getName()));
    if (gen_ != null) {
      param_.parametresGeneration(gen_);
      // System.out.println(param_.parametresGeneration().graine+"--"+param_.parametresGeneration().dureeSimulation);
    }
  }

  /**
   * Methode utilis�e pour l'exportation de fichiers
   */
  void genererFichiers(final File _base, final String _nom, final ProgressionInterface _prog) {
    if (_base == null || !_base.isDirectory()) {
      error(Sinavi2Resource.SINAVI2.getString("Exporter"), CtuluLib.getS("Le chemin {0) n'est pas un r�pertoire",
          _base == null ? "inconnu" : _base.getAbsolutePath()));
      return;
    }
    final File nav = new File(_base, _nom + ".nav");
    final String error = CtuluLibFile.canWrite(nav);
    if (error != null) {
      error(Sinavi2Resource.SINAVI2.getString("Exporter"), error);
      return;
    }
    if (CtuluLibFile.exists(nav)
        && !question("Exporter", Sinavi2Resource.SINAVI2.getString(
            "Les fichiers sinavi {0}\n seront �cras�s.\nVoulez-vous continuer ?", CtuluLibFile.getSansExtension(nav)
                .getAbsolutePath()))) {
      return;
    }

    Sinavi2Writers.doBateaux((SParametresBateau[]) listeBateaux_.toArray(new SParametresBateau[listeBateaux_.size()]),
        nav, null, an_);
    _prog.setProgression(10);
    final File bie = new File(_base, _nom + ".bie");
    final SParametresBief[] biefs = (SParametresBief[]) listeBiefs_.toArray(new SParametresBief[listeBiefs_.size()]);
    Sinavi2Writers.doBiefs(biefs, bie, null, an_);
    _prog.setProgression(20);
    final File ouv = new File(_base, _nom + ".ouv");
    final SParametresEcluse[] ecluse = (SParametresEcluse[]) listeEcluses_.toArray(new SParametresEcluse[listeEcluses_
        .size()]);
    Sinavi2Writers.doEcluses(ecluse, ouv, null, an_);
    _prog.setProgression(30);
    final File cnx = new File(_base, _nom + ".cnx");
    // param_.parametresBateau((SParametresBateau[]) listeBateaux_.toArray(new
    // SParametresBateau[listeBateaux_.size()]));
    Sinavi2Writers.doGares(ecluse, biefs, cnx, null, an_);
    _prog.setProgression(40);
    final File vites = new File(_base, _nom + ".vites");
    Sinavi2Writers.doVitesses((SParametresVitesses[]) listeVitesses_.toArray(new SParametresVitesses[listeVitesses_
        .size()]), vites, null, an_);
    _prog.setProgression(40);
    final File croist = new File(_base, _nom + ".croist");
    Sinavi2Writers.doCroisements((SParametresCroisements[]) listeCroisements_
        .toArray(new SParametresCroisements[listeCroisements_.size()]), croist, null, an_);
    _prog.setProgression(50);
    final File tremat = new File(_base, _nom + ".tremat");
    Sinavi2Writers.doTrematages((SParametresTrematages[]) listeTrematages_
        .toArray(new SParametresTrematages[listeTrematages_.size()]), tremat, null, an_);
    _prog.setProgression(60);
    final File man = new File(_base, _nom + ".man");
    Sinavi2Writers.doManoeuvres((SParametresManoeuvres[]) listeManoeuvres_
        .toArray(new SParametresManoeuvres[listeManoeuvres_.size()]), man, null, an_);
    _prog.setProgression(70);
    final File trajet = new File(_base, _nom + ".trajet");
    Sinavi2Writers.doTrajets(listeTrajets_.getTrajets(), trajet, null, an_);
    _prog.setProgression(0);

  }

  void remplirResultats() { // methode uniquement invoquee a la fin du calcul dans oprCalculer()

    // res_ =new DResultatsSinavi2();
    try {
      res_ = IResultatsSinavi2Helper.narrow(SERVEUR_SINAVI2.resultats(CONNEXION_SINAVI2));
    } catch (final NullPointerException e) {
      System.err.println("Pas de connexion serveur => local");
      affMessage("Vous n'etes pas connect� � un serveur sinavi  \nVous ne pourrez pas lancer de simulation");
    }
    if (CtuluLibArray.isEmpty(res_.parametresResultatGenerationBateau())) {
      error("Sinavi n'a donn� aucun r�sultat.\n Vous pouvez v�rifier les messages du code dans la console (menu Edition)");
      return;
    }
    final BuDialogInput nomSim = new BuDialogInput(getApp(), informationsSoftware(), "Nom de la simulation",
        "Entrez un nom de simulation", "nomSimulation");
    nomSim.activate();
    while (nomSim.getValue().length() == 0) {
      nomSim.activate();
    }
    // /if(nomSim.activate()==0)
    nomSimCourant_ = nomSim.getValue();

    simulationCourante_ = listeSimulationsSinavi2_.size();
    final SSimulationSinavi2 temp = new SSimulationSinavi2();
    final SSimulationSinavi2 temp2 = new SSimulationSinavi2();
    temp.nomSim = nomSimCourant_;
    temp.nomPrj = nomPrj_;
    // copie des donn�es pour ajouter dans la liste de simulation
    temp.parametresBateau = new SParametresBateau[listeBateaux_.size()];
    temp2.parametresBateau = new SParametresBateau[listeBateaux_.size()];
    for (int i = 0; i < listeBateaux_.size(); i++) {

      temp.parametresBateau[i] = (SParametresBateau) listeBateaux_.get(i);
      temp2.parametresBateau[i] = temp.parametresBateau[i];
    }

    temp.parametresBief = new SParametresBief[listeBiefs_.size()];
    temp2.parametresBief = new SParametresBief[listeBiefs_.size()];
    for (int i = 0; i < listeBiefs_.size(); i++) {
      temp.parametresBief[i] = (SParametresBief) listeBiefs_.get(i);
      temp2.parametresBief[i] = temp.parametresBief[i];
    }

    temp.parametresEcluse = new SParametresEcluse[listeEcluses_.size()];
    temp2.parametresEcluse = new SParametresEcluse[listeEcluses_.size()];
    for (int i = 0; i < listeEcluses_.size(); i++) {
      temp.parametresEcluse[i] = (SParametresEcluse) listeEcluses_.get(i);
      temp2.parametresEcluse[i] = temp.parametresEcluse[i];
    }
    temp.parametresCroisement = new SParametresCroisements[listeCroisements_.size()];
    temp2.parametresCroisement = new SParametresCroisements[listeCroisements_.size()];
    for (int i = 0; i < listeCroisements_.size(); i++) {
      temp.parametresCroisement[i] = (SParametresCroisements) listeCroisements_.get(i);
      temp2.parametresCroisement[i] = temp.parametresCroisement[i];
    }
    temp.parametresTrematage = new SParametresTrematages[listeTrematages_.size()];
    temp2.parametresTrematage = new SParametresTrematages[listeTrematages_.size()];
    for (int i = 0; i < listeTrematages_.size(); i++) {
      temp.parametresTrematage[i] = (SParametresTrematages) listeTrematages_.get(i);
      temp2.parametresTrematage[i] = temp.parametresTrematage[i];
    }
    temp.parametresVitesse = new SParametresVitesses[listeVitesses_.size()];
    temp2.parametresVitesse = new SParametresVitesses[listeVitesses_.size()];
    for (int i = 0; i < listeVitesses_.size(); i++) {
      temp.parametresVitesse[i] = (SParametresVitesses) listeVitesses_.get(i);
      temp2.parametresVitesse[i] = temp.parametresVitesse[i];
    }
    temp.parametresTrajet = listeTrajets_.getTrajets();
    temp2.parametresTrajet = listeTrajets_.getTrajets();
    temp.parametresManoeuvre = new SParametresManoeuvres[listeManoeuvres_.size()];
    temp2.parametresManoeuvre = new SParametresManoeuvres[listeManoeuvres_.size()];

    for (int i = 0; i < listeManoeuvres_.size(); i++) {
      temp.parametresManoeuvre[i] = (SParametresManoeuvres) listeManoeuvres_.get(i);
      temp2.parametresManoeuvre[i] = temp.parametresManoeuvre[i];
    }

    temp2.nomSim = "Future Simulation";
    temp2.nomPrj = nomPrj_;
    temp.parametresGeneration = gen_;
    // une simulation est r�alis�e 8 fois, nous ne voulons pas garder la premiere
    // et la derniere qui sont consid�r�es comme d�but et fin du traffic et donc non significatives
    int nbJoursSimulation = gen_.dureeSimulation;

    listeGeneration_ = new ArrayList();
    if (res_.parametresResultatGenerationBateau() != null) {
      final int nb = res_.parametresResultatGenerationBateau().length;
      listeGeneration_.ensureCapacity(nb);
      for (int i = 0; i < nb; i++) {
        if ((res_.parametresResultatGenerationBateau()[i].jour >= nbJoursSimulation && res_
            .parametresResultatGenerationBateau()[i].jour < (nbJoursSimulation * (NB_SIMULATIONS + 1)))) {
          final SResultatGenerationBateau s = res_.parametresResultatGenerationBateau()[i];
          s.jour -= nbJoursSimulation;
          listeGeneration_.add(s);
        }
      }

    }
    final int size = listeGeneration_.size();
    temp.resultatGenerationBateau = new SResultatGenerationBateau[size];
    for (int i = 0; i < size; i++) {
      temp.resultatGenerationBateau[i] = (SResultatGenerationBateau) listeGeneration_.get(i);

    }
    // listeGeneration_ = new ArrayList(); // nc :
    // temp.resultatGenerationBateau=res_.parametresResultatGenerationBateau();
    listeHistorique_ = new ArrayList();
    nbJoursSimulation *= (3600 * 24);
    if (res_.parametresHistorique() != null) {
      for (int i = 0; i < res_.parametresHistorique().length; i++) {
        if (res_.parametresHistorique()[i].heureEntree >= nbJoursSimulation
            && res_.parametresHistorique()[i].heureEntree < (nbJoursSimulation * (NB_SIMULATIONS + 1))) {
          final SResultatHistorique s = res_.parametresHistorique()[i];
          s.heureEntree -= nbJoursSimulation;
          s.heureSortie -= nbJoursSimulation;
          listeHistorique_.add(s);
        }
      }
    }
    temp.resultatHistorique = (SResultatHistorique[]) listeHistorique_.toArray(new SResultatHistorique[listeHistorique_
        .size()]);
    // listeHistorique_ = new ArrayList(); // nc :

    /**
     * ** � d�commenter lorsque la lecture du fichier .cons sera fait listeConsommation_= new ArrayList();
     * if(res_.parametresConsommationDEau()!=null){ for(int i=0;i<res_.parametresConsommationDEau().length;i++) {
     * listeConsommation_.add(res_.parametresConsommationDEau()[i]); } } temp.resultatConsommationDEau=new
     * SResultatConsommationDEau[listeConsommation_.size()]; for(int i=0;i<listeConsommation_.size();i++){
     * temp.resultatConsommationDEau[i]= (SResultatConsommationDEau)listeConsommation_.get(i); }
     */

    System.out.println("sauvegarder la simulation " + listeSimulationsSinavi2_.size());
    System.out.println("taille simulation " + listeSimulationsSinavi2_.size());
    // Il faut supprimer l'ex "Future simulation"
    final int derniereSimu = listeSimulationsSinavi2_.size() - 1;
    listeSimulationsSinavi2_.remove(derniereSimu);
    // et on rajoute les 2 nouvelles
    listeSimulationsSinavi2_.add(temp);
    listeSimulationsSinavi2_.add(temp2);

    affMessage("La future simulation est d�j� selectionn�e. Pour observer vos r�sultats, allez dans gestion simulation pour la consulter.");
    System.out.println("taille simulation apr�s ajout " + listeSimulationsSinavi2_.size());
    /**
     * ***simulationCourante_=listeSimulationsSinavi2_.size(); peut etre garder la simulation en cours pour voir les
     * resultats*
     */
    /*
     * BuDialogInput nomSim=new BuDialogInput(getApp(),informationsSoftware(),"Nom de la simulation","Entrez un nom de
     * simulation","nomSimulation"); nomSim.activate(); while(nomSim.getValue().equalsIgnoreCase("")) nomSim.activate();
     * ///if(nomSim.activate()==0) String nomSimulation=nomSim.getValue(); ArrayList listeGeneration2_;
     * if(listeGeneration_==null) listeGeneration_=new ArrayList(); listeGeneration2_=new ArrayList();
     * listeGeneration2_.add(0,nomSimulation); if(res_.parametresResultatGenerationBateau()!=null){
     * System.out.println("generation bateaux "); for(int i=0;i<res_.parametresResultatGenerationBateau().length;i++) {
     * listeGeneration2_.add(i+1,res_.parametresResultatGenerationBateau()[i]);
     * System.out.println("-->"+res_.parametresResultatGenerationBateau()[i].jour); }
     * listeGeneration_.add(0,listeGeneration2_);//**0 } ArrayList listeHistorique2_; if(listeHistorique_==null)
     * listeHistorique_=new ArrayList(); listeHistorique2_=new ArrayList(); listeHistorique2_.add(0,nomSimulation);
     * if(res_.parametresHistorique()!= null){ for(int i=0;i<res_.parametresHistorique().length ;i++) {
     * System.out.println("historique "); listeHistorique2_.add(i+1,res_.parametresHistorique()[i]);
     * System.out.println("-->"+ "numero bateau " + res_.parametresHistorique()[i].numeroBateau);
     * System.out.println("-->"+ "numero type de bateau " + res_.parametresHistorique()[i].numeroTypeBateau); }
     * listeHistorique_.add(0,listeHistorique2_); } else System.out.println(" rien dans le fichier historique");
     */
  }

  /**
   * Commandes activ�es d�s qu'une simulation est charg�e.
   */
  protected void activerCommandesSimulation(final boolean _val) {

    setEnabledForAction("PARAMETRE", _val);
    setEnabledForAction("TEXTE", _val);
    setEnabledForAction("ENREGISTRER", _val);
    setEnabledForAction("ENREGISTRERSOUS", _val);
    setEnabledForAction("FERMER", _val);
    // setEnabledForAction("QUITTER", _val);//rajout

    setEnabledForAction("CALCULER", _val);
    setEnabledForAction("ENREGISTRER_LISTE", _val);
    setEnabledForAction("VOIR_RESEAU", _val);
    setEnabledForAction("GENERER", _val);
    // temporaire pour les tests de la fenetre...
    setEnabledForAction("GRAPHE", _val);
    setEnabledForAction(getActAddBat(), _val);// seulementpour simulation apr�s
    setEnabledForAction("CONTROLE", _val);
    setEnabledForAction("MODBATEAUX", _val);
    setEnabledForAction("AFFBATEAUX", _val);
    setEnabledForAction(getActSupBateau(), _val);
    setEnabledForAction("ADDBIEFS", _val);
    setEnabledForAction("MODBIEFS", _val);
    setEnabledForAction("AFFBIEFS", _val);
    setEnabledForAction(getActSupBiefs(), _val);
    setEnabledForAction(getActAddEcluse(), _val);
    setEnabledForAction(getActModEcluse(), _val);
    setEnabledForAction("AFFECLUSES", _val);
    setEnabledForAction(getActSupEcluse(), _val);
    setEnabledForAction("CROISEMENTS", _val);
    setEnabledForAction("TREMATAGES", _val);
    setEnabledForAction("VITESSES", _val);
    setEnabledForAction("PALETTE", _val);
    setEnabledForAction("MANOEUVRES", _val);
    setEnabledForAction(getActAssembler(), _val);
    setEnabledForAction("VISUALISER", _val);
    setEnabledForAction("LANCER", _val);
    setEnabledForAction("ECLUSES", _val);
    setEnabledForAction("BATEAUX", _val);
    setEnabledForAction("BIEFS", _val);
    setEnabledForAction("CIRCULATION", _val);
    setEnabledForAction("TRAJETS", _val);
    setEnabledForAction("IMPBATEAU", _val);
    setEnabledForAction(getActImpNav(), _val);
    setEnabledForAction("IMPORTERNAV2", _val);
    setEnabledForAction(getActImpBief(), _val);
    setEnabledForAction("IMPECLUSE", _val);
    setEnabledForAction(getActImportOuv(), _val);
    setEnabledForAction("IMPORTEROUV2", _val);
    setEnabledForAction("IMPORTERRESEAU", _val);
    setEnabledForAction("EXPBATEAU", _val);
    setEnabledForAction("EXPORTERNAV", _val);
    setEnabledForAction("EXPORTERNAV2", _val);
    setEnabledForAction("EXPORTERBIE", _val);
    setEnabledForAction("EXPECLUSE", _val);
    setEnabledForAction("EXPORTEROUV", _val);
    setEnabledForAction("EXPORTEROUV2", _val);
    setEnabledForAction("EXPORTERRESEAU", _val);
    setEnabledForAction("GENERATION", _val);
    setEnabledForAction("HISTORIQUE", _val);
    setEnabledForAction("RESDUREEPARCOURS", _val);
    setEnabledForAction("RESATTENTESBATEAUX", _val);
    setEnabledForAction("RESATTENTESELEMENTS", _val);
    setEnabledForAction("RESATTENTES", _val);
    setEnabledForAction("RESCONSODEAU", _val);
    setEnabledForAction("COMPARAISONSSIMULATIONS", _val);
    setEnabledForAction("RESGESTIONSIMULATIONS", _val);
    setEnabledForAction("SEUL", _val);
    setEnabledForAction("RESULTATS", _val);
    setEnabledForAction("INDISPONIBILITES", _val);

    /**
     * A decommentariser pour l'ajouter setEnabledForAction("RESCONSO",_val);
     * setEnabledForAction("TABLEAURECAPITULATIF",_val);
     */
    setEnabledForAction("TABLEAURES", _val);

  }

  private String getActImportOuv() {
    return "IMPORTEROUV";
  }

  private String getActImpBief() {
    return "IMPORTERBIE";
  }

  private String getActImpNav() {
    return "IMPORTERNAV";
  }

  private String getActAssembler() {
    return "ASSEMBLER";
  }

  private String getActSupEcluse() {
    return "SUPECLUSES";
  }

  private String getActModEcluse() {
    return "MODECLUSES";
  }

  private String getActAddEcluse() {
    return "ADDECLUSES";
  }

  private String getActSupBiefs() {
    return "SUPBIEFS";
  }

  private String getActSupBateau() {
    return "SUPBATEAUX";
  }

  private String getActAddBat() {
    return "ADDBATEAUX";
  }

  protected void addModIndisponibilites(final String _element, final int _loi) {
    /*******************************************************************************************************************
     * if(listeTrajets_ == null ||listeTrajets_.equals(null) || listeTrajets_.size()==0 ){ test("Il n'y a aucun trajet
     * dans la base"); }
     ******************************************************************************************************************/
    // else{
    try {
      if (sinavi2filleaddmodindisponibilites_ == null) {

        sinavi2filleaddmodindisponibilites_ = new Sinavi2FilleAddModIndisponibilites(this, _element, _loi);
        sinavi2filleaddmodindisponibilites_.setVisible(true);
        addInternalFrame(sinavi2filleaddmodindisponibilites_);
      } else {
        if (sinavi2filleaddmodindisponibilites_.isClosed()) {
          // sinavi2filleaddmodindisponibilites_.miseajour(listeBiefs_);
          addInternalFrame(sinavi2filleaddmodindisponibilites_);
        } else {
          // sinavi2filleaddmodindisponibilites_.miseajour(listeBiefs_);
          activateInternalFrame(sinavi2filleaddmodindisponibilites_);
        }
      }

    } catch (final NullPointerException e) {

      JOptionPane.showMessageDialog(null, "exception capt�e +" + e.getMessage() + CtuluLibString.LINE_SEP
          + e.getCause() + CtuluLibString.LINE_SEP + e.getClass() + CtuluLibString.LINE_SEP + e.getLocalizedMessage()
          + CtuluLibString.LINE_SEP);
    }
  }

  protected void afficherIndisponibilites(final int _elem) {
    try {
      if (sinavi2filleaffindisponibilites_ == null) {
        sinavi2filleaffindisponibilites_ = new Sinavi2FilleAffIndisponibilites(this, _elem);
        FuLog.debug("crearion");
        sinavi2filleaffindisponibilites_.setVisible(true);
        addInternalFrame(sinavi2filleaffindisponibilites_);
      } else {
        if (sinavi2filleaffindisponibilites_.isClosed()) {
          // sinavi2filleaffindisponibilites_.miseajour(listeBiefs_);
          addInternalFrame(sinavi2filleaffindisponibilites_);
        } else {
          // sinavi2filleaffindisponibilites_.miseajour(listeBiefs_);
          activateInternalFrame(sinavi2filleaffindisponibilites_);
        }
      }
    } catch (final NullPointerException e) {
      JOptionPane.showMessageDialog(null, "exception capt�e +" + e.getMessage() + CtuluLibString.LINE_SEP
          + e.getCause() + CtuluLibString.LINE_SEP + e.getClass() + CtuluLibString.LINE_SEP + e.getLocalizedMessage()
          + CtuluLibString.LINE_SEP);
    }
  }

  protected void afficherTrajets() {
    if (sinavi2filleafftrajets_ == null) {
      if (!Sinavi2FilleAddModTrajets.isParamsOk(this)) {
        return;
      }
      if (listeTrajets_ == null) {
        listeTrajets_ = new Sinavi2TrajetMng();
      }
      sinavi2filleafftrajets_ = new Sinavi2FilleAffTrajets(this, listeTrajets_);
      addInternalFrame(sinavi2filleafftrajets_);
    } else {
      if (sinavi2filleafftrajets_.isClosed()) {
        addInternalFrame(sinavi2filleafftrajets_);
      } else {
        activateInternalFrame(sinavi2filleafftrajets_);
      }
    }

  }

  /**
   * ajouter un bateaux.
   */
  protected void ajouterBateaux(final int bat, final boolean _modif) {

    if (listeBateaux_ == null) {
      // ---------------- listeBateaux_=new LinkedList();
      listeBateaux_ = new ArrayList();
    }

    try {
      if (sinavi2filleaddmodbateaux_ == null) {
        sinavi2filleaddmodbateaux_ = new Sinavi2FilleAddModBateaux(this, listeBateaux_, _modif);
        // sinavi2filleaddmodbateaux_.setnbat(-1);
        sinavi2filleaddmodbateaux_.setnBat(bat);
        sinavi2filleaddmodbateaux_.initialiseChamps(bat);
        sinavi2filleaddmodbateaux_.setVisible(true);
        addInternalFrame(sinavi2filleaddmodbateaux_);

      } else {
        if (sinavi2filleaddmodbateaux_.isClosed()) {
          sinavi2filleaddmodbateaux_.setnBat(bat);
          sinavi2filleaddmodbateaux_.initialiseChamps(bat);
          addInternalFrame(sinavi2filleaddmodbateaux_);
        } else {
          sinavi2filleaddmodbateaux_.setnBat(bat);
          sinavi2filleaddmodbateaux_.initialiseChamps(bat);
          activateInternalFrame(sinavi2filleaddmodbateaux_);

        }
      }
    } catch (final Exception e) {
      System.out.println(e.getMessage());
      JOptionPane.showMessageDialog(null, "exception");
    }

    /*
     * if(listeBateaux_ == null){ //---------------- listeBateaux_=new LinkedList(); listeBateaux_=new ArrayList(); }
     * try{ if(sinavi2filleaddmodbateaux_ == null){ sinavi2filleaddmodbateaux_=new
     * Sinavi2FilleAddModBateaux(this,listeBateaux_); sinavi2filleaddmodbateaux_.setVisible(true);
     * addInternalFrame(sinavi2filleaddmodbateaux_); } else{ if(sinavi2filleaddmodbateaux_.isClosed()){
     * addInternalFrame(sinavi2filleaddmodbateaux_); } else activateInternalFrame(sinavi2filleaddmodbateaux_); } }
     * catch(Exception e) { System.out.println(e.getMessage()); JOptionPane.showMessageDialog(null,"exception"); }
     */
  }

  protected void ajouterBief(final int bief, final boolean _modif) {

    if (listeBiefs_ == null) {
      // ---------------- listeBateaux_=new LinkedList();
      listeBiefs_ = new ArrayList();
    }

    try {
      if (sinavi2filleaddmodbiefs_ == null) {
        sinavi2filleaddmodbiefs_ = new Sinavi2FilleAddModBiefs(this, listeBiefs_, _modif);
        sinavi2filleaddmodbiefs_.setNBief(bief);
        sinavi2filleaddmodbiefs_.initialiseChamps(bief);
        sinavi2filleaddmodbiefs_.setVisible(true);
        addInternalFrame(sinavi2filleaddmodbiefs_);

      } else {
        if (sinavi2filleaddmodbiefs_.isClosed()) {
          sinavi2filleaddmodbiefs_.setNBief(bief);
          sinavi2filleaddmodbiefs_.initialiseChamps(bief);
          addInternalFrame(sinavi2filleaddmodbiefs_);
        } else {
          sinavi2filleaddmodbiefs_.setNBief(bief);
          sinavi2filleaddmodbiefs_.initialiseChamps(bief);
          activateInternalFrame(sinavi2filleaddmodbiefs_);

        }
      }
    } catch (final Exception e) {
      System.out.println(e.getMessage());
      JOptionPane.showMessageDialog(null, "exception ");
    }

  }

  protected void ajouterEcluse(final int ecluse, final boolean _modif) {

    if (listeEcluses_ == null) {
      // ---------------- listeBateaux_=new LinkedList();
      listeEcluses_ = new ArrayList();
    }

    try {
      if (sinavi2filleaddmodecluses_ == null) {

        sinavi2filleaddmodecluses_ = new Sinavi2FilleAddModEcluses(this, listeEcluses_, _modif);
        sinavi2filleaddmodecluses_.setnEcluse(ecluse);
        sinavi2filleaddmodecluses_.initialiseChamps(ecluse);
        sinavi2filleaddmodecluses_.setVisible(true);
        addInternalFrame(sinavi2filleaddmodecluses_);

      } else {
        if (sinavi2filleaddmodecluses_.isClosed()) {
          sinavi2filleaddmodecluses_.setnEcluse(ecluse);
          sinavi2filleaddmodecluses_.initialiseChamps(ecluse);
          addInternalFrame(sinavi2filleaddmodecluses_);
        } else {
          sinavi2filleaddmodecluses_.setnEcluse(ecluse);
          sinavi2filleaddmodecluses_.initialiseChamps(ecluse);
          activateInternalFrame(sinavi2filleaddmodecluses_);

        }
      }
    } catch (final Exception e) {
      System.out.println(e.getMessage());
      JOptionPane.showMessageDialog(null, " exception");
    }

  }

  /**
   * Cr�ation ou affichage de la fenetre pour les pr�f�rences.
   */
  protected void buildPreferences(final List _prefs) {
    _prefs.add(new BuBrowserPreferencesPanel(this));
    _prefs.add(new BuDesktopPreferencesPanel(this));
    _prefs.add(new BuLanguagePreferencesPanel(this));
  }

  protected void calculer() {
    // Si les donn�es sont acceptables
    // if (controle()) {
    remplirParametres();
    oprCalculer();
    // }
  }

  protected void clearVariables() {
    CONNEXION_SINAVI2 = null;
    SERVEUR_SINAVI2 = null;
  }

  protected void consoDEau() {
    if (listeEcluses_ != null) {
      final int j = listeEcluses_.size();
      final String[] s = new String[j];
      final ListIterator iter = listeEcluses_.listIterator();
      int i = 0;
      while (iter.hasNext()) {
        SParametresEcluse c = new SParametresEcluse();
        c = (SParametresEcluse) iter.next();
        s[i] = c.identification;
        i++;
      }
      // test();
      final BuDialogChoice b = new BuDialogChoice(getApp(), InfoSoftSinavi2_, "Consommation d'eau de l'�cluse",
          "Choisissez :", s);
      // test();
      b.activate();
      final String eclusecourant = b.getValue();
      final int x = rechercherEcluse(eclusecourant);
      final SParametresEcluse e = ((SParametresEcluse) listeEcluses_.get(x));
      final double res = e.largeur * e.longueur * e.hauteurChuteDEau;
      affMessage("L'�cluse " + eclusecourant + " utilise  " + res + " m3 pour chaque bassin�e ou fausse bassin�e");
    } else {
      final BuDialogMessage dialog_mess = new BuDialogMessage(getApp(), InfoSoftSinavi2_,
          "Il n'y a aucune �cluse dans votre projet !");
      dialog_mess.activate();
    }

  }

  /* Menu constituion */
  protected BuMenu construitMenuConstitution() {
    final BuMenu menuConstitution_ = new BuMenu("Constitution", "CONSTITUTION");
    menuConstitution_.addMenuItem("Assembler", getActAssembler(), false);
    menuConstitution_.addMenuItem("Palette", "PALETTE", false);
    menuConstitution_.addMenuItem("Visualiser", "VISUALISER", false);
    return menuConstitution_;
  }

  /* donn�es du mod�le */
  protected BuMenu construitMenuDonnees() {

    final BuMenu menuDonnees_ = new BuMenu("Donn�es du mod�le", "DONNEES");

    final BuMenu bateaux_ = new BuMenu("Bateaux", "BATEAUX");
    bateaux_.addMenuItem("Ajouter", getActAddBat(), false)/* .setActionCommand("ADDBATEAUX") */;
    // bateaux_.addMenuItem("Ajouter","ADDBATEAUX");
    bateaux_.addMenuItem("Modifier", "MODBATEAUX", false);
    bateaux_.addMenuItem("Supprimer", getActSupBateau(), false);
    bateaux_.addMenuItem("Afficher", "AFFBATEAUX", false);

    final BuMenu biefs_ = new BuMenu("Biefs", "BIEFS");
    biefs_.addMenuItem("Ajouter", "ADDBIEFS", false);
    biefs_.addMenuItem("Modifier", "MODBIEFS", false);
    biefs_.addMenuItem("Supprimer", getActSupBiefs(), false);
    biefs_.addMenuItem("Afficher", "AFFBIEFS", false);

    final BuMenu ecluses_ = new BuMenu("Ecluses", "ECLUSES");
    ecluses_.addMenuItem("Ajouter", getActAddEcluse(), false);
    ecluses_.addMenuItem("Modifier", getActModEcluse(), false);
    ecluses_.addMenuItem("Supprimer", getActSupEcluse(), false);
    ecluses_.addMenuItem("Afficher", "AFFECLUSES", false);

    menuDonnees_.addSubMenu(bateaux_, false);
    menuDonnees_.addSubMenu(biefs_, false);
    menuDonnees_.addSubMenu(ecluses_, false);
    menuDonnees_.addMenuItem("Controle", "CONTROLE", false);
    return menuDonnees_;
  }

  // private JMenuItem assembler_,palette_,visualisation_ ;
  protected BuMenu construitMenuFonctionnement() {

    final BuMenu menuFonctionnement_ = new BuMenu("Fonctionnement", "FONCTIONNEMENT");
    final BuMenu menuCirculation_ = new BuMenu("Circulation", "CIRCULATION", false);

    menuFonctionnement_.addMenuItem("Vitesses", "VITESSES", false);
    menuFonctionnement_.addMenuItem("Trajets", "TRAJETS", false);
    menuCirculation_.addMenuItem("Croisements", "CROISEMENTS", false);
    menuCirculation_.addMenuItem("Trematages", "TREMATAGES", false);
    menuFonctionnement_.addSubMenu(menuCirculation_, false);
    menuFonctionnement_.addMenuItem("Manoeuvres", "MANOEUVRES", false);
    menuFonctionnement_.addMenuItem("Indisponibilit�s", "INDISPONIBILITES", false);
    return menuFonctionnement_;
  }

  protected BuMenu construitMenuResultats() {

    final BuMenu menuResultats_ = new BuMenu("R�sultats", "RESULTATS");

    final BuMenu menuSeul_ = new BuMenu("Simulation Courante", "SEUL");
    menuSeul_.addMenuItem("Generation des Bateaux", "GENERATION", false);
    menuSeul_.addMenuItem("Historique", "HISTORIQUE", false);
    menuSeul_.addMenuItem("Dur�e de Parcours", "RESDUREEPARCOURS", false);
    // menuSeul_.addMenuItem("Consommation d'eau","RESCONSODEAU",false);
    final BuMenu menuConsommation_ = new BuMenu("Consommation d'eau", "RESCONSODEAU");
    menuConsommation_.addMenuItem("Tableau r�capitulatif", "TABLEAURECAPITULATIF", false);
    menuConsommation_.addMenuItem("Consommation", "RESCONSO", false);
    menuSeul_.addSubMenu(menuConsommation_, true);
    final BuMenu menuAttentes_ = new BuMenu("Attentes", "RESATTENTES");
    menuAttentes_.addMenuItem("Par Bateaux", "RESATTENTESBATEAUX", false);
    menuAttentes_.addMenuItem("Par Elements", "RESATTENTESELEMENTS", false);
    menuAttentes_.addMenuItem("Tableau R�sultat", "TABLEAURES", false);
    menuSeul_.addSubMenu(menuAttentes_, false);

    menuResultats_.addSubMenu(menuSeul_, false);
    menuResultats_.addMenuItem("Comparaisons des Simulations", "COMPARAISONSSIMULATIONS", false);
    menuResultats_.addMenuItem("Gestion Simulations", "RESGESTIONSIMULATIONS", false);
    return menuResultats_;

  }

  protected BuMenu construitMenuSimulation() {

    final BuMenu menuSimulation_ = new BuMenu("Simulation", "SIMULATION");
    // menuSimulation_.addMenuItem("Generation des bateaux","GENERER",false);
    menuSimulation_.addMenuItem("Lancer", "LANCER", false);
    return menuSimulation_;
  }

  protected void controler() {
    // genererFichiers();

    sinavi2filleaffcontroles_ = new Sinavi2FilleAffControles(this, listeBateaux_, listeBiefs_, listeEcluses_,
        listeCroisements_, listeTrematages_);

    // /**---------------------
    /*
     * File _nav=new File("test3.nav"); param_.parametresBateau((SParametresBateau[]) listeBateaux_.toArray(new
     * SParametresBateau[listeBateaux_.size()]));
     * DParametresSinavi2.ecritParametresBateaux(param_.parametresBateau(),_nav,null,an_);
     */

    // ------------------------------- */
    // DParametresSinaviBen.ecritParametres(param_,null,"test2",null,an_);
    /*
     * String nomfic="etude.nav"; JOptionPane.showMessageDialog(null,"tentative Creation fichier"); //DataOutputStream
     * fs= new DataOutputStream (new FileOutputStream (nomfic) ); PrintWriter ecr; try { if(listeBateaux_ != null){ ecr =
     * new PrintWriter( new FileWriter (nomfic)); ListIterator iter= listeBateaux_.listIterator(); while
     * (iter.hasNext()){ JOptionPane.showMessageDialog(null,"�criture d'un bateau"); Sinavi2TypeBateau c= new
     * Sinavi2TypeBateau(); c= (Sinavi2TypeBateau) iter.next(); ecr.println((String)c.getId());
     * ecr.println(CtuluLibString.ESPACE + c.getLongueur().getLongueurField()); ecr.println(CtuluLibString.ESPACE +
     * c.getLargeur().getLongueurField()); ecr.println(CtuluLibString.ESPACE + c.getTirant_deau().getLongueurField());
     * ecr.println(CtuluLibString.ESPACE + (float)c.getGene().getDureeField()/60); ecr.println(CtuluLibString.ESPACE +
     * (float)c.getH_deb_nav().getDureeField()/60); ecr.println(CtuluLibString.ESPACE +
     * (float)c.getH_fin_nav().getDureeField()/60); } ecr.println(" | Ligne de fin de fichier"); ecr.close(); } } catch
     * (IOException e) { // TODO Auto-generated catch block JOptionPane.showMessageDialog(null,"erreur");
     * e.printStackTrace(); } JOptionPane.showMessageDialog(null,"fichier CREER");
     */

  }

  /**
   * cr�er une nouvelle simulation.
   */

  protected void creer() {

    assistant_.changeAttitude(BuAssistant.PAROLE, "Cr�ation d'une nouvelle\n �tude");
    // projet_.creer();
    if (projet_ != null) {
      fermer();
    }
    projet_ = new FudaaProjet(getApp(), new FudaaFiltreFichier("nom"));
    projet_.creer();
    main_menubar_.getMenu("EXPORTER").remove(4);// getMenuComponent(4).setEnabled(false);
    main_menubar_.getMenu("IMPORTER").remove(4);// getMenuComponent(4).setEnabled(false);
    if (!projet_.getFichier().equalsIgnoreCase("null.nom")) {
      setPermettreModif(true);
      // param_ = new DParametresSinavi2();
      listeSimulationsSinavi2_ = new ArrayList();

      // setNomPrj(projet_.getFichier().substring(0,projet_.getFichier().length()-4));
      activerCommandesSimulation(true);
      // setTitle(projet_.getFichier());
      setNomPrj(projet_.getFichier());
      final SSimulationSinavi2 s = new SSimulationSinavi2();
      s.nomSim = "Future Simulation";
      System.out.println("nom projet " + getNomPrj());
      s.nomPrj = getNomPrj();
      listeSimulationsSinavi2_.add(s);
    } else {
      fermer();
    }

  }

  /**
   * enregistrer la simulation ouverte.
   */
  protected void enregistrer() {

    // remplirParametres();
    setSimulationCourante(simulationCourante_);

    if (listeSimulationsSinavi2_ != null) {
      for (int i = 0; i < listeSimulationsSinavi2_.size(); i++) {
        ((SSimulationSinavi2) listeSimulationsSinavi2_.get(i)).nomPrj = projet_.getFichier();
      }
    }

    projet_.addParam("par", listeSimulationsSinavi2_);
    projet_.enregistre();
    boolean found = false;
    final String r = projet_.getFichier();
    for (int i = 1; i < RECENT_COUNT; i++) {
      if (r.equals(Sinavi2Preferences.SINAVI2.getStringProperty("file.recent." + i + ".path"))) {
        found = true;
      }
    }
    if (!found) {
      getMainMenuBar().addRecentFile(r, "sinavi2");
      Sinavi2Preferences.SINAVI2.writeIniFile();
    }
    setEnabledForAction("REOUVRIR", true);
    /*
     * assistant_.changeAttitude(BuAssistant.PAROLE, "Enregistrement de l'�tude\nen cours...");
     * glassStop_.setVisible(true); sinavi2gestionprojet_.enregistre();
     */
    System.out.println("nom projet enreg " + ((SSimulationSinavi2) listeSimulationsSinavi2_.get(0)).nomPrj);
  }

  /**
   * enregistrer une simulation.
   */
  protected void enregistrerSous() {
    // remplirParametres();
    // projet_.addParam("par",param_);
    if (listeSimulationsSinavi2_ != null) {
      FuLog.debug("test sim" + projet_.getFichier());
      for (int i = 0; i < listeSimulationsSinavi2_.size(); i++) {
        ((SSimulationSinavi2) listeSimulationsSinavi2_.get(i)).nomPrj = projet_.getFichier();
      }
    }
    projet_.addParam("par", listeSimulationsSinavi2_);
    /*******************************************************************************************************************
     * if(listeGares_==null) listeGares_=new ArrayList(); projet_.addParam("gar",listeGares_);
     * if(listeHistorique_==null) listeHistorique_=new ArrayList(); projet_.addParam("his",listeHistorique_);
     * if(listeGeneration_==null) listeGeneration_=new ArrayList(); projet_.addParam("gen",listeGeneration_);
     ******************************************************************************************************************/

    projet_.enregistreSous();
    boolean found = false;
    final String r = projet_.getFichier();
    for (int i = 1; i < RECENT_COUNT; i++) {
      if (r.equals(Sinavi2Preferences.SINAVI2.getStringProperty("file.recent." + i + ".path"))) {
        found = true;
      }
    }
    if (!found) {
      getMainMenuBar().addRecentFile(r, "sinavi2");
      Sinavi2Preferences.SINAVI2.writeIniFile();
    }
    setEnabledForAction("REOUVRIR", true);
    if (!projet_.getFichier().equalsIgnoreCase(getNomPrj())) {
      setNomPrj(projet_.getFichier());
      FuLog.debug("projet " + projet_.getFichier());
      // projet_.addParam("par",listeSimulationsSinavi2_);
      if (listeSimulationsSinavi2_ != null) {
        FuLog.debug("test sim" + projet_.getFichier());
        for (int i = 0; i < listeSimulationsSinavi2_.size(); i++) {
          ((SSimulationSinavi2) listeSimulationsSinavi2_.get(i)).nomPrj = projet_.getFichier();
        }
      }
      projet_.enregistre();
    }

    /*
     * assistant_.changeAttitude(BuAssistant.PAROLE, "Enregistrement de l'�tude\nsous un nouveau nom...");
     * glassStop_.setVisible(true); sinavi2gestionprojet_.enregistreSous();
     */
  }

  /*
   * private int rechercher_bief(String biefcourant) { ListIterator iter= listeBiefs_.listIterator(); int i=0; while
   * (iter.hasNext()){ SParametresBief c=new SParametresBief(); c=(SParametresBief) iter.next(); if( c.identification ==
   * biefcourant){ System.out.println("rechercher"+i); return i; } else i++; /* Sinavi2TypeBateau c= new
   * Sinavi2TypeBateau(); c= (Sinavi2TypeBateau) iter.next(); if( (String)c.getId()==batcourant) return i; else i++;
   */
  /*
   * } return -1; }
   */

  /**
   * fermer une simulation.
   */
  protected void fermer() {

    if (projet_ != null) {
      if (!projet_.estVierge()) {
        final BuDialogConfirmation conf_ = new BuDialogConfirmation(getApp(), InfoSoftSinavi2_,
            "Voulez-vous sauvegarder les modifications ?");
        if (conf_.activate() == 0) {
          this.enregistrer();
          affMessage("Les donn�es sont sauvegard�es.");
        } else {
          affMessage("Les derni�res modifications ne sont pas sauvegard�es.");
        }
      }
      projet_.fermer();

      /*
       * main_menubar_.getMenu("EXPORTER").remove(4);//getMenuComponent(4).setEnabled(false);
       * main_menubar_.getMenu("IMPORTER").remove(4);
       */
      listeBateaux_ = null;
      listeBiefs_ = null;
      listeEcluses_ = null;
      listeCroisements_ = null;
      listeTrematages_ = null;
      listeVitesses_ = null;
      listeManoeuvres_ = null;
      listeGares_ = null;
      listeTrajets_ = null;
      listeGeneration_ = null;
      listeHistorique_ = null;
      sinavi2filleaddmodbateaux_ = null;
      sinavi2filleaffbateaux_ = null;
      sinavi2filleaddmodbiefs_ = null;
      sinavi2filleaffbiefs_ = null;
      sinavi2filleaddmodecluses_ = null;
      sinavi2filleaffecluses_ = null;
      sinavi2filleaffcroisements_ = null;
      sinavi2filleafftrematages_ = null;
      sinavi2filleaffvitesses_ = null;
      sinavi2fillemodvitesses_ = null;
      sinavi2filleaffmanoeuvres_ = null;
      sinavi2fillemodmanoeuvres_ = null;
      sinavi2filleaffcontroles_ = null;
      sinavi2filleaddmodconnexions_ = null;
      sinavi2filleaffconnexions_ = null;
      sinavi2filleafftrajets_ = null;
      sinavi2filletirage_ = null;
      sinavi2filleaffgeneration_ = null;
      sinavi2filleaffhistorique_ = null;
      listeSimulationsSinavi2_ = new ArrayList();
      /*
       * setEnabledForAction("PARAMETRE", false); setEnabledForAction("TEXTE", false);
       * setEnabledForAction("ENREGISTRER", false); setEnabledForAction("ENREGISTRERSOUS", false);
       * setEnabledForAction("FERMER", false); setEnabledForAction("QUITTER", true);//rajout
       * setEnabledForAction("CALCULER", false); setEnabledForAction("ENREGISTRER_LISTE", false);
       * setEnabledForAction("VOIR_RESEAU", false); setEnabledForAction("GENERER", false); // temporaire pour les tests
       * de la fenetre... setEnabledForAction("GRAPHE", false); setEnabledForAction("ADDBATEAUX", false);//seulementpour
       * simulation apr�s setEnabledForAction("CONTROLE", false); setEnabledForAction("MODBATEAUX", false);
       * setEnabledForAction("AFFBATEAUX", false); setEnabledForAction("SUPBATEAUX",false);
       * setEnabledForAction("ADDBIEFS", false); setEnabledForAction("MODBIEFS", false); setEnabledForAction("AFFBIEFS",
       * false); setEnabledForAction("SUPBIEFS",false); setEnabledForAction("ADDECLUSES", false);
       * setEnabledForAction("MODECLUSES", false); setEnabledForAction("AFFECLUSES", false);
       * setEnabledForAction("SUPECLUSES",false); setEnabledForAction("CROISEMENTS", false);
       * setEnabledForAction("TREMATAGES",false); setEnabledForAction("VITESSES", false); setEnabledForAction("PALETTE",
       * false); setEnabledForAction("MANOEUVRES",false); setEnabledForAction("ASSEMBLER",false);
       * setEnabledForAction("VISUALISER",false); setEnabledForAction("LANCER",false);
       * setEnabledForAction("ECLUSES",false); setEnabledForAction("BATEAUX",false); setEnabledForAction("BIEFS",false);
       * setEnabledForAction("CIRCULATION",false); setEnabledForAction("TRAJETS",false);
       */
      activerCommandesSimulation(false);
      setNomPrj("Sinavi2");
      projet_ = null;
    }
  }

  protected FudaaDodicoTacheConnexion[] getTacheConnexionMap() {
    final FudaaDodicoTacheConnexion c = new FudaaDodicoTacheConnexion(SERVEUR_SINAVI2, CONNEXION_SINAVI2);
    return new FudaaDodicoTacheConnexion[] { c };
  }

  protected Class[] getTacheDelegateClass() {
    return new Class[] { DCalculSinavi2.class };
  }

  /**
   * importer un projet.
   */
  protected void importerProjet() {

  }

  protected void initConnexions(final Map _r) {
    final FudaaDodicoTacheConnexion c = (FudaaDodicoTacheConnexion) _r.get(DCalculSinavi2.class);
    CONNEXION_SINAVI2 = c.getConnexion();
    SERVEUR_SINAVI2 = ICalculSinavi2Helper.narrow(c.getTache());

  }

  protected void mAJGares() {

    listeGares_ = new ArrayList();
    ListIterator it;

    it = listeBiefs_.listIterator();
    while (it.hasNext()) {

      final SParametresBief b = (SParametresBief) it.next();
      if (!listeGares_.contains("" + b.gareEnAmont)) {

        listeGares_.add("" + b.gareEnAmont);

      }
      if (!listeGares_.contains("" + b.gareEnAval)) {

        listeGares_.add("" + b.gareEnAval);
      }
    }

    it = listeEcluses_.listIterator();
    while (it.hasNext()) {

      final SParametresEcluse b = (SParametresEcluse) it.next();
      if (!listeGares_.contains("" + b.gareEnAmont)) {

        listeGares_.add("" + b.gareEnAmont);
      }
      if (!listeGares_.contains("" + b.gareEnAval)) {

        listeGares_.add("" + b.gareEnAval);
      }
    }

  }

  /**
   * ouvrir une simulation existante.
   */
  protected void majSimulationCourante(final int _indice) {

    final int indice = _indice;
    paramCourant_ = (SSimulationSinavi2) listeSimulationsSinavi2_.get(indice);
    remplirListes(paramCourant_);
    /**
     * A d�commentariser lorsque la consommation sera ajout�e if(paramCourant_.resultatConsommationDEau!=null) for(int
     * i=0;i<paramCourant_.resultatConsommationDEau.length;i++)
     * listeConsommation_.add(paramCourant_.resultatConsommationDEau[i]);
     */
    gen_ = paramCourant_.parametresGeneration;
    nomSimCourant_ = paramCourant_.nomSim;
    commentaireCourant_ = paramCourant_.commentaire;
    setNomPrj(paramCourant_.nomPrj);
    if (_indice == listeSimulationsSinavi2_.size() - 1) {
      setPermettreModif(true);
    }

  }

  protected void menuPalette(final SParametresTrajets _trajet) {
    mAJGares();
    final Sinavi2Reseau s = new Sinavi2Reseau(this, listeBateaux_, listeBiefs_, listeEcluses_, listeGares_, _trajet);
    s.setVisible(true);
    this.addInternalFrame(s);

  }

  protected void ouvrir() {

    assistant_.changeAttitude(BuAssistant.PAROLE, "Ouverture d'une �tude\nexistante");
    // projet_=new FudaaProjet(getApp(),new FudaaFiltreFichier("nom"), System.getProperty("user.dir"));

    // test(essai);
    // if(projet_==null){
    projet_ = new FudaaProjet(getApp(), new FudaaFiltreFichier("nom"));
    // FuLog.debug("test");
    // fermer();
    // }

    projet_.ouvrir();

    main_menubar_.getMenu("EXPORTER").remove(4);// getMenuComponent(4).setEnabled(false);
    main_menubar_.getMenu("IMPORTER").remove(4);// getMenuComponent(4).setEnabled(false);

    if (!projet_.getFichier().equalsIgnoreCase("null.nom")) {
      setPermettreModif(true);
      /*
       * param_ = new DParametresSinavi2(); param_=(IParametresSinavi2) projet_.getParam("par");
       */
      listeSimulationsSinavi2_ = new ArrayList();
      listeSimulationsSinavi2_ = (ArrayList) projet_.getParam("par");
      majSimulationCourante(listeSimulationsSinavi2_.size() - 1);// nc : on charge la "future simulation" par defaut ?
      simulationCourante_ = listeSimulationsSinavi2_.size() - 1;
      mAJGares();
      // setNomPrj(((SSimulationSinavi2)listeSimulationsSinavi2_.get(0)).nomPrj);
      // Au cas ou l'utilisateur aurait r�alis� une modif du style copier coller
      setNomPrj(projet_.getFichier());
      for (int i = 0; i < listeSimulationsSinavi2_.size(); i++) {
        ((SSimulationSinavi2) listeSimulationsSinavi2_.get(i)).nomPrj = getNomPrj();
      }
      /**
       * *TODO mettres les listes � jour au dernier indice mettre param ajour a moins de le faire avant le lancement
       * regarde la mise a jour des gares et enlever � terme le parametre gare ki sert pas a gran chose on pourra
       * �galement enlever listehistorique et parametres gen
       */

      /**
       * listeGares_=new ArrayList(); listeGares_ =(ArrayList) projet_.getParam("gar"); listeHistorique_=new
       * ArrayList(); //listeHistorique2_=new ArrayList(); listeHistorique_ =(ArrayList) projet_.getParam("his");
       */
      // simulationCourante_=listeHistorique_.size()-1;
      /*
       * listeGeneration_=new ArrayList(); listeGeneration_=(ArrayList) projet_.getParam("gen");
       */

      // listeHistorique_.add(0,listeHistorique2_);
      /*****************************************************************************************************************
       * remplirListes(); remplirListes2();
       ****************************************************************************************************************/
      // test(""+projet_.getFichier());
      // test(projet_.getInformations()+"");
      activerCommandesSimulation(true);
      System.out.println("nom projet ouverture " + ((SSimulationSinavi2) listeSimulationsSinavi2_.get(0)).nomPrj);
    }
    /*
     * sinavi2gestionprojet_ = new Sinavi2GestionProjet(this); glassStop_.setVisible(true);
     * sinavi2gestionprojet_.ouvrir();
     */

    // CtuluLibDialog ben=new CtuluLibDialog();
    // CtuluLibDialog.showWarn(getApp(),"test","que veux tu ouvrir ?");
    // JOptionPane.showMessageDialog(null,"Charmout");
    /*
     * try{ Sinavi2FilleAddModBateaux b=new Sinavi2FilleAddModBateaux(this); b.setVisible(true); addInternalFrame(b); }
     * catch(Exception e) { c System.out.println(e.getMessage()); JOptionPane.showMessageDialog(null," de d'exception a
     * la con"); }
     */
    // JOptionPane.showMessageDialog(null,"Charmout");
    /*
     * activateInternalFrame(b); JInternalFrame currentFrame = getCurrentInternalFrame();
     * addInternalFrame(currentFrame);
     */

  }

  protected void ouvrir(final String _arg) {

    assistant_.changeAttitude(BuAssistant.PAROLE, "Ouverture d'une �tude\nexistante");
    // projet_=new FudaaProjet(getApp(),new FudaaFiltreFichier("nom"), System.getProperty("user.dir"));

    // test(essai);
    // if(projet_==null){
    projet_ = new FudaaProjet(getApp(), new FudaaFiltreFichier("nom"));
    // FuLog.debug("test");
    // fermer();
    // }

    projet_.ouvrir(_arg);
    main_menubar_.getMenu("EXPORTER").remove(4);// getMenuComponent(4).setEnabled(false);
    main_menubar_.getMenu("IMPORTER").remove(4);// getMenuComponent(4).setEnabled(false);

    if (!projet_.getFichier().equalsIgnoreCase("null.nom")) {
      setPermettreModif(true);
      /*
       * param_ = new DParametresSinavi2(); param_=(IParametresSinavi2) projet_.getParam("par");
       */
      listeSimulationsSinavi2_ = new ArrayList();
      listeSimulationsSinavi2_ = (ArrayList) projet_.getParam("par");
      majSimulationCourante(listeSimulationsSinavi2_.size() - 1);
      simulationCourante_ = listeSimulationsSinavi2_.size() - 1;
      mAJGares();
      System.out.println("nom projet " + ((SSimulationSinavi2) listeSimulationsSinavi2_.get(0)).nomPrj);
      setNomPrj(((SSimulationSinavi2) listeSimulationsSinavi2_.get(0)).nomPrj);
      /**
       * *TODO mettres les listes � jour au dernier indice mettre param ajour a moins de le faire avant le lancement
       * regarde la mise a jour des gares et enlever � terme le parametre gare ki sert pas a gran chose on pourra
       * �galement enlever listehistorique et parametres gen
       */

      /**
       * listeGares_=new ArrayList(); listeGares_ =(ArrayList) projet_.getParam("gar"); listeHistorique_=new
       * ArrayList(); //listeHistorique2_=new ArrayList(); listeHistorique_ =(ArrayList) projet_.getParam("his");
       */
      // simulationCourante_=listeHistorique_.size()-1;
      /*
       * listeGeneration_=new ArrayList(); listeGeneration_=(ArrayList) projet_.getParam("gen");
       */

      // listeHistorique_.add(0,listeHistorique2_);
      /*****************************************************************************************************************
       * remplirListes(); remplirListes2();
       ****************************************************************************************************************/
      // test(""+projet_.getFichier());
      // test(projet_.getInformations()+"");
      activerCommandesSimulation(true);
    }
    /*
     * sinavi2gestionprojet_ = new Sinavi2GestionProjet(this); glassStop_.setVisible(true);
     * sinavi2gestionprojet_.ouvrir();
     */

    // CtuluLibDialog ben=new CtuluLibDialog();
    // CtuluLibDialog.showWarn(getApp(),"test","que veux tu ouvrir ?");
    // JOptionPane.showMessageDialog(null,"Charmout");
    /*
     * try{ Sinavi2FilleAddModBateaux b=new Sinavi2FilleAddModBateaux(this); b.setVisible(true); addInternalFrame(b); }
     * catch(Exception e) { c System.out.println(e.getMessage()); JOptionPane.showMessageDialog(null," de d'exception a
     * la "); }
     */
    // JOptionPane.showMessageDialog(null,"Charmout");
    /*
     * activateInternalFrame(b); JInternalFrame currentFrame = getCurrentInternalFrame();
     * addInternalFrame(currentFrame);
     */
    if (_arg == null) {
      final String r = projet_.getFichier();
      getMainMenuBar().addRecentFile(r, "sinavi2");
      Sinavi2Preferences.SINAVI2.writeIniFile();
    }
  }

  /**
   * quitter Sinavi.
   */
  protected void quitter() {
    // super.exit();

    // String test = ETUDE_SINAVI2.genere();
    /* boite de dialogue sinavi */
    // BuDialogMessage dialog_mess = new BuDialogMessage(getApp(), InfoSoftSinavi2_, "Au revoir");
    // dialog_mess.activate();
    /*
     * if(projet_!=null){ BuDialogConfirmation conf_=new BuDialogConfirmation(getApp(),InfoSoftSinavi2_,"Voulez-vous
     * sauvegarder les modifications ?"); if(conf_.activate()==0){ this.enregistrer(); test("Les donn�es sont
     * sauvegard�es. A bient�t !"); } else{ test("Les derni�res modifications ne sont pas sauvegard�es. A bient�t !"); } }
     * else{ test("A bient�t"); }
     */
    exit();
    /*
     * BuDialogMessage dialog_mess2 = new BuDialogMessage(getApp(), InfoSoftSinavi2_, "Navires G�n�r�s");
     * dialog_mess2.activate();
     */
    // SinaviBateauxParametres t = new SinaviBateauxParametres(getApp());
  }

  protected double rechercheAttenteGarePrec(final int _indice, final int _bateau, final char _sens, final int _numGare,
      final int _simulation) {
    for (int i = _indice; i >= 0; i--) {
      final SResultatHistorique s3 = ((SSimulationSinavi2) listeSimulationsSinavi2_.get(_simulation)).resultatHistorique[i];
      final Sinavi2TypeHistorique s = new Sinavi2TypeHistorique(s3);
      if (s.numeroBateau_ == _bateau && s.avalantMontant_ == _sens && s.elementAFranchir_.equalsIgnoreCase("G")
          && s.numeroElement_ == _numGare) {
        return s.attente_;
      }
    }
    return 0;
  }

  protected void ResAffTableauRecapitulatif(boolean _comparaison) {

    if (!_comparaison) {
      // donneesConsommation();
      final SResultatConsommationDEau[] temp = ((SSimulationSinavi2) listeSimulationsSinavi2_
          .get(getNumeroSimulationCourante())).resultatConsommationDEau;
      listeConsommation_ = new ArrayList();
      if (temp != null) {
        for (int i = 0; i < temp.length; i++) {
          listeConsommation_.add(temp[i]);
        }
      }
      donneesConsommation(false);
    } else {
      donneesConsommation(true);
      System.out.println("taillle de liste consommation avant l'affichage du tableau" + listeCons_.size());
    }
    try {

      if (sinavi2resafftableauconso_ == null) {

        sinavi2resafftableauconso_ = new Sinavi2ResAffTableauConso(this, listeCons_, listeSimulationsSinavi2_,
            _comparaison, simulationsSel_);

        sinavi2resafftableauconso_.setVisible(true);
        addInternalFrame(sinavi2resafftableauconso_);
      } else {
        if (sinavi2resafftableauconso_.isClosed()) {
          addInternalFrame(sinavi2resafftableauconso_);
        } else {
          activateInternalFrame(sinavi2resafftableauconso_);
        }
      }

    } catch (final NullPointerException e) {

      JOptionPane.showMessageDialog(null, "exception capt�e +" + e.getMessage() + CtuluLibString.LINE_SEP
          + e.getCause() + CtuluLibString.LINE_SEP + e.getClass() + CtuluLibString.LINE_SEP + e.getLocalizedMessage()
          + CtuluLibString.LINE_SEP);
    }
  }

  protected void resAttentesBateaux() {
    // donneesAttentes();
    if (listeSimulationsSinavi2_.size() == getNumeroSimulationCourante() - 1) {
      affMessage("La simulation en cours n'a pas encore �t� lanc�e. Veuillez en selectionner une ancienne ou la lancer.");
    }

    else {
      final Sinavi2ResAttentesBateaux f = new Sinavi2ResAttentesBateaux(this,/* (ArrayList)listeAttentes_.get(0), */
      listeBateaux_, listeTrajets_, listeGares_, listeBiefs_, listeEcluses_,
          listeSimulationsSinavi2_/* (ArrayList)listeHistorique_.get(0) */, false);
      /*
       * Sinavi2FilleGraphe f=new Sinavi2FilleGraphe(this, this.getInformationsDocument()); addInternalFrame(f);
       */
      f.setVisible(true);
    }
  }

  protected void resAttentesBateauxC() {
    // donneesAttentes();
    final Sinavi2ResAttentesBateaux f = new Sinavi2ResAttentesBateaux(this/* ,(ArrayList)listeAttentes_.get(0) */,
        listeBateaux_, listeTrajets_, listeGares_, listeBiefs_, listeEcluses_,
        listeSimulationsSinavi2_/* (ArrayList)listeHistorique_.get(0) */, true);
    /*
     * Sinavi2FilleGraphe f=new Sinavi2FilleGraphe(this, this.getInformationsDocument()); addInternalFrame(f);
     */
    f.setVisible(true);
  }

  protected void resAttentesElements() {
    if (listeSimulationsSinavi2_.size() == getNumeroSimulationCourante() - 1) {
      affMessage("La simulation en cours n'a pas encore �t� lanc�e. Veuillez en selectionner une ancienne ou la lancer.");
    }

    else {
      listeAttentes_ = null;
      donneesAttentes(false);
      /*
       * ListIterator it=((ArrayList)listeAttentes_.get(0)).listIterator(); while(it.hasNext()){
       * System.out.println("atta : " + ((SinaviTypeAttente)(it.next())).element_); }
       */
      if (listeAttentes_ != null) {
        final Sinavi2ResAttentesElements f = new Sinavi2ResAttentesElements(this, listeAttentes_, listeBateaux_,
            listeTrajets_, listeGares_, listeBiefs_, listeEcluses_, listeSimulationsSinavi2_, false);
        /*
         * Sinavi2FilleGraphe f=new Sinavi2FilleGraphe(this, this.getInformationsDocument()); addInternalFrame(f);
         */
        f.setVisible(true);
      }
    }
  }

  protected void resAttentesElementsC() {
    donneesAttentes(true);
    final Sinavi2ResAttentesElements f = new Sinavi2ResAttentesElements(this, listeAttentes_, listeBateaux_,
        listeTrajets_, listeGares_, listeBiefs_, listeEcluses_, listeSimulationsSinavi2_, true);
    /*
     * Sinavi2FilleGraphe f=new Sinavi2FilleGraphe(this, this.getInformationsDocument()); addInternalFrame(f);
     */
    f.setVisible(true);
  }

  protected void resComparaisonSimulation() {
    final Sinavi2ResComparaisonSimulations f = new Sinavi2ResComparaisonSimulations(this, listeSimulationsSinavi2_);
    // Sinavi2ResGestionSimulations f=new Sinavi2ResGestionSimulations(this,(ArrayList)listeHistorique_);
    addInternalFrame(f);
  }

  protected void resConsommation() {
    // listeCons_=null;
    // donneesConsommation();
    final SResultatConsommationDEau[] temp = ((SSimulationSinavi2) listeSimulationsSinavi2_
        .get(getNumeroSimulationCourante())).resultatConsommationDEau;
    listeConsommation_ = new ArrayList();
    if (temp != null) {
      for (int i = 0; i < temp.length; i++) {
        listeConsommation_.add(temp[i]);
      }
    }
    donneesConsommation(false);
    // if(listeCons_!=null && listeConsommation_ !=null){
    final Sinavi2ResConsommationDEau f = new Sinavi2ResConsommationDEau(this, listeEcluses_, listeSimulationsSinavi2_,
        listeCons_, listeBateaux_, listeConsommation_, false);
    f.setVisible(true);
    // }
  }

  protected void resConsommationC() {
    donneesConsommation(true);
    final Sinavi2ResConsommationDEau f = new Sinavi2ResConsommationDEau(this, listeEcluses_, listeSimulationsSinavi2_,
        listeCons_, listeBateaux_, listeConsommation_, true);
    f.setVisible(true);
  }

  protected void resDureeParcours() {
    if (listeSimulationsSinavi2_.size() == getNumeroSimulationCourante() + 1) {
      affMessage("La simulation en cours n'a pas encore �t� lanc�e. \nVeuillez en s�lectionner une ancienne ou la lancer.");
    }

    else {
      mAJGares();
      final Sinavi2ResDureeParcours f = new Sinavi2ResDureeParcours(this, listeSimulationsSinavi2_, listeBateaux_,
          listeTrajets_, listeGares_, false);
      // Sinavi2ResDureeParcours f=new
      // Sinavi2ResDureeParcours(this,(ArrayList)listeHistorique_.get(simulationCourante_),listeBateaux_,listeTrajets_,listeGares_,false);
      /*
       * Sinavi2FilleGraphe f=new Sinavi2FilleGraphe(this, this.getInformationsDocument()); addInternalFrame(f);
       */
      f.setVisible(true);
      activateInternalFrame(f);
    }
  }

  /*
   * private int rechercher_ecluse(String eclusecourant) { ListIterator iter= listeEcluses_.listIterator(); int i=0;
   * while (iter.hasNext()){ SParametresEcluse c=new SParametresEcluse(); c=(SParametresEcluse) iter.next(); if(
   * c.identification == eclusecourant){ System.out.println("rechercher"+i); return i; } else i++; /* Sinavi2TypeBateau
   * c= new Sinavi2TypeBateau(); c= (Sinavi2TypeBateau) iter.next(); if( (String)c.getId()==batcourant) return i; else
   * i++;
   */
  /*
   * } return -1; }
   */

  protected void resDureeParcoursC() {
    if (listeSimulationsSinavi2_.size() == getNumeroSimulationCourante() + 1) {
      affMessage("La simulation en cours n'a pas encore �t� lanc�e. Veuillez en selectionner une ancienne ou la lancer.");
    }

    else {
      mAJGares();
      final Sinavi2ResDureeParcours f = new Sinavi2ResDureeParcours(this, listeSimulationsSinavi2_, listeBateaux_,
          listeTrajets_, listeGares_, true);
      /*
       * Sinavi2FilleGraphe f=new Sinavi2FilleGraphe(this, this.getInformationsDocument()); addInternalFrame(f);
       */
      f.setVisible(true);
    }
  }

  /*
   * public void menuCroisements() { /*if(listeEcluses_ == null || listeEcluses_.equals(null) || listeEcluses_.size()==0 ){
   * test("Il n'y a aucune �cluses dans la base"); } else{
   */

  // test de la g�n�ration des fichiers
  // activer le menu que quand les premieres �tapes sont effectu�es donc peut pas besoin de test
  /*
   * try{ if(sinavi2filleaffcroisements_ == null){ if(listeCroisements_ == null){
   * listeCroisements_=creationListeCroisements_(listeBiefs_,listeBateaux_);
   * System.out.println("nulllllllllllllllllllllll"); } else{
   * listeCroisements_=creationListeCroisements_(listeBiefs_,listeBateaux_,listeCroisements_);
   * System.out.println("XXXXXXXXXXXXXXXXXXXXXXXXxxxxxxx"); } sinavi2filleaffcroisements_=new
   * Sinavi2FilleAffCroisements(this,listeBiefs_,listeCroisements_,listeBateaux_,null);
   * sinavi2filleaffcroisements_.setVisible(true); addInternalFrame(sinavi2filleaffcroisements_); } else{
   * if(sinavi2filleaffcroisements_.isClosed()){
   * listeCroisements_=creationListeCroisements_(listeBiefs_,listeBateaux_,listeCroisements_);
   * sinavi2filleaffcroisements_=new Sinavi2FilleAffCroisements(this,listeBiefs_,listeCroisements_,listeBateaux_,null);
   * sinavi2filleaffcroisements_.setVisible(true);
   * sinavi2filleaffcroisements_.miseAJourTableau(listeCroisements_,listeBateaux_);
   * addInternalFrame(sinavi2filleaffcroisements_); System.out.println("apr�s internal");
   * activateInternalFrame(sinavi2filleaffcroisements_); /*
   * sinavi2filleaffcroisements_.miseAJourTableau(listeCroisements_,listeBateaux_);
   * addInternalFrame(sinavi2filleaffcroisements_);
   */

  /*
   * } else{ listeCroisements_=creationListeCroisements_(listeBiefs_,listeBateaux_,listeCroisements_);
   * sinavi2filleaffcroisements_=new Sinavi2FilleAffCroisements(this,listeBiefs_,listeCroisements_,listeBateaux_,null);
   * sinavi2filleaffcroisements_.setVisible(true);
   * sinavi2filleaffcroisements_.miseAJourTableau(listeCroisements_,listeBateaux_);
   * addInternalFrame(sinavi2filleaffcroisements_); System.out.println("apr�s internal");
   * activateInternalFrame(sinavi2filleaffcroisements_);
   * /*sinavi2filleaffcroisements_.miseAJourTableau(listeCroisements_,listeBateaux_); /*
   * activateInternalFrame(sinavi2filleaffcroisements_);
   */
  /*
   * } } } catch(NullPointerException e) { JOptionPane.showMessageDialog(null," exception +"+e.getMessage()+
   * CtuluLibString.LINE_SEP +e.getCause()+ CtuluLibString.LINE_SEP +e.getClass()+ CtuluLibString.LINE_SEP
   * +e.getLocalizedMessage()+ CtuluLibString.LINE_SEP); } /*}
   */

  /**
   * Afficher tous les types de bateaux.
   */
  protected void resGestionSimulation() {
    System.out.println("simulation sleectionn�e : " + getNumeroSimulationCourante());
    final Sinavi2ResGestionSimulations f = new Sinavi2ResGestionSimulations(this, listeSimulationsSinavi2_);
    // Sinavi2ResGestionSimulations f=new Sinavi2ResGestionSimulations(this,(ArrayList)listeHistorique_);
    addInternalFrame(f);
  }

  /**
   * Cr�ation ou affichage d'une fenetre pour l'exploitation des r�sultats des simulations ouvertes.
   */
  protected void resultatsSimulations() {
  // cr�ation du rapport
  }

  protected void setCommentaire(final String _commentaire) {
    commentaireCourant_ = _commentaire;
  }

  /**
   * gestion des menus du bureau.
   * 
   * @param _evt
   */
  public void actionPerformed(final ActionEvent _evt) {
    // int i=-1;

    String action = _evt.getActionCommand();
    System.err.println("ACTION=" + action);

    final int i = action.indexOf('(');
    String arg = null;
    if (i >= 0) {
      arg = action.substring(i + 1, action.length() - 1);
      action = action.substring(0, i);
    }
    if (action.equals("CREER")) {
      creer();
    } else if (action.equals("CONSOLE")) {
      final File ts = new File(FudaaLib.getUserDirUrl() + "/ts.log");

      System.out.println("chemin log : " + ts.getAbsolutePath());
      // openFileInLogFrame(ts);
      cmdOpenFollowOnTsLog();
      // openLogFrame();
    } else if (action.equals("OUVRIR")) ouvrir();
    else if (action.equals("REOUVRIR")) ouvrir(arg);
    else if (action.equals("ENREGISTRER")) enregistrer();
    else if (action.equals("ENREGISTRERSOUS")) enregistrerSous();
    else if (action.equals("FERMER")) fermer();
    else if (action.equals("QUITTER")) quitter();
    else if (action.equals("PREFERENCE")) preferences();
    else if (action.equals(getActAddBat())) ajouterBateaux(i, false);
    else if (action.equals("CONTROLE")) controler();
    else if (action.equals("MODBATEAUX")) modifierBateaux();
    else if (action.equals(getActSupBateau())) supprimerBateaux();
    else if (action.equals("AFFBATEAUX")) afficherBateaux();
    else if (action.equals("ADDBIEFS")) ajouterBief(i, false);
    else if (action.equals("MODBIEFS")) modifierBiefs();
    else if (action.equals(getActSupBiefs())) supprimerBiefs();
    else if (action.equals("AFFBIEFS")) afficherBiefs();
    else if (action.equals(getActAddEcluse())) ajouterEcluse(i, false);
    else if (action.equals(getActModEcluse())) modifierEcluses();
    else if (action.equals(getActSupEcluse())) supprimerEcluses();
    else if (action.equals("AFFECLUSES")) afficherEcluses();
    else if (action.equals("CROISEMENTS")) menuCroisements(null);
    else if (action.equals("TREMATAGES")) menuTrematages(null);
    else if (action.equals("VITESSES")) menuVitesses(0, 0);
    else if (action.equals("MANOEUVRES")) menuManoeuvres(0, 0);
    else if (action.equals("PALETTE")) menuPalette(/* (SParametresTrajets) listeTrajets_.get(0) */null);
    else if (action.equals(getActAssembler())) creerConnexion();
    else if (action.equals("VISUALISER")) afficherConnexions();
    else if (action.equals("LANCER")) {
      // opr
      initialiserTirage();
      // lancer();
    }
    // opr
    else if (action.equals("TRAJETS")) afficherTrajets();// trajet(i);
    /*
     * else if (action.equals("IMPORTER")) oprCalculer();
     */
    else if (action.equals("GENERER")) initialiserTirage();
    else if (action.equals(getActImpNav())) importer(new String[] { "nav", "NAV" });
    else if (action.equals(getActImpBief())) importer(new String[] { "bie", "BIE" });
    else if (action.equals(getActImportOuv())) importer(new String[] { "ouv", "OUV" });
    else if (action.equals("IMPORTERRESEAU")) importer(new String[] { "res", "RES" });
    else if (action.equals("IMPORTERNAV2")) importer2(new String[] { "nav2", "NAV2" });
    else if (action.equals("IMPORTEROUV2")) importer2(new String[] { "ouv2", "OUV2" });
    else if (action.equals("EXPORTERNAV")) exporterBateau();
    else if (action.equals("EXPORTEROUV")) exporterEcluse();
    else if (action.equals("EXPORTERNAV2")) exporterBateau2();
    else if (action.equals("EXPORTALL")) exporterAll();

    else if (action.equals("EXPORTEROUV2")) exporterEcluse2();
    else if (action.equals("EXPORTERBIE")) exporterBief();
    else if (action.equals("EXPORTERRESEAU")) exporterReseau();
    else if (action.equals("GENERATION")) affGenerationBateaux(0, 0, 0);
    else if (action.equals("HISTORIQUE")) affHistorique(0, 0, 0, 0, false);
    else if (action.equals("RESDUREEPARCOURS")) resDureeParcours();
    /*
     * else if ( action.equals("RESCONSODEAU")) consoDEau();//pas encore fait
     */
    else if (action.equals("RESATTENTESBATEAUX")) resAttentesBateaux();
    else if (action.equals("RESATTENTESELEMENTS")) resAttentesElements();
    else if (action.equals("RESGESTIONSIMULATIONS")) resGestionSimulation();
    else if (action.equals("COMPARAISONSSIMULATIONS")) resComparaisonSimulation();
    else if (action.equals("INDISPONIBILITES")) afficherIndisponibilites(0);
    else if (action.equals("TABLEAURECAPITULATIF")) ResAffTableauRecapitulatif(false);
    else if (action.equals("RESCONSO")) resConsommation();
    else if (action.equals("TABLEAURES")) affTableauResultat();
    else
      super.actionPerformed(_evt);

  }

  public void addListeBief(final SParametresBief b_) {
    /** verifier l'existance du bief* */
    final int x = rechercherBief(b_.identification);
    if (x == -1) {
      listeBiefs_.add(b_);
    } else {
      listeBiefs_.remove(x);
      definirIndisponibilites(b_, null);
      listeBiefs_.add(b_);
    }
  }

  public void addListeEcluse(final SParametresEcluse _b) {
    /** verifier l'existance de l'ecluse* */

    final int x = rechercherEcluse(_b.identification);
    if (x == -1) {
      listeEcluses_.add(_b);
    } else {
      listeEcluses_.remove(x);
      definirIndisponibilites(null, _b);
      listeEcluses_.add(_b);
    }
  }

  /**
   * Afficher tous les types de bateaux.
   */
  public void affGenerationBateaux(final int _bateauSel, final int _avalantMontantSel, final int _jourSel) {

    System.out.println("simulation courante : " + getNumeroSimulationCourante());

    // SResultatGenerationBateau[] temp = ((SSimulationSinavi2)
    // listeSimulationsSinavi2_.get(getNumeroSimulationCourante())).resultatGenerationBateau;
    // SResultatGenerationBateau[] temp = ((SSimulationSinavi2)
    // listeSimulationsSinavi2_.get(getNumeroSimulationCourante())).resultatGenerationBateau;

    final SResultatGenerationBateau[] temp = ((SSimulationSinavi2) listeSimulationsSinavi2_
        .get(getNumeroSimulationCourante())).resultatGenerationBateau;
    listeGeneration_ = new ArrayList();
    if (temp != null) {
      for (int i = 0; i < temp.length; i++) {
        listeGeneration_.add(temp[i]);
      }
    }

    if (listeGeneration_.size() == 0) {
      affMessage("Aucune g�n�ration n'a �t� effectu�e pour cette simulation. \n Veuillez en selectionner une nouvelle");
    }

    else {

      try {

        if (sinavi2filleaffgeneration_ == null) {

          sinavi2filleaffgeneration_ = new Sinavi2FilleAffGeneration(this, listeGeneration_, listeBateaux_,
              gen_.dureeSimulation, _bateauSel, _avalantMontantSel, _jourSel);

          sinavi2filleaffgeneration_.setVisible(true);
          addInternalFrame(sinavi2filleaffgeneration_);
        } else {
          if (sinavi2filleaffgeneration_.isClosed()) {
            // sinavi2filleaffgeneration_.miseajour(gen_.dureeSimulation);
            addInternalFrame(sinavi2filleaffgeneration_);

          } else {
            // sinavi2filleaffgeneration_.miseajour(listeBateaux_);
            activateInternalFrame(sinavi2filleaffgeneration_);
            addInternalFrame(sinavi2filleaffgeneration_);
            affMessage("Modification bien effectu�e");

          }
        }

      } catch (final NullPointerException e) {

        JOptionPane.showMessageDialog(null, " exception  +" + e.getMessage() + CtuluLibString.LINE_SEP + e.getCause()
            + CtuluLibString.LINE_SEP + e.getClass() + CtuluLibString.LINE_SEP + e.getLocalizedMessage()
            + CtuluLibString.LINE_SEP);
      }
    }

  }

  public void affGraphe2(final BGraphe _g) {
    // Sinavi2FilleGraphe f=new Sinavi2FilleGraphe(this, this.getInformationsDocument(),_g);
    fillegraphe_ = new Sinavi2FilleGraphe(this, this.getInformationsDocument(), _g);
    /*
     * addInternalFrame(f); activateInternalFrame(f); f.setVisible(true);
     */
    addInternalFrame(fillegraphe_);
    activateInternalFrame(fillegraphe_);
    fillegraphe_.setVisible(true);

  }

  /** * afficher l'historique suivant des param�tres. * */
  public void affHistorique(final int _bateauSelect, final int _typeBateauSelect, final int _elementAFranchirSelect,
      final int _elementSelected, final boolean _attenteSelected) {

    // listeHistorique_=new ArrayList();
    /*
     * if(temp!=null){ for(int i=0;i<temp.length;i++) listeHistorique_.add(temp[i]); }
     */

    if (listeSimulationsSinavi2_.size() == getNumeroSimulationCourante() - 1) {
      affMessage("La simulation en cours n'a pas encore �t� lanc�e.  \n Veuillez en s�lectionner une ancienne ou la lancer.");
    }

    else {
      final SResultatHistorique[] temp = ((SSimulationSinavi2) listeSimulationsSinavi2_.get(simulationCourante_)).resultatHistorique;
      try {
        if (sinavi2filleaffhistorique_ == null) {

          sinavi2filleaffhistorique_ = new Sinavi2FilleAffHistorique(this, temp, listeBateaux_, listeBiefs_,
              listeEcluses_, _bateauSelect, _typeBateauSelect, _elementAFranchirSelect, _elementSelected, listeGares_,
              _attenteSelected);

          sinavi2filleaffhistorique_.setVisible(true);
          addInternalFrame(sinavi2filleaffhistorique_);
        } else {
          if (sinavi2filleaffhistorique_.isClosed()) {
            // sinavi2filleaffhistorique_=new
            // Sinavi2FilleAffHistorique(this,listeHistorique_,listeBateaux_,_bateauSelect,_typeBateauSelect,_elementAFranchirSelect);
            addInternalFrame(sinavi2filleaffhistorique_);

          } else {
            activateInternalFrame(sinavi2filleaffhistorique_);
            // sinavi2filleaffhistorique_=new
            // Sinavi2FilleAffHistorique(this,listeHistorique_,listeBateaux_,_bateauSelect,_typeBateauSelect,_elementAFranchirSelect);
            addInternalFrame(sinavi2filleaffhistorique_);

          }
        }

      } catch (final NullPointerException e) {

        JOptionPane.showMessageDialog(null, " exception +" + e.getMessage() + CtuluLibString.LINE_SEP + e.getCause()
            + CtuluLibString.LINE_SEP + e.getClass() + CtuluLibString.LINE_SEP + e.getLocalizedMessage()
            + CtuluLibString.LINE_SEP);
      }
    }

  }

  public void afficherBateaux() {

    // if (listeBateaux_ == null || listeBateaux_.equals(null) || listeBateaux_.size() == 0) {
    if (CtuluLibArray.isEmpty(listeBateaux_)) {
      affMessage("Il n'y a aucun bateaux dans la base");
    } else {
      try {
        if (sinavi2filleaffbateaux_ == null) {

          sinavi2filleaffbateaux_ = new Sinavi2FilleAffBateaux(this, listeBateaux_);

          sinavi2filleaffbateaux_.setVisible(true);
          addInternalFrame(sinavi2filleaffbateaux_);
        } else {
          if (sinavi2filleaffbateaux_.isClosed()) {
            sinavi2filleaffbateaux_.miseAJour(listeBateaux_);
            addInternalFrame(sinavi2filleaffbateaux_);

          } else {
            sinavi2filleaffbateaux_.miseAJour(listeBateaux_);
            activateInternalFrame(sinavi2filleaffbateaux_);

            affMessage("Modification bien effectu�e");

          }
        }

      } catch (final NullPointerException e) {

        JOptionPane.showMessageDialog(null, " de  d'exception � la con +" + e.getMessage() + CtuluLibString.LINE_SEP
            + e.getCause() + CtuluLibString.LINE_SEP + e.getClass() + CtuluLibString.LINE_SEP + e.getLocalizedMessage()
            + CtuluLibString.LINE_SEP);
      }
    }

  }

  /**
   * Afficher tous les types de bateaux.
   */

  public void afficherBiefs() {
    // Fred aie
    // if (listeBiefs_ == null || listeBiefs_.equals(null) || listeBiefs_.size() == 0) {
    if (CtuluLibArray.isEmpty(listeBiefs_)) {
      affMessage("Il n'y a aucun biefs dans la base");
    } else {
      try {
        if (sinavi2filleaffbiefs_ == null) {

          sinavi2filleaffbiefs_ = new Sinavi2FilleAffBiefs(this, listeBiefs_);

          sinavi2filleaffbiefs_.setVisible(true);
          addInternalFrame(sinavi2filleaffbiefs_);
        } else {
          if (sinavi2filleaffbiefs_.isClosed()) {
            sinavi2filleaffbiefs_.miseAJour(listeBiefs_);
            addInternalFrame(sinavi2filleaffbiefs_);

          } else {
            sinavi2filleaffbiefs_.miseAJour(listeBiefs_);
            activateInternalFrame(sinavi2filleaffbiefs_);
          }
        }

      } catch (final NullPointerException e) {

        JOptionPane.showMessageDialog(null, " exception+" + e.getMessage() + CtuluLibString.LINE_SEP + e.getCause()
            + CtuluLibString.LINE_SEP + e.getClass() + CtuluLibString.LINE_SEP + e.getLocalizedMessage()
            + CtuluLibString.LINE_SEP);
      }
    }

  }

  public void afficherConnexions() {
    mAJGares();
    // fred aie aie: si listeEcluses est null cela plante
    // equals null ne marche jamais ....
    /*
     * if ((listeEcluses_ == null || listeEcluses_.equals(null) || listeEcluses_.size() == 0) && (listeBiefs_ == null ||
     * listeBiefs_.equals(null) || listeBiefs_.size() == 0)) {
     */
    if (CtuluLibArray.isEmpty(listeEcluses_) && CtuluLibArray.isEmpty(listeBiefs_)) {
      affMessage("Il n'y a ni bief ni �cluse");
    } else {
      if (sinavi2filleaffconnexions_ == null) {

        sinavi2filleaffconnexions_ = new Sinavi2FilleAffConnexions(this, listeBiefs_, listeEcluses_, listeGares_);

        sinavi2filleaffconnexions_.setVisible(true);
        addInternalFrame(sinavi2filleaffconnexions_);
      } else {
        if (sinavi2filleaffconnexions_.isClosed()) {
          sinavi2filleaffconnexions_.miseajour(listeBiefs_, listeEcluses_);
          addInternalFrame(sinavi2filleaffconnexions_);

        } else {
          sinavi2filleaffconnexions_.miseajour(listeBiefs_, listeEcluses_);
          activateInternalFrame(sinavi2filleaffconnexions_);
        }
      }

    }

  }

  public void afficherEcluses() {

    // AIE: equals(null)! if (listeEcluses_ == null || listeEcluses_.equals(null) || listeEcluses_.size() == 0) {
    if (CtuluLibArray.isEmpty(listeEcluses_)) {
      affMessage("Il n'y a aucune �cluses dans la base");
    } else {
      try {
        if (sinavi2filleaffecluses_ == null) {

          sinavi2filleaffecluses_ = new Sinavi2FilleAffEcluses(this, listeEcluses_);

          sinavi2filleaffecluses_.setVisible(true);
          addInternalFrame(sinavi2filleaffecluses_);
        } else {
          if (sinavi2filleaffecluses_.isClosed()) {
            sinavi2filleaffecluses_.miseajour(listeEcluses_);
            addInternalFrame(sinavi2filleaffecluses_);

          } else {
            sinavi2filleaffecluses_.miseajour(listeEcluses_);
            activateInternalFrame(sinavi2filleaffecluses_);
          }
        }
//fred a ne jamais faire ....
      } catch (final NullPointerException e) {

        JOptionPane.showMessageDialog(null, "exception  +" + e.getMessage() + CtuluLibString.LINE_SEP + e.getCause()
            + CtuluLibString.LINE_SEP + e.getClass() + CtuluLibString.LINE_SEP + e.getLocalizedMessage()
            + CtuluLibString.LINE_SEP);
      }
    }

  }

  public void affMessage(final String _s) {
    final BuDialogMessage dialogMess = new BuDialogMessage(getApp(), InfoSoftSinavi2_, "" + _s);
    dialogMess.activate();
  }

  public void affTableauResultat() {
    // FuLog.debug("Entr� ");
    if (getNumeroSimulationCourante() == listeSimulationsSinavi2_.size() - 1) {
      message("Selectionnez une simulation dans le menu gestion de simulations");
    } else {
      donneesAttentes(false);
      try {
        // donneesAttentes(false);
        if (sinavi2afftableauresultat_ == null) {
          FuLog.debug("entr�e 1");
          sinavi2afftableauresultat_ = new Sinavi2ResAffTableauResultat(this, listeAttentes_, listeBateaux_,
              listeBiefs_, listeEcluses_);
          // System.out.println("55555");
          sinavi2afftableauresultat_.setVisible(true);
          addInternalFrame(sinavi2afftableauresultat_);
        } else {
          FuLog.debug("entr�e 2");
          if (sinavi2afftableauresultat_.isClosed()) {
            addInternalFrame(sinavi2afftableauresultat_);

          } else {
            FuLog.debug("entr�e 3");
            activateInternalFrame(sinavi2afftableauresultat_);
            addInternalFrame(sinavi2afftableauresultat_);

          }
        }

      } catch (final NullPointerException e) {

        JOptionPane.showMessageDialog(null, "exception+" + e.getMessage() + CtuluLibString.LINE_SEP + e.getCause()
            + CtuluLibString.LINE_SEP + e.getClass() + CtuluLibString.LINE_SEP + e.getLocalizedMessage()
            + CtuluLibString.LINE_SEP + getNumeroSimulationCourante() + CtuluLibString.LINE_SEP);
      }
    }
  }

  public boolean confirmExit() {
    return true;
  }

  public void creerConnexion() {
    if (listeBiefs_ == null & listeEcluses_ == null) {
      affMessage("Il n'y a ni bief ni �cluse");
    } else {
      if (listeGares_ == null) {
        listeGares_ = new ArrayList();
      }
      try {
        // if(sinavifilleaddmodconnexion_ == null) {
        sinavi2filleaddmodconnexions_ = new Sinavi2FilleAddModConnexion(this, listeGares_, listeBiefs_, listeEcluses_);
        sinavi2filleaddmodconnexions_.setVisible(true);
        /*
         * } else if (sinavifilleaddmodconnexion_.isClosed()) { addInternalFrame(sinavifilleaddmodconnexion_); } else {
         * activateInternalFrame(sinavifilleaddmodconnexion_); } } // sinavifilleparametres_.ongletCourant_ =
         * sinavifilleparametres_.tpMain_.getSelectedIndex();
         */
      } catch (final Exception e) {
        affMessage("Erreur Technique veuillez nous en excuser");
      }
    }
  }

  /*
   * protected void trajet(int _traj){ if(listeBiefs_!=null & listeEcluses_!=null){ mAJGares(); } if(listeTrajets_ ==
   * null){ listeTrajets_=new ArrayList(); } try{ if(sinavi2filleaddmodtrajets_ == null){ sinavi2filleaddmodtrajets_=new
   * Sinavi2FilleTrajet(this,listeTrajets_,listeBateaux_,listeGares_); FuLog.debug("listebateau taille
   * :"+listeBateaux_.size()); sinavi2filleaddmodtrajets_.setntrajet(-1); sinavi2filleaddmodtrajets_.setVisible(true);
   * addInternalFrame(sinavi2filleaddmodtrajets_); } else{ if(sinavi2filleaddmodtrajets_.isClosed()){
   * sinavi2filleaddmodtrajets_.setntrajet(_traj); sinavi2filleaddmodtrajets_.initialiseChamps(_traj);
   * addInternalFrame(sinavi2filleaddmodtrajets_); } else { sinavi2filleaddmodtrajets_.setntrajet(_traj);
   * sinavi2filleaddmodtrajets_.initialiseChamps(_traj); activateInternalFrame(sinavi2filleaddmodtrajets_); } } }
   * catch(Exception e) { System.out.println(e.getMessage()); JOptionPane.showMessageDialog(null,"pas de bateaux ou de
   * listes de gares"); } }
   */
  public void definirIndisponibilites(final SParametresBief _bief, final SParametresEcluse _ecluse) {
    if (_bief != null) {
      FuLog.debug("test bief indisponibilit�s");
      _bief.loiD = new SParametresIndispoLoiD[0];
      _bief.loiE = new SParametresIndispoLoiE();
      _bief.loiE.duree = new SHeure();
      _bief.loiE.duree.heure = 0;
      _bief.loiE.duree.minutes = 0;
      _bief.loiE.nbJoursDEcart = 0;
      _bief.loiE.ordreDuree = 0;
      _bief.loiE.ordreEcart = 0;
      _bief.loiJ = new SParametresIndispoLoiJ();
      _bief.loiJ.heureDebut = new SHeure();
      _bief.loiJ.heureFin = new SHeure();
      _bief.loiJ.heureDebut.heure = 0;
      _bief.loiJ.heureDebut.minutes = 0;
      _bief.loiJ.heureFin.heure = 0;
      _bief.loiJ.heureFin.minutes = 0;
    } else {
      _ecluse.loiD = new SParametresIndispoLoiD[0];
      _ecluse.loiE = new SParametresIndispoLoiE();
      _ecluse.loiE.duree = new SHeure();
      _ecluse.loiE.duree.heure = 0;
      _ecluse.loiE.duree.minutes = 0;
      _ecluse.loiE.nbJoursDEcart = 0;
      _ecluse.loiE.ordreDuree = 0;
      _ecluse.loiE.ordreEcart = 0;
      _ecluse.loiJ = new SParametresIndispoLoiJ();
      _ecluse.loiJ.heureDebut = new SHeure();
      _ecluse.loiJ.heureFin = new SHeure();
      _ecluse.loiJ.heureDebut.heure = 0;
      _ecluse.loiJ.heureDebut.minutes = 0;
      _ecluse.loiJ.heureFin.heure = 0;
      _ecluse.loiJ.heureFin.minutes = 0;
    }

  }

  /*
   * public void donneesAttentes(){ listeAttentes_ =new ArrayList(); //SinaviTypeAttente s2; for(int i=0;i<listeHistorique_.size();i++){
   * ArrayList attenteTemp=new ArrayList(); ListIterator it=((ArrayList)listeHistorique_.get(i)).listIterator();
   * it.next();//nom simulatio while(it.hasNext()){ boolean trouve=false; SResultatHistorique
   * s3=(SResultatHistorique)it.next(); SinaviTypeHistorique s=new SinaviTypeHistorique(s3); ListIterator
   * it2=attenteTemp.listIterator(); while(it2.hasNext() && !trouve){ SinaviTypeAttente
   * s2=(SinaviTypeAttente)it2.next();
   * if(((SParametresBateau)(listeBateaux_.get(s.numeroTypeBateau_-1))).identification.equalsIgnoreCase(s2.getTypeBateaux()) &&
   * (s.elementAFranchir_.charAt(0)==(s2.getTypeElement())) && ((s.elementAFranchir_.equalsIgnoreCase("O") &&
   * ((SParametresEcluse)listeEcluses_.get(s.numeroElement_-1)).identification.equalsIgnoreCase(s2.element_)
   * )||((s.elementAFranchir_.equalsIgnoreCase("B") &&
   * ((SParametresBief)listeBiefs_.get(s.numeroElement_-1)).identification.equalsIgnoreCase(s2.element_))))){
   * trouve=true; s2.nbBatTotal_++; //mise a jour des donn�es if(s.attente_>0){ if(s.avalantMontant_=='M'){
   * s2.nbBatMontantAtt_++; if(s.attente_>s2.getDureeMaxMontantAtt()) s2.setDureeMaxMontantAtt(s.attente_);
   * if(s.attente_<s2.getDureeMinMontantAtt()) s2.setDureeMinMontantAtt(s.attente_); //ajoute la nouvelle dur�e de plus
   * on pourra ainsi calculer la moyenne double[]tabTemp=new double[s2.listeMontantDesTempsDAtt_.length+1]; double
   * moyTemp=0; for(int j=0;j<s2.listeMontantDesTempsDAtt_.length;j++){ tabTemp[j]=s2.listeMontantDesTempsDAtt_[j];
   * moyTemp+=s2.listeMontantDesTempsDAtt_[j]; } tabTemp[s2.listeMontantDesTempsDAtt_.length]=s.attente_;
   * moyTemp+=s.attente_; s2.setDureeMoyMontantAtt(moyTemp/tabTemp.length);
   * s2.setDureeTtlMontantAtt(s2.getDureeTtlMontantAtt()+s.attente_); s2.listeMontantDesTempsDAtt_=tabTemp; } else{
   * s2.nbBatAvalantAtt_++; if(s.attente_>s2.getDureeMaxAvalantAtt()) s2.setDureeMaxAvalantAtt(s.attente_);
   * if(s.attente_<s2.getDureeMinAvalantAtt()) s2.setDureeMinAvalantAtt(s.attente_); double[]tabTemp=new
   * double[s2.listeAvalantDesTempsDAtt_.length+1]; double moyTemp=0; for(int j=0;j<s2.listeAvalantDesTempsDAtt_.length;j++){
   * tabTemp[j]=s2.listeAvalantDesTempsDAtt_[j]; moyTemp+=s2.listeAvalantDesTempsDAtt_[j]; }
   * tabTemp[s2.listeAvalantDesTempsDAtt_.length]=s.attente_; moyTemp+=s.attente_;
   * s2.setDureeMoyAvalantAtt(moyTemp/tabTemp.length); s2.setDureeTtlAvalantAtt(s2.getDureeTtlAvalantAtt()+s.attente_);
   * s2.listeAvalantDesTempsDAtt_=tabTemp; } } else{// pas d'attente if(s.avalantMontant_=='M'){ s2.nbBatMontant_++;
   * //est ce qu'il faut inclure dans la moyenne ceux qui n'ont pas attendus? } else { s2.nbBatAvalant_++; } } }
   * //attenteTemp.add(s2); } if(!trouve){//creation du nouveau SinaviTypeAttente System.out.println("ajout");
   * SinaviTypeAttente s2=new SinaviTypeAttente(); boolean nonGare=false; if(s.elementAFranchir_.charAt(0)=='G'){
   * System.out.println("on veut pasl es gares"); nonGare=true; } else if(s.elementAFranchir_.charAt(0)=='B'){
   * s2.setElement(((SParametresBief)(listeBiefs_.get(s.numeroElement_-1))).identification); } else
   * if(s.elementAFranchir_.charAt(0)=='O'){
   * s2.setElement(((SParametresEcluse)(listeEcluses_.get(s.numeroElement_-1))).identification); } if(!nonGare){
   * s2.nbBatTotal_=1; s2.setTypeBateaux(((SParametresBateau) listeBateaux_.get(s.numeroTypeBateau_-1)).identification);
   * s2.setTypeElement(s.elementAFranchir_.charAt(0)); if(s.attente_>0){ if(s.avalantMontant_=='M'){
   * s2.nbBatMontantAtt_=1; s2.dureeMoyMontantAtt_=s.attente_; s2.setDureeMinMontantAtt(s.attente_);
   * s2.setDureeMaxMontantAtt(s.attente_); s2.listeMontantDesTempsDAtt_=new double[1];
   * s2.listeMontantDesTempsDAtt_[0]=s.attente_; } else{ s2.nbBatAvalantAtt_=1; s2.dureeMoyAvalantAtt_=s.attente_;
   * s2.setDureeMinAvalantAtt(s.attente_); s2.setDureeMaxAvalantAtt(s.attente_); s2.listeAvalantDesTempsDAtt_=new
   * double[1]; s2.listeAvalantDesTempsDAtt_[0]=s.attente_; } } else{ if(s.avalantMontant_=='M') s2.nbBatMontant_++;
   * else s2.nbBatAvalant_++; } attenteTemp.add(s2); } } } listeAttentes_.add(attenteTemp); }
   * System.out.println("test"); ArrayList test=(ArrayList) listeAttentes_.get(0); ListIterator it=test.listIterator();
   * while(it.hasNext()){ SinaviTypeAttente test2=(SinaviTypeAttente)it.next();
   * System.out.println("-------"+test2.element_ + CtuluLibString.ESPACE + test2.getNbBatTotal()); } }
   */
  /**
   * On recherche toutes les donn�es pour travailler sur les attentes.
   */
  public void donneesAttentes(boolean _comparaison) {
    if (!_comparaison && (getNumeroSimulationCourante() == listeSimulationsSinavi2_.size() - 1)) {
      affMessage("Selectionnez une simulation dans le menu gestion de simulations");
    } else {
      listeAttentes_ = new ArrayList();
      FuLog.debug("num simulation qd pas choisi : " + getNumeroSimulationCourante());
      int[] tabSim;
      if (!_comparaison) {

        tabSim = new int[1];
        tabSim[0] = getNumeroSimulationCourante();
      } else {
        tabSim = simulationsSel_;
      }
      // SinaviTypeAttente s2;
      /**
       * on cr� un attenteTemp pour chaque simulation que l'on met dans liste d'attente
       */
      for (int i = 0; i < tabSim.length; i++) {
        final ArrayList attenteTemp = new ArrayList();
        for (int i2 = 0; i2 < ((SSimulationSinavi2) listeSimulationsSinavi2_.get(tabSim[i])).resultatHistorique.length; i2++) {
          // ListIterator it=((ArrayList)listeHistorique_.get(i)).listIterator();
          // it.next();//nom simulatio
          // while(it.hasNext()){
          boolean trouve = false;
          final SResultatHistorique s3 = ((SSimulationSinavi2) listeSimulationsSinavi2_.get(tabSim[i])).resultatHistorique[i2];
          final Sinavi2TypeHistorique s = new Sinavi2TypeHistorique(s3);
          final ListIterator it2 = attenteTemp.listIterator();
          // int z=-1;
          if (s.elementAFranchir_.equalsIgnoreCase("G")) {
            trouve = true;
          }
          while (it2.hasNext() && !trouve) {// z++;
            final Sinavi2TypeAttente s2 = (Sinavi2TypeAttente) it2.next();
            if (((SParametresBateau) (listeBateaux_.get(s.numeroTypeBateau_ - 1))).identification.equalsIgnoreCase(s2
                .getTypeBateaux())
                && (s.elementAFranchir_.charAt(0) == (s2.getTypeElement()))
                && ((s.elementAFranchir_.equalsIgnoreCase("O") && ((SParametresEcluse) listeEcluses_
                    .get(s.numeroElement_ - 1)).identification.equalsIgnoreCase(s2.element_)) || ((s.elementAFranchir_
                    .equalsIgnoreCase("B") && ((SParametresBief) listeBiefs_.get(s.numeroElement_ - 1)).identification
                    .equalsIgnoreCase(s2.element_))))) {
              trouve = true;
              s2.nbBatTotal_++;
              double attente = s.attente_;
              if (s.elementAFranchir_.equalsIgnoreCase("B")) {
                if (s.avalantMontant_ == 'M') {
                  attente += rechercheAttenteGarePrec(i2, s.numeroBateau_, s.avalantMontant_,
                      ((SParametresBief) listeBiefs_.get(s.numeroElement_ - 1)).gareEnAval, tabSim[i]);
                } else {
                  attente += rechercheAttenteGarePrec(i2, s.numeroBateau_, s.avalantMontant_,
                      ((SParametresBief) listeBiefs_.get(s.numeroElement_ - 1)).gareEnAmont, tabSim[i]);
                }
              } else if (s.elementAFranchir_.equalsIgnoreCase("O")) {
                if (s.avalantMontant_ == 'M') {
                  attente += rechercheAttenteGarePrec(i2, s.numeroBateau_, s.avalantMontant_,
                      ((SParametresEcluse) listeEcluses_.get(s.numeroElement_ - 1)).gareEnAval, tabSim[i]);
                } else {
                  attente += rechercheAttenteGarePrec(i2, s.numeroBateau_, s.avalantMontant_,
                      ((SParametresEcluse) listeEcluses_.get(s.numeroElement_ - 1)).gareEnAmont, tabSim[i]);
                }
              }

              // mise a jour des donn�es
              if (attente > 0) {
                if (s.avalantMontant_ == 'M') {
                  s2.nbBatMontantAtt_++;
                  s2.nbBatMontant_++;
                  if (attente > s2.getDureeMaxMontantAtt()) {
                    s2.setDureeMaxMontantAtt(attente);
                  }
                  if (attente < s2.getDureeMinMontantAtt()) {
                    s2.setDureeMinMontantAtt(attente);
                  }
                  // ajoute la nouvelle dur�e de plus on pourra ainsi calculer la moyenne
                  if (s2.listeMontantDesTempsDAtt_ != null) {
                    final double[] tabTemp = new double[s2.listeMontantDesTempsDAtt_.length + 1];
                    double moyTemp = 0;
                    for (int j = 0; j < s2.listeMontantDesTempsDAtt_.length; j++) {
                      // FuLog.debug("non null");
                      tabTemp[j] = s2.listeMontantDesTempsDAtt_[j];
                      moyTemp += s2.listeMontantDesTempsDAtt_[j];
                    }
                    tabTemp[s2.listeMontantDesTempsDAtt_.length] = attente;
                    System.out.println("attaente  : " + attente);
                    // *** moyTemp+=s.attente_;
                    // ***s2.setDureeMoyMontantAtt(moyTemp/tabTemp.length);
                    s2.setDureeTtlMontantAtt(s2.getDureeTtlMontantAtt() + attente);
                    s2.listeMontantDesTempsDAtt_ = new double[tabTemp.length];
                    for (int v = 0; v < tabTemp.length; v++) {
                      s2.listeMontantDesTempsDAtt_[v] = tabTemp[v];
                      // FuLog.debug("liste montant : "+s2.listeMontantDesTempsDAtt_[v]);
                    }
                    s2.calculeMoy();
                    // attenteTemp.set(z,s2);
                  } else {
                    // FuLog.debug(" null");
                    s2.listeMontantDesTempsDAtt_ = new double[1];
                    s2.listeMontantDesTempsDAtt_[0] = attente;
                    s2.setDureeTtlMontantAtt(attente);
                  }
                  // System.out.println("attaente 2:"+
                  // s2.listeMontantDesTempsDAtt_[s2.listeMontantDesTempsDAtt_.length-1]);
                } else {
                  s2.nbBatAvalantAtt_++;
                  s2.nbBatAvalant_++;
                  if (attente > s2.getDureeMaxAvalantAtt()) {
                    s2.setDureeMaxAvalantAtt(attente);
                  }
                  if (attente < s2.getDureeMinAvalantAtt()) {
                    s2.setDureeMinAvalantAtt(attente);
                  }
                  if (s2.listeAvalantDesTempsDAtt_ != null) {
                    final double[] tabTemp = new double[s2.listeAvalantDesTempsDAtt_.length + 1];
                    double moyTemp = 0;
                    for (int j = 0; j < s2.listeAvalantDesTempsDAtt_.length; j++) {
                      tabTemp[j] = s2.listeAvalantDesTempsDAtt_[j];
                      moyTemp += s2.listeAvalantDesTempsDAtt_[j];
                    }
                    tabTemp[s2.listeAvalantDesTempsDAtt_.length] = attente;
                    moyTemp += attente;
                    // ***s2.setDureeMoyAvalantAtt(moyTemp/tabTemp.length);
                    s2.setDureeTtlAvalantAtt(s2.getDureeTtlAvalantAtt() + attente);
                    s2.listeAvalantDesTempsDAtt_ = tabTemp;
                  } else {
                    s2.listeAvalantDesTempsDAtt_ = new double[1];
                    s2.listeAvalantDesTempsDAtt_[0] = attente;
                    s2.setDureeTtlAvalantAtt(attente);
                  }

                }
              } else {// pas d'attente
                if (s.avalantMontant_ == 'M') {
                  final double[] tabTemp;
                  s2.nbBatMontant_++;
                  /*****************************************************************************************************
                   * *if(s2.listeMontantDesTempsDAtt_!=null){ tabTemp=new double[s2.listeMontantDesTempsDAtt_.length+1];
                   * tabTemp[s2.listeMontantDesTempsDAtt_.length]=0; } else{ tabTemp=new double[1]; tabTemp[0]=0; }
                   * s2.listeMontantDesTempsDAtt_=tabTemp;
                   ****************************************************************************************************/
                  // est ce qu'il faut inclure dans la moyenne ceux qui n'ont pas attendus?
                } else {
                  final double[] tabTemp;
                  s2.nbBatAvalant_++;
                  /*****************************************************************************************************
                   * *if(s2.listeAvalantDesTempsDAtt_!=null){ tabTemp=new double[s2.listeAvalantDesTempsDAtt_.length+1];
                   * tabTemp[s2.listeAvalantDesTempsDAtt_.length]=0; } else{ tabTemp=new double[1]; tabTemp[0]=0; }
                   * s2.listeAvalantDesTempsDAtt_=tabTemp;
                   ****************************************************************************************************/
                }
              }
              // recherche de l'attente de la gare pr�c�dente

            }

            // attenteTemp.add(s2);
          }
          if (!trouve) {// creation du nouveau SinaviTypeAttente
            // System.out.println("ajout");
            final Sinavi2TypeAttente s2 = new Sinavi2TypeAttente();
            boolean nonGare = false;
            if (s.elementAFranchir_.charAt(0) == 'G') {
              // System.out.println("on veut pasl es gares");
              nonGare = true;
            } else if (s.elementAFranchir_.charAt(0) == 'B') {
              s2.setElement(((SParametresBief) (listeBiefs_.get(s.numeroElement_ - 1))).identification);
            } else if (s.elementAFranchir_.charAt(0) == 'O') {
              s2.setElement(((SParametresEcluse) (listeEcluses_.get(s.numeroElement_ - 1))).identification);
            }
            if (!nonGare) {
              s2.nbBatTotal_ = 1;
              s2.setTypeBateaux(((SParametresBateau) listeBateaux_.get(s.numeroTypeBateau_ - 1)).identification);
              s2.setTypeElement(s.elementAFranchir_.charAt(0));
              /** ************************* */
              double attente = s.attente_;
              if (s.elementAFranchir_.equalsIgnoreCase("B")) {
                if (s.avalantMontant_ == 'M') {
                  attente += rechercheAttenteGarePrec(i2, s.numeroBateau_, s.avalantMontant_,
                      ((SParametresBief) listeBiefs_.get(s.numeroElement_ - 1)).gareEnAval, tabSim[i]);
                } else {
                  attente += rechercheAttenteGarePrec(i2, s.numeroBateau_, s.avalantMontant_,
                      ((SParametresBief) listeBiefs_.get(s.numeroElement_ - 1)).gareEnAmont, tabSim[i]);
                }
              } else if (s.elementAFranchir_.equalsIgnoreCase("O")) {
                if (s.avalantMontant_ == 'M') {
                  attente += rechercheAttenteGarePrec(i2, s.numeroBateau_, s.avalantMontant_,
                      ((SParametresEcluse) listeEcluses_.get(s.numeroElement_ - 1)).gareEnAval, tabSim[i]);
                } else {
                  attente += rechercheAttenteGarePrec(i2, s.numeroBateau_, s.avalantMontant_,
                      ((SParametresEcluse) listeEcluses_.get(s.numeroElement_ - 1)).gareEnAmont, tabSim[i]);
                }
              }
              if (attente > 0) {
                if (s.avalantMontant_ == 'M') {
                  s2.nbBatMontantAtt_ = 1;
                  s2.nbBatMontant_ = 1;// ***
                  s2.dureeMoyMontantAtt_ = attente;
                  s2.setDureeMinMontantAtt(attente);
                  s2.setDureeMaxMontantAtt(attente);
                  s2.listeMontantDesTempsDAtt_ = new double[1];
                  s2.listeMontantDesTempsDAtt_[0] = attente;
                } else {
                  s2.nbBatAvalantAtt_ = 1;
                  s2.nbBatAvalant_ = 1;
                  s2.dureeMoyAvalantAtt_ = attente;
                  s2.setDureeMinAvalantAtt(attente);
                  s2.setDureeMaxAvalantAtt(attente);
                  s2.listeAvalantDesTempsDAtt_ = new double[1];
                  s2.listeAvalantDesTempsDAtt_[0] = attente;

                }
              } else {
                if (s.avalantMontant_ == 'M') {
                  s2.nbBatMontant_++;
                  /*****************************************************************************************************
                   * *s2.listeMontantDesTempsDAtt_=new double[1]; s2.listeMontantDesTempsDAtt_[0]=0;
                   ****************************************************************************************************/
                } else {
                  s2.nbBatAvalant_++;
                  /*****************************************************************************************************
                   * *s2.listeAvalantDesTempsDAtt_=new double[1]; s2.listeAvalantDesTempsDAtt_[0]=0;
                   ****************************************************************************************************/
                }
              }
              attenteTemp.add(s2);
            }
          }
        }

        listeAttentes_.add(attenteTemp);

      }
      System.out.println("test");
    }
    /*
     * ArrayList test=(ArrayList) listeAttentes_.get(0); ListIterator it=test.listIterator(); while(it.hasNext()){
     * SinaviTypeAttente test2=(SinaviTypeAttente)it.next(); System.out.println("-------"+test2.element_ +
     * CtuluLibString.ESPACE + test2.getNbBatTotal()); }
     */

  }

  public void donneesConsommation(boolean _comparaison) {
    if (!_comparaison && (getNumeroSimulationCourante() == listeSimulationsSinavi2_.size() - 1)) {
      message("Selectionnez une simulation dans le menu gestion de simulations");
    } else {
      listeCons_ = new ArrayList();

      int[] tabSim;
      if (!_comparaison) {

        tabSim = new int[1];
        tabSim[0] = getNumeroSimulationCourante();
        // tabSim[0]=0;
      } else {
        tabSim = simulationsSel_;
        // tabSim= new int[2];
      }
      for (int i = 0; i < tabSim.length; i++) {
        final SParametresEcluse[] tempEcluses = ((SSimulationSinavi2) listeSimulationsSinavi2_.get(tabSim[i])).parametresEcluse;
        final SResultatConsommationDEau[] tempConso = ((SSimulationSinavi2) listeSimulationsSinavi2_.get(tabSim[i])).resultatConsommationDEau;
        // System.out.println("taille de tempConsommation" + tempConso.length);

        final ArrayList consoTemp = new ArrayList();
        for (int j = 1; j < /* listeEcluseCourant.size() */tempEcluses.length + 1; j++) {
          final ArrayList listetemp = new ArrayList();
          final ArrayList listeHeureFermetureAvalant = new ArrayList();
          final ArrayList listeHeureFermetureMontant = new ArrayList();

          final Sinavi2TypeConsommation cons2 = new Sinavi2TypeConsommation();
          for (int f = 0; f < tempConso.length; f++) {
            // if(/*j==((SResultatConsommationDEau)((ArrayList)listeConsommation_.get(i)).get(f)).numeroEcluse){
            if (j == tempConso[f].numeroEcluse) {
              // listetemp.add( ((ArrayList)listeConsommation_.get(i)).get(f) );
              listetemp.add(tempConso[f]);
            }
          }

          final ListIterator iter = listetemp.listIterator();
          while (iter.hasNext()) {
            final SResultatConsommationDEau cons3 = (SResultatConsommationDEau) iter.next();
            final Sinavi2TypeConsommationDEau cons = new Sinavi2TypeConsommationDEau(cons3);
            System.out.println("num" + cons.numeroEcluse_);

            if (cons.getAvalantMontant() == 'M') {

              if (cons.getNbBateaux() == 0 && cons.getTypeBateau() == 0) {
                cons2.nbFaussesBassineesMontantes_++;
              }

              else if (cons.getNbBateaux() != 0 /* && HeureFermetureMontant!=cons.dateHeureFermeture_ */) {
                System.out.println("consDateHeureFermeture Montant " + cons.dateHeureFermeture_);
                if (!listeHeureFermetureMontant.contains(String.valueOf(cons.dateHeureFermeture_))) {
                  listeHeureFermetureMontant.add("" + cons.dateHeureFermeture_);
                }
              }

            }

            else {

              if (cons.getNbBateaux() == 0 && cons.getTypeBateau() == 0) {
                cons2.nbFaussesBassineesAvalantes_++;
              }

              else if (cons.getNbBateaux() != 0 /* && HeureFermetureAvalant!=cons.dateHeureFermeture_ */) {
                System.out.println("consDateHeureFermeture Avalant " + cons.dateHeureFermeture_);
                if (!listeHeureFermetureAvalant.contains(String.valueOf(cons.dateHeureFermeture_))) {
                  listeHeureFermetureAvalant.add("" + cons.dateHeureFermeture_);
                }
              }
            }
          }

          System.out.println("taille de heure fermeture montant" + listeHeureFermetureMontant.size());
          System.out.println("taille de heure fermeture avalant" + listeHeureFermetureAvalant.size());

          cons2.setNbBassineesMontantes(listeHeureFermetureMontant.size());
          cons2.setNbBassineesAvalantes(listeHeureFermetureAvalant.size());
          cons2.setNbTotalBassinees(cons2.nbBassineesMontantes_ + cons2.nbBassineesAvalantes_);
          cons2.setNbTotalFaussesBassinees(cons2.nbFaussesBassineesAvalantes_ + cons2.nbFaussesBassineesMontantes_);
          cons2.setEcluse(tempEcluses[j - 1].identification);
          cons2.setConsommationDEauAvalant(tempEcluses[j - 1].hauteurChuteDEau * tempEcluses[j - 1].largeur
              * tempEcluses[j - 1].longueur * (cons2.nbBassineesAvalantes_ + cons2.nbFaussesBassineesAvalantes_));

          cons2.setConsommationDEauMontant(tempEcluses[j - 1].hauteurChuteDEau * tempEcluses[j - 1].largeur
              * tempEcluses[j - 1].longueur * (cons2.nbBassineesMontantes_ + cons2.nbFaussesBassineesMontantes_));

          cons2.setConsommationDEau(cons2.consommationDEauAvalant_ + cons2.consommationDEauMontant_);

          System.out.println(" valeur de l'ecluse" + cons2.ecluse_);
          consoTemp.add(cons2);

        }
        listeCons_.add(consoTemp);

      }
      System.out.println(" la taille de listeCons" + listeCons_.size());
    }
  }

  public File enregistrerXls() {
    return CtuluLibFile.appendExtensionIfNeeded(super.ouvrirFileChooser("Fichier Excel", new String[] { "xls,Xls" },
        true), "xls");
  }

  /**
   * methode qui rend visible la fen�tre de saisie du r�seau.
   */

  public void exit() {
    // fermer();
    if (projet_ != null) {
      final BuDialogConfirmation conf = new BuDialogConfirmation(getApp(), InfoSoftSinavi2_,
          "Voulez-vous sauvegarder les modifications ?");
      if (conf.activate() == 0) {
        this.enregistrer();
        affMessage("Les donn�es sont sauvegard�es. A bient�t !");
      } else {
        affMessage("Les derni�res modifications ne sont pas sauvegard�es. A bient�t !");
      }
    }

    super.exit();
  }

  public void genererBateaux() {
    // param_ =new DParametresSinavi2();
    try {
      // param_ = IParametresSinavi2Helper.narrow (SERVEUR_SINAVI2.parametres(CONNEXION_SINAVI2) );
      // remplirParametres();
      // siporResults_ = IResultatsSiporHelper.narrow (SERVEUR_SIPOR.resultats() );
    } catch (final NullPointerException e) {
      System.err.println("Pas de connexion serveur => local");
      affMessage("Vous n'etes pas connect� � un serveur SINAVI2 \nVous ne pourrez pas lancer de simulation");
    }
    // param_.parametresGeneration(gen_);
    // test("bateaux g�n�r�s");
    lancer();
  }

  public BuPreferences getApplicationPreferences() {
    return Sinavi2Preferences.SINAVI2;
  }

  public FudaaAstucesAbstract getAstuces() {
    return Sinavi2Astuces.SINAVI2;
  }

  public BuInformationsDocument getInformationsDocument() {
    return InfoDocsSinavi2_;
  }

  public BuInformationsSoftware getInformationsSoftware() {
    return InfoSoftSinavi2_;
  }

  public String getNomPrj() {
    return nomPrj_;

  }

  public int getNumeroSimulationCourante() {
    return simulationCourante_;
  }

  public void importer(final String[] _ext) {
    final BuFileChooser chooser = new BuFileChooser();
    chooser.setFileFilter(new BuFileFilter(_ext, "Fichiers " + _ext[1]));
    final int returnVal = chooser.showOpenDialog((JFrame) getApp());
    if (returnVal == JFileChooser.APPROVE_OPTION) {
      final String filename = chooser.getSelectedFile().getAbsolutePath();
      if (_ext[0].equals("nav")) {
        SParametresBateau[] bateaux;
        bateaux = Sinavi2Readers.lireFichierBateauxImport(filename);
        int compteur = 0;
        if (listeBateaux_ == null) {
          listeBateaux_ = new ArrayList();
          for (int i = 0; i < bateaux.length; i++) {
            listeBateaux_.add(bateaux[i]);
          }
        } else {
          while (compteur < bateaux.length) {
            final int x = rechercherBateau(bateaux[compteur].identification);
            if (x == -1) {
              listeBateaux_.add(bateaux[compteur]);
              // compteur++;
            } else {
              SParametresBateau bat = new SParametresBateau();
              bat = (SParametresBateau) listeBateaux_.get(x);
              final BuDialogChoice choix = new BuDialogChoice(getApp(), InfoSoftSinavi2_, "Importation de bateaux",
                  "Le bateau " + bat.identification + " existe d�j�." + " Que voulez-vous faire ?",
                  createDialogOptions(), null, false, null);
              if (choix.activate() == 0) {
                final String choixcourant = choix.getValue();
                if (choixcourant.equalsIgnoreCase("Ignorer")) {
                  // compteur++;
                } else if (choixcourant.equalsIgnoreCase("Renommer")) {
                  boolean trouve = false;
                  String nom = "";
                  while (!trouve) {
                    final BuDialogInput mod = new BuDialogInput(getApp(), InfoSoftSinavi2_,
                        "Type de bateau � modifier ", getStrIdent(), bat.identification);
                    if (mod.activate() == 0) {
                      nom = mod.getValue();
                      final char lastChar = nom.charAt(nom.length() - 1);
                      if (lastChar != 'C' && lastChar != 'L') {
                        affMessage("Terminez par L si le bateau est L�ge \n et C si le bateau est Charg�.");
                      } else {
                        final int batExist = rechercherBateau(nom);
                        if (batExist == -1) {
                          trouve = true;
                          bateaux[x].identification = nom;
                          listeBateaux_.add(bateaux[x]);
                          affMessage("Le bateau " + bat.identification + " est renomm� en " + nom + CtuluLibString.DOT);
                        } else {
                          affMessage("Le bateau " + nom + " existe d�j�.");
                        }
                      }
                    } else {
                      trouve = true;
                      // compteur--;
                    }

                  }

                  // compteur++;
                } else if (choixcourant.equalsIgnoreCase("Ecraser")) {
                  // String ancienbateau=bateaux[x].identification;
                  // listeBateaux_.remove(x);
                  listeBateaux_.set(x, bateaux[compteur]);
                  affMessage("le bateau " + bat.identification + " est �cras�.");
                  // compteur++;
                }
              }
            }
            compteur++;
          }
        }
      }
      if (_ext[0].equals("bie")) {
        SParametresBief[] biefs;
        biefs = Sinavi2Readers.lireFichierBiefsImport(filename);
        int compteur = 0;
        if (listeBiefs_ == null) {
          listeBiefs_ = new ArrayList();
          for (int i = 0; i < biefs.length; i++) {
            System.out.println("ajout du biefs " + biefs[i].identification);
            definirIndisponibilites(biefs[i], null);
            listeBiefs_.add(biefs[i]);
          }
        } else {
          while (compteur < biefs.length) {
            final int x = rechercherBief(biefs[compteur].identification);
            if (x == -1) {
              definirIndisponibilites(biefs[compteur], null);
              listeBiefs_.add(biefs[compteur]);
              // compteur++;
            } else {
              SParametresBief bief = new SParametresBief();
              bief = (SParametresBief) listeBiefs_.get(x);
              final BuDialogChoice choix = new BuDialogChoice(getApp(), InfoSoftSinavi2_, "Importation de biefs",
                  "Le bief " + bief.identification + " existe d�j�." + " Que voulez-vous faire?",
                  createDialogOptions(), null, false, null);
              if (choix.activate() == 0) {
                final String choixcourant = choix.getValue();
                if (choixcourant.equalsIgnoreCase("Ignorer")) {
                  // compteur++;
                } else if (choixcourant.equalsIgnoreCase("Renommer")) {
                  boolean trouve = false;
                  String nom = "";
                  while (!trouve) {
                    final BuDialogInput mod = new BuDialogInput(getApp(), InfoSoftSinavi2_, "Bief � modifier ",
                        getStrIdent(), bief.identification);
                    if (mod.activate() == 0) {
                      nom = mod.getValue();
                      final int biefExist = rechercherBief(nom);
                      if (biefExist == -1) {
                        trouve = true;
                        biefs[x].identification = nom;
                        definirIndisponibilites(biefs[x], null);
                        listeBiefs_.add(biefs[x]);
                        affMessage("Le bief " + bief.identification + " est renomm� en " + nom + CtuluLibString.DOT);
                      } else {
                        affMessage("Le bief " + nom + " existe d�j�.");
                      }

                    } else {
                      trouve = true;
                      // compteur--;
                    }

                  }

                } else if (choixcourant.equalsIgnoreCase("Ecraser")) {
                  // listeBiefs_.remove(x);
                  definirIndisponibilites(biefs[compteur], null);
                  listeBiefs_.set(x, biefs[compteur]);
                  affMessage("Le bief " + bief.identification + " est �cras�.");
                  // compteur++;
                }
              }
            }
            compteur++;
          }
        }
      }
      if (_ext[0].equals("ouv")) {
        SParametresEcluse[] ecluses;
        ecluses = Sinavi2Readers.lireFichierEclusesImport(filename);
        int compteur = 0;
        if (listeEcluses_ == null) {
          listeEcluses_ = new ArrayList(Arrays.asList(ecluses));
        } else {
          while (compteur < ecluses.length) {
            final int x = rechercherEcluse(ecluses[compteur].identification);
            if (x == -1) {
              definirIndisponibilites(null, ecluses[compteur]);
              listeEcluses_.add(ecluses[compteur]);
              // compteur++;
            } else {
              SParametresEcluse ecluse = new SParametresEcluse();
              ecluse = (SParametresEcluse) listeEcluses_.get(x);
              final BuDialogChoice choix = new BuDialogChoice(getApp(), InfoSoftSinavi2_, "Importation d'�cluses",
                  "L'�cluse " + ecluse.identification + " existe d�j�." + " Que voulez-vous faire?",
                  createDialogOptions(), null, false, null);
              if (choix.activate() == 0) {
                final String choixcourant = choix.getValue();
                if (choixcourant.equalsIgnoreCase("Ignorer")) {
                  // compteur++;
                } else if (choixcourant.equalsIgnoreCase("Renommer")) {
                  boolean trouve = false;
                  String nom = "";
                  while (!trouve) {
                    final BuDialogInput mod = new BuDialogInput(getApp(), InfoSoftSinavi2_, "Ecluse � modifier ",
                        getStrIdent(), ecluse.identification);
                    if (mod.activate() == 0) {
                      nom = mod.getValue();
                      final int ecluseExist = rechercherEcluse(nom);
                      if (ecluseExist == -1) {
                        trouve = true;
                        ecluses[x].identification = nom;
                        definirIndisponibilites(null, ecluses[x]);
                        listeEcluses_.add(ecluses[x]);
                        affMessage("L'�cluse " + ecluse.identification + " est renomm�e en " + nom + CtuluLibString.DOT);
                      } else {
                        affMessage("L'�cluse " + nom + " existe d�j�.");
                      }

                    } else {
                      trouve = true;
                      // compteur--;
                    }

                  }

                } else if (choixcourant.equalsIgnoreCase("Ecraser")) {
                  // listeBiefs_.remove(x);
                  definirIndisponibilites(null, ecluses[compteur]);
                  listeEcluses_.set(x, ecluses[compteur]);
                  affMessage("L'�cluse " + ecluse.identification + " est �cras�e.");
                  // compteur++;
                }
              }
            }
            compteur++;
          }
        }
      }
      if (_ext[0].equals("res")) {
        // SParametresEcluse[] eclusesReseau=new SParametresEcluse[100];
        // SParametresBief[] biefsReseau=new SParametresBief[100] ;
        final ArrayList listeBiefsTemp = new ArrayList();
        final ArrayList listeEclusesTemp = new ArrayList();
        Sinavi2Readers.lireFichierReseauImport(filename, listeBiefsTemp, listeEclusesTemp);
        if (listeBiefs_ == null) {
          listeBiefs_ = new ArrayList();
          // System.out.println("taille imple bief "+listeBiefsTemp.size());
          final ListIterator it = listeBiefsTemp.listIterator();
          while (it.hasNext()) {
            final SParametresBief b = (SParametresBief) it.next();
            definirIndisponibilites(b, null);
            listeBiefs_.add(b);
          }
        } else {
          SParametresBief[] biefs;
          biefs = (SParametresBief[]) listeBiefsTemp.toArray(new SParametresBief[listeBiefsTemp.size()]);
          int compteur = 0;
          if (listeBiefs_ == null) {
            listeBiefs_ = new ArrayList();
            for (int i = 0; i < biefs.length; i++) {
              definirIndisponibilites(biefs[i], null);
              listeBiefs_.add(biefs[i]);
            }
          } else {
            while (compteur < biefs.length) {
              final int x = rechercherBief(biefs[compteur].identification);
              if (x == -1) {
                definirIndisponibilites(biefs[compteur], null);
                listeBiefs_.add(biefs[compteur]);
                compteur++;
              } else {
                SParametresBief bief = new SParametresBief();
                bief = (SParametresBief) listeBiefs_.get(x);
                final BuDialogChoice choix = new BuDialogChoice(getApp(), InfoSoftSinavi2_, "Importation de biefs",
                    "Le bief " + bief.identification + " existe d�j�." + " Que voulez-vous faire?",
                    createDialogOptions(), null, false, null);
                if (choix.activate() == 0) {
                  final String choixcourant = choix.getValue();
                  if (choixcourant.equalsIgnoreCase("Ignorer")) {
                    // compteur++;
                  } else if (choixcourant.equalsIgnoreCase("Renommer")) {
                    boolean trouve = false;
                    String nom = "";
                    while (!trouve) {
                      final BuDialogInput mod = new BuDialogInput(getApp(), InfoSoftSinavi2_, "Bief � modifier ",
                          getStrIdent(), bief.identification);
                      if (mod.activate() == 0) {
                        nom = mod.getValue();
                        final int biefExist = rechercherBief(nom);
                        if (biefExist == -1) {
                          trouve = true;
                          biefs[x].identification = nom;
                          definirIndisponibilites(biefs[x], null);
                          listeBiefs_.add(biefs[x]);

                          affMessage("Le bief " + bief.identification + " est renomm� en " + nom + CtuluLibString.DOT);
                        } else {
                          affMessage("Le bief " + nom + " existe d�j�.");
                        }

                      } else {
                        trouve = true;
                        // compteur--;
                      }

                    }

                  } else if (choixcourant.equalsIgnoreCase("Ecraser")) {
                    // listeBiefs_.remove(x);
                    definirIndisponibilites(biefs[compteur], null);
                    listeBiefs_.set(x, biefs[compteur]);

                    affMessage("Le bief " + bief.identification + " est �cras�.");
                    // compteur++;
                  }
                }
              }
              compteur++;
            }
          }
        }
        if (listeEcluses_ == null) {
          listeEcluses_ = new ArrayList();
          // System.out.println("taille imple bief "+listeBiefsTemp.size());
          final ListIterator it = listeEclusesTemp.listIterator();
          while (it.hasNext()) {
            final SParametresEcluse e = (SParametresEcluse) it.next();
            definirIndisponibilites(null, e);
            listeEcluses_.add(e);
          }
          /*
           * for(int i=0; i<listeBiefsTemp.size(); i++){ listeBiefs_.add(listeBiefsTemp[i]); }
           */
        } else {
          SParametresEcluse[] ecluses;
          ecluses = (SParametresEcluse[]) listeEclusesTemp.toArray(new SParametresEcluse[listeEclusesTemp.size()]);
          int compteur = 0;
          if (listeEcluses_ == null) {
            listeEcluses_ = new ArrayList();
            for (int i = 0; i < ecluses.length; i++) {
              definirIndisponibilites(null, ecluses[i]);
              listeEcluses_.add(ecluses[i]);
            }
          } else {
            while (compteur < ecluses.length) {
              final int x = rechercherEcluse(ecluses[compteur].identification);
              if (x == -1) {
                definirIndisponibilites(null, ecluses[compteur]);
                listeEcluses_.add(ecluses[compteur]);
                // compteur++;
              } else {
                SParametresEcluse ecluse = new SParametresEcluse();
                ecluse = (SParametresEcluse) listeEcluses_.get(x);
                final BuDialogChoice choix = new BuDialogChoice(getApp(), InfoSoftSinavi2_, "Importation d'�cluses",
                    "L'�cluse " + ecluse.identification + " existe d�j�." + " Que voulez-vous faire?",
                    createDialogOptions(), null, false, null);
                if (choix.activate() == 0) {
                  final String choixcourant = choix.getValue();
                  if (choixcourant.equalsIgnoreCase("Ignorer")) {
                    // compteur++;
                  } else if (choixcourant.equalsIgnoreCase("Renommer")) {
                    boolean trouve = false;
                    String nom = "";
                    while (!trouve) {
                      final BuDialogInput mod = new BuDialogInput(getApp(), InfoSoftSinavi2_, "Ecluse � modifier ",
                          getStrIdent(), ecluse.identification);
                      if (mod.activate() == 0) {
                        nom = mod.getValue();
                        final int ecluseExist = rechercherEcluse(nom);
                        if (ecluseExist == -1) {
                          trouve = true;
                          ecluses[x].identification = nom;
                          definirIndisponibilites(null, ecluses[x]);
                          listeEcluses_.add(ecluses[x]);
                          affMessage("L'�cluse " + ecluse.identification + " est renomm� en " + nom
                              + CtuluLibString.DOT);
                        } else {
                          affMessage("L'�cluse " + nom + " existe d�j�.");
                        }

                      } else {
                        trouve = true;
                        // compteur--;
                      }

                    }

                  } else if (choixcourant.equalsIgnoreCase("Ecraser")) {
                    // listeBiefs_.remove(x);
                    definirIndisponibilites(null, ecluses[compteur]);
                    listeEcluses_.set(x, ecluses[compteur]);
                    affMessage("L'�cluse " + ecluse.identification + " est �cras�.");
                    // compteur++;
                  }
                }
              }
              compteur++;
            }
          }
        }
      }

      // System.err.println("obj:" + obj);
      new BuDialogMessage(getApp(), InfoSoftSinavi2_, "Param�tres " + _ext[0] + " charg�.").activate();
    }
  }

  /** *Prend en parametre un tableau d'extensions de fichier: ex: ["nav2","NAV2"]. */
  public void importer2(final String[] _ext) {
    final BuFileChooser chooser = new BuFileChooser();
    chooser.setFileFilter(new BuFileFilter(_ext, "Fichiers " + _ext[1]));
    final int returnVal = chooser.showOpenDialog((JFrame) getApp());
    if (returnVal == JFileChooser.APPROVE_OPTION) {
      final Object obj = null;
      final String filename = chooser.getSelectedFile().getAbsolutePath();

      if (_ext[0].equals("nav2")) {
        SParametresBateau[] bateaux;
        bateaux = Sinavi2Readers.lireFichierBateauxImport2(filename);
        int compteur = 0;
        if (listeBateaux_ == null) {
          listeBateaux_ = new ArrayList();
          for (int i = 0; i < bateaux.length; i++) {
            listeBateaux_.add(bateaux[i]);
          }
        }

        else {
          while (compteur < bateaux.length) {
            final int x = rechercherBateau(bateaux[compteur].identification);
            if (x == -1) {
              listeBateaux_.add(bateaux[compteur]);
              // compteur++;
            } else {
              SParametresBateau bat = new SParametresBateau();
              bat = (SParametresBateau) listeBateaux_.get(x);
              final BuDialogChoice choix = new BuDialogChoice(getApp(), InfoSoftSinavi2_, "Importation de bateaux",
                  "Le bateau " + bat.identification + " existe d�j�." + " Que voulez-vous faire ?",
                  createDialogOptions(), null, false, null);
              if (choix.activate() == 0) {
                final String choixcourant = choix.getValue();
                if (choixcourant.equalsIgnoreCase("Ignorer")) {
                  // compteur++;
                } else if (choixcourant.equalsIgnoreCase("Renommer")) {
                  boolean trouve = false;
                  String nom = "";
                  while (!trouve) {
                    final BuDialogInput mod = new BuDialogInput(getApp(), InfoSoftSinavi2_,
                        "Type de bateau � modifier ", getStrIdent(), bat.identification);
                    if (mod.activate() == 0) {
                      nom = mod.getValue();
                      final char lastChar = nom.charAt(nom.length() - 1);
                      if (lastChar != 'C' && lastChar != 'L') {
                        affMessage("Terminez par L si le bateau est L�ge \n et C si le bateau est Charg�.");
                      } else {
                        final int batExist = rechercherBateau(nom);
                        if (batExist == -1) {
                          trouve = true;
                          bateaux[x].identification = nom;
                          listeBateaux_.add(bateaux[x]);
                          affMessage("Le bateau " + bat.identification + " est renomm� en " + nom + CtuluLibString.DOT);
                        } else {
                          affMessage("Le bateau " + nom + " existe d�j�.");
                        }
                      }
                    } else {
                      trouve = true;
                      // compteur--;
                    }

                  }

                  // compteur++;
                } else if (choixcourant.equalsIgnoreCase("Ecraser")) {
                  // String ancienbateau=bateaux[x].identification;
                  // listeBateaux_.remove(x);
                  listeBateaux_.set(x, bateaux[compteur]);
                  affMessage("le bateau " + bat.identification + " est �cras�.");
                  // compteur++;
                }
              }
            }
            compteur++;
          }
        }
      }

      if (_ext[0].equals("ouv2")) {
        SParametresEcluse[] ecluses;
        int compteur = 0;
        ecluses = Sinavi2Readers.lireFichierEclusesImport2(filename);
        if (listeEcluses_ == null) {
          listeEcluses_ = new ArrayList();
          for (int i = 0; i < ecluses.length; i++) {
            definirIndisponibilites(null, ecluses[i]);
            listeEcluses_.add(ecluses[i]);
          }
        } else {
          while (compteur < ecluses.length) {
            final int x = rechercherEcluse(ecluses[compteur].identification);
            if (x == -1) {
              definirIndisponibilites(null, ecluses[compteur]);
              listeEcluses_.add(ecluses[compteur]);
              // compteur++;
            } else {
              SParametresEcluse ecluse = new SParametresEcluse();
              ecluse = (SParametresEcluse) listeEcluses_.get(x);
              final BuDialogChoice choix = new BuDialogChoice(getApp(), InfoSoftSinavi2_, "Importation d'�cluses",
                  "L'�cluse " + ecluse.identification + " existe d�j�." + " Que voulez-vous faire?",
                  createDialogOptions(), null, false, null);
              if (choix.activate() == 0) {
                final String choixcourant = choix.getValue();
                if (choixcourant.equalsIgnoreCase("Ignorer")) {
                  // compteur++;
                } else if (choixcourant.equalsIgnoreCase("Renommer")) {
                  boolean trouve = false;
                  String nom = "";
                  while (!trouve) {
                    final BuDialogInput mod = new BuDialogInput(getApp(), InfoSoftSinavi2_, "Ecluse � modifier ",
                        getStrIdent(), ecluse.identification);
                    if (mod.activate() == 0) {
                      nom = mod.getValue();
                      final int ecluseExist = rechercherEcluse(nom);
                      if (ecluseExist == -1) {
                        trouve = true;
                        ecluses[x].identification = nom;
                        definirIndisponibilites(null, ecluses[x]);
                        listeEcluses_.add(ecluses[x]);
                        affMessage("L'�cluse " + ecluse.identification + " est renomm�e en " + nom + CtuluLibString.DOT);
                      } else {
                        affMessage("L'�cluse " + nom + " existe d�j�.");
                      }

                    } else {
                      trouve = true;
                      // compteur--;
                    }

                  }

                } else if (choixcourant.equalsIgnoreCase("Ecraser")) {
                  // listeBiefs_.remove(x);
                  definirIndisponibilites(null, ecluses[compteur]);
                  listeEcluses_.set(x, ecluses[compteur]);
                  affMessage("L'�cluse " + ecluse.identification + " est �cras�e.");
                  // compteur++;
                }
              }
            }
            compteur++;
          }
        }
      }

      System.err.println("obj:" + obj);
      new BuDialogMessage(getApp(), InfoSoftSinavi2_, "Param�tres " + _ext[0] + " charg�").activate();
    }
  }

  /**
   * methode init obligatoire ( initialise les objets java avant l'affichage du bureau ).
   */
  public void init() {
    super.init();

    sinavi2filleaddmodbateaux_ = null;
    sinavi2filleaffbateaux_ = null;
    sinavi2filleaddmodbiefs_ = null;
    sinavi2filleaffbiefs_ = null;
    sinavi2filleaddmodecluses_ = null;
    sinavi2filleaffecluses_ = null;
    sinavi2filleaffcroisements_ = null;
    sinavi2filleafftrematages_ = null;
    sinavi2filleaffvitesses_ = null;
    sinavi2fillemodvitesses_ = null;
    sinavi2filleaffmanoeuvres_ = null;
    sinavi2fillemodmanoeuvres_ = null;
    sinavi2filleaddmodconnexions_ = null;
    sinavi2filleaffconnexions_ = null;
    sinavi2filletirage_ = null;
    sinavi2filleaffgeneration_ = null;
    sinavi2filleaffhistorique_ = null;
    aide_ = null;
    // /sinavireseauframe_ = null;
    // sinavi2gestionprojet_ = null;
    // /sinavifillechoixrappeldonnees_ = null;
    // /sinavifillechoixresultats_ = null;
    // enregistrement des trois classes utilise dans la grille dja pour la sauvegarde �ventuelle avec Dja
    // / DjaRegistry.register("gare", SinaviReseauGare.class);
    // /DjaRegistry.register("ecluse", SinaviReseauEcluse.class);
    // /DjaRegistry.register("bief", SinaviReseauBief.class);
    try {
      setTitle(InfoSoftSinavi2_.name + CtuluLibString.ESPACE + InfoSoftSinavi2_.version);
      final BuMenuBar mb = getMainMenuBar();
      final BuMenu mi = (BuMenu) mb.getMenu("IMPORTER");
      final BuMenu me = (BuMenu) mb.getMenu("EXPORTER");
      final BuMenu impbateau = new BuMenu("Bateau", "IMPBATEAU", false);
      final BuMenu impecluse = new BuMenu("ecluse", "IMPECLUSE", false);
      final BuMenu expbateau = new BuMenu("Bateau", "EXPBATEAU", false);
      final BuMenu expecluse = new BuMenu("ecluse", "EXPECLUSE", false);
      impbateau.addMenuItem("(*.nav)", getActImpNav(), false);
      impbateau.addMenuItem("(*.nav2)", "IMPORTERNAV2", false);
      impecluse.addMenuItem("(*.ouv)", getActImportOuv(), false);
      impecluse.addMenuItem("(*.ouv2)", "IMPORTEROUV2", false);
      mi.addSubMenu(impbateau, false);
      mi.addMenuItem("Bief (*.bie)", getActImpBief(), false);
      mi.addSubMenu(impecluse, false);
      mi.addMenuItem("Reseau(*.res)", "IMPORTERRESEAU", false);
      expbateau.addMenuItem("(*.nav)", "EXPORTERNAV", false);
      expbateau.addMenuItem("(*.nav2)", "EXPORTERNAV2", false);
      expecluse.addMenuItem("(*.ouv)", "EXPORTEROUV", false);
      expecluse.addMenuItem("(*.ouv2)", "EXPORTEROUV2", false);
      me.addMenuItem("Tous les fichiers", "EXPORTALL");
      me.addSubMenu(expbateau, false);
      me.addMenuItem("Bief ", "EXPORTERBIE", false);
      me.addSubMenu(expecluse, false);
      me.addMenuItem("Reseau(*.res)", "EXPORTERRESEAU", false);

      // mb.addActionListener(this);
      // /mb.addMenu(construitMenuSimulation());
      // /mb.addMenu(construitMenuProjets());
      mb.addMenu(construitMenuDonnees());
      mb.addMenu(construitMenuConstitution());
      mb.addMenu(construitMenuFonctionnement());
      mb.addMenu(construitMenuSimulation());
      mb.addMenu(construitMenuResultats());

      final BuToolBar tb = getMainToolBar();
      tb.addSeparator();

      /** *************** */
      setEnabledForAction("CONSOLE", true);

      setEnabledForAction("CREER", true);
      setEnabledForAction("OUVRIR", true);
      setEnabledForAction("REOUVRIR", true);
      setEnabledForAction("ENREGISTRER", false);
      setEnabledForAction("ENREGISTRERSOUS", false);
      setEnabledForAction("FERMER", true);
      setEnabledForAction("QUITTER", true);
      setEnabledForAction("IMPORTER", true);
      setEnabledForAction("EXPORTER", true);
      setEnabledForAction("IMPRIMER", false);
      setEnabledForAction("PREFERENCE", true);
      setEnabledForAction("AIDE_INDEX", true);
      setEnabledForAction("AIDE_ASSISTANT", true);
      setEnabledForAction("TABLEAU", false);
      setEnabledForAction("GRAPHE", false);
      final BuMenuRecentFiles mr = (BuMenuRecentFiles) mb.getMenu("REOUVRIR");
      if (mr != null) {
        mr.setPreferences(Sinavi2Preferences.SINAVI2);
        mr.setResource(Sinavi2Resource.SINAVI2);
        mr.setEnabled(true);
      }
      assistant_ = new BuAssistant();
      taches_ = new BuTaskView();
      glassStop_ = new BuGlassPaneStop();
      glassStop_.setVisible(false);
      getRootPane().setGlassPane(glassStop_);
      final BuScrollPane sp1 = new BuScrollPane(taches_);
      sp1.setPreferredSize(new Dimension(150, 80));
      getMainPanel().getRightColumn().addToggledComponent("Assistant", "ASSISTANT", assistant_, this);
      getMainPanel().getRightColumn().addToggledComponent(BuResource.BU.getString("T�ches"), "TACHE", sp1, this);
      getMainPanel().setTaskView(taches_);
    } catch (final Throwable t) {
      System.err.println("$$$ " + t);
    }
  }

  public void initialiserTirage() {
    if (gen_ == null) {

      gen_ = new SParametresGeneration();
    }

    try {
      if (sinavi2filletirage_ == null) {
        sinavi2filletirage_ = new Sinavi2FilleInitialiserTirage(this, gen_);
        sinavi2filletirage_.setVisible(true);
      }

      else if (sinavi2filletirage_.isClosed()) {
        addInternalFrame(sinavi2filletirage_);
      } else {
        activateInternalFrame(sinavi2filletirage_);
      }

    } catch (final Exception e) {
      affMessage("V�rifiez les param�tres que vous avez entr� \n Donn�es du Mod�le ou sch�ma du r�seau !");
    }

  }

  public boolean isCloseFrameMode() {
    return false;
  }

  public boolean isPermettreModif() {
    return permettreModif_;
  }

  public void menuCroisements(final String _bief) {

    // test de la g�n�ration des fichiers
    // activer le menu que quand les premieres �tapes sont effectu�es donc peut pas besoin de test
    try {
      // if(sinavi2filleaffcroisements_ == null){
      if (listeCroisements_ == null) {
        listeCroisements_ = Sinavi2OutilsDonnees.creationListeCroisements_(listeBiefs_, listeBateaux_);
        System.out.println("null");
      }

      else {
        listeCroisements_ = Sinavi2OutilsDonnees.creationListeCroisements_(listeBiefs_, listeBateaux_,
            listeCroisements_);
        System.out.println("XXXX");

      }

      sinavi2filleaffcroisements_ = new Sinavi2FilleAffCroisements(this, listeBiefs_, listeCroisements_, listeBateaux_,
          _bief);
      sinavi2filleaffcroisements_.setVisible(true);
      addInternalFrame(sinavi2filleaffcroisements_);

    } catch (final NullPointerException e) {
      JOptionPane.showMessageDialog(null, "Il faut au moins un bateau et un bief !!!");
    }

  }

  public void menuManoeuvres(final int _ecluse, final int _typeBateau) {
    try {
      listeManoeuvres_ = Sinavi2OutilsDonnees.creationListeManoeuvres(listeBateaux_, listeEcluses_, listeManoeuvres_);
      sinavi2filleaffmanoeuvres_ = new Sinavi2FilleAffManoeuvres(this, listeManoeuvres_, _ecluse, _typeBateau);
      sinavi2filleaffmanoeuvres_.setVisible(true);
      addInternalFrame(sinavi2filleaffmanoeuvres_);

    } catch (final NullPointerException e) {

      JOptionPane.showMessageDialog(null, "Il faut au moins un bateau et une �cluse !!!");
    }
  }

  public void menuTrematages(final String _trem) {
    try {
      // if(sinavi2filleaffcroisements_ == null){
      if (listeTrematages_ == null) {
        listeTrematages_ = Sinavi2OutilsDonnees.creationListeTrematages(listeBiefs_, listeBateaux_);
      }

      else {
        listeTrematages_ = Sinavi2OutilsDonnees.creationListeTrematages_(listeBiefs_, listeBateaux_, listeTrematages_);

      }

      sinavi2filleafftrematages_ = new Sinavi2FilleAffTrematages(this, listeBiefs_, listeTrematages_, listeBateaux_,
          _trem);
      sinavi2filleafftrematages_.setVisible(true);
      addInternalFrame(sinavi2filleafftrematages_);

    } catch (final NullPointerException e) {

      JOptionPane.showMessageDialog(null, "Il faut au moins un bateau et un bief !!!");
    }
  }

  public void menuVitesses(final int _bief, final int _bateau) {
    try {
      listeVitesses_ = Sinavi2OutilsDonnees.creationListeVitesses(listeBateaux_, listeBiefs_, listeVitesses_);
      sinavi2filleaffvitesses_ = new Sinavi2FilleAffVitesses(this, listeVitesses_, _bief, _bateau);
      sinavi2filleaffvitesses_.setVisible(true);
      addInternalFrame(sinavi2filleaffvitesses_);

    } catch (final NullPointerException e) {

      JOptionPane.showMessageDialog(null, "Il faut au moins un bateau et un bief !!!");
    }
    /* } */

  }

  public void modifierVitesse(final String _bateauCourant, final String _biefCourant, final Sinavi2TableauVitesse _tb) {
    sinavi2fillemodvitesses_ = new Sinavi2FilleModVitesses(this, listeVitesses_, _bateauCourant, _biefCourant, _tb);
    // addInternalFrame(sinavi2fillemodvitesses_);
    sinavi2fillemodvitesses_.setVisible(true);
  }

  public void oprCalculer() {
    final String calcul = "Calcul";
    message(calcul, "Transmission des parametres...", true);
    // Erreur et retour si aucun serveur sipor connect�
    if (!isConnected()) {
      new BuDialogError(getApp(), InfoSoftSinavi2_, "vous n'etes pas connect� � un serveur sinavi ! ").activate();
      return;
    }
    // Messages et initialisatio des barres de progression.
    setEnabledForAction("CALCULER", false);
    message(calcul, "Calcul pass� en t�che de fond", true);
    final CtuluTaskDelegate task = createTask("Execution du calcul");
    task.start(new Runnable() {
      public void run() {
        try {
          SERVEUR_SINAVI2.calcul(CONNEXION_SINAVI2);

          // recevoirResultats();
        } catch (final org.omg.CORBA.UNKNOWN u) {
          new BuDialogError(getApp(), InfoSoftSinavi2_, u.toString()).activate();

        } finally {
          BuLib.invokeNowOrLater(new Runnable() {
            public void run() {
              Sinavi2Implementation.this.message(calcul, CtuluLib.getS("Le calcul est termin�"), false);
              setEnabledForAction("CALCULER", true);
              remplirResultats();
            }
          });

        }
      }
    });

    // activeActionsExploitation();
    // setEnabledForAction("PASIMPRESSION" , true);
  }

  /**
   * Methode permettant de mettre a null la fenetre de sinavi2filleaffbateaux_.
   */
  public void resetFille2AffBateaux() {
    this.sinavi2filleaffbateaux_ = null;
  }

  /**
   * Methode permettant de mettre a null la fenetre de sinavi2filleaffbiefs_.
   */
  public void resetFille2AffBiefs() {
    this.sinavi2filleaffbiefs_ = null;
  }

  /**
   * Methode permettant de mettre a null la fenetre de sinavi2filleaffconnexions_.
   */
  public void resetFille2AffConnexion() {
    this.sinavi2filleaffconnexions_ = null;
  }

  /**
   * Methode permettant de mettre a null la fenetre de sinavi2filleaffcroisements_.
   */
  public void resetFille2AffCroisement() {
    this.sinavi2filleaffcroisements_ = null;
  }

  /**
   * Methode permettant de mettre a null la fenetre de sinavi2filleaffecluses_.
   */
  public void resetFille2AffEcluses() {
    this.sinavi2filleaffecluses_ = null;
  }

  /**
   * Methode permettant de mettre a null la fenetre de sinavi2filleafftrematages_.
   */
  public void resetFille2AffTrematage() {
    this.sinavi2filleafftrematages_ = null;
  }

  /**
   * Methode permettant de mettre a null la fenetre de sinavi2filleaffvitesses_.
   */
  public void resetFille2AffVitesse() {
    sinavi2filleaffvitesses_ = null;
  }

  /**
   * Methode permettant de mettre a null la fenetre de sinavi2Filleaddmodbateaux.
   */
  public void resetFille2ModBateaux() {
    this.sinavi2filleaddmodbateaux_ = null;
  }

  /**
   * Methode permettant de mettre a null la fenetre de sinavi2filleaddmodbiefs_.
   */
  public void resetFille2ModBiefs() {
    this.sinavi2filleaddmodbiefs_ = null;
  }

  /**
   * Methode permettant de mettre a null la fenetre de sinavi2filleaddmodconnexions_.
   */
  public void resetFille2ModConnexion() {
    sinavi2filleaddmodconnexions_ = null;
  }

  /**
   * Methode permettant de mettre a null la fenetre de sinavi2filleaddmodecluses_.
   */
  public void resetFille2ModEcluses() {
    this.sinavi2filleaddmodecluses_ = null;
  }

  /**
   * Methode permettant de mettre a null la fenetre de sinavi2fillemodmanoeuvres_.
   */
  public void resetFille2ModManoeuvre() {
    sinavi2fillemodmanoeuvres_ = null;
  }

  /**
   * Methode permettant de mettre a null la fenetre de sinavi2filleaffvitesses_.
   */
  public void resetFille2ModVitesse() {
    sinavi2fillemodvitesses_ = null;
  }

  public void resetFilleAddModIndisponibilites() {
    sinavi2filleaddmodindisponibilites_ = null;
  }

  public void resetFilleAffGeneration() {
    this.sinavi2filleaffgeneration_ = null;
  }

  public void resetFilleAffHistorique() {
    this.sinavi2filleaffhistorique_ = null;
  }

  public void resetFilleAffIndisponibilites() {
    sinavi2filleaffindisponibilites_ = null;
  }

  /**
   * Methode permettant de mettre a null la fenetre de sinavifilleAffTrajet_.
   */
  public void resetFilleAffTrajet() {
    this.sinavi2filleafftrajets_ = null;
  }

  public void resetFilleTableauResultat() {
    sinavi2afftableauresultat_ = null;
  }

  /**
   * Methode permettant de mettre a null la fenetre de sinaviFilleInitialiserTirage.
   */
  public void resetFilleTirage() {
    this.sinavi2filletirage_ = null;
  }

  public void setNomPrj(final String _nomProjet) {
    nomPrj_ = _nomProjet;
    setTitle(_nomProjet);
  }

  public void setPermettreModif(final boolean _permettreModif) {
    permettreModif_ = _permettreModif;
    setEnabledForAction(getActAddBat(), _permettreModif);
    setEnabledForAction("MODBATEAUX", _permettreModif);
    setEnabledForAction(getActSupBateau(), _permettreModif);
    setEnabledForAction("ADDBIEFS", _permettreModif);
    setEnabledForAction("MODBIEFS", _permettreModif);
    setEnabledForAction(getActSupBiefs(), _permettreModif);
    setEnabledForAction(getActAddEcluse(), _permettreModif);
    setEnabledForAction(getActModEcluse(), _permettreModif);
    setEnabledForAction(getActSupEcluse(), _permettreModif);
    setEnabledForAction(getActAssembler(), _permettreModif);
    // setEnabledForAction("LANCER",_permettreModif);
    setEnabledForAction(getActImpNav(), _permettreModif);
    setEnabledForAction(getActImpBief(), _permettreModif);
    setEnabledForAction(getActImportOuv(), _permettreModif);
  }

  public void sauveParametresSimulationCourante(final int _paramSimu) {
  // nc : sauvegarde des parametres de la simulation en cours
  }

  public void sauveResultatsSimulationCourante(final int _resultSimu) {
  // nc : sauvegarde des resultats de la simulation en cours
  }

  public void chargeParamSimulation(final int _chargeParamSimu) {
  // nc : charge les parametres et les r�sultats d'une simulation d�j� effectu�e
  }

  public void chargeResultatsSimulation(final int _chargeResultSimu) {
  // nc : charge les parametres et les r�sultats d'une simulation d�j� effectu�e
  }

  public void setSimulationCourante(final int _sim) {
    // sauvegarde dans la liste des simulation puis charge les nouveaux parametres
    // maj de la liste des donn�es pour la sauvegarde
    paramCourant_ = new SSimulationSinavi2();
    if (listeBateaux_ != null) {
      paramCourant_.parametresBateau = ((SParametresBateau[]) listeBateaux_.toArray(new SParametresBateau[listeBateaux_
          .size()]));
    }
    if (listeBiefs_ != null) {
      FuLog.debug("taille bief :" + listeBiefs_.size());
      paramCourant_.parametresBief = ((SParametresBief[]) listeBiefs_.toArray(new SParametresBief[listeBiefs_.size()]));
      FuLog.debug("bief");
    }
    if (listeEcluses_ != null) {
      paramCourant_.parametresEcluse = ((SParametresEcluse[]) listeEcluses_.toArray(new SParametresEcluse[listeEcluses_
          .size()]));
    }
    if (listeVitesses_ != null) {
      paramCourant_.parametresVitesse = ((SParametresVitesses[]) listeVitesses_
          .toArray(new SParametresVitesses[listeVitesses_.size()]));
    }
    if (listeCroisements_ != null) {
      paramCourant_.parametresCroisement = ((SParametresCroisements[]) listeCroisements_
          .toArray(new SParametresCroisements[listeCroisements_.size()]));
    }
    if (listeTrematages_ != null) {
      paramCourant_.parametresTrematage = ((SParametresTrematages[]) listeTrematages_
          .toArray(new SParametresTrematages[listeTrematages_.size()]));
    }
    if (listeManoeuvres_ != null) {
      paramCourant_.parametresManoeuvre = ((SParametresManoeuvres[]) listeManoeuvres_
          .toArray(new SParametresManoeuvres[listeManoeuvres_.size()]));
    }
    if (listeTrajets_ != null) {
      paramCourant_.parametresTrajet = listeTrajets_.getTrajets();
    }
    if (listeGeneration_ != null) {
      paramCourant_.resultatGenerationBateau = ((SResultatGenerationBateau[]) listeGeneration_
          .toArray(new SResultatGenerationBateau[listeGeneration_.size()]));
    }
    if (listeHistorique_ != null) {
      paramCourant_.resultatHistorique = ((SResultatHistorique[]) listeHistorique_
          .toArray(new SResultatHistorique[listeHistorique_.size()]));
    }
    if (listeConsommation_ != null) {
      paramCourant_.resultatConsommationDEau = ((SResultatConsommationDEau[]) listeConsommation_
          .toArray(new SResultatConsommationDEau[listeConsommation_.size()]));
      // paramCourant_.nomPrj=(nomPrj_); on essaie ce qui suit
    }

    FuLog.debug("numero de simu " + simulationCourante_);

    if (listeSimulationsSinavi2_.size() != 0) {
      paramCourant_.nomPrj = ((SSimulationSinavi2) listeSimulationsSinavi2_.get(0)).nomPrj;
      paramCourant_.parametresGeneration = (gen_);
      if (((SSimulationSinavi2) listeSimulationsSinavi2_.get(simulationCourante_)).commentaire != null) {
        paramCourant_.commentaire = ((SSimulationSinavi2) listeSimulationsSinavi2_.get(simulationCourante_)).commentaire;
      } else {
        paramCourant_.commentaire = "";
      }

      paramCourant_.nomSim = ((SSimulationSinavi2) listeSimulationsSinavi2_.get(simulationCourante_)).nomSim;
    } else {
      paramCourant_.commentaire = commentaireCourant_;
      paramCourant_.nomSim = nomSimCourant_;
    }
    SSimulationSinavi2 temp = new SSimulationSinavi2();
    temp = paramCourant_;

    if (listeSimulationsSinavi2_.size() != 0) {
      listeSimulationsSinavi2_.set(simulationCourante_, temp);
    } else {
      listeSimulationsSinavi2_.add(temp);
    }
    simulationCourante_ = _sim;
    FuLog.debug("num sim maj :" + _sim);
    majSimulationCourante(_sim);
    // chargement des donn�es dans parametres Courant

  }

  /**
   * methode start.
   */
  public void start() {
    super.start();
    // UsineLib.createService(DCalculSinavi2.class);
    assistant_.changeAttitude(BuAssistant.PAROLE, "Bienvenue !\n" + InfoSoftSinavi2_.name + CtuluLibString.ESPACE
        + InfoSoftSinavi2_.version);

    final BuMainPanel mp = getMainPanel();
    mp.doLayout();
    mp.validate();
    assistant_.addEmitters((Container) getApp());
    assistant_.changeAttitude(BuAssistant.ATTENTE, "Vous pouvez cr�er une\nnouvelle simulation\nou en ouvrir une");
    // Application des pr�f�rences
    BuPreferences.BU.applyOn(this);
    // SinaviPreferences.SINAVI.applyOn(this);
  }

  public void supprimerBateaux() {
    if (CtuluLibArray.isEmpty(listeBateaux_)) {
      new BuDialogMessage(getApp(), InfoSoftSinavi2_, "Il n'y aucun type de bateaux dans votre projet !").activate();
    } else {
      final int nb = listeBateaux_.size();
      final String[] s = new String[nb];
      for (int i = 0; i < nb; i++) {
        s[i] = ((SParametresBateau) listeBateaux_.get(i)).identification;
      }
      final BuDialogChoice b = new BuDialogChoice(getApp(), InfoSoftSinavi2_, "Type de Bateaux � Supprimer",
          "Choisissez :", s);
      /** **demande confirmation** */
      if (b.activate() == 0) {
        final String batcourant = b.getValue();
        final int x = rechercherBateau(batcourant);
        supprimerBateaux(x);
      }
    }

  }

  public void supprimerBateaux(final int _x) {
    final SParametresBateau c = (SParametresBateau) listeBateaux_.get(_x);
    final BuDialogConfirmation conf = new BuDialogConfirmation(getApp(), InfoSoftSinavi2_,
        "Voulez-vous supprimer le type de bateau " + c.identification + getInterrog());
    if (conf.activate() == 0) {
      deleteBateau(_x);
    } else {
      affMessage("Le type de bateau " + c.identification + " est conserv� !");
    }
    conf.dispose();

  }

  private String getInterrog() {
    return " ?";
  }

  public void supprimerBiefs() {
    if (CtuluLibArray.isEmpty(listeBiefs_)) {
      final BuDialogMessage dialogMess = new BuDialogMessage(getApp(), InfoSoftSinavi2_,
          "Il n'y aucun type de bief dans votre projet !");
      dialogMess.activate();
    } else {
      final int nb = listeBiefs_.size();
      final String[] s = new String[nb];
      for (int i = 0; i < nb; i++) {
        s[i] = ((SParametresBief) listeBiefs_.get(i)).identification;
      }
      final BuDialogChoice b = new BuDialogChoice(getApp(), InfoSoftSinavi2_, "Type de Bief � Supprimer",
          "Choisissez :", s);
      b.activate();
      final String biefcourant = b.getValue();
      final int idx = rechercherBief(biefcourant);
      if (idx >= 0) {
        supprimerBiefs(idx);
      }
    }

  }

  public void supprimerBiefs(final int _idx) {
    final SParametresBief c = (SParametresBief) listeBiefs_.get(_idx);
    final BuDialogConfirmation conf = new BuDialogConfirmation(getApp(), InfoSoftSinavi2_,
        "Voulez-vous supprimer le type de bief " + c.identification + getInterrog());

    if (conf.activate() == 0) {
      // test(""+ JOptionPane.YES_OPTION);
      listeBiefs_.remove(_idx);
      affMessage("Le type de bief" + c.identification + " est supprim� !");
    } else {

      affMessage("Le type de bief " + c.identification + " est conserv� !");
    }

  }

  public void supprimerEcluses() {
    if (CtuluLibArray.isEmpty(listeEcluses_)) {
      new BuDialogMessage(getApp(), InfoSoftSinavi2_, "Il n'y a aucune �cluse dans votre projet !").activate();
    } else {
      final int nb = listeEcluses_.size();
      final String[] s = new String[nb];
      for (int i = nb - 1; i >= 0; i--) {
        s[i] = ((SParametresEcluse) listeEcluses_.get(i)).identification;
      }
      final BuDialogChoice b = new BuDialogChoice(getApp(), InfoSoftSinavi2_, "L'�cluse � Supprimer", "Choisissez :", s);
      b.activate();
      final String eclusecourant = b.getValue();
      final int x = rechercherEcluse(eclusecourant);
      if (x >= 0) {
        supprimerEcluses(x);
      }
    }

  }

  public void supprimerEcluses(final int _idx) {
    final SParametresEcluse c = (SParametresEcluse) listeEcluses_.get(_idx);
    final BuDialogConfirmation conf = new BuDialogConfirmation(getApp(), InfoSoftSinavi2_,
        "Voulez-vous supprimer l'�cluse " + c.identification + getInterrog());
    if (conf.activate() == 0) {
      listeEcluses_.remove(_idx);
      affMessage("L'�cluse" + c.identification + " est supprim�e !");
    } else {

      affMessage("L'�cluse " + c.identification + " est conserv�e !");
    }

  }

}