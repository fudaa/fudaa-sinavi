/*
 * @file         Sinavi2FilleAff�Bateaux.java
 * @creation     2001-05-17
 * @modification $Date: 2006-09-19 15:08:58 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.sinavi2;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.ListIterator;

import javax.swing.JComponent;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;
import javax.swing.event.InternalFrameEvent;
import javax.swing.event.InternalFrameListener;

import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuComboBox;
import com.memoire.bu.BuCommonImplementation;
import com.memoire.bu.BuDialogMessage;
import com.memoire.bu.BuInternalFrame;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuTable;

import org.fudaa.ctulu.table.CtuluTableExcelWriter;

import org.fudaa.dodico.corba.sinavi2.SParametresBateau;
import org.fudaa.dodico.corba.sinavi2.SResultatGenerationBateau;

/**
 * impl�mentation d'une fen�tre interne permettant d'afficher la g�n�ration des bateaux, on pourra filtrer suivant
 * diff�rents crit�res afin de trouver les donn�es qui nous int�ressent. On trouvera NB_SIMULATION( de la classe
 * implementation) jours de simulations car on fait plusieurs simulations pour avoir de meilleurs r�sultats.
 * 
 * @version $Revision: 1.11 $ $Date: 2006-09-19 15:08:58 $ by $Author: deniger $
 * @author Fatimatou Ka , Beno�t Maneuvrier
 */
public class Sinavi2FilleAffGeneration extends BuInternalFrame implements ActionListener, InternalFrameListener {

  private final BuLabel lTitre_ = new BuLabel("G�n�ration des bateaux  ");
  private final BuComboBox cBateau_ = new BuComboBox();
  private final BuComboBox cAvalantMontant_ = new BuComboBox();
  private final BuComboBox cJourGeneration_ = new BuComboBox();
  private final BuLabel lFiltre_ = new BuLabel("Filtrer : ");
  private final BuLabel lBateau_ = new BuLabel(" Bateaux");
  private final BuLabel lAvalantMontant_ = new BuLabel(" Sens");
  private final BuLabel lJourGeneration_ = new BuLabel(" Jour");

  private final BuButton bAnnuler_ = new BuButton(Sinavi2Resource.SINAVI2.getIcon("QUITTER"), "Quitter");
  private final BuButton bImprimer_ = new BuButton(Sinavi2Resource.SINAVI2.getIcon("IMPRIMER"), "Imprimer");
  private final BuButton bMiseAJour_ = new BuButton(Sinavi2Resource.SINAVI2.getIcon("MISAJOUR"), "Mise � Jour");

  /** **gerer la liste des bateaux**** */
  // ---------------------------------public LinkedList listeBateaux2_;
  public ArrayList listeGeneration_;
  public ArrayList listeBateaux_;
  public int nbjours_;
  // public static int nbBateaux_;

  /** **bateau pour les champs en cas de modifications*** */
  public SParametresBateau batCourant_;
  // private int nbat_=-1;

  /** * panel contenant les boutons, il est plac� en bas */

  private final BuPanel pBoutons_ = new BuPanel();
  /**
   * panel contenant les diff�rents champs de la fen�tre
   */
  private final BuPanel pTitre_ = new BuPanel();

  private final BuPanel pDonnees2_ = new BuPanel();

  /**
   * une instance de SinaviImplementation
   */
  // private BuCommonImplementation app_;
  /**
   * Constructeur
   * 
   * @param _app une instance de SinaviImplementation
   */

  // private BuCommonImplementation _appli;
  // public JTable tabBateaux_;
  public Sinavi2TableauGeneration tb_;
  public BuTable table_;
  public Sinavi2Implementation imp2_ = null;

  public Sinavi2FilleAffGeneration(final BuCommonImplementation _appli, final ArrayList _listeGeneration,
      final ArrayList _listeBateaux, final int _nbJoursSimulation, final int _bateauSelected,
      final int _avalantMontantSelected, final int _jourSelected) {
    super("G�n�ration des bateaux   ", true, true, true, false);

    nbjours_ = _nbJoursSimulation;
    System.out.println("nb jour de simulation " + _nbJoursSimulation);
    remplirListeGeneration(_listeGeneration, _nbJoursSimulation);
    listeBateaux_ = _listeBateaux;
    cBateau_.addItem("TOUS");
    if (listeBateaux_ != null) {
      final ListIterator itBateau = listeBateaux_.listIterator();
      while (itBateau.hasNext()) {
        SParametresBateau b = new SParametresBateau();
        b = (SParametresBateau) itBateau.next();
        cBateau_.addItem(b.identification);
      }
    }

    cAvalantMontant_.addItem("TOUS");
    cAvalantMontant_.addItem("A");
    cAvalantMontant_.addItem("M");

    cJourGeneration_.addItem("TOUS");

    final int nbj = _nbJoursSimulation * Sinavi2Implementation.NB_SIMULATIONS;
    for (int i = 0; i < nbj; i++) {
      cJourGeneration_.addItem("" + i);
      /*
       * miseajour(nbjours_); table_.setSize(250,250); table_.setAutoResizeMode(AUTO_RESIZE_ALL_COLUMNS);
       * table_.setRowSelectionAllowed(true); table_.setColumnSelectionAllowed(false);
       */
    }

    // pDonnees2_.add(table_);//--> bouton mise � jour;
    if (_bateauSelected != 0 || _avalantMontantSelected != 0 || _jourSelected != 0) {
      cBateau_.setSelectedIndex(_bateauSelected);
      cAvalantMontant_.setSelectedIndex(_avalantMontantSelected);
      cJourGeneration_.setSelectedIndex(_jourSelected);

    }
    miseajour(nbjours_);

    imp2_ = (Sinavi2Implementation) _appli.getImplementation();

    bAnnuler_.addActionListener(this);
    bImprimer_.addActionListener(this);
    bMiseAJour_.addActionListener(this);

    ((JComponent) getContentPane()).setBorder(new EmptyBorder(25, 25, 25, 25));
    final GridBagLayout g = new GridBagLayout();
    pTitre_.setLayout(g);
    final GridBagConstraints c = new GridBagConstraints();
    c.fill = GridBagConstraints.BOTH;
    c.gridx = 0;
    c.gridy = 2;
    pTitre_.add(lTitre_, c);
    c.gridx = 1;
    c.gridy = 2;

    pTitre_.add(lFiltre_, c);
    c.gridx = 2;
    c.gridy = 2;
    pTitre_.add(lBateau_, c);

    c.gridx = 2;
    c.gridy = 1;
    pTitre_.add(lAvalantMontant_, c);

    c.gridx = 2;
    c.gridy = 3;
    pTitre_.add(lJourGeneration_, c);

    c.gridx = 3;
    c.gridy = 2;
    pTitre_.add(cBateau_, c);

    c.gridx = 3;
    c.gridy = 1;
    pTitre_.add(cAvalantMontant_, c);

    c.gridx = 3;
    c.gridy = 3;
    pTitre_.add(cJourGeneration_, c);

    /*******************************************************************************************************************
     * pTitre_.add(lTitre_); pTitre_.add(lBateau_); pTitre_.add(cBateau_); pTitre_.add(lAvalantMontant_);
     * pTitre_.add(cAvalantMontant_); pTitre_.add(lJourGeneration_); pTitre_.add(cJourGeneration_);
     ******************************************************************************************************************/
    pBoutons_.setLayout(new FlowLayout(FlowLayout.CENTER));
    pBoutons_.add(bAnnuler_);
    pBoutons_.add(bImprimer_);
    pBoutons_.add(bMiseAJour_);
    getContentPane().add(pTitre_, BorderLayout.NORTH);
    getContentPane().add(pDonnees2_, BorderLayout.CENTER);
    getContentPane().add(pBoutons_, BorderLayout.SOUTH);

    pack();

    /** *reinitialisation des valeurs* */

    setVisible(true);
    addInternalFrameListener(this);
    imp2_.addInternalFrame(this);

  }

  /**
   * on rempli la g�n�ration en cr�ant des sinavi2TypeGeneration � partir de SreulstatGeneration
   */

  private void remplirListeGeneration(final ArrayList _listeGeneration, final int _nbJours) {
    listeGeneration_ = new ArrayList();

    final ListIterator iter = _listeGeneration.listIterator();
    iter.next();
    while (iter.hasNext()) {
      SResultatGenerationBateau g = new SResultatGenerationBateau();
      g = (SResultatGenerationBateau) iter.next();

      final Sinavi2TypeGeneration tg = new Sinavi2TypeGeneration(g);
      listeGeneration_.add(tg);

    }

  }

  /**
   * on cr� une liste selesction o� l'on insert les donn�es correspondant aux filstres selectionn�s.
   */
  public void miseajour(final int _nbJours) {
    // pDonnees2_.removeAll();
    ArrayList selection = new ArrayList();
    cJourGeneration_.getSelectedIndex();
    int row = 0;
    ListIterator iter;
    /* remplit une liste et ensuite et ensuite on remplira le tableau */

    /* if(tb_==null) */tb_ = new Sinavi2TableauGeneration();
    if (cBateau_.getSelectedIndex() != 0) {
      final int bateauSel = cBateau_.getSelectedIndex();
      iter = listeGeneration_.listIterator();
      while (iter.hasNext()) {
        Sinavi2TypeGeneration tg = new Sinavi2TypeGeneration();
        tg = (Sinavi2TypeGeneration) iter.next();
        /*
         * System.out.println("PPPPPPPPPPP"+tg.jour_); System.out.println("PPPPPPPPPPP"+tg.heureDepart_);
         */
        System.out.println("type bateau sel " + bateauSel);
        if (tg.typeBateau_ == bateauSel) {
          selection.add(tg);
        }
      }
    }
    if (cAvalantMontant_.getSelectedIndex() != 0) {
      final String am = cAvalantMontant_.getSelectedItem().toString();
      // System.out.println("aaaaaaaaaaaaaaaa mmmmmmmmmm "+am);
      final ArrayList temp = new ArrayList();
      if ((selection.size() != 0)) {
        iter = selection.listIterator();
      } else {
        iter = listeGeneration_.listIterator();
      }
      while (iter.hasNext()) {
        Sinavi2TypeGeneration tg = new Sinavi2TypeGeneration();
        tg = (Sinavi2TypeGeneration) iter.next();
        // System.out.println(am.charAt(0) + " difff "+tg.avalantMontant_);
        if (am.charAt(0) == tg.avalantMontant_) {
          temp.add(tg);
          // System.out.println("ajout avalant montant");
        }
      }
      selection = new ArrayList(temp);
      // System.out.println("taille dela selection -->-->--> "+selection.size());
    }

    if (cJourGeneration_.getSelectedIndex() != 0) {
      final int jg = cJourGeneration_.getSelectedIndex() - 1;// -1;//+_nbJours;
      System.out.println("jour de generation" + jg);
      final ArrayList temp = new ArrayList();
      if ((selection.size() != 0)) {
        // cBateau_.getSelectedIndex()!=0)))
        iter = selection.listIterator();
      } else {
        iter = listeGeneration_.listIterator();
      }
      while (iter.hasNext()) {
        Sinavi2TypeGeneration tg = new Sinavi2TypeGeneration();
        tg = (Sinavi2TypeGeneration) iter.next();
        if (tg.jour_ == jg) {
          temp.add(tg);
        }
      }
      selection = new ArrayList(temp);
    }
    // System.out.println(" jour "+cJourGeneration_.getSelectedIndex());
    if (cJourGeneration_.getSelectedIndex() == 0 && cBateau_.getSelectedIndex() == 0
        && cAvalantMontant_.getSelectedIndex() == 0) {
      if (listeGeneration_ == null) {
        imp2_.affMessage("a comprend pas");
      } else {
        // imp2_.test("taille generation "+listeGeneration_.size());
        selection = new ArrayList(listeGeneration_);
      }
    }
    if (selection != null) {
      System.out.println("taille de de la selection :" + selection.size());
      iter = selection.listIterator();
      final Object[][] data = new Object[selection.size()][tb_.getColumnCount()];
      iter = selection.listIterator();
      while (iter.hasNext()) {
        Sinavi2TypeGeneration tg = new Sinavi2TypeGeneration();
        tg = (Sinavi2TypeGeneration) iter.next();
        final int j = tg.jour_;// -nbjours_;//--> ON LE REMMETRA PLUS FACILE POUR VERIFIER LA
        String s = (Integer.toString(j));
        // tb_.setValueAt(s,row,0);

        data[row][0] = s;
        // System.out.println("copie "+tb_.getValueAt(row,0));

        data[row][1] = tg.getHeureDepart();
        s = tg.avalantMontant_ + "";

        data[row][2] = s;

        /*
         * int indice=0; ListIterator bat= listeBateaux_.listIterator(); while(indice<cBateau_.getSelectedIndex()-1 &&
         * bat.hasNext()){ bat.next(); indice++; } SParametresBateau b= (SParametresBateau) bat.next(); s=
         * b.identification;
         */

        s = ((SParametresBateau) listeBateaux_.get(tg.typeBateau_ - 1)).identification;

        data[row][3] = s;// -->a corriger
        s = (Integer.toString(tg.getGareDepart()));
        data[row][4] = s;
        s = (Integer.toString(tg.getGareArrivee()));
        data[row][5] = s;

        row++;

      }
      tb_.setNbRow(row);
      tb_.data_ = data;
    }

    table_ = new BuTable(tb_.data_, tb_.nomCol());

    final JScrollPane scrollPane = new JScrollPane(table_);
    pDonnees2_.add(scrollPane);
    scrollPane.repaint();
    // table_.setAutoscrolls(true);
    pDonnees2_.add(scrollPane);

    // table_.repaint();
    /*
     * if(tb_.data_ !=null){ table_.setSize(250,250); table_.setAutoResizeMode(AUTO_RESIZE_ALL_COLUMNS);
     * table_.setRowSelectionAllowed(true); table_.setColumnSelectionAllowed(false); pDonnees2_.add(table_);//--> bouton
     * mise � jour; } //table_.repaint();
     */

  }

  /**
   * @param _e
   */

  public void annuler() {

    imp2_.removeInternalFrame(this);
    // imp2_.sinavi2filleaffbateaux_ =null;
    imp2_.resetFilleAffGeneration();
  }

  public void actionPerformed(final ActionEvent _e) {
    if (_e.getSource() == bAnnuler_) {
      annuler();

    }

    else if (_e.getSource() == bImprimer_) {
      final File s = imp2_.enregistrerXls();
      if (s == null) {
        imp2_.affMessage("Nom de Fichier incorrect");
        return;
      }
      final Sinavi2TableauGeneration tbtemp = new Sinavi2TableauGeneration();
      tbtemp.data_ = new Object[tb_.getRowCount() + 2][tb_.getColumnCount()];
      tbtemp.initNomCol(1);
      tbtemp.setColumnName("G�n�ration des bateaux", 0);
      for (int i = 1; i < tb_.getColumnCount(); i++) {
        tbtemp.setColumnName(" ", i);
      }
      tbtemp.setNbRow(tb_.getRowCount() + 2);
      for (int i = 2; i <= (tb_.getRowCount() + 1); i++) {
        for (int j = 0; j < tb_.getColumnCount(); j++) {
          tbtemp.data_[i][j] = tb_.data_[i - 2][j];
        }

      }
      final CtuluTableExcelWriter test = new CtuluTableExcelWriter(tbtemp, s);
      try {
        test.write(null);

      } catch (final RowsExceededException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      } catch (final WriteException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      } catch (final IOException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }
    } else if (_e.getSource() == bMiseAJour_) {
      annuler();
      // imp2_.afficher_bateaux();
      imp2_.affGenerationBateaux(cBateau_.getSelectedIndex(), cAvalantMontant_.getSelectedIndex(), cJourGeneration_
          .getSelectedIndex());
      // table_.repaint();
      /*
       * if(tb_.data_ !=null){ table_.setSize(250,250); table_.setAutoResizeMode(AUTO_RESIZE_ALL_COLUMNS);
       * table_.setRowSelectionAllowed(true); table_.setColumnSelectionAllowed(false); pDonnees2_.add(table_);//-->
       * bouton mise � jour;
       */
      //
      // miseajour(listeBateaux2_);
      /*
       * if(controler_entrees()==true){ JOptionPane.showMessageDialog(null,"Cr�ation R�ussie"); initialise_champs(-1);
       */
    }
  }

  // a virer
  public void affMessage(final String _t) {
    final BuDialogMessage dialog_mess = new BuDialogMessage(imp2_.getApp(), imp2_.getInformationsSoftware(), "" + _t);
    dialog_mess.activate();
  }

  public void internalFrameActivated(final InternalFrameEvent _e) {
  // TODO Auto-generated method stub

  }

  public void internalFrameClosed(final InternalFrameEvent _e) {
    this.closable = true;
    System.out.println("fermeture centralis�e des portes");
    annuler();

  }

  public void internalFrameDeactivated(final InternalFrameEvent _e) {
  // TODO Auto-generated method stub

  }

  public void internalFrameDeiconified(final InternalFrameEvent _e) {
  // TODO Auto-generated method stub

  }

  public void internalFrameIconified(final InternalFrameEvent _e) {
  // TODO Auto-generated method stub

  }

  public void internalFrameOpened(final InternalFrameEvent _e) {
  // TODO Auto-generated method stub

  }

  public void internalFrameClosing(final InternalFrameEvent _e) {
  // TODO Auto-generated method stub

  }

}
