package org.fudaa.fudaa.sinavi2;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

import javax.swing.JComponent;
import javax.swing.border.EmptyBorder;
import javax.swing.event.InternalFrameEvent;
import javax.swing.event.InternalFrameListener;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuComboBox;
import com.memoire.bu.BuCommonImplementation;
import com.memoire.bu.BuDialogMessage;
import com.memoire.bu.BuInternalFrame;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuTextField;

import org.fudaa.dodico.corba.sinavi2.SHeure;
import org.fudaa.dodico.corba.sinavi2.SParametresBief;
import org.fudaa.dodico.corba.sinavi2.SParametresEcluse;
import org.fudaa.dodico.corba.sinavi2.SParametresIndispoLoiD;
import org.fudaa.dodico.corba.sinavi2.SParametresIndispoLoiE;

/**
 * @version $Revision: 1.5 $ $Date: 2006-09-19 15:08:58 $ by $Author: deniger $
 * @author Fatimatou Ka , Benoit Maneuvrier Cette classe a pour but d'ajouter ou dem odifier des indisponibilit�s. Une
 *         indisponibilit� est d�finie par une loi sur un el�ment. Elle immobilise un element c'est � dire que les
 *         bateaux ne peuvent l'emprunter. Alain Pourplanche n'a pas encore impl�ment� cette fonction donc cette classe
 *         fonctionne mais l'ajout de param�tres n'aura aucun effet sur la simulation dans l'instant pr�sent.
 */

public class Sinavi2FilleAddModIndisponibilites extends BuInternalFrame implements ActionListener,
    InternalFrameListener, FocusListener {

  private final BuLabel lElement_ = new BuLabel("El�ment ");
  private final BuComboBox coElement_ = new BuComboBox();
  private final BuLabel lTypeLoi_ = new BuLabel("Type de loi ");
  private final BuComboBox coTypeLoi_ = new BuComboBox();

  /** Composants pour le panel Loi Erlang */
  private final BuPanel pErlang_ = new BuPanel();
  private final BuLabel lTitreEcarts_ = new BuLabel("Ecarts entre les indisponibilit�s");
  private final BuLabel lOrdreEcart_ = new BuLabel("Ordre pour l'�cart");
  private final BuTextField tOrdreEcart_ = new BuTextField("1");
  private final BuLabel lEcart_ = new BuLabel("Nb de jours");
  private final BuTextField tEcart_ = new BuTextField("0");
  private final BuLabel lTitreDurees_ = new BuLabel("Dur�e de l'indisponibilit�");
  private final BuLabel lOrdreDuree_ = new BuLabel("Ordre pour la dur�e");
  private final BuTextField tOrdreDuree_ = new BuTextField("1");
  private final BuLabel lDureePanne_ = new BuLabel("Dur�e Moyenne");
  private final DureeField dDureePanneHeure_ = new DureeField(false, false, true, false, false);
  private final DureeField dDureePanneMinute_ = new DureeField(false, false, false, true, false);

  /** composants pour le panel Loi Deterministe */
  private final BuPanel pDeterministe_ = new BuPanel();
  private final BuLabel lDates_ = new BuLabel("Dates ");
  private final BuLabel lBlanc1_ = new BuLabel("         ");
  private final BuLabel lDateDebut_ = new BuLabel("Date de D�but ");
  private final BuLabel lDateFin_ = new BuLabel("Date de Fin ");
  private final BuTextField tDateDeDebut_ = new BuTextField();
  private final BuTextField tDateDeFin_ = new BuTextField();
  private final BuLabel lHeuresDet_ = new BuLabel("Heures ");
  private final DureeField dHoraireDebutHeureDet_ = new DureeField(false, false, true, false, false);
  private final DureeField dHoraireDebutMinuteDet_ = new DureeField(false, false, false, true, false);
  private final DureeField dHoraireFinHeureDet_ = new DureeField(false, false, true, false, false);
  private final DureeField dHoraireFinMinuteDet_ = new DureeField(false, false, false, true, false);

  /** composants pour le panel de la loi journali�re */
  private final BuPanel pJournaliere_ = new BuPanel();
  private final BuLabel lHeuresJou_ = new BuLabel("Heures d'Indisponibilit�s");
  private final BuLabel lHeureDeDebutJou_ = new BuLabel("Heure de D�but");
  private final BuLabel lHeureDeFinJou_ = new BuLabel("Heure de Fin");
  private final DureeField dHoraireDebutHeureJou_ = new DureeField(false, false, true, false, false);
  private final DureeField dHoraireDebutMinuteJou_ = new DureeField(false, false, false, true, false);
  private final DureeField dHoraireFinHeureJou_ = new DureeField(false, false, true, false, false);
  private final DureeField dHoraireFinMinuteJou_ = new DureeField(false, false, false, true, false);

  private final BuPanel pDonnees_ = new BuPanel();
  private final BuLabel lTitre_ = new BuLabel("Saisie des Indisponibilit�s");
  private final BuLabel lblanc_ = new BuLabel("     ");

  /** boutons* */
  private final BuButton bValide_ = new BuButton(Sinavi2Resource.SINAVI2.getIcon("VALIDER"), "Sauver");
  private final BuButton bQuitter_ = new BuButton(Sinavi2Resource.SINAVI2.getIcon("QUITTER"), "Quitter");
  private final BuButton bAffiche_ = new BuButton(Sinavi2Resource.SINAVI2.getIcon("AFFICHER"), "Afficher");

  private final BuPanel pBoutons_ = new BuPanel();
  private final GridBagLayout g2_ = new GridBagLayout();
  private final GridBagConstraints c_ = new GridBagConstraints();
  private final Container pan_ = getContentPane();
  private Sinavi2Implementation imp_ = null;
  private final Object[] tabErlang_ = new Object[4];
  private final Object[] tabDeterministe_ = new Object[6];
  private final Object[] tabJournaliere_ = new Object[4];

  /**
   * @param _appli : instance de Sinavi2implementation
   * @param _element : nom de l'el�ment : nomBief...
   * @param _loi : num�ro de la loi - 0 Erlang - 1 D�terministe - 2 Journali�re _element="" et loi =3 pour un ajout
   *          d'indisponibilit�
   */
  public Sinavi2FilleAddModIndisponibilites(final BuCommonImplementation _appli, final String _element, final int _loi) {
    super("Ajouter/Modifier une indisponibilit�", true, true, true, false);
    imp_ = (Sinavi2Implementation) _appli.getImplementation();
    /** Containtes sur les listes d�roulantes */
    coTypeLoi_.addItem("Erlang");
    coTypeLoi_.addItem("Deterministe");
    coTypeLoi_.addItem("Journali�re");
    coTypeLoi_.addActionListener(this);
    /**
     * remplissage des combobox
     */
    if (imp_.listeBiefs_ != null) {
      final ListIterator it = imp_.listeBiefs_.listIterator();
      while (it.hasNext()) {
        final String s = "bie:" + ((SParametresBief) it.next()).identification;
        coElement_.addItem(s);
      }
    }
    if (imp_.listeEcluses_ != null) {
      final ListIterator it = imp_.listeEcluses_.listIterator();
      while (it.hasNext()) {
        final String s = "ecl:" + ((SParametresEcluse) it.next()).identification;
        coElement_.addItem(s);
      }
    }
    if (!_element.equalsIgnoreCase("")) {
      coElement_.setSelectedItem(_element);
    }
    // coElement_.setSelectedIndex(0);
    /** ajouts de listeners aux boutons */
    bValide_.addActionListener(this);
    bQuitter_.addActionListener(this);
    bAffiche_.addActionListener(this);

    ((JComponent) pan_).setBorder(new EmptyBorder(25, 200, 150, 200));
    pan_.setLayout(new FlowLayout());
    /** panel commun a toutes les lois */

    pDonnees_.setLayout(g2_);
    c_.fill = GridBagConstraints.BOTH;

    c_.gridx = 1;
    c_.gridy = 0;
    pDonnees_.add(lTitre_, c_);
    c_.gridx = 0;
    c_.gridy = 1;
    pDonnees_.add(lblanc_, c_);
    c_.gridx = 0;
    c_.gridy = 2;
    pDonnees_.add(lElement_, c_);
    c_.gridx = 2;
    c_.gridy = 2;
    pDonnees_.add(coElement_, c_);
    c_.gridx = 0;
    c_.gridy = 3;
    pDonnees_.add(lTypeLoi_, c_);
    c_.gridx = 2;
    c_.gridy = 3;
    pDonnees_.add(coTypeLoi_, c_);
    c_.gridx = 0;
    c_.gridy = 4;

    pan_.add(pDonnees_, BorderLayout.CENTER);

    initialiseChamps(_element, _loi);
    pack();
    setVisible(true);
    imp_.addInternalFrame(this);
  }

  public void actionPerformed(final ActionEvent _e) {
    /**
     * affichage du panel correspondant � la loi
     */
    if (_e.getSource() == coTypeLoi_) {
      if (coTypeLoi_.getSelectedItem().toString().equals("Erlang")) {
        pDeterministe_.removeAll();
        pJournaliere_.removeAll();
        pan_.validate();
        pErlang_.setLayout(g2_);
        c_.gridx = 0;
        c_.gridy = 5;
        pErlang_.add(lTitreEcarts_, c_);
        c_.gridx = 1;
        c_.gridy = 6;
        pErlang_.add(lOrdreEcart_, c_);
        c_.gridx = 2;
        c_.gridy = 6;
        tOrdreEcart_.addFocusListener(this);
        pErlang_.add(tOrdreEcart_, c_);
        /*
         * tOrdreEcart_.setValueValidator(BuValueValidator.MIN(1)); tabErlang_[0]=tOrdreEcart_;
         */
        c_.gridx = 1;
        c_.gridy = 7;
        pErlang_.add(lEcart_, c_);
        c_.gridx = 2;
        c_.gridy = 7;
        tEcart_.addFocusListener(this);
        pErlang_.add(tEcart_, c_);
        /*
         * tEcart_.setValueValidator(BuValueValidator.MIN(1)); tabErlang_[1]=tEcart_;
         */
        c_.gridx = 0;
        c_.gridy = 8;
        pErlang_.add(lTitreDurees_, c_);
        c_.gridx = 1;
        c_.gridy = 9;
        pErlang_.add(lOrdreDuree_, c_);
        c_.gridx = 2;
        c_.gridy = 9;
        tOrdreDuree_.addFocusListener(this);
        pErlang_.add(tOrdreDuree_, c_);
        /*
         * tOrdreDuree_.setValueValidator(BuValueValidator.MIN(1)); tabErlang_[2]=tOrdreDuree_;
         */
        c_.gridx = 1;
        c_.gridy = 10;
        pErlang_.add(lDureePanne_, c_);
        c_.gridx = 2;
        c_.gridy = 10;
        pErlang_.add(dDureePanneHeure_, c_);
        c_.gridx = 3;
        c_.gridy = 10;
        pErlang_.add(dDureePanneMinute_, c_);
        dDureePanneMinute_.setDureeValidator(DureeFieldValidator.creeMax(3599));
        tabErlang_[3] = dDureePanneMinute_;
        pBoutons_.setLayout(new FlowLayout(FlowLayout.CENTER));
        pBoutons_.add(bQuitter_);
        pBoutons_.add(bValide_);
        pBoutons_.add(bAffiche_);
        pan_.add(pErlang_, BorderLayout.SOUTH);
        pan_.add(pBoutons_);

        pan_.validate();
      } else if (coTypeLoi_.getSelectedItem().toString().equals("Deterministe")) {

        pErlang_.removeAll();
        pJournaliere_.removeAll();
        pan_.validate();
        pDeterministe_.setLayout(g2_);

        c_.gridx = 0;
        c_.gridy = 5;
        pDeterministe_.add(lDates_, c_);
        c_.gridx = 1;
        c_.gridy = 5;
        pDeterministe_.add(lBlanc1_, c_);
        c_.gridx = 2;
        c_.gridy = 5;
        pDeterministe_.add(lHeuresDet_, c_);
        c_.gridx = 0;
        c_.gridy = 6;
        pDeterministe_.add(lDateDebut_, c_);
        c_.gridx = 1;
        c_.gridy = 6;
        tDateDeDebut_.addFocusListener(this);
        pDeterministe_.add(tDateDeDebut_, c_);
        tDateDeDebut_.setText("1");
        /*
         * tDateDeDebut_.setValueValidator(BuValueValidator.MIN(1)); tabDeterministe_[0]=tDateDeDebut_;
         */
        c_.gridx = 0;
        c_.gridy = 7;
        pDeterministe_.add(lDateFin_, c_);

        c_.gridx = 1;
        c_.gridy = 7;
        tDateDeFin_.addFocusListener(this);
        tDateDeFin_.setText("2");
        pDeterministe_.add(tDateDeFin_, c_);
        /*
         * tDateDeFin_.setValueValidator(BuValueValidator.MIN(Double.parseDouble(tDateDeDebut_.getText())));
         * tabDeterministe_[1]=tDateDeFin_;
         */

        c_.gridx = 2;
        c_.gridy = 6;
        pDeterministe_.add(dHoraireDebutHeureDet_, c_);
        dHoraireDebutHeureDet_.setDureeValidator(DureeFieldValidator.creeMax(82800));
        tabDeterministe_[2] = dHoraireDebutHeureDet_;
        c_.gridx = 3;
        c_.gridy = 6;
        pDeterministe_.add(dHoraireDebutMinuteDet_, c_);
        dHoraireDebutMinuteDet_.setDureeValidator(DureeFieldValidator.creeMax(3599));
        tabDeterministe_[3] = dHoraireDebutMinuteDet_;
        c_.gridx = 2;
        c_.gridy = 7;
        pDeterministe_.add(dHoraireFinHeureDet_, c_);
        c_.gridx = 3;
        c_.gridy = 7;
        pDeterministe_.add(dHoraireFinMinuteDet_, c_);
        dHoraireFinHeureDet_.setDureeField(86400);
        DureeFieldValidator.creeMinMax(dHoraireFinHeureDet_, dHoraireDebutHeureDet_.getDureeField()
            + dHoraireDebutMinuteDet_.getDureeField(), 86400);
        tabDeterministe_[4] = dHoraireFinHeureDet_;
        c_.gridx = 3;
        c_.gridy = 7;
        pDeterministe_.add(dHoraireFinMinuteDet_, c_);
        dHoraireFinMinuteDet_.setDureeValidator(DureeFieldValidator.creeMax(3599));
        tabDeterministe_[5] = dHoraireFinMinuteDet_;
        pBoutons_.setLayout(new FlowLayout(FlowLayout.CENTER));
        pBoutons_.add(bQuitter_);
        pBoutons_.add(bValide_);
        pBoutons_.add(bAffiche_);

        pan_.add(pDeterministe_, BorderLayout.SOUTH);
        pan_.add(pBoutons_);
        pan_.validate();
        final List dureeList = new ArrayList();
        dHoraireDebutMinuteDet_.setDureeValidator(DureeFieldValidator.creeMax(3600,
            "Entrez un horaire de d�but correct"));
        dureeList.add(dHoraireDebutMinuteDet_);
        dHoraireDebutHeureDet_.setDureeValidator(DureeFieldValidator.creeMax(86400,
            "Entrez un horaire de d�but correct"));
        dureeList.add(dHoraireDebutHeureDet_);
        dHoraireFinMinuteDet_.setDureeValidator(DureeFieldValidator.creeMax(3600, "Entrez un horaire de fin correct"));
        dureeList.add(dHoraireFinMinuteDet_);
        dHoraireFinHeureDet_.setDureeValidator(DureeFieldValidator.creeMax(86400, "Entrez un horaire de fin correct"));
        dureeList.add(dHoraireFinHeureDet_);
        dureesAValider_ = (DureeField[]) dureeList.toArray(new DureeField[dureeList.size()]);

      } else if (coTypeLoi_.getSelectedItem().toString().equals("Journali�re")) {
        pErlang_.removeAll();
        pDeterministe_.removeAll();
        pan_.validate();
        pJournaliere_.setLayout(g2_);

        c_.gridx = 0;
        c_.gridy = 5;
        pJournaliere_.add(lHeuresJou_, c_);
        c_.gridx = 1;
        c_.gridy = 6;
        pJournaliere_.add(lHeureDeDebutJou_, c_);
        c_.gridx = 2;
        c_.gridy = 6;
        pJournaliere_.add(dHoraireDebutHeureJou_, c_);
        dHoraireDebutHeureJou_.setDureeValidator(DureeFieldValidator.creeMax(82800));
        tabJournaliere_[0] = dHoraireDebutHeureJou_;
        c_.gridx = 3;
        c_.gridy = 6;
        pJournaliere_.add(dHoraireDebutMinuteJou_, c_);
        dHoraireDebutMinuteJou_.setDureeValidator(DureeFieldValidator.creeMax(3599));
        tabJournaliere_[1] = dHoraireDebutMinuteJou_;
        c_.gridx = 1;
        c_.gridy = 7;
        pJournaliere_.add(lHeureDeFinJou_, c_);

        c_.gridx = 2;
        c_.gridy = 7;
        pJournaliere_.add(dHoraireFinHeureJou_, c_);
        dHoraireFinHeureJou_.setDureeField(86400);
        DureeFieldValidator.creeMinMax(dHoraireFinHeureDet_, dHoraireDebutHeureJou_.getDureeField()
            + dHoraireDebutMinuteJou_.getDureeField(), 86400);
        tabJournaliere_[2] = dHoraireFinHeureJou_;
        c_.gridx = 3;
        c_.gridy = 7;
        pJournaliere_.add(dHoraireFinMinuteJou_, c_);
        dHoraireFinMinuteJou_.setDureeValidator(DureeFieldValidator.creeMax(3599));
        tabJournaliere_[3] = dHoraireFinMinuteJou_;
        pan_.add(pJournaliere_, BorderLayout.SOUTH);
        pBoutons_.setLayout(new FlowLayout(FlowLayout.CENTER));
        pBoutons_.add(bQuitter_);
        pBoutons_.add(bValide_);
        pBoutons_.add(bAffiche_);

        pan_.add(pJournaliere_, BorderLayout.SOUTH);
        pan_.add(pBoutons_);
        pan_.validate();
        final List dureeList = new ArrayList();
        dHoraireDebutMinuteJou_.setDureeValidator(DureeFieldValidator.creeMax(3600,
            "Entrez un horaire de d�but correct"));
        dureeList.add(dHoraireDebutMinuteJou_);
        dHoraireDebutHeureJou_.setDureeValidator(DureeFieldValidator.creeMax(86400,
            "Entrez un horaire de d�but correct"));
        dureeList.add(dHoraireDebutHeureJou_);
        dHoraireFinMinuteJou_.setDureeValidator(DureeFieldValidator.creeMax(3600, "Entrez un horaire de fin correct"));
        dureeList.add(dHoraireFinMinuteJou_);
        dHoraireFinHeureJou_.setDureeValidator(DureeFieldValidator.creeMax(86400, "Entrez un horaire de fin correct"));
        dureeList.add(dHoraireFinHeureJou_);
        dureesAValider_ = (DureeField[]) dureeList.toArray(new DureeField[dureeList.size()]);

      }
    }

    else if (_e.getSource() == bValide_) {
      if (controlerEntrees()) {
        initialiseChamps("", 3);

      }
    } else if (_e.getSource() == bQuitter_) {
      annuler();

    } else if (_e.getSource() == bAffiche_) {
      annuler();
      imp_.afficherIndisponibilites(0);

    }
  }
  DureeField[] dureesAValider_;

  private boolean controlerEntrees() {
    // Erlang
    final String typeElement = coElement_.getSelectedItem().toString().substring(0, 4);
    final String elem = coElement_.getSelectedItem().toString().substring(4);
    System.out.println(elem);
    if (coTypeLoi_.getSelectedIndex() == 0) {

      if (!((DureeField) tabErlang_[3]).isValueValid()) {
        imp_.affMessage(((DureeField) tabErlang_[3]).val_.getDescription());
        return false;
      }
      if ((dDureePanneHeure_.getDureeField() + dDureePanneMinute_.getDureeField()) == 0) {
        imp_.affMessage("Entrez une dur�e d'indisponibilit� positive");
        return false;
      }
      if (Integer.parseInt(tOrdreEcart_.getText()) <= 0) {
        imp_.affMessage("Entrez un ordre pour l'�cart sup�rieur 0");
        return false;
      }
      if (Integer.parseInt(tOrdreDuree_.getText()) <= 0) {
        imp_.affMessage("Entrez un ordre pour la dur�e sup�rieur � 0");
        return false;
      }
      // ajout si ok
      /**
       * On recherche l'el�ment et on lui ajoute la loi d'indisponibilit�
       */
      if (typeElement.equalsIgnoreCase("bie:")) {

        final ListIterator it = imp_.listeBiefs_.listIterator();
        boolean trouve = false;
        while (it.hasNext() & !trouve) {

          final SParametresBief b = (SParametresBief) it.next();
          if (b.identification.equalsIgnoreCase(elem)) {

            trouve = true;
            b.loiE = new SParametresIndispoLoiE();
            b.loiE.duree = new SHeure();
            b.loiE.duree.heure = dDureePanneHeure_.getDureeField() / 3600;
            b.loiE.duree.minutes = dDureePanneMinute_.getDureeField() / 60;
            b.loiE.nbJoursDEcart = Integer.parseInt(tEcart_.getText());
            b.loiE.ordreDuree = Integer.parseInt(tOrdreDuree_.getText());
            b.loiE.ordreEcart = Integer.parseInt(tOrdreEcart_.getText());
            imp_.affMessage("L'indisponibilit� a �t� ajout�e");
          }
        }
      } else {// ecluse
        final ListIterator it = imp_.listeEcluses_.listIterator();
        boolean trouve = false;
        while (it.hasNext() & !trouve) {
          final SParametresEcluse e = (SParametresEcluse) it.next();
          if (e.identification.equalsIgnoreCase(elem)) {
            trouve = true;
            e.loiE.duree.heure = dDureePanneHeure_.getDureeField() / 3600;
            e.loiE.duree.minutes = dDureePanneMinute_.getDureeField() / 60;
            e.loiE.nbJoursDEcart = Integer.parseInt(tEcart_.getText());
            e.loiE.ordreDuree = Integer.parseInt(tOrdreDuree_.getText());
            e.loiE.ordreEcart = Integer.parseInt(tOrdreEcart_.getText());
            imp_.affMessage("L'indisponibilit� a �t� ajout�e");
          }
        }
      }

    }
    /**
     * loi deterministe
     */
    else if (coTypeLoi_.getSelectedIndex() == 1) {

      for (int i = 0; i < dureesAValider_.length; i++) {
        if (!dureesAValider_[i].isValueValid()) {
          imp_.affMessage(dureesAValider_[i].val_.getDescription());
          return false;
        }
      }

      if (Integer.parseInt(tDateDeDebut_.getText()) <= 0) {
        imp_.affMessage("Entrez une date de d�but correcte");
        return false;
      }
      if (Integer.parseInt(tDateDeFin_.getText()) <= 0) {
        imp_.affMessage("Entrez une date de fin correcte");
        return false;
      }
      for (int i = 2; i < 6; i++) {
        if (!((DureeField) tabDeterministe_[i]).isValueValid()) {
          imp_.affMessage(((DureeField) tabDeterministe_[i]).val_.getDescription());
          return false;
        }
      }
      if (dHoraireFinHeureDet_.getDureeField() == 86400 && dHoraireFinMinuteDet_.getDureeField() > 0) {
        imp_.affMessage("Une journ�e ne peut d�passer 24h");
        return false;
      }
      if (Integer.parseInt(tDateDeDebut_.getText()) > Integer.parseInt(tDateDeFin_.getText())) {
        imp_.affMessage("La date de d�but doit �tre inf�rieur � la date de fin");
        return false;
      }
      if (typeElement.equalsIgnoreCase("bie:")) {
        /**
         * on recherche si la loi d�terministe existe d�j�, si elle n'existe pas on l'ajoute � la fin du tableau de la
         * loi deterministe de l'�lement selectionn�.
         */
        final ListIterator it = imp_.listeBiefs_.listIterator();
        boolean trouve = false;
        while (it.hasNext() & !trouve) {
          final SParametresBief b = (SParametresBief) it.next();
          if (b.identification.equalsIgnoreCase(elem)) {
            trouve = true;
            boolean exist = false;
            for (int i = 0; i < b.loiD.length; i++) {
              final SParametresIndispoLoiD temp = b.loiD[i];
              if (temp.heureDebut.heure == (dHoraireDebutHeureDet_.getDureeField() / 3600)
                  && temp.heureDebut.minutes == (dHoraireDebutMinuteDet_.getDureeField() / 60)
                  && temp.heureFin.heure == (dHoraireFinHeureDet_.getDureeField() / 3600)
                  && temp.heureFin.minutes == (dHoraireFinMinuteDet_.getDureeField() / 60)
                  && temp.dateDebut == Integer.parseInt(tDateDeDebut_.getText())
                  && temp.dateFin == Integer.parseInt(tDateDeFin_.getText())) {
                exist = true;
              }
            }
            if (!exist) {
              final SParametresIndispoLoiD[] temploid = new SParametresIndispoLoiD[b.loiD.length];
              for (int i = 0; i < b.loiD.length; i++) {
                temploid[i] = b.loiD[i];
              }
              b.loiD = new SParametresIndispoLoiD[b.loiD.length + 1];
              for (int i = 0; i < temploid.length; i++) {
                b.loiD[i] = temploid[i];
              }
              final SParametresIndispoLoiD temp = new SParametresIndispoLoiD();
              temp.heureDebut = new SHeure();
              temp.heureFin = new SHeure();
              temp.heureDebut.heure = (dHoraireDebutHeureDet_.getDureeField() / 3600);
              temp.heureDebut.minutes = (dHoraireDebutMinuteDet_.getDureeField() / 60);
              temp.heureFin.heure = (dHoraireFinHeureDet_.getDureeField() / 3600);
              temp.heureFin.minutes = (dHoraireFinMinuteDet_.getDureeField() / 60);
              System.out.println("date fin " + tDateDeFin_.getText());
              temp.dateDebut = Integer.parseInt(tDateDeDebut_.getText());
              temp.dateFin = Integer.parseInt(tDateDeFin_.getText());
              System.out.println("date fin " + temp.dateFin);
              b.loiD[b.loiD.length - 1] = temp;
              imp_.affMessage("L'indisponibilit� a �t� ajout�e");
            } else {
              imp_.affMessage("Cette indisponibilit� existe d�j�.");
            }
          }
        }
      } else {// ecluse
        final ListIterator it = imp_.listeEcluses_.listIterator();
        boolean trouve = false;
        while (it.hasNext() & !trouve) {
          final SParametresEcluse e = (SParametresEcluse) it.next();
          if (e.identification.equalsIgnoreCase(elem)) {
            trouve = true;
            boolean exist = false;
            for (int i = 0; i < e.loiD.length; i++) {
              final SParametresIndispoLoiD temp = e.loiD[i];
              if (temp.heureDebut.heure == (dHoraireDebutHeureDet_.getDureeField() / 3600)
                  && temp.heureDebut.minutes == (dHoraireDebutMinuteDet_.getDureeField() / 60)
                  && temp.heureFin.heure == (dHoraireFinHeureDet_.getDureeField() / 3600)
                  && temp.heureFin.minutes == (dHoraireFinMinuteDet_.getDureeField() / 60)
                  && temp.dateDebut == Integer.parseInt(tDateDeDebut_.getText())
                  && temp.dateFin == Integer.parseInt(tDateDeFin_.getText())) {
                exist = true;
              }
            }
            if (!exist) {
              final SParametresIndispoLoiD[] temploid = new SParametresIndispoLoiD[e.loiD.length];
              for (int i = 0; i < e.loiD.length; i++) {
                temploid[i] = e.loiD[i];
              }
              e.loiD = new SParametresIndispoLoiD[e.loiD.length + 1];
              for (int i = 0; i < temploid.length; i++) {
                e.loiD[i] = temploid[i];
              }
              final SParametresIndispoLoiD temp = new SParametresIndispoLoiD();
              temp.heureDebut = new SHeure();
              temp.heureFin = new SHeure();
              temp.heureDebut.heure = (int) (dHoraireDebutHeureDet_.getDureeField() / 3600);
              temp.heureDebut.minutes = (int) (dHoraireDebutMinuteDet_.getDureeField() / 60);
              temp.heureFin.heure = (int) (dHoraireFinHeureDet_.getDureeField() / 3600);
              temp.heureFin.minutes = (int) (dHoraireFinMinuteDet_.getDureeField() / 60);
              temp.dateDebut = Integer.parseInt(tDateDeDebut_.getText());
              temp.dateFin = Integer.parseInt(tDateDeFin_.getText());
              e.loiD[e.loiD.length - 1] = temp;
              imp_.affMessage("L'indisponibilit� a �t� ajout�e");
            } else {
              imp_.affMessage("Cette indisponibilit� existe d�j�.");
            }
          }
        }
      }

    }
    // Journali�re
    else if (coTypeLoi_.getSelectedIndex() == 2) {
      for (int i = 0; i < 4; i++) {
        if (!((DureeField) tabJournaliere_[i]).isValueValid()) {
          imp_.affMessage(((DureeField) tabJournaliere_[i]).val_.getDescription());
          return false;
        }
      }
      for (int i = 0; i < dureesAValider_.length; i++) {
        if (!dureesAValider_[i].isValueValid()) {
          imp_.affMessage(dureesAValider_[i].val_.getDescription());
          return false;
        }
      }

      if (dHoraireFinHeureJou_.getDureeField() == 86400 && dHoraireFinMinuteJou_.getDureeField() > 0) {
        imp_.affMessage("Une journ�e ne peut d�passer 24h");
        return false;
      }
      if (typeElement.equalsIgnoreCase("bie:")) {
        final ListIterator it = imp_.listeBiefs_.listIterator();
        boolean trouve = false;
        while (it.hasNext() & !trouve) {
          final SParametresBief b = (SParametresBief) it.next();
          if (b.identification.equalsIgnoreCase(elem)) {
            trouve = true;
            /*
             * b.loiJ.heureDebut=new SHeure(); b.loiJ.heureFin=new SHeure();
             */
            b.loiJ.heureDebut.heure = (int) (dHoraireDebutHeureJou_.getDureeField() / 3600);
            b.loiJ.heureDebut.minutes = (int) (dHoraireDebutMinuteJou_.getDureeField() / 60);
            b.loiJ.heureFin.heure = (int) (dHoraireFinHeureJou_.getDureeField() / 3600);
            b.loiJ.heureFin.minutes = (int) (dHoraireFinMinuteJou_.getDureeField() / 60);
            imp_.affMessage("L'indisponibilit� a �t� ajout�e");
          }
        }
      }

      else {// ecluse
        final ListIterator it = imp_.listeEcluses_.listIterator();
        boolean trouve = false;
        while (it.hasNext() & !trouve) {
          final SParametresEcluse e = (SParametresEcluse) it.next();
          if (e.identification.equalsIgnoreCase(elem)) {
            trouve = true;
            e.loiJ.heureDebut = new SHeure();
            e.loiJ.heureFin = new SHeure();
            e.loiJ.heureDebut.heure = (int) (dHoraireDebutHeureJou_.getDureeField() / 3600);
            e.loiJ.heureDebut.minutes = (int) (dHoraireDebutMinuteJou_.getDureeField() / 60);
            e.loiJ.heureFin.heure = (int) (dHoraireFinHeureJou_.getDureeField() / 3600);
            e.loiJ.heureFin.minutes = (int) (dHoraireFinMinuteJou_.getDureeField() / 60);
            imp_.affMessage("L'indisponibilit� a �t� ajout�e");
          }
        }
      }
    }

    return true;
  }

  private void annuler() {
    imp_.removeInternalFrame(this);
    // imp_.sinavifilletrajet_ =null;
    imp_.resetFilleAddModIndisponibilites();

  }

  /**
   * initilise les champs avec les param�tres correspondants
   * 
   * @param _elem : nom de l'�lement
   * @param loi : num�ro de la loi 3 _elem="" , loi=3 pour un ajout d'insdisponibilit�
   */
  public void initialiseChamps(final String _elem, final int loi) {
    if (_elem.equalsIgnoreCase("") && loi == 3) {
      tOrdreDuree_.setText("1");
      tEcart_.setText("0");
      tOrdreEcart_.setText("1");
      dDureePanneHeure_.setDureeField(0);
      dDureePanneMinute_.setDureeField(0);

      tDateDeDebut_.setText("1");
      tDateDeFin_.setText("2");
      dHoraireDebutHeureDet_.setDureeField(0);
      dHoraireDebutMinuteDet_.setDureeField(0);
      dHoraireFinHeureDet_.setDureeField(86400);
      dHoraireFinMinuteDet_.setDureeField(0);

      dHoraireDebutHeureJou_.setDureeField(0);
      dHoraireDebutMinuteJou_.setDureeField(0);
      dHoraireFinHeureJou_.setDureeField(86400);
      dHoraireFinMinuteJou_.setDureeField(0);
    } else if (loi == 0) {// Erlang
      coTypeLoi_.setSelectedIndex(0);
      final String prec = _elem.substring(0, 4);
      final String elem = _elem.substring(4);
      if (prec.equalsIgnoreCase("bie:")) {
        final ListIterator it = imp_.listeBiefs_.listIterator();
        boolean trouve = false;
        while (it.hasNext() & !trouve) {
          final SParametresBief b = (SParametresBief) it.next();
          if (b.identification.equalsIgnoreCase(elem)) {
            trouve = true;
            tOrdreDuree_.setText("" + b.loiE.ordreDuree);
            tEcart_.setText("" + b.loiE.nbJoursDEcart);
            tOrdreEcart_.setText("" + b.loiE.ordreEcart);
            dDureePanneHeure_.setDureeField(b.loiE.duree.heure * 3600);
            dDureePanneMinute_.setDureeField(b.loiE.duree.minutes * 60);
          }
        }
      } else {
        final ListIterator it = imp_.listeEcluses_.listIterator();
        boolean trouve = false;
        while (it.hasNext() & !trouve) {
          final SParametresEcluse e = (SParametresEcluse) it.next();
          if (e.identification.equalsIgnoreCase(elem)) {
            trouve = true;
            tOrdreDuree_.setText("" + e.loiE.ordreDuree);
            tEcart_.setText("" + e.loiE.nbJoursDEcart);
            tOrdreEcart_.setText("" + e.loiE.ordreEcart);
            dDureePanneHeure_.setDureeField(e.loiE.duree.heure * 3600);
            dDureePanneMinute_.setDureeField(e.loiE.duree.minutes * 60);
          }
        }
      }

    }

    else if (loi == 2) {// journaliere
      coTypeLoi_.setSelectedIndex(2);
      final String prec = _elem.substring(0, 4);
      final String elem = _elem.substring(4);
      if (prec.equalsIgnoreCase("bie:")) {
        final ListIterator it = imp_.listeBiefs_.listIterator();
        boolean trouve = false;
        while (it.hasNext() & !trouve) {
          final SParametresBief b = (SParametresBief) it.next();
          if (b.identification.equalsIgnoreCase(elem)) {
            trouve = true;
            dHoraireDebutHeureJou_.setDureeField(b.loiJ.heureDebut.heure * 3600);
            dHoraireDebutMinuteJou_.setDureeField(b.loiJ.heureDebut.minutes * 60);
            dHoraireFinHeureJou_.setDureeField(b.loiJ.heureFin.heure * 3600);
            dHoraireFinMinuteJou_.setDureeField(b.loiJ.heureFin.minutes * 60);

          }
        }
      } else {
        final ListIterator it = imp_.listeEcluses_.listIterator();
        boolean trouve = false;
        while (it.hasNext() & !trouve) {
          final SParametresEcluse e = (SParametresEcluse) it.next();
          if (e.identification.equalsIgnoreCase(elem)) {
            trouve = true;
            dHoraireDebutHeureJou_.setDureeField(e.loiJ.heureDebut.heure * 3600);
            dHoraireDebutMinuteJou_.setDureeField(e.loiJ.heureDebut.minutes * 60);
            dHoraireFinHeureJou_.setDureeField(e.loiJ.heureFin.heure * 3600);
            dHoraireFinMinuteJou_.setDureeField(e.loiJ.heureFin.minutes * 60);
          }
        }
      }

    }

  }

  public void focusGained(final FocusEvent _e) {
    tOrdreEcart_.selectAll();
    tEcart_.selectAll();
    tOrdreDuree_.selectAll();
    tDateDeDebut_.selectAll();
    tDateDeFin_.selectAll();
    processFocusEvent(new FocusEvent(this, FocusEvent.FOCUS_GAINED));
  }

  /**
   * M�thode permettant de donner la loi a partir d'un entier 0 : Erlang 1: Deterministe 2:Journali�re
   */
  public static String typeLoi(final int _l) {
    String s = null;
    if (_l == 0) {
      s = "Erlang";
    } else if (_l == 1) {
      s = "Deterministe";
    } else if (_l == 2) {
      s = "Journali�re";
    }
    return s;
  }

  /**
   * M�thode permettant de savoir si deux bateaux ont le meme nom, sens, gare de d�part et d'arriv�e et loi
   * 
   * @param _t1
   * @param _t2
   * @return
   */
  public void Message(final String _mess) {
    final BuDialogMessage dialog_mess = new BuDialogMessage(imp_.getApp(), imp_.getInformationsSoftware(), "" + _mess);
    dialog_mess.activate();
  }

  public void internalFrameActivated(final InternalFrameEvent _e) {
  // TODO Auto-generated method stub

  }

  public void internalFrameClosed(final InternalFrameEvent _e) {
    this.closable = true;
    annuler();

  }

  public void internalFrameClosing(final InternalFrameEvent _e) {
  // TODO Auto-generated method stub

  }

  public void internalFrameDeactivated(final InternalFrameEvent _e) {
  // TODO Auto-generated method stub

  }

  public void internalFrameDeiconified(final InternalFrameEvent _e) {
  // TODO Auto-generated method stub

  }

  public void internalFrameIconified(final InternalFrameEvent _e) {
  // TODO Auto-generated method stub

  }

  public void internalFrameOpened(final InternalFrameEvent _e) {
  // TODO Auto-generated method stub

  }

  public void focusLost(final FocusEvent e) {
  // TODO Auto-generated method stub

  }
}
