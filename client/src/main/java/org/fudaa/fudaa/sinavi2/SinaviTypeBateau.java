package org.fudaa.fudaa.sinavi2;

import org.fudaa.ctulu.CtuluLibString;

import org.fudaa.dodico.corba.sinavi2.SParametresBateau;

class SinaviTypeBateau {

  /*
   * public void desssineMoi(){ dessinateur.dessineBateau(); }
   */

  // constructeur
  public SinaviTypeBateau() {}

  /**
   * @param id non null
   * @param longueur peut etre null
   * @param largeur_
   * @param hdebnav
   * @param hfinnav
   * @param tirantdeau
   * @param gene
   */
  public SinaviTypeBateau(final String _id, final double _longueur, final double _largeur, /* int hdebnav, int hfinnav */
      final int _debnavheure, final int _debnavminute, final int _finnavheure, final int _finnavminute,
      final double _tirantdeau, final int _geneHeure, final int _geneMinute, final double _vitesseMontant,
      final double _vitesseDescendant) {
    /*
     * this.id_ = id; this.longueur_=longueur; this.largeur_ =largeur; this.h_deb_nav_ =hdebnav; this.h_fin_nav_
     * =hfinnav; this.tirant_deau_ =tirantdeau; this.gene_ = gene;
     */
    /* System.out.println("testative id"+id); */
    bat_ = new SParametresBateau();
    bat_.identification = _id;
    // System.out.println("testative id reussie");
    bat_.longueur = _longueur;
    // System.out.println("testative id reussie");
    bat_.largeur = _largeur;
    // bat_.debutNavigation=(double)hdebnav/3600;
    // bat_.finNavigation=(double)hfinnav/3600;
    final int hdebnav = _debnavheure + _debnavminute;
    final int hfinnav = _finnavheure + _finnavminute;
    bat_.debutNavigation = determineHeure(hdebnav);
    bat_.finNavigation = determineHeure(hfinnav);
    bat_.tirantDeau = _tirantdeau;
    final int hgene = _geneHeure + _geneMinute;
    bat_.dureeGeneAdmissible = determineHeure(hgene);
    arrondir(bat_.debutNavigation, 2);// inutil
    arrondir(bat_.finNavigation, 2);// inutil
    arrondir(bat_.dureeGeneAdmissible, 2); // inutil
    // ---------------------------------------

    bat_.vitesseMontantParDefaut = _vitesseMontant / 1000;
    bat_.vitesseDescendantParDefaut = _vitesseDescendant / 1000;
    // System.out.println(SinaviLib.getS("bonjour"));

    /* this.gene_ =gene; */
  }

  /*
   * public Component getEditeur(){ return new double(true,false,true); }
   */

  public static int determineHeureSeule(final double _heure) {
    final int m = (int) (_heure * 100);
    final int heu = m / 100 * 3600;
    return heu;
  }

  public static int determineMinuteSeule(final double _heure) {
    final int m = (int) (_heure * 100);
    final int min = m % 100;
    return min;
  }

  public static double determineHeure(final int _nbSecondes) {
    final int heure = _nbSecondes / 3600;
    final int minute = _nbSecondes / 60 - heure * 60;
    String s;
    if (minute > 9) {
      s = new String(heure + CtuluLibString.DOT + minute);
    } else {
      s = new String(heure + ".0" + minute);
    }
    final Double h = new Double(s);
    return h.doubleValue();
  }

  public static int determineSeconde(final double _heure) {
    final int m = (int) (_heure * 100);
    final int min = m % 100 * 60;
    final int heu = m / 100 * 3600;
    return heu + min;
  }

  // accesseurs
  public String getIdentification() {
    return bat_.identification;
  }

  public double getLongueur() {
    return bat_.longueur;
  }

  public double getLargeur() {
    return bat_.largeur;
  }

  public double getDebutNavigation() {
    return bat_.debutNavigation;
  }

  public double getFinNavigation() {
    return bat_.finNavigation;
  }

  public double getDureeGeneAdmissible() {
    return bat_.dureeGeneAdmissible;
  }

  public double getTirantDeau() {
    return bat_.tirantDeau;
  }

  public double getVitesseMontantParDefaut() {
    return bat_.vitesseMontantParDefaut;
  }

  public double getVitesseDescendantParDefaut() {
    return bat_.vitesseDescendantParDefaut;
  }

  // modifieurs
  public void setIdentification(final String _id) {
    bat_.identification = _id;
  }

  public void setLongueur(final double _longueur) {
    bat_.longueur = _longueur;
  }

  public void setLargeur(final double _largeur) {
    bat_.largeur = _largeur;
  }

  public void setDebutNavigation(final double _h_deb_nav) {
    bat_.debutNavigation = _h_deb_nav;
  }

  public void setFinNavigation(final double _h_fin_nav) {
    bat_.finNavigation = _h_fin_nav;
  }

  public void setDureeGeneAdmissible(final double _gene) {
    bat_.dureeGeneAdmissible = _gene;
  }

  public void setTirantDeau(final double _tirant_deau) {
    bat_.tirantDeau = _tirant_deau;
  }

  public void setVitesseMontantParDefaut(final double _vitesseMontantParDefaut) {
    bat_.vitesseMontantParDefaut = _vitesseMontantParDefaut;
  }

  public void setVitesseDescendantParDefaut(final double _vitesseDescendantParDefaut) {
    bat_.vitesseDescendantParDefaut = _vitesseDescendantParDefaut;
  }

  /*
   * public double creeComponent(boolean _useK,boolean _useM,boolean _useL){ LongueurField field=new
   * LongueurField(_useK,_useM,_useL); field.setValueValidator(new BuValueValidator() { public boolean
   * isValueValid(Object _value) { boolean r=(_value instanceof LongueurField); if(r){ double
   * l=((LongueurField)_value).getLongueurField(); return l>0 && l<1E6; } // TODO Auto-generated method stub return
   * false; } }); return field; }
   */

  public void arrondir(double _a, final int _nbChiffres) {
    for (int i = 0; i < _nbChiffres; i++) {
      _a *= 10;
    }
    _a = (int) (_a + .5);
    for (int i = 0; i < _nbChiffres; i++) {
      _a /= 10;
    }
  }

  public boolean typeBateauEquals(final SinaviTypeBateau _b) {
    return (this.getDureeGeneAdmissible() == _b.getDureeGeneAdmissible()
        && this.getDebutNavigation() == _b.getDebutNavigation() && this.getFinNavigation() == _b.getFinNavigation()
        && this.getIdentification() == _b.getIdentification() && this.getLargeur() == _b.getLargeur()
        && this.getLongueur() == _b.getLongueur() && this.getTirantDeau() == _b.getTirantDeau());
  }

  /*
   * private String id_; private double longueur_,largeur_,tirant_deau_,h_deb_nav_,h_fin_nav_,gene_;
   */
  SParametresBateau bat_;
}
