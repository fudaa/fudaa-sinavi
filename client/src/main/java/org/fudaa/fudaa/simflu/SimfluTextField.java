package org.fudaa.fudaa.simflu;

import java.awt.Color;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;

import com.memoire.bu.BuTextField;


/**
 * Classe de base d�crivant un composant JTextField.
 * Permet de mettre un style de pr�sentation du contenu et du comportement.
 * Les principaux �v�nements g�r�s sont :
 * � l'acquisition du focus (changement de couleur...)
 * � la perte de focus (traitement de la donn�e saisie par l'utilisateur et messages d'erreurs...)
 *@version $Version$
 * @author hadoux
 *
 */
public class SimfluTextField extends BuTextField{

	public SimfluTextField() {
		super();
		SiporStyle();
	}

	

	public SimfluTextField(int columns) {
		super(columns);
		SiporStyle();
	}

	public SimfluTextField(String text, int columns) {
		super(text, columns);
		SiporStyle();
	}

	public SimfluTextField(String text) {
		super(text);
		SiporStyle();
	}


	/**
	 * Methode qui decrit le style, comportement et tests des textfields sipor
	 */
	protected void SiporStyle(){
		
		setColumns(taille());
		
		specifier_validator();
		
		//-- Traitement avant et apr�s focus --//
		this.addFocusListener(new FocusAdapter()
		{
			public void focusGained(FocusEvent e)
			{
				SimfluTextField.this.selectAll();
		//		SimfluTextField.this.setForeground(SiporImplementation.bleuSipor);//new Color(80,161,80));
			}
			public void focusLost(FocusEvent e){
				
				//-- remplacement virgule ou point --//
				String contenu=SimfluTextField.this.getText();
				if(!contenu.equals("")){
					int indice=contenu.lastIndexOf(",");
					if(indice!=-1){
						//-- remplacement de la virgule par un point --//
						String section1=contenu.substring(0, contenu.lastIndexOf(","));
						String section2=contenu.substring(contenu.lastIndexOf(",")+1,contenu.length());
						SimfluTextField.this.setText(section1+"."+section2);
					}
				
				
				}
				//-- traitement sp�cifique du type de textfield --//
					//-- M�thode h�rit�e dans les classes filles --//
				traitementApresFocus(e);

			}

		});


	}

	/**Methode destinee a etre surchargee par les classes h�ritant de SiporTextField.
	 * Permet de g�rer les contr�les apr�s saisie de l'utilisateur **/
	protected void traitementApresFocus(FocusEvent e){
		SimfluTextField.this.setForeground(Color.black);
	}
	
	protected int taille(){
		return 10;
	}
	
	protected void specifier_validator(){
		
		
	}
	

}
