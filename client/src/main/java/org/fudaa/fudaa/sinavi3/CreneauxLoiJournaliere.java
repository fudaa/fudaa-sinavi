/**
 *@creation 10 oct. 06
 *@modification $Dates$
 *@license  GNU General Public Licence 2
 *@copyright   (c)1998-2006 CETMEF 2 bd Gambetta F -60231 Compiegne
 *@mail   devel@fudaa.fr 
 */
package org.fudaa.fudaa.sinavi3;

/**
 * classe de gestion des donn�es des lois journalieres
 * 
 * @version $Version$
 * @author adrien hadoux
 */
class CreneauxLoiJournaliere {

  double creneau1deb = 0;
  double creneau1fin = 0;
  double creneau2deb = 0;
  double creneau2fin = 0;
  double creneau3deb = 0;
  double creneau3fin = 0;

}