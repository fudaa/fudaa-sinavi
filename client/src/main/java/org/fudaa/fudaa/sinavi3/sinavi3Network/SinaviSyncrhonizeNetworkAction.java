package org.fudaa.fudaa.sinavi3.sinavi3Network;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

import org.fudaa.fudaa.sinavi3.Sinavi3DataSimulation;
/**
 * Action in case something went wrong between modelisation  and data typing in the overall software.
 * @author Adrien
 *
 */
public class SinaviSyncrhonizeNetworkAction extends AbstractAction {
	SinaviNetwork network;
	Sinavi3DataSimulation datas;
	public SinaviSyncrhonizeNetworkAction(SinaviNetwork network, Sinavi3DataSimulation datas) {
		super("re-synchro donn�es simu");
		this.network = network;
		this.datas = datas;
	}
	
	public void actionPerformed(ActionEvent e) {
		
		network.synchronizeFromSinaviData(datas);
	}
	
	

}
