package org.fudaa.fudaa.sinavi3;

import java.util.ArrayList;

import org.fudaa.ebli.network.simulationNetwork.NetworkUserObject;

/**
 * 
 */

/**
 * Classe d�crivant le Tron�on:
 * 
 * @author Adrien Hadoux
 */
public class Sinavi3Bief implements NetworkUserObject{

  String nom_ = "";
  double longueur_ = 1;
  double largeur_= 1;
  double profondeur_ = -1;
  double vitesse_ = 1;
  Sinavi3Horaire h_ = new Sinavi3Horaire();

  /**
   * donn�es topologiques
   */
  int gareAmont_ = 0;
  /**
   * donn�es topologiques
   */

  int gareAval_ = 1;

  /**
   * regles de croisement du tron�on
   */
  Sinavi3StrutureReglesNavigation reglesCroisement_;

  /** Regles de trematage du tron�on **/
  Sinavi3StrutureReglesNavigation reglesTrematage_;
  
  double dureeIndispo_;
  double frequenceMoyenne_;

  // loi: indice de 1 a 10
  int loiIndispo_=1;
  int loiFrequence_=1;

  /**
   * Type de loi ENTIER QUI PREND LA VALEUR DE LA LOI CHOISIE.<br>: 0 => loi d erlang<br>
   * 1 => deterministe <br>
   * 2 => journaliere par defaut loi d'erlang
   */
  int typeLoi_;
  
  /**
   * Declaration du tableau de loi deterministe.
   */
  ArrayList loiDeterministe_ = new ArrayList();
  
  /**
   * constructeur: initialisation des regles de navigations cette methode doit etre appel�e des que l'on cr�e un nouevau
   * tron�on que l'on va stocker dans le vecteur de donn�es
   * 
   * @params numNavires. correspond au nombre de navires qui correspond a la dimension de la matrice:
   */
  public Sinavi3Bief(final int numNavires) {
    // initialisation de la matrice du tron�on
    this.reglesCroisement_ = new Sinavi3StrutureReglesNavigation(numNavires);
    this.reglesTrematage_ = new Sinavi3StrutureReglesNavigation(numNavires);
  }

  public Sinavi3Bief() {
	// TODO Auto-generated constructor stub
}

/**
   * methode d affichage des donn�es du tron�on
   */
  String[] affichage() {
    final String[] t = new String[3];
    t[0] = "nom: " + nom_;
    t[1] = "\n profondeur: " + (float) profondeur_;
 
    return t;

  }

public String getName() {
	
	return nom_;
}

public String getNom_() {
	return nom_;
}

public void setNom_(String nom_) {
	this.nom_ = nom_;
}

public double getLongueur_() {
	return longueur_;
}

public void setLongueur_(double longueur_) {
	this.longueur_ = longueur_;
}

public double getLargeur_() {
	return largeur_;
}

public void setLargeur_(double largeur_) {
	this.largeur_ = largeur_;
}

public double getProfondeur_() {
	return profondeur_;
}

public void setProfondeur_(double profondeur_) {
	this.profondeur_ = profondeur_;
}

public double getVitesse_() {
	return vitesse_;
}

public void setVitesse_(double vitesse_) {
	this.vitesse_ = vitesse_;
}

public Sinavi3Horaire getH_() {
	return h_;
}

public void setH_(Sinavi3Horaire h_) {
	this.h_ = h_;
}

public int getGareAmont_() {
	return gareAmont_;
}

public void setGareAmont_(int gareAmont_) {
	this.gareAmont_ = gareAmont_;
}

public int getGareAval_() {
	return gareAval_;
}

public void setGareAval_(int gareAval_) {
	this.gareAval_ = gareAval_;
}

public Sinavi3StrutureReglesNavigation getReglesCroisement_() {
	return reglesCroisement_;
}

public void setReglesCroisement_(
		Sinavi3StrutureReglesNavigation reglesCroisement_) {
	this.reglesCroisement_ = reglesCroisement_;
}

public Sinavi3StrutureReglesNavigation getReglesTrematage_() {
	return reglesTrematage_;
}

public void setReglesTrematage_(Sinavi3StrutureReglesNavigation reglesTrematage_) {
	this.reglesTrematage_ = reglesTrematage_;
}

public double getDureeIndispo_() {
	return dureeIndispo_;
}

public void setDureeIndispo_(double dureeIndispo_) {
	this.dureeIndispo_ = dureeIndispo_;
}

public double getFrequenceMoyenne_() {
	return frequenceMoyenne_;
}

public void setFrequenceMoyenne_(double frequenceMoyenne_) {
	this.frequenceMoyenne_ = frequenceMoyenne_;
}

public int getLoiIndispo_() {
	return loiIndispo_;
}

public void setLoiIndispo_(int loiIndispo_) {
	this.loiIndispo_ = loiIndispo_;
}

public int getLoiFrequence_() {
	return loiFrequence_;
}

public void setLoiFrequence_(int loiFrequence_) {
	this.loiFrequence_ = loiFrequence_;
}

public int getTypeLoi_() {
	return typeLoi_;
}

public void setTypeLoi_(int typeLoi_) {
	this.typeLoi_ = typeLoi_;
}

public ArrayList getLoiDeterministe_() {
	return loiDeterministe_;
}

public void setLoiDeterministe_(ArrayList loiDeterministe_) {
	this.loiDeterministe_ = loiDeterministe_;
}

public void setName(String value) {
	setNom_(value);
	
}

}
