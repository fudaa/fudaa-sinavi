package org.fudaa.fudaa.sinavi3;

import java.util.ArrayList;

import javax.swing.AbstractListModel;

public class Sinavi3ActionListModel extends AbstractListModel{
	
	ArrayList listeActions_;
	
	public Sinavi3ActionListModel(ArrayList listeActions){
		listeActions_=listeActions;
	}
	
	
	public Object getElementAt(int index) {
		// TODO Auto-generated method stub
		if(index==0)
			return "Initialisation";
		
//		-- On recupere la derniere action realisee --//	
		  Sinavi3ActionClicUSer action=(Sinavi3ActionClicUSer)listeActions_.get(index);
		
		if(action.compteur==action.nbTotalGares_)
			return " Schema complet ";
			
		return "Action " + index; 
	}

	public int getSize() {
		// TODO Auto-generated method stub
		return listeActions_.size();
	}

	
	public void miseAjour(){
   	 fireContentsChanged(this,0, getSize());
    }
}
