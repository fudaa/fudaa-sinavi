package org.fudaa.fudaa.sinavi3.ui.panel;

import java.util.List;

import javax.swing.JComboBox;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;

import jxl.write.Label;
import jxl.write.Number;
import jxl.write.WritableCell;

import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.table.CtuluTableModelInterface;
import org.fudaa.fudaa.sinavi3.Sinavi3DataSimulation;
import org.fudaa.fudaa.sinavi3.structures.DureeManeuvreEcluse.DureeParcours;

import com.memoire.bu.BuDialogError;

/**
 * Modele de tableau pour les maneuvres
 * @author Adrien
 *
 */
public class TableModelDureeManeuvre implements TableModel, CtuluTableModelInterface{

	private final Sinavi3DataSimulation data;
	private final JComboBox<String> dataToModify;
	public TableModelDureeManeuvre(Sinavi3DataSimulation data,JComboBox<String> dataToModify) {
		this.data = data;
		this.dataToModify = dataToModify;
	}

	public int getRowCount() {
		return data.getListeEcluse().getListeEcluses().size();
	}

	public int getColumnCount() {				
		return data.getListeBateaux_().getListeNavires_().size()+1;
	}

	public String getColumnName(int columnIndex) {		
		if(columnIndex == 0)
			return "Ecluse";
		columnIndex = columnIndex -1;
		return data.getListeBateaux_().getListeNavires_().get(columnIndex).getNom();
	}

	public Class<?> getColumnClass(int columnIndex) {				
		return Object.class;
	}

	public boolean isCellEditable(int rowIndex, int columnIndex) {
		if(columnIndex == 0)
			return false;
		return true;
	}

	public Object getValueAt(int rowIndex, int columnIndex) {
		if(columnIndex == 0)
			return data.getListeEcluse().getListeEcluses().get(rowIndex).getName();

		columnIndex = columnIndex-1;

		List<DureeParcours> durees = data.getDureeManeuvresEcluses().getListeDureesParcours().get(data.getListeEcluse().retournerEcluse(rowIndex));
		if(durees != null)
			return displayDuree(durees.get(columnIndex));
		return "";
	}

	public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
		if(columnIndex ==0)
			return;
		columnIndex = columnIndex-1;

		DureeParcours  duree = data.getDureeManeuvresEcluses().getListeDureesParcours().get(data.getListeEcluse().retournerEcluse(rowIndex)).get(columnIndex);
		setDuree(duree, aValue);
	}

	public void addTableModelListener(TableModelListener l) {
	}

	public void removeTableModelListener(TableModelListener l) {
	}


	public boolean checkData(Object data) {
		if(data == null) {
			new BuDialogError(this.data.getApplication().getApp(), this.data.getApplication().isSinavi_, "La valeur doit �tre non nulle.")
			.activate();
			return false;
		}

		try {
			String value = data.toString().replace(":", ".").replace(",",".");
			double val = Double.parseDouble(value);

			if(val <0 ) {
				new BuDialogError(this.data.getApplication().getApp(), this.data.getApplication().isSinavi_, "La valeur doit �tre positif.")
				.activate();
				return false;
			}

		}catch (NumberFormatException e) {
			new BuDialogError(this.data.getApplication().getApp(), this.data.getApplication().isSinavi_, "La valeur doit �tre un nombre r�el.")
			.activate();
			return false;
		}
		return true;
	}

	public void setDuree(final DureeParcours d, Object data) {

		if(!checkData(data)) 
			return;
          
		if(this.dataToModify.getSelectedIndex() == 0)
			d.dureeEntrant1 = Double.parseDouble( data.toString().replace(":", ".").replace(",","."));
		if(this.dataToModify.getSelectedIndex() == 1)
			d.dureeEntrant2 = Double.parseDouble( data.toString().replace(":", ".").replace(",","."));
		if(this.dataToModify.getSelectedIndex() == 2)
			d.dureeSortant1 = Double.parseDouble( data.toString().replace(":", ".").replace(",","."));
		if(this.dataToModify.getSelectedIndex() == 3)
			d.dureeSortant2 = Double.parseDouble( data.toString().replace(":", ".").replace(",","."));
	}

	public String displayDuree(final DureeParcours d) {
		if(this.dataToModify.getSelectedIndex() == 0)
			return ""+d.dureeEntrant1;
		if(this.dataToModify.getSelectedIndex() == 1)
			return ""+d.dureeEntrant2;
		if(this.dataToModify.getSelectedIndex() == 2)
			return ""+d.dureeSortant1;
		if(this.dataToModify.getSelectedIndex() == 3)
			return ""+d.dureeSortant2;

		return "";
	}

	public WritableCell getExcelWritable(final int _row, final int _col, int _rowXls, int _colXls) {

		final Object o = getValueAt(_row, _col);
		if (o == null) {
			return null;
		}
		String s = o.toString();
		if(CtuluLibString.isEmpty(s)) return null;
		try {
			return new Number(_colXls, _rowXls, Double.parseDouble(s));
		} catch (final NumberFormatException e) {}
		return new Label(_colXls, _rowXls, s);

	}

	public List<String> getComments() {
		
		return null;
	}

	public int getMaxCol() {
		
		return getColumnCount();
	}

	public int getMaxRow() {
		return data.getListeEcluse().getListeEcluses().size();
	}

	public Object getValue(int _row, int _col) {
		
		return getValueAt(_row, _col);
	}
        
           public int[] getSelectedRows() {
        return null;
    }


}
