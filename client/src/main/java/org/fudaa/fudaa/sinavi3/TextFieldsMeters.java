/*
 * @file         TextFieldsMeters.java
 * @creation     1999-10-01
 * @modification $Date: 2007-11-23 11:28:07 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.sinavi3;

import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import javax.swing.JComponent;

import com.memoire.bu.BuCharValidator;
import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuStringValidator;
import com.memoire.bu.BuTextField;
import com.memoire.bu.BuValueValidator;

/**
 * Cette classe permet la saisie des donn�es de type "m�tres, centim�tres". Il est impossible de saisir plus d'un point.
 * Les valeurs sont automatiquement arrondies � deux d�cimales.
 * 
 * @version $Revision: 1.1 $ $Date: 2007-11-23 11:28:07 $ by $Author: hadouxad $
 * @author Nicolas Chevalier
 */
public class TextFieldsMeters extends JComponent implements FocusListener {
  private final BuTextField zoneTexte = new BuTextField();
  private final BuLabel label = new BuLabel("m");

  /** Compte le nombre de points dans chaine. */
  int compteSeparateurs(final String chaine) {
    final int nbChar = chaine.length();
    int nbSepa = 0;
    for (int i = 0; i < nbChar; i++) {
      if (chaine.charAt(i) == '.') {
        nbSepa++;
      }
    }
    return nbSepa;
  }
  // Caract�res valides pour les metres : num�ro ou point
  final BuCharValidator CHAR_METERS = new BuCharValidator() {
    public boolean isCharValid(char _char) {
      boolean valid = false;
      if ((_char == '.') && (compteSeparateurs(zoneTexte.getText()) == 0)) {
        valid = true;
      } else if (Character.isDigit(_char)) {
        valid = true;
      }
      return valid;
    }
  };
  /** Valeurs valides pour les m�tres : double. */
  final BuValueValidator VALUE_METERS = new BuValueValidator() {
    public boolean isValueValid(Object _value) {
      if (_value instanceof Double) {
        if (((Double) _value).doubleValue() >= 0.0) {
          return true;
        }
      }
      return false;
    }
  };
  /** Chaines valides : doubles et vides (transform�es en 0.0 dans stringToValue). */
  final BuStringValidator STRING_METERS = new BuStringValidator() {
    public boolean isStringValid(String _string) {
      try {
        if (_string != null && _string.trim().length() > 0) {
          new Double(_string);
        }
      } catch (Exception e) {
        return false;
      }
      return true;
    }

    public Object stringToValue(String _string) {
      return new Double(_string.equals("") ? 0.0 : Double.parseDouble(_string));
    }

    public String valueToString(Object value) {
      int arrondi = Math.round(((Double) value).floatValue() * 100);
      return String.valueOf(((double) arrondi) / 100);
    }
  };

  /**
   * Construit une instance TextFieldsMeters.
   * 
   * @param value valeur avec laquele il faut initialiser la zone de texte.
   */
  public TextFieldsMeters(final double value) {
    super();
    // Initialisation de la zone de texte
    zoneTexte.setCharValidator(CHAR_METERS);
    zoneTexte.setValueValidator(VALUE_METERS);
    zoneTexte.setStringValidator(STRING_METERS);
    zoneTexte.setColumns(5);
    zoneTexte.addFocusListener(this);
    // Layout manager pour le composant
    final BuGridLayout lm = new BuGridLayout(2, 5, 5, false, false);
    setLayout(lm);
    // Ajout des composants
    add(zoneTexte);
    add(label);
    // Initialisation de la valeur
    setValue(value);
  }

  /** Construit une instance de TextFieldsMeters initialis�e � z�ro. */
  public TextFieldsMeters() {
    this(0.0);
  }

  /**
   * Construit une instance de TextFieldsMeters.
   * 
   * @param m nombre initial de m�tres.
   * @param cm nombre initial de centim�tres.
   */
  public TextFieldsMeters(final int m, final int cm) {
    this(m + ((double) cm) / 100);
  }

  /** Quand on entre dans la zone de texte c'est que l'on entre dans le composant. */
  public void focusGained(final FocusEvent e) {
    zoneTexte.selectAll();
    processFocusEvent(new FocusEvent(this, FocusEvent.FOCUS_GAINED));
  }

  /** Quand on sort de la zone de texte c'est que l'on sort du composant. */
  public void focusLost(final FocusEvent e) {
    processFocusEvent(new FocusEvent(this, FocusEvent.FOCUS_LOST));
  }

  /** Surcharge de setEnabled pour agir sur le label et la zone de texte. */
  public void setEnabled(final boolean flag) {
    zoneTexte.setEnabled(flag);
    label.setEnabled(flag);
  }

  /** Pour lire la valeur de la zone de texte. */
  public double toDouble() {
    return ((Double) zoneTexte.getValue()).doubleValue();
  }

  /** Pour lire la valeur de la zone de texte. */
  public double getValue() {
    return toDouble();
  }

  /**
   * Pour d�finir la valeur de la zone de texte.
   * 
   * @param value nouvelle valeur.
   */
  public void setValue(final double value) {
    if (value >= 0.0) {
      zoneTexte.setValue(new Double(value));
    } else {
      throw (new IllegalArgumentException("Valeurs n�gatives interdites dans un TextFieldsMeters"));
    }
  }
}
