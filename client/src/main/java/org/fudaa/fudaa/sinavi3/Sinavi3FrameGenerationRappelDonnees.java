package org.fudaa.fudaa.sinavi3;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.ImageIcon;
import javax.swing.JCheckBox;
import javax.swing.JColorChooser;
import javax.swing.JDialog;
import javax.swing.JEditorPane;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JTree;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeSelectionModel;

import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.gui.CtuluHtmlEditorPanel;
import org.fudaa.fudaa.ressource.FudaaResource;
import com.memoire.bu.BuBorderLayout;
import com.memoire.bu.BuButton;
import com.memoire.bu.BuDialogError;
import com.memoire.bu.BuDialogMessage;
import com.memoire.bu.BuFormLayout;
import com.memoire.bu.BuPanel;

/**
 * classe qui g�n�re un rappel des diff�rentes donn�es.
 * 
 * @version $Version$
 * @author hadoux
 */
//FIXME Fred ActionListener deja implemente par SiporInternalFrame
public class Sinavi3FrameGenerationRappelDonnees extends Sinavi3InternalFrame implements /*ActionListener, */TreeSelectionListener {

	Sinavi3DataSimulation donnees_;

	/**
	 * arbre hierarchique contenant les infos a mettre en valeur
	 */
	JTree arbre_;


	final JScrollPane visionneurArbre;
	/**
	 * Editeur de texte html
	 */
	JEditorPane htmlPane;

	private final BuButton quitter_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_quitter"), "Quitter");
	private final BuButton exportationHTML_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_generer"),
	"Exporter");
	private final BuButton choisirCouleur_ = new BuButton("couleur sommaire");
	private final BuButton apercu_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_previsualiser"),"Aper�u");
	/**
	 * panel contenant les boutons de commande.
	 */
	BuPanel controlPanel_ = new BuPanel();

	
	String fichierImagePort_="";
	
	
	/**
	 * panel contenant les informations � selectionner ce panel se modifie en fonction du choix de l'utilisateur dans la
	 * hierarchie de l'arbre
	 */
	BuPanel selectionPanel_ = new BuPanel();
	JSplitPane conteneur;
	// liste des composants de personalisation du rapport
	JTextField auteur_ = new JTextField(20);
	JTextField titre_ = new JTextField(20);
	JTextArea ZoneText_ = new JTextArea(2, 30);

	JCheckBox dg_ = new JCheckBox("Pr�senter les donn�es g�n�rales", true);
	JCheckBox dgNbJ_ = new JCheckBox("Afficher le nombre de jours de la simulation", true);
	JCheckBox dgGraine_ = new JCheckBox("Afficher la graine d'initialisation", true);
	
	JCheckBox gare_ = new JCheckBox("Pr�senter les gares", true);
	
	
	JCheckBox topo2_ = new JCheckBox("Pr�senter la topologie des tron�ons", true);
	
	JCheckBox topo4_ = new JCheckBox("Pr�senter la topologie des ecluses", true);
	JCheckBox topo5_ = new JCheckBox("Pr�senter le mod�le du r�seau", true);

	JCheckBox regle1_ = new JCheckBox("Pr�senter les r�gles de croisement des tron�ons", true);
	JCheckBox regle2_ = new JCheckBox("Pr�senter les r�gles de trematage des tron�ons", true);
	JCheckBox regle3_ = new JCheckBox("Pr�senter les vitesses des bateaux dans les tron�ons", true);
	JCheckBox regle4_ = new JCheckBox("Pr�senter les dur�es de manoeuvres dans les �cluses", true);

	JCheckBox nav_ = new JCheckBox("Pr�senter les cat�gories de bateaux", true);
	JCheckBox navPrio_ = new JCheckBox("priorite", true);
	JCheckBox navGare_ = new JCheckBox("gare de depart", true);
	JCheckBox navLong_ = new JCheckBox("longueur", true);
	JCheckBox navLarg_ = new JCheckBox("largeur", true);
	JCheckBox navTE_ = new JCheckBox("tirant d eau", true);
	
	JCheckBox navCr_ = new JCheckBox("creneaux", false);
	JCheckBox navVitesses_ = new JCheckBox("vitesses", false);

	JCheckBox chenal_ = new JCheckBox("Pr�senter les tron�ons", true);
	JCheckBox chenalPro_ = new JCheckBox("Hauteur d'eau du tron�on", true);
	JCheckBox chenalMar_ = new JCheckBox("Vitesse autorisee", true);
	JCheckBox chenalCr_ = new JCheckBox("inserer les creneaux", true);

	JCheckBox ecluse_ = new JCheckBox("Pr�senter les �cluses", true);
	JCheckBox ecltaille_ = new JCheckBox("caracteristique de l ecluse", true);
	JCheckBox eclDur_ = new JCheckBox("duree d eclusee, fausse bassinnee", true);
	JCheckBox ecluseIndispo_ = new JCheckBox("les indisponibilites", true);
	JCheckBox ecluseCr_ = new JCheckBox("inserer les creneaux", true);

	
	Color couleurSommaire;
	String couleurTableau = "#AACCEE";
	String couleurLegende = "#EEEEEE";


	CtuluHtmlEditorPanel editeur=new CtuluHtmlEditorPanel();
	/**
	 * constructeur de la classe
	 * 
	 * @param _donnees
	 */

	Sinavi3FrameGenerationRappelDonnees(final Sinavi3DataSimulation _donnees) {
		super("Rappel des donn�es", true, true, true, true);

		donnees_ = _donnees;
		this.setSize(720, 630);
		setBorder(Sinavi3Bordures.compound_);
		this.getContentPane().setLayout(new BorderLayout());

		/*******************************************************************************************************************
		 * Affichage de l'arbre et de son contenu
		 ******************************************************************************************************************/
		final DefaultMutableTreeNode sommet = new DefaultMutableTreeNode("Contenu de la note");
		arbre_ = new JTree(sommet);

		DefaultMutableTreeNode categorie;
		DefaultMutableTreeNode feuille;
		// premiere categorie de l arbre
		categorie = new DefaultMutableTreeNode("Pr�sentation");
		sommet.add(categorie);
		categorie = new DefaultMutableTreeNode("Donn�es g�n�rales");
		sommet.add(categorie);

		categorie = new DefaultMutableTreeNode("Param�tres");
		sommet.add(categorie);
	/*	feuille = new DefaultMutableTreeNode("Gares");
		categorie.add(feuille);
		feuille = new DefaultMutableTreeNode("Biefs");
		categorie.add(feuille);
		
		feuille = new DefaultMutableTreeNode("Ecluses");
		categorie.add(feuille);
	
		feuille = new DefaultMutableTreeNode("Cat�gories de bateaux");
		categorie.add(feuille);
	*/	categorie = new DefaultMutableTreeNode("R�seau");
		sommet.add(categorie);
		
	/*	feuille = new DefaultMutableTreeNode("Topologie des biefs");
		categorie.add(feuille);
		
		feuille = new DefaultMutableTreeNode("Topologie des �cluses");
		categorie.add(feuille);
		feuille = new DefaultMutableTreeNode("Mod�lisation du r�seau");
		categorie.add(feuille);
	*/	categorie = new DefaultMutableTreeNode("Navigation");
		sommet.add(categorie);
	/*	feuille = new DefaultMutableTreeNode("Croisement dans les tron�on");
		categorie.add(feuille);
		feuille = new DefaultMutableTreeNode("Tr�matage dans les tron�on");
		categorie.add(feuille);
		feuille = new DefaultMutableTreeNode("Vitesse dans les tron�on");
		categorie.add(feuille);
		feuille = new DefaultMutableTreeNode("Dur�es de manoeuvres dans les �cluses");
		categorie.add(feuille);
	 */
		arbre_.expandRow(0);
		// arbre_.expandRow(1);
		// arbre_.expandRow(3);
		// arbre_.expandRow(4);
		// ajout de l arbre dans un ascenceur
		visionneurArbre = new JScrollPane(arbre_);
		selectionPanel_.add(visionneurArbre, BorderLayout.WEST);

		// listener de l'arbre
		arbre_.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);

		arbre_.addTreeSelectionListener(this);

		/*******************************************************************************************************************
		 * Affichage du panel de boutons
		 ******************************************************************************************************************/
		this.quitter_.setToolTipText("Ferme la sous-fen�tre");
		this.quitter_.addActionListener(this);
		exportationHTML_.setToolTipText("Exporte le rapport au format HTML");
		exportationHTML_.setEnabled(false);
		apercu_.setToolTipText("G�n�re un aper�u du rapport");
		exportationHTML_.addActionListener(this);
		apercu_.addActionListener(this);
		choisirCouleur_.addActionListener(this);
		quitter_.addActionListener(new ActionListener() {
			public void actionPerformed(final ActionEvent e) {
				Sinavi3FrameGenerationRappelDonnees.this.windowClosed();

			}
		});

		controlPanel_.add(quitter_);
		controlPanel_.add(apercu_);
		controlPanel_.add(exportationHTML_);
		
		// controlPanel_.add(this.choisirCouleur_);
		this.add(controlPanel_, BorderLayout.SOUTH);
		/*******************************************************************************************************************
		 * remplissage panneau central
		 ******************************************************************************************************************/
		// actualisation du panneau de donn�es
		auteur_.setText("Inconnu");
		this.titre_.setText("Simulation de trafic");
		
		
		//conteneur.add(selectionPanel_);
		
		
		//-- incrustation de l'editeur dans le panel principal --//
		JPanel conteneurEditeur=new JPanel(new BorderLayout());
		
		//ajout du panel d'affichage de l'editeur
		conteneurEditeur.add(editeur,BorderLayout.CENTER);
		//ajout de la toolbar de l'editeur
		conteneurEditeur.add(editeur.getToolBar(true), BuBorderLayout.NORTH);
		//conteneur.add(conteneurEditeur);
		conteneurEditeur.setBorder(BorderFactory.createTitledBorder(Sinavi3Bordures.compound_, "Affichage du rapport"));
		//ajout de la barre de menu de l'editeur
		setJMenuBar(editeur.getMenuBar());
		
		
		//-- splitpane --//
		conteneur = new JSplitPane(JSplitPane.VERTICAL_SPLIT, selectionPanel_,conteneurEditeur);
		conteneur.setDividerLocation(230);
		conteneur.setDividerSize(1);
		this.getContentPane().add(conteneur, BorderLayout.CENTER);
		actualiser("Contenu de la note");

	}

	/**
	 * Methode d'actualisation des donn�es.
	 */

	public void actualiser(final String choix) {

		TitledBorder bordurea = BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED),
		"personalisation");
		final JPanel table1 = new JPanel(new FlowLayout(FlowLayout.LEFT));
		final JPanel table2 = new JPanel(new FlowLayout(FlowLayout.LEFT));
		final JPanel table3 = new JPanel(new FlowLayout(FlowLayout.LEFT));
		final JPanel table4 = new JPanel(new FlowLayout(FlowLayout.LEFT));
		final JPanel table5 = new JPanel(new FlowLayout(FlowLayout.LEFT));
		final JPanel table6 = new JPanel();
		final JPanel table7 = new JPanel();
		final JPanel table8 = new JPanel();
		final JPanel table9 = new JPanel();
		final JPanel table10 = new JPanel();
		final JPanel table11 = new JPanel();
		final JPanel table12 = new JPanel();
		final JPanel table13 = new JPanel();

		String titre = "truc";
		this.selectionPanel_.removeAll();
		this.selectionPanel_.setBorder(BorderFactory.createTitledBorder(Sinavi3Bordures.compound_, "Param�tres du rapport"));
		// this.selectionPanel_=new BuPanel();
		this.selectionPanel_.setLayout(new BorderLayout());
		selectionPanel_.add(visionneurArbre, BorderLayout.WEST);
		// format tableau pour affichage des caract�ristiques
		Box table = Box.createVerticalBox();
		this.selectionPanel_.add(/*new JScrollPane(*/table/*)*/, BorderLayout.CENTER);


		dg_.addActionListener(this);
		nav_.addActionListener(this);
		chenal_.addActionListener(this);
		ecluse_.addActionListener(this);
		
		if (choix.equals("Donn�es g�n�rales")) {

			//table.setLayout(new GridLayout(10, 1));
			table.add(table1);
			table.add(table2);
			table.add(table3);
			table1.add(dg_);
			table2.add(dgNbJ_);
			table3.add(dgGraine_);
			
			bordurea = BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED),
					"Donn�es g�n�rales");
			table.setBorder(bordurea);
		}  else if (choix.equals("Contenu de la note"))
		{
			titre = "Contenu de la note";

			//table.setLayout(new GridLayout(5, 1));

			table1.add(new JLabel("Pr�sentation"));
			table.add(table1);
			table2.add(new JLabel("Donn�es g�n�rales"));
			table.add(table2);
			table3.add(new JLabel("Param�tres"));
			table.add(table3);
			table4.add(new JLabel("R�seau"));
			table.add(table4);
			table5.add(new JLabel("Navigation"));
			table.add(table5);

			bordurea = BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED), titre);
			table.setBorder(bordurea);

		}  else if (choix.equals("Pr�sentation"))
		{
			titre = "Pr�sentation";

			//table.setLayout(new GridLayout(5, 1));

			table1.add(new JLabel("Titre:"));
			table1.add(this.titre_);
			table.add(table1);

			table2.add(new JLabel("Auteur:"));
			table2.add(this.auteur_);
			table.add(table2);
			table3.add(new JLabel("Commentaire:"));
			ZoneText_.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED));
			table3.add(ZoneText_);
			table.add(table3);
			bordurea = BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED), titre);
			table.setBorder(bordurea);

		} else if (choix.equals("Param�tres"))
		{
			titre = "Param�tres";
			table.add(table1);
			table.add(table2);
			table.add(table3);
			table.add(table4);
			table1.add(gare_);
			table2.add(chenal_);
			table3.add(ecluse_);
			table4.add(nav_);
			bordurea = BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED), titre);
			table.setBorder(bordurea);
			
		} else if (choix.equals("R�seau"))
		{
			titre = "R�seau";
			table.add(table1);
			table.add(table2);
			table.add(table3);
			table1.add(topo2_);
			table2.add(topo4_);
			table3.add(topo5_);
			bordurea = BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED), titre);
			table.setBorder(bordurea);
			
		} else if (choix.equals("Navigation"))
		{
			titre = "Navigation";
			table.add(table1);
			table.add(table2);
			table.add(table3);
			table1.add(regle1_);
			table2.add(regle2_);
			table3.add(regle3_);
			bordurea = BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED), titre);
			table.setBorder(bordurea);
			
		} else if (choix.equals("Gares"))// panel de selection des gares
		{
			titre = "Donn�es relatives aux gares";
			//table.setLayout(new GridLayout(5, 1));
			table.add(table1);
			//table.add(table2);
			//table.add(table3);
			//table.add(table4);
			//table.add(table5);
			table1.add(gare_);
			bordurea = BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED), titre);
			table.setBorder(bordurea);
			
		}  else if (choix.equals("Biefs")) {

			//table.setLayout(new GridLayout(10, 1));
			table.add(table1);
			table1.add(chenal_);
			/*
			table.add(chenalPro_);
			table.add(chenalMar_);
			table.add(chenalCr_);
			*/
			bordurea = BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED),
					"Donn�es relatives aux tron�ons");
			table.setBorder(bordurea);
		} else

			if (choix.equals("Ecluses")) {

				//table.setLayout(new GridLayout(10, 1));
				table.add(table1);
				table1.add(ecluse_);
				/*table.add(ecltaille_);
				table.add(eclDur_);
				table.add(ecluseIndispo_);
				table.add(ecluseCr_);
				 */
				bordurea = BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED),
						"Donn�es relatives aux �cluses");
				table.setBorder(bordurea);
			} 
			 else

				if (choix.equals("Topologie des tron�ons"))// panel de selection des gares
				{
					titre = "Topologie des tron�ons";
					//table.setLayout(new GridLayout(5, 1));
					table.add(table1);
					//table.add(table2);
					//table.add(table3);
					//table.add(table4);
					//table.add(table5);
					table1.add(topo2_, BorderLayout.CENTER);
					bordurea = BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED), titre);
					table.setBorder(bordurea);

				} else if (choix.equals("Topologie des �cluses"))// panel de selection des gares
				{
					titre = "Topologie des �cluses";
					//table.setLayout(new GridLayout(5, 1));
					table.add(table1);
					/*table.add(table2);
					table.add(table3);
					table.add(table4);
					table.add(table5);*/
					table1.add(topo4_, BorderLayout.CENTER);
					bordurea = BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED), titre);
					table.setBorder(bordurea);

				} else if (choix.equals("Mod�le du r�seau"))// panel de selection des gares
				{
					titre = "Mod�lisation du r�seau";
					//table.setLayout(new GridLayout(5, 1));
					table.add(table1);
					/*table.add(table2);
					table.add(table3);
					table.add(table4);
					table.add(table5);*/
					table1.add(topo5_, BorderLayout.CENTER);
					bordurea = BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED), titre);
					table.setBorder(bordurea);

				} else if (choix.equals("Tr�matage dans les tron�ons"))// panel de selection des gares
				{
					titre = "Tr�matage dans les tron�ons";
					//table.setLayout(new GridLayout(5, 1));
					table.add(table1);
					/*table.add(table2);
					table.add(table3);
					table.add(table4);
					table.add(table5);*/
					table1.add(regle2_, BorderLayout.CENTER);
					bordurea = BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED), titre);
					table.setBorder(bordurea);

				} else if (choix.equals("Croisement dans les tron�ons"))// panel de selection des gares
				{
					titre = "Croisement dans les tron�ons";
					//table.setLayout(new GridLayout(5, 1));
					table.add(table1);
					/*table.add(table2);
					table.add(table3);
					table.add(table4);
					table.add(table5);*/
					table1.add(regle1_, BorderLayout.CENTER);
					bordurea = BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED), titre);
					table.setBorder(bordurea);

				} else if (choix.equals("Dur�es de manoeuvres dans les �cluses"))// panel de selection des gares
				{
					titre = "Dur�es de manoeuvres dans les �cluses";
					//table.setLayout(new GridLayout(5, 1));
					table.add(table1);
					/*table.add(table2);
					table.add(table3);
					table.add(table4);
					table.add(table5);*/
					table1.add(regle4_, BorderLayout.CENTER);
					bordurea = BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED), titre);
					table.setBorder(bordurea);

				} else if (choix.equals("Vitesse dans les tron�ons"))// panel de selection des gares
				{
					titre = "Vitesse des bateaux dans les tron�ons";
					//table.setLayout(new GridLayout(5, 1));
					table.add(table1);
					/*table.add(table2);
					table.add(table3);
					table.add(table4);
					table.add(table5);*/
					table1.add(regle3_, BorderLayout.CENTER);
					bordurea = BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED), titre);
					table1.setBorder(bordurea);

				} else if (choix.equals("Cat�gories de bateaux")) {
					bordurea = BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED),
					"Param�tres cat�gories de bateaux");
					table.setBorder(bordurea);
					//table.setLayout(new GridLayout(13, 1));
					table.add(nav_);
					/*
					table.add(navPrio_);
					table.add(navGare_);
					table.add(navLong_);
					table.add(navLarg_);
					table.add(navTE_);
					table.add(navCr_);
					table.add(navVitesses_);*/
				}

		final JPanel aide = new JPanel(new FlowLayout(FlowLayout.LEFT));
		//aide.setLayout(new GridLayout(2, 1));
		//final JPanel l1 = new JPanel();
		ImageIcon im = new ImageIcon();
		im = FudaaResource.FUDAA.getIcon("crystal_commentaire");
		final JLabel image = new JLabel(im);
		aide.add(image);
		aide.add(new JLabel("Vous pouvez ici d�finir les informations � faire figurer dans le rapport."));
		//final JPanel l2 = new JPanel();
		//l2.add(new JLabel("puis cliquez sur le bouton d'exportation."));
		//aide.add(l1);
		//aide.add(l2);
		// aide.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED));
		this.selectionPanel_.add(aide, BorderLayout.NORTH);
	



		this.selectionPanel_.validate();
		this.validate();

	}

	/**
	 * methode qui ecoute le listener de l'arbre
	 */
	public void valueChanged(final TreeSelectionEvent e) {
		final DefaultMutableTreeNode node = (DefaultMutableTreeNode) arbre_.getLastSelectedPathComponent();

		if (node == null) {
			return;
		}

		// recupere le contenu du noeud qui est une chaine de caractere
		final String nodeInfo = (String) node.getUserObject();

		// si c est une feuille
	//	if (node.isLeaf()) {

			// on recherche la feuille selectionn�e et on modifie le panel en cons�quence:

			actualiser(nodeInfo);

			// displayURL(book.bookURL);
	//	}
	}

	public void actionPerformed(final ActionEvent ev) {

	 if (ev.getSource().equals(ecluse_)) {
			if (ecluse_.isSelected()) {

				ecltaille_.setEnabled(true);
				eclDur_.setEnabled(true);
				ecluseIndispo_.setEnabled(true);
				ecluseCr_.setEnabled(true);

				ecltaille_.setSelected(true);
				eclDur_.setSelected(true);
				ecluseIndispo_.setSelected(true);
				ecluseCr_.setSelected(false);

			} else {

				ecltaille_.setEnabled(false);
				eclDur_.setEnabled(false);
				ecluseIndispo_.setEnabled(false);
				ecluseCr_.setEnabled(false);

				ecltaille_.setSelected(false);
				eclDur_.setSelected(false);
				ecluseIndispo_.setSelected(false);
				ecluseCr_.setSelected(false);

			}
		} else if (ev.getSource().equals(chenal_)) {
			if (chenal_.isSelected()) {
				chenalPro_.setEnabled(true);
				chenalMar_.setEnabled(true);
				chenalCr_.setEnabled(true);
				chenalPro_.setSelected(true);
				chenalMar_.setSelected(true);
				chenalCr_.setSelected(true);

			} else {
				chenalPro_.setEnabled(false);
				chenalMar_.setEnabled(false);
				chenalCr_.setEnabled(false);
				chenalPro_.setSelected(false);
				chenalMar_.setSelected(false);
				chenalCr_.setSelected(false);

			}
		} else

			if (ev.getSource().equals(dg_)) {
				if (dg_.isSelected()) {
					dgNbJ_.setEnabled(true);
					dgGraine_.setEnabled(true);
					dgNbJ_.setSelected(true);
					dgGraine_.setSelected(true);
					
				} else {
					dgNbJ_.setEnabled(false);
					dgGraine_.setEnabled(false);
					dgNbJ_.setSelected(false);
					dgGraine_.setSelected(false);
					
				}
			} else if (ev.getSource().equals(nav_)) {
				if (nav_.isSelected()) {
					navPrio_.setEnabled(true);
					navGare_.setEnabled(true);
					navLong_.setEnabled(true);
					navLarg_.setEnabled(true);
					navTE_.setEnabled(true);
					navCr_.setEnabled(true);
					navVitesses_.setEnabled(true);

					navPrio_.setSelected(true);
					navGare_.setSelected(true);
					navLong_.setSelected(true);
					navLarg_.setSelected(true);
					navTE_.setSelected(true);
					navCr_.setSelected(false);
					navVitesses_.setSelected(false);
				} else {
					navPrio_.setEnabled(false);
					navGare_.setEnabled(false);
					navLong_.setEnabled(false);
					navLarg_.setEnabled(false);
					navTE_.setEnabled(false);
					navCr_.setEnabled(false);
					navVitesses_.setEnabled(false);
					navPrio_.setSelected(false);
					navGare_.setSelected(false);
					navLong_.setSelected(false);
					navLarg_.setSelected(false);
					navTE_.setSelected(false);
						navCr_.setSelected(false);
					navVitesses_.setSelected(false);
				}
			} else
				if (ev.getSource() == apercu_) {
					Sinavi3GenereNoteHtml.apercuRapport(this, this.donnees_);
					exportationHTML_.setEnabled(true);
				}
				else

					if (ev.getSource() == exportationHTML_) {
						// creation du rapport:
						final JFileChooser fc = new JFileChooser();
						final int returnVal = fc.showSaveDialog(Sinavi3FrameGenerationRappelDonnees.this);

						
						
						
						if (returnVal == JFileChooser.APPROVE_OPTION) {
							// creation du rapport
							//SiporGenereNoteHtml.rappelDonnees(fc.getSelectedFile(), this, this.donnees_);
							//ecriture du contenu du rapport g�n�r�
							 
						     try {
						    	
						    	 //-- ETAPE 1: recuperer le contenu de l'editeur html --//
						    	 String contenuEditeur=editeur.getDocumentText();
						    	
						    	 //-- Etape 2: supprimer le fichier temporaire de dessin du port  --//
						    	 //String ancienCodeHtmlFichierPort=SiporGenereNoteHtml.creerModele(donnees_,this,this.fichierImagePort_);
							    	
						    	 (new File(this.fichierImagePort_)).delete();
						    	 
						    	 //-- ETAPE 3: recuperer le fichier du rapport a g�n�rer --//
						    	File file =CtuluLibFile.appendExtensionIfNeeded(fc.getSelectedFile(), "html");
						       
						       //-- ETAPE 4: g�n�rer l'image du port en ajoutant une extension .jpg --//
						    	String nouveauCodeHtmlFichierPort=Sinavi3GenereNoteHtml.creerModele(donnees_,this,file.getAbsolutePath()+".jpg");
						    	nouveauCodeHtmlFichierPort=nouveauCodeHtmlFichierPort.substring(0, nouveauCodeHtmlFichierPort.indexOf("src=")+5)+file.getName()+".jpg\">"+nouveauCodeHtmlFichierPort.substring(nouveauCodeHtmlFichierPort.indexOf("</img>"), nouveauCodeHtmlFichierPort.length());
						    	
						    	//-- ETAPE 5: realiser un matching sur le rapport de sipor: --//
						    	//--	      le but est de remplacer la ligne de commande d'insertion de l'image --//
						    	String nouveauContenuEditeur=contenuEditeur.substring(0, contenuEditeur.indexOf("<img")-1)+nouveauCodeHtmlFichierPort+contenuEditeur.substring(contenuEditeur.indexOf(this.fichierImagePort_)+fichierImagePort_.length()+31, contenuEditeur.length());
						    	
						    	//-- ETAPE 6: enregistrement du nouveau fichier html r�sultata du matching --//
						       final FileWriter fw = new FileWriter(file);
						       fw.write(nouveauContenuEditeur, 0, nouveauContenuEditeur.length());
						       fw.flush();
						       fw.close();
						     } catch (final IOException _e1) {
						    	 new BuDialogError(donnees_.application_.getApp(),donnees_.application_.isSinavi_,
						    			 "Impossible d'�crire le fichier sur le disque.")
									.activate();
						      
						    	 return ;
						     }
							// ouverture de la note
							final File file = CtuluLibFile.appendExtensionIfNeeded(fc.getSelectedFile(), "html");

							new BuDialogMessage(donnees_.application_.getApp(), Sinavi3Implementation.isSinavi_,"Fichier correctement g�n�r� � l'emplacement\n"+file.getAbsolutePath()).activate();


						}
						
						//editeur.getTextPane().getText();
						//editeur.getDocumentText()
						
						
						
					} else if (ev.getSource() == choisirCouleur_) {
						final JColorChooser coulSommaire = new JColorChooser();

						final JDialog fen = coulSommaire.createDialog(null, "couleur du sommaire", true, coulSommaire, null, null);
						fen.show();

						couleurSommaire = coulSommaire.getColor();

						this.setBackground(coulSommaire.getColor());
						this.selectionPanel_.setBackground(coulSommaire.getColor());
						this.selectionPanel_.validate();
						this.controlPanel_.setBackground(coulSommaire.getColor());
						this.controlPanel_.validate();
						this.validate();

					}

	}

	/*
	 * private void displayURL(URL url) { try { if (url != null) { htmlPane.setPage(url); } else { //null url
	 * htmlPane.setText("File Not Found"); this.getContentPane().add(htmlPane,BorderLayout.CENTER); this.validate(); } }
	 * catch (IOException e) { System.err.println("Attempted to read a bad URL: " + url); } }
	 */

	protected void windowClosed() {
		// verif.stop(); //stop n est aps sur on le modifie donc par une autre variable

		// desactivation du thread
		// dureeVieThread=false;

		System.out.print("Fin de la fenetre de gestion des bassins!!");
		dispose();
	}
}