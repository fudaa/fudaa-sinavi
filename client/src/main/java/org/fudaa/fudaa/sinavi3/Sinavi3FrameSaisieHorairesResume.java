package org.fudaa.fudaa.sinavi3;

/**
 * 
 */

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.Border;

import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluUIAbstract;
import org.fudaa.fudaa.ressource.FudaaResource;

import com.memoire.bu.BuButton;

/**
 * Fenetre de saisie des crneaux d'horaires standart.
 * 
 * @author Adrien Hadoux
 */
public class Sinavi3FrameSaisieHorairesResume extends Sinavi3InternalFrame {

  // attributs

  /**
   * Panel global contenant toutes les données de la frame.
   */
  JPanel global_;

  /**
   * JText du premier horaire a saisir.
   */
  Sinavi3TextFieldDureeJournee creneau1debut_ = new Sinavi3TextFieldDureeJournee(3);
  Sinavi3TextFieldDureeJournee creneau1fin_ = new Sinavi3TextFieldDureeJournee(3);
  Sinavi3TextFieldDureeJournee creneau2debut_ = new Sinavi3TextFieldDureeJournee(3);
  Sinavi3TextFieldDureeJournee creneau2fin_ = new Sinavi3TextFieldDureeJournee(3);
  Sinavi3TextFieldDureeJournee creneau3debut_ = new Sinavi3TextFieldDureeJournee(3);
  Sinavi3TextFieldDureeJournee creneau3fin_ = new Sinavi3TextFieldDureeJournee(3);

  /**
   * Bouton de validation des horaires lance les tests decontroles de cohrence des donnes.
   */
  final BuButton validation_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_oui"), "valider");

  // bordures
  Border raisedBevel_ = BorderFactory.createRaisedBevelBorder();
  Border loweredBevel_ = BorderFactory.createLoweredBevelBorder();
  Border compound_ = BorderFactory.createCompoundBorder(raisedBevel_, loweredBevel_);
  Border bordnormal_ = BorderFactory.createEtchedBorder();

  /**
   * Objet horaire qui sera rempli suite a la saisie de l utilisateur.
   */
  Sinavi3Horaire horaire_;

  /**
   * Constructeur de la fenetre de saisie des horaires.
   * 
   * @param _h horaire qui va etre rempli par l'utilisateur
   */

  Sinavi3FrameSaisieHorairesResume(final Sinavi3Horaire _h) {

    super(CtuluLibString.EMPTY_STRING, true, true, true, true);
    horaire_ = _h;
    setTitle("Saisie d'un horaire");
    setSize(415, 180);
    setBorder(BorderFactory.createCompoundBorder(BorderFactory.createRaisedBevelBorder(), BorderFactory.createLoweredBevelBorder()));
    
    // listener du bouton de validation:
    validation_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent _e) {
        System.out.print("test de cohrence de saisie des donnes: ");
        // lancement de la fonction de validation des dones saisies:
        creationHoraire();
      }
    });

    this.creneau1debut_.setToolTipText(getTooltipOuverture());
    this.creneau2debut_.setToolTipText(getTooltipOuverture());
    this.creneau1fin_.setToolTipText(getTooltipFermeture());
    this.creneau2fin_.setToolTipText(getTooltipFermeture());
    this.creneau3fin_.setToolTipText(getTooltipFermeture());
    this.creneau3debut_.setToolTipText(getTooltipOuverture());

    initCreneauLabels();
    

    // Organisation des donnes dans la frame:
    buildPanelGlobal();
    getContentPane().add(global_);

    // affichage de la fenetre
    setVisible(true);

  }

  private void buildPanelGlobal() {
    global_ = new JPanel();
    global_.setLayout(new GridLayout(4, 1));

    // panel de saisie du premier creneau horaire
    final JPanel ch1 = new JPanel();

    ch1.add(new JLabel(" Creneau 1: Horaire: "));
    ch1.add(this.creneau1debut_);
    ch1.add(new JLabel("�"));
    ch1.add(this.creneau1fin_);
    ch1.add(new JLabel("HEURES:MINUTES"));
    ch1.setBorder(bordnormal_);
    global_.add(ch1);

    // panel de saisie du deuxieme horaire
    final JPanel ch2 = new JPanel();
    ch2.add(new JLabel(" Creneau 2: Horaire: "));
    ch2.add(this.creneau2debut_);
    ch2.add(new JLabel("�"));
    ch2.add(this.creneau2fin_);
    ch2.add(new JLabel("HEURES:MINUTES"));
    ch2.setBorder(bordnormal_);

    global_.add(ch2);

    // //panel de saisie du troisieme horaire
    final JPanel ch4 = new JPanel();
    ch4.add(new JLabel(" Creneau 3: Horaire: "));
    ch4.add(this.creneau3debut_);
    ch4.add(new JLabel("�"));
    ch4.add(this.creneau3fin_);
    ch4.add(new JLabel("HEURES:MINUTES"));
    ch4.setBorder(bordnormal_);

    global_.add(ch4);

    // panel qui contient le bouton de validation:
    final JPanel ch3 = new JPanel();
    ch3.add(new JLabel("Cliquez ici pour valider: "));
    ch3.add(this.validation_);
    ch3.setBorder(bordnormal_);
    global_.add(ch3);
    global_.setBorder(compound_);
  }

  

 
  

  private void initCreneauLabels() {
    if (horaire_.semaineCreneau1HeureArrivee != -1 && horaire_.semaineCreneau2HeureArrivee != -1
        && horaire_.semaineCreneau3HeureArrivee != -1 && horaire_.semaineCreneau1HeureDep != -1
        && horaire_.semaineCreneau2HeureDep != -1 && horaire_.semaineCreneau3HeureDep != -1) {
      // valeur recuperee de la structure horaire
      this.creneau1debut_.setText(CtuluLibString.EMPTY_STRING + (float) horaire_.semaineCreneau1HeureDep);
      this.creneau1fin_.setText(CtuluLibString.EMPTY_STRING + (float) horaire_.semaineCreneau1HeureArrivee);
      this.creneau2debut_.setText(CtuluLibString.EMPTY_STRING + (float) horaire_.semaineCreneau2HeureDep);
      this.creneau2fin_.setText(CtuluLibString.EMPTY_STRING + (float) horaire_.semaineCreneau2HeureArrivee);
      this.creneau3debut_.setText(CtuluLibString.EMPTY_STRING + (float) horaire_.semaineCreneau3HeureDep);
      this.creneau3fin_.setText(CtuluLibString.EMPTY_STRING + (float) horaire_.semaineCreneau3HeureArrivee);

    } else {
      // valeur par defaut:
      this.creneau1debut_.setText(getZero());
      this.creneau1fin_.setText("24.0");
      this.creneau2debut_.setText(getZero());
      this.creneau2fin_.setText(getZero());
      this.creneau3debut_.setText(getZero());
      this.creneau3fin_.setText(getZero());
    }
  }

  private String getTooltipFermeture() {
    return "Saisissez le nombre correspondant � l'horaire de fermeture. Par exemple, si vous voulez rentrer 8h46, tapez 8.46";
  }

  private String getTooltipOuverture() {
    return "Saisissez le nombre correspondant � l'horaire d'ouverture. Par exemple, si vous voulez rentrer 8h46, tapez 8.46";
  }

  String getZero() {
    return "0.0";
  }

  /**
   * Methode booleene de validation des donnes saisies.
   */
  boolean controleCreationHoraire() {

    if (this.creneau1debut_.getText().equals(CtuluLibString.EMPTY_STRING)) {
      JOptionPane.showMessageDialog(null, "Creneau 1: heure de depart manquant.",
          CtuluUIAbstract.getDefaultWarnTitle(), JOptionPane.ERROR_MESSAGE);
      return false;
    }
    if (this.creneau1fin_.getText().equals(CtuluLibString.EMPTY_STRING)) {
      JOptionPane.showMessageDialog(null, "Creneau 1: heure de fin manquant.", CtuluUIAbstract.getDefaultWarnTitle(),
          JOptionPane.ERROR_MESSAGE);
      return false;
    }

    if (this.creneau2debut_.getText().equals(CtuluLibString.EMPTY_STRING)) {
      JOptionPane.showMessageDialog(null, "Creneau 2: heure de depart manquant.",
          CtuluUIAbstract.getDefaultWarnTitle(), JOptionPane.ERROR_MESSAGE);
      return false;
    }
    if (this.creneau2fin_.getText().equals(CtuluLibString.EMPTY_STRING)) {
      JOptionPane.showMessageDialog(null, "Creneau 2: heure de fin manquant.", CtuluUIAbstract.getDefaultWarnTitle(),
          JOptionPane.ERROR_MESSAGE);
      return false;
    }

    // tous les tests ont t ngatifs, les donnes sont donc correctes.

    if (verifCoherenceCreneaux(CtuluLibString.EMPTY_STRING, Float.parseFloat(this.creneau1debut_.getText()), Float
        .parseFloat(this.creneau1fin_.getText()), Float.parseFloat(this.creneau2debut_.getText()), Float
        .parseFloat(this.creneau2fin_.getText()), Float.parseFloat(this.creneau3debut_.getText()), Float
        .parseFloat(this.creneau3fin_.getText()))) {
      return true;
    }
    return false;
  }

  /**
   * methode de verifiation de la saisie des horaires.
   * 
   * @param _texte texte qui specifie de quel horairez il s agit
   * @param _a1 creneau 1 debut
   * @param _a2 creneau 1 fin
   * @param _b1 creneau 2 debut
   * @param _b2 creneau 2 fin
   * @param _c1 creneau 3 debut
   * @param _c2 creneau 3 fin
   * @return
   */
  boolean verifCoherenceCreneaux(final String _texte, final float _a1, final float _a2, final float _b1,
      final float _b2, final float _c1, final float _c2) {
    String errHoraires = "Erreur horaires " + _texte;
    if (!verifCoherenceHoraires(_a1, _a2, _b1, _b2, _c1, _c2, errHoraires)) return false;

    // inclusions des horaires:
    if (isCreneauWrong(_a1, _a2, _b1, _b2)) {
      JOptionPane.showMessageDialog(null, errHoraires + "Le creneau 2 est inclus dans le creneau 1",
          CtuluUIAbstract.getDefaultWarnTitle(), JOptionPane.ERROR_MESSAGE);
      return false;
    }

    if (isCreneauWrong(_c1, _c2, _b1, _b2)) {
      JOptionPane.showMessageDialog(null, errHoraires + "Le creneau 2 est inclus dans le creneau 3",
          CtuluUIAbstract.getDefaultWarnTitle(), JOptionPane.ERROR_MESSAGE);
      return false;
    }

    if (isCreneauWrong(_a1, _a2, _c1, _c2)) {
      JOptionPane.showMessageDialog(null, errHoraires + "Le creneau 3 est inclus dans le creneau 1",
          CtuluUIAbstract.getDefaultWarnTitle(), JOptionPane.ERROR_MESSAGE);
      return false;
    }
    else
    	if(_a1==_a2 && _b1==_b2 && _c1==_c2){
    		JOptionPane.showMessageDialog(null,
    				errHoraires+"Toutes les valeurs sont identiques. Ce ne sont pas des cr�neaux.", "Avertissement",
      	          JOptionPane.ERROR_MESSAGE);
      	      return false;
    		
    	}
    	else
    		if(_a1>_b2 && _b2!=0){
    			JOptionPane.showMessageDialog(null,errHoraires+"Le creneau 1 doit �tre inf�rieur au cr�neau 2.","Avertissement", JOptionPane.ERROR_MESSAGE);
    			return false;
    		}
    		else if(_a1>_c2 && _c2!=0){
    			JOptionPane.showMessageDialog(null,errHoraires+"Le creneau 1 doit �tre inf�rieur au cr�neau 3.","Avertissement", JOptionPane.ERROR_MESSAGE);
    			return false;
    		}
    		else if(_b1>_c2 && _c2!=0){
    			JOptionPane.showMessageDialog(null,errHoraires+"Le creneau 2 doit �tre inf�rieur au cr�neau 3.","Avertissement", JOptionPane.ERROR_MESSAGE);
    			return false;
    		}
    		else
    			
    return true;
  }

  private boolean isCreneauWrong(final float _a1, final float _a2, final float _b1, final float _b2) {
    return (_b1 < _a2 && _b1 > _a1) || (_b2 < _a2 && _b2 > _a1) || (_a1 > _b1 && _a1 < _b2) || (_a2 > _b1 && _a2 < _b2);
  }

  private boolean verifCoherenceHoraires(final float _a1, final float _a2, final float _b1, final float _b2,
      final float _c1, final float _c2, String _errHoraires) {
    if (_a1 > _a2) {
      JOptionPane.showMessageDialog(null, _errHoraires + "!\n Creneau 1: horaire depart superieur a celui d arrivee",
          CtuluUIAbstract.getDefaultWarnTitle(), JOptionPane.ERROR_MESSAGE);
      return false;
    } else if (_b1 > _b2) {
      JOptionPane.showMessageDialog(null, _errHoraires + "!\n Creneau 2: horaire depart superieur a celui d arrivee",
          CtuluUIAbstract.getDefaultWarnTitle(), JOptionPane.ERROR_MESSAGE);
      return false;
    } else if (_c1 > _c2) {
      JOptionPane.showMessageDialog(null, _errHoraires + "!\n Creneau 3: horaire depart superieur a celui d arrivee",
          CtuluUIAbstract.getDefaultWarnTitle(), JOptionPane.ERROR_MESSAGE);
      return false;
    }

    return true;
  }

  /**
   * Methode de controle de creation des horaires.
   */

  void creationHoraire() {

    if (controleCreationHoraire()) {

      // creation d'un nouvel objet horaire:
      horaire_.semaineCreneau1HeureDep = Float.parseFloat(this.creneau1debut_.getText());
      horaire_.semaineCreneau1HeureArrivee = Float.parseFloat(this.creneau1fin_.getText());
      horaire_.semaineCreneau2HeureDep = Float.parseFloat(this.creneau2debut_.getText());
      horaire_.semaineCreneau2HeureArrivee = Float.parseFloat(this.creneau2fin_.getText());
      horaire_.semaineCreneau3HeureDep = Float.parseFloat(this.creneau3debut_.getText());
      horaire_.semaineCreneau3HeureArrivee = Float.parseFloat(this.creneau3fin_.getText());

      // verification des donnes saisies:
      horaire_.affichage();

      // destruction de la frame:
      this.dispose();

    }

  }

  public static void showInvalidNumberErreur() {
    JOptionPane.showMessageDialog(null, "Ce nombre est invalide.", CtuluUIAbstract.getDefaultWarnTitle(),
        JOptionPane.ERROR_MESSAGE);
  }

  public static void erreurHoraireInvalide() {
    JOptionPane.showMessageDialog(null, "Cet horaire doit etre compris entre 0 et 24.", CtuluUIAbstract
        .getDefaultWarnTitle(), JOptionPane.ERROR_MESSAGE);
  }

  public static void erreurHoraireNegatif() {
    JOptionPane.showMessageDialog(null, "Cet horaire est n�gatif.", CtuluUIAbstract.getDefaultWarnTitle(),
        JOptionPane.ERROR_MESSAGE);
  }

}
