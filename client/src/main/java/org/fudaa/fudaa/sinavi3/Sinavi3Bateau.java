package org.fudaa.fudaa.sinavi3;

import java.util.ArrayList;

import org.fudaa.ctulu.CtuluLibString;

/**
 * Classe de base de dfinition des Navires avec leur nom et differentes caractristiques
 * 
 * @author Adrien Hadoux
 */

public class Sinavi3Bateau {

  // definition des attributs de Navire

  /**
   * Nom de la catgorie de Navire.
   */
  String nom = CtuluLibString.EMPTY_STRING;

  /**
   * Priorite de la categorie: 2 valeur 0 ou 1 Par defaut 1.
   */
  int priorite = 1;

  
  /**
   * Longueur du bateaux : entier
   */
  double longueurMax = -1;

  double largeurMax = -1;
  
  double vitesseDefautAvalant =1;
  double vitesseDefautMontant =1;
  
  
  double dureeAttenteMaximaleAdmissible=0;
  
  /**
   * gare de depart correspond a l'indice de la gare dans le vecteur de gares de datasimulation apr defaut correspond a
   * la gare de rade: la gare 0
   */
 // ArrayList gareDepart_ = new ArrayList();
//  ArrayList gareArrivee_= new ArrayList();
  /**Sens: 0= avalant, 1=montant**/
  //ArrayList sens_= new ArrayList();
  
  
  
  /**
   * Reel qui correspond au Tirant d'eau en entre
   */
  double tirantEau = -1;

  /**
   * Tirant d eau:
   * C; charg�
   * L; lege
   */
  String typeTirant = "C";



 
  Sinavi3Horaire creneauxJournaliers_ = new Sinavi3Horaire();
  //Sinavi3Horaire creneauxouverture_ = new Sinavi3Horaire();

  
  
  /** -- TRAJETS -- **/
  Sinavi3Trajets listeTrajet_=new Sinavi3Trajets();
  
  
  /**
   * Type de loi Loi de g�n�ration des navires 0 => loi d erlang 1 => deterministe 2 => journaliere par defaut loi
   * d'erlang.
   */
  //ArrayList typeLoiGenerationNavires_=new ArrayList() ;

  /**
   * loi d erlang typeLoi=0
   */
  //ArrayList loiErlangGenerationNavire = new ArrayList();
  //ArrayList nbBateauxattendus = new ArrayList();
  //double ecartMoyenEntre2arrivees = 0;

  /**
   * Declaration du tableau de loi deterministe et journaliere:
   */
  //ArrayList loiDeterministe_ = new ArrayList();


 
  /**
   * constructeur par defaut
   */
  public Sinavi3Bateau() {

  }

  /**
   * Constructeur avec parametre d'entree: le nom de la categorie de Navire
   */
  Sinavi3Bateau(final String nom) {
    this.nom = nom;
  }

  /**
   * Methode de verification des donnes saisies Verifie que toutes les donnes ont t saisies:
   * 
   * @return oui si le navire est saisi completement
   */

  boolean navireCompletementSaisi() {

    // test de la non vacuit du nom du navire
    if (nom == "") {
      return false;
    }

    return true;
  }

  /**
   * Methode qui verifie si des incohrences ont lieu dans toutes les donnes relative aux catgories de navire
   */

  boolean verifCoherenceDonnees() {
    if (this.vitesseDefautAvalant <= 0) {
      return false;
    }
   
    if (this.tirantEau == -1) {
      return false;
    }
    

  
    return true;
  }

public String getNom() {
	return nom;
}

public void setNom(String nom) {
	this.nom = nom;
}

public int getPriorite() {
	return priorite;
}

public void setPriorite(int priorite) {
	this.priorite = priorite;
}

public double getLongueurMax() {
	return longueurMax;
}

public void setLongueurMax(double longueurMax) {
	this.longueurMax = longueurMax;
}

public double getLargeurMax() {
	return largeurMax;
}

public void setLargeurMax(double largeurMax) {
	this.largeurMax = largeurMax;
}

public double getVitesseDefautAvalant() {
	return vitesseDefautAvalant;
}

public void setVitesseDefautAvalant(double vitesseDefautAvalant) {
	this.vitesseDefautAvalant = vitesseDefautAvalant;
}

public double getVitesseDefautMontant() {
	return vitesseDefautMontant;
}

public void setVitesseDefautMontant(double vitesseDefautMontant) {
	this.vitesseDefautMontant = vitesseDefautMontant;
}

public double getTirantEau() {
	return tirantEau;
}

public void setTirantEau(double tirantEau) {
	this.tirantEau = tirantEau;
}

public String getTypeTirant() {
	return typeTirant;
}

public void setTypeTirant(String typeTirant) {
	this.typeTirant = typeTirant;
}

public Sinavi3Horaire getCreneauxJournaliers_() {
	return creneauxJournaliers_;
}

public void setCreneauxJournaliers_(Sinavi3Horaire creneauxJournaliers_) {
	this.creneauxJournaliers_ = creneauxJournaliers_;
}

public Sinavi3Trajets getListeTrajet_() {
	return listeTrajet_;
}

public void setListeTrajet_(Sinavi3Trajets listeTrajet_) {
	this.listeTrajet_ = listeTrajet_;
}

public double getDureeAttenteMaximaleAdmissible() {
	return dureeAttenteMaximaleAdmissible;
}

public void setDureeAttenteMaximaleAdmissible(
		double dureeAttenteMaximaleAdmissible) {
	this.dureeAttenteMaximaleAdmissible = dureeAttenteMaximaleAdmissible;
}

 
}
