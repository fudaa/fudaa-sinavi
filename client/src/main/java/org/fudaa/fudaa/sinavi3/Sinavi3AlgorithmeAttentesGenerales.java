package org.fudaa.fudaa.sinavi3;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

import org.fudaa.dodico.corba.sinavi3.SParametresResultatsAttente;
import org.fudaa.dodico.corba.sinavi3.SParametresResultatsAttenteCategorie;
import org.fudaa.dodico.corba.sinavi3.SParametresResultatsCompletSimulation;
import org.fudaa.dodico.corba.sinavi3.Sinavi3ResultatConsoEau;
import org.fudaa.dodico.corba.sinavi3.Sinavi3ResultatGarage;
import org.fudaa.dodico.corba.sinavi3.Sinavi3ResultatsDonneeTrajet;

public class Sinavi3AlgorithmeAttentesGenerales {

	/**
	 * metode de calcul de la totalit� des temps d attente dans tous les sens de la simulation. effectue tous ces calculs
	 * des fin du lancement de la simulation
	 * 
	 * @param _donnees
	 * @param nomfichier
	 */
	public static void calculApresSimu(final Sinavi3DataSimulation _donnees) {
		/*
		 * rangements des diff�rents type d'�l�ment dans le tableau: indiceType == 0 =>"chenal" indiceType == 1 =>"ecluse" 
		 */

		/*
		 * sens: 0: sens amont vers aval: on ne cherche que les attentes dans les gares amont de lelement 1: sens aval vers
		 * amont: on ne cherche els temps que dans les gares aval de l element 2: les 2 sens : on cumule les attentes a la
		 * fois des gares amont et aval.
		 */

		final SParametresResultatsCompletSimulation resultats = _donnees.params_.ResultatsCompletsSimulation;
		int compteurElementsAttente;

		/*******************************************************************************************************************
		 * CALCUL DES ATTENTES DANS LE SENS AVALANT
		 ******************************************************************************************************************/
		// allocation de m�moire pour autant d elements qu il y a eu de saisies
		resultats.AttentesTousElementsToutesCategoriesSens1 = new SParametresResultatsAttente[ _donnees.listeEcluse_.listeEcluses_.size()
		                                                                                      + _donnees.listeBief_.listeBiefs_.size() ];

		compteurElementsAttente = 0;

		// chenal
		for (int i = 0; i < _donnees.listeBief_.listeBiefs_.size(); i++) {
			resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente] = new SParametresResultatsAttente();
			calculeAttentesElement(_donnees, resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente], 0, i, 0);
			compteurElementsAttente++;
		}

		// ecluse
		for (int i = 0; i < _donnees.listeEcluse_.listeEcluses_.size(); i++) {
			resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente] = new SParametresResultatsAttente();
			calculeAttentesElement(_donnees, resultats.AttentesTousElementsToutesCategoriesSens1[compteurElementsAttente], 0, i, 1);
			compteurElementsAttente++;
		}
		
		/*******************************************************************************************************************
		 * CALCUL DES ATTENTES DANS LE SENS MONTANT
		 ******************************************************************************************************************/
		// allocation de m�moire pour autant d elements qu il y a eu de saisies
		resultats.AttentesTousElementsToutesCategoriesSens2 = new SParametresResultatsAttente[	_donnees.listeEcluse_.listeEcluses_.size()
		                                                                                      + _donnees.listeBief_.listeBiefs_.size()];
		compteurElementsAttente = 0;

		// chenal
		for (int i = 0; i < _donnees.listeBief_.listeBiefs_.size(); i++) {
			resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente] = new SParametresResultatsAttente();
			calculeAttentesElement(_donnees, resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente], 1, i, 0);
			compteurElementsAttente++;
		}

		// ecluse
		for (int i = 0; i < _donnees.listeEcluse_.listeEcluses_.size(); i++) {
			resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente] = new SParametresResultatsAttente();
			calculeAttentesElement(_donnees, resultats.AttentesTousElementsToutesCategoriesSens2[compteurElementsAttente], 1, i, 1);
			compteurElementsAttente++;
		}
		
		/*******************************************************************************************************************
		 * CALCUL DES ATTENTES DANS LES 2 SENS CUMULES
		 ******************************************************************************************************************/
		// allocation de m�moire pour autant d elements qu il y a eu de saisies
		resultats.AttentesTousElementsToutesCategoriesLes2Sens = new SParametresResultatsAttente[ _donnees.listeEcluse_.listeEcluses_.size()
		                                                                                      + _donnees.listeBief_.listeBiefs_.size() ];

		compteurElementsAttente = 0;

		// chenal
		for (int i = 0; i < _donnees.listeBief_.listeBiefs_.size(); i++) {
			resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente] = new SParametresResultatsAttente();
			calculeAttentesElement(_donnees, resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente], -1, i, 0);
			compteurElementsAttente++;
		}

		// ecluse
		for (int i = 0; i < _donnees.listeEcluse_.listeEcluses_.size(); i++) {
			resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente] = new SParametresResultatsAttente();
			calculeAttentesElement(_donnees, resultats.AttentesTousElementsToutesCategoriesLes2Sens[compteurElementsAttente], -1, i, 1);
			compteurElementsAttente++;
		}

		// on met par defaut les attentes en entr�es dans l'�l�ment:
		_donnees.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories = _donnees.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategoriesSens1;

	}

	/**
	 * Methode statique qui ne fais que charger la structure en fonction du sens donn�e en effet tous les calculs ont �t�
	 * �ffectu�s apr�s simulation.
	 * 
	 * @param donnees_
	 * @param sensCirculation
	 * @param heureDep
	 * @param heureFin
	 */
	public static void calcul(final Sinavi3DataSimulation donnees_, final int sensCirculation) {

		if (sensCirculation == 0) {
			// sens entrant: alors on charge la structure des r�sultats dans le sens entrant
			donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories = donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategoriesSens1;
		} else if (sensCirculation == 1) {
			// sens sortant: alors on charge la structure des r�sultats dans le sa�ns sortant
			donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories = donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategoriesSens2;

		} else if (sensCirculation == 2) {
			// deux sens cumul�s
			donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories = donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategoriesLes2Sens;
		}
	}// fin methode statique de calcul


	public static int[] existenceTrajet(final int typeElementDepart, final int typeElementArrivee,
			final int elementDepart, final int elementArrivee, final Sinavi3ResultatsDonneeTrajet[] trajet) {
		final int[] t = new int[2];
		// on initialise le tableau avec les indices non trouv�s
		t[0] = -1;
		t[1] = -1;
		
		int	init=-1;
		boolean trouve=false;

		//--Depart all� --//
		for (int i = 0; i < trajet.length && !trouve; i++) {
			
			// des qu on a trouv� les elements de depart et d'arrivee, alors on peut quitter
			// si l'�lement de depart est equivalent � l'�l�ment du trajet alors on a trouv� notre depart
			if (typeElementDepart == trajet[i].typeElement && elementDepart == trajet[i].indiceElement) {
				t[0] = i;
				trouve=true;
			}

		}

		trouve=false;

		//--arriv�e all� --//
		for (int i = 0; i < trajet.length && !trouve; i++) {

			// si l'�lement de depart est equivalent � l'�l�ment du trajet alors on a trouv� notre depart
			if (typeElementArrivee == trajet[i].typeElement && elementArrivee == trajet[i].indiceElement) {
				t[1] = i;
				trouve=true;
			}
		}

		// verification que l'utilisateur a bien entr� son element de depart =>ie qu il se trouve bien avant l'�l�ment
		// d'arriv�e
		// sinon l'algorithme le remet dans l'ordre:

		if (t[0] > t[1]) {
			final int temp = t[0];
			t[0] = t[1];
			t[1] = temp;
		}

		return t;
	}

	/**
	 * methode qui calcule les attentes sur un trajet donn� par l'utilisateur:
	 * 
	 * @param typeDepart
	 * @param indiceElementDep
	 * @param typeArrivee
	 * @param indiceElementAr
	 * @param sens
	 */
	public static void calculTrajet(final Sinavi3DataSimulation donnees_, final int typeElementDepart,
			final int elementDepart, final int typeElementArrivee, final int elementArrivee, final int sens) {


		/*
		 * sens: 0: sens aval:
		 */

		final SParametresResultatsCompletSimulation resultats = donnees_.params_.ResultatsCompletsSimulation;

		// allocation de m�moire pour un element: cet element est un trajet
		resultats.tableauAttenteTrajet = new SParametresResultatsAttente();

		resultats.tableauAttenteTrajet.tableauAttenteCategories = new SParametresResultatsAttenteCategorie[donnees_.listeBateaux_.listeNavires_.size()];
		// initialisation de tous les parametres d'attente
		for (int k = 0; k < donnees_.listeBateaux_.listeNavires_.size(); k++) {
			resultats.tableauAttenteTrajet.tableauAttenteCategories[k] = new SParametresResultatsAttenteCategorie();
			initialiseParams(resultats.tableauAttenteTrajet.tableauAttenteCategories[k]);
		}
		// boucle sur l historique et pour chaque ligne, on remplit les donn�es dans la bonne case grace a l indice de
		// cat�gorie

		for (int k = 0; k < donnees_.listeResultatsSimu_.nombreNavires; k++) {
			if(donnees_.listeResultatsSimu_.listeEvenements[k].sens==sens) {//--Si le sens est le bon --//
				
				int[] t = new int[2];
				int Depart = 0;
				int Arrivee = 0;
				//int Depart2=0;
				//int Arrivee2=0;
				// on recherche dans le trajet du navire si le sous trajet existe
				t = Sinavi3AlgorithmeAttentesGenerales.existenceTrajet(typeElementDepart, typeElementArrivee, elementDepart,
						elementArrivee, donnees_.listeResultatsSimu_.listeEvenements[k].tableauTrajet);
				// la m�thode retourne les indices des positions depart et arrivee du le tableau de trajet dans t
				if ( t[0] != -1 && t[1] != -1){
				
					Depart = t[0];
					Arrivee = t[1];
				
					double ATTENTE_TOTALE_ACCES = 0;
					double ATTENTE_TOTALE_SECURITE = 0;
					double ATTENTE_TOTALE_OCCUPATION = 0;
					double ATTENTE_TOTALE_INDISPONIBILITE = 0;
					final int n = donnees_.listeResultatsSimu_.listeEvenements[k].categorie;
					//System.out.println("depart= "+Depart+" TypeElement="+typeElementDepart+ "// Arrivee= "+Arrivee+" TypeElement="+typeElementDepart+ "//NbElementsTrajet= "+donnees_.listeResultatsSimu_.listeEvenements[k].NbElemtnsParcours+" SENS= "+sens+ " et\n  T[0]="+t[0]+"T[1]="+t[1]+"T[2]="+t[2]+"T[3]="+t[3]);
					// on comptabilise les attentes entre les bornes depart et arrivee

					//-- cas aller --//
				
					for (int i = Depart; i < Arrivee + 1; i++) // on a v�rifi� dans l'autre fonction que depart<arrivee
					{ 
						// on comptabilise les attentes dans le cas du sens entrant ou sortant
						// ou alors is c 'est dans les 2 sens mais il s'agit d'un quai donc pas de retour a comptabiliser
						ATTENTE_TOTALE_ACCES += donnees_.listeResultatsSimu_.listeEvenements[k].tableauTrajet[i].acces;
						ATTENTE_TOTALE_SECURITE += donnees_.listeResultatsSimu_.listeEvenements[k].tableauTrajet[i].secu;
						ATTENTE_TOTALE_OCCUPATION += donnees_.listeResultatsSimu_.listeEvenements[k].tableauTrajet[i].occupation;
						ATTENTE_TOTALE_INDISPONIBILITE += donnees_.listeResultatsSimu_.listeEvenements[k].tableauTrajet[i].indispo;
					}
			

					// on augente le nombre de navires de la cat�gorie n
					resultats.tableauAttenteTrajet.tableauAttenteCategories[n].nombreNaviresTotal++;
					// on ajoute les attentes totales du trajet et on determine le max, min et le nombre de navires qui ont attendus
					// attente acces
					resultats.tableauAttenteTrajet.tableauAttenteCategories[n].attenteAccesTotale += ATTENTE_TOTALE_ACCES;
					if (ATTENTE_TOTALE_ACCES != 0) {
						resultats.tableauAttenteTrajet.tableauAttenteCategories[n].nbNaviresAttenteAcces++;
					}
					if (ATTENTE_TOTALE_ACCES > resultats.tableauAttenteTrajet.tableauAttenteCategories[n].attenteAccesMaxi) {
						resultats.tableauAttenteTrajet.tableauAttenteCategories[n].attenteAccesMaxi = ATTENTE_TOTALE_ACCES;
					}
					if (ATTENTE_TOTALE_ACCES < resultats.tableauAttenteTrajet.tableauAttenteCategories[n].attenteAccesMini) {
						resultats.tableauAttenteTrajet.tableauAttenteCategories[n].attenteAccesMini = ATTENTE_TOTALE_ACCES;
					}
					// attente secu
					resultats.tableauAttenteTrajet.tableauAttenteCategories[n].attenteSecuTotale += ATTENTE_TOTALE_SECURITE;
					if (ATTENTE_TOTALE_SECURITE != 0) {
						resultats.tableauAttenteTrajet.tableauAttenteCategories[n].nbNaviresAttenteSecu++;
					}
					if (ATTENTE_TOTALE_SECURITE > resultats.tableauAttenteTrajet.tableauAttenteCategories[n].attenteSecuMaxi) {
						resultats.tableauAttenteTrajet.tableauAttenteCategories[n].attenteSecuMaxi = ATTENTE_TOTALE_SECURITE;
					}
					if (ATTENTE_TOTALE_SECURITE < resultats.tableauAttenteTrajet.tableauAttenteCategories[n].attenteSecuMini) {
						resultats.tableauAttenteTrajet.tableauAttenteCategories[n].attenteSecuMini = ATTENTE_TOTALE_SECURITE;
					}
					// attente occup
					resultats.tableauAttenteTrajet.tableauAttenteCategories[n].attenteOccupTotale += ATTENTE_TOTALE_OCCUPATION;
					if (ATTENTE_TOTALE_OCCUPATION != 0) {
						resultats.tableauAttenteTrajet.tableauAttenteCategories[n].nbNaviresAtenteOccup++;
					}
					if (ATTENTE_TOTALE_OCCUPATION > resultats.tableauAttenteTrajet.tableauAttenteCategories[n].attenteOccupMaxi) {
						resultats.tableauAttenteTrajet.tableauAttenteCategories[n].attenteOccupMaxi = ATTENTE_TOTALE_OCCUPATION;
					}
					if (ATTENTE_TOTALE_OCCUPATION < resultats.tableauAttenteTrajet.tableauAttenteCategories[n].attenteOccupMini) {
						resultats.tableauAttenteTrajet.tableauAttenteCategories[n].attenteOccupMini = ATTENTE_TOTALE_OCCUPATION;
					}
					// attente indisponibilit�
					resultats.tableauAttenteTrajet.tableauAttenteCategories[n].attentePanneTotale += ATTENTE_TOTALE_INDISPONIBILITE;
					if (ATTENTE_TOTALE_INDISPONIBILITE != 0) {
						resultats.tableauAttenteTrajet.tableauAttenteCategories[n].nbNaviresAttentePanne++;
					}
					if (ATTENTE_TOTALE_INDISPONIBILITE > resultats.tableauAttenteTrajet.tableauAttenteCategories[n].attentePanneMaxi) {
						resultats.tableauAttenteTrajet.tableauAttenteCategories[n].attentePanneMaxi = ATTENTE_TOTALE_INDISPONIBILITE;
					}
					if (ATTENTE_TOTALE_INDISPONIBILITE < resultats.tableauAttenteTrajet.tableauAttenteCategories[n].attentePanneMini) {
						resultats.tableauAttenteTrajet.tableauAttenteCategories[n].attentePanneMini = ATTENTE_TOTALE_INDISPONIBILITE;
					}

					// attente totale:
					final double dureeTotale = ATTENTE_TOTALE_OCCUPATION + ATTENTE_TOTALE_INDISPONIBILITE + ATTENTE_TOTALE_SECURITE
					+ ATTENTE_TOTALE_ACCES;
					resultats.tableauAttenteTrajet.tableauAttenteCategories[n].attenteMegaTotale += dureeTotale;
					if (dureeTotale != 0) {
						resultats.tableauAttenteTrajet.tableauAttenteCategories[n].nbNaviresAttenteTotale++;
					}
					if (dureeTotale > resultats.tableauAttenteTrajet.tableauAttenteCategories[n].attenteTotaleMaxi) {
						resultats.tableauAttenteTrajet.tableauAttenteCategories[n].attenteTotaleMaxi = dureeTotale;
					}
					if (dureeTotale < resultats.tableauAttenteTrajet.tableauAttenteCategories[n].attenteTotaleMini) {
						resultats.tableauAttenteTrajet.tableauAttenteCategories[n].attenteTotaleMini = dureeTotale;
					}

				}// fin du si le sous trajet appartient au trajet du navire

			}//-- fin test du sens
			
		}//-- fin boucle historique

		for (int n = 0; n < donnees_.listeBateaux_.listeNavires_.size(); n++) {
			if (resultats.tableauAttenteTrajet.tableauAttenteCategories[n].attenteAccesMini == 9999999) {
				resultats.tableauAttenteTrajet.tableauAttenteCategories[n].attenteAccesMini = 0;
			}
			if (resultats.tableauAttenteTrajet.tableauAttenteCategories[n].attenteSecuMini == 9999999) {
				resultats.tableauAttenteTrajet.tableauAttenteCategories[n].attenteSecuMini = 0;
			}
			if (resultats.tableauAttenteTrajet.tableauAttenteCategories[n].attenteOccupMini == 9999999) {
				resultats.tableauAttenteTrajet.tableauAttenteCategories[n].attenteOccupMini = 0;
			}
			if (resultats.tableauAttenteTrajet.tableauAttenteCategories[n].attentePanneMini == 9999999) {
				resultats.tableauAttenteTrajet.tableauAttenteCategories[n].attentePanneMini = 0;
			}
			if (resultats.tableauAttenteTrajet.tableauAttenteCategories[n].attenteTotaleMini == 9999999) {
				resultats.tableauAttenteTrajet.tableauAttenteCategories[n].attenteTotaleMini = 0;
			}
		}
		
		// Calcul des distributions d'attentes
		// On est oblig� de refaire une boucle sur les bateaux de la simulation car on a besoin des attentes maxi donn�es par la boucle pr�c�dente
		int m = 0;
	    int nbIntervalles = resultats.nombreIntervallesDistribution;
	    int valeurIntervalleDistribution = Sinavi3GestionResultats.determineValeurIntervalleDistribution(determineAttenteMaxTrajet(donnees_), nbIntervalles);
	    resultats.tableauAttenteTrajet.valeurIntervalleDistribution = valeurIntervalleDistribution;
	    for (int i = 0; i < donnees_.listeBateaux_.listeNavires_.size(); i++) {
	        // allocation memoire
	    	resultats.tableauAttenteTrajet.tableauAttenteCategories[i].attenteTotaleDistrib = new short[nbIntervalles];
	    	resultats.tableauAttenteTrajet.tableauAttenteCategories[i].attenteAccesDistrib = new short[nbIntervalles];
	    	resultats.tableauAttenteTrajet.tableauAttenteCategories[i].attenteOccupDistrib = new short[nbIntervalles];
	    	resultats.tableauAttenteTrajet.tableauAttenteCategories[i].attentePanneDistrib = new short[nbIntervalles];
	    	resultats.tableauAttenteTrajet.tableauAttenteCategories[i].attenteSecuDistrib = new short[nbIntervalles];
	    }
	        
	    for (int k = 0; k < donnees_.listeResultatsSimu_.nombreNavires; k++) {
	    	if(donnees_.listeResultatsSimu_.listeEvenements[k].sens==sens) {//--Si le sens est le bon --//
					
	    		int[] t = new int[2];
	    		int depart = 0;
	    		int arrivee = 0;
	    		// on recherche dans le trajet du navire si le sous trajet existe
	    		t = Sinavi3AlgorithmeAttentesGenerales.existenceTrajet(typeElementDepart, typeElementArrivee, elementDepart,
	    				elementArrivee, donnees_.listeResultatsSimu_.listeEvenements[k].tableauTrajet);
	    		// la m�thode retourne les indices des positions depart et arrivee du le tableau de trajet dans t (-1 si le trajet n'existe pas)
	    		if ( t[0] != -1 && t[1] != -1){
	    			
	    			depart = t[0];
	    			arrivee = t[1];
	    			double attenteAcces = 0;
	    			double attenteSecu = 0;
	    			double attenteOccup = 0;
	    			double attenteIndispo = 0;
	    			double attenteTotale = 0;
	    			final int n = donnees_.listeResultatsSimu_.listeEvenements[k].categorie;
	    			
	    			for (int i = depart; i < arrivee + 1; i++)
	    			{ 
	    				attenteAcces += donnees_.listeResultatsSimu_.listeEvenements[k].tableauTrajet[i].acces;
	    				attenteSecu += donnees_.listeResultatsSimu_.listeEvenements[k].tableauTrajet[i].secu;
	    				attenteOccup += donnees_.listeResultatsSimu_.listeEvenements[k].tableauTrajet[i].occupation;
	    				attenteIndispo += donnees_.listeResultatsSimu_.listeEvenements[k].tableauTrajet[i].indispo;
	    			}
	    			m = ((int) Math.floor(attenteAcces))/valeurIntervalleDistribution;
        			resultats.tableauAttenteTrajet.tableauAttenteCategories[n].attenteAccesDistrib[m] ++;
        			m = ((int) Math.floor(attenteSecu))/valeurIntervalleDistribution;
        			resultats.tableauAttenteTrajet.tableauAttenteCategories[n].attenteSecuDistrib[m] ++;
        			m = ((int) Math.floor(attenteOccup))/valeurIntervalleDistribution;
        			resultats.tableauAttenteTrajet.tableauAttenteCategories[n].attenteOccupDistrib[m] ++;
        			m = ((int) Math.floor(attenteIndispo))/valeurIntervalleDistribution;
        			resultats.tableauAttenteTrajet.tableauAttenteCategories[n].attentePanneDistrib[m] ++;
	    			attenteTotale = attenteAcces + attenteSecu + attenteOccup + attenteIndispo;
	    			m = ((int) Math.floor(attenteTotale))/valeurIntervalleDistribution;
        			resultats.tableauAttenteTrajet.tableauAttenteCategories[n].attenteTotaleDistrib[m] ++;
	    		}
	    	}
	    }

	}

	public static void calculTrajetDepuisListeTrajet(final Sinavi3DataSimulation _donnees, final int _typeElementDepart,
			final int _elementDepart, final int _typeElementArrivee, final int _elementArrivee, final int _sens) {


		/*
		 * sens: 0: sens amont vers aval: on ne cherche que les attentes dans les gares amont de lelement 1: sens aval vers
		 * amont: on ne cherche els temps que dans les gares aval de l element 2: les 2 sens : on cumule les attentes a la
		 * fois des gares amont et aval.
		 */

		final SParametresResultatsCompletSimulation resultats = _donnees.params_.ResultatsCompletsSimulation;

		// allocation de m�moire pour un element: cet element est un trajet
		resultats.tableauAttenteTrajet = new SParametresResultatsAttente();

		resultats.tableauAttenteTrajet.tableauAttenteCategories = new SParametresResultatsAttenteCategorie[_donnees.listeBateaux_.listeNavires_
		                                                                                                   .size()];
		
/*		if ((_elementDepart == _elementArrivee) && (_typeElementDepart == _typeElementArrivee)) {
			calcul(_donnees, _sens);
			int k = 0;
			int l = resultats.AttentesTousElementsToutesCategories.length;
			while (k < l
					&& resultats.AttentesTousElementsToutesCategories[k].typeElement != _typeElementDepart
					&& resultats.AttentesTousElementsToutesCategories[k].indiceElement != _elementDepart) {
				k++;
			}
			if (k < l) {
				resultats.tableauAttenteTrajet.tableauAttenteCategories = resultats.AttentesTousElementsToutesCategories[k].tableauAttenteCategories;
			}
			return;
		}
*/
		for (int i = 0; i < resultats.TOUTESAttenteTrajet.length; i++) {
			if (resultats.TOUTESAttenteTrajet[i].indiceElement == _elementDepart
					&& resultats.TOUTESAttenteTrajet[i].typeElement == _typeElementDepart
					&& resultats.TOUTESAttenteTrajet[i].indiceElement2 == _elementArrivee
					&& resultats.TOUTESAttenteTrajet[i].typeElement2 == _typeElementArrivee
					&& resultats.TOUTESAttenteTrajet[i].sens == _sens) {
				resultats.tableauAttenteTrajet.valeurIntervalleDistribution = resultats.TOUTESAttenteTrajet[i].valeurIntervalleDistribution;
				resultats.tableauAttenteTrajet.tableauAttenteCategories = resultats.TOUTESAttenteTrajet[i].tableauAttenteCategories;
				return;
			} else if (resultats.TOUTESAttenteTrajet[i].indiceElement == _elementArrivee
					&& resultats.TOUTESAttenteTrajet[i].typeElement == _typeElementArrivee
					&& resultats.TOUTESAttenteTrajet[i].indiceElement2 == _elementDepart
					&& resultats.TOUTESAttenteTrajet[i].typeElement2 == _typeElementDepart
					&& resultats.TOUTESAttenteTrajet[i].sens == _sens) {
				resultats.tableauAttenteTrajet.valeurIntervalleDistribution = resultats.TOUTESAttenteTrajet[i].valeurIntervalleDistribution;
				resultats.tableauAttenteTrajet.tableauAttenteCategories = resultats.TOUTESAttenteTrajet[i].tableauAttenteCategories;
				return;
			}

		}

	}
	
	/**
	 * Methode qui initialise les param�tres d'attente.
	 * 
	 * @param donnees_
	 * @return
	 */
	public static void initialiseParams(SParametresResultatsAttenteCategorie _params) {
		_params.nombreNaviresTotal = 0;

		_params.attenteAccesTotale = 0;
		_params.nbNaviresAttenteAcces = 0;
		_params.attenteAccesMaxi = 0;
		_params.attenteAccesMini = 9999999;

		_params.attenteSecuTotale = 0;
		_params.nbNaviresAttenteSecu = 0;
		_params.attenteSecuMaxi = 0;
		_params.attenteSecuMini = 9999999;

		_params.attenteOccupTotale = 0;
		_params.nbNaviresAtenteOccup = 0;
		_params.attenteOccupMaxi = 0;
		_params.attenteOccupMini = 9999999;

		_params.attentePanneTotale = 0;
		_params.nbNaviresAttentePanne = 0;
		_params.attentePanneMaxi = 0;
		_params.attentePanneMini = 9999999;

		_params.attenteMegaTotale = 0;
		_params.nbNaviresAttenteTotale = 0;
		_params.attenteTotaleMaxi = 0;
		_params.attenteTotaleMini = 9999999;
	}
	
	/**
	 * Methode qui calcule les attentes pour un �l�ment, dans un sens donn�.
	 * 
	 * @param _donnees
	 * @param _attentes dans lequel seront stock�es les attentes
	 * @param _sens num�ro du sens, -1 signifiant tous sens confondus
	 * @return
	 */
	public static void calculeAttentesElement(final Sinavi3DataSimulation _donnees, SParametresResultatsAttente _attentes, final int _sens, final int _indiceElement, final int _typeElement) {
		
		_attentes.typeElement = _typeElement;
		_attentes.indiceElement = _indiceElement;
		_attentes.tableauAttenteCategories = new SParametresResultatsAttenteCategorie[_donnees.listeBateaux_.listeNavires_.size()];
		
		boolean tousSensConfondus = (_sens == -1) ? true : false;
		
		// initialisation de tous les parametres d'attente
		for (int k = 0; k < _donnees.listeBateaux_.listeNavires_.size(); k++) {
			_attentes.tableauAttenteCategories[k] = new SParametresResultatsAttenteCategorie();
			initialiseParams(_attentes.tableauAttenteCategories[k]);
		}
		// boucle sur l historique et pour chaque ligne, on remplit les donn�es dans la bonne case grace a l indice de
		// cat�gorie

		for (int k = 0; k < _donnees.listeResultatsSimu_.nombreNavires; k++) {
			if(tousSensConfondus || _donnees.listeResultatsSimu_.listeEvenements[k].sens == _sens){ 
			
				// on r�cup�re la categorie associee au bateau:
				final int n = _donnees.listeResultatsSimu_.listeEvenements[k].categorie;

				// pour chaque element du trajet du navire k des qu on trouve le premier element on se retire
				boolean trouve = false;
				for (int e = 0; e < _donnees.listeResultatsSimu_.listeEvenements[k].NbElemtnsParcours && !trouve; e++) {

					// si l'�l�ment du trajet est equivalent a celui de l'�l�ment recherch�
					if (_donnees.listeResultatsSimu_.listeEvenements[k].tableauTrajet[e].typeElement == _typeElement
							&& _donnees.listeResultatsSimu_.listeEvenements[k].tableauTrajet[e].indiceElement == _indiceElement) {
						trouve = true;
						_attentes.tableauAttenteCategories[n].nombreNaviresTotal++;
						
						// on comptabilise les diff�rents temps d'attente

						// attentes d'acces
						double attente = _donnees.listeResultatsSimu_.listeEvenements[k].tableauTrajet[e].acces;
						_attentes.tableauAttenteCategories[n].attenteAccesTotale += attente;
						if (attente != 0) _attentes.tableauAttenteCategories[n].nbNaviresAttenteAcces++;
						if (attente > _attentes.tableauAttenteCategories[n].attenteAccesMaxi) _attentes.tableauAttenteCategories[n].attenteAccesMaxi = attente;
						if (attente < _attentes.tableauAttenteCategories[n].attenteAccesMini) _attentes.tableauAttenteCategories[n].attenteAccesMini = attente;

						// attentes de Secu
						attente = _donnees.listeResultatsSimu_.listeEvenements[k].tableauTrajet[e].secu;
						_attentes.tableauAttenteCategories[n].attenteSecuTotale += attente;
						if (attente != 0) _attentes.tableauAttenteCategories[n].nbNaviresAttenteSecu++;
						if (attente > _attentes.tableauAttenteCategories[n].attenteSecuMaxi) _attentes.tableauAttenteCategories[n].attenteSecuMaxi = attente;
						if (attente < _attentes.tableauAttenteCategories[n].attenteSecuMini) _attentes.tableauAttenteCategories[n].attenteSecuMini = attente;

						// attentes d'Occup
						attente = _donnees.listeResultatsSimu_.listeEvenements[k].tableauTrajet[e].occupation;
						_attentes.tableauAttenteCategories[n].attenteOccupTotale += attente;
						if (attente != 0) _attentes.tableauAttenteCategories[n].nbNaviresAtenteOccup++;
						if (attente > _attentes.tableauAttenteCategories[n].attenteOccupMaxi) _attentes.tableauAttenteCategories[n].attenteOccupMaxi = attente;
						if (attente < _attentes.tableauAttenteCategories[n].attenteOccupMini) _attentes.tableauAttenteCategories[n].attenteOccupMini = attente;

						// attentes d'Indispo
						attente = _donnees.listeResultatsSimu_.listeEvenements[k].tableauTrajet[e].indispo;
						_attentes.tableauAttenteCategories[n].attentePanneTotale += attente;
						if (attente != 0) _attentes.tableauAttenteCategories[n].nbNaviresAttentePanne++;
						if (attente > _attentes.tableauAttenteCategories[n].attentePanneMaxi) _attentes.tableauAttenteCategories[n].attentePanneMaxi = attente;
						if (attente < _attentes.tableauAttenteCategories[n].attentePanneMini) _attentes.tableauAttenteCategories[n].attentePanneMini = attente;
					
						// attentes d'totale
						attente = _donnees.listeResultatsSimu_.listeEvenements[k].tableauTrajet[e].acces
							+ _donnees.listeResultatsSimu_.listeEvenements[k].tableauTrajet[e].indispo
							+ _donnees.listeResultatsSimu_.listeEvenements[k].tableauTrajet[e].occupation
							+ _donnees.listeResultatsSimu_.listeEvenements[k].tableauTrajet[e].secu;
						_attentes.tableauAttenteCategories[n].attenteMegaTotale += attente;
						if (attente != 0) _attentes.tableauAttenteCategories[n].nbNaviresAttenteTotale++;
						if (attente > _attentes.tableauAttenteCategories[n].attenteTotaleMaxi) _attentes.tableauAttenteCategories[n].attenteTotaleMaxi = attente;
						if (attente < _attentes.tableauAttenteCategories[n].attenteTotaleMini) _attentes.tableauAttenteCategories[n].attenteTotaleMini = attente;
					}
				}

			}//fin test sens
		}//fin de boucle historique

		for (int n = 0; n < _donnees.listeBateaux_.listeNavires_.size(); n++) {
			if (_attentes.tableauAttenteCategories[n].attenteAccesMini == 9999999) {
				_attentes.tableauAttenteCategories[n].attenteAccesMini = 0;
			}
			if (_attentes.tableauAttenteCategories[n].attenteSecuMini == 9999999) {
				_attentes.tableauAttenteCategories[n].attenteSecuMini = 0;
			}
			if (_attentes.tableauAttenteCategories[n].attenteOccupMini == 9999999) {
				_attentes.tableauAttenteCategories[n].attenteOccupMini = 0;
			}
			if (_attentes.tableauAttenteCategories[n].attentePanneMini == 9999999) {
				_attentes.tableauAttenteCategories[n].attentePanneMini = 0;
			}
			if (_attentes.tableauAttenteCategories[n].attenteTotaleMini == 9999999) {
				_attentes.tableauAttenteCategories[n].attenteTotaleMini = 0;
			}
		}
	}

	/**
	 * Methode qui determine l attente maximum pour les attentes g�n�ralis�es
	 * 
	 * @param _donnees
	 * @return
	 */
	public static float determineAttenteMax(final Sinavi3DataSimulation _donnees) {
		float max = 0;

		for (int i = 0; i < _donnees.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories.length; i++) {
			for (int j = 0; j < _donnees.listeBateaux_.listeNavires_.size(); j++) {
				if (max < _donnees.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[i].tableauAttenteCategories[j].attenteTotaleMaxi) {
					max = (float) _donnees.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[i].tableauAttenteCategories[j].attenteTotaleMaxi;
				}

			}
		}
		return (float) (max + 25.0 / 100.0 * max);
	}

	/**
	 * Methode qui determine les attentes maximum pour les attentes de parcours
	 * 
	 * @param _donnees
	 * @return
	 */
	public static float determineAttenteMaxTrajet(final Sinavi3DataSimulation _donnees) {
		float max = 0;

		for (int j = 0; j < _donnees.listeBateaux_.listeNavires_.size(); j++) {
			if (max < _donnees.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[j].attenteTotaleMaxi) {
				max = (float) _donnees.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[j].attenteTotaleMaxi;
			}

		}
		return (float) (max + 25.0 / 100.0 * max);
	}
	
	
	public static void computeGarages(final Sinavi3DataSimulation d) {
		
		// -- step 1: tri des garages par frequence--//
		Collections.sort(d.getListeGarages(), new Comparator<Sinavi3ResultatGarage>() {

			@Override
			public int compare(Sinavi3ResultatGarage o1,
					Sinavi3ResultatGarage o2) {
				if(o1.ecluse == o2.ecluse && o1.gare == o2.gare){
					if(o1.frequenceRetour == o2.frequenceRetour)
						return 0;
					else
					return o1.frequenceRetour<o2.frequenceRetour?-1:1;
				}else if(o1.ecluse == o2.ecluse) {
					return o1.gare<o2.gare?-1:1;
				}
				else	
				return o1.ecluse<o2.ecluse?-1:1;
			}
		});
		
		//-- step 2: pourcentage des valeurs + cummule --//
		
		//-- calcul total occurence par ecluse et gare --//
		Map<Integer,Double[]> mapOccurence = new HashMap<Integer,Double[]>();
		for(Sinavi3ResultatGarage garage:d.getListeGarages() ) {
			Double[] cpt = mapOccurence.get(Integer.valueOf(garage.ecluse));
			if(cpt == null) {
				cpt = new Double[2];
				cpt[0] = 0.0;
				cpt[1] =0.0;
				mapOccurence.put(Integer.valueOf(garage.ecluse), cpt);
			}
			Sinavi3Ecluse ecluse = d.getListeEcluse().retournerEcluse(garage.ecluse);
			if(ecluse.gareAmont_ == garage.gare){
				cpt[0] = cpt[0]+(int)garage.frequenceRetour;
			}else
				cpt[1] = cpt[1]+(int)garage.frequenceRetour;
		}
		
		//-- calcul en frequencetotal = 100%
		int previousEcluse =-1;
		int previousGare = -1;
		double frequenceCumulee=0;
		Sinavi3ResultatGarage previousGarage = null;
		for(Sinavi3ResultatGarage garage:d.getListeGarages() ) {
			Double[] totalFrequence = mapOccurence.get(garage.ecluse);
			Sinavi3Ecluse ecluse = d.getListeEcluse().retournerEcluse(garage.ecluse);
			if(ecluse.gareAmont_ == garage.gare){
				garage.frequenceRetour = garage.frequenceRetour / totalFrequence[0];
			}else {
				garage.frequenceRetour = garage.frequenceRetour / totalFrequence[1];
			}
			if(garage.ecluse != previousEcluse || garage.gare != previousGare) {
				frequenceCumulee =0;
				if(previousGarage != null)
					previousGarage.frequenceCumulee =1.0;
			}
			garage.frequenceCumulee = garage.frequenceRetour + frequenceCumulee;
			
			frequenceCumulee = garage.frequenceCumulee;
			previousEcluse = garage.ecluse;
			previousGare = garage.gare;
			previousGarage = garage;
		}
		previousGarage.frequenceCumulee =1.0;
		//-- step 3: set resultats complet simulation  --//
		d.params_.ResultatsCompletsSimulation.listeGarageEcluse = new Sinavi3ResultatGarage[d.getListeGarages().size()];
		    for(int i=0;i<d.getListeGarages().size(); i++) {
		    	d.params_.ResultatsCompletsSimulation.listeGarageEcluse[i] = d.getListeGarages().get(i);
		    }
	}

}
