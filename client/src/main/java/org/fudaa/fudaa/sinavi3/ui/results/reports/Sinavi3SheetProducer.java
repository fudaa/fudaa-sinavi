package org.fudaa.fudaa.sinavi3.ui.results.reports;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuCheckBox;
import com.memoire.bu.BuDialogError;
import com.memoire.bu.BuInternalFrame;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuProgressBar;
import com.memoire.bu.BuTable;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import jxl.Workbook;
import jxl.format.Alignment;
import jxl.format.Border;
import jxl.format.BorderLineStyle;
import jxl.format.Colour;
import jxl.format.VerticalAlignment;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;

import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.CtuluResource;
import org.fudaa.fudaa.sinavi3.Sinavi3DataSimulation;
import org.fudaa.fudaa.sinavi3.ui.results.Sinavi3ResultatsGarage.KeyGarage;

import com.memoire.fu.FuLog;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.util.ArrayList;
import javax.swing.DefaultCellEditor;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import javax.swing.table.TableModel;
import jxl.write.DateFormat;
import jxl.write.Formula;
import jxl.write.NumberFormat;
import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.dodico.corba.sinavi3.SParametresSinavi32;
import org.fudaa.fudaa.commun.projet.FudaaProjet;
import org.fudaa.fudaa.sinavi3.Sinavi3AlgorithmeDureesParcours;
import org.fudaa.fudaa.sinavi3.Sinavi3Bordures;
import org.fudaa.fudaa.sinavi3.Sinavi3Ecluse;
import org.fudaa.fudaa.sinavi3.Sinavi3Resource;
import org.fudaa.fudaa.sinavi3.Sinavi3ResultatsDureesParcours;

/**
 *
 * this class produce calc sheet with post processed params comming from sinavi.
 *
 * @author Adrien Hadoux
 *
 */
public class Sinavi3SheetProducer extends Sinavi3SheetFactoryHelper{

    public static boolean produceGarageEcluse(final File destination, Sinavi3DataSimulation data, final String[] title,
            final Map<KeyGarage, Object[][]> mapData, final List<KeyGarage> keys) {
        boolean res = false;
        WritableWorkbook classeur = null;
        try {
            classeur = Workbook.createWorkbook(destination);
            WritableSheet sheet = classeur.createSheet("Lin�aire garage �cluse", 0);
            sheet.setColumnView(0, 10);
            sheet.setColumnView(1, 10);
            sheet.setColumnView(2, 20);

            sheet.setColumnView(3 + data.getListeBateaux_().NombreNavires(), 20);
            sheet.setColumnView(4 + data.getListeBateaux_().NombreNavires(), 20);
            sheet.setColumnView(5 + data.getListeBateaux_().NombreNavires(), 20);

            WritableFont cellFont = new WritableFont(WritableFont.TIMES, 12);
            cellFont.setColour(Colour.BLACK);
            WritableCellFormat cellFormatTitle = new WritableCellFormat(cellFont);
            cellFormatTitle.setBackground(Colour.YELLOW);
            cellFormatTitle.setAlignment(Alignment.CENTRE);
            cellFormatTitle.setVerticalAlignment(VerticalAlignment.CENTRE);
            cellFormatTitle.setBorder(Border.ALL, BorderLineStyle.THIN);
            cellFormatTitle.setWrap(true);

            WritableCellFormat cellFormat = new WritableCellFormat(cellFont);
            cellFormat.setAlignment(Alignment.CENTRE);
            cellFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
            cellFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
            cellFormat.setWrap(true);

            //-- title --//		    
            sheet.mergeCells(0, 0, 0, 3);
            sheet.addCell(new Label(0, 0, "Ecluse", cellFormatTitle));
            sheet.mergeCells(1, 0, 1, 3);
            sheet.addCell(new Label(1, 0, "Gare", cellFormatTitle));
            sheet.mergeCells(2, 0, 2, 3);
            sheet.addCell(new Label(2, 0, "Nb bateaux \nen \nattente", cellFormatTitle));
            sheet.mergeCells(3, 0, 3 + title.length - 5, 0);
            sheet.addCell(new Label(3, 0, "Cat�gorie de bateaux", cellFormatTitle));
            for (int k = 1; k < title.length - 3; k++) {
                sheet.mergeCells(2 + k, 1, 2 + k, 2);
                sheet.addCell(new Label(2 + k, 1, title[k], cellFormatTitle));
                int posNavire = k - 1;
                if (data.getListeBateaux_().NombreNavires() > posNavire) {
                    sheet.addCell(new Label(2 + k, 3, "" + data.getListeBateaux_().retournerNavire(posNavire).getLongueurMax(), cellFormatTitle));
                }
            }
            sheet.mergeCells(4 + title.length - 5, 0, 4 + title.length - 5, 3);
            sheet.addCell(new Label(4 + title.length - 5, 0, "Lin�aire \ntotal \n(m)", cellFormatTitle));
            sheet.mergeCells(5 + title.length - 5, 0, 5 + title.length - 5, 3);
            sheet.addCell(new Label(5 + title.length - 5, 0, "Fr�quence \nde\nretour", cellFormatTitle));
            sheet.mergeCells(6 + title.length - 5, 0, 6 + title.length - 5, 3);
            sheet.addCell(new Label(6 + title.length - 5, 0, "Fr�quence \ncumul�e", cellFormatTitle));

            //-- data --//
            int rowIndex = 4;
            int colIndex = 0;
            for (KeyGarage clef : keys) {
                Object[][] d = mapData.get(clef);

                sheet.mergeCells(0, rowIndex, 0, rowIndex + d.length - 1);
                sheet.addCell(new Label(0, rowIndex, data.getListeEcluse().retournerEcluse(clef.ecluse).getName(), cellFormat));
                sheet.mergeCells(1, rowIndex, 1, rowIndex + d.length - 1);
                sheet.addCell(new Label(1, rowIndex, data.getListeGare_().retournerGare(clef.gare), cellFormat));
                colIndex = 2;
                for (int ir = 0; ir < d.length; ir++) {
                    colIndex = 2;
                    for (int ic = 0; ic < d[ir].length; ic++) {

                        sheet.addCell(new Label(colIndex++, rowIndex, d[ir][ic].toString(), cellFormat));
                    }
                    rowIndex++;
                }

            }

            classeur.write();

        } catch (Exception e) {
            FuLog.error(e);
            res = false;
        } finally {
            if (classeur != null) {
                try {
                    classeur.close();
                } catch (WriteException e) {
                    FuLog.error(e);
                    res = false;
                } catch (IOException e) {
                    FuLog.error(e);
                    res = false;
                }
            }
        }
        return res;
    }

    public static  JComboBox feedComboEcluses(Sinavi3DataSimulation data) {
      String[] res = new String[data.getListeEcluse().getListeEcluses().size()];
      for(int i=0; i< data.getListeEcluse().getListeEcluses().size();i++) {
          res[i] = data.getListeEcluse().retournerEcluse(i).getNom();
          
      }
       return new JComboBox(res);
    }

    public static  JComboBox feedComboTroncons(Sinavi3DataSimulation data) {
        String[] res = new String[data.getListeBief_().getListeBiefs_().size()];
      for(int i=0; i< data.getListeBief_().getListeBiefs_().size();i++) {
          res[i] = data.getListeBief_().retournerBief(i).getName();
          
      }
       return new JComboBox(res);
        
    }

   

    public static class TrajetReport {
        public int etypeElement=0;
        public int eelement=0;
        public int stypeElement=0;
        public int selement=0;
    }
    
    
    public static class ModelTrajets implements  TableModel{

        
        public List<TrajetReport> trajets;
        public boolean editable = true;
        public Sinavi3DataSimulation simulation;
        public BuTable tableau;
        public ModelTrajets(final List<TrajetReport> liste, final Sinavi3DataSimulation s, final BuTable table) {
            trajets = liste;
            simulation=s;
            tableau = table;
        }
        
        @Override
        public int getRowCount() {
            return trajets.size() +1;
        }

        @Override
        public int getColumnCount() {
           return 4;
        }

        @Override
        public String getColumnName(int columnIndex) {
            switch(columnIndex) {
                case 0: return "Type �l�ment entrant";
                case 1: return "Element entrant";
                case 2: return "Type �l�ment sortant";
                case 3: return "Element sortant";
                default: return "";
            }
            
        }

        @Override
        public Class<?> getColumnClass(int columnIndex) {
           return String.class;
        }

        @Override
        public boolean isCellEditable(int rowIndex, int columnIndex) {
           return editable;
        }

        @Override
        public Object getValueAt(int rowIndex, int columnIndex) {
            if(rowIndex == trajets.size())
                return "Nouveau";
            
             switch(columnIndex) {
                case 0: return trajets.get(rowIndex).etypeElement==0?"Tron�on":"Ecluse";
                case 1: return getNameForItem(true, trajets.get(rowIndex));
                case 2: return trajets.get(rowIndex).stypeElement==0?"Tron�on":"Ecluse";
                case 3: return getNameForItem(false, trajets.get(rowIndex));
                default: return "";
             }
            
        }

        public String getNameForItem(boolean entrant, TrajetReport t) {
            if(entrant ) {
                if(t.etypeElement == 0)
                return  simulation.getListeBief_().retournerBief(t.eelement).getName();
                else
                    return  simulation.getListeEcluse().retournerEcluse(t.eelement).getName();
                
            }else
            {
                if(t.stypeElement == 0)
                return  simulation.getListeBief_().retournerBief(t.selement).getName();
                else
                    return  simulation.getListeEcluse().retournerEcluse(t.selement).getName();
            }
        }
        
        public int getIndiceElementForName(String value, int type) {
            if(type==0 ) {
                return simulation.getListeBief_().retourneIndice(value);
            }else
                return simulation.getListeEcluse().retourneIndice(value);
        }
        
        @Override
        public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
            TrajetReport data = null;
            if(rowIndex == trajets.size()) {
                    data = new TrajetReport();
                    trajets.add(data);
            }else 
                data = trajets.get(rowIndex);
            
            switch(columnIndex) {
                case 0: data.etypeElement= "Ecluse".equals(aValue)?1:0;
                        data.eelement =0;
                        if(tableau != null) {
                            if(data.etypeElement==0)tableau.getColumnModel().getColumn(1).setCellEditor(new DefaultCellEditor(feedComboTroncons(simulation)));
                            else tableau.getColumnModel().getColumn(1).setCellEditor(new DefaultCellEditor(feedComboEcluses(simulation)));
                        }
                        break;
                case 1: data.eelement = getIndiceElementForName(aValue.toString(), data.etypeElement);break;
                case 2: 
                    data.stypeElement= "Ecluse".equals(aValue)?1:0;
                    data.selement =0;
                    if(tableau != null) {
                        if(data.stypeElement==0)tableau.getColumnModel().getColumn(3).setCellEditor(new DefaultCellEditor(feedComboTroncons(simulation)));
                            else tableau.getColumnModel().getColumn(3).setCellEditor(new DefaultCellEditor(feedComboEcluses(simulation)));
                    }
                    break;
                case 3: data.selement = getIndiceElementForName(aValue.toString(), data.stypeElement);break;
                
             }
            
        }

        @Override
        public void addTableModelListener(TableModelListener l) {
        }

        @Override
        public void removeTableModelListener(TableModelListener l) {
        }

        private void setEditable(boolean b) {
         editable = b;
        }
        
    }
    
    
    public List<TrajetReport> listeTrajets ;
    public boolean allTrajets= true;
    public File fileDestination =null;
    BuTable tableau;
    public void buildReportUi(final Sinavi3DataSimulation data, boolean produceDuree, boolean attente, boolean occup, boolean indispo, boolean bassin, final boolean multiSimulation){
        
        BuPanel panel = new BuPanel(new BorderLayout());
        
        BuPanel panelFile = new BuPanel(new FlowLayout());
        if(multiSimulation) {
               BuPanel panelMultiSimu = new BuPanel(new GridLayout(data.getApplication().getSimulations().length,1));
               panelMultiSimu.setBorder(Sinavi3Bordures.createTitledBorder("Noms des simulations"));
               int cptSimu= 1;
              for (final FudaaProjet projet : data.getApplication().getSimulations()) {
                  if (data.getProjet_() != projet) {
                    BuPanel panelS = new BuPanel(new FlowLayout(FlowLayout.CENTER));
                    panelS.add(new JLabel("Simu " + (cptSimu++) + ": "));
                    final JTextField nameS = new JTextField(projet.getInformations().titre, 15);
                    panelS.add(nameS);
                    nameS.addFocusListener(new FocusListener() {

                        @Override
                        public void focusGained(FocusEvent e) {

                        }

                        @Override
                        public void focusLost(FocusEvent e) {
                            projet.getInformations().titre = nameS.getText();
                        }
                    });
                    panelMultiSimu.add(panelS);
                }
            }
              
              BuPanel panelFileAndMulti = new BuPanel(new BorderLayout());
              panelFileAndMulti.add(panelFile,BorderLayout.NORTH);
              panelFileAndMulti.add(panelMultiSimu,BorderLayout.CENTER);
              panel.add(panelFileAndMulti,BorderLayout.NORTH);
            
        }else
            panel.add(panelFile,BorderLayout.NORTH);
        BuButton button = new BuButton("Fichier");
        panelFile.setBorder(Sinavi3Bordures.createTitledBorder("S�lection du fichier"));
        final JLabel label = new JLabel("emplacement du fichier ");
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                File fichier;
                final JFileChooser fc = new JFileChooser();
                final int returnVal = fc.showSaveDialog(null);
                if (returnVal == JFileChooser.APPROVE_OPTION) {
                    fichier = new File(fc.getSelectedFile().getAbsolutePath() + "_" + System.currentTimeMillis());
                    fileDestination = CtuluLibFile.appendExtensionIfNeeded(fichier, "xls");
                    label.setText("fichier " + fichier.getName());
        }
            }});
        panelFile.add(button);
        panelFile.add(label);
            
            
        //-- les trajets --//
        BuPanel panelTrajets = new BuPanel(new BorderLayout());
        listeTrajets = new ArrayList<TrajetReport>();
        panelTrajets.setBorder(Sinavi3Bordures.createTitledBorder("S�lection des trajets"));
        final ModelTrajets modele = new ModelTrajets(listeTrajets,data, tableau);
        tableau = new BuTable(modele);
        modele.tableau = tableau;
        JComboBox combo = new JComboBox(new String[]{"Tron�on","Ecluse"});
        tableau.getColumnModel().getColumn(0).setCellEditor(new DefaultCellEditor(combo));
        tableau.getColumnModel().getColumn(2).setCellEditor(new DefaultCellEditor(combo));
        tableau.getColumnModel().getColumn(1).setCellEditor(new DefaultCellEditor(feedComboTroncons(data)));
        tableau.getColumnModel().getColumn(3).setCellEditor(new DefaultCellEditor(feedComboTroncons(data)));
         
        
        
        
        panelTrajets.add(new  JScrollPane(tableau));
        panel.add(panelTrajets);
        final BuCheckBox boxAllTrajets = new BuCheckBox("Tous les trajets", allTrajets);
        boxAllTrajets.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
           
                allTrajets = boxAllTrajets.isSelected();
                modele.setEditable(!allTrajets);
                
            }
        });
        panelTrajets.add(boxAllTrajets,BorderLayout.NORTH);
        
        
        //-- les table report a exporter --//
        BuPanel panelReport = new BuPanel(new BorderLayout());
        panel.add(panelReport, BorderLayout.SOUTH);
        BuPanel panelCheck = new BuPanel(new GridLayout(2,2));
        panelCheck.setBorder(Sinavi3Bordures.createTitledBorder("S�lection des rapports"));
        
        panelReport.add(panelCheck,BorderLayout.CENTER);
        
        final BuCheckBox checkDuree = new BuCheckBox("Dur�es de parcours", produceDuree);
        panelCheck.add(checkDuree);
        final BuCheckBox checkattente = new BuCheckBox("Attentes totales", attente);
        panelCheck.add(checkattente);
        final BuCheckBox checkOccup = new BuCheckBox("Occupations", occup);
        panelCheck.add(checkOccup);
        final BuCheckBox checkAttente = new BuCheckBox("Indisponibilit�s", indispo);
       // panelCheck.add(checkAttente);
        final BuCheckBox checConso = new BuCheckBox("Bassin�e et conso eau", bassin);
        panelCheck.add(checConso);
        
        final BuInternalFrame frame = new BuInternalFrame("Rapport",true, true, true, false);
        
        final BuPanel okPanel = new BuPanel();
        final BuButton buttonOK = new BuButton("G�n�rer le rapport");
        buttonOK.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                
                SwingUtilities.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        
                        if (!allTrajets && (listeTrajets == null || listeTrajets.isEmpty())) {
                            new BuDialogError(data.getApplication().getApp(), data.getApplication().getInformationsSoftware(),
                                    "Il faut choisir au moins un trajet.").activate();
                        } else if (fileDestination == null) {
                            new BuDialogError(data.getApplication().getApp(), data.getApplication().getInformationsSoftware(),
                                    "Il faut choisir le fichier de destination.").activate();

                        } else {
                            final BuProgressBar progress = new BuProgressBar();
                            okPanel.removeAll();
                            okPanel.add(progress);
                
                            if (Sinavi3SheetProducer.producePostProcessingReport(fileDestination, data, checkDuree.isSelected(),
                                checkattente.isSelected(), checkOccup.isSelected(), checkAttente.isSelected(),
                                checConso.isSelected(), multiSimulation, allTrajets?null:listeTrajets, progress)) {
                            frame.dispose();
                            
                            try{
                                Desktop.getDesktop().open(fileDestination);
                                
                            }catch(Exception e) {
                                FuLog.error("impossible d'ouvrir directement le fichier.\n veuillez l'ouvrir manuellement � l'emplacement:" + fileDestination.getAbsolutePath());
                            }
                            
                        } else {
                            okPanel.removeAll();
                            okPanel.add(buttonOK);

                        }
                     }
                    }
                    
                
                });
               
        }
            });
        okPanel.add(buttonOK);
        panelReport.add(okPanel,BorderLayout.SOUTH);
        
        frame.getContentPane().add(panel);
        
        data.getApplication().addInternalFrame(frame);
        frame.setMinimumSize(new Dimension(400,400));
        frame.setMaximumSize(new Dimension(400,400));
        frame.setSize(new Dimension(400,400));
        frame.setPreferredSize(new Dimension(400,400));
        
        
    }
    

    public static boolean producePostProcessingReport(final File destination, final Sinavi3DataSimulation data, boolean produceDuree, 
            boolean attente, boolean occup, boolean indispo, boolean bassin, boolean multiSimulation, List<TrajetReport> listeTrajets, final BuProgressBar progress) {
        boolean res = true;
        WritableWorkbook classeur = null;
        try {
            if(progress != null){
                progress.setValue(0);
                progress.setStringPainted(true);
            }
                
            initFormat();
            classeur = Workbook.createWorkbook(destination);
             if(progress != null){
                progress.setValue(10);
             }
            if (produceDuree) {
                Sinavi3ReportDureeParcours.produceSheetDureeParcours(classeur, data, multiSimulation,listeTrajets);
            }
            if(progress != null){
                progress.setValue(30);
             }
            if (attente) {
                Sinavi3ReportAttemtesTrajets.produceSheetAttentesTrajets(classeur, data, multiSimulation, listeTrajets);
            }
            if(progress != null){
                progress.setValue(50);
             }
             if (occup) {
                Sinavi3ReportOccupations.produceSheetOccup(classeur, data, multiSimulation);
            }
             if(progress != null){
                progress.setValue(70);
             }
             
             if(bassin) {
                 Sinavi3ReportConsoEau.produceSheetBassinnee(classeur, data, multiSimulation);
             }
             if(progress != null){
                progress.setValue(90);
             }
            classeur.write();

        } catch (Exception e) {
             FuLog.error(e);
             new BuDialogError(data.getApplication().getApp(), data.getApplication().getInformationsSoftware(),
			          "Erreur lors de la production du rapport.\n "+ e ).activate();
            res = false;
        } finally {
            if (classeur != null) {
                try {
                    classeur.close();
                } catch (WriteException e) {
                    FuLog.error(e);
                    new BuDialogError(data.getApplication().getApp(), data.getApplication().getInformationsSoftware(),
			          "Erreur lors de la production du rapport.\n "+ e ).activate();
                    res = false;
                } catch (IOException e) {
                    FuLog.error(e);
                    new BuDialogError(data.getApplication().getApp(), data.getApplication().getInformationsSoftware(),
			          "Erreur lors de la production du rapport.\n "+ e ).activate();
                    res = false;
                }
            }
        }
        if(res) {
            if(progress != null){
                progress.setValue(100);
             }
        }
        return res;
    }
    

}
