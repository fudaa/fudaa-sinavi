
/**
 *@creation 14 nov. 06
 *@modification $Dates$
 *@license  GNU General Public Licence 2
 *@copyright   (c)1998-2006 CETMEF 2 bd Gambetta F -60231 Compiegne
 *@mail   devel@fudaa.fr 
 */
package org.fudaa.fudaa.sinavi3;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.ByteArrayInputStream;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JScrollPane;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;

import org.fudaa.ctulu.image.CtuluImageExport;
import org.fudaa.dodico.corba.sinavi3.SParametresSinavi32;
import org.fudaa.dodico.corba.sipor.SParametresSipor2;
import org.fudaa.ebli.graphe.BGraphe;
import org.fudaa.fudaa.commun.projet.FudaaFiltreFichier;
import org.fudaa.fudaa.commun.projet.FudaaProjet;
import org.fudaa.fudaa.ressource.FudaaResource;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuDialogMessage;
import com.memoire.bu.BuInternalFrame;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuTabbedPane;
import com.memoire.bu.BuTable;

/**
 * classe de gestion des comparaisons des resultats de la generation des bateaux.
 * 
 * @version $Version$
 * @author Adrien Hadoux
 */
public class Sinavi3ResultatComparaisonGenerationBateaux extends Sinavi3InternalFrame /*implements ActionListener */{

  /**
   * liste des projets pour la comparaison de simulation: limit� a 10 simulations
   */
  FudaaProjet[] listeProjet_ = new FudaaProjet[10];

  /**
   * liste des r�sultats de simulation des simulations
   */
  SParametresSinavi32[] listeParamsSimu_ = new SParametresSinavi32[10];

  /**
   * nombre de simulations comparables sur les 10
   */
  int nombreSimulationsComparees_ = 0;

  /**
   * tableau sopecifiant selon els criteres de comparaison si chacune des simulatiopns sont comparables et indique
   * l'indice du navire a comparer
   */
  int[] comparePossible_;

  /**
   * tableau de checkBox destiner a l utilisateuir pour choisr les navire a visualiser sur les diff�rents supports.
   */
  JCheckBox[] tableauChoixSimulations_;

  /**
   * ensemble des donn�es du tableau sous la forme de data
   */
  Object[][] data;

  /**
   * Graphe associ�e aux r�sultats de la g�n�ration de bateaux
   */
  BGraphe graphe_ = new BGraphe();

  /**
   * histogramme associ�e aux r�sultats de la g�n�ration de bateaux
   */
  BGraphe histo_ = new BGraphe();

  /**
   * Tableau r�capitulatif des r�sultats de la simulation
   */
  BuTable tableau_;

  String titreTableau_[] = { "Cat�gorie", "Nombre de navires" };

  /**
   * Panel tabbed qui g�re les 2 onglets, ie les 2 versions d'affichage des r�sultats:
   */
  BuTabbedPane panelPrincipal_ = new BuTabbedPane();

  /**
   * Panel cniotenant le tableau et les boutns de controles
   */
  BuPanel panelGestionTableau_ = new BuPanel();

  /**
   * panel de gestion du tableau et des diff�rents boutons
   */
  BuPanel panelTableau_ = new BuPanel();

  /**
   * panel de gestion des boutons
   */
  BuPanel controlPanel_ = new BuPanel();

  /**
   * Panel des options: type affichages, colonnes � faire figurer:
   */
  BuPanel optionPanel_ = new BuPanel();

  /**
   * Panel de gestion des boutons des courbes
   */
  BuPanel controlPanelCourbes_ = new BuPanel();

  /**
   * Panel de gestion des boutons des histogrammes
   */
  BuPanel controlPanelHisto_ = new BuPanel();

  /**
   * panel de gestion des courbes
   */
  BuPanel panelCourbe_ = new BuPanel();

  /**
   * panel de gestion des histogrammes
   */
  BuPanel panelHisto_ = new BuPanel();

  /**
   * combolist qui permet de selectionenr les lignes deu tableau a etre affich�es:
   */
  JComboBox ListeNavires_ = new JComboBox();

  /**
   * buoton de generation des resultats
   */
  private final BuButton exportationExcel_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_generer"), "Exporter");

  final BuButton exportationgraphe_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_generer"), "Exporter");

  final BuButton exportationHisto_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_generer"), "Exporter");
  private final BuButton quitter_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_quitter"), "Quitter");
  private final BuButton quitter2_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_quitter"), "Quitter");
  private final BuButton quitter3_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_quitter"), "Quitter");

  Border raisedBevel_ = BorderFactory.createRaisedBevelBorder();
  Border loweredBevel_ = BorderFactory.createLoweredBevelBorder();
  Border compound_ = BorderFactory.createCompoundBorder(raisedBevel_, loweredBevel_);
  Border bordnormal_ = BorderFactory.createEtchedBorder();
  /**
   * donnees de la simulation
   */
  Sinavi3DataSimulation donnees_;

  /**
   * constructeur de la sous fenetre de gestion des resultats:
   */
  Sinavi3ResultatComparaisonGenerationBateaux(final Sinavi3DataSimulation _donnees) {
    super("Comparaison des bateaux g�n�r�s", true, true, true, true);

    // recuperation des donn�es de la simulation
    donnees_ = _donnees;

    for (int i = 0; i < donnees_.listeBateaux_.listeNavires_.size(); i++) {
      this.ListeNavires_.addItem("" + donnees_.listeBateaux_.retournerNavire(i).nom);
    }

    /**
     * ajout des simulation valides a comparer et chargement de leur donn�es:
     */
    tableauChoixSimulations_ = new JCheckBox[10];
    int nbProjets = 0;
    for (int i = 0; i < this.donnees_.application_.liste_.model_.size(); i++) {
      // A) creation du projet
      this.listeProjet_[nbProjets] = new FudaaProjet(this.donnees_.application_.getApp(), new FudaaFiltreFichier(
          "sipor"));
      // B) ouverture du projet
      this.listeProjet_[nbProjets].ouvrir(this.donnees_.application_.liste_.model_.get(i).toString());

      // C) verification des donn�es

      if (((SParametresSinavi32) this.listeProjet_[nbProjets].getParam(Sinavi3Resource.parametres)).ResultatsCompletsSimulation == null)// on
                                                                                                                                    // ne
                                                                                                                                    // peut
                                                                                                                                    // pas
                                                                                                                                    // comparer
                                                                                                                                    // cette
                                                                                                                                    // simulation
      {

        new BuDialogMessage(donnees_.application_.getApp(), donnees_.application_.isSinavi_, "La simulation "
            + this.donnees_.application_.liste_.model_.get(i).toString() + " non valide"
            + "\n car la simulation n'a pas encore �t� lanc�e." +

            "\n lancer les calculs de cette simulation avant " + "\n de pouvoir la comparer").activate();
      } else if (((SParametresSinavi32) this.listeProjet_[nbProjets].getParam(Sinavi3Resource.parametres)).ResultatsCompletsSimulation.ResultatsGenerationNavires == null)// on
                                                                                                                                                                      // ne
                                                                                                                                                                      // peut
                                                                                                                                                                      // pas
                                                                                                                                                                      // comparer
                                                                                                                                                                      // cette
                                                                                                                                                                      // simulation
      {
        new BuDialogMessage(donnees_.application_.getApp(), donnees_.application_.isSinavi_, "La simulation "
            + this.donnees_.application_.liste_.model_.get(i).toString() + " non valide"
            + "\n car la simulation n'a pas encore �t� lanc�e." +

            "\n lancer les calculs de cette simulation avant " + "\n de pouvoir la comparer").activate();
      } else

      {
        this.listeParamsSimu_[nbProjets] = (SParametresSinavi32) this.listeProjet_[nbProjets]
            .getParam(Sinavi3Resource.parametres);

        final String os = System.getProperty("os.name");
        int debut = 0;
        if (os.startsWith("Windows")) {
          debut = this.listeProjet_[nbProjets].getFichier().lastIndexOf("\\") + 1;
        } else {
          debut = this.listeProjet_[nbProjets].getFichier().lastIndexOf("/") + 1;
        }
        this.tableauChoixSimulations_[nbProjets] = new JCheckBox(this.listeProjet_[nbProjets].getFichier().substring(
            debut, this.listeProjet_[nbProjets].getFichier().lastIndexOf(".sipor")), true);
        this.tableauChoixSimulations_[nbProjets].addActionListener(this);
        nombreSimulationsComparees_++;
        nbProjets++;
      }

    }

    // liste des comparaisons possibles
    comparePossible_ = VerificationComparaisonSimulationsPossibleEntreNavires();

    setSize(700, 500);
    setBorder(Sinavi3Bordures.compound_);
    this.getContentPane().setLayout(new BorderLayout());

    this.getContentPane().add(this.panelPrincipal_, BorderLayout.CENTER);

    this.getContentPane().add(this.optionPanel_, BorderLayout.WEST);
    // ajout du tableau dans le panel tabbed
    // panelPrincipal_.addTab("tableau", FudaaResource.FUDAA.getIcon("crystal_arbre"), panelGestionTableau_);

    // ajout des courbes dans le panel de la sous fenetre

    panelPrincipal_.addTab("Graphe", FudaaResource.FUDAA.getIcon("crystal_graphe"), panelCourbe_);
    panelPrincipal_.addTab("Histogramme", FudaaResource.FUDAA.getIcon("crystal_graphe"), panelHisto_);

    /*******************************************************************************************************************
     * gestion du panel du haut
     ******************************************************************************************************************/

    // panel qui contient les differents boutons
    this.controlPanel_.setLayout(new GridLayout(1,1));
    BuPanel panel1=new BuPanel(new FlowLayout(FlowLayout.LEFT));
    panel1.add(new JLabel("Cat�gorie de bateaux:"));
    panel1.add(this.ListeNavires_);
    final TitledBorder bordurea = BorderFactory.createTitledBorder(BorderFactory
        .createEtchedBorder(EtchedBorder.LOWERED), "D�finition de la recherche");
    this.controlPanel_.setBorder(bordurea);
    this.controlPanel_.add(panel1);

    this.getContentPane().add(this.controlPanel_, BorderLayout.NORTH);

    this.ListeNavires_.addActionListener(this);
    this.ListeNavires_.setSelectedIndex(0);

    /*******************************************************************************************************************
     * gestion du panel des options
     ******************************************************************************************************************/

    // Box panoption=Box.createVerticalBox();
    // panoption.setLayout(new GridLayout(2,1));
    // JScrollPane pcnasc=new JScrollPane(panoption);

    final Box bVert2 = Box.createVerticalBox();
    // bVert2.add(new JLabel("Affichage navires:"));
    // bVert2.setBorder(this.bordnormal_);
    for (int i = 0; i < nombreSimulationsComparees_; i++) {
      bVert2.add(this.tableauChoixSimulations_[i]);
    }
    final TitledBorder bordure1 = BorderFactory.createTitledBorder(BorderFactory
        .createEtchedBorder(EtchedBorder.LOWERED), "Simulations");
    bVert2.setBorder(bordure1);
    //final JScrollPane pcnasc = new JScrollPane(bVert2);
    this.optionPanel_.add(bVert2);
    this.optionPanel_.setBorder(this.compound_);

    /*******************************************************************************************************************
     * gestion du panel courbes panelCourbe_
     ******************************************************************************************************************/
    // etape 1: architecture de la frame
    this.panelCourbe_.setLayout(new BorderLayout());

    // etape 2: creation de la description du graphe
    final String descriptionGraphe = affichageGraphe();

    // etape 3: rattachement du descriptif du graphe au graphe
    this.graphe_.setFluxDonnees(new ByteArrayInputStream(descriptionGraphe.getBytes()));

    // etape 4: affichage du graphe dans le panel associ�
    this.panelCourbe_.add(this.graphe_, BorderLayout.CENTER);

    // etape 5: bouton de generation du fichier image
    exportationgraphe_.setToolTipText("Exporte le graphe au format image");
    exportationgraphe_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {

        CtuluImageExport.exportImageFor(donnees_.application_, graphe_);
      }
    });

    // etape 6: creation du panel des boutons des courbes:
    this.controlPanelCourbes_.add(quitter2_);
    this.controlPanelCourbes_.add(exportationgraphe_);
    this.panelCourbe_.add(this.controlPanelCourbes_, BorderLayout.SOUTH);

    /*******************************************************************************************************************
     * gestion du panel histogramme
     ******************************************************************************************************************/
    // etape 1: architecture de la frame
    this.panelHisto_.setLayout(new BorderLayout());

    // etape 2: creation de la description du graphe
    final String descriptionHisto = this.affichageHistogramme();

    // etape 3: rattachement du descriptif du graphe au graphe
    this.histo_.setFluxDonnees(new ByteArrayInputStream(descriptionHisto.getBytes()));

    // etape 4: affichage du graphe dans le panel associ�
    this.panelHisto_.add(this.histo_, BorderLayout.CENTER);

    // etape 5: bouton de generation du fichier image
    exportationHisto_.setToolTipText("Exporte l'histogramme au format image");
    exportationHisto_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {

        CtuluImageExport.exportImageFor(donnees_.application_, histo_);
      }
    });

    // etape 6: creation du panel des boutons des courbes:
    this.controlPanelHisto_.add(quitter3_);
    this.controlPanelHisto_.add(exportationHisto_);
    this.panelHisto_.add(this.controlPanelHisto_, BorderLayout.SOUTH);

    /** listener des boutons quitter */
    this.quitter_.setToolTipText("Ferme la sous-fen�tre");
    this.quitter2_.setToolTipText("Ferme la sous-fen�tre");
    this.quitter3_.setToolTipText("Ferme la sous-fen�tre");
    final ActionListener actionQuitter = new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        Sinavi3ResultatComparaisonGenerationBateaux.this.windowClosed();
      }
    };
    this.quitter_.addActionListener(actionQuitter);
    this.quitter2_.addActionListener(actionQuitter);
    this.quitter3_.addActionListener(actionQuitter);

    // ajout d'un menuBar
    // petite barre de menu agreable
  /*  final JMenuBar menuBar = new JMenuBar();
    final JMenu menuFile = new JMenu("Fichier");
    final JMenuItem menuFileExit = new JMenuItem("Quitter");
    final JMenu menuOption = new JMenu("Options");
    final JMenu menuInfo = new JMenu("A propos de");

    // menuFile.set.setLabel("Fichier");
    // menuOption.setLabel("Options");
    // menuFileExit.setLabel("Quitter");
    // menuInfo.setLabel("A propos de");

    menuFileExit.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        Sinavi3ResultatComparaisonGenerationBateaux.this.windowClosed();
      }
    });
    menuFile.add(menuFileExit);
    menuBar.add(menuFile);
    menuBar.add(menuOption);
    menuBar.add(menuInfo);
    setJMenuBar(menuBar);
*/
  }

  /**
   * Methode d'affichage du tableau remarque: cete m�thode sert aussi de rafraichissement du tableau
   * 
   * @param val entier qui indique le num�ro de la cat�gorie de navire � afficher si ce parametre vaut -1 alorso n
   *          affiche la totalit� des navires
   */

  /**
   * Methode qui permet de d�crire le graphe � afficher.
   * 
   * @return chaine: chaine qui contient la des cription de la chaine de caracteres.
   */
  String affichageGraphe() {

    String g = "";

    // determiner el nombre de cat�gories de navires selectionn�s

    // variable qui permet de determiner a quel indice du navire on est car on en prends pas tous les indices de navires
    int indiceNavire = 0;

    g += "graphe\n{\n";
    g += "  titre \"Comparaison de simulations\"\n";
    g += "  sous-titre \"Bateaux g�n�r�s : cat�gorie " + (String) this.ListeNavires_.getSelectedItem()
        + " \"\n";
    g += "  animation non\n";
    g += "  legende " + "oui" + "\n";
    
    g += " marges\n {\n";
    g += " gauche 100\n"; g += " droite 100\n"; g += " haut 50\n"; g += " bas 30\n }\n";

    g += "  axe\n  {\n"; // abscisses
    g += "    titre \"Simulation" + "\"\n";
    g += "    unite \"num�ro\"\n";
    g += "    orientation " + "horizontal" + "\n";
    g += "    graduations oui\n";
    g += "    minimum " + 0 + "\n";
    g += "    maximum " + (this.nombreSimulationsComparees_ + 3)// (this.donnees_.categoriesNavires_.listeNavires_.size()+1)
        + "\n";
    System.out.print("nb comparaisons: " + this.nombreSimulationsComparees_);

    g += "  }\n";

    g += "  axe\n  {\n"; // Ordonn�es
    g += "    titre \"Bateaux" + "\"\n";
    g += "    unite \"" + "nombre" + "\"\n";
    g += "    orientation " + "vertical" + "\n";
    g += "    graduations oui\n";
    g += "    minimum " + 0 + "\n";
    g += "    maximum "
        + FonctionsSimu.diviserSimu(Sinavi3AlgorithmesComparaisonSimulation.determinerMaxGenerationNavires(this.listeParamsSimu_,
            this.nombreSimulationsComparees_))

        + "\n";
    g += "  }\n";

    g += "  courbe\n  {\n";
    g += "    titre \"" + "nombre de navires g�n�r�s"

    + "\"\n";
    g += "    type " + "courbe" + "\n";
    g += "    aspect\n {\n";
    g += "contour.largeur 0 \n";
    g += "surface.couleur BBCC00 \n";
    g += "texte.couleur 000000 \n";
    g += "contour.couleur BBCC00 \n";

    g += "    }\n";
    g += "    valeurs\n    {\n";

    indiceNavire = 0;
    for (int n = 0; n < this.nombreSimulationsComparees_; n++) {
      if (this.tableauChoixSimulations_[n].isSelected() && this.comparePossible_[n] != -1) {
        g += (indiceNavire + 1)// numero de la cat�gorie
            + " "
            + FonctionsSimu.diviserSimu(this.listeParamsSimu_[n].ResultatsCompletsSimulation.ResultatsGenerationNavires[this.comparePossible_[n]]); // tableau
                                                                                                                          // qui
                                                                                                                          // contient
                                                                                                                          // le
                                                                                                                          // nombre
                                                                                                                          // de
                                                                                                                          // navire
                                                                                                                          // pour
                                                                                                                          // chaque
                                                                                                                          // cat�gorie

        final String os = System.getProperty("os.name");
        int debut = 0;
        if (os.startsWith("Windows")) {
          debut = this.listeProjet_[n].getFichier().lastIndexOf("\\") + 1;
        } else {
          debut = this.listeProjet_[n].getFichier().lastIndexOf("/") + 1;
        }

        g += "\n etiquette  \n \""
            + this.listeProjet_[n].getFichier().substring(debut,
                this.listeProjet_[n].getFichier().lastIndexOf(".sipor")) + "\" \n" + "\n";
        System.out
            .println("valeur= "
                + this.listeParamsSimu_[n].ResultatsCompletsSimulation.ResultatsGenerationNavires[this.comparePossible_[n]]);
        indiceNavire++;
      }
    }// din du pour remplissage des coordonn�es
    g += "    }\n";

    g += "  }\n";

    /*
     * g += " contrainte\n"; g += "{\n"; // a mettre le seuil g += "titre \"seuil : max =" + 50 + "\"\n"; //
     * str+="orientation horizontal \n"; g += " type max\n"; g += " valeur " + 100 +
     * CtuluLibString.LINE_SEP_SIMPLE;///la valeur ordonn�e du seuil g += " \n }\n";
     */
    // }//fin du for

    return g;
  }

  /**
   * methode qui retoune l histogramme correspondant aux donn�es resultats:
   * 
   * @return
   */
  String affichageHistogramme() {

    String g = "";

    // determiner el nombre de cat�gories de navires selectionn�s

    // variable qui permet de determiner a quel indice du navire on est car on en prends pas tous les indices de navires
    int indiceNavire = 0;

    g += "graphe\n{\n";
    g += "  titre \"Comparaison de simulations\"\n";
    g += "  sous-titre \"Bateaux g�n�r�s : cat�gorie " + (String) this.ListeNavires_.getSelectedItem()
        + " \"\n";
    g += "  animation non\n";
    g += "  legende " + "oui" + "\n";
    
    g += " marges\n {\n";
    g += " gauche 100\n"; g += " droite 100\n"; g += " haut 50\n"; g += " bas 30\n }\n";

    g += "  axe\n  {\n"; // abscisses
    g += "    titre \"Simulation" + "\"\n";
    g += "    unite \"num�ro\"\n";
    g += "    orientation " + "horizontal" + "\n";
    g += "    graduations oui\n";
    g += "    minimum " + 0 + "\n";
    g += "    maximum " + (this.nombreSimulationsComparees_ + 3)// (this.donnees_.categoriesNavires_.listeNavires_.size()+1)
        + "\n";
    System.out.print("nb comparaisons: " + this.nombreSimulationsComparees_);

    g += "  }\n";

    g += "  axe\n  {\n"; // Ordonn�es
    g += "    titre \"Bateaux" + "\"\n";
    g += "    unite \"" + "nombre" + "\"\n";
    g += "    orientation " + "vertical" + "\n";
    g += "    graduations oui\n";
    g += "    minimum " + 0 + "\n";
    g += "    maximum "
        + FonctionsSimu.diviserSimu(Sinavi3AlgorithmesComparaisonSimulation.determinerMaxGenerationNavires(this.listeParamsSimu_,
            this.nombreSimulationsComparees_))

        + "\n";
    g += "  }\n";

    g += "  courbe\n  {\n";
    g += "    titre \"" + "nombre de navires g�n�r�s"

    + "\"\n";
    g += "    type " + "histogramme" + "\n";
    g += "    aspect\n {\n";
    g += "contour.largeur 1 \n";
    g += "surface.couleur BB8800 \n";
    g += "texte.couleur 000000 \n";
    g += "contour.couleur 000000 \n";

    g += "    }\n";
    g += "    valeurs\n    {\n";

    indiceNavire = 0;
    for (int n = 0; n < this.nombreSimulationsComparees_; n++) {
      if (this.tableauChoixSimulations_[n].isSelected() && this.comparePossible_[n] != -1) {
        g += (indiceNavire + 1)// numero de la cat�gorie
            + " "
            + FonctionsSimu.diviserSimu(this.listeParamsSimu_[n].ResultatsCompletsSimulation.ResultatsGenerationNavires[this.comparePossible_[n]]); // tableau
                                                                                                                          // qui
                                                                                                                          // contient
                                                                                                                          // le
                                                                                                                          // nombre
                                                                                                                          // de
                                                                                                                          // navire
                                                                                                                          // pour
                                                                                                                          // chaque
                                                                                                                          // cat�gorie

        final String os = System.getProperty("os.name");
        int debut = 0;
        if (os.startsWith("Windows")) {
          debut = this.listeProjet_[n].getFichier().lastIndexOf("\\") + 1;
        } else {
          debut = this.listeProjet_[n].getFichier().lastIndexOf("/") + 1;
        }

        g += "\n etiquette  \n \""
            + this.listeProjet_[n].getFichier().substring(debut,
                this.listeProjet_[n].getFichier().lastIndexOf(".sipor")) + "\" \n" + "\n";
        System.out
            .println("valeur= "
                + this.listeParamsSimu_[n].ResultatsCompletsSimulation.ResultatsGenerationNavires[this.comparePossible_[n]]);
        indiceNavire++;
      }
    }// din du pour remplissage des coordonn�es
    g += "    }\n";

    g += "  }\n";

    /*
     * g += " contrainte\n"; g += "{\n"; // a mettre le seuil g += "titre \"seuil : max =" + 50 + "\"\n"; //
     * str+="orientation horizontal \n"; g += " type max\n"; g += " valeur " + 100 +
     * CtuluLibString.LINE_SEP_SIMPLE;///la valeur ordonn�e du seuil g += " \n }\n";
     */
    // }//fin du for

    return g;
  }

  public void actionPerformed(final ActionEvent ev) {
    final Object source = ev.getSource();

    // action commune a tous les �v�nements: redimensionnement de la fenetre
    final Dimension actuelDim = this.getSize();
    final Point pos = this.getLocation();

    // si la source provient d un navire du tableau de checkBox
    if (source == this.ListeNavires_) {
      // on v�rifie que la comparaison est possible
      this.comparePossible_ = VerificationComparaisonSimulationsPossibleEntreNavires();
      // mise a jour de l'histogramme
      final String descriptionHisto = this.affichageHistogramme();
      this.histo_.setFluxDonnees(new ByteArrayInputStream(descriptionHisto.getBytes()));
      // mise a jour des courbes
      final String descriptionCourbes = this.affichageGraphe();
      this.graphe_.setFluxDonnees(new ByteArrayInputStream(descriptionCourbes.getBytes()));

    }
    for (int i = 0; i < this.tableauChoixSimulations_.length; i++) {
      if (source == this.tableauChoixSimulations_[i]) {
        // on v�rifie que la comparaison est possible
        this.comparePossible_ = VerificationComparaisonSimulationsPossibleEntreNavires();
        // mise a jour de l'histogramme
        final String descriptionHisto = this.affichageHistogramme();
        this.histo_.setFluxDonnees(new ByteArrayInputStream(descriptionHisto.getBytes()));
        // mise a jour des courbes
        final String descriptionCourbes = this.affichageGraphe();
        this.graphe_.setFluxDonnees(new ByteArrayInputStream(descriptionCourbes.getBytes()));

      }
    }

    // on redimensionne la fenetre comme elle etais avant manipulation des elements graphique
    this.setSize(actuelDim);
    this.setLocation(pos);
  }// fin de actionPerformed

  /**
   * Methode qui permet de v�rifier si les simulations sont comparables:
   * 
   * @return un tableau de booleen qui dit pour chaque simulation si elle est comparable ou non
   */
  public int[] VerificationComparaisonSimulationsPossibleEntreNavires() {
    final int[] comparePossible = new int[10];
    // on v�rifie que l'�l�ment navire a comparer existe bien dans l'ensemble des simulations
//    final int indiceNavire = this.ListeNavires_.getSelectedIndex();
    final String nomNavire = (String) this.ListeNavires_.getSelectedItem();
    /*
     * //etape 1: on verifie qu'il y a bien moins d'�l�ments for(int i=0;i<this.nombreSimulationsComparees_;i++)
     * if(this.listeParamsSimu_[i].navires.listeNavires.length<=indiceNavire) comparePossible[i]=-1;
     */
    // etape 2: on v�rifie que le nom du navire existe bien dans chaque simu
    boolean trouve = false;
    System.out.println("\n***\nelement a voir: " + nomNavire);
    for (int i = 0; i < this.nombreSimulationsComparees_; i++) {
      trouve = false;
      for (int k = 0; !trouve && k < this.listeParamsSimu_[i].navires.listeNavires.length; k++) {
        if ((this.listeParamsSimu_[i].navires.listeNavires[k].nom).equals(nomNavire)) {
          trouve = true;
          comparePossible[i] = k;
          System.out.println("Simu "+i+": correspond � indice "+k);
        }
      }
      if (trouve) {} else {
        comparePossible[i] = -1;
      }
    }

    return comparePossible;
  }

  /**
   * Methode qui s active lorsque l'on quitte l'application
   */
  protected void windowClosed() {
    // verif.stop(); //stop n est aps sur on le modifie donc par une autre variable

    // desactivation du thread
    // dureeVieThread=false;

    System.out.print("Fin de la fenetre de gestion des cercles!!");
    dispose();
  }

}
