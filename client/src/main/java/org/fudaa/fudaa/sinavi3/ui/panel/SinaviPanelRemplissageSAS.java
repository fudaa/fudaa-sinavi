/**
 *@creation 6 oct. 06
 *@modification $Dates$
 *@license  GNU General Public Licence 2
 *@copyright   (c)1998-2006 CETMEF 2 bd Gambetta F -60231 Compiegne
 *@mail   devel@fudaa.fr 
 */
package org.fudaa.fudaa.sinavi3.ui.panel;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

import javax.swing.BorderFactory;
import javax.swing.JFileChooser;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.Border;

import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuDialogChoice;
import com.memoire.bu.BuDialogError;
import com.memoire.bu.BuDialogMessage;
import com.memoire.bu.BuTable;
import com.memoire.fu.FuLog;

import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.table.CtuluTableExcelWriter;
import org.fudaa.fudaa.ressource.FudaaResource;
import org.fudaa.fudaa.sinavi3.Sinavi3Bordures;
import org.fudaa.fudaa.sinavi3.Sinavi3CellEditorDureeParcours;
import org.fudaa.fudaa.sinavi3.Sinavi3DataSimulation;
import org.fudaa.fudaa.sinavi3.Sinavi3Implementation;
import org.fudaa.fudaa.sinavi3.Sinavi3InternalFrame;
import org.fudaa.fudaa.sinavi3.ui.modeles.SinaviModeleExcel;
import org.fudaa.fudaa.sinavi3.ui.modeles.SinaviModeleRemplissageSAS;


/**
 * IHM permet de saisir le temps de remplissage des SAS des ecluses par cat�gorie de navire.
 * @version $Version$
 * @author hadoux
 */
@SuppressWarnings("serial")
public class SinaviPanelRemplissageSAS extends Sinavi3InternalFrame  {

  /**
   * Tableau contenant les donn�es du tableau affich� en java (definition attribut car sert pour transformation excel)
   */
  Object[][] ndata;

  /**
   * Descriptif des elements des colonnes
   */
  String[] titreColonnes_;

  /**
   * Tableau de type JTable qui contiendra les donnees des navires
   */

  public  BuTable tableau_;

  /**
   * Modele qui contient la partie metier des donn�es.
   * Utilisee comme mod�le du tableau
   */
  public  SinaviModeleRemplissageSAS modeleTableau_;
  
  
  

  /**
   * Bouton de validation des regles de navigations.
   */

  private final BuButton impression_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_generer"), "Excel");
  private final BuButton valider_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_oui"), "Valider");
  private final BuButton duplication_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_ranger"), "Dupliquer");
  
  
  /**
   * Panel qui contiendra le tableau.
   */
  public JPanel global_ = new JPanel();

  /**
   * panel de controle
   */
  public JPanel controlPanel = new JPanel();
  /**
   * Bordure du tableau.
   */

  public Border borduretab_ = BorderFactory.createLoweredBevelBorder();

  /**
   * donn�es de la simulation
   */
  public Sinavi3DataSimulation donnees_;

  /**
   * Indice de l'ecluse choisie (par defaut 0 => la premiere ecluse.
   */
  public  int ecluseChoisie = 0;

  /**
   * constructeur du panel .
   * 
   * @param d donn�es de la simulation.
   */
  public SinaviPanelRemplissageSAS(final Sinavi3DataSimulation _donnees) {

    super("", true, true, true, true);

    donnees_ = _donnees;
    
    //-- Creation du modele du tableau --//
    modeleTableau_=new SinaviModeleRemplissageSAS(donnees_);
    
    global_.setLayout(new BorderLayout());

    /**
     * Initialisation du nombre de cheneaux.
     */

    this.affichage();

    /**
     * Creation de la fenetre
     */

    setTitle("D�lai d'optimisation du remplissage du SAS (en Heures.Minutes) ");
    setSize(800, 400);
    setBorder(Sinavi3Bordures.compound_);
    getContentPane().setLayout(new BorderLayout());
    
    final JScrollPane ascenceur = new JScrollPane(global_);

    getContentPane().add(ascenceur, BorderLayout.CENTER);

    controlPanel.setBorder(this.borduretab_);
    controlPanel.add(this.impression_);
    controlPanel.add(valider_);
    controlPanel.add(duplication_);
     
    duplication_.setToolTipText("Permet de dupliquer les d�lais de remplissage d'un SAS avec celles d'un autre SAS.");

    getContentPane().add(controlPanel, BorderLayout.SOUTH);

    /**
     * Listener du comboChoix de chenal: lors dde la selection du chenal, on affiche (rafraichis) le tableau de choix
     * des regles de navigations pour ce chenal
     */
    this.impression_
        .setToolTipText("G�n�re un fichier Excel du tableau. Attention, ce bouton ne g�n�re que le tableau du chenal affich�.");

    valider_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        dispose();
      }
    });
    
    this.duplication_.addActionListener(new ActionListener() {
        public void actionPerformed(final ActionEvent e) {
            duplicateSAS();
          }

		
        });
    

    this.impression_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        tableau_.editCellAt(0, 0);
        // generation sous forme d'un fichier excel:
        File fichier;
        // definition d un file chooser
        final JFileChooser fc = new JFileChooser();
        final int returnVal = fc.showOpenDialog(SinaviPanelRemplissageSAS.this);

        if (returnVal == JFileChooser.APPROVE_OPTION) {
          fichier = fc.getSelectedFile();
          final File f = CtuluLibFile.appendExtensionIfNeeded(fichier, "xls");

          // on r�cupere l abstrct model du tableau contenant les donn�es

          /**
           * creation d un abstract model impl�mentant l'interface CtuluTableModelInterface
           */
          final SinaviModeleExcel modele = modeleTableau_;
          
          /**
           * on essaie d 'ecrire en format excel
           */
          final CtuluTableExcelWriter ecrivain = new CtuluTableExcelWriter(modele, f);

          try {
            ecrivain.write(null);
            donnees_.getApplication();
            new BuDialogMessage(donnees_.getApplication().getApp(), Sinavi3Implementation.informationsSoftware(), 
                "Fichier Excel g�n�r� avec succ�s.").activate();

          } catch (final RowsExceededException _err) {
            FuLog.error(_err);
          } catch (final WriteException _err) {
            FuLog.error(_err);
          } catch (final IOException _err) {
            FuLog.error(_err);
          }

        }// fin du if si le composant est bon
      }

    });

    
    // affichage de la frame
    setVisible(true);

  }
  
  private void duplicateSAS() {
	  final String[] values = new String[donnees_.getListeEcluse().getListeEcluses().size()];

	  for (int i = 0; i < donnees_.getListeEcluse().getListeEcluses().size(); i++) {
		  values[i] = donnees_.getListeEcluse().retournerEcluse(i).getNom();
	  }

	  BuDialogChoice choixModele = new BuDialogChoice(donnees_.getApplication().getApp(), Sinavi3Implementation.informationsSoftware(),
			  "Choix du SAS de l'�cluse qui servira de mod�le", "Copie les r�gles du SAS de l'�cluse choisie ci-dessous", values);

	  int reponse = choixModele.activate();
	  if (reponse != -1) {

		  if (reponse == 0) {

			  final int ecluseModele = donnees_.getListeEcluse().retourneIndice(choixModele.getValue());
			  if (ecluseModele != -1) {

				  //-- on choisit l'�cluse cible --//
				  final String[] valuesCibles = new String[donnees_.getListeEcluse().getListeEcluses().size()-1];
				  int cpt=0;	
				  for (int i = 0; i < donnees_.getListeEcluse().getListeEcluses().size(); i++) {
					  if(i !=ecluseModele)
						  valuesCibles[cpt++] = donnees_.getListeEcluse().retournerEcluse(i).getNom();
				  }
				  
				  choixModele = new BuDialogChoice(donnees_.getApplication().getApp(), Sinavi3Implementation.informationsSoftware(),
						  "Choix du SAS de l'�cluse dans lequel coller les valeurs", "colle les r�gles pr�c�dente dans le SAS de l'�cluse choisie ci-dessous", valuesCibles);
				  reponse = choixModele.activate();
				  if (reponse != -1) {

					  if (reponse == 0) {

						  final int ecluseCible = donnees_.getListeEcluse().retourneIndice(choixModele.getValue());
						  if (ecluseCible != -1) {

							  donnees_.getReglesRemplissageSAS().dupliquerLigne(ecluseModele, ecluseCible, donnees_.getListeBateaux_().NombreNavires());


						  }
						  affichage();
					  }
				  }

			  } else {
				  new BuDialogError(donnees_.getApplication().getApp(), Sinavi3Implementation.informationsSoftware(),
						  "La duplication est impossible: le SAS de l'�cluse choisi n'existe pas.").activate();
			  }

		  }

	  } else {
		  new BuDialogError(donnees_.getApplication().getApp(), Sinavi3Implementation.informationsSoftware(),
				  "La duplication est impossible: le SAS de l'�cluse choisi n'existe pas.").activate();
	  }

  }

  /**
   * Methode de remplissage des JComboBox et des donn�es par d�fauts pour chaque objet.
   */
  void remplissage() {}

  /**
   * Methode d affichage des composants du BuTable et du tableau de combo Cette methode est a impl�menter dans les
   * classes d�riv�es pour chaque composants
   */
  void affichage() {

    
    // --creation du tableau avec l'obejt modele qui g�re la liaison entre utilisateur et partie m�tier --//
    tableau_= new BuTable(modeleTableau_);
    
    //-- editeur par d�faut du tableau --// 
    tableau_.setDefaultEditor(Object.class,new Sinavi3CellEditorDureeParcours());
  
    this.global_.add(/* ascenceur */tableau_.getTableHeader(), BorderLayout.PAGE_START);
    this.global_.add(tableau_, BorderLayout.CENTER);

    this.global_.revalidate();
    this.global_.updateUI();

    this.revalidate();
    this.updateUI();

  }



  

}
