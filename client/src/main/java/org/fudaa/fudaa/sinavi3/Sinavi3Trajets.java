package org.fudaa.fudaa.sinavi3;

import java.util.ArrayList;

/**
 * Cette classe d�finit la structure de liste de trajets et des m�thodes associ�es
 *@version $Version$
 * @author hadoux
 *
 */
public class Sinavi3Trajets {

	ArrayList listeTrajet=new ArrayList();
	
	public Sinavi3Trajets(){
		
	}
	
	public int size(){
		return listeTrajet.size();
	}
	
	/**Procedure de verification de l'existence d'au moins un trajet**/
	public boolean existeTrajet(){
		if(listeTrajet.size()==0)
			return false;
		return true;
	}
	
	/**retourner un trajet**/
	
	public Sinavi3Trajet retournerTrajet(int i){
		
		return (Sinavi3Trajet)listeTrajet.get(i);
	}
	
	/** suppression trajet **/
	public void supprimerTrajet(int i){
		listeTrajet.remove(i);
	}
	
	
	/** ajout d'un trajet **/
	public void ajouterTrajet(Sinavi3Trajet trajet){
		listeTrajet.add(trajet);
	}
	
	/** modification d'un trajet **/
	void modifierTrajet(Sinavi3Trajet trajet, int i){
		
		listeTrajet.set(i, trajet);
		
	}
	
	//-- Methode qui retourne le plus grand nombre de couple deterministe pour un trajet --//
	public int maxLoisaisieTrajet(){
		int max=0;
		for(int i=0;i<size();i++)
			if(retournerTrajet(i).nbCouples()>max)
				max=retournerTrajet(i).nbCouples();
		
		return max;
		
	}
	
	
	//-- Methode utilis� pour la duplication de bateaux: permet de dupliquer l'ensemble du trajet --//
	
	public Sinavi3Trajets dupliquerTrajets(){
		
		Sinavi3Trajets copie=new Sinavi3Trajets();
		
		for(int i=0;i<size();i++){
			
			Sinavi3Trajet trajet =new Sinavi3Trajet();
			
			trajet.gareArrivee_=retournerTrajet(i).gareArrivee_;
			trajet.gareDepart_=retournerTrajet(i).gareDepart_;
			trajet.sens_=retournerTrajet(i).sens_;
			trajet.loiErlangGenerationNavire=retournerTrajet(i).loiErlangGenerationNavire;
			trajet.loiErlangEcartType=retournerTrajet(i).loiErlangEcartType;
			trajet.typeErlangLoi=retournerTrajet(i).typeErlangLoi;
			
			trajet.nbBateauxattendus=retournerTrajet(i).nbBateauxattendus;
			trajet.erlangDebut=retournerTrajet(i).erlangDebut;
			trajet.erlangFin=retournerTrajet(i).erlangFin;
			trajet.typeLoiGenerationNavires_=retournerTrajet(i).typeLoiGenerationNavires_;
			
			
			//-- copie des lois deterministes --//
			trajet.loiDeterministeOUjournaliere_=retournerTrajet(i).copieLoiDeterministe();		
			
			//-- ajout du trajet dupliquer dans la liste des trajet --//
			copie.ajouterTrajet(trajet);
		}
		
		return copie;
		
	}
	
}
