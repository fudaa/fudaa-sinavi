/**
 *@creation 19 fev. 09
 *@modification $Dates$
 *@license  GNU General Public Licence 2
 *@copyright   (c)1998-2006 CETMEF 2 bd Gambetta F -60231 Compiegne
 *@mail   devel@fudaa.fr 
 */
package org.fudaa.fudaa.sinavi3;

import java.io.File;
import java.io.IOException;

import javax.swing.JFileChooser;

import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.table.CtuluTableExcelWriter;

import com.memoire.fu.FuLog;
import org.fudaa.fudaa.sinavi3.ui.results.reports.Sinavi3SheetProducer;

/**
 * Classe proposant des m�thodes statiques utiles � plusieurs classes dans la gestion des r�sultats.
 * 
 * @version $Version$
 * @author Mederic FARGEIX
 */
public final class Sinavi3GestionResultats {
	
	private Sinavi3GestionResultats() {}
	@Deprecated
        public static void exportTableau(Sinavi3InternalFrame _parent, String[] _titre, Object[][] _contenu) {
		File fichier;
        final JFileChooser fc = new JFileChooser();
        final int returnVal = fc.showSaveDialog(_parent);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
        	fichier = fc.getSelectedFile();
        	final File f = CtuluLibFile.appendExtensionIfNeeded(fichier, "xls");
        	final Sinavi3ModeleExcel modele = new Sinavi3ModeleExcel();
        	modele.nomColonnes_ = _titre;
        	modele.data_ = new Object[_contenu.length][_titre.length];
        	/** recopiage des donn�es */
        	for (int i = 0; i < _contenu.length; i++) {
        		modele.data_[i] = _contenu[i];
        	}
        	modele.setNbRow(_contenu.length);
        	/** on essaie d 'ecrire en format xls */
        	final CtuluTableExcelWriter ecrivain = new CtuluTableExcelWriter(modele, f);
        	try {
        		ecrivain.write(null);
        	} catch (final RowsExceededException _err) {
        		FuLog.error(_err);
        	} catch (final WriteException _err) {
        		FuLog.error(_err);
        	} catch (final IOException _err) {
        		FuLog.error(_err);
        	}
        }
	}

    public static void exportReport(Sinavi3DataSimulation data, boolean dureeParcours, boolean attente, boolean occup, boolean indispo, boolean bassin, boolean multiSimu) {
        /*File fichier;
        final JFileChooser fc = new JFileChooser();
        final int returnVal = fc.showSaveDialog(null);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            fichier = new File(fc.getSelectedFile().getAbsolutePath() + "_" + System.currentTimeMillis());
            final File f = CtuluLibFile.appendExtensionIfNeeded(fichier, "xls");
        */  
        Sinavi3SheetProducer producer = new Sinavi3SheetProducer();
            producer.buildReportUi(data, occup, attente, occup, indispo, bassin, multiSimu);
       // }
    }


	/**
	   * Methode permettant de deduire une valeur pertinente d'intervalle de distribution.
	   * 
	   * @return
	   */

	  public static int determineValeurIntervalleDistribution(final float _maxi, final int _nb) {
	    int val;
	    int ratio = Math.round(_maxi / _nb);
	    int length = Integer.toString(ratio).length();
	    
	    val = (int) (Math.ceil(_maxi / (_nb * Math.pow(10, length - 1))) * Math.pow(10, length - 1));
	    
	    if (val == 0) val = 1;
	    
	    return val;
	  }
	
}