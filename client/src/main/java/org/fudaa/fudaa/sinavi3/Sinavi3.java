/**
 * @file         Sipor.java
 * @creation     1999-10-01
 * @modification $Date: 2007-11-23 11:28:02 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.sinavi3;

import java.io.PrintStream;

import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import org.fudaa.fudaa.commun.impl.Fudaa;

/**
 * Permet de lancer l'application cliente de SIPOR.
 * 
 * @version $Revision: 1.1 $ $Date: 2007-11-23 11:28:02 $ by $Author: hadouxad $
 * @author Nicolas Chevalier , Bertrand Audinet
 */
public class Sinavi3 {

  /**
   * @param args les arg de l'appli
   */
  public static void main(final String[] args) {

	  
	 
	  try {
		//UIManager.setLookAndFeel("com.birosoft.liquid.LiquidLookAndFeel");
		  UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
	} catch (ClassNotFoundException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (InstantiationException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (IllegalAccessException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (UnsupportedLookAndFeelException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	  
	  
	  
    final Fudaa f = new Fudaa();
    if(args != null && args.length>0 && args[0] != null &&  "MOCK=true".equals(args[0]) ) {
    	Sinavi3Implementation.MOCK_ENABLE = true;
    }
   
    f.launch(args, Sinavi3Implementation.informationsSoftware(), true);
    f.startApp(new Sinavi3Implementation());
    
   
   
  }
}