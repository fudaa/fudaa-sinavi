package org.fudaa.fudaa.sinavi3.sinavi3Network;

import java.awt.Color;
import java.util.HashMap;

import javax.swing.UIManager;

import org.fudaa.ebli.network.simulationNetwork.DefaultNetworkUserObject;
import org.fudaa.ebli.network.simulationNetwork.NetworkGraphModel;
import org.fudaa.ebli.network.simulationNetwork.NetworkUserObject;
import org.fudaa.ebli.network.simulationNetwork.SimulationNetworkEditor;
import org.fudaa.ebli.network.simulationNetwork.shape.ShapeChenal;
import org.fudaa.ebli.network.simulationNetwork.ui.EditorMenuBar;
import org.fudaa.fudaa.sinavi3.Sinavi3Bief;
import org.fudaa.fudaa.sinavi3.Sinavi3DataSimulation;
import org.fudaa.fudaa.sinavi3.Sinavi3Ecluse;
import org.fudaa.fudaa.sinavi3.Sinavi3Implementation;
import com.mxgraph.canvas.mxGraphics2DCanvas;
import com.mxgraph.swing.util.mxSwingConstants;
import com.mxgraph.util.mxConstants;
/**
 * Neowrk graph of Sinavi.
 * @author Adrien Hadoux
 *
 */
public class SinaviNetwork extends SimulationNetworkEditor{




public SinaviNetwork(NetworkGraphModel modelSinavi3, boolean isVoixNavigable) {
		super(modelSinavi3,isVoixNavigable);
		
		
		
	
	}

/**
 * usefull method if synchro is lost, this action allow to re-syncrho data to model entities.
 */
public void synchronizeFromSinaviData(Sinavi3DataSimulation d) {
	//-- first clean all --//
	this.cleanAllNetwork();
	startingX = 600;
	startingY= 230;
	startingNodeX = 100;
	startingNodeY= 230;
	HashMap<Integer, DefaultNetworkUserObject> mapGares =new HashMap<Integer, DefaultNetworkUserObject>();
	//-- add each element --//
	for(int i=0;i< d.getListeGare_().getListeGares_().size() ; i++) {
		String value = d.getListeGare_().retournerGare(i);
		DefaultNetworkUserObject gare =  new DefaultNetworkUserObject(value, true);
		this.addNewNetworkElement(DEFAULT_VALUE_GARE,gare);
		mapGares.put(i,gare);
		
	}
	
		
	for(int i=0;i< d.getListeBief_().getListeBiefs_().size() ; i++) {
		NetworkUserObject data = d.getListeBief_().retournerBief(i);
		 this.addNewNetworkElement(DEFAULT_VALUE_BIEF,data);
		connectElement(mapGares.get(d.getListeBief_().retournerBief(i).getGareAmont_()),
				mapGares.get(d.getListeBief_().retournerBief(i).getGareAval_()), data);
	}
	for(int i=0;i< d.getListeEcluse().getListeEcluses().size() ; i++) {
		NetworkUserObject data = d.getListeEcluse().retournerEcluse(i);
		 this.addNewNetworkElement(DEFAULT_VALUE_ECLUSE,data);
		 connectElement(mapGares.get(d.getListeEcluse().retournerEcluse(i).getGareAmont_()),
					mapGares.get(d.getListeEcluse().retournerEcluse(i).getGareAval_()), data);
	}
}


/**
 * 
 * @param args
 */
public static void main(String[] args)
{
	try
	{
		UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
	}
	catch (Exception e1)
	{
		e1.printStackTrace();
	}

	mxSwingConstants.SHADOW_COLOR = Color.LIGHT_GRAY;
	mxConstants.W3C_SHADOWCOLOR = "#D3D3D3";
	Sinavi3DataSimulation donnees = new Sinavi3DataSimulation(new Sinavi3Implementation());
	
	
	Sinavi3Bief chenal = new Sinavi3Bief();
	chenal.setNom_("Chenal 0");
	donnees.getListeBief_().ajout(chenal);
	
	Sinavi3Ecluse ecluse = new Sinavi3Ecluse();
	ecluse.setNom_("Ecluse 0");
	donnees.getListeEcluse().ajout(ecluse);
	
	
	
	donnees.getListeGare_().ajout("Gare 0");
	
	mxGraphics2DCanvas.putShape("shapeChenal", new ShapeChenal());
	
	SinaviNetwork networkEditor = new SinaviNetwork(new NetworkGraphModel( new Sinavi3NetworkDaoFactory(donnees)), true);
	networkEditor.createFrame(new EditorMenuBar(networkEditor)).setVisible(true);
	
	Sinavi3Bief c = new Sinavi3Bief();
	c.setNom_("TOToR 5");
	
	
	networkEditor.addNewNetworkElement("Chenal",c);
	
	DefaultNetworkUserObject g = new DefaultNetworkUserObject("GAREOR 2",true);
	DefaultNetworkUserObject g2 = new DefaultNetworkUserObject("GAREOR 3",true);
	networkEditor.addNewNetworkElement("Gare",new DefaultNetworkUserObject("GAREOR",true));
	networkEditor.addNewNetworkElement("Gare",g);
	networkEditor.addNewNetworkElement("Gare",g2);
	networkEditor.addNewNetworkElement("Gare",new DefaultNetworkUserObject("GAREOR 4",true));
	
	
	networkEditor.connectElement(g,g2, c);
	
	
	
}

}
