/**
 *@creation 16 nov. 06
 *@modification $Dates$
 *@license  GNU General Public Licence 2
 *@copyright   (c)1998-2006 CETMEF 2 bd Gambetta F -60231 Compiegne
 *@mail   devel@fudaa.fr 
 */
package org.fudaa.fudaa.sinavi3;

import org.fudaa.dodico.corba.sinavi3.SParametresCoupleDistribution;
import org.fudaa.dodico.corba.sinavi3.SParametresResultatsCompletSimulation;
import org.fudaa.dodico.corba.sinavi3.SParametresResultatsDureParcoursCategories;
import org.fudaa.dodico.corba.sinavi3.SParametresResultatsDureeParcoursNavires;

/**
 * @version $Version$
 * @author hadoux
 */
public class Sinavi3AlgorithmeDureesParcours {

  /** Methode qui realise les calculs pour les g�n�rations de bateaux */
  public static void calcul(final Sinavi3DataSimulation donnees_, final int typeElement1, final int indiceElement1,
      final int typeElement2, final int indiceElement2, final int sens/* ,float horaireDep, float horaireFin */) {

    /**
     * SENS 0 => ENTRANT 1 => SORTANT
     */
    /**
     * ATTENTION WARNING: CAS OU L ON A LA MEME element en entree qu en sortie: on comptabilise le temps d entree et LE
     * TEMPS DE SORTIE DE LA PROCHAINE OCCURENCE DE L ELEMENT DANS LA LISTE DU TRAJET ON MET UN ELSE LORS DE LA
     * SELECTION DU NAVIRE POUR LA SELECTRION SOIT DU TEMPS DE DEPART DE LA GARE OU LE TEMSP DE SORTIE DE LA GARE CAR
     * SINON IL VA PRENDRE LE DEPART ET LA SORTIE DU MEME EVNEMENT!!!!!! LE FAIT DE METTRE UN ELSE PERMET DE PRENDRE LE
     * TEMPS DE DEPART A LA GARE 1 ET PLUS LOIN DE PRENDRE L ARRIVEE A LA GARE 1; EN EFFET LES DONNES DE LA SIMU SONT
     * ORGANIS� EN ORDRE EVENEMENTIEL CROISSANT.
     */

    final SParametresResultatsCompletSimulation resultats = donnees_.params_.ResultatsCompletsSimulation;

    // etape 1: remplissage du tableau resultats.DureesParcoursNavires[]
    // qui donne les dur�es d attente de tous les navires ainsi que leur cat�gorie associ�e.
    resultats.DureeParcoursNavire = new SParametresResultatsDureeParcoursNavires[donnees_.listeResultatsSimu_.nombreNavires];

    for (int i = 0; i < donnees_.listeResultatsSimu_.nombreNavires; i++) {
      // allocation memoire des donn�es
      resultats.DureeParcoursNavire[i] = new SParametresResultatsDureeParcoursNavires();
      // initialisation des dur�es des heures entrees des elements dans les 2 sens
      resultats.DureeParcoursNavire[i].heure1Entree1Navire = -1;
      resultats.DureeParcoursNavire[i].heure1Entree2Navire = -1;
      resultats.DureeParcoursNavire[i].heure2Entree1Navire = -1;
      resultats.DureeParcoursNavire[i].heure2Entree2Navire = -1;
      resultats.DureeParcoursNavire[i].categorieAssociee = donnees_.listeResultatsSimu_.listeEvenements[i].categorie;

     
      
      //-- si le sens correspond bien: --//
      if(sens==donnees_.listeResultatsSimu_.listeEvenements[i].sens){
      
      for (int k = 0; k < donnees_.listeResultatsSimu_.listeEvenements[i].NbElemtnsParcours; k++) {
        if (donnees_.listeResultatsSimu_.listeEvenements[i].tableauTrajet[k].indiceElement == indiceElement1
            && donnees_.listeResultatsSimu_.listeEvenements[i].tableauTrajet[k].typeElement == typeElement1) {
          // si l'�l�ment du trajet est equivalent a celui entr� en parametre 1
         
        	
            //-- recuperation des heures d'entr�e/sortie du premier element --//
            resultats.DureeParcoursNavire[i].heure1Entree1Navire = donnees_.listeResultatsSimu_.listeEvenements[i].tableauTrajet[k].heureEntree;
            resultats.DureeParcoursNavire[i].heure1Entree2Navire = donnees_.listeResultatsSimu_.listeEvenements[i].tableauTrajet[k].heureSortie;
            
            if (donnees_.listeResultatsSimu_.listeEvenements[i].tableauTrajet[k].indiceElement == indiceElement2
                    && donnees_.listeResultatsSimu_.listeEvenements[i].tableauTrajet[k].typeElement == typeElement2) {
                 
                	//-- recuperation des heures d'entr�e/sortie du deuxieme element --//
            	resultats.DureeParcoursNavire[i].heure2Entree1Navire = donnees_.listeResultatsSimu_.listeEvenements[i].tableauTrajet[k].heureEntree;
                resultats.DureeParcoursNavire[i].heure2Entree2Navire = donnees_.listeResultatsSimu_.listeEvenements[i].tableauTrajet[k].heureSortie;
            }
          
        }// fin du if si l'�l�ment du trajet est egal a celui entree en parametre 1 de l utilisateur
        else if (donnees_.listeResultatsSimu_.listeEvenements[i].tableauTrajet[k].indiceElement == indiceElement2
            && donnees_.listeResultatsSimu_.listeEvenements[i].tableauTrajet[k].typeElement == typeElement2) {
         
        	//-- recuperation des heures d'entr�e/sortie du deuxieme element --//
        	resultats.DureeParcoursNavire[i].heure2Entree1Navire = donnees_.listeResultatsSimu_.listeEvenements[i].tableauTrajet[k].heureEntree;
        	resultats.DureeParcoursNavire[i].heure2Entree2Navire = donnees_.listeResultatsSimu_.listeEvenements[i].tableauTrajet[k].heureSortie;
            
           

        }// fin du if si l'�l�ment du trajet est egal a celui entree en parametre 2 de l utilisateur
      }
   
    
    }//-- fin test du sens --// 
    
    }// fin de l algorithme 1

    // etape 2: remplissage du tableau de cat�gories de navires avec els don�nes suivantes:
    // dur�e de parcour total de tous les navire de la cat�gorie
    // nombre de navires de la cat�gorie
    // dur�e minimum parcour
    // dur�e maximum du parcour:
    // utilisation de la structure DureeParcoursCat�gorie:
    resultats.DureeParcoursCategorie = new SParametresResultatsDureParcoursCategories[donnees_.listeBateaux_.listeNavires_
        .size()];
    float duree = 0;
    for (int i = 0; i < donnees_.listeBateaux_.listeNavires_.size(); i++) {
      // allocation memoire
      resultats.DureeParcoursCategorie[i] = new SParametresResultatsDureParcoursCategories();
      // initialisation des donn�es des dur�es de parcours pour les cat�gories
      resultats.DureeParcoursCategorie[i].dureeMaximumParcours = 0;
      resultats.DureeParcoursCategorie[i].dureeMinimumParcours = 9999999;
      resultats.DureeParcoursCategorie[i].dureeParcoursTotale = 0;
      resultats.DureeParcoursCategorie[i].nombreNavires = 0;

      for (int k = 0; k < donnees_.listeResultatsSimu_.nombreNavires; k++) {
        if (resultats.DureeParcoursNavire[k].categorieAssociee == i
        		) {
          // si la cat�gorie du navire qui contient les dur�es de parcours est �quivalente a la cat�gorie selectionn�e
          duree = 0;
          // exploitation des horaires trouv�s en fonction du sens demand�

          
            // cas normal: type ou element differents
            if (resultats.DureeParcoursNavire[k].heure1Entree1Navire!=-1 && resultats.DureeParcoursNavire[k].heure1Entree2Navire!=-1 && resultats.DureeParcoursNavire[k].heure2Entree1Navire!=-1 && resultats.DureeParcoursNavire[k].heure2Entree2Navire!=1) {
              // sens entrant:
              // on prend els horaires entrees 1 des element 1 et 2
            	duree = Math.max((float) (resultats.DureeParcoursNavire[k].heure2Entree2Navire - resultats.DureeParcoursNavire[k].heure1Entree1Navire), (float) (resultats.DureeParcoursNavire[k].heure1Entree2Navire - resultats.DureeParcoursNavire[k].heure2Entree1Navire));
         
              
            // exploitation de la donnee duree recuperee pour la categorie
        	
            
        	  resultats.DureeParcoursCategorie[i].nombreNavires++;
        	
        	
        	resultats.DureeParcoursCategorie[i].dureeParcoursTotale += duree;
           
            
            if (resultats.DureeParcoursCategorie[i].dureeMaximumParcours < duree) {
              resultats.DureeParcoursCategorie[i].dureeMaximumParcours = duree;
            }

            if (resultats.DureeParcoursCategorie[i].dureeMinimumParcours > duree) {
              resultats.DureeParcoursCategorie[i].dureeMinimumParcours = duree;
            }

          }
        
        	  
        }// fni du si la cat�gorie concorde

      }// fin du for
      if (resultats.DureeParcoursCategorie[i].dureeMinimumParcours == 9999999) {
        resultats.DureeParcoursCategorie[i].dureeMinimumParcours = 0;
      }
    }
    
    int m = 0;
    int nbIntervalles = resultats.nombreIntervallesDistribution;
    int valeurIntervalleDistribution = Sinavi3GestionResultats.determineValeurIntervalleDistribution(determineDureeMaxi(donnees_), nbIntervalles);
    for (int i = 0; i < donnees_.listeBateaux_.listeNavires_.size(); i++) {
        // allocation memoire
    	resultats.DureeParcoursCategorie[i].distribution = new short[nbIntervalles];
        resultats.DureeParcoursCategorie[i].valeurIntervalleDistribution = valeurIntervalleDistribution; 
        for (int k = 0; k < donnees_.listeResultatsSimu_.nombreNavires; k++) {
        	if (resultats.DureeParcoursNavire[k].categorieAssociee == i) {
        		// si la cat�gorie du navire qui contient les dur�es de parcours est �quivalente a la cat�gorie selectionn�e
        		duree = 0;
        		// exploitation des horaires trouv�s en fonction du sens demand�
        		// cas normal: type ou element differents
        		if (resultats.DureeParcoursNavire[k].heure1Entree1Navire!=-1 && resultats.DureeParcoursNavire[k].heure1Entree2Navire!=-1 
        				&& resultats.DureeParcoursNavire[k].heure2Entree1Navire!=-1 && resultats.DureeParcoursNavire[k].heure2Entree2Navire!=1)
        		{
        			duree = Math.max((float) (resultats.DureeParcoursNavire[k].heure2Entree2Navire - resultats.DureeParcoursNavire[k].heure1Entree1Navire), 
        					(float) (resultats.DureeParcoursNavire[k].heure1Entree2Navire - resultats.DureeParcoursNavire[k].heure2Entree1Navire));
        			m = ((int) Math.floor(duree))/valeurIntervalleDistribution;
        			resultats.DureeParcoursCategorie[i].distribution[m] ++;
        		}
        	}
        }
    }
  }
	  
  
  /**
   * Methode qui lit la structure ayant deja calcul� l ensemble des dur�es de parcours. Cette methode permet d'eviter de
   * relire le fichier historique
   */

  public static void calcul2(final Sinavi3DataSimulation donnees_, final int typeElement1, final int indiceElement1,
      final int typeElement2, final int indiceElement2, final int sens/* ,float horaireDep, float horaireFin */) {
    
      for (int i = 0; i < donnees_.params_.ResultatsCompletsSimulation.TOUTEDureesParoucrs.length; i++) {
        if (donnees_.params_.ResultatsCompletsSimulation.TOUTEDureesParoucrs[i] == null) {
          return;
        }
        if (typeElement1 == donnees_.params_.ResultatsCompletsSimulation.TOUTEDureesParoucrs[i].typeElement1
            && typeElement2 == donnees_.params_.ResultatsCompletsSimulation.TOUTEDureesParoucrs[i].typeElement2
            && indiceElement1 == donnees_.params_.ResultatsCompletsSimulation.TOUTEDureesParoucrs[i].element1
            && indiceElement2 == donnees_.params_.ResultatsCompletsSimulation.TOUTEDureesParoucrs[i].element2
            && sens == donnees_.params_.ResultatsCompletsSimulation.TOUTEDureesParoucrs[i].sens) {
          // on recupere la structure voulue
          donnees_.params_.ResultatsCompletsSimulation.DureeParcoursCategorie = donnees_.params_.ResultatsCompletsSimulation.TOUTEDureesParoucrs[i].DureeParcoursCategorie;
          System.out.print("trouve!!!\n");
//        affichage test
          for(int j=0;j<donnees_.params_.ResultatsCompletsSimulation.DureeParcoursCategorie.length;j++)
          {
          	System.out.println("categorie "+(j+1)+"; duree parcours:"
          			+donnees_.params_.ResultatsCompletsSimulation.DureeParcoursCategorie[j].dureeParcoursTotale);
          	
          }
          return;
        } else if (typeElement1 == donnees_.params_.ResultatsCompletsSimulation.TOUTEDureesParoucrs[i].typeElement2
            && typeElement2 == donnees_.params_.ResultatsCompletsSimulation.TOUTEDureesParoucrs[i].typeElement1
            && indiceElement1 == donnees_.params_.ResultatsCompletsSimulation.TOUTEDureesParoucrs[i].element2
            && indiceElement2 == donnees_.params_.ResultatsCompletsSimulation.TOUTEDureesParoucrs[i].element1
            && sens == donnees_.params_.ResultatsCompletsSimulation.TOUTEDureesParoucrs[i].sens) {
          // on recupere la structure voulue
          donnees_.params_.ResultatsCompletsSimulation.DureeParcoursCategorie = donnees_.params_.ResultatsCompletsSimulation.TOUTEDureesParoucrs[i].DureeParcoursCategorie;
          System.out.print("trouve!!!");
//        affichage test
          for(int j=0;j<donnees_.params_.ResultatsCompletsSimulation.DureeParcoursCategorie.length;j++)
          {
          	System.out.println("categorie "+(j+1)+"; duree parcours:"
          			+donnees_.params_.ResultatsCompletsSimulation.DureeParcoursCategorie[j].dureeParcoursTotale);
          	
          }
          return;
        }
      }
      System.out.print("aie aie!!! pas trouve!!!");

  }

  /**
   * methode qui permet de deduire la dur�e maxi afin de pouvior fixer la limite max du graphe ainsi que de l
   * histogramme
   * 
   * @return
   */

  public static float determineDureeMaxi(final Sinavi3DataSimulation _donnees) {
    double max = 0;
    final SParametresResultatsCompletSimulation resultats = _donnees.params_.ResultatsCompletsSimulation;

    // recherche du maxdans un tableau:
    for (int i = 0; i < _donnees.listeBateaux_.listeNavires_.size(); i++) {
      if (resultats.DureeParcoursCategorie[i].dureeMaximumParcours > max) {
        max = resultats.DureeParcoursCategorie[i].dureeMaximumParcours;
      }

    }

    return (float) (max + 0.25 * max);
  }
  
}
