package org.fudaa.fudaa.sinavi3;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.AbstractCellEditor;
import javax.swing.JTable;
import javax.swing.table.TableCellEditor;


/**
 * Classe qui d�crit un editeur de cellules pour un composant jtable poru les dur�es de parcours.
 *@version $Version$
 * @author hadoux
 *
 */

public class Sinavi3CellEditorDureeParcours extends AbstractCellEditor
implements TableCellEditor,
ActionListener {

	Object contenu;
	Sinavi3TextField saisie=new Sinavi3TextField(10);
	
	public Sinavi3CellEditorDureeParcours(){
		
	}
	
	
	/**Methode qui retourne le composant graphique destin� � l'utilisateur pour saisir ses donn�es **/
	public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
		
		
		
		
		contenu=value;
		
		//par d�faut on met dans le champs text le contenu pr�c�dent
		saisie.setText((String)value);
		
		return saisie;
		
	}

	/**Methode qui retourne le contenu de la saisie de l'utilisateur **/
	public Object getCellEditorValue() {
		
		//-- remplacement virgule ou point --//
		String contenu=saisie.getText();
		if(!contenu.equals("")) {
			contenu = contenu.replace(",",".").replace(":", ".");
		}
		/*if(!contenu.equals("")){
			int indice=contenu.lastIndexOf(",");
			if(indice!=-1){
				//-- remplacement de la virgule par un point --//
				String section1=contenu.substring(0, contenu.lastIndexOf(","));
				String section2=contenu.substring(contenu.lastIndexOf(",")+1,contenu.length());
				saisie.setText(section1+"."+section2);
			}
		}*/
		
		//-- formatage des H.Min --//
		contenu=saisie.getText();
		float valeur;
		try {
			valeur = Float.parseFloat(contenu);
			float format= conversionFormat(valeur,contenu);
			saisie.setText(""+format);
			
		} catch (NumberFormatException e) {
			//ne rien mettre ici, ce test est r�alis� par la suite dans le abstract model du jtable
		}
		
		
		//saisie.traitementApresFocus(null);
		return saisie.getText();
	}

	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		
	}
	
	/**methode qui formatte un reel en duree.
	 * 2 chiffres apr�s la virgule.
	 * modulo 60 minutes
	 * exemple: 35.652 se transformera en 36.05  **/
	private float conversionFormat(float valeur,String contenu){

		float resul=valeur;
		String min="";
		int minutes=00;

		if(contenu.lastIndexOf(".")==-1)
			minutes=00;
		else
		{
			min=contenu.substring(contenu.lastIndexOf(".")+1);
			minutes=Integer.parseInt(min);
			if(minutes/10==0 && min.length()==1)
				minutes=minutes*10;
		}
		
		//while(minutes/100!=0)
		//	minutes=minutes/10;
		
		//--SI L utilisateur saisie des centiems on signale comme erreur, donc ici on renvoie la saisie de l'utilisateur --//
		if(minutes/100 !=0)
			return valeur;
		
		
		
		System.out.println("minutes: "+minutes);

		//creation des heures
		int heures=0;
		//le cast va arrondir au chiffre le plus proche or on veut garder le nombre tel quel
		heures=(int)valeur;

		//modulo des minutes
		while(minutes>=60)
		{
			heures++;
			minutes-=60;
		}

		float resultat=heures+ (float)(minutes/100.0);
		return resultat;

	}


}
