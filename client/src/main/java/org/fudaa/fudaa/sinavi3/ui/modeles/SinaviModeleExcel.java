/**
 * 
 */
package org.fudaa.fudaa.sinavi3.ui.modeles;

import java.util.List;

import javax.swing.table.AbstractTableModel;

import jxl.write.Label;
import jxl.write.Number;
import jxl.write.WritableCell;

import org.fudaa.ctulu.CtuluLibString;

/**
 * Cette classe permet de cr�er un model de table (Jtable de java) afin de pouvoir l'utiliser pour faire une generation
 * de ficheir excel: en effet cetet classe utilise l'implementation de CtuluTableModelInterface ce qui est necessaire
 * poru utiliser la classe CtuluTableExcelWriter ecrivain = new CtuluTableExcelWriter(modele, file); avec modele : un
 * objet de type de la classe SiporModeleExcel et file: un fichier de type File (ce fichier contiendra les donn�es
 * excel) il suffit alors pour ecrire le fichier en format excel, de faire appel a la methode ecrivain.write(null); qui
 * a pour but d'ecrire els donn�es du contenu de data_ de la classe SiporModelExcel
 * 
 * @author Adrien Hadoux
 */
public class SinaviModeleExcel extends AbstractTableModel implements org.fudaa.ctulu.table.CtuluTableModelInterface {
  private int nbRow_ = 0; // nombre de lignes
  public String[] nomColonnes_;
  public Object[][] data_ = null;// =new Object[0][0];

  /**
   * @return le tableau contenant le nom des colonnes
   */
  public String[] nomCol() {
    return nomColonnes_;
  }

  /**
   * On met � jour le nombre de lignes gr�ce � cette m�thode
   * 
   * @param _n : nombre de ligne
   */
  public void setNbRow(final int _n) {
    nbRow_ = _n;

  }
   public int[] getSelectedRows() {
        return null;
    }

  /**
   * @param _row ligne o� on veut ins�rer les noms des colonnes
   */
  public void initNomCol(final int _row) {
    for (int i = 0; i < getColumnCount(); i++) {
      data_[_row][i] = nomColonnes_[i];
    }

  }

  /**
   * nombre de colonnes
   */
  public int getColumnCount() {

    return nomColonnes_.length;
  }

  /**
   * nombre de lignes
   */
  public int getRowCount() {
    return nbRow_;

  }

  /**
   * retourne la valeur du tableau � la ligne _rowIndex et � la colonne _columnIndex_
   */
  public Object getValueAt(final int _rowIndex, final int _columnIndex) {

    return data_[_rowIndex][_columnIndex];
  }

  /**
   * Cette fonction ne fait pas qu'entrer une valeur elle copie les valeurs du tableau et agrandit la valeur data Il
   * faut entrer les valeurs dans l'ordre. Cette fonction n'est pas optimis�e, et si le tableau est grand elle consomme
   * beaucoup de ressources.
   * 
   * @see javax.swing.table.TableModel#setValueAt(java.lang.Object, int, int)
   */
  public void setValueAt(final Object _value, final int _row, final int _col) {
    // String x=new String(value);
    // x=value;
    final Object[][] temp = new Object[_row + 1][getColumnCount()];
    nbRow_ = _row;
    if (data_ != null) {
      for (int i = 0; i <= _row; i++) {
        if (i == _row) {
          for (int j = 0; j < _col; j++) {
            // temp[i][j]=new Object();
            temp[i][j] = data_[i][j];
          }
        } else {
          for (int j = 0; j < getColumnCount(); j++) {
            // temp[i][j]=new Object();
            temp[i][j] = data_[i][j];
          }
        }
      }
    }
    temp[_row][_col] = _value;
    data_ = new Object[_row + 1][getColumnCount()];
    for (int i = 0; i <= _row; i++) {
      if (i == _row) {
        for (int j = 0; j <= _col; j++) {
          // temp[i][j]=new Object();
          data_[i][j] = temp[i][j];
        }
      } else {
        for (int j = 0; j < getColumnCount(); j++) {
          // temp[i][j]=new Object();
          data_[i][j] = temp[i][j];
        }
      }
    }
  }

  /**
   * retourne le nombre de colonnes
   */
  public int getMaxCol() {
    return nomCol().length;
  }

  /**
   * retourne le nombre de ligne
   */
  public int getMaxRow() {
    return nbRow_;
  }

  /**
   * retourne la valeur du tableau � la ligne _row et � la colonne _col
   * 
   * @see org.fudaa.ctulu.table.CtuluTableModelInterface#getValue(int, int)
   */

  public Object getValue(final int _row, final int _col) {
    return getValueAt(_row, _col);
  }

  /**
   * retoune un tableau pour le format excel Celui-ci sera utilis� avec la fonction write
   * 
   * @see org.fudaa.ctulu.table.CtuluTableModelInterface#getExcelWritable(int, int)
   */
  public WritableCell getExcelWritable(final int _row, final int _col, int _rowXls, int _colXls) {
    final int r = _row;
    final int c = _col;
    final Object o = data_[r][c];
    if (o == null) {
      return null;
    }
    String s = o.toString();
    if(CtuluLibString.isEmpty(s)) return null;
    try {
      return new Number(_colXls, _rowXls, Double.parseDouble(s));
    } catch (final NumberFormatException e) {}
    return new Label(_colXls, _rowXls, s);

    // return cell;
  }

  /**
   * nom de la colonne de num�ro _i.
   * 
   * @see javax.swing.table.TableModel#getColumnName(int)
   */
  public String getColumnName(final int _i) {
    return nomColonnes_[_i];

  }

  /**
   * @param _name : nom � donner � la colonne
   * @param _i : indice de la colonne � renommer
   */
  public void setColumnName(final String _name, final int _i) {
    nomColonnes_[_i] = _name;

  }

public List<String> getComments() {
	
	return null;
}

}// fin de la classe modelExcel
