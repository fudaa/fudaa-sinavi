package org.fudaa.fudaa.sinavi3;

/**
 * 
 */
import java.awt.BorderLayout;
import java.awt.Component;
import java.util.EventObject;

import javax.swing.BorderFactory;
import javax.swing.DefaultCellEditor;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListCellRenderer;
import javax.swing.ListSelectionModel;
import javax.swing.border.Border;
import javax.swing.event.CellEditorListener;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableColumn;
import javax.swing.table.TableModel;

import org.apache.commons.lang.StringUtils;

import com.memoire.bu.BuDialogError;
import com.memoire.bu.BuTable;

// import javax.swing.event.*;
/**
 * Panel d'affichage des donn�es des cheneaux sous forme d un tableau: avec la possibilit� de modifier ou de
 * supprimer une donn�e: NOTE TRES PRATIQUE SUR LE FONCTIONNEMENT DES BuTable IL FAUT AFFICHER LE HEADER SEPAREMMENT
 * SI ON UTILISE PAS DE JSCROLLPANE DIRECTEMENT ON PEUT POUR CELA UTILISER LE LAYOUT BORDERLAYOUT ET AFFICHER LE
 * TABLEHEADER EN DEBUT DE FICHIER ET AFFICHER LE TABLEAU AU CENTER
 * 
 * @author Adrien Hadoux
 */
public class Sinavi3PanelAffichageBief extends JPanel {

  String[] titreColonnes_ = { "Nom", "Hauteur d'eau","longueur","largeur","vitesse","loi Frequence", "Frequence indispo", "duree indispo", "loi Erlang", "Creneau 1", "Creneau 2", "Creneau 3" };

  /**
   * Tableau de type BuTable qui contiendra les donn�es des quais
   */

  Object[][] data_ = {};

  TableModel modele_;
  BuTable tableau_ = new BuTable(data_, titreColonnes_);
  Sinavi3DataSimulation donnees_;
  /**
   * Bordure du tableau
   */

  Border borduretab_ = BorderFactory.createLoweredBevelBorder();

  /**
   * constructeur du panel d'affichage des quais
   * 
   * @param d donn�es de la simulation
   */
  Sinavi3PanelAffichageBief(final Sinavi3DataSimulation d) {

    setLayout(new BorderLayout());

    this.maj(d);

  }

  /**
   * Methode de mise a jour du tableau
   */
  void maj(final Sinavi3DataSimulation d) {
	  donnees_ = d;
	  modele_ = new TableModel() {

			public int getRowCount() {
				return d.listeBief_.listeBiefs_.size();
			}

			public int getColumnCount() {
				return titreColonnes_.length;
			}

			public String getColumnName(int columnIndex) {
				return titreColonnes_[columnIndex];
			}

			public Class<?> getColumnClass(int columnIndex) {

				return String.class;
			}

			public boolean isCellEditable(int rowIndex, int columnIndex) {
				return true;
			}

			public Object getValueAt(int rowIndex, int columnIndex) {
				
				return constructLine(rowIndex)[columnIndex];
			}

			

			public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
				if(StringUtils.isBlank(aValue.toString())) {
					new BuDialogError(donnees_.application_.getApp(), donnees_.application_.isSinavi_, "Saisie manquante")
					.activate();
					return;
				}
				final Sinavi3Bief bief = d.listeBief_.retournerBief(rowIndex);
				try{ 
				switch(columnIndex) {
				case 0: {
					if(d.listeBief_.existeDoublon(aValue.toString(), rowIndex)) {
						new BuDialogError(donnees_.application_.getApp(), donnees_.application_.isSinavi_, "Nom deja pris.")
						.activate();
						
					}else
						bief.setNom_(aValue.toString());
				}break;
				case 1:bief.setProfondeur_(Double.parseDouble(aValue.toString()));break;
				case 2:bief.setLongueur_(Double.parseDouble(aValue.toString()));break;
				case 3:bief.setLargeur_(Double.parseDouble(aValue.toString()));break;
				case 4:bief.setVitesse_(Double.parseDouble(aValue.toString()));break;
				case 5:bief.setTypeLoi_(Integer.parseInt(aValue.toString()));break;
				case 6:bief.setFrequenceMoyenne_(Double.parseDouble(aValue.toString()));break;
				case 7:bief.setDureeIndispo_(Double.parseDouble(aValue.toString()));break;
				case 8:bief.setLoiIndispo_(Integer.parseInt(aValue.toString()));break;
				}
				} catch(NumberFormatException e) {
					new BuDialogError(d.application_.getApp(), d.application_.isSinavi_, "La valeur doit �tre un nombre.")
					.activate();
				}
			}

			public void addTableModelListener(TableModelListener l) {
				// TODO Auto-generated method stub

			}

			public void removeTableModelListener(TableModelListener l) {
				// TODO Auto-generated method stub

			}

		};

		this.tableau_ = new BuTable(modele_);
		tableau_.setDefaultEditor(Object.class,new Sinavi3CellEditor());		
		JComboBox<String> combo = new JComboBox<String>(new String[]{"0","1"});
		tableau_.getColumnModel().getColumn(5).setCellEditor(new DefaultCellEditor(combo));
		tableau_.getColumnModel().getColumn(8).setCellEditor(new DefaultCellEditor(new JComboBox<String>(new String[]{"0","1","2","3","4","5","6","7","8","9","10"})));
		final DefaultListCellRenderer defaultRenderer = new DefaultListCellRenderer();
		combo.setRenderer(new ListCellRenderer<String>() {

			public Component getListCellRendererComponent(
					JList<? extends String> list, String value, int index,
					boolean isSelected, boolean cellHasFocus) {
				 JLabel renderer = (JLabel) defaultRenderer.getListCellRendererComponent(list, value, index,
					        isSelected, cellHasFocus);
				 renderer.setText(value.equals("0")?"Erlang":"Deterministe");
					
				return renderer;
			}
		});
		TableCellEditor editorCreneau = new TableCellEditor() {
			
			public boolean stopCellEditing() {
			return true;
			}
			
			public boolean shouldSelectCell(EventObject anEvent) {
				return true;
			}
			
			public void removeCellEditorListener(CellEditorListener l) {
			}
			
			public boolean isCellEditable(EventObject anEvent) {
				return true;
			}
			
			public Object getCellEditorValue() {
					return null;
			}
			
			public void cancelCellEditing() {
			}
			
			public void addCellEditorListener(CellEditorListener l) {
			}
			
			public Component getTableCellEditorComponent(JTable table, Object value,
					boolean isSelected, int row, int column) {					
				Sinavi3Bief bief = d.listeBief_.retournerBief(row);
				donnees_.application_.addInternalFrame(new Sinavi3FrameSaisieHorairesResume(bief.h_));
					
				return null;
			}
		}; 
		tableau_.getColumnModel().getColumn(9).setCellEditor(editorCreneau);
		tableau_.getColumnModel().getColumn(10).setCellEditor(editorCreneau);
		tableau_.getColumnModel().getColumn(11).setCellEditor(editorCreneau);
   

   
    for(int i=0; i<titreColonnes_.length;i++){
        TableColumn column = tableau_.getColumnModel().getColumn(i);
               column.setPreferredWidth(150); 
               
        }
    
    tableau_.setBorder(this.borduretab_);
    tableau_.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    org.fudaa.fudaa.sinavi.factory.ColumnAutoSizer.sizeColumnsToFit(tableau_);
    tableau_.revalidate();
    this.removeAll();
    this.add(/* ascenceur */tableau_.getTableHeader(), BorderLayout.PAGE_START);

    this.add(new JScrollPane(tableau_), BorderLayout.CENTER);
    tableau_.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
    this.revalidate();
    this.updateUI();
  }
  
  public Object[] constructLine(int line) {
		return constructLine(donnees_.getListeBief_().retournerBief(line));
	}
	
	public Object[] constructLine(Sinavi3Bief bief) {
		Object[] data = new Object[titreColonnes_.length];
		int cpt = 0;
		data[cpt++] = bief.nom_;
		data[cpt++] = "" + (float) bief.profondeur_;
		data[cpt++] = "" + (float) bief.longueur_;
		data[cpt++] = "" + (float) bief.largeur_;
		data[cpt++] = "" + (float) bief.vitesse_;

		// -- lois --//
		if (bief.typeLoi_ == 0) {
			data[cpt++] = "Loi d'Erlang: "
					+ Integer.toString(bief.loiFrequence_);
		} else if (bief.typeLoi_ == 1) {
			data[cpt++] = "Loi deterministe: ";
		} else if (bief.typeLoi_ == 2) {
			data[cpt++] = "Loi journaliere: ";
		}

		if (bief.typeLoi_ == 0) {
			data[cpt++] = "" + (float) bief.frequenceMoyenne_;
		} else {
			data[cpt++] = "";
		}
		data[cpt++] = "" + (float) bief.dureeIndispo_;
		data[cpt++] = Integer.toString(bief.loiIndispo_);
		data[cpt++] = ""
				+ Sinavi3Horaire.format(bief.h_.semaineCreneau1HeureDep)
				+ " � "
				+ Sinavi3Horaire.format(bief.h_.semaineCreneau1HeureArrivee);
		data[cpt++] = ""
				+ Sinavi3Horaire.format(bief.h_.semaineCreneau2HeureDep)
				+ " � "
				+ Sinavi3Horaire.format(bief.h_.semaineCreneau2HeureArrivee);
		data[cpt++] = ""
				+ Sinavi3Horaire.format(bief.h_.semaineCreneau3HeureDep)
				+ " � "
				+ Sinavi3Horaire.format(bief.h_.semaineCreneau3HeureArrivee);

		return data;
	}

}
