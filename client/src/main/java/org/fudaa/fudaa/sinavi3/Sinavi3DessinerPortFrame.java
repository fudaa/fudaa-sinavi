/**
 * 
 */
package org.fudaa.fudaa.sinavi3;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;

import javax.swing.AbstractButton;
import javax.swing.AbstractListModel;
import javax.swing.BorderFactory;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JViewport;
import javax.swing.ListModel;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;

import org.fudaa.dodico.corba.sinavi3.*;
import org.fudaa.fudaa.commun.dodico.FudaaImplementation;
import org.fudaa.fudaa.commun.impl.FudaaCommonImplementation;
import org.fudaa.fudaa.ressource.FudaaResource;
import org.jdesktop.swingx.ScrollPaneSelector;

//import sun.text.Normalizer.QuickCheckResult;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuDialogMessage;
import com.memoire.bu.BuToolBar;

/**
 * Classe qui g�re le panel d'affichage du port affiche un message indiquant le nombre de gare a saisir et un bouton de
 * validation/impression/generation en format postscript.
 * 
 * @author Adrien Hadoux
 */
public class Sinavi3DessinerPortFrame extends Sinavi3InternalFrame {

  /**
   * Panel de dessin du port
   */
  Sinavi3DessinerPort panelDessin_;

  int compteur_ = 0;

  JLabel labelCompteur_;
  JLabel labelMessage1_=new JLabel();
  JLabel labelMessage2_=new JLabel();
  JLabel labelMessage3_=new JLabel();
  //BuPanel panelBoutons_=new BuPanel();
  JMenuItem menuaffichageNoms_;

  private final BuButton quitter_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_quitter"), "Quitter");
  final BuButton rafraichir_ = new BuButton(FudaaResource.FUDAA.getIcon("rafraichir_18"), "rafraichir");
  final BuButton validation_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_generer"), "exporter Ps");
  final BuButton validation2_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_generer"), "exporter");

  final BuButton initialisation_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_creer"), "Nouveau");

  final BuButton retour_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_defaire"), "retour arri�re");

  
  /**
   * Arraylist contenant la liste successives des graphes apr�s chaque modification.
   */
  ArrayList listeActions_=new ArrayList();
  
  /**
   * Liste d�crivant les diff�rentes actions r�alis�es par l'utilisateur. 
   */
  JList bigDataList;
  Sinavi3ActionListModel bigData;
  
  /**
   * Listener
   */
  ActionListener listenerInit_;

  /**
   * Donn�es de la simulation
   */
  Sinavi3DataSimulation donnees_;

  /**
   * Definition d'un scroll pane
   */
  JScrollPane ascenceur_;

  
  boolean firstTime_=true;
  
  /**
   * Constructeur de la fenetre d'affichage du port
   * 
   * @param _donnees donn�es de la simulation
   */
  Sinavi3DessinerPortFrame(final Sinavi3DataSimulation _donnees, final FudaaCommonImplementation _application) {
    super("", true, true, true, true);
    donnees_ = _donnees;

    this.getContentPane().setLayout(new BorderLayout());
    this.setTitle("Mod�lisation du r�seau");
    this.setSize(820, 640);
    this.setBorder(Sinavi3Bordures.compound_);
    // insertion des composants dans la frame
    final JPanel controlPanel = new JPanel(new GridLayout(1,2));
    //controlPanel.setBorder(Sinavi3Bordures.compound_);
    // controlPanel.add(new JLabel("Nombre de gares � ins�rer: "));
    labelCompteur_ = new JLabel("");
    //labelCompteur_.setBorder(Sinavi3Bordures.bordnormal_);
    JPanel panelLabel=new JPanel(new FlowLayout(FlowLayout.LEFT));
    panelLabel.add(labelCompteur_);
    panelLabel.add(labelMessage1_);
    //labelMessage1_.setBorder(Sinavi3Bordures.bordnormal_);
    //labelMessage2_.setBorder(Sinavi3Bordures.bordnormal_);
    //labelMessage3_.setBorder(Sinavi3Bordures.bordnormal_);
    JPanel panelBoutons=new JPanel(new FlowLayout(FlowLayout.LEFT));
    panelBoutons.add(quitter_);
    controlPanel.add(panelBoutons);
    controlPanel.add(panelLabel);
    //JPanel panelLabel2=new JPanel(new FlowLayout(FlowLayout.LEFT));
    panelLabel.add(labelMessage2_);
    //controlPanel.add(panelLabel2);
    //controlPanel.add(this.rafraichir_);
    //controlPanel.add(validation_);
    //controlPanel.add(validation2_);
    //controlPanel.add(initialisation_);
   //controlPanel.add(retour_);
  this.getContentPane().add(controlPanel, BorderLayout.SOUTH);

  
 
  
  
  
    // insertion du panel de dessin dans la frame
    this.panelDessin_ = new Sinavi3DessinerPort(this.donnees_, this.compteur_, this, _application);
    this.ascenceur_ = new JScrollPane(/*panelDessin_*/);

    JViewport port=new JViewport();	
	port.setView(panelDessin_);
	//panelDessinPlaque.setPreferredSize(new Dimension(500,4000));
	//panelDessinPlaque.setBackground(new Color(200,220,240));
	this.ascenceur_.setViewport(port);
	
	//	utilisation de swingx
    ScrollPaneSelector.installScrollPaneSelector(ascenceur_);
    
    final JPanel bourrin = new JPanel();
    bourrin.setSize(1200, 800);
    bourrin.setLayout(new GridLayout(1, 1));
    bourrin.add(ascenceur_);
    this.getContentPane().add(bourrin, BorderLayout.CENTER);

    rafraichir_.setToolTipText("rafraichis le dessin de la modelisation du port");
    rafraichir_.addActionListener(new ActionListener() {

      public void actionPerformed(final ActionEvent e) {
        panelDessin_.repaint();
      }
    });

    //-- Listener retour topologie du port --//
    retour_.addActionListener(new ActionListener() {

        public void actionPerformed(final ActionEvent e) {
          
        	undo();
        
        
        }
      });
    
    
    listenerInit_ = new ActionListener() {

      public void actionPerformed(final ActionEvent e) {
       initialisationDessin();
    	  
    	
      }
    };

    initialisation_.setToolTipText("R�initialiser la mod�lisation du r�seau");
    initialisation_.addActionListener(listenerInit_);
    
    /** listener des boutons quitter */
    this.quitter_.setToolTipText("Ferme la sous-fen�tre");
    final ActionListener actionQuitter = new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        Sinavi3DessinerPortFrame.this.windowClosed();
      }
    };
    this.quitter_.addActionListener(actionQuitter);

    validation_.setToolTipText("Permet de generer un fichier PostScript a partir de la modelisation du port");
    validation_.addActionListener(new ActionListener() {

      public void actionPerformed(final ActionEvent e) {
       
      }
    });

    retour_.setToolTipText("Permet de revenir en rri�re");
    
    validation2_.setToolTipText("Exporter le dessin au format jpg");

    validation2_.addActionListener(new ActionListener() {

      public void actionPerformed(final ActionEvent e) {
        // execution de la methode de generation de JPeg
        panelDessin_.exportation();

      }
    });
   
    
   creationToolBarre();
   creationMenuBar();
   creationUndoList();
   enregistrerAction();
   
   
   // affichage de la fenetre
    this.setVisible(true);

  }

  /**Methode qui reinitialise le dessin **/
  public void initialisationDessin(){
	  panelDessin_.compteur_ = -1;
      donnees_.params_.grapheTopologie = new SParametresGrapheTopologies();
      donnees_.params_.grapheTopologie.nbArcs = 0;
      donnees_.params_.grapheTopologie.graphe = new SParametresGrapheTopologie[2000];
      donnees_.params_.grapheTopologie.nbGaresDessinnees = 0;
      panelDessin_.x_ = 0;
      panelDessin_.y_ = 0;
      panelDessin_.oldX_ = 0;
      panelDessin_.oldY_ = 0;
      panelDessin_.tableauGare_ = new ArrayList();
      panelDessin_.nbGares_ = donnees_.listeGare_.listeGares_.size();;
      System.out.println("REINITIAlISATION***********");
      labelMessage1_.setText("Cliquer sur le dessin pour positionner les gares.");
    	labelMessage2_.setText("");
      panelDessin_.repaint();
  }
  
  
  //-- methode de creation de la barre toolkit --//
  private void creationToolBarre(){
  BuToolBar barre=new BuToolBar();
  //barre.setBorder(SiporBordures.compound_);
  validation_.setVerticalTextPosition(AbstractButton.BOTTOM);
  validation_.setHorizontalTextPosition(AbstractButton.CENTER);
  validation2_.setVerticalTextPosition(AbstractButton.BOTTOM);
  validation2_.setHorizontalTextPosition(AbstractButton.CENTER);
  initialisation_.setVerticalTextPosition(AbstractButton.BOTTOM);
  initialisation_.setHorizontalTextPosition(AbstractButton.CENTER);
  retour_.setVerticalTextPosition(AbstractButton.BOTTOM);
  retour_.setHorizontalTextPosition(AbstractButton.CENTER);
  
  retour_.setFont(new Font("Tahoma",Font.PLAIN,10));
  validation2_.setFont(new Font("Tahoma",Font.PLAIN,10));
  initialisation_.setFont(new Font("Tahoma",Font.PLAIN,10));
  barre.add(initialisation_);
  //barre.add(validation_);
  barre.add(validation2_);
  //barre.add(retour_);
  barre.setFloatable(false);
  this.getContentPane().add(barre,BorderLayout.PAGE_START);
  
  }
  
  
  //-- methode de creation de la barre de menu --//
  private void creationMenuBar(){
	  

	    // petite barre de menu agreable
	    final JMenuBar menuBar = new JMenuBar();
	    final JMenu menuFile = new JMenu("Fichier");
	    final JMenuItem menuFileExit = new JMenuItem("Quitter");
	    final JMenu menuOption = new JMenu("Affichage");
	    final JMenuItem menuRafraichissement = new JMenuItem("Rafraichir");
	    final JMenuItem menuReinitialisation = new JMenuItem("R�initialiser");
	    menuaffichageNoms_ = new JMenuItem("Masquer les noms de param�tres");

	    final JMenu menuInfo = new JMenu("A propos de");
	    final JMenu menuGeneration = new JMenu("Export");
	    final JMenuItem menuJpeg = new JMenuItem("Fichier image");
	    
	    


	    menuFile.add(menuFileExit);
	    menuOption.add(menuRafraichissement);
	    menuOption.add(menuReinitialisation);
	    menuOption.add(menuaffichageNoms_);

	    menuGeneration.add(menuJpeg);
	    menuBar.add(menuFile);
	    menuBar.add(menuOption);
	    menuBar.add(menuGeneration);
	    menuBar.add(menuInfo);
	    setJMenuBar(menuBar);

	  
	    //-- listener des menus --//

	    menuFileExit.addActionListener(new ActionListener() {
	        public void actionPerformed(final ActionEvent e) {
	          Sinavi3DessinerPortFrame.this.windowClosed();
	        }
	      });

	    menuRafraichissement.addActionListener(new ActionListener() {

	      public void actionPerformed(final ActionEvent e) {
	        panelDessin_.repaint();
	      }
	    });
	    menuaffichageNoms_.addActionListener(new ActionListener() {

	      public void actionPerformed(final ActionEvent e) {
	        if (panelDessin_.afficheConnections_ == true) {
	          panelDessin_.afficheConnections_ = false;
	          menuaffichageNoms_.setText("Afficher noms param�tres");
	        } else {
	          panelDessin_.afficheConnections_ = true;
	          menuaffichageNoms_.setText("Masquer noms param�tres");
	        }
	        panelDessin_.repaint();
	      }
	    });

	    menuReinitialisation.addActionListener(this.listenerInit_);


	    menuJpeg.addActionListener(new ActionListener() {

	      public void actionPerformed(final ActionEvent e) {
	        // execution de la methode de generation de JPeg
	        panelDessin_.exportation();

	      }
	    });
  }
  
  
  public void enregistrerAction(){
	  
	  //-- Nouveau graphe duplicata --//
	  SParametresGrapheTopologies nouveau=new SParametresGrapheTopologies();
	  
	  //-- Graphe source --//
	  SParametresGrapheTopologies copie=donnees_.params_.grapheTopologie;
	  
	  nouveau.graphe=new SParametresGrapheTopologie[2000];
	  nouveau.nbArcs = copie.nbArcs;
	    nouveau.nbGaresDessinnees = copie.nbGaresDessinnees;
	    for (int k = 0; k < nouveau.nbArcs; k++) {
	      nouveau.graphe[k] = new SParametresGrapheTopologie();
	      nouveau.graphe[k].nomConnection = copie.graphe[k].nomConnection;
	      nouveau.graphe[k].numGare1 = copie.graphe[k].numGare1;
	      nouveau.graphe[k].numGare2 = copie.graphe[k].numGare2;
	      nouveau.graphe[k].typeConnection = copie.graphe[k].typeConnection;
	      nouveau.graphe[k].xGare1 = copie.graphe[k].xGare1;
	      nouveau.graphe[k].xGare2 = copie.graphe[k].xGare2;
	      nouveau.graphe[k].yGare1 = copie.graphe[k].yGare1;
	      nouveau.graphe[k].yGare2 = copie.graphe[k].yGare2;

	    }
	  
	  ArrayList copieGare=new ArrayList();
	  for(int i=0;i<panelDessin_.tableauGare_.size();i++)
	  {
		  int x=((PointPort)panelDessin_.tableauGare_.get(i)).x_;
		  int y=((PointPort)panelDessin_.tableauGare_.get(i)).y_;
		  PointPort p=new PointPort(x,y);
		  
		  copieGare.add(p);
	  }
	    
	    
	  //--Enregistrement du duplicata dans la listeAction --//
	  Sinavi3ActionClicUSer action=new Sinavi3ActionClicUSer(nouveau,panelDessin_.compteur_,copieGare,donnees_.listeGare_.listeGares_.size());
	  listeActions_.add(action);
	  
	  //-- Mise a jour de la liste des actions --//
	  bigData.miseAjour();
	  
  }
  
  /**Methode qui charge les parametres correspondant � une action precedemment sauvegard�e **/
  public void chargerAction(Sinavi3ActionClicUSer actionPrecedente){

	  panelDessin_.compteur_=actionPrecedente.compteur;
	  donnees_.params_.grapheTopologie= actionPrecedente.graphe;
	  panelDessin_.tableauGare_=actionPrecedente.tableauGare_;
      
	  
	  //-- Nouveau graphe source --//
	  SParametresGrapheTopologies copie=actionPrecedente.graphe;
	  
	  //-- Graphe duplicata --//
	  
	  SParametresGrapheTopologies nouveau=new SParametresGrapheTopologies();
	  
	  nouveau.graphe=new SParametresGrapheTopologie[2000];
	  nouveau.nbArcs = copie.nbArcs;
	    nouveau.nbGaresDessinnees = copie.nbGaresDessinnees;
	    for (int k = 0; k < nouveau.nbArcs; k++) {
	      nouveau.graphe[k] = new SParametresGrapheTopologie();
	      nouveau.graphe[k].nomConnection = copie.graphe[k].nomConnection;
	      nouveau.graphe[k].numGare1 = copie.graphe[k].numGare1;
	      nouveau.graphe[k].numGare2 = copie.graphe[k].numGare2;
	      nouveau.graphe[k].typeConnection = copie.graphe[k].typeConnection;
	      nouveau.graphe[k].xGare1 = copie.graphe[k].xGare1;
	      nouveau.graphe[k].xGare2 = copie.graphe[k].xGare2;
	      nouveau.graphe[k].yGare1 = copie.graphe[k].yGare1;
	      nouveau.graphe[k].yGare2 = copie.graphe[k].yGare2;

	    }
	  
	  ArrayList copieGare=new ArrayList();
	  for(int i=0;i<actionPrecedente.tableauGare_.size();i++)
	  {
		  int x=((PointPort)actionPrecedente.tableauGare_.get(i)).x_;
		  int y=((PointPort)actionPrecedente.tableauGare_.get(i)).y_;
		  PointPort p=new PointPort(x,y);
		  
		  copieGare.add(p);
	  }
	  //on reinitialise les parametres a leur ancienne valeur
	  panelDessin_.tableauGare_=copieGare;
	  panelDessin_.compteur_=actionPrecedente.compteur;
	  donnees_.params_.grapheTopologie=nouveau;
	  
  }
  
  public void undo(){
	  
	  if(listeActions_.size()<1)
		  {
		  System.out.println("Dessin depar: on reinitialise");
		  initialisationDessin();
		  return;
		  }
	  //-- On recupere la derniere action realisee --//	
	  Sinavi3ActionClicUSer actionPrecedente=(Sinavi3ActionClicUSer)listeActions_.get(listeActions_.size()-1);
	  
	  chargerAction(actionPrecedente);
	  
	  //--On supprime la derniere action de la liste puisque l'on retourne � elle --//
      listeActions_.remove(listeActions_.size()-1);
      //listeActions_.remove(listeActions_.size()-1);
     
      
      
      panelDessin_.repaint();
  }
  
  
public void undoSpecial(int index){
	  
	  if(index<1)
		  {
		  System.out.println("Dessin depar: on reinitialise");
		  initialisationDessin();
		  return;
		  }
	  //-- On recupere la derniere action realisee --//	
	  Sinavi3ActionClicUSer actionPrecedente=(Sinavi3ActionClicUSer)listeActions_.get(index);
	  
	  //on reinitialise les parametres a leur ancienne valeur
	  chargerAction(actionPrecedente);
	  
      
	  
	  //--On supprime la derniere action de la liste puisque l'on retourne � elle --//
     // listeActions_.remove(listeActions_.size()-1);
      //listeActions_.remove(listeActions_.size()-1);
     
      
      
      panelDessin_.repaint();
  }
  
/**methode de creation de la list des actions pour revenir en arriere. **/
  public void creationUndoList(){
	
	  	bigData = new Sinavi3ActionListModel(listeActions_);
 
		 bigDataList = new JList(bigData);
		 
		 bigDataList.setOpaque(true);
		 JPanel panelUndo=new JPanel(new BorderLayout());
		 panelUndo.add(new JScrollPane(bigDataList),BorderLayout.CENTER);
		 panelUndo.setBorder(BorderFactory.createTitledBorder(Sinavi3Bordures.compound_, "Actions"));
		 
		 BuButton nettoyer=new BuButton(FudaaResource.FUDAA.getIcon("crystal_corbeille"),"Nettoyer");
		 nettoyer.setFont(new Font("Tahoma",Font.PLAIN,10));
		 nettoyer.addActionListener(new ActionListener(){

			public void actionPerformed(ActionEvent e) {
				
				if(listeActions_.size()>1){
					Sinavi3ActionClicUSer init=(Sinavi3ActionClicUSer)listeActions_.get(0);
					listeActions_.clear();
					listeActions_.add(init);
					//-- Mise a jour de la liste des actions --//
					bigData.miseAjour();
				}
			}
			 
		 });
		 
		 panelUndo.add(nettoyer,BorderLayout.SOUTH);
		 this.getContentPane().add(panelUndo,BorderLayout.EAST);
		 
		 
		 
		 
		 //-- Listener de la liste --//
		 MouseListener mouseListener = new MouseAdapter() {
		     public void mouseClicked(MouseEvent e) {
		         //if (e.getClickCount() == 1) {
		             int index = bigDataList.locationToIndex(e.getPoint());
		             System.out.println("Double clicked on Item " + index);
		             
		             undoSpecial(index);
		             
		          //}
		     }
		 };
		 
		 
		 //ajout du listener de la liste
		 bigDataList.addMouseListener(mouseListener);
		 
  }
 
  protected void windowClosed() {
	    System.out.print("Fin de la fenetre de mod�lisation du r�seau!!");
	    dispose();
  }
  
}
