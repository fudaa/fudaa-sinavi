package org.fudaa.fudaa.sinavi3;


/**
 * Classe qui d�crit lun navire g�n�r� par l'executable fortran g�narr.
 *@version $Version$
 * @author hadoux
 *
 */

public class GenarrNavire {

	int navire;
	int categorie;
	int jour;
	int heure;
	int minute;
	//int secondes;
	String sens;
	int gareDep;
	int gareArriv;
	
	public GenarrNavire(int navire, int categorie, int jour, int heure, int minute/*,int secondes*/,String sens,int gareDep,int gareArr) {
		super();
		this.navire = navire;
		this.categorie = categorie;
		this.jour = jour;
		this.heure = heure;
		this.minute = minute;
		//this.secondes=secondes;
		this.sens=sens;
		this.gareDep=gareDep;
		this.gareArriv=gareArr;
	}
	
	public GenarrNavire(GenarrNavire clone){
		navire=clone.navire;
		categorie=clone.categorie;
		jour=clone.jour;
		heure=clone.heure;
		minute=clone.minute;
		sens=clone.sens;
		gareDep=clone.gareDep;
		gareArriv=clone.gareArriv;
	}
	
	
}
