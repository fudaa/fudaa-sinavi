/*
 * @file         SiporListeSimulations.java
 * @creation     1999-10-01
 * @modification $Date: 2007-11-23 11:28:09 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.sinavi3;

import java.awt.Component;
import java.beans.PropertyVetoException;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Vector;

import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import org.fudaa.fudaa.commun.projet.FudaaFiltreFichier;
import org.fudaa.fudaa.commun.projet.FudaaProjet;

import com.memoire.bu.BuCommonInterface;
import com.memoire.bu.BuDesktop;
import com.memoire.bu.BuDialogError;
import com.memoire.bu.BuDialogMessage;
import com.memoire.bu.BuEmptyList;
import com.memoire.fu.FuLib;

/**
 * Liste g�rant l'ouverture de plusieurs simulations � la fois.
 * 
 * @version $Revision: 1.1 $ $Date: 2007-11-23 11:28:09 $ by $Author: hadouxad $
 * @author Nicolas Chevalier
 */
public class Sinavi3ListeSimulations extends BuEmptyList implements ListSelectionListener {
  DefaultListModel model_;
  Sinavi3Implementation sinavi;

  public Sinavi3ListeSimulations(final BuCommonInterface appli) {
    super();
    sinavi = (Sinavi3Implementation) appli.getImplementation();
    model_ = new DefaultListModel();
    setModel(model_);
    setEmptyText("Aucune simulation");
    setCellRenderer(new AfficheurNomsFichiers());
    addListSelectionListener(this);
  }

  /** Ajoute une simulation dans la liste. */
  public void ajouteSimulation(final String nomFichier, final FudaaProjet projet) {
    sinavi.projets_.put(nomFichier, projet);
    if (!model_.contains(nomFichier)) {
      model_.addElement(nomFichier);
    }
    revalidate();
    repaint(200);
  }

  /** Retire une simulation de la liste. */
  public void enleveSimulation(final String nomFichier) {
    sinavi.projets_.remove(nomFichier);
    model_.removeElement(nomFichier);
    revalidate();
    repaint(200);
  }

  /** Changement du projet en cours dans l'impl�mentation. */
  public void valueChanged(final ListSelectionEvent evt) {
    if (evt.getValueIsAdjusting()) {
      return;
    }
    
    final JList source = (JList) evt.getSource();
    // Effectue le changement de projet en cours.

    sinavi.projet_ = (FudaaProjet) sinavi.projets_.get(source.getSelectedValue());
    sinavi.outils_.setProjet(sinavi.projet_);

    /**
     * modification du projet pour SiporDataSimulation
     */
    sinavi.donnees_.changerProjet(sinavi.projet_);

    sinavi.setTitle(sinavi.projet_.getFichier());
    System.out.println("valueChanged : " + sinavi.projet_.getFichier());
    sinavi.activeActionsExploitation();
    // Mise � jour de fp_ et fermeture de fRappelDonnees.

    final BuDesktop dk = sinavi.getMainPanel().getDesktop();
    if (sinavi.fRappelDonnees_ != null) {
      try {
        sinavi.fRappelDonnees_.setClosed(true);
        sinavi.fRappelDonnees_.setSelected(false);
      } catch (final PropertyVetoException ex) {}
    }
    if (sinavi.fStatistiquesFinales_ != null) {
      try {
        sinavi.fStatistiquesFinales_.setClosed(true);
        sinavi.fStatistiquesFinales_.setSelected(false);
      } catch (final PropertyVetoException ex) {}
      dk.removeInternalFrame(sinavi.fStatistiquesFinales_);
      sinavi.fStatistiquesFinales_ = null;
    }

  }

  /** Pour enregistrer la liste des simulations ouvertes dans un fihcier .pro. */
  protected void enregistre() {
    final String[] ext = { "pro" };
    String contenu = "";
    final Object[] tempo = model_.toArray();
    final String[] cles = new String[tempo.length];
    for (int i = 0; i < tempo.length; i++) {
      cles[i] = (String) tempo[i];
    }
    // V�rifie que toutes les simulations sont dans le meme repertoire
    String rpt = cles[0];
    // test si fichier du repertoire courant
    if (rpt.lastIndexOf(File.separator) == -1) {
      rpt = System.getProperty("user.dir") + File.separator + "exemples" + File.separator + "sipor" + File.separator;
    } else {
      rpt = rpt.substring(0, rpt.lastIndexOf(File.separator) + 1);
    }
    String cle = "";
    for (int i = 0; i < cles.length; i++) {
      cle = cles[i];
      // si chemin d'acces identique
      if ((cle.startsWith(rpt)) && ((cle.substring(rpt.length())).lastIndexOf(File.separator) == -1)) {
        contenu += cle.substring(rpt.length()) + "\n";
      } else if (rpt.equalsIgnoreCase(System.getProperty("user.dir") + File.separator + "exemples" + File.separator
          + "sipor" + File.separator)) {
        contenu += cle + "\n";
      } else {
        new BuDialogError(sinavi.getApp(), Sinavi3Implementation.isSinavi_,
            "Les simulations doivent etre dans le meme r�pertoire").activate();
        return;
      }
    }
    // V�rifie que le fichier d'enregistrement de la liste se trouve dans le meme repertoire
    // que les simulations.
    String nomFichier = MethodesUtiles.choisirFichierEnregistrement(ext, (Component) sinavi.getApp(), rpt);
    if (nomFichier == null) {
      return;
    }
    if (!nomFichier.substring(0, nomFichier.lastIndexOf(File.separator) + 1).equals(rpt)) {
      new BuDialogError(sinavi.getApp(), Sinavi3Implementation.isSinavi_,
          "La liste doit etre enregistr�e dans\nle meme repertoire que les simulations").activate();
      return;
    }
    // ajoute l"extension si elle est manquante
    if (nomFichier.lastIndexOf(".pro") != (nomFichier.length() - 4)) {
      nomFichier += ".pro";
    }
    MethodesUtiles.enregistrer(nomFichier, contenu, sinavi.getApp());
  }

  /** Pour ouvrir un ensemble de simulations (d�fini dans un fichier .pro). */
  protected void ouvrir() {
    // Ferme les simulations ouvertes.
    final Object[] liste = model_.toArray();
    for (int i = 0; i < liste.length; i++) {
      setSelectedValue(liste[i], true);
      sinavi.fermer();
    }
    final String[] ext = { "pro" };
    final String nomFichier = MethodesUtiles.choisirFichier(ext, (Component) sinavi.getApp(), null, 1);
    if (nomFichier == null) {
      return;
    }
    final String rpt = nomFichier.substring(0, nomFichier.lastIndexOf(File.separator) + 1);
    final Vector simulations = new Vector();
    // fred si exception le flux ne sera pas frme
    BufferedReader lecteur = null;
    try {
      lecteur = new BufferedReader(new FileReader(nomFichier));
      String line = "";
      while ((line = lecteur.readLine()) != null) {
        simulations.add(rpt + line);
      }
      // dans le finally

    } catch (final IOException ex) {
      new BuDialogError(sinavi.getApp(), Sinavi3Implementation.isSinavi_, "Lecture du fichier impossible");
    } finally {
      FuLib.safeClose(lecteur);
    }
    for (int i = 0; i < simulations.size(); i++) {
      final FudaaProjet projet = new FudaaProjet(sinavi.getApp(), new FudaaFiltreFichier("sipor"));
      projet.setEnrResultats(true);
      projet.ouvrir((String) simulations.elementAt(i));
      if (projet.estConfigure()) {
        ajouteSimulation((String) simulations.elementAt(i), projet);
      }
    }
    if (sinavi.projets_.size() > 0) {
      new BuDialogMessage(sinavi.getApp(), Sinavi3Implementation.isSinavi_, "Fichiers charg�s").activate();
      setSelectedIndex(0);
      sinavi.activerCommandesSimulation();
      sinavi.activeActionsExploitation();
    }
  }
}
