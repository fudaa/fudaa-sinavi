package org.fudaa.fudaa.sinavi3;

import java.awt.Color;
import java.awt.event.FocusEvent;

import com.memoire.bu.BuDialogError;

public class Sinavi3TextFieldDureePM extends Sinavi3TextField{

	
	public Sinavi3TextFieldDureePM() {
		super();
		// TODO Auto-generated constructor stub
	}
	

	public Sinavi3TextFieldDureePM(int columns) {
		super(columns);
		// TODO Auto-generated constructor stub
	}

	public Sinavi3TextFieldDureePM(String text, int columns) {
		super(text, columns);
		// TODO Auto-generated constructor stub
	}

	public Sinavi3TextFieldDureePM(String text) {
		super(text);
		// TODO Auto-generated constructor stub
	}

	@Override
	public String getText() {
		String res = super.getText();
		if(res != null) {
			res = res.replace(":",".").replace(",", ".");
		}
		return res;
	}

	@Override
	public void setText(String res) {
		if(res != null) {
			res = res.replace(".",":").replace(",", ":");
		}
		super.setText(res);
	}
	
	
	/**Methode destinee a etre surchargee par les classes h�ritant de SiporTextField.
	 * Permet de g�rer les contr�les apr�s saisie de l'utilisateur **/
	public void afterFocus(FocusEvent e){

		String contenu=this.getText();

		if (contenu.equals("")) 
			return;
		//-- Traduction en float --//

		try {
			float valeur=Float.parseFloat(contenu);
			

			if(valeur>6 || valeur<-6){
				new BuDialogError(null, Sinavi3Implementation.isSinavi_,
				"Le nombre doit �tre compris entre -6 et 6.").activate();
				setText("");
				this.requestFocus();
			}else
			{
				
				if(contenu.lastIndexOf(".")!=-1){
					String unite=contenu.substring(contenu.lastIndexOf(".")+1, contenu.length());
					if(unite.length()>2){
						new BuDialogError(null, Sinavi3Implementation.isSinavi_,
						"Il doit y avoir 2 chiffres maximum apr�s la virgule").activate();
						setText("");
						this.requestFocus();
						return;
					}
					
					
						float valUnite=Float.parseFloat(unite);
						if(valUnite>=60){
							new BuDialogError(null, Sinavi3Implementation.isSinavi_,
							"Les unites doivent �tre inf�rieures � 60 minutes.").activate();
							setText("");
							this.requestFocus();
							return;
							
						}
					
				
				}
				
			
				//transformation en nombre � 2 chiffres apr�s le virgule
				float format= valeur;//conversionFormat(valeur,contenu);
				this.setText(""+format);
			}
			

		} catch (NumberFormatException e1) {
			new BuDialogError(null, Sinavi3Implementation.isSinavi_,
			"Ce nombre n'existe pas.").activate();
			setText("");
			this.requestFocus();
		}

		Sinavi3TextFieldDureePM.this.setForeground(Color.black);
	}


	/**methode qui formatte un reel en duree.
	 * 2 chiffres apr�s la virgule.
	 * modulo 60 minutes
	 * exemple: 35.652 se transformera en 36.05  **/
	protected float conversionFormat(float valeur,String contenu){

		boolean isNegative=false;
		
		if(valeur<0){
			isNegative=true;
			valeur=valeur*-1;
			contenu=""+valeur;
		}
		
		
		float resul=valeur;
		String min="";
		int minutes=00;

		if(contenu.lastIndexOf(".")==-1)
			minutes=00;
		else
		{
			min=contenu.substring(contenu.lastIndexOf(".")+1);
			minutes=Integer.parseInt(min);
			if(minutes/10==0 && min.length()==1)
				minutes=minutes*10;
		}
		while(minutes/100!=0)
			minutes=minutes/10;

		System.out.println("minutes: "+minutes);

		//creation des heures
		int heures=0;
		//le cast va arrondir au chiffre le plus proche or on veut garder le nombre tel quel
		heures=(int)valeur;

		//modulo des minutes
		while(minutes>=60)
		{
			heures++;
			minutes-=60;
		}

		float resultat=heures+ (float)(minutes/100.0);
		
		if(isNegative)
			resultat=resultat*-1;
		
		return resultat;

	}

	protected int taille(){
		return 5;
	}
	
protected void specifier_validator(){
	
	}	
	
	
}
