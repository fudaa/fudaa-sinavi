package org.fudaa.fudaa.sinavi3;

import jxl.write.Label;
import jxl.write.Number;
import jxl.write.WritableCell;

import org.fudaa.ctulu.CtuluLibString;

import com.memoire.bu.BuDialogError;

public class GenarrModeleTable extends Sinavi3ModeleExcel{

	GenarrListeNavires listeNavires_;	
	
	Sinavi3DataSimulation donnees_;
	
	String[] titreColonnes_ = { "Bateau", "Cat�gorie", "Jour","Heure","Minute","Sens","Gare d�part","Gare arriv�e" };
	
public GenarrModeleTable(Sinavi3DataSimulation d){
	listeNavires_=d.genarr_;
	donnees_=d;
}



public int getColumnCount() {
	return titreColonnes_.length;
}

public int getRowCount() {
	return listeNavires_.taille();
}


/** retourne l'objet correspondant correspondant au parcours **/
public Object getValueAt(int i, int j) {
	
	
	if(j==0)return ""+listeNavires_.retourner(i).navire;
	else
		if(j==1) return donnees_.listeBateaux_.retournerNavire(listeNavires_.retourner(i).categorie).nom;
		else
			if(j==2) return ""+listeNavires_.retourner(i).jour;
			else
				if(j==3) return ""+listeNavires_.retourner(i).heure;
				else
					if(j==4)
					return ""+listeNavires_.retourner(i).minute;
					else
						/*if(j==5)
						return ""+listeNavires_.retourner(i).secondes;
						else*/
							if(j==5)
							return ""+listeNavires_.retourner(i).sens;
							else
								if(j==6)
								return ""+donnees_.listeGare_.retournerGare(listeNavires_.retourner(i).gareDep) ;
								else
									
									return ""+donnees_.listeGare_.retournerGare(listeNavires_.retourner(i).gareArriv);
					
				
	
}


public void setValueAt(Object value, int rowIndex, int columnIndex) {
	
	int nbMaxCategories=donnees_.listeBateaux_.listeNavires_.size();
	int nbMaxJoursSimu=donnees_.params_.donneesGenerales.nombreJours;
	
	if(columnIndex==1)
	{
		try {
			String nomCateg=(String)value;
			int val=donnees_.listeBateaux_.retournerIndiceNavire(nomCateg);	
			
			if(val==-1)
			{
				new BuDialogError(donnees_.application_.getApp(), Sinavi3Implementation.isSinavi_,
						"La cat�gorie "+nomCateg+" n'existe pas.").activate();
				return ;
			}

			//-- modification des valeurs --//
			GenarrNavire gn=listeNavires_.retourner(rowIndex);
			gn.categorie=val;


		} catch (NumberFormatException e) {
			new BuDialogError(donnees_.application_.getApp(), Sinavi3Implementation.isSinavi_,
					"La cat�gorie doit �tre un entier compris entre 0 et "+(nbMaxCategories-1)).activate();
			return;
		}
	}
	else
		if(columnIndex==2)
		{
			try {
				int val=Integer.parseInt((String)value);

				if(val>nbMaxJoursSimu*FonctionsSimu.NOMBRE_SIMULATIONS+2)
				{
					new BuDialogError(donnees_.application_.getApp(), Sinavi3Implementation.isSinavi_,
							"Le jour doit �tre un entier inf�rieur � "+(nbMaxJoursSimu*FonctionsSimu.NOMBRE_SIMULATIONS+2)).activate();
					return ;
				}

				//-- modification des valeurs --//
				GenarrNavire gn=listeNavires_.retourner(rowIndex);
				gn.jour=val;
				this.listeNavires_.trierNaviresChronologiquement();

			} catch (NumberFormatException e) {
				new BuDialogError(donnees_.application_.getApp(), Sinavi3Implementation.isSinavi_,
						"Le jour doit �tre un entier inf�rieur � "+(nbMaxJoursSimu)).activate();
				return;
			}
		}
		else
			if(columnIndex==3)
			{
				try {
					int val=Integer.parseInt((String)value);

					if(val>24)
					{
						new BuDialogError(donnees_.application_.getApp(), Sinavi3Implementation.isSinavi_,
								"L'heure doit �tre un entier inf�rieur strict � 24").activate();
						return ;
					}

					//-- modification des valeurs --//
					GenarrNavire gn=listeNavires_.retourner(rowIndex);
					gn.heure=val;
					this.listeNavires_.trierNaviresChronologiquement();

				} catch (NumberFormatException e) {
					new BuDialogError(donnees_.application_.getApp(), Sinavi3Implementation.isSinavi_,
							"L'heure doit �tre un entier inf�rieur � 24").activate();
					return;
				}
			}
			else
				if(columnIndex==4)
				{
					try {
						int val=Integer.parseInt((String)value);

						if(val>60)
						{
							new BuDialogError(donnees_.application_.getApp(), Sinavi3Implementation.isSinavi_,
									"Les minutes doivent �tre un entier inf�rieur strict � 60").activate();
							return ;
						}

						//-- modification des valeurs --//
						GenarrNavire gn=listeNavires_.retourner(rowIndex);
						gn.minute=val;
						this.listeNavires_.trierNaviresChronologiquement();

					} catch (NumberFormatException e) {
						new BuDialogError(donnees_.application_.getApp(), Sinavi3Implementation.isSinavi_,
								"Les minutes doivent �tre un entier inf�rieur strict � 60").activate();
						return;
					}
				}
	
	
//	mise a jour du tableau (cot� graphique)
	if(columnIndex>=2){
	//-- modification chronologique: un tri s'impose et mise a jour globale modele --//
		
		miseAjour();
		
		
	}else
	fireTableCellUpdated(rowIndex, columnIndex);
	
}


public void miseAjour(){
	fireTableDataChanged();
}

public boolean isCellEditable(int row, int col) {
    //Note that the data/cell address is constant,
    //no matter where the cell appears onscreen.
    if (col < 1 ||col>5) {
        return false;
    } else {
        return true;
    }
}


/**Methode qui permet de recuperer les noms des colonnes du tableau **/
public String getColumnName(int column) {
	
	return titreColonnes_[column];
}


public int getMaxCol() {
    // TODO Auto-generated method stub
    return getColumnCount();
  }

  /**
   * retourne le nombre de ligne
   */
  public int getMaxRow() {
    // TODO Auto-generated method stub
    return getRowCount();
  }

  /**
   * retoune un tableau pour le format excel Celui-ci sera utilis� avec la fonction write
   * 
   * @see org.fudaa.ctulu.table.CtuluTableModelInterface#getExcelWritable(int, int)
   */
  public WritableCell getExcelWritable(final int _row, final int _col, int _rowXls, int _colXls) {
    final int r = _row;
    final int c = _col;
    /*
     * if (column_ != null) c = column_[c]; if (row_ != null) { r = row_[r]; }
     */
    final Object o = getValueAt(r,c);
    if (o == null) {
      return null;
    }
    String s = o.toString();
    if(CtuluLibString.isEmpty(s)) return null;
    try {
      return new Number(_colXls, _rowXls, Double.parseDouble(s));
    } catch (final NumberFormatException e) {}
    return new Label(_colXls, _rowXls, s);

    // return cell;
  }

	
}
