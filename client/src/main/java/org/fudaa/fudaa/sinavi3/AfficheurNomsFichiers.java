/*
 * @file         AfficheurNomsFichiers.java
 * @creation     1999-10-01
 * @modification $Date: 2007-11-23 11:28:02 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.sinavi3;

import java.awt.Component;
import java.io.File;

import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;

import org.fudaa.ctulu.CtuluLibString;

/**
 * Afficheur sp�cifique pour les noms des fichiers dans les cellules d'une liste. Il retire le chemin d'acc�s et
 * l'extension s'ils existent.
 * 
 * @version $Revision: 1.1 $ $Date: 2007-11-23 11:28:02 $ by $Author: hadouxad $
 * @author Nicolas Chevalier , Bertrand Audinet
 */
public class AfficheurNomsFichiers implements ListCellRenderer {
  public Component getListCellRendererComponent(final JList _list, final Object _value, final int _index,
      final boolean _isSelected, final boolean _cellHasFocus) {
    String nomFichier = (String) _value;
    int position = nomFichier.lastIndexOf(File.separator) + 1;
    if (position != -1) {
      nomFichier = nomFichier.substring(position);
    }
    position = nomFichier.lastIndexOf(CtuluLibString.DOT);
    if (position != -1) {
      nomFichier = nomFichier.substring(0, position);
    }
    final JLabel label = new JLabel(nomFichier);
    label.setBackground(_list.getBackground());
    label.setOpaque(true);
    if (_isSelected) {
      label.setBackground(_list.getSelectionBackground());
    }
    return label;
  }
}
