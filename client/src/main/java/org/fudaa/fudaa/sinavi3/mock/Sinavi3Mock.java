package org.fudaa.fudaa.sinavi3.mock;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Random;

import org.fudaa.dodico.fortran.FortranWriter;
import org.fudaa.fudaa.sinavi3.Sinavi3Bateau;
import org.fudaa.fudaa.sinavi3.Sinavi3Bief;
import org.fudaa.fudaa.sinavi3.Sinavi3DataSimulation;
import org.fudaa.fudaa.sinavi3.Sinavi3Ecluse;

/**
 * Simulate action done by the fortran brain. 
 * @author Adrien
 *
 */
public class Sinavi3Mock {

	public static int NB_RECORD_OCCUP_TO_GENERATE = 100;
	public static int NB_RECORD_BATEAUX_TO_GENERATE = 1000;
	public static int NB_EVENT_CONSO_EAU = 3000;
	public void mockGenerationBateaux(String path, List<Sinavi3Bateau> listeBateaux) {
		try {
			feedBateaux(  path,300,listeBateaux);
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
        
        
	public void mockGeneral(final Sinavi3DataSimulation data, final String pathSimu) {
		
		try {
			feedDataIfNeeded(data);
			feedFileHis( data, pathSimu,NB_RECORD_BATEAUX_TO_GENERATE, data.getListeBateaux_().getListeNavires_());
			feedFileOccup(pathSimu, NB_RECORD_OCCUP_TO_GENERATE,data);
			feedFileConso(pathSimu, NB_EVENT_CONSO_EAU);
			//feedFileConsoEau(pathSimu, 2);
                        if(data.getParams_() == null || data.getParams_().ecluses == null) {
                             data.setParametresProjet();
                        }
			
			
		} catch (IOException e) {
			
			e.printStackTrace();
		}
		
	}
	
	private void feedDataIfNeeded(Sinavi3DataSimulation data) {
		if(data.getListeGare_().getListeGares_().size() <1) {
			data.getListeGare_().ajout("Gare 1");
			data.getListeGare_().ajout("Gare 2");
			data.getListeGare_().ajout("Gare 3");
			data.getListeGare_().ajout("Gare 4");
		}
		if(data.getListeEcluse().getListeEcluses().size() <1) {			
			Sinavi3Ecluse ecluse = new Sinavi3Ecluse();
			ecluse.setGareAmont_(0);
			ecluse.setGareAmont_(1);
			data.getListeEcluse().ajout(ecluse );
			ecluse = new Sinavi3Ecluse();
			ecluse.setGareAmont_(1);
			ecluse.setGareAmont_(2);
			data.getListeEcluse().ajout(ecluse );
		}
               
                if(data.getListeBateaux_().NombreNavires() <1) {
                    Sinavi3Bateau bateau = new Sinavi3Bateau();
                    bateau.setTirantEau(15.0d);
                     bateau.setLargeurMax(5.0d);
                    bateau.setLongueurMax(10.0d);
                    data.getListeBateaux_().ajout(bateau);
                    bateau = new Sinavi3Bateau();
                    bateau.setTirantEau(10.0d);
                     bateau.setLargeurMax(2.0d);
                    bateau.setLongueurMax(5.0d);
                    data.getListeBateaux_().ajout(bateau);
                    
                }
                 if(data.getListeBief_().getListeBiefs_().size() <3) {			
			Sinavi3Bief bief = new Sinavi3Bief(data.getListeBateaux_().NombreNavires());
                        bief.setGareAmont_(0);
                        bief.setGareAval_(2);
                        data.getListeBief_().ajout(bief);
                        bief = new Sinavi3Bief(data.getListeBateaux_().NombreNavires());
                        bief.setGareAmont_(1);
                        bief.setGareAval_(3);
                        data.getListeBief_().ajout(bief);
                        bief = new Sinavi3Bief(data.getListeBateaux_().NombreNavires());
                        bief.setGareAmont_(2);
                        bief.setGareAval_(3);
                        data.getListeBief_().ajout(bief);
                }      
		
	}

	public void feedBateaux(String pathSimu, int nbbateaux,List<Sinavi3Bateau> listeBateaux ) throws IOException {
		PrintWriter writer = new PrintWriter(new File(pathSimu + ".arriv"));
		FortranWriter fortranwriter = new FortranWriter(writer);
		for(int i=0;i<nbbateaux;i++) {			
			fortranwriter.writeln(" 1 2 5 A 1 0 2 ");
		}
		fortranwriter.close();
	}
	
//	public void feedFileConsoEau(String pathSimu, int nbEcluses) throws IOException {
//		PrintWriter writer = new PrintWriter(new File(pathSimu + ".consoEau"));
//		FortranWriter fortranwriter = new FortranWriter(writer);
//		for(int i=0;i<nbEcluses;i++) {
//			fortranwriter.writeln((float)((i+1)*25.6)+"      coefficient d'�pargne du bassin pour l'ecluse "+i);
//		}
//		fortranwriter.close();
//	}
	
	public void feedFileOccup(String pathSimu, int nbData,final Sinavi3DataSimulation data) throws IOException {
		PrintWriter writer = new PrintWriter(new File(pathSimu + ".occup"));
		FortranWriter fortranwriter = new FortranWriter(writer);
		for(int i=0;i<nbData;i++) {
			
			int nbEcluses = data.getListeEcluse().getListeEcluses().size();
			
			int ecluse = Math.min((int)(Math.random()*nbEcluses)+1,nbEcluses);
			double moment = Math.random() *60;
			double longueurCumulee =Math.random()*10;
			if(data.getListeEcluse().getListeEcluses().size()<ecluse-1)
				ecluse =1;
			Sinavi3Ecluse ecluseD = data.getListeEcluse().retournerEcluse(ecluse-1);
			int gare = Math.random()>0.5?(ecluseD.getGareAmont_()+1):(ecluseD.getGareAval_()+1);
			
			String apparitionBateaux="";
			for(int k=0;k<data.getListeBateaux_().NombreNavires();k++)
				apparitionBateaux = apparitionBateaux + " " + ((int)(Math.random()*10.0));
			
			fortranwriter.writeln("G " + gare + " E "+ecluse+" "+moment+" "+ (Math.random()>0.5?"A":"M")+ " " +longueurCumulee+ apparitionBateaux);
			fortranwriter.writeln("G " + gare + " E "+ecluse+" "+moment+" "+ (Math.random()>0.5?"A":"M")+ " "+longueurCumulee+ apparitionBateaux);
		}
		
		//-- feed pour une ecluse --//
		
		fortranwriter.close();
	}
	
	public void feedFileConso(String pathSimu, int nbData) throws IOException {
		PrintWriter writer = new PrintWriter(new File(pathSimu + ".conso"));
		FortranWriter fortranwriter = new FortranWriter(writer);
                Random random = new Random();
		for(int i=0;i<nbData;i++) {
                     int indiceEcluse = random.nextInt(50)>25?1:2;
                     String fausse = random.nextInt(50)>25?"F":"N";
                     String avalant = random.nextInt(50)>25?"A":"M";
                     //-- on table sur 300 jours  a exprimer en secondes --//
                     int timeEvent = random.nextInt(365)*24*60*60;
			fortranwriter.writeln("toto " + indiceEcluse + " "+fausse+" "+timeEvent+" "+avalant+" ");
		}
		fortranwriter.close();
	}
	
	
	private void feedFileHis(Sinavi3DataSimulation data, String pathSimu , int nbBateaux, List<Sinavi3Bateau> listeBateau) throws IOException {
			PrintWriter writer = new PrintWriter(new File(pathSimu +".his"));
			FortranWriter fortranwriter = new FortranWriter(writer);
                          Random random = new Random();
			float heureEntree = 1*24*60*60;
			float heureSortie = 1*24*60*60+2000;
			for(int i=0;i<nbBateaux;i++) {
				
				int random1or2= Math.random()>0.6f?1:2; 
				if(random1or2 > listeBateau.size())
					random1or2 = 1;
                                  String sens  = random.nextInt(50)>25?"A":"M";
				fortranwriter.writeln((i+1)+"  "+ random1or2 +"  "+sens+"   10");
				//-- write 5 trajets --//
				 for(int j=0;j<5;j++) {
					 
                                   
                                         int acces =random.nextInt(50);
                                        int secu =random.nextInt(50);
                                        int occup =random.nextInt(50);
                                        int indispo =random.nextInt(50);
                                        int indiceTroncon = random.nextInt(3);
                                        if(indiceTroncon == 3)
                                            indiceTroncon = 2;
					 fortranwriter.writeln("T  "+ (indiceTroncon+1) +" " + heureEntree + "  " + heureSortie + " "+acces+" "+secu+" "+occup+" "+indispo+" ");
					 heureEntree = heureSortie + 60* random.nextInt(6); //heure entr�e = heure sortie + quelques minutes
					 heureSortie = heureEntree + 60* random.nextInt(300);// heure entr�e + maximum 300 minutes
                                         //--passage ecluse --//
                                          acces =random.nextInt(50);
                                         secu =random.nextInt(50);
                                         occup =random.nextInt(50);
                                         indispo =random.nextInt(50);
                                         int indiceEcluse = random.nextInt(50)>25?1:2;
                                         
                                         fortranwriter.writeln("E  "+indiceEcluse+"  " + heureEntree + "  " + heureSortie + " "+acces+" "+secu+" "+occup+" "+indispo+" ");
					
                                         heureEntree += 0.2f;
					 heureSortie += 0.2f;
				 }
			}
			fortranwriter.close();
	}
	
	
}
