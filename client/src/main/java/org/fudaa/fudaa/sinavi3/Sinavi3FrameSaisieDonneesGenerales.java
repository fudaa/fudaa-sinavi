/**
 *@creation 10 oct. 06
 *@modification $Dates$
 *@license  GNU General Public Licence 2
 *@copyright   (c)1998-2006 CETMEF 2 bd Gambetta F -60231 Compiegne
 *@mail   devel@fudaa.fr 
 */

package org.fudaa.fudaa.sinavi3;

/**
 * 
 */

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.util.ArrayList;
import java.util.Random;

import javax.swing.BorderFactory;
import javax.swing.JComboBox;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;

import org.fudaa.fudaa.ressource.FudaaResource;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuDialogError;
import com.memoire.bu.BuDialogMessage;

/**
 * Panel de saisie des donn�es generales: on saisie par al meme occasion les jours f�ri�s repr�sent�s sous forme d'un
 * tableau ainsi que les don�nes de base pour la simulation
 * 
 * @author Adrien Hadoux
 */
public class Sinavi3FrameSaisieDonneesGenerales extends Sinavi3InternalFrame {

  Random generateur_ = new Random();

  /**
   * Descriptif des elements des colonnes
   */
  String[] titreColonnes = { "Numero du Jour ferie" };

  /**
   * Tableau de type JTable qui contiendra les donn�es des bassins
   */

  JTable tableau;

  /**
   * definition des composants
   */
  Sinavi3TextFieldInteger nbJours_ = new Sinavi3TextFieldInteger(4);
  Sinavi3TextFieldInteger piedPilote_ = new Sinavi3TextFieldInteger(4);
  Sinavi3TextFieldInteger graine_ = new Sinavi3TextFieldInteger(8);
  String[] joursSemaine = { "lundi", "mardi", "mercredi", "jeudi", "vendredi", "samedi", "dimanche" };
  JComboBox jourDepart = new JComboBox(joursSemaine);

  /**
   * Bouton de validation des donn�es topolgiques saisies pour le chenal
   */
  final BuButton validation_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_oui"), "Valider");

  /**
   * Fenetre qui contiendra le panel de jours feri�s a droite0
   */
  JPanel panelJoursFeries = new JPanel();

  /**
   * panel contenant les principaux controles:
   */
  JPanel panelDonnees_ = new JPanel();

  /**
   * Bordure du tableau
   */

  Border borduretab = BorderFactory.createLoweredBevelBorder();
  Border loweredBevel_ = BorderFactory.createLoweredBevelBorder();
  Border raisedBevel_ = BorderFactory.createRaisedBevelBorder();
  Border bordnormal_ = BorderFactory.createEtchedBorder();
  Border compound_ = BorderFactory.createCompoundBorder(raisedBevel_, loweredBevel_);

  /**
   * donn�es de la loi deterministe
   */
  ArrayList listeJoursFeries_ = new ArrayList();

  /**
   * donnees de la simulation
   */
  Sinavi3DataSimulation donnees_;

  /**
   * constructeur du panel d'affichage des bassins
   * 
   * @param d donn�es de la simulation
   */
  Sinavi3FrameSaisieDonneesGenerales(final Sinavi3DataSimulation _donnees) {
    super("", true, true, true, true);
    // recuperation des donn�es de la simulation

    donnees_ = _donnees;
    panelJoursFeries.setLayout(new BorderLayout());

    this.nbJours_.setToolTipText("Le nombre de jours de la simulation. Par d�faut r�gl� sur 365 jours");
    this.piedPilote_
        .setToolTipText("le piede de pilote en pourcentage. Par defaut regl� a 10%. ne saisir s'un nombre entre 0 et 100 sans particules");
    this.jourDepart.setToolTipText("le jour de d�part de la simulation. La simulation commencera le jour selectionn�");
    // initialisation des donn�es
    this.initialisation();
    // afichage des elements dans le tableau.
    this.affichage();

   
    this.piedPilote_.addFocusListener(new FocusAdapter() {
      public void focusGained(final FocusEvent e) {
        piedPilote_.selectAll();
      }

      public void focusLost(final FocusEvent e) {
        // test de validite
        if (!piedPilote_.getText().equals("")) {
          try {
            final float i = Float.parseFloat(piedPilote_.getText());
             if (i > 100) {
              new BuDialogError(donnees_.application_.getApp(), Sinavi3Implementation.isSinavi_,
                  "Erreur!!  pied de pilote doit etre inferieur a 100% \n il faut entrer un pourcentage sans le %!!!!")
                  .activate();
              piedPilote_.setText("");

            }
          } catch (final NumberFormatException nfe) {
           
          }
        }
      }
    });

  
    /*******************************************************************************************************************
     * listener du bouton de validation
     */

    this.validation_.addActionListener(new ActionListener() {

      public void actionPerformed(final ActionEvent e) {

        // deselectionne toutes les selections du tableau
        tableau.clearSelection();
        tableau.editCellAt(299, 1);
        tableau.editingCanceled(null);
        miseAjourSaisieDonneesGenerales();

      }

    });

    /*******************************************************************************************************************
     * Creation de la fenetre
     */

    setTitle("Saisie des donn�es g�n�rales");
    setSize(360, 300);
    setBorder(Sinavi3Bordures.compound_);
    getContentPane().setLayout(new BorderLayout());

    final JScrollPane ascenceur = new JScrollPane(panelJoursFeries);

    // getContentPane().add(ascenceur,BorderLayout.EAST);

    final JPanel controlPanel = new JPanel();
    controlPanel.add(validation_);
    controlPanel.setBorder(Sinavi3Bordures.compound_);
    getContentPane().add(controlPanel, BorderLayout.SOUTH);

    this.panelDonnees_.setLayout(new GridLayout(4, 1));

    final JPanel p0 = new JPanel();
    p0.add(new JLabel("Graine d'initialisation: "));
    p0.add(this.graine_);
    p0.setBorder(Sinavi3Bordures.compound_);
    this.panelDonnees_.add(p0);

    final JPanel p1 = new JPanel();
    p1.add(new JLabel("Dur�e de la simulation: "));
    p1.add(this.nbJours_);
    p1.add(new JLabel("jours  "));
    p1.setBorder(Sinavi3Bordures.compound_);
    this.panelDonnees_.add(p1);

    final JPanel p2 = new JPanel();
    p2.add(new JLabel("Jour de d�part de la simulation: "));
    p2.add(this.jourDepart);
    p2.setBorder(Sinavi3Bordures.compound_);
 //   this.panelDonnees_.add(p2);

    final JPanel p3 = new JPanel();
    p3.add(new JLabel("Pourcentage de pied de Pilote: "));
    p3.add(this.piedPilote_);
    p3.add(new JLabel("%"));
    p3.setBorder(Sinavi3Bordures.compound_);
//    this.panelDonnees_.add(p3);

    final TitledBorder bordure1 = BorderFactory.createTitledBorder(Sinavi3Bordures.compound_, "jours f�ri�s");
    ascenceur.setBorder(bordure1);

    final TitledBorder bordure2 = BorderFactory.createTitledBorder(Sinavi3Bordures.compound_, "G�n�ralit�s");
    this.panelDonnees_.setBorder(bordure2);

    final JPanel definitif = new JPanel();
    definitif.setLayout(new GridLayout(1, 1));
    definitif.add(this.panelDonnees_);
    //definitif.add(ascenceur);
    getContentPane().add(definitif, BorderLayout.CENTER);

    // affichage de la frame
    setVisible(true);

  }

  /**
   * Methode de remplissage des JComboBox et des donn�es par d�fauts pour chaque objet.
   */
  void initialisation() {

    // remplissage des donn�es de la simulation:
    if (donnees_.params_.donneesGenerales.graine != 0) {
      this.graine_.setText("" + this.donnees_.params_.donneesGenerales.graine);
    } else {
      this.graine_.setText("" + this.generateur_.nextInt());
    }
    if (donnees_.params_.donneesGenerales.nombreJours != 0) {
      this.nbJours_.setText("" + donnees_.params_.donneesGenerales.nombreJours);
    } else {
      this.nbJours_.setText("365");
    }

    if (donnees_.params_.donneesGenerales.piedDePilote != 0) {
      this.piedPilote_.setText("" + donnees_.params_.donneesGenerales.piedDePilote);
    } else {
      this.piedPilote_.setText("0");
    }

    if (donnees_.params_.donneesGenerales.jourDepart != 0) {
      this.jourDepart.setSelectedIndex(donnees_.params_.donneesGenerales.jourDepart - 1);
    }

  }

  /**
   * Methode d affichage des composants du jtable et du tableau de combo Cette methode est a impl�menter dans les
   * classes d�riv�es pour chaque composants
   */
  void affichage() {

    final Object[][] ndata = new Object[300][1];

    System.out.println("nb jours ***: " + listeJoursFeries_.size());

    // affichage par defaut des dernieres couples saisis:
    for (int i = 0; i < this.donnees_.params_.donneesGenerales.nombreJoursFeries; i++) {

      ndata[i][0] = "" + donnees_.params_.donneesGenerales.tableauJoursFeries[i];

    }

    this.tableau = new JTable(ndata, this.titreColonnes) {
      /*
       * public boolean isCellEditable(int row,int col ){ return false; }
       */
    };

    tableau.setBorder(this.borduretab);

    // tableau.revalidate();
    // this.removeAll();
    this.panelJoursFeries.add(/* ascenceur */tableau.getTableHeader(), BorderLayout.PAGE_START);
    this.panelJoursFeries.add(tableau, BorderLayout.CENTER);

    final JPanel controlePanel = new JPanel();
    controlePanel.add(validation_);

    panelJoursFeries.add(controlePanel, BorderLayout.SOUTH);
    this.revalidate();
    this.updateUI();

  }

  /**
   * Methode qui permet de verifier la pertinence des donn�es saisies: IE: v�rifie que les gares choisies en amont et
   * avales sont bien diff�rentes pour un m�me chenal
   * 
   * @return true si les donn�es sont coh�rentes sinon retourne false et surtout indique a quel endroit se situe
   *         l'erreur de logique de la saisie
   */
  boolean verificationCoherence() {

    /**
     * **************************************************************************** VERIFICATION DONNEES des zones de
     * texte ****************************************************************************
     */

    if (this.graine_.getText().equals("")) {
      new BuDialogError(donnees_.application_.getApp(), Sinavi3Implementation.isSinavi_,
          "Erreur!! Graine d'initialisation manquante!!!!").activate();
      return false;
    }
    if (this.nbJours_.getText().equals("")) {
      new BuDialogError(donnees_.application_.getApp(), Sinavi3Implementation.isSinavi_,
          "Erreur!! Nombre de jours de la simulation manquant!!!!").activate();
      return false;
    }
    if (this.piedPilote_.getText().equals("")) {
      new BuDialogError(donnees_.application_.getApp(), Sinavi3Implementation.isSinavi_,
          "Erreur!! Pied de pilote de la simulation manquant!!!!").activate();
      return false;
    }

    /**
     * **************************************************************************** VERIFICATION DONNEES DU TABLEAU
     * ****************************************************************************
     */

    String jour = "";

    for (int i = 0; i < 300; i++) {
      if (this.tableau.getModel().getValueAt(i, 0) != null) {
        jour = (String) this.tableau.getModel().getValueAt(i, 0);
      } else {
        jour = "";
      }

      if (!jour.equals(""))

      {
        // test si le nombre entr�e est bien un nombre r�el:
        try {
          final int nj = Integer.parseInt(jour);

          if (nj < 0) {
            new BuDialogError(donnees_.application_.getApp(), Sinavi3Implementation.isSinavi_, "Erreur!! le jour ligne "
                + i + " est negatif!!!!").activate();
            return false;
          } else if (nj > Integer.parseInt(this.nbJours_.getText())) {
            new BuDialogError(donnees_.application_.getApp(), Sinavi3Implementation.isSinavi_, "Erreur!! le jour ligne "
                + i + " est superieur au \nnombre total de jours de simulation " + this.nbJours_.getText() + "!!!!")
                .activate();
            return false;
          }

          else {
            System.out.println("donnees saisies: jour " + nj);
            // on ajoute al donn�e au vecteur ou on modifie selon le cas
            if (i >= this.listeJoursFeries_.size()) {
              this.listeJoursFeries_.add("" + nj);
            } else {
              this.listeJoursFeries_.set(i, "" + nj);
            }

          }
        } catch (final NumberFormatException nfe) {
          new BuDialogError(donnees_.application_.getApp(), Sinavi3Implementation.isSinavi_, "Erreur!!  le jour  ligne "
              + i + " saisi n'existe pas!!!!").activate();
          return false;
        }

      }// if les mots sont non vides
      else {
        new BuDialogMessage(donnees_.application_.getApp(), Sinavi3Implementation.isSinavi_,
            "Les donn�es ont �t� correctement saisies."/* + i + " jours saisis!! "*/).activate();
        dispose();
        return true;

      }
    }

    // arriv� � ce stade de la m�thode , tous les test de non coh�rence ont �chou�, il suit que
    // les donn�es saisies sont bien coh�rentes et l'on peut les ajouter aux cheneaux.
    new BuDialogMessage(donnees_.application_.getApp(), Sinavi3Implementation.isSinavi_,
        "Les donn�es ont �t� correctement saisies: " /*+ this.listeJoursFeries_.size() + " couples saisis!! "*/).activate();
    dispose();
    return true;
  }

  /**
   * Methode qui permet de mettre a jour les gares saiies en amont et vaales pour chacun des cheneaux: v�rifie dans un
   * premier temps l coh�rence des donn�es:
   */

  void miseAjourSaisieDonneesGenerales() {

    /**
     * coherence des donn�es sdu tableau
     */
    if (this.verificationCoherence()) {

      // recuperation des donn�es de la simulation:
      this.donnees_.params_.donneesGenerales.graine = Integer.parseInt(this.graine_.getText());
      this.donnees_.params_.donneesGenerales.jourDepart = this.jourDepart.getSelectedIndex() + 1;
      this.donnees_.params_.donneesGenerales.nombreJours = Integer.parseInt(this.nbJours_.getText());
      this.donnees_.params_.donneesGenerales.piedDePilote = Integer.parseInt(this.piedPilote_.getText());

      // 0) on alloue de la memoire pour le tableau de jours feries
      this.donnees_.params_.donneesGenerales.tableauJoursFeries = new int[this.listeJoursFeries_.size()];
      // 1) on recopie le vecteur rempli dans la methode coherence

      for (int i = 0; i < this.listeJoursFeries_.size(); i++) {
        this.donnees_.params_.donneesGenerales.tableauJoursFeries[i] = Integer.parseInt((String) this.listeJoursFeries_
            .get(i));
      }
      // 2) on recopie le nombre de jours f�ri�s:
      this.donnees_.params_.donneesGenerales.nombreJoursFeries = this.listeJoursFeries_.size();
    }

  }

}
