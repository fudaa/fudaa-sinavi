/**
 *@creation 10 oct. 06
 *@modification $Dates$
 *@license  GNU General Public Licence 2
 *@copyright   (c)1998-2006 CETMEF 2 bd Gambetta F -60231 Compiegne
 *@mail   devel@fudaa.fr 
 */
package org.fudaa.fudaa.sinavi3;

/**
 * FIXME FRED a quoi sert cette classe
 * 
 * @version $Id: Sinavi3DonneesGenerales.java,v 1.1 2007-11-23 11:27:53 hadouxad Exp $
 * @author hadoux
 */
public class Sinavi3DonneesGenerales {

  /**
   * nombre de jours de al simulation par defaut ce nombre est 365
   */
  int nbJours_ = 365;

  /**
   * FIXME FRED Bon courage !
   * definition des jours feries.
   */
  int joursFeries_[] = new int[365];

  /**
   * nombre de jours feries
   */
  int nbJoursFeries_ = 0;

  /**
   * entier qui correspond au jour de depart 1=> lundi 2=> mardi ... 7=> dimanche par defaut 1=> lundi
   */
  int jourDepart = 1;

  /**
   * pourcentage du pied de pilote par defaut ce pourcentage est de 10%
   */
  int piedDePilote = 10;

}
