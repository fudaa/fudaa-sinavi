/*
 * @file         Sinavi3Implementation.java
 * @creation     1999-10-01
 * @modification $Date: 2008-01-25 14:04:25 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.sinavi3;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;

import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.dodico.corba.objet.IConnexion;
import org.fudaa.dodico.corba.sinavi3.*;
import org.fudaa.dodico.objet.CExec;
import org.fudaa.dodico.sinavi3.*;
import org.fudaa.ebli.network.simulationNetwork.NetworkGraphModel;
import org.fudaa.ebli.network.simulationNetwork.ui.EditorMenuBar;
import org.fudaa.fudaa.commun.FudaaLib;
import org.fudaa.fudaa.commun.dodico.FudaaDodicoTacheConnexion;
import org.fudaa.fudaa.commun.impl.FudaaCommonImplementation;
import org.fudaa.fudaa.commun.impl.FudaaStartupExitPreferencesPanel;
import org.fudaa.fudaa.commun.projet.FudaaProjet;
import org.fudaa.fudaa.ressource.FudaaResource;
import org.fudaa.fudaa.sinavi3.Sinavi3Resource;
import org.fudaa.fudaa.sinavi3.mock.Sinavi3Mock;
import org.fudaa.fudaa.sinavi3.sinavi3Network.Sinavi3NetworkDaoFactory;
import org.fudaa.fudaa.sinavi3.sinavi3Network.SinaviNetwork;
import org.fudaa.fudaa.sinavi3.sinavi3Network.SinaviSyncrhonizeNetworkAction;
import org.fudaa.fudaa.sinavi3.ui.panel.SinaviPanelDureeManeuvresEclusesTreeView;
import org.fudaa.fudaa.sinavi3.ui.panel.SinaviPanelDureeManeuvresEcluses;
import org.fudaa.fudaa.sinavi3.ui.panel.SinaviPanelRemplissageSAS;
import org.fudaa.fudaa.sinavi3.ui.results.Sinavi3ResultatsConsommationEau;
import org.fudaa.fudaa.sinavi3.ui.results.Sinavi3ResultatsGarage;
import org.fudaa.fudaa.utilitaire.ServeurCopieEcran;

import com.hexidec.ekit.component.JButtonNoFocus;
import com.memoire.bu.BuActionRemover;
import com.memoire.bu.BuAssistant;
import com.memoire.bu.BuBrowserFrame;
import com.memoire.bu.BuBrowserPreferencesPanel;
import com.memoire.bu.BuButton;
import com.memoire.bu.BuDesktopPreferencesPanel;
import com.memoire.bu.BuDialogConfirmation;
import com.memoire.bu.BuDialogError;
import com.memoire.bu.BuDialogMessage;
import com.memoire.bu.BuHelpFrame;
import com.memoire.bu.BuInformationsDocument;
import com.memoire.bu.BuInformationsSoftware;
import com.memoire.bu.BuInternalFrame;
import com.memoire.bu.BuLanguagePreferencesPanel;
import com.memoire.bu.BuLookPreferencesPanel;
import com.memoire.bu.BuMainPanel;
import com.memoire.bu.BuMenu;
import com.memoire.bu.BuMenuBar;
import com.memoire.bu.BuMenuItem;
import com.memoire.bu.BuMenuRecentFiles;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuPreferences;
import com.memoire.bu.BuPrinter;
import com.memoire.bu.BuResource;
import com.memoire.bu.BuScrollPane;
import com.memoire.bu.BuSplashScreen;
import com.memoire.bu.BuTaskOperation;
import com.memoire.bu.BuTaskView;
import com.memoire.bu.BuToolBar;
import com.memoire.fu.FuLib;
import com.memoire.fu.FuLog;


/**
 * L'implementation du client Sinavi.
 * 
 * @version $Revision: 1.11 $ $Date: 2008-01-25 14:04:25 $ by $Author: hadouxad $
 * @author Adrien Hadoux
 */
public class Sinavi3Implementation extends FudaaCommonImplementation {
  // public final static String LOCAL_UPDATE= ".";
  public final static boolean IS_APPLICATION = true;
  public static ICalculSinavi3 SERVEUR_SINAVI;
  public static IConnexion CONNEXION_SINAVI;
  /**
   * Pointe vers parametres() de SERVEUR_SIPOR.
   */
  /**
   * Pointe vers resultats() de SERVEUR_SIPOR.
   */
  //protected IResultatsSipor siporResults_;
  public static final BuInformationsSoftware isSinavi_ = new BuInformationsSoftware();
  protected static final BuInformationsDocument idSinavi_ = new BuInformationsDocument();
  public static  boolean MOCK_ENABLE = false;
  // Concerne les donn�es
  protected Hashtable<String, FudaaProjet> projets_; // Contient les projets ouverts
  protected FudaaProjet projet_; // Projet en cours
  protected Sinavi3OutilsDonnees outils_;

  //-- Definition des couleurs --//
  public static Color bleuSinavi=new Color(54,138,191);
  public static Color bleuClairSinavi=new Color(132,166,188);
  /**
   * Les donn�es de la simulation version 2007
   */
  Sinavi3DataSimulation donnees_ = new Sinavi3DataSimulation(this);


  /**
   * generation du reseau.
   */
 SinaviNetwork networkEditor = new SinaviNetwork(new NetworkGraphModel( new Sinavi3NetworkDaoFactory(donnees_)), true);
 
  
  /**
   * Fenetre de saisie des donn�es du logiciel:
   */

  /**
   * Fenetre de gestion des cat�gories
   */
  Sinavi3VisualiserBateaux gestionNavires_ ;
  /**
   * Fenetre de gestion des tron�ons
   */
  Sinavi3VisualiserBief gestionChenaux_ ;
   /**
   * Fenetre de gestion des ecluses
   */
  Sinavi3VisualiserGares gestionGares_ ;
  
  /**
   * Fenetre de gestion des ecluses
   */
  Sinavi3VisualiserEcluses gestionEcluses_ ;

   /**
   * fenetre de gstion des donn�es g�n�rales
   */
  Sinavi3FrameSaisieDonneesGenerales gestionDonneesGenerales_ ;

  /**
   * fenetre de gestion des topologies
   */

  
  /**
   * fenetre de gestion des ecluses
   */
  Sinavi3PanelTopologieEcluse gestionTopoEcluses_ ;
  /**
   * fenetre de gestion des cheanux
   */
  Sinavi3PanelTopologieBief gestionTopoChenaux_ ;
   /**
   * fenetre de modelisation du port
   */
  Sinavi3DessinerPortFrame gestionModelisation_ ;

  /**
   * fenetre des regels et durees de parcours
   */

  /**
   * fenetre de gestion des regles de navigations pour les tron�ons
   */
  Sinavi3PanelReglesCroisementBief gestionCroisementBief_ ;
  
  /**
   * fenetre de gestion des regles trematage pour les tron�ons
   */
  Sinavi3PanelReglestrematageBief gestionTrematageBief_ ;
  
  /**
   * fenetre des vitesses dans les tron�ons
   */
  Sinavi3PanelVitessesBief gestionVitessesBief_ ;
  
  /**
   * fenetre des dur�es maneuvres dans ecluses
   */
  Sinavi3InternalFrame gestionDureeManeuvreEcluse_ ;
  
  /**
   * fenetre des gestion de trajets
   */
  Sinavi3PanelGestionTrajetsBateaux gestionTrajetsBateaux_;
  
  
  public SinaviPanelRemplissageSAS gestionRemplissageSAS_;
  
  /**
   * Fenetre qui gere le rappel des donn�es.
   */
  Sinavi3FrameGenerationRappelDonnees rappelDonnees_ ;

  // Outils pour acc�der aux donn�es de projet_
  // Concerne les fenetres

  protected BuBrowserFrame fRappelDonnees_;
  protected BuBrowserFrame fStatistiquesFinales_;

  protected BuAssistant assistant_;
  protected BuHelpFrame aide_;
  protected BuTaskView taches_;
  protected Sinavi3ListeSimulations liste_;
  protected BuInternalFrame fGraphiques_;
  static {

	  isSinavi_.banner = Sinavi3Resource.SINAVI.getIcon("/org/fudaa/fudaa/sinavi3/sinavi2pres3");	    
	    if( isSinavi_.banner == null)
	    	isSinavi_.banner = Sinavi3Resource.SINAVI.getIcon("/resources/org/fudaa/fudaa/sinavi3/sinavi2pres3");
	 isSinavi_.logo = Sinavi3Resource.SINAVI.getIcon("/org/fudaa/fudaa/sinavi3/sinavi2logo");
	 if( isSinavi_.logo == null)
	    	isSinavi_.logo = Sinavi3Resource.SINAVI.getIcon("/resources/org/fudaa/fudaa/sinavi3/sinavi2logo");
	    	    

    if( isSinavi_.banner == null)
    	isSinavi_.banner = Sinavi3Resource.SINAVI.getIcon("/resources/org/fudaa/fudaa/sinavi3/sinavi2pres3");
    	
    isSinavi_.logo = Sinavi3Resource.SINAVI.getIcon("/org/fudaa/fudaa/sinavi3/sinavi2logo");
    
    if( isSinavi_.logo == null)
    	isSinavi_.logo = Sinavi3Resource.SINAVI.getIcon("/resources/org/fudaa/fudaa/sinavi3/sinavi2logo");
    	   
    
    isSinavi_.name = "Sinavi";
    isSinavi_.version = "3.3.5";
    isSinavi_.date = "04-01-2016";
    isSinavi_.rights = "Tous droits r�serv�s. CETMEF (c)1999,2006";
    isSinavi_.contact = "Fudaa-Sinavi.CETMEF@developpement-durable.gouv.fr";
    isSinavi_.license = "GPL2";
    isSinavi_.languages = "fr,en";
    isSinavi_.http = "http://www.cetmef.developpement-durable.gouv.fr/";
    isSinavi_.update = "http://marina.cetmef.equipement.gouv.fr/fudaa/deltas/";
    isSinavi_.man = FudaaLib.LOCAL_MAN;
    isSinavi_.authors = new String[] { "M�d�ric Fargeix", "Adrien Hadoux" };
    isSinavi_.contributors = new String[] { "Equipes Dodico, Ebli et Fudaa" };
    isSinavi_.documentors = new String[] { "Alain Chambreuil" };
    isSinavi_.testers = new String[] { "Alain Pourplanche", "Alain Chambreuil", "Nicolas Clavreul" };
    idSinavi_.name = "Etude";
    idSinavi_.version = "0.02";
    idSinavi_.organization = "CETMEF";
    idSinavi_.author = "Equipe Fudaa-Sinavi";
    idSinavi_.contact = "devel@fudaa.org";
    idSinavi_.date = FuLib.date();
    BuPrinter.INFO_LOG = isSinavi_;
    BuPrinter.INFO_DOC = idSinavi_;
  }

  
 
  protected BuButton supprimerSimu_= new BuButton(FudaaResource.FUDAA.getIcon("crystal_enlever"), "Enlever");
  protected BuButton ajouterSimu_= new BuButton(FudaaResource.FUDAA.getIcon("crystal_ajouter"), "Ajouter");
  
  
  
  
  public BuInformationsSoftware getInformationsSoftware() {
    return isSinavi_;
  }

  public static BuInformationsSoftware informationsSoftware() {
    return isSinavi_;
  }

  public BuInformationsDocument getInformationsDocument() {
    return idSinavi_;
  }

  public void init() {
    super.init();
    
    setVersionNoyauCalcul();
    
    isSinavi_.version +="\nNoyau: " + this.versionNoyauCalcul+"\n";
    
    final BuSplashScreen ss = getSplashScreen();

    projet_ = null;
    aide_ = null;
    try {

      setTitle(getInformationsSoftware().name + CtuluLibString.ESPACE + getInformationsSoftware().version);
      // FD debut
      // BuMenuBar mb = BuMenuBar.buildBasicMenuBar();
      // setMainMenuBar(mb);
      // mb.addActionListener(this);
      if (ss != null) {
        ss.setText("Menus et barres d'outils compl�t�s par Sinavi");
        ss.setProgression(50);
      }
      final BuMenuBar mb = getMainMenuBar();
      // FD fin

      /**
       * Ajout des menubar
       */
      mb.addMenu(construitMenuDonneesGenerales(IS_APPLICATION));
      mb.addMenu(construitMenuParametresConstruction(IS_APPLICATION));
      mb.addMenu(construitMenuTopologie(IS_APPLICATION));
      mb.addMenu(construitMenuReglesNavigations(IS_APPLICATION));
      mb.addMenu(construitMenuGeneration(IS_APPLICATION));
      mb.addMenu(construitMenuSimulation(IS_APPLICATION));
      mb.addMenu(construitMenuExploitation(IS_APPLICATION));
      mb.addMenu(construitMenuComparaisonSimulations(IS_APPLICATION));
     
      final BuToolBar tb = getMainToolBar();

      construitToolBar(tb);

      // this.setMainToolBar(newtb);
      /*
       * tb.addToolButton( "Connecter", "CONNECTER", FudaaResource.FUDAA.getIcon("connecter"), true);
       */
      // setEnabledForAction("CREER2", true);
      // setEnabledForAction("OUVRIR2", true);
      
      setEnabledForAction("CREER", true);
      setEnabledForAction("OUVRIR", true);
      setEnabledForAction("REOUVRIR", true);
      setEnabledForAction("ENREGISTRER", false);
      setEnabledForAction("ENREGISTRERSOUS", false);
      setEnabledForAction("FERMER", false);
      setEnabledForAction("QUITTER", true);
      setEnabledForAction("IMPORTER", false);
      setEnabledForAction("EXPORTER", false);
      setEnabledForAction("IMPRIMER", false);
      setEnabledForAction("PREFERENCE", true);
      setEnabledForAction("AIDE_INDEX", true);
      setEnabledForAction("AIDE_ASSISTANT", false);
      setEnabledForAction("TABLEAU", false);
      setEnabledForAction("GRAPHE", false);
      BuActionRemover.removeAction(getMainToolBar(), "COPIER");
      BuActionRemover.removeAction(getMainToolBar(), "COUPER");
      BuActionRemover.removeAction(getMainToolBar(), "COLLER");
      BuActionRemover.removeAction(getMainToolBar(), "DEFAIRE");
      BuActionRemover.removeAction(getMainToolBar(), "REFAIRE");
      BuActionRemover.removeAction(getMainToolBar(), "TOUTSELECTIONNER");
      BuActionRemover.removeAction(getMainToolBar(), "RANGERICONES");
      BuActionRemover.removeAction(getMainToolBar(), "RANGERPALETTES");
      BuActionRemover.removeAction(getMainToolBar(), "POINTEURAIDE");
      BuActionRemover.removeAction(getMainToolBar(), "RECHERCHER");
      BuActionRemover.removeAction(getMainToolBar(), "REMPLACER");
      BuActionRemover.removeAction(getMainToolBar(), "CONNECTER");
      BuActionRemover.removeAction(getMainMenuBar(), "POINTEURAIDE");
      
      /*mb.setEnabled(true);
      for(int i=0;i<mb.getMenuCount();i++)
    	  mb.getMenu(i).setEnabled(true);
      */
      final BuMenuRecentFiles mr = (BuMenuRecentFiles) mb.getMenu("REOUVRIR");
      if (mr != null) {
        mr.setPreferences(Sinavi3Preferences.SINAVI);
        mr.setResource(Sinavi3Resource.SINAVI);
        mr.setEnabled(true);
      }
      
      
      if (ss != null) {
        ss.setText("Assistant, barres des taches");
        ss.setProgression(75);
      }
      assistant_ = new Sinavi3Assistant();
      taches_ = new BuTaskView();
      
      //-- ajout des boutons de manipulation des simulations --//
      //Box csp1=Box.createVerticalBox();
      BuPanel csp1=new BuPanel(new BorderLayout());
      liste_ = new Sinavi3ListeSimulations(getApp());
      final BuScrollPane sp1 = new BuScrollPane(taches_);
      final BuScrollPane sp2 = new BuScrollPane(liste_);
      sp1.setPreferredSize(new Dimension(150, 80));
      sp2.setPreferredSize(new Dimension(150, 80));
      
      csp1.add(sp2,BorderLayout.CENTER);
      BuPanel b1=new BuPanel(new GridLayout(2,1,2,5));
      b1.add(ajouterSimu_);
      //BuPanel b2=new BuPanel(new GridLayout());
      b1.add(supprimerSimu_);
      
      csp1.add(b1,BorderLayout.SOUTH);
      //csp1.add(b2);
      //ajouterSimu_.setBackground(Color.white);
      //supprimerSimu_.setBackground(Color.white);
      //csp1.setBackground(Color.white);
      
      /*
       * getMainPanel().getRightColumn().addToggledComponent( "Assistant", "ASSISTANT", assistant_, this);
       */
      getMainPanel().getRightColumn().addToggledComponent(BuResource.BU.getString("Taches"), "TACHE", sp1, this);
      getMainPanel().getRightColumn().addToggledComponent("Simulations", "SIMULATIONS", csp1, this);
      
      
      ajouterSimu_.addActionListener(this);
      supprimerSimu_.addActionListener(this);
      
      if (ss != null) {
        ss.setText("Logo et barre des taches");
        ss.setProgression(95);
      }
      getMainPanel().setLogo(getInformationsSoftware().logo);
      getMainPanel().setTaskView(taches_);
      if (ss != null) {
        ss.setText("Termin�");

        ss.setProgression(100);
      }
    } catch (final Throwable t) {
      System.err.println("$$$ " + t);
      t.printStackTrace();
    }
    
    
  //-- application look and feel --// 
  //BuPreferences.BU.putStringProperty("lookandfeel.name", "ASPECT_PLASTIC3D");
  //BuPreferences.BU.applyLookAndFeel();
  //-- famille des images BU -//
  BuResource.BU.setIconFamily("crystal");
  //LiquidLookAndFeel.setLiquidDecorations(true, "mac");
//  LiquidLookAndFeel.setShowTableGrids(true);
    
  }
  
  private void extendFrame() {
      this.getFrame().setExtendedState(Frame.MAXIMIZED_BOTH);
  
}

  public void start() {
    super.start();
   
    assistant_.changeAttitude(BuAssistant.PAROLE, "Bienvenue !\n" + getInformationsSoftware().name + " "
        + getInformationsSoftware().version);
   
    final BuMainPanel mp = getMainPanel();
    mp.doLayout();
    mp.validate();
    projets_ = new Hashtable<String, FudaaProjet>();
    outils_ = new Sinavi3OutilsDonnees(projet_);
    assistant_.addEmitters((Container) getApp());
    assistant_.changeAttitude(BuAssistant.ATTENTE, "Vous pouvez cr�er une\nnouvelle simulation\nou en ouvrir une");
    // Application des pr�f�rences
    BuPreferences.BU.applyOn(this);
    Sinavi3Preferences.SINAVI.applyOn(this);
    extendFrame();
  }

  protected void construitToolBar(final BuToolBar newtb) {
    newtb.removeAll();
    newtb.addToolButton("Cr�er", "CREER", FudaaResource.FUDAA.getIcon("crystal_creer"), false);
    newtb.addToolButton("Ouvrir", "OUVRIR", FudaaResource.FUDAA.getIcon("crystal_ouvrir"), false);
    newtb.addToolButton("Enregistrer", "ENREGISTRER", FudaaResource.FUDAA.getIcon("crystal_enregistrer"), false);
    newtb.addToolButton("Nettoyer", "NETTOYER", FudaaResource.FUDAA.getIcon("crystal_valeur-initiale"), false);

    newtb.addSeparator();
    newtb.addSeparator();
    newtb.addSeparator();
    newtb.addToolButton("G�n�ral", "DONNEESGENERALES", FudaaResource.FUDAA.getIcon("analyser_"), false);
    
    newtb.addSeparator();

    newtb.addToolButton("Gares", "PARAMETREGARE", FudaaResource.FUDAA.getIcon("crystal_parametre"), false);
    newtb.addToolButton("Tron�ons", "PARAMETRECHENAL", FudaaResource.FUDAA.getIcon("crystal_parametre"), false);
    newtb.addToolButton("Ecluses", "PARAMETREECLUSE", FudaaResource.FUDAA.getIcon("crystal_parametre"), false);
    newtb.addToolButton("Bateaux", "PARAMETRECATEGORIE", FudaaResource.FUDAA.getIcon("crystal_parametre"), false);
    newtb.addSeparator();
    newtb.addToolButton("Topologie tron�ons", "TOPOLOGIECHENAL", FudaaResource.FUDAA.getIcon("graphe_"), false);
    newtb.addToolButton("Topologie �cluses", "TOPOLOGIEECLUSE", FudaaResource.FUDAA.getIcon("graphe_"), false);
    newtb.addToolButton("Mod�lisation du r�seau", "MODELISATIONTOPOLOGIE", FudaaResource.FUDAA.getIcon("crystal_colorier"),
        false);
    newtb.addSeparator();
    newtb.addToolButton("Croisement dans les tron�ons", "REGLESNAVIGATIONCHENAL", FudaaResource.FUDAA.getIcon("configurer_"), false);
    newtb.addToolButton("Tr�matage dans les tron�ons", "REGLESNAVIGATIONCHENAL2", FudaaResource.FUDAA.getIcon("configurer_"), false);
    newtb.addToolButton("Vitesse dans les tron�ons", "REGLESDUREEPARCOURSCHENAL", FudaaResource.FUDAA.getIcon("configurer_"), false);
    newtb.addToolButton("Dur�es de manoeuvres dans les �cluses", "REGLESDUREEMANEUVRE", FudaaResource.FUDAA.getIcon("configurer_"), false);
    newtb.addToolButton("Trajets par bateau", "REGLESTRAJETBATEAU", FudaaResource.FUDAA.getIcon("configurer_"), false);
    
    newtb.addSeparator();
    newtb.addSeparator();

    newtb.addToolButton("V�rification", "VERIFICATIONDONNEES", FudaaResource.FUDAA.getIcon("crystal_analyser"), false);
    newtb.addToolButton("Simulation", "LANCEMENTCALCUL", FudaaResource.FUDAA.getIcon("crystal_executer"), false);
    newtb.addSeparator();
    newtb.addToolButton("Bateaux g�n�r�s", "GRAPHEGENERATIONNAV", FudaaResource.FUDAA.getIcon("crystal_graphe"), false);
    newtb.addToolButton("Occupations des �cluses", "OCCUPATIONS", FudaaResource.FUDAA.getIcon("crystal_graphe"), false);
    newtb.addToolButton("Dur�es de parcours", "GRAPHEDUREEPARCOURS", FudaaResource.FUDAA.getIcon("crystal_graphe"), false);
    newtb.addToolButton("Attentes par trajet", "ATTENTESPECIALISEE", FudaaResource.FUDAA.getIcon("crystal_graphe"), false);
    newtb.addToolButton("Consommation eau par �cluse", "CONSOEAU", FudaaResource.FUDAA.getIcon("crystal_graphe"), false);
    newtb.addToolButton("garage �cluse", "GARAGEECLUSE", FudaaResource.FUDAA.getIcon("crystal_graphe"), false);
    

  }

  /**
   * Cr�� le menu donn�es g�n�rales
   * 
   * @param _app
   * 
   */
  protected BuMenu construitMenuDonneesGenerales(final boolean _app) {
    final BuMenu r = new BuMenu("G�n�ralit�s", "GENERALITES");
    BuMenuItem b=r.addMenuItem("Saisie des donn�es g�n�rales", "DONNEESGENERALES", FudaaResource.FUDAA.getIcon("analyser"), false);
    b.setMnemonic(KeyEvent.VK_O);
    b.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O, ActionEvent.SHIFT_MASK|ActionEvent.CTRL_MASK));
    
    r.setMnemonic(KeyEvent.VK_G);
    r.getAccessibleContext().setAccessibleDescription(
            "Saisie des donn�es g�n�rales");
    
    return r;
  }

  /**
   * Cr�� le menu simulation Ce menu permet d'entrer les parametres de saisies de la simulation -dans le menu
   * simulation, on a la possibilit� de cliquer sur les sous menus parmares quai, ecluses,tron�ons.
   */
  protected BuMenu construitMenuParametresConstruction(final boolean _app) {
    final BuMenu r = new BuMenu("Param�tres", "PARAMETRESSIMULATION");

    //-- modif AH --//
   BuMenuItem b= r.addMenuItem("Gares", "PARAMETREGARE", FudaaResource.FUDAA.getIcon("crystal_parametre"), false);
   b.setMnemonic(KeyEvent.VK_G);
   b.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_G, ActionEvent.SHIFT_MASK|ActionEvent.CTRL_MASK));
   
   /*
    b=r.addMenuItem("Bassins", "PARAMETREBASSIN", FudaaResource.FUDAA.getIcon("crystal_parametre"), false);
    b.setMnemonic(KeyEvent.VK_B);
    b.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_B, ActionEvent.SHIFT_MASK|ActionEvent.CTRL_MASK));
    */
   /*
    b=r.addMenuItem("Mar�es", "PARAMETREMAREE", FudaaResource.FUDAA.getIcon("crystal_parametre"), false);
    b.setMnemonic(KeyEvent.VK_M);
    b.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_M, ActionEvent.SHIFT_MASK|ActionEvent.CTRL_MASK));
    */
    b=r.addMenuItem("Tron�ons", "PARAMETRECHENAL", FudaaResource.FUDAA.getIcon("crystal_parametre"), false);
    b.setMnemonic(KeyEvent.VK_B);
    b.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_B, ActionEvent.SHIFT_MASK|ActionEvent.CTRL_MASK));
    /*
    b=r.addMenuItem("Cercles", "PARAMETRECERCLE", FudaaResource.FUDAA.getIcon("crystal_parametre"), false);
    b.setMnemonic(KeyEvent.VK_C);
    b.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_C, ActionEvent.SHIFT_MASK|ActionEvent.CTRL_MASK));
    */
    b=r.addMenuItem("Ecluses", "PARAMETREECLUSE", FudaaResource.FUDAA.getIcon("crystal_parametre"), false);
    b.setMnemonic(KeyEvent.VK_E);
    b.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_E, ActionEvent.SHIFT_MASK|ActionEvent.CTRL_MASK));
    /*
    b=r.addMenuItem("Quais", "PARAMETREQUAI", FudaaResource.FUDAA.getIcon("crystal_parametre"), false);
    b.setMnemonic(KeyEvent.VK_Q);
    b.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Q, ActionEvent.SHIFT_MASK|ActionEvent.CTRL_MASK));
    */
    b=r.addMenuItem("Bateaux", "PARAMETRECATEGORIE", FudaaResource.FUDAA.getIcon("crystal_parametre"), false);
    b.setMnemonic(KeyEvent.VK_N);
    b.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, ActionEvent.SHIFT_MASK|ActionEvent.CTRL_MASK));
    
    b=r.addMenuItem("D�lais remplissage SAS", "REGLESREMPLISSAGEECLUSE", FudaaResource.FUDAA.getIcon("crystal_parametre"), false);
    
    
    
    r.setMnemonic(KeyEvent.VK_P);
    r.getAccessibleContext().setAccessibleDescription(
            "Saisie des param�tres de la simulation");
    
    

    /*
     * r.addMenuItem("Calculer", "CALCULER", false); r.addMenuItem("Rappel des donn�es", "TEXTE", false);
     * r.addMenuItem("Exploitation de la simulation", "TABLEAU", false); r.addMenuItem("Exploitation du projet",
     * "GRAPHE", false);
     */

    return r;
  }

  protected BuMenu  construitMenuGeneration(final boolean _app) {
	  final BuMenu r = new BuMenu("G�n�ration", "GENERATION");
	   
	  BuMenuItem b=r.addMenuItem("G�n�ration des bateaux", "GENARR", FudaaResource.FUDAA.getIcon(""), false);
	 
	  b=r.addMenuItem("Affichage des bateaux g�n�r�s", "GENARR2", FudaaResource.FUDAA.getIcon(""), false);
	    
	  
	  return r;
  }
  
  /**
   * Cr�ation du menu projet. Ce menu permet de lancer les calculs et d'exploiter les resultats retourne le menu de
   * construction du projet:
   */

  protected BuMenu construitMenuSimulation(final boolean _app) {
    final BuMenu r = new BuMenu("Simulation", "LANCEMENTSIMULATION");
    // r.addMenuItem("Param�tres", "PARAMETRE", false);

   r.addSeparator("Projet");
    
    BuMenuItem b=r.addMenuItem("Rappel des donn�es", "RAPPELDONNEES", FudaaResource.FUDAA.getIcon(""), false);
    b.setMnemonic(KeyEvent.VK_R);
    b.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_R, ActionEvent.SHIFT_MASK|ActionEvent.CTRL_MASK));
    
    
    b=r.addMenuItem("Duplication de la simulation", "DUPLIQUERSIMU", FudaaResource.FUDAA.getIcon("dupliquer_"), false);
    b.setMnemonic(KeyEvent.VK_I);
    b.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_I, ActionEvent.SHIFT_MASK|ActionEvent.CTRL_MASK));
    
    r.addSeparator("Simulation");
    b=r.addMenuItem("V�rification des param�tres de simulation", "VERIFICATIONDONNEES", FudaaResource.FUDAA.getIcon("crystal_analyser"), false);
    b.setMnemonic(KeyEvent.VK_V);
    b.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_V, ActionEvent.SHIFT_MASK|ActionEvent.CTRL_MASK));
    
    
    b=r.addMenuItem("Ex�cution de la simulation", "LANCEMENTCALCUL", FudaaResource.FUDAA.getIcon("crystal_executer"), false);
    b.setMnemonic(KeyEvent.VK_L);
    b.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_L, ActionEvent.SHIFT_MASK|ActionEvent.CTRL_MASK));
    
    r.addSeparator("Historique");
    b= r.addMenuItem("Acc�s au fichier historique", "AUTRE2", FudaaResource.FUDAA.getIcon("crystal_arbre"), false);
    b.setMnemonic(KeyEvent.VK_H);
    b.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_H, ActionEvent.SHIFT_MASK|ActionEvent.CTRL_MASK));
    
    b=r.addMenuItem("Suppression du fichier historique", "AUTRE1", false);
    b.setMnemonic(KeyEvent.VK_F);
    b.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F, ActionEvent.SHIFT_MASK|ActionEvent.CTRL_MASK));
    
    
    
    
    // r.addMenuItem("Rappel des donn�es", "TEXTE", false);
    // r.addMenuItem("Exploitation de la simulation", "EXPLOITATIONTABLEAU", false);
    // r.addMenuItem("Exploitation du projet", "EXPLOITATIONGRAPHE", false);
    r.setMnemonic(KeyEvent.VK_U);
    r.getAccessibleContext().setAccessibleDescription(
            "Menu Simulation");
    return r;
  }

  /**
   * Cr�ation du menu projet. Ce menu permet de lancer les calculs et d'exploiter les resultats retourne le menu de
   * construction du projet:
   */

  protected BuMenu construitMenuSimulation2(final boolean _app) {
    final BuMenu r = new BuMenu("Simulation", "SIMULATION");
    r.addMenuItem("Param�tres", "PARAMETRE", false);

    r.addMenuItem("Calculer", "CALCULER", FudaaResource.FUDAA.getIcon("crystal_calculer"), false);
    r.addMenuItem("Rappel des donn�es", "TEXTE", false);
    r.addMenuItem("Exploitation de la simulation", "TABLEAU", false);
    r.addMenuItem("Exploitation du projet", "GRAPHE", false);

    return r;
  }

  /**
   * Cr�ation du menu topologie. Ce menu permet de cr�er la topologie du port a parti des diff�rents �l�ments d�ja
   * inscrits Il est neccessaire d'avoir au moins 2 gares afin de pouvoir cr��er une topologie retourne le menu de
   * topologie
   */

  protected BuMenu construitMenuTopologie(final boolean _app) {
    final BuMenu r = new BuMenu("R�seau", "TOPOLOGIE");

    r.addSeparator("Topologie");
    BuMenuItem b;
    /*
    BuMenuItem b=r.addMenuItem("Bassins", "TOPOLOGIEBASSIN", FudaaResource.FUDAA.getIcon("graphe_"), false);
    b.setMnemonic(KeyEvent.VK_B);
    b.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_B, ActionEvent.SHIFT_MASK|ActionEvent.ALT_MASK));
    */
    b=r.addMenuItem("Tron�ons", "TOPOLOGIECHENAL", FudaaResource.FUDAA.getIcon("graphe_"), false);
    b.setMnemonic(KeyEvent.VK_T);
    b.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_T, ActionEvent.SHIFT_MASK|ActionEvent.ALT_MASK));
    
    b=r.addMenuItem("Ecluses", "TOPOLOGIEECLUSE", FudaaResource.FUDAA.getIcon("graphe_"), false);
    b.setMnemonic(KeyEvent.VK_E);
    b.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_E, ActionEvent.SHIFT_MASK|ActionEvent.ALT_MASK));
    /*
    b=r.addMenuItem("Cercles d'�vitage", "TOPOLOGIECERCLE", FudaaResource.FUDAA.getIcon("graphe_"), false);
    b.setMnemonic(KeyEvent.VK_C);
    b.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_C, ActionEvent.SHIFT_MASK|ActionEvent.ALT_MASK));
    */
    r.addSeparator("Construction");
    b=r.addMenuItem("Mod�lisation du r�seau", "MODELISATIONTOPOLOGIE", FudaaResource.FUDAA.getIcon("crystal_colorier"), false);
    b.setMnemonic(KeyEvent.VK_D);
    b.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_D, ActionEvent.SHIFT_MASK));
    
    
    r.setMnemonic(KeyEvent.VK_E);
    r.getAccessibleContext().setAccessibleDescription(
            "Topologie du r�seau");
    
    return r;
  }

  /**
   * Cr�ation du menu DES REGLES DE NAVIGATION. Ce menu permet de lancer les saisies des regles de navigations retourne
   * le menu de construction du projet:
   */

  protected BuMenu construitMenuReglesNavigations(final boolean _app) {
    final BuMenu r = new BuMenu("Navigation", "REGLESNAVIGATION");

    
    
    
    
    r.addMenuItem("Croisement dans les Tron�ons", "REGLESNAVIGATIONCHENAL", FudaaResource.FUDAA.getIcon("configurer_"), false);
    r.addMenuItem("Tr�matage dans les Tron�ons", "REGLESNAVIGATIONCHENAL2", FudaaResource.FUDAA.getIcon("configurer_"), false);
    
    //r1.addMenuItem("Cercles", "REGLESNAVIGATIONCERCLE", FudaaResource.FUDAA.getIcon("configurer_"), false);
    r.addMenuItem("Vitesse des bateaux dans les Tron�ons", "REGLESDUREEPARCOURSCHENAL", FudaaResource.FUDAA.getIcon("configurer_"), false);
    r.addMenuItem("Dur�es de maneuvres dans les �cluses", "REGLESDUREEMANEUVRE", FudaaResource.FUDAA.getIcon("configurer_"), false);
    //r2.addMenuItem("Cercles", "REGLESDUREEPARCOURSCERCLE", FudaaResource.FUDAA.getIcon("configurer_"), false);
    r.addMenuItem("D�finition des trajets par bateau", "REGLESTRAJETBATEAU", FudaaResource.FUDAA.getIcon("configurer_"), false);
    
    
    
    r.setMnemonic(KeyEvent.VK_I);
     r.getAccessibleContext().setAccessibleDescription(
            "Navigation");
    return r;
  }

  /**
   * Methode qui permet la cr�ation de resultats des parametres.
   * 
   * @param _app
   * @return le menu des resultats.
   */
  protected BuMenu construitMenuExploitation(final boolean _app) {
    final BuMenu r = new BuMenu("R�sultats", "RESULTATSSIMU");

    // final BuMenu r3= new BuMenu("Durees de parcours ", "DURR");
    //final BuMenu r4 = new BuMenu("Occupation des quais", "OCC");
    //final BuMenu r5 = new BuMenu("Attentes", "ATT");
   

    r.addMenuItem("Bateaux g�n�r�s", "GRAPHEGENERATIONNAV", FudaaResource.FUDAA.getIcon("crystal_graphe"), false);

    //r.addMenuItem("Historique", "HISTORIQUETABLEAU", FudaaResource.FUDAA.getIcon("crystal_arbre"), false);

    r.addMenuItem("Dur�es de parcours", "GRAPHEDUREEPARCOURS", FudaaResource.FUDAA.getIcon("crystal_graphe"), false);
    
    r.addMenuItem("Occupations", "OCCUPATIONS", FudaaResource.FUDAA.getIcon("crystal_graphe"), false);

    r.addMenuItem("Consommation d'eau", "CONSOEAU", FudaaResource.FUDAA.getIcon("crystal_graphe"), false);
    r.addMenuItem("Garage �cluse", "GARAGEECLUSE", FudaaResource.FUDAA.getIcon("crystal_graphe"), false);
    
    
      r.addMenuItem("Attentes par cat�gorie de bateaux", "ATTENTEGENERALESCATEGORIETABLEAU", FudaaResource.FUDAA
        .getIcon("crystal_graphe"), false);
    r.addMenuItem("Attentes par trajet", "ATTENTESPECIALISEE", FudaaResource.FUDAA.getIcon("crystal_graphe"), false);
    //r.addSubMenu(r5, false);
  //  r.addMenuItem("Croisements dans les Tron�ons", "CROISEMENTSCHENAUXTABLEAU", FudaaResource.FUDAA.getIcon("crystal_arbre"), false);
  //  r.addMenuItem("Tr�matages dans les Tron�ons", "TREMATAGESCHENAUXTABLEAU", FudaaResource.FUDAA.getIcon("crystal_arbre"), false);
    
    r.addMenuItem("Tableau de Synth�se", "SYNTHESESIMU", FudaaResource.FUDAA.getIcon("crystal_tableau"), false);
   
    
   
    
    r.setMnemonic(KeyEvent.VK_R);
    r.getAccessibleContext().setAccessibleDescription(
            "R�sultats de la simulation");

    return r;
  }

  protected BuMenu construitMenuAutre(final boolean _app) {

    final BuMenu r = new BuMenu("Autre", "AUTRE");
    
    return r;
  }

  protected BuMenu construitMenuComparaisonSimulations(final boolean _app) {
    final BuMenu r = new BuMenu("Comparaison", "COMPARESIMU");
    r.addMenuItem("Bateaux g�n�r�s", "COMPARESIMU1", FudaaResource.FUDAA.getIcon("crystal_graphe"), false);
   // r.addMenuItem("Occupation globale des quais", "COMPARESIMU2", FudaaResource.FUDAA.getIcon("crystal_graphe"), false);
   // r.addMenuItem("Occupation d�taill�e des quais", "COMPARESIMU4", FudaaResource.FUDAA.getIcon("crystal_graphe"),false);
    r.addMenuItem("Dur�es de parcours", "COMPARESIMU3", FudaaResource.FUDAA.getIcon("crystal_graphe"), false);
    
    final BuMenu r1 = new BuMenu("Attentes", "ACOMPARE");
    r1.addMenuItem("Attentes par �l�ment", "COMPARESIMU5", FudaaResource.FUDAA.getIcon("crystal_graphe"), false);
    r1.addMenuItem("Attentes par trajet", "COMPARESIMU6", FudaaResource.FUDAA.getIcon("crystal_graphe"), false);
    r.addSubMenu(r1, true);
    
    
    r.setMnemonic(KeyEvent.VK_C);
    r.getAccessibleContext().setAccessibleDescription(
            "Comparaisons de simulation");
    
   r.addMenuItem("Tableau de Synth�se", "SYNTHESESIMUMULTI", FudaaResource.FUDAA.getIcon("crystal_tableau"), false);
    return r;
  }

  protected BuMenu construitMenuProjets(final boolean _app) {
    final BuMenu menu = new BuMenu("Projet", "PROJET");
    menu.addMenuItem("Enregistrer", "ENREGISTRER_LISTE", false);
    menu.addMenuItem("Ouvrir", "OUVRIR_LISTE", true);
    return menu;
  }

  


  
  // Actions
  public void actionPerformed(final ActionEvent _evt) {
    String action = _evt.getActionCommand();
    System.err.println("ACTION=" + action);
    String fichier= Sinavi3Lib.actionArgumentStringOnly(action);
    action = Sinavi3Lib.actionStringWithoutArgument(action);
    /**
     * signal= ihm saisies parametres
     */

    if (action.equals("DONNEESGENERALES")) {
      // lancement de l'interface de saisie des donn�es generales
      parametreDonneesGenerales();
    }
    if (action.equals("SINAVI2")) {
        // lancement de l'interface de saisie des donn�es generales
        donnees_.moulinetteRecuperationSinavi2();
      }

    else if (action.equals("PARAMETREECLUSE")) {
      parametreEcluse(); // signal sur ecluse => lance l application de saisie des ecluses
      assistant_.changeAttitude(BuAssistant.PAROLE, "Mes ecluses... \nOu les ai-je mises???\n"
          + getInformationsSoftware().name + " " + getInformationsSoftware().version);
    } else if (action.equals("PARAMETRECHENAL")) {
      parametreBief(); // signal sur Tron�ons => lance l application de saisie des cheneaux
      assistant_.changeAttitude(BuAssistant.PAROLE, "Par ou je passe??\n" + getInformationsSoftware().name + " "
          + getInformationsSoftware().version);
    } else if (action.equals("PARAMETRECATEGORIE")) {
      if (this.donnees_.listeGare_.listeGares_.size() <= 1) {
        new BuDialogError(getApp(), getInformationsSoftware(),
            "Erreur,Il faut au moins deux gares pour cr�er un bateau!! ").activate();
      } else

      {
        parametreNavire(); // signal sur categorie de navire => lance l application de saisie des cat�gories
        assistant_.changeAttitude(BuAssistant.PAROLE, "Oh mon bateau!! tu es\n le plus beau de tous\n les bateaux!\n"
            + getInformationsSoftware().name + " " + getInformationsSoftware().version);
      }

    } else if (action.equals("PARAMETREGARE")) {
      parametreGare(); // signal sur quai => lance l application de saisie des quais
      assistant_.changeAttitude(BuAssistant.PAROLE, "Il en faut, il en faut!!\n" + getInformationsSoftware().name + " "
          + getInformationsSoftware().version);
    } else
    /**
     * signal = ihm de topologie du port:
     */
    if (action.equals("TOPOLOGIEBASSIN")) {
      activerCommandesTopologie(1);

    } else if (action.equals("TOPOLOGIECHENAL")) {
      activerCommandesTopologie(3);
    } else if (action.equals("TOPOLOGIEECLUSE")) {
      activerCommandesTopologie(2);
    } else if (action.equals("TOPOLOGIECERCLE")) {
      activerCommandesTopologie(4);
    } else if (action.equals("MODELISATIONTOPOLOGIE")) {
      //activerModelisation();
    	activerNetworkEditor();
    } else if (action.equals("REGLESNAVIGATIONCHENAL")) {
      activerReglesCroisement();

    }else if (action.equals("REGLESTRAJETBATEAU")) {
      activerGestionTrajet();

    } else if (action.equals("REGLESNAVIGATIONCHENAL2")) {
      activerReglesTrematage();

    } else if (action.equals("REGLESREMPLISSAGEECLUSE")) {
        activerRemplissageSAS();
    } else if(action.equals("GENARR")){
    	lancementGenarr();
    }else if(action.equals("GENARR2")){
    	new BuTaskOperation(this, "generation navires", "affichageGenarr").start();
    	
    }else if (action.equals("VERIFICATIONDONNEES")) {
    
      activerVerficiationDonnees();
    } else if (action.equals("DUPLIQUERSIMU")) {

      donnees_.dupliquerSimulation();
    } else if (action.equals("REGLESDUREEPARCOURSCHENAL")) {
      activerReglesVitessesBief();
    } else if(action.equals("REGLESDUREEMANEUVRE")){
    	activerReglesDurManeuvreEcluses();
    }
    else if (action.equals("RAPPELDONNEES")) {
      activationRappelDonnees();
    } else if (action.equals("LANCEMENTCALCUL")) {
      new BuTaskOperation(this, "Calcul", "lancementCalcul").start();
      // lancementCalcul();
    } else if (action.equals("GRAPHEGENERATIONNAV")) {
      new BuTaskOperation(this, "generation navires", "activerResultatsgenerationNavires").start();
      // activerResultatsgenerationNavires();
      // resultatsSimulations();//test graphes
    } else if (action.equals("HISTORIQUETABLEAU") ||action.equals("AUTRE2")) {
      new BuTaskOperation(this, "historique", "activerResultatHistorique").start();
      // activerResultatHistorique();
    } else if (action.equals("AUTRE1")) {
        new BuTaskOperation(this, "Suppression Historique", "supprimerHistoriqueSimulation").start();
        // activerResultatDureesParcours();
    }
    else if (action.equals("OCCUPATIONS")) {
    	new BuTaskOperation(this, "Occupations", "activerResultatOccupations").start();
    }else if (action.equals("CONSOEAU")) {    	
    	new BuTaskOperation(this, "ConsommationEau", "activerResultatConsommationEau").start();
    }
    else if (action.equals("GRAPHEDUREEPARCOURS")) {
      new BuTaskOperation(this, "durees parcours", "activerResultatDureesParcours").start();
      // activerResultatDureesParcours();
    } else if (action.equals("GARAGEECLUSE")) {
        new BuTaskOperation(this, "garage �cluses", "activerResultatGarage").start();
        // activerResultatDureesParcours();
      }else if (action.equals("CROISEMENTSCHENAUXTABLEAU")) {
      new BuTaskOperation(this, "croisements Tron�ons", "activerResultatCroisementsBiefs").start();
      // activerResultatCroisementsChenaux();
    }  else if (action.equals("TREMATAGESCHENAUXTABLEAU")) {
        new BuTaskOperation(this, "croisements Tron�ons", "activerResultatTrematagesBiefs").start();
      // activerResultatCroisementsChenaux();
        }
    else if (action.equals("ATTENTEGENERALESELEMENTTABLEAU")) {
      new BuTaskOperation(this, "attentes elements", "activerResultatAttentesGeneralesElement").start();
      // activerResultatAttentesGeneralesElement();
    } else if (action.equals("ATTENTEGENERALESCATEGORIETABLEAU")) {
      new BuTaskOperation(this, "attentes cat�gories", "activerResultatAttentesGeneralesCategorie").start();
      // activerResultatAttentesGeneralesCategorie();
    } else if (action.equals("ATTENTESPECIALISEE")) {
      new BuTaskOperation(this, "attentes trajet", "activerResultatAttentesSpecialisee").start();
      // activerResultatAttentesSpecialisee();
    } else if (action.equals("TABLEAUOCCUPGLOBAL")) {
      new BuTaskOperation(this, "occupation globale", "activerResultatOccupGlobales").start();
      // activerResultatOccupGlobales();
    } else if (action.equals("TABLEAUOCCUPQUAI")) {
      new BuTaskOperation(this, "occupation detaillee", "activerResultatOccupQuai").start();
      // activerResultatOccupQuai();
    } else if (action.equals("COMPARESIMU1")) {
      new BuTaskOperation(this, "comparaison nb navires", "activerComparaisonNombreNavires").start();
    } else if (action.equals("COMPARESIMU2")) {
      new BuTaskOperation(this, "comparaison nb navires", "activerComparaisonOccupationGlobale").start();
    } else if (action.equals("COMPARESIMU3")) {
      new BuTaskOperation(this, "comparaison dur�e de parcours", "activerComparaisonDureesParcours").start();
    } else if (action.equals("COMPARESIMU4")) {
      new BuTaskOperation(this, "comparaison d'occupation aux quais", "activerComparaisonOccupationQuais").start();
    }else if (action.equals("COMPARESIMU5")) {
      new BuTaskOperation(this, "comparaison d'occupation aux quais", "activerComparaisonAttenteGenerale").start();
    }else if (action.equals("COMPARESIMU6")) {
      new BuTaskOperation(this, "comparaison d'occupation aux quais", "activerComparaisonAttenteTrajet").start();
    }
    else if (action.equals("CREER")) {
      creer();
    } else if (action.equals("CREER2")) {
      creer();
    } else if (action.equals("OUVRIR")) {
      ouvrir(null);
      setEnabledForAction("REOUVRIR", true);
    } else if (action.equals("OUVRIR2")) {
      ouvrir(null);
      setEnabledForAction("REOUVRIR", true);
    }else if (action.equals("REOUVRIR")){
    	
    	ouvrir(fichier);
    	System.out.println("fichier reouvrir: "+fichier);
    	
    }else if(action.equals("SYNTHESESIMU")) {
        Sinavi3GestionResultats.exportReport(donnees_, true,true,true,true,true,false);
    }else if(action.equals("SYNTHESESIMUMULTI")) {
        if(donnees_.getApplication().getSimulations().length<=1) {
            new BuDialogError(getApp(), getInformationsSoftware(),
                    "Vous devez ouvrir plusieurs simulations pour pouvoir les comparer.\n Utilisez le bouton Ouvrir pour ouvrir d'autre projets comparables.").activate();
         
        }else
        Sinavi3GestionResultats.exportReport(donnees_, true,true,true,true,true,true);
    }
    else if (action.equals("NETTOYER")) {
      fermerToutesLesInterfaces();
    } else if (action.equals("ENREGISTRER")) {
      enregistrer();
    } else if (action.equals("ENREGISTRERSOUS")) {
      enregistrerSous();

    } else if (action.equals("FERMER")) {
      fermer();
    } else if (action.equals("QUITTER")) {
      // quitter();
      exit();
    }
    else if(_evt.getSource().equals(supprimerSimu_)){
    	supprimerSimulation();
    }
    else  if(_evt.getSource().equals(ajouterSimu_)){
    	ouvrir(null);
    }
    	

    else if (action.equals("ENREGISTRER_LISTE")) {
      liste_.enregistre();
    } else if (action.equals("OUVRIR_LISTE")) {
      liste_.ouvrir();

    } else {
      super.actionPerformed(_evt);
    }
  }

  public void oprServeurCopie() {
    System.err.println("Lancement du serveur de copie d'ecran");
    new ServeurCopieEcran(getMainPanel(), "ScreenSpy");
  }

  /**
   * Retire une �ventuelle extension au nom du fichier du projet et lance la lecture du fichier de r�sultats brutes
   * correspondants.
   */
  /*public void recevoirResultats() {
    projet_.addResult(Sinavi3Resource.resultats, siporResults_.litResultatsSipor());
  }*/

  /**
   * Methode permettant d'effectuer le lancement de al simulation envoyer les parametres au server via la classe
   * SiporParams_
   */

  public void oprCalculer() {
     
	  //-- redirection vers la procedure de lancement de calcul --//
	  lancementCalcul();
	  
  }

  /**
   * Commandes activ�es d�s qu'une simulation est charg�e. Les commandes sont activ�es apres chargmeent d un projet
   */
  protected void activerCommandesSimulation() {
    setEnabledForAction("PARAMETRE", true);
    setEnabledForAction("TEXTE", true);
    setEnabledForAction("ENREGISTRER", true);
    setEnabledForAction("ENREGISTRERSOUS", true);
    setEnabledForAction("FERMER", true);
    setEnabledForAction("CALCULER", true);
    setEnabledForAction("ENREGISTRER_LISTE", true);

    /**
     * Activation des donn�es generales
     */
    setEnabledForAction("DONNEESGENERALES", true);
    setEnabledForAction("SINAVI2", true);
    /**
     * Activation des menu de saisies des donn�es de la simulation
     */
    setEnabledForAction("PARAMETREQUAI", true);
    setEnabledForAction("PARAMETREECLUSE", true);
    setEnabledForAction("PARAMETREBASSIN", true);
    setEnabledForAction("PARAMETREGARE", true);
    setEnabledForAction("PARAMETRECERCLE", true);
    setEnabledForAction("PARAMETRECATEGORIE", true);
    setEnabledForAction("PARAMETRECHENAL", true);
    setEnabledForAction("PARAMETREMAREE", true);

    /**
     * Activation des menu de saisies de la topologie du porc grouink
     */
    setEnabledForAction("TOPOLOGIEBASSIN", true);
    setEnabledForAction("TOPOLOGIEECLUSE", true);
    setEnabledForAction("TOPOLOGIECHENAL", true);
    setEnabledForAction("TOPOLOGIECERCLE", true);

    /**
     * Activation du menu de modelisation du port:
     */
    setEnabledForAction("MODELISATIONTOPOLOGIE", true);

    /**
     * Activation du menu regles de navigations
     */
    setEnabledForAction("NAVIGATIONCROISEMENT", true);
    setEnabledForAction("NAVIGATIONDUREES", true);

    setEnabledForAction("REGLESNAVIGATION", true);
    setEnabledForAction("REGLESNAVIGATIONCHENAL", true);
    setEnabledForAction("REGLESREMPLISSAGEECLUSE", true);
    setEnabledForAction("REGLESNAVIGATIONCHENAL2", true);
    setEnabledForAction("REGLESNAVIGATIONCERCLE", true);
    setEnabledForAction("REGLESDUREEPARCOURSCHENAL", true);
    setEnabledForAction("REGLESDUREEMANEUVRE", true);
    setEnabledForAction("REGLESDUREEPARCOURSCERCLE", true);
    setEnabledForAction("REGLESTRAJETBATEAU", true);
    /**
     * Activation du menu simulation:
     */
    setEnabledForAction("VERIFICATIONDONNEES", true);
    setEnabledForAction("DUPLIQUERSIMU", true);
    setEnabledForAction("NETTOYER", true);

    //-- Menu g�n�ration de navires --//
    setEnabledForAction("GENARR", true);
    setEnabledForAction("GENARR2", true);
    
    // menu resultats
    activerCommandesResultatsSimulation();
  }

  void activerCommandesResultatsSimulation() {

    if (this.donnees_.params_.niveau >= 1) {
      setEnabledForAction("RAPPELDONNEES", true);
    }

    // menu calcul
    if (this.donnees_.params_.niveau >= 1) {
      setEnabledForAction("LANCEMENTCALCUL", true);
      
      
      
      
    }

    // menu resultats
    if (this.donnees_.params_.niveau >= 2) {
    	
      setEnabledForAction("AUTRE1", true);
      setEnabledForAction("AUTRE2", true);
      setEnabledForAction("GRAPHEGENERATIONNAV", true);
      setEnabledForAction("HISTORIQUETABLEAU", true);
      setEnabledForAction("OCCUPATIONS", true);
      setEnabledForAction("CONSOEAU", true);
      setEnabledForAction("GARAGEECLUSE", true);
      
      setEnabledForAction("GRAPHEDUREEPARCOURS", true);
      
      setEnabledForAction("ATTENTEGENERALESELEMENTTABLEAU", true);
      setEnabledForAction("ATTENTEGENERALESCATEGORIETABLEAU", true);
      setEnabledForAction("ATTENTESPECIALISEE", true);

      setEnabledForAction("TABLEAUOCCUPGLOBAL", true);
      setEnabledForAction("TABLEAUOCCUPQUAI", true);
      setEnabledForAction("SYNTHESESIMU", true);
      setEnabledForAction("SYNTHESESIMUMULTI", true);

      setEnabledForAction("CROISEMENTSCHENAUXTABLEAU", true);
      setEnabledForAction("TREMATAGESCHENAUXTABLEAU", true);
      

      setEnabledForAction("RESULTATSSIMU", true);
      setEnabledForAction("GN", true);
      setEnabledForAction("HIST", true);
      setEnabledForAction("DURR", true);
      setEnabledForAction("OCC", true);
      setEnabledForAction("ATT", true);
      setEnabledForAction("CR", true);

      // activer els comparaisons de simulation
      setEnabledForAction("COMPARESIMU1", true);
      setEnabledForAction("COMPARESIMU2", true);
      setEnabledForAction("COMPARESIMU3", true);
      setEnabledForAction("COMPARESIMU4", true);
      setEnabledForAction("COMPARESIMU5", true);
      setEnabledForAction("COMPARESIMU6", true);
    }

  }

  /**
   * M�thode qui active la saisie des topologie des que les conditions suivantes sont remplies: - au moins 2 gares
   * saisies par l'utilisateur dans la partie donn�e - voir avec Alain P et C pour d'autre règles d'activation de la
   * siaie de la topologie Les commandes sont activ�es des que les conditions ci dessus sont remplies Fonctionnement de
   * la methode:
   * 
   * @param numeroParametreConsidere un entier en parametre d'entr�e qui indique de quel �l�ment de saisie il s'agit
   *          exemple: 1 => bassin 2 => Ecluse......
   */
  protected void activerCommandesTopologie(final int numeroParametreConsidere) {

    

    if (numeroParametreConsidere != 1) {
      if (donnees_.listeGare_.listeGares_.size() >= 2) {

        /**
         * Test d activation des ecluses si au moins une ecluse existe
         */
        if (numeroParametreConsidere == 2) {
          if (donnees_.listeEcluse_.listeEcluses_.size() >= 1) {
        	  gestionTopoEcluses_=new Sinavi3PanelTopologieEcluse(donnees_);
            this.addInternalFrame(gestionTopoEcluses_);
            

          } else {
            new BuDialogError(getApp(), getInformationsSoftware(), "Aucune �cluse n'a �t� d�finie dans le projet courant.\nVous pouvez d�finir une �cluse via le menu \"Param�tres\".").activate();
          }
        }

        /**
         * Test d activation des cheneaux si au moins un chenal existe
         */
        if (numeroParametreConsidere == 3) {
          if (donnees_.listeBief_.listeBiefs_.size() >= 1)
          {
        	  gestionTopoChenaux_=new Sinavi3PanelTopologieBief(donnees_);
            this.addInternalFrame(gestionTopoChenaux_);
            
          } else {
            new BuDialogError(getApp(), getInformationsSoftware(), "Aucun tron�on n'a �t� d�fini dans le projet courant.\nVous pouvez d�finir un tron�on via le menu \"Param�tres\".").activate();
          }
        }

        

      }// fin du if si au moins 2 gares ont �t� saisies
      else {
        /**
         * on lance une fenetre indiquant l'erreur: il faut au moins 2 gares de saisies:
         */
        new BuDialogError(getApp(), getInformationsSoftware(),
            "Deux gares sont n�cessaires pour �diter la topologie.\nVous pouvez d�finir des gares via le menu \"Param�tres\".").activate();

      }

    }// fin du if si l on est dans le cas d une topologie pour les �l�ments differents du bassin

  }

  /**
   * Methode qui r�alise els controles et lance la fenetre de modelisation des ports
   */
  @Deprecated
  public void activerModelisation() {
    if (donnees_.listeGare_.listeGares_.size() >= 2) {

      /**
       * ajout d'une fenetre interne
       */
      if (this.gestionModelisation_ == null) {
        FuLog.debug("interface nulle");
        gestionModelisation_ = new Sinavi3DessinerPortFrame(this.donnees_, this);
        // System.out.println("55555");
        gestionModelisation_.setVisible(true);
        addInternalFrame(gestionModelisation_);
      } else {
        FuLog.debug("interface ferm�e");
        if (gestionModelisation_.isClosed()) {
          addInternalFrame(gestionModelisation_);

        } else {
          FuLog.debug("interface cas de figur restant autre que null et fermeture");
          activateInternalFrame(gestionModelisation_);
          addInternalFrame(gestionModelisation_);

        }

      }
      
    } else {
      /**
       * on lance une fenetre indiquant l'erreur: il faut au moins 2 gares de saisies:
       */
      new BuDialogError(getApp(), getInformationsSoftware(),
    		  "Deux gares sont n�cessaires pour mod�liser le r�seau.\nVous pouvez d�finir des gares via le menu \"Param�tres\".")
          .activate();

    }

  }
  

  public void updateNetworkData() {
	  if(gestionChenaux_ != null)
		  gestionChenaux_.getAffichagePanel_().maj(donnees_);
	  if(gestionEcluses_ != null) 
		  gestionEcluses_.affichagePanel_.maj(donnees_);	  
	   if(gestionGares_ != null) 
		   gestionGares_.affichagePanel_.maj(donnees_);	 	
  }
  
  
 Sinavi3InternalFrame networkInternalFrame = null ;
  
  public void activerNetworkEditor() {
	  
	  //-- create frame --//
	  if(networkInternalFrame == null) {
		  networkInternalFrame = new Sinavi3InternalFrame("Mod�lisation",true,true,true,true);
		  networkInternalFrame.getContentPane().add(networkEditor);
		  networkInternalFrame.setJMenuBar(new EditorMenuBar(networkEditor));
		  networkInternalFrame.setSize(900, 700);
		  JButton boutonSynchro = new JButtonNoFocus(networkEditor.bind("Re-synchronizer avec Sinavi params", new SinaviSyncrhonizeNetworkAction(networkEditor,this.donnees_),
					"/org/fudaa/ebli/network/simulationNetwork/images/connect.gif"));
		  boutonSynchro.setText("Re-synchronizer avec Sinavi params");
		  networkEditor.getToolBar().add(boutonSynchro );
	  }
	  addInternalFrame(networkInternalFrame);
	  
	  /*JInternalFrame frame =  networkEditor.createInternalFrame(new EditorMenuBar(networkEditor));
	  */
	  
	  //-- try to load the graph if exist. project name + .network extension --//
	  File networkFile = new File(this.donnees_.getProjet_().getFichier() + ".network");
	  if(networkFile.exists()) {
		try {
			networkEditor.loadGraph(networkFile);
		} catch (IOException e) {
			FuLog.error(e);
			 new BuDialogError(getApp(), getInformationsSoftware(),
			          "Erreur lors du chargement du graphe. le fichier est corrompu.").activate();
		}
	  } else {
		  //-- clean the network no matter --//
		  networkEditor.cleanAllNetwork();
	  }
	  
	  
  }
  
  

  /**
   * Methode qui r�alise les controles et lance la fenetre de saisies des regles de navigation.
   */
  protected void activerReglesCroisement() {
    /**
     * une patate de controles et de test doit etre effectu� afin de determiner si: 1) les tableau poru els cheneaux ont
     * d�ja �t� saisis ou non 2) plus de possibilit� de modifier les cat�gories de navires
     */

    // 1) demander confirmation de lancer cette fenetre en sachant que la saisie des navires sera bloqu�e
    // 2)lancer l'application
    if (this.donnees_.listeBief_.listeBiefs_.size() <= 0)

    {
      /**
       * on lance une fenetre indiquant l'erreur: il faut au moins 2 gares de saisies:
       */
      new BuDialogError(getApp(), getInformationsSoftware(),
    		  "Aucun tron�on n'a �t� d�fini dans le projet courant.\nVous pouvez d�finir un tron�on via le menu \"Param�tres\".")
          .activate();

    } else if (this.donnees_.listeBateaux_.listeNavires_.size() <= 0) {
      new BuDialogError(getApp(), getInformationsSoftware(),
    		  "Aucune cat�gorie de bateaux n'a �t� d�finie dans le projet courant.\nVous pouvez d�finir une cat�gorie via le menu \"Param�tres\".")
          .activate();
    } else

    {
      /**
       * on lance une fenetre indiquant l'erreur: il faut au moins 2 gares de saisies:
       */
       gestionCroisementBief_ = new Sinavi3PanelReglesCroisementBief(this.donnees_);
      addInternalFrame(gestionCroisementBief_);

    }

  }
  
  protected void activerGestionTrajet() {
	    /**
	     * une patate de controles et de test doit etre effectu� afin de determiner si: 1) les tableau poru els cheneaux ont
	     * d�ja �t� saisis ou non 2) plus de possibilit� de modifier les cat�gories de navires
	     */

	    // 1) demander confirmation de lancer cette fenetre en sachant que la saisie des navires sera bloqu�e
	    // 2)lancer l'application
	    if (this.donnees_.listeBateaux_.listeNavires_.size() <= 0)

	    {
	      /**
	       * on lance une fenetre indiquant l'erreur: il faut au moins 2 gares de saisies:
	       */
	      new BuDialogError(getApp(), getInformationsSoftware(),
	      "Aucune cat�gorie de bateaux n'a �t� d�finie dans le projet courant.\nVous pouvez d�finir une cat�gorie via le menu \"Param�tres\".")
	          .activate();

	    
	    } else

	    {
	      /**
	       * on lance une fenetre indiquant l'erreur: il faut au moins 2 gares de saisies:
	       */
	       gestionTrajetsBateaux_= new Sinavi3PanelGestionTrajetsBateaux(this.donnees_);
	      addInternalFrame(gestionTrajetsBateaux_);

	    }

	  }
  
  /**
   * Methode qui r�alise les controles et lance la fenetre de saisies des regles de navigation.
   */
  protected void activerReglesTrematage() {
    /**
     * une patate de controles et de test doit etre effectu� afin de determiner si: 1) les tableau poru els cheneaux ont
     * d�ja �t� saisis ou non 2) plus de possibilit� de modifier les cat�gories de navires
     */

    // 1) demander confirmation de lancer cette fenetre en sachant que la saisie des navires sera bloqu�e
    // 2)lancer l'application
    if (this.donnees_.listeBief_.listeBiefs_.size() <= 0)

    {
      /**
       * on lance une fenetre indiquant l'erreur: il faut au moins 2 gares de saisies:
       */
      new BuDialogError(getApp(), getInformationsSoftware(),
      "Aucun tron�on n'a �t� d�fini dans le projet courant.\nVous pouvez d�finir un tron�on via le menu \"Param�tres\".")
          .activate();

    } else if (this.donnees_.listeBateaux_.listeNavires_.size() <= 0) {
      new BuDialogError(getApp(), getInformationsSoftware(),
      "Aucune cat�gorie de bateaux n'a �t� d�finie dans le projet courant.\nVous pouvez d�finir une cat�gorie via le menu \"Param�tres\".")
          .activate();
    } else

    {
      /**
       * on lance une fenetre indiquant l'erreur: il faut au moins 2 gares de saisies:
       */
        gestionTrematageBief_ = new Sinavi3PanelReglestrematageBief(this.donnees_);
      addInternalFrame(gestionTrematageBief_);

    }

  }
 
  /**
   * Methode de lancement des vitesses des navires dans les tron�ons
   */
  protected void activerReglesVitessesBief() {

    if (this.donnees_.listeBief_.listeBiefs_.size() <= 0)

    {
      /**
       * on lance une fenetre indiquant l'erreur: il faut au moins 2 gares de saisies:
       */
      new BuDialogError(getApp(), getInformationsSoftware(),
    		  "Aucun tron�on n'a �t� d�fini dans le projet courant.\nVous pouvez d�finir un tron�on via le menu \"Param�tres\".")
          .activate();

    } else if (this.donnees_.listeBateaux_.listeNavires_.size() <= 0) {
      new BuDialogError(getApp(), getInformationsSoftware(),
    		  "Aucune cat�gorie de bateaux n'a �t� d�finie dans le projet courant.\nVous pouvez d�finir une cat�gorie via le menu \"Param�tres\".")
          .activate();
    } else

    {
      // 2)lancer l'applicationw
    	
    	gestionVitessesBief_=new Sinavi3PanelVitessesBief(donnees_);
      this.addInternalFrame(gestionVitessesBief_);
    }
  }
  /**
   * Methode de lancement des durees de maneuvres dans les ecluses
   */
  protected void activerReglesDurManeuvreEcluses() {

    if (this.donnees_.listeEcluse_.listeEcluses_.size() <= 0)

    {
      /**
       * on lance une fenetre indiquant l'erreur: il faut au moins 2 gares de saisies:
       */
      new BuDialogError(getApp(), getInformationsSoftware(),
    		  "Aucune �cluse n'a �t� d�finie dans le projet courant.\nVous pouvez d�finir une �cluse via le menu \"Param�tres\".")
          .activate();

    } else if (this.donnees_.listeBateaux_.listeNavires_.size() <= 0) {
      new BuDialogError(getApp(), getInformationsSoftware(),
    		  "Aucune cat�gorie de bateaux n'a �t� d�finie dans le projet courant.\nVous pouvez d�finir une cat�gorie via le menu \"Param�tres\".")
          .activate();
    } else

    {
      // 2)lancer l'applicationw
    	
    	//gestionDureeManeuvreEcluse_=new Sinavi3PanelDureeManeuvresEcluses(donnees_);     
    	gestionDureeManeuvreEcluse_ = new SinaviPanelDureeManeuvresEcluses(donnees_);
      this.addInternalFrame(gestionDureeManeuvreEcluse_);
    }
  }
  
  /**
   * Methode tres importante de verification des donn�es avant lancement de la simulation;
   */
  protected void activerVerficiationDonnees() {
    if (this.donnees_.verificationDonnees()) {
      // passage au niveau1: possibilit� de lancer la simulation
      if (this.donnees_.params_.niveau < 1) {
        this.donnees_.params_.niveau = 1;
      }
      // mise a jour des boutons accessibles pour la simu:
      activerCommandesResultatsSimulation();

    }
  }
  
  
  /**
   * Methode qui place le niveau de s�curite de l'application au plus bas.
   * Consequence: on doit redemarrer le test de v�rification des coh�rences des donn�es.
   * Utiliser: on utilise cette m�thode dans le cas ou l'on modifie des donn�es.
   *
   */
  public void baisserNiveauSecurite(){
	  this.donnees_.params_.niveau=0;
	 
	  
	      //setEnabledForAction("CONSOEAU", false);
	      
	  
	  	setEnabledForAction("RAPPELDONNEES", false);
	      setEnabledForAction("LANCEMENTCALCUL", false);
	      setEnabledForAction("AUTRE1", false);
	      setEnabledForAction("AUTRE2", false);
	      setEnabledForAction("GRAPHEGENERATIONNAV", false);
	      setEnabledForAction("HISTORIQUETABLEAU", false);
	      setEnabledForAction("OCCUPATIONS", false);
	      setEnabledForAction("GRAPHEDUREEPARCOURS", false);

	      setEnabledForAction("ATTENTEGENERALESELEMENTTABLEAU", false);
	      setEnabledForAction("ATTENTEGENERALESCATEGORIETABLEAU", false);
	      setEnabledForAction("ATTENTESPECIALISEE", false);

	      setEnabledForAction("TABLEAUOCCUPGLOBAL", false);
	      setEnabledForAction("SYNTHESESIMU", false);
              setEnabledForAction("SYNTHESESIMUMULTI", false);
              
	      setEnabledForAction("TABLEAUOCCUPQUAI", false);

	      setEnabledForAction("CROISEMENTSCHENAUXTABLEAU", false);
	      setEnabledForAction("TREMATAGESCHENAUXTABLEAU", false);

	      //setEnabledForAction("RESULTATSSIMU", false);
	      setEnabledForAction("GN", false);
	      setEnabledForAction("HIST", false);
	      setEnabledForAction("DURR", false);
	      setEnabledForAction("OCC", false);
	      setEnabledForAction("ATT", false);
	      setEnabledForAction("CR", false);

	      // activer els comparaisons de simulation
	      setEnabledForAction("COMPARESIMU1", false);
	      setEnabledForAction("COMPARESIMU2", false);
	      setEnabledForAction("COMPARESIMU3", false);
	      setEnabledForAction("COMPARESIMU4", false);
	      setEnabledForAction("COMPARESIMU5", false);
	      setEnabledForAction("COMPARESIMU6", false);
	    

	  
  }
  

  /**
   * methode de lancement de calculs de sinavi
   */
  public void lancementCalcul() {

    enregistrer();
    
    //-- creation d'une fenetre d'affichage d e la progression --//
    Sinavi3ProgressFrame fenetreProgression=new Sinavi3ProgressFrame();
    this.addInternalFrame(fenetreProgression);
    
    final BuMainPanel mp = getMainPanel();
    mp.setProgression(0);

    
    
    // ETAPE 1: ecriture des diff�rents fichiers de donn�es pour le noyau de clacul:

    mp.setMessage("Envoi des param�tres au noyau de calcul...");
    mp.setProgression(10);

    try {
    	fenetreProgression.miseAjourBarreProgression(0, "Ecriture des param�tres...", "Ecriture des param�tres bateaux");
    	//-- ecriture des parametres avant lancement calcul
    	  
    	
    	  //cat�gories
		  DParametresSinavi3.ecritDonneesCategoriesNavires(this.donnees_.params_.navires,this.donnees_.projet_.getFichier());
		  fenetreProgression.miseAjourBarreProgression(1, "Ecriture des param�tres g�n�raux");
		  // donn�es generales
		  DParametresSinavi3
		  .ecritDonneesGenerales(this.donnees_.params_.donneesGenerales,this.donnees_.projet_.getFichier());
		  fenetreProgression.miseAjourBarreProgression(2, "Ecriture des param�tres �cluses");
		  // ecluses
		  DParametresSinavi3.ecritDonneesEcluses(this.donnees_.params_.ecluses,this.donnees_.projet_.getFichier());
		   fenetreProgression.miseAjourBarreProgression(3, "Ecriture des param�tres gares");
		  // gares
		  DParametresSinavi3.ecritDonneesGares(this.donnees_.params_.gares,this.donnees_.projet_.getFichier());
		   fenetreProgression.miseAjourBarreProgression(4, "Ecriture des param�tres tron�ons");
		  // bief
		  DParametresSinavi3.ecritDonneesBief(this.donnees_.params_.cheneaux,this.donnees_.projet_.getFichier());
		  fenetreProgression.miseAjourBarreProgression(5, "Ecriture des vitesses dans les tron�ons");

		  // vitesses des biefs
		  DParametresSinavi3.ecritDonneesVitessesAvalantBief(this.donnees_.params_,this.donnees_.projet_.getFichier());
		  DParametresSinavi3.ecritDonneesVitessesMontantBief(this.donnees_.params_,this.donnees_.projet_.getFichier());
			
		  fenetreProgression.miseAjourBarreProgression(6, "Ecriture des temps de manoeuvres dans les �cluses");

		  // temps de maneuvres ecluses
		  DParametresSinavi3.ecritDonneesDurManeuvreEntrantEcluse(this.donnees_.params_,this.donnees_.projet_.getFichier());
		  DParametresSinavi3.ecritDonneesDurManeuvreSortantEcluse(this.donnees_.params_,this.donnees_.projet_.getFichier());
			
		  fenetreProgression.miseAjourBarreProgression(7, "Ecriture des croisements dans les tron�ons");

		  
		  // d�lais remplissage SAS ecluse.
		  DParametresSinavi3.ecritDonneesRemplissageSAS(this.donnees_.getParams_(), this.donnees_.getProjet_().getFichier());
	      fenetreProgression.miseAjourBarreProgression(8, "Ecriture des d�lais de remplissage des SAS �cluses");
	      
		  // dur�es de croisement des biefs:
		  DParametresSinavi3
		  .ecritDonneesCroisementsBief(this.donnees_.params_.cheneaux,this.donnees_.projet_.getFichier());
    	
		  //trajets
		  fenetreProgression.miseAjourBarreProgression(8, "Ecriture des trajets");
		  Sinavi3DataSimulation.ecritDonneestrajets(donnees_.listeBateaux_, this.donnees_.projet_.getFichier());
		  
		 
		  
		  
		  
    } catch (final Exception ex) {

    }

    //-- ETAPE 2: lancement calcul noyau --//
    mp.setMessage("Ex�cution du calcul...");
    mp.setProgression(10);
    fenetreProgression.miseAjourBarreProgression(10, "Ex�cution du calcul", "");
    
    File fic=new File(donnees_.projet_.getFichier()+".his");
    
    if(this.MOCK_ENABLE) {
    	Sinavi3Mock mock = new Sinavi3Mock();
    	mock.mockGeneral(donnees_, donnees_.projet_.getFichier());
    } else {
    
	    if(fic.exists()){    	
	    	int confirmation = new BuDialogConfirmation(donnees_.application_.getApp(),
	    			Sinavi3Implementation.isSinavi_, "Un fichier historique existe d�j�. Il contient les r�sultats d'un pr�c�dent \ncalcul.\nSouhaitez-vous relancer le noyau de calcul afin de g�n�rer un fichier historique mis � jour?\nDans le cas contraire, Sinavi exploitera le fichier historique existant.").activate();
	
	    	if (confirmation == 0) {
	    		fenetreProgression.miseAjourBarreProgression(true);
	    		if(!ExecuterCommandeDepuisServeur(1,donnees_.projet_.getFichier()))
	    			return ;//si la procedure retourne une valeure false alors il y a eu erreur, par cons�quent on quitte.
	    	}
	    } else {
	    	//-- on execute obligatoirement le noyau de calcul car aucun fichier historique trouv�... --//
	    	fenetreProgression.miseAjourBarreProgression(true);
	    	if(!ExecuterCommandeDepuisServeur(1,donnees_.projet_.getFichier()))
				return ;//si la procedure retourne une valeure false alors il y a eu erreur, par cons�quent on quitte.
	    }
      
    }
    
    //-- ETAPE 2.5: Verification que le fichier historique a bien �t� cr�� --//
    fenetreProgression.miseAjourBarreProgression(false);
        fenetreProgression.miseAjourBarreProgression(65, "V�rification de la g�n�ration", "");
   	 
        
        File fichier=new File(donnees_.projet_.getFichier()+".his");
		if(!fichier.exists()){
			new BuDialogError(null,Sinavi3Implementation.isSinavi_,"Le fichier historique est introuvable.\nVeuillez relancer le calcul (menu \"Simulation\", bouton \"Calculer\")").activate();
			return;
		}
		
		File fichierOccup=new File(donnees_.projet_.getFichier()+".occup");
		if(!fichierOccup.exists()){
			new BuDialogError(null,Sinavi3Implementation.isSinavi_,"Le fichier d'occupations est introuvable.\nVeuillez relancer le calcul (menu \"Simulation\", bouton \"Calculer\")").activate();
			return;
      }

      //-- ETAPE 3: Exploitation des resultats --//
      mp.setMessage("Fin calcul du noyau, exploitation des r�sultats.");
      mp.setProgression(65);
      fenetreProgression.miseAjourBarreProgression(65, "Lecture des r�sultats", "");

      // lecture du ficheir resultats:
      lectureResultats(fenetreProgression, this.donnees_.projet_.getFichier(), this.donnees_);

      fenetreProgression.miseAjourBarreProgression(70, "Exploitation des r�sultats", "");
      calculExploitationResultats(getMainPanel(), fenetreProgression, donnees_);
      // on place le niveau a 2 puisque les donn�es ont �t� calcul�es.
      donnees_.params_.niveau = 2;
      // apres execution du calcul passage au niveau 2 pour acc�der aux r�sultats et activation des r�sultats
      activerCommandesResultatsSimulation();
      
      new BuDialogMessage(donnees_.application_.getApp(), donnees_.application_.getInformationsSoftware(),
              "La simulation est termin�e, vous pouvez visualiser les r�sultats.\nUn fichier historique \n" + donnees_.projet_.getFichier() + ".his a �t� cr��.\nCe fichier peut occuper un important espace disque.\nPensez � la supprimer (menu \"Simulation\") une fois votre travail termin�.").activate();

      /*
     * projet_.addResult( SiporResource.resultats, siporResults_.litResultatsSipor());
       */

 /*
     * mp.setProgression(0); // receptionResultats(true); setEnabledForAction("CALCULER", true);
     * activeActionsExploitation(); // setEnabledForAction("PASIMPRESSION" , true);
       */
      //-- fin de la fen�tre des t�ches --//
      fenetreProgression.terminaison();


  }

    public static void lectureResultats(Sinavi3ProgressFrame fenetreProgression, String path, Sinavi3DataSimulation data) {
        data.listeResultatsSimu_ = DResultatsSinavi3.litResultatsSinavi3(path + ".his");
        if (fenetreProgression != null) {
            fenetreProgression.miseAjourBarreProgression(67, "Lecture des r�sultats", "");
        }

        data.mapBassineeEcluses = new HashMap<Integer, Integer>();

        data.setListeGarages(new ArrayList<Sinavi3ResultatGarage>());
        data.listeResultatsOccupationsSimu_ = DResultatsSinavi3.litOccupationsSinavi3(
                path + ".occup", data.params_.ecluses.nbEcluses, data.getListeGarages());
        if (fenetreProgression != null) {
            fenetreProgression.miseAjourBarreProgression(69, "Lecture des r�sultats", "");
        }
        data.listeResultatsBassineesSimu_ = DResultatsSinavi3.litBassineesSinavi3(path + ".conso", data.mapBassineeEcluses);



  }

  /**
   * Methode qui realise les calculs de toutes les interfaces de sinavi.
   */
  public static void calculExploitationResultats(final BuMainPanel mp , final Sinavi3ProgressFrame fenetreProgression, final Sinavi3DataSimulation data) {

        data.params_.ResultatsCompletsSimulation = new SParametresResultatsCompletSimulation();
        data.params_.ResultatsCompletsSimulation.nombreIntervallesDistribution = 20;
        if (fenetreProgression != null) {
            fenetreProgression.miseAjourBarreProgression(70, "Exploitation de la g�n�ration des bateaux");
        }
        if (mp != null) {
            mp.setMessage("Exploitation de la g�n�ration des bateaux...");

            mp.setProgression(70);
        }
// Etape 1: calcul des generations de navires:
        Sinavi3AlgorithmeGenerationBateaux.calcul(data);
        if (fenetreProgression != null) {
            fenetreProgression.miseAjourBarreProgression(71, "Exploitation des occupations");
        }
        if (mp != null) {
            mp.setMessage("Exploitation des occupations...");
            mp.setProgression(71);
        }
        Sinavi3AlgorithmeOccupations.calculApresSimu(data);
        if (fenetreProgression != null) {
            fenetreProgression.miseAjourBarreProgression(72, "Exploitation des bassin�es");
        }
        if (mp != null) {
            mp.setMessage("Exploitation des bassin�es...");
            mp.setProgression(71);
        }
        Sinavi3AlgorithmeBassinees.calculApresSimu(data);

        //-- lecture des consommation eau --//
        data.listeResultatsConsommationEau_ = Sinavi3AlgorithmeBassinees.computeConsommationEauSinavi3(data.mapBassineeEcluses, data);

        if (fenetreProgression != null) {
            fenetreProgression.miseAjourBarreProgression(73, "Calcul des attentes g�n�rales par �l�ment et par cat�gorie");
        }
        if (mp != null) {
            mp.setMessage("Calcul des attentes g�n�rales par �l�ment et par cat�gorie...");
            mp.setProgression(73);
        }
        // etape 4: calcul des attentes g�n�rales par �l�ments et par cat�gories
        Sinavi3AlgorithmeAttentesGenerales.calculApresSimu(data);
        if (fenetreProgression != null) {
            fenetreProgression.miseAjourBarreProgression(76, "Calcul des dur�es de parcours");
        }
        if (mp != null) {
            mp.setMessage("Calcul des dur�es de parcours...");
            mp.setProgression(75);
        }
        // etape 5: Calcul de toutes les dur�es de parcours.
        Sinavi3AlgorithmeTOUTESDureesParcours.calcul(data, fenetreProgression);
        if (fenetreProgression != null) {
            fenetreProgression.miseAjourBarreProgression(85, "Calcul des attentes par trajet");
        }
        if (mp != null) {
            mp.setMessage("Calcul des attentes par trajet...");
            mp.setProgression(85);
        }
        // etape 6: calcul de la totalit� des attentes trajet
        Sinavi3AlgorithmeTOUTESAttentesTrajet.calcul(data, fenetreProgression);

        if (fenetreProgression != null) {
            fenetreProgression.miseAjourBarreProgression(90, "Calcul des consommation d'eau");
        }
        if (mp != null) {
            mp.setMessage("Calcul des consommation d'eau...");
            mp.setProgression(87);
        }
        data.params_.ResultatsCompletsSimulation.listeConsoEau = new Sinavi3ResultatConsoEau[data.getListeResultatsConsommationEau_().size()];
        for (int i = 0; i < data.getListeResultatsConsommationEau_().size(); i++) {
            data.params_.ResultatsCompletsSimulation.listeConsoEau[i] = data.getListeResultatsConsommationEau_().get(i);
        }
        if (fenetreProgression != null) {
            fenetreProgression.miseAjourBarreProgression(90, "Calcul des garage ecluses");
        }
        if (mp != null) {
            mp.setMessage("Calcul des garage ecluses");
            mp.setProgression(89);
        }
        Sinavi3AlgorithmeAttentesGenerales.computeGarages(data);

        // enregistrement des r�sultats de la simulation
        if (fenetreProgression != null) {
            fenetreProgression.miseAjourBarreProgression(95, "Enregistrement des r�sultats");
        }
        if (mp != null) {
            mp.setMessage("Enregistrement des r�sultats...");
            mp.setProgression(95);
        }
        if(!MOCK_ENABLE)
            data.enregistrer();

        data.listeResultatsSimu_ = null;
        data.listeResultatsOccupationsSimu_ = null;
        data.listeResultatsBassineesSimu_ = null;
        data.listeResultatsGarages = null;
        if (fenetreProgression != null) {
            fenetreProgression.miseAjourBarreProgression(100, "Simulation et exploitation termin�es", "");
        }
        if (mp != null) {
            mp.setMessage("Simulation et exploitation termin�es");
            mp.setProgression(100);
        }
      

    }

  // Change le projet en cours.
  protected void changerProjet(final String nomProjet) {
    projet_ =  projets_.get(nomProjet);
    
  }

  protected void creer() {

    this.donnees_.creer();
    
  }

  protected void ouvrir(String fichier) {

    donnees_.ouvrir(fichier);

    
    
    

  }

  protected void enregistrer() {

    // enregistrement des donn�es via la methode datasimualtion
    if (donnees_.projet_ != null) {
      donnees_.enregistrer();
      
    }
  }// fin methode enregistrer

  // Enregistrement du rappel des donn�es sous un nom choisi par l'utilisateur
  protected void enregistrerRappelDonneesSous() {
    final String[] ext = { "html", "htm" };
    final String nomFichier = MethodesUtiles.choisirFichierEnregistrement(ext, (Component) getApp());
    if (nomFichier != null) {
      MethodesUtiles.enregistrer(nomFichier, fRappelDonnees_.getHtmlSource(), getApp());
      fRappelDonnees_.putClientProperty("NomFichier", nomFichier);
    }
  }

  protected void enregistrerSous() {

    this.donnees_.enregistrerSous();
    
  }// fin methode enregistresous

  /**
   * Methode de fermeture du projet 2 methodes � g�rer: cas 1: aucun projet charg� cas 2: un projet charg�
   */
  protected void fermer() {

    // cas 1: projet inexistant:
    if (this.donnees_.projet_ == null) {
      return;
    }

    // cas 2: projet existant:
    final BuDialogConfirmation c = new BuDialogConfirmation(getApp(), getInformationsSoftware(),
        "Voulez-vous enregistrer la simulation\n" + this.donnees_.projet_.getFichier());
    if (c.activate() == JOptionPane.YES_OPTION) {
      enregistrer();
    }

    /*
     * //cas 2: projet existant: demande de sauvegarde avant de quitter final String nomFichier= projet_.getFichier();
     * String nomCourt; if (nomFichier.lastIndexOf(File.separator) == -1) { nomCourt= nomFichier; } else { nomCourt=
     * nomFichier.substring(nomFichier.lastIndexOf(File.separator)); } // Enregistrement �ventuel avant fermeture
     * final BuDialogConfirmation c= new BuDialogConfirmation( getApp(), getInformationsSoftware(), "Voulez-vous
     * enregistrer la simulation\n" + nomCourt); if (c.activate() == JOptionPane.YES_OPTION) { enregistrer(); }
     */
    /**
     * code permettant sois disant de g�rer une liste de projet mais pas au point du tout et compl�tement foireux
     * abandon de cet id�e pour le moment reprise une fois le logiciel fonctionnant proprement et sans bug pour le
     * moment: on ne peut g�rer qu'un seul projet
     */
    /*
     * final BuDesktop dk= getMainPanel().getDesktop(); liste_.enleveSimulation(nomFichier); final FudaaProjet ancien=
     * projet_; if (projets_.size() > 0) { liste_.setSelectedIndex(0); ancien.fermer(); activeActionsExploitation();
     * return; } ancien.fermer(); // Fermeture de fp_ if (fp_ != null) { try { fp_.setClosed(true);
     * fp_.setSelected(false); } catch (final PropertyVetoException e) {} dk.removeInternalFrame(fp_); fp_= null; } //
     * Fermeture de fRappelDonnees_ if (fRappelDonnees_ != null) { try { fRappelDonnees_.setClosed(true);
     * fRappelDonnees_.setSelected(false); } catch (final PropertyVetoException e) {}
     * dk.removeInternalFrame(fRappelDonnees_); fRappelDonnees_= null; } // Fermeture de fStatistiquesFinales_ if
     * (fStatistiquesFinales_ != null) { try { fStatistiquesFinales_.setClosed(true);
     * fStatistiquesFinales_.setSelected(false); } catch (final PropertyVetoException e) {}
     * dk.removeInternalFrame(fStatistiquesFinales_); fStatistiquesFinales_= null; } // Fermeture de fResultats_ if
     * (fResultats_ != null) { try { fResultats_.setClosed(true); fResultats_.setSelected(false); } catch (final
     * PropertyVetoException ex) {} dk.removeInternalFrame(fResultats_); fResultats_= null; } // Fermeture de
     * fResultatsSimulations_ if (fResultatsSimulations_ != null) { try { fResultatsSimulations_.setClosed(true);
     * fResultatsSimulations_.setSelected(false); } catch (final PropertyVetoException ex) {}
     * dk.removeInternalFrame(fResultatsSimulations_); fResultatsSimulations_= null; } dk.repaint();
     */
    setEnabledForAction("PARAMETRE", false);
    setEnabledForAction("TEXTE", false);
    setEnabledForAction("FERMER", false);
    setEnabledForAction("ENREGISTRER", false);
    setEnabledForAction("ENREGISTRERSOUS", false);
    setEnabledForAction("CALCULER", false);
    setEnabledForAction("PARAMETRE", false);
    activeActionsExploitation();
    setTitle("sinavi");
  }// fni methode fermer

  protected void quitter() {
    // si un projet pr�sent
    final Object[] liste = projets_.keySet().toArray();
    if (liste.length != 0) {
      // demande si enregistrer le projet
      final BuDialogConfirmation c = new BuDialogConfirmation(getApp(), getInformationsSoftware(),
          "Voulez-vous enregistrer le projet actuel\n");
      if (c.activate() == JOptionPane.YES_OPTION) {
        liste_.enregistre();
      }
      // Ferme les simulations ouvertes.
      for (int i = 0; i < liste.length; i++) {
        changerProjet((String) liste[i]);
        fermer();
      }
    }
    exit();// permet de fermer la fenetre???
  }

  protected void importerProjet() {
  /*
   * if( !projet_.estConfigure() ) { projet_.fermer(); // si ouverture de projet echouee setEnabledForAction("PARAMETRE" ,
   * false); setEnabledForAction("RAPPORT" , false); setEnabledForAction("IMPORTVAG" , false);
   * setEnabledForAction("EXPORTER" , false); setEnabledForAction("FERMER" , false); setEnabledForAction("ENREGISTRER" ,
   * false); setEnabledForAction("ENREGISTRERSOUS", false); setEnabledForAction("CALCULER" , false);
   * setEnabledForAction("PASIMPRESSION" , false); setEnabledForAction("ORTHOGONALE" , false); } else { if( fp_!=null )
   * fp_.updatePanels(projet_); new BuDialogMessage(getApp(), isVag_, "Param�tres charg�s").activate();
   * setEnabledForAction("PARAMETRE" , true); setEnabledForAction("RAPPORT" , true); setEnabledForAction("IMPORTVAG" ,
   * true); setEnabledForAction("EXPORTER" , true); setEnabledForAction("FERMER" , true);
   * setEnabledForAction("ENREGISTRER" , true); setEnabledForAction("ENREGISTRERSOUS", true); if( connecte() ) {
   * setEnabledForAction("CALCULER" , true); } else { setEnabledForAction("CALCULER" , false); }
   * setEnabledForAction("ORTHOGONALE" , false); setTitle(projet_.getFichier()); }
   */
  }

  protected void importer(final String _vagId) {
  /*
   * VagImport vi=new VagImport(getApp()); if( _vagId!=null ) vi.onlyEnable(_vagId); vi.show(); String tmp;
   * tmp=(String)vi.getImport(VagResource.VAG01); if( tmp!=null ) projet_.addImport(VagResource.VAG01, tmp);
   * tmp=(String)vi.getImport(VagResource.VAG02); if( tmp!=null ) projet_.addImport(VagResource.VAG02, tmp);
   * tmp=(String)vi.getImport(VagResource.VAG03); if( tmp!=null ) projet_.addImport(VagResource.VAG03, tmp);
   * tmp=(String)vi.getImport(VagResource.VAG04); if( tmp!=null ) projet_.addImport(VagResource.VAG04, tmp); if(
   * fp_!=null ) fp_.updatePanels(projet_); setEnabledForAction("CALCULER" , true);
   */
  }

  /** Cr�ation ou affichage de la fentre pour les pr�f�rences. */
  protected void buildPreferences(final List _prefs) {
    _prefs.add(new BuBrowserPreferencesPanel(this));
    _prefs.add(new BuDesktopPreferencesPanel(this));
    _prefs.add(new BuLanguagePreferencesPanel(this));
    _prefs.add(new Sinavi3PreferencesPanel());
    //_prefs.add(new BuUserPreferencesPanel(this));
    //_prefs.add(new BuBrowserPreferencesPanel(this));
    _prefs.add(new FudaaStartupExitPreferencesPanel(true));
    //_prefs.add(new BuLanguagePreferencesPanel(this));
    _prefs.add(new BuLookPreferencesPanel(this));
    //_prefs.add(new TrChainePreferencePanel());
  }

  /**
   * ************************************************************************* METHODE DE DECLENCHEMENT DES FENETRES DE
   * SAISIES DE DONNEES *************************************************************************
   */

  /**
   * Methode de creation d'une fenetre interne de saisie des donnees generales dans l application principale
   */

  void parametreDonneesGenerales() {
    if (gestionDonneesGenerales_ == null) {
      FuLog.debug("interface nulle");
      gestionDonneesGenerales_ = new Sinavi3FrameSaisieDonneesGenerales(this.donnees_);

      gestionDonneesGenerales_.setVisible(true);
      addInternalFrame(gestionDonneesGenerales_);
    } else {
      FuLog.debug("interface r�duite");// il ne faut pas la recr��er
      if (gestionDonneesGenerales_.isClosed()) {
        addInternalFrame(gestionDonneesGenerales_);

      } else {
        FuLog.debug("interface cas de figur restant autre que null et fermeture");
        activateInternalFrame(gestionDonneesGenerales_);
        addInternalFrame(gestionDonneesGenerales_);

      }

    }
    
    
  }

  
  /**
   * Methode de cration d une fenetre interne de saisie d'Ecluses dans l application principale
   */

  void parametreEcluse() {
    if (gestionEcluses_ == null) {
      FuLog.debug("interface nulle");
      gestionEcluses_ = new Sinavi3VisualiserEcluses(this.donnees_);
      // System.out.println("55555");
      gestionEcluses_.setVisible(true);
      addInternalFrame(gestionEcluses_);
    } else {
      FuLog.debug("interface ferm�e");
      if (gestionEcluses_.isClosed()) {
        gestionEcluses_.dispose();
        gestionEcluses_ = new Sinavi3VisualiserEcluses(this.donnees_);
        addInternalFrame(gestionEcluses_);

      } else {
        FuLog.debug("interface cas de figur restant autre que null et fermeture");
        activateInternalFrame(gestionEcluses_);
        gestionEcluses_.dispose();
        gestionEcluses_ = new Sinavi3VisualiserEcluses(this.donnees_);
        addInternalFrame(gestionEcluses_);

      }

    }
    

    // addInternalFrame(new SiporVisualiserEcluses(donnees_));
  }

  /**
   * Methode de cration d une fenetre interne de saisie de bassins dans l application principale
   */

 
  /**
   * Methode de cration d une fenetre interne de saisie de gares dans l application principale
   */

  void parametreGare() {
    if (gestionGares_ == null) {
      FuLog.debug("interface nulle");
      gestionGares_ = new Sinavi3VisualiserGares(this.donnees_);
      // System.out.println("55555");
      gestionGares_.setVisible(true);
      gestionGares_.affichagePanel_.maj(donnees_);
      addInternalFrame(gestionGares_);
    } else {
      FuLog.debug("interface ferm�e");
      if (gestionGares_.isClosed()) {
        gestionGares_.affichagePanel_.maj(donnees_);
        addInternalFrame(gestionGares_);

      } else {
        FuLog.debug("interface cas de figur restant autre que null et fermeture");
        gestionGares_.affichagePanel_.maj(donnees_);
        activateInternalFrame(gestionGares_);
        addInternalFrame(gestionGares_);

      }

    }
    // addInternalFrame(new SiporVisualiserGares(donnees_));
  }

  /**
   * Methode de cration d une fenetre interne de saisie de cheneaux dans l application principale
   */

  void parametreBief() {

    if (gestionChenaux_ == null) {
      FuLog.debug("interface nulle");
      gestionChenaux_ = new Sinavi3VisualiserBief(this.donnees_);
      // System.out.println("55555");
      gestionChenaux_.setVisible(true);
      addInternalFrame(gestionChenaux_);
    } else {
      FuLog.debug("interface ferm�e");
      if (gestionChenaux_.isClosed()) {
        addInternalFrame(gestionChenaux_);

      } else {
        FuLog.debug("interface cas de figur restant autre que null et fermeture");
        activateInternalFrame(gestionChenaux_);
        addInternalFrame(gestionChenaux_);

      }

    }
    
    // addInternalFrame(new SiporVisualiserChenal(donnees_));
  }

  
  /**
   * Methode de creation d une fenetre interne de saisie des cat�gories de navire dans l application principale
   */

  void parametreNavire() {
    if (gestionNavires_ == null) {
      FuLog.debug("interface nulle");
      gestionNavires_ = new Sinavi3VisualiserBateaux(this.donnees_);
      // System.out.println("55555");
      gestionNavires_.setVisible(true);

      addInternalFrame(gestionNavires_);
    } else {
      FuLog.debug("interface ferm�e");
      if (gestionNavires_.isClosed()) {
        gestionNavires_.dispose();
        gestionNavires_ = new Sinavi3VisualiserBateaux(this.donnees_);
        addInternalFrame(gestionNavires_);

      } else {
        FuLog.debug("interface cas de figur restant autre que null et fermeture");
        activateInternalFrame(gestionNavires_);
        gestionNavires_.dispose();
        gestionNavires_ = new Sinavi3VisualiserBateaux(this.donnees_);

        addInternalFrame(gestionNavires_);

      }

    }

    
    // addInternalFrame(new SiporVisualiserNavires(donnees_));
  }

  
  void activationRappelDonnees() {

    if (rappelDonnees_ == null) {
      FuLog.debug("interface nulle");
      rappelDonnees_ = new Sinavi3FrameGenerationRappelDonnees(this.donnees_);
      // System.out.println("55555");
      rappelDonnees_.setVisible(true);
      addInternalFrame(rappelDonnees_);
    } else {
      FuLog.debug("interface ferm�e");
      if (rappelDonnees_.isClosed()) {
        addInternalFrame(rappelDonnees_);

      } else {
        FuLog.debug("interface cas de figur restant autre que null et fermeture");
        activateInternalFrame(rappelDonnees_);
        addInternalFrame(rappelDonnees_);

      }

    }

    // addInternalFrame(new SiporFrameSaisieMaree(donnees_));
  }

  /*********************************************************************************************************************
   * ****************************************************************************************************
   * ************************** Activation des Interfaces de sorties *********************************
   * ****************************************************************************************************
   ********************************************************************************************************************/

  /**
   * methode qui active le lancement de l'interface de visualisation des parametres r�sultats de la g�n�ration de
   * navires
   */
  public void activerResultatsgenerationNavires() {

    if (this.donnees_.params_.ResultatsCompletsSimulation.ResultatsGenerationNavires == null) {
      // le fichier de donn�es de la simulation a �t� supprim� ou alors les donn�es n'ont pas �t� lues
      new BuDialogError(donnees_.application_.getApp(), donnees_.application_.getInformationsSoftware(),
          "La simulation doit �tre ex�cut�e afin de permettre l'exploitation des\nr�sultats.\nVous pouvez ex�cuter la simulation via la menu \"Simulation\".")
          .activate();

      return;
    }
    addInternalFrame(new Sinavi3ResultatGenerationBateaux(donnees_));

  }

  public void activerResultatHistorique() {
    if (this.donnees_.listeResultatsSimu_ == null) {
      // le fichier de donn�es de la simulation a �t� supprim� ou alors les donn�es n'ont pas �t� lues
      new BuDialogError(donnees_.application_.getApp(), donnees_.application_.getInformationsSoftware(),
      "La simulation doit �tre ex�cut�e afin de permettre l'exploitation des\nr�sultats.\nVous pouvez ex�cuter la simulation via la menu \"Simulation\".")
          .activate();

      return;
    }
    addInternalFrame(new Sinavi3ResultatHistorique(donnees_));

  }

  public void activerResultatOccupations() {
	    if (this.donnees_.params_.ResultatsCompletsSimulation.Occupations == null) {
	      // le fichier de donn�es de la simulation a �t� supprim� ou alors les donn�es n'ont pas �t� lues
	      new BuDialogError(donnees_.application_.getApp(), donnees_.application_.getInformationsSoftware(),
	      "La simulation doit �tre ex�cut�e afin de permettre l'exploitation des\nr�sultats.\nVous pouvez ex�cuter la simulation via la menu \"Simulation\".")
	          .activate();

	      return;
	    }
	    addInternalFrame(new Sinavi3ResultatsOccupations(donnees_));
	  }
  
  public void activerResultatDureesParcours() {
    if (this.donnees_.params_.ResultatsCompletsSimulation.TOUTEDureesParoucrs == null) {
      // le fichier de donn�es de la simulation a �t� supprim� ou alors les donn�es n'ont pas �t� lues
      new BuDialogError(donnees_.application_.getApp(), donnees_.application_.getInformationsSoftware(),
      "La simulation doit �tre ex�cut�e afin de permettre l'exploitation des\nr�sultats.\nVous pouvez ex�cuter la simulation via la menu \"Simulation\".")
          .activate();

      return;
    }
    addInternalFrame(new Sinavi3ResultatsDureesParcours(donnees_));
  }

public void activerResultatGarage() {
	  
	  if (this.donnees_.listeResultatsGarages == null || this.donnees_.listeResultatsGarages.size() ==0) {

		  if(donnees_.params_!= null && donnees_.params_.ResultatsCompletsSimulation != null &&
				  donnees_.params_.ResultatsCompletsSimulation.listeGarageEcluse != null) {
			  this.donnees_.listeResultatsGarages  = new ArrayList<Sinavi3ResultatGarage>();
			  for(int i=0;i<donnees_.params_.ResultatsCompletsSimulation.listeGarageEcluse.length;i++)
				  this.donnees_.listeResultatsGarages.add(donnees_.params_.ResultatsCompletsSimulation.listeGarageEcluse[i]);

		  }
	  }
	  if (this.donnees_.listeResultatsGarages  == null) {
		  // le fichier de donn�es de la simulation a �t� supprim� ou alors les donn�es n'ont pas �t� lues
		  new BuDialogError(donnees_.application_.getApp(), donnees_.application_.getInformationsSoftware(),
				  "La simulation doit �tre ex�cut�e afin de permettre l'exploitation des\nr�sultats.\nVous pouvez ex�cuter la simulation via la menu \"Simulation\".")
		  .activate();

		  return;
	  }
	  addInternalFrame(new Sinavi3ResultatsGarage(donnees_));
  }
  
  public void activerResultatConsommationEau() {
	  
	  if (this.donnees_.listeResultatsConsommationEau_ == null || this.donnees_.listeResultatsConsommationEau_.size() ==0) {

		  if(donnees_.params_!= null && donnees_.params_.ResultatsCompletsSimulation != null &&
				  donnees_.params_.ResultatsCompletsSimulation.listeConsoEau != null) {
			  this.donnees_.listeResultatsConsommationEau_  = new ArrayList<Sinavi3ResultatConsoEau>();
			  for(int i=0;i<donnees_.params_.ResultatsCompletsSimulation.listeConsoEau.length;i++)
				  this.donnees_.listeResultatsConsommationEau_.add(donnees_.params_.ResultatsCompletsSimulation.listeConsoEau[i]);

		  }
	  }
	  if (this.donnees_.listeResultatsConsommationEau_  == null) {
		  // le fichier de donn�es de la simulation a �t� supprim� ou alors les donn�es n'ont pas �t� lues
		  new BuDialogError(donnees_.application_.getApp(), donnees_.application_.getInformationsSoftware(),
				  "La simulation doit �tre ex�cut�e afin de permettre l'exploitation des\nr�sultats.\nVous pouvez ex�cuter la simulation via la menu \"Simulation\".")
		  .activate();

		  return;
	  }
	  addInternalFrame(new Sinavi3ResultatsConsommationEau(donnees_));
  }
  

  public void activerResultatCroisementsBiefs() {
    if (this.donnees_.listeResultatsSimu_ == null) {
      // le fichier de donn�es de la simulation a �t� supprim� ou alors les donn�es n'ont pas �t� lues
      new BuDialogError(donnees_.application_.getApp(), donnees_.application_.getInformationsSoftware(),
      "La simulation doit �tre ex�cut�e afin de permettre l'exploitation des\nr�sultats.\nVous pouvez ex�cuter la simulation via la menu \"Simulation\".")
          .activate();

      return;
    }
    addInternalFrame(new Sinavi3ResultatsCroisementsBief(donnees_));
  }

  public void activerResultatTrematagesBiefs() {
	    if (this.donnees_.listeResultatsSimu_ == null) {
	      // le fichier de donn�es de la simulation a �t� supprim� ou alors les donn�es n'ont pas �t� lues
	      new BuDialogError(donnees_.application_.getApp(), donnees_.application_.getInformationsSoftware(),
	      "La simulation doit �tre ex�cut�e afin de permettre l'exploitation des\nr�sultats.\nVous pouvez ex�cuter la simulation via la menu \"Simulation\".")
	          .activate();

	      return;
	    }
	    addInternalFrame(new Sinavi3ResultatsTrematagesBief(donnees_));
	  }
 

  public void activerResultatAttentesGeneralesElement() {
    if (this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories == null) {
      // le fichier de donn�es de la simulation a �t� supprim� ou alors les donn�es n'ont pas �t� lues
      new BuDialogError(donnees_.application_.getApp(), donnees_.application_.getInformationsSoftware(),
      "La simulation doit �tre ex�cut�e afin de permettre l'exploitation des\nr�sultats.\nVous pouvez ex�cuter la simulation via la menu \"Simulation\".")
          .activate();

      return;
    }
    addInternalFrame(new Sinavi3ResultatsAttenteGeneraleElement(donnees_));
  }

  public void activerResultatAttentesGeneralesCategorie() {
    if (this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories == null) {
      // le fichier de donn�es de la simulation a �t� supprim� ou alors les donn�es n'ont pas �t� lues
      new BuDialogError(donnees_.application_.getApp(), donnees_.application_.getInformationsSoftware(),
      "La simulation doit �tre ex�cut�e afin de permettre l'exploitation des\nr�sultats.\nVous pouvez ex�cuter la simulation via la menu \"Simulation\".")
          .activate();

      return;
    }
    addInternalFrame(new Sinavi3ResultatsAttenteGeneraleCategories(donnees_));
  }

  public void activerResultatAttentesSpecialisee() {
    if (this.donnees_.params_.ResultatsCompletsSimulation.TOUTESAttenteTrajet == null) {
      // le fichier de donn�es de la simulation a �t� supprim� ou alors les donn�es n'ont pas �t� lues
      new BuDialogError(donnees_.application_.getApp(), donnees_.application_.getInformationsSoftware(),
      "La simulation doit �tre ex�cut�e afin de permettre l'exploitation des\nr�sultats.\nVous pouvez ex�cuter la simulation via la menu \"Simulation\".")
          .activate();

      return;
    }
    addInternalFrame(new Sinavi3ResultatsAttenteTrajet(donnees_));
  }

 

  

  public void activerComparaisonNombreNavires() {
    addInternalFrame(new Sinavi3ResultatComparaisonGenerationBateaux(donnees_));
  }

 
  public void activerComparaisonDureesParcours() {
    addInternalFrame(new Sinavi3ResultatComparaisonDureeParcours(donnees_));
  }

 
  
  public void activerComparaisonAttenteGenerale(){
	  addInternalFrame(new Sinavi3ResultatComparaisonAttenteElement(donnees_));
  }
  
  public void activerComparaisonAttenteTrajet(){
	  addInternalFrame(new Sinavi3ResultatComparaisonAttentetrajet(donnees_));
  }

  /**
   * methode de fermeture de toutes les interfaces
   */

  void fermerToutesLesInterfaces() {

   
    if (gestionNavires_ != null) {
      gestionNavires_.dispose();
    }
    if (gestionChenaux_ != null) {
      gestionChenaux_.dispose();
    }
    
    if (gestionGares_ != null) {
      gestionGares_.dispose();
    }
    
    if (gestionEcluses_ != null) {
      gestionEcluses_.dispose();
    }
    
    if (gestionDonneesGenerales_ != null) {
      gestionDonneesGenerales_.dispose();
    }
    
    if (gestionTopoEcluses_ != null) {
      gestionTopoEcluses_.dispose();
    }
    if (gestionTopoChenaux_ != null) {
      gestionTopoChenaux_.dispose();
    }
    
    if (gestionModelisation_ != null) {
      gestionModelisation_.dispose();
    }
    if (gestionCroisementBief_ != null) {
      gestionCroisementBief_.dispose();
    }
    
    if (gestionTrematageBief_ != null) {
        gestionTrematageBief_.dispose();
      }
    
    if (gestionVitessesBief_ != null) {
      gestionVitessesBief_.dispose();
    }
    if(gestionDureeManeuvreEcluse_!=null){
    	gestionDureeManeuvreEcluse_.dispose();
    }
    
    if (gestionTrajetsBateaux_ != null) {
        gestionTrajetsBateaux_.dispose();
      }
    if (gestionRemplissageSAS_ != null) {
    	gestionRemplissageSAS_.dispose();
      }
  }

  void initialiserToutesLesInterfaces() {

    // etape 1: on ferme toutes les interfaces
    fermerToutesLesInterfaces();

    // etape 2: on remet a null toutes les interfaces .
    
    gestionNavires_ = null;
    gestionChenaux_ = null;
    gestionGares_ = null;
    gestionEcluses_ = null;
    gestionDonneesGenerales_ = null;
    gestionTopoEcluses_ = null;
    gestionTopoChenaux_ = null;
    gestionModelisation_ = null;
    gestionCroisementBief_ = null;
    gestionTrematageBief_=null;
    gestionVitessesBief_ = null;
    gestionDureeManeuvreEcluse_=null;
    gestionTrajetsBateaux_=null;
    gestionRemplissageSAS_ = null;

  }

  // Ajoute une fenetre.
  protected BuInternalFrame creerFenetreInterne() {
    final BuInternalFrame frame = new BuInternalFrame("Graphe", true, true, true, true);
    frame.setBackground(Color.white);
    frame.getContentPane().setLayout(new BorderLayout());
    frame.setBounds(200, 200, 200, 200);
    frame.setSize(400, 400);
    frame.setVisible(true);
    addInternalFrame(frame);
    return frame;
  }

  /*--- methode de controle des parametres ---*/

  protected void activeActionsExploitation() {
    Sinavi3OutilsDonnees simulation = null;
    // autorise l'exploitation de la simulation si projet_ contient des r�sultats
    simulation = new Sinavi3OutilsDonnees(projet_);
    if (simulation.getResultats() != null) {
      setEnabledForAction("TABLEAU", true);
    } else {
      setEnabledForAction("TABLEAU", false);
    }
    // autorise l'exploitation du projet si au moins une simulation avec des r�sultats
    final Object[] simulations = projets_.keySet().toArray();
    for (int i = 0; i < simulations.length; i++) {
      simulation = new Sinavi3OutilsDonnees((FudaaProjet) projets_.get(simulations[i]));
      if (simulation.getResultats() != null) {
        setEnabledForAction("GRAPHE", true);
        return;
      }
    }
    // sinon pas de simulation avec des r�sultats
    setEnabledForAction("GRAPHE", false);
  }

  public void exit() {
    fermer();
    super.exit();
  }

  public boolean isCloseFrameMode() {
    return false;
  }

  /**
   * @see org.fudaa.fudaa.commun.dodico.FudaaImplementation#clearVariables()
   */
  protected void clearVariables() {
    CONNEXION_SINAVI = null;
    SERVEUR_SINAVI = null;
  }

  /**
   * @see org.fudaa.fudaa.commun.dodico.FudaaImplementation#getTacheConnexionMap()
   */
  protected FudaaDodicoTacheConnexion[] getTacheConnexionMap() {
    final FudaaDodicoTacheConnexion c = new FudaaDodicoTacheConnexion(SERVEUR_SINAVI, CONNEXION_SINAVI);
    return new FudaaDodicoTacheConnexion[] { c };
  }

  /**
   * @see org.fudaa.fudaa.commun.dodico.FudaaImplementation#getTacheDelegateClass()
   */
  protected Class[] getTacheDelegateClass() {
    return new Class[] { DCalculSinavi3.class };
  }

  /**
   * @see org.fudaa.fudaa.commun.dodico.FudaaImplementation#initConnexions(java.util.Map)
   */
  protected void initConnexions(final Map _r) {
    final FudaaDodicoTacheConnexion c = (FudaaDodicoTacheConnexion) _r.get(DCalculSinavi3.class);
    CONNEXION_SINAVI = c.getConnexion();
    SERVEUR_SINAVI = ICalculSinavi3Helper.narrow(c.getTache());
  }

  /**
   * @see org.fudaa.fudaa.commun.impl.FudaaCommonImplementation#getApplicationPreferences()
   */
  public BuPreferences getApplicationPreferences() {
    return Sinavi3Preferences.SINAVI;
  }
  
  
  public FudaaProjet[] getSimulations() {
     int cpt=0;
      FudaaProjet[] projets = new FudaaProjet[projets_.values().size()];
      for(FudaaProjet f: projets_.values()) {
          projets[cpt++] = f;
      }
      return projets;
  }
  
  /**
   * Methode de suppression d'une simulation parmi les simulations ouvertes.
   *
   */
  public void supprimerSimulation(){
	  
	  //-- cas particulier: 1 seule simulation pr�sente: on propose de quitter le logiciel --//
	  if(liste_.getModel().getSize()<=1){
		  exit();
		  return ;
	  }
	  
	  //-- Recuperation du projet Fudaa selectionn� --//
	  String nomProjet=(String)liste_.getSelectedValue();
	  FudaaProjet deleteProject= (FudaaProjet)projets_.get(liste_.getSelectedValue());
	  int indiceProjetSupprime=liste_.getSelectedIndex();
	  
	  //-- Test: si le projet supprim� est le projet en cours: on recharge un autre projet --//
	  if(deleteProject==donnees_.projet_)
	  {
		  if(indiceProjetSupprime!=0)
		  //position 0 -> premiere simulation
		  liste_.setSelectedIndex(0);
		  else
			  liste_.setSelectedIndex(1);
		  // changement de simulation du cot� m�tier:
		  donnees_.changerProjet((FudaaProjet)projets_.get(liste_.getSelectedValue()));
	  }
	  
	  //-- On supprime le projet selectionn� de la liste et de la hashTable --//
	  System.out.println("\nl'indice a supprimer est "+indiceProjetSupprime);
	   System.out.println("la taille du modele est "+liste_.getModel().getSize());
	  System.out.println("\nl'indice a selectionne est "+liste_.getSelectedIndex());
	  ((DefaultListModel)liste_.getModel()).remove(indiceProjetSupprime);
	  projets_.remove(nomProjet);
	  
	  liste_.revalidate();
	  
  }
  
  
  /**
   * Methode de suppression du fichier historique.
   * Methode importante car le fichier est souvent tr�s volumineux et les r�sultats sont sauvegard� donc historique inutile.
   *
   */
  public void supprimerHistoriqueSimulation(){
	  
	  //-- path du fichier --//
	  String path= donnees_.projet_.getFichier()+".his";
	  
	  File historique=new File(path);
	  
	  BuDialogConfirmation mess=new BuDialogConfirmation(this,isSinavi_,"Voulez-vous vraiment supprimer le fichier historique?");
	  final int confirmation=mess.activate();
	  if(confirmation==0)
	  {
		  boolean reussite=historique.delete();
		  
		  if(!reussite)
			  new BuDialogError(this.getApp(),isSinavi_,"Le fichier historique n'a pas pu �tre supprim�.").activate();
		  else
			  new BuDialogMessage(this.getApp(),isSinavi_,"Le fichier historique a �t� correctement supprim�.").activate();
	  }
	  
  }
  

  
  /**
   * Methode qui renseigne sur le chenmin vers les serveurs sipor.
   * @return
   */
  protected final String cheminServeur() {
	    String path = System.getProperty("FUDAA_SERVEUR");
	    if ((path == null) || path.equals("")) {
	      path = System.getProperty("user.dir") + File.separator + "serveurs"
	          + File.separator + "Fudaa-Sinavi";
	    }
	    if (!path.endsWith(File.separator)) {
	      path += File.separator;
	    }
	    return path;
	  }

	protected PrintStream streamVersionNoyauCalcul = null;
	public String versionNoyauCalcul = null;

	/**
	 * Methode qui extraie le numero de version a partir du flux du noyau.
	 */
	public void extractVersion() {
            
                                  
                                    if(versionNoyauCalcul.indexOf("\r\n") !=-1) 
                                        versionNoyauCalcul = versionNoyauCalcul.substring(0, versionNoyauCalcul.indexOf("\r\n") );
                                    else 
                                          if(versionNoyauCalcul.indexOf("\n") !=-1) 
                                        versionNoyauCalcul = versionNoyauCalcul.substring(0, versionNoyauCalcul.indexOf("\n") );
                                   
                                    else
                                    if(versionNoyauCalcul.indexOf("\r\t") !=-1) 
                                        versionNoyauCalcul = versionNoyauCalcul.substring(0, versionNoyauCalcul.indexOf("\r\t") );
                                        else
                                    if(versionNoyauCalcul.indexOf("\r") !=-1) 
                                        versionNoyauCalcul = versionNoyauCalcul.substring(0, versionNoyauCalcul.indexOf("\r") );
                                    
                                    else {
		String res = versionNoyauCalcul;

		String[] result = res.split(" ");
		if (result.length > 1) {
			versionNoyauCalcul = result[2];

		}
                                    }
	}

  /**
   * Recupere le numero de version du noyau de calcul et l'ins�re dans les donn�es de sinavi IHM.
   * @author Adrien Hadoux
   */
	public void setVersionNoyauCalcul() {
		BufferedReader reader = null;
		File tempVersion = null;
		try {
			tempVersion = File.createTempFile("version", "noyau");

			tempVersion.deleteOnExit();

			streamVersionNoyauCalcul = new PrintStream(tempVersion);
			if(MOCK_ENABLE) {
				versionNoyauCalcul ="1.1.2";
				System.out.println("***************\n* Noyau: "
						+ versionNoyauCalcul+"\n***************\n");
			}else
			if ( ExecuterCommandeDepuisServeur(2, "")) {

				reader = new BufferedReader(new FileReader(tempVersion));
				String line = null;
				while ((line = reader.readLine()) != null) {
					versionNoyauCalcul += line;
				}
				extractVersion();
				System.out.println("***************\n* Noyau: "
						+ versionNoyauCalcul+"\n***************\n");
			} else {
				throw new Exception(
						"erreur lors de l'execution du noyau de calcul.... V�rifiez son emplacement");
			}
		} catch (Exception e) {
			e.printStackTrace();			
		} finally {
			try {
				if (reader != null)

					reader.close();

				if (tempVersion != null)
					tempVersion.delete();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

	}

 /**
  * Methode qui permet d'executer une comande via la classe CExec. 
  * @param progFortran programme executable a lancer 
  * 0= SiporGenarr
  * 1= noyau de calcul 
  * 2 = version du noyau de calcul
  * @param nomEtude nom de l'etude sipor
  */
 protected boolean ExecuterCommandeDepuisServeur(int progFortran,String nomEtude){
	 
	 final String os = System.getProperty("os.name");
	    final String path = cheminServeur();
	    int graineEnCours= 0;
	    int dureeEnCours =0;
	    if(progFortran != 2) {
	    final SParametresSinavi32 par = (SParametresSinavi32)projet_.getParam("parametres");
		    graineEnCours=par.donneesGenerales.graine;
		    dureeEnCours = par.donneesGenerales.nombreJours;
	    }
	    System.out.println("**\nLe chemin des serveurs est:\n "+path+" \n**");
	    try {
	      String[] cmd;
	      
	      if (os.startsWith("Windows")) {
	    	  if(progFortran == 2)
	    		  cmd = new String[1];
	    	  else
	    		  cmd = new String[4];
	    	  if(progFortran==0)
	    		  cmd[0] = path + "bin" + "\\win\\sinaviGenarr_win.exe";
	    	  else
	    		  cmd[0] = path + "bin" + "\\win\\sinavi_win.exe";

	    	  //  cmd[1] = path + "bin" + "\\win\\";
	    	  if(progFortran != 2) {
		    	  cmd[1] = nomEtude;
		    	  cmd[2]= Integer.toString(dureeEnCours);
		    	  cmd[3]= Integer.toString(graineEnCours);
	    	  }
	      } else {
	    	  // System.out.println("* yo 2 *");
	    	  if(progFortran == 2)
	    		  cmd = new String[1];
	    	  else
	    		  cmd = new String[4];
	    	  if(progFortran==0)
	    		  cmd[0] = path + "bin" + "/linux/sinaviGenarr_linux.x";
	    	  else
	    		  cmd[0] = path + "bin" + "/linux/sinavi_linux.x";
	    	  //cmd[1] = path + "bin" + "/linux/";
	    	  if(progFortran != 2) {
		    	  cmd[1] = nomEtude;
		    	  cmd[2]= Integer.toString(dureeEnCours);
		    	  cmd[3]= Integer.toString(graineEnCours);
		    	  
		    	  System.out.println("**\nLa commande ex�cut�e est: \n "+cmd[0] + " " + cmd[1] +"\n**");

	    	  } 
	      }
	      

	      try {
	    	  final CExec ex = new CExec();
	    	  ex.setCommand(cmd);
	    	  if(progFortran == 2)
	    		  ex.setOutStream(streamVersionNoyauCalcul);
	    	  else
	    		  ex.setOutStream(System.out);
	    	  ex.setErrStream(System.err);
	    	  ex.exec();
	      }
	      catch (final Throwable _e1) {
	    	  System.out.println("Erreur rencontr�e lors de l'execution du code de calcul");
	    	  _e1.printStackTrace();
	    	  new BuDialogError(this.getApp(),isSinavi_,"Erreur rencontr�e lors de l'execution du code de calcul").activate();
	    	  return false;
	      }
	     
	    }
	    catch (final Exception ex) {
	    	System.out.println("Erreur lors de l'execution du code de calcul");
	    	if(progFortran==0)
	    		new BuDialogMessage(this.getApp(),isSinavi_,"Impossible d'executer le g�n�rateur de bateaux Genarr").activate();
	    	else
	    		new BuDialogMessage(this.getApp(),isSinavi_,"Impossible d'executer le noyau de calcul Sinavi").activate();
	    	return false;
	    }

	    return true;
 }
  
  
  
  
  /**
   * Methode de lancement de l'executable GENARR.
   * Permet de g�n�rer un fichier contenant la liste des navires.
   *
   */
  public void lancementGenarr(){
	  
	  //-- ETAPE 1: v�rification du nombre minimal de cat�gories de navire --//
	  if(donnees_.listeBateaux_.listeNavires_.size()==0){
		  new BuDialogError(this.getApp(),isSinavi_,"Il doit exister au moins un bateau.\n" +
	  		"avant de r�aliser la g�n�ration de bateaux.").activate();
		  return;
	  }
	  int h=donnees_.listeBateaux_.existeTrajetPourChaqueNavires();
	  if(h!=-1){
		  new BuDialogError(this.getApp(),isSinavi_,"Il n'existe pas de trajet pour " +donnees_.listeBateaux_.retournerNavire(h).nom+"\n"+
	  		"avant de r�aliser la g�n�ration de bateaux.\n Pour saisir un trajet, aller dans l'onglet Navigations, rubrique trajet").activate();
		  return;
	  }
	  
	  try {
		 //-- Etape 2: creation du fichier des cat�gories de navire: .categ et donn�es g�n�rales --// 
		DParametresSinavi3.ecritDonneesCategoriesNavires(this.donnees_.params_.navires, this.donnees_.projet_.getFichier());
		DParametresSinavi3.ecritDonneesGenerales(this.donnees_.params_.donneesGenerales, this.donnees_.projet_.getFichier());
       Sinavi3DataSimulation.ecritDonneestrajets(donnees_.listeBateaux_, this.donnees_.projet_.getFichier());
	  
	  //-- Etape 3: lancement de l'executable Genarr --//
		
       if(MOCK_ENABLE) {
    	   Sinavi3Mock mock = new Sinavi3Mock();
    	   mock.mockGenerationBateaux(donnees_.projet_.getFichier(),donnees_.getListeBateaux_().getListeNavires_());
       } else {
       
		if(!ExecuterCommandeDepuisServeur(0,donnees_.projet_.getFichier()))
		      return ;//si la procedure retourne une valeure false alors il y a eu erreur, par cons�quent on quitte.
       }
	  //-- Etape 4: lecture du fichier g�n�r� et remplissage de la structure GENARR--//
	  boolean result=donnees_.genarr_.lectureFichierGenarr(donnees_.projet_.getFichier());
	  if(!result)
		  return;
	  
	  new BuDialogMessage(this.getApp(),isSinavi_,"La g�n�ration de bateaux a �t� r�alis� avec succ�s.").activate();
	  
	  
	  } catch (IOException e) {
		  new BuDialogError(this.getApp(),isSinavi_,"Erreur dans la cr�ation du fichier des cat�gories de bateaux.\n" +
		  		"V�rifier la saisie des cat�gories de bateaux.").activate();
	  }
  }
  
  /**
   * Methode d'aafichage des navires g�n�r�s par GENARR.
   * L'affichage s'effectue sous forme de tableau r�capitulatif triable.
   *
   */
  public void affichageGenarr(){
	  
	  //-- ETAPE 1: verification de l'existence du fichier GENARR: --//
	    File fichier=new File(donnees_.projet_.getFichier()+".arriv");
		if(!fichier.exists()){
			new BuDialogError(null,Sinavi3Implementation.isSinavi_,"Le fichier de g�n�ration est introuvable.\n Veuillez relancer la g�n�ration de bateaux (onglet G�n�ration)").activate();
			return;
		}
		
		//-- ETAPE 2: Lecture du fichier GENARR --//
		boolean result= donnees_.genarr_.lectureFichierGenarr(donnees_.projet_.getFichier());
		if(!result)
			  return;
		 
		//-- activation de l'interface d'affichage de Genarr --// 
		 addInternalFrame(new GenarrFrameAffichage(donnees_));
  }

  /**
   * Methode de lancement des remplissage des SAS
   */
  protected void activerRemplissageSAS() {

    if (this.donnees_.getListeEcluse().getListeEcluses().size() <= 0)

    {
      /**
       * on lance une fenetre indiquant l'erreur: il faut au moins 2 gares de saisies:
       */
      new BuDialogError(getApp(), getInformationsSoftware(),
          "Il faut au moins une �cluse pour lancer les r�gles de remplissage de SAS. Veuillez d'abord cr�er une �cluse")
          .activate();

    } else if (this.donnees_.getListeBateaux_().getListeNavires_().size() <= 0) {
      new BuDialogError(getApp(), getInformationsSoftware(),
          "Il faut au moins une cat�gorie de navire pour lancer les r�gles de remplissage de SAS. Veuillez d'abord cr�er une cat�gorie")
          .activate();
    } else

    {
      this.addInternalFrame(new SinaviPanelRemplissageSAS(donnees_));
    }
  }
  
  
public SinaviPanelRemplissageSAS getGestionRemplissageSAS_() {
	return gestionRemplissageSAS_;
}

public void setGestionRemplissageSAS_(
		SinaviPanelRemplissageSAS gestionRemplissageSAS_) {
	this.gestionRemplissageSAS_ = gestionRemplissageSAS_;
}

public Hashtable getProjets_() {
	return projets_;
}

public void setProjets_(Hashtable projets_) {
	this.projets_ = projets_;
}

public FudaaProjet getProjet_() {
	return projet_;
}

public void setProjet_(FudaaProjet projet_) {
	this.projet_ = projet_;
}

public Sinavi3DataSimulation getDonnees() {
	return donnees_;
}

public void setDonnees_(Sinavi3DataSimulation donnees_) {
	this.donnees_ = donnees_;
}

public SinaviNetwork getNetworkEditor() {
	return networkEditor;
}

public void setNetworkEditor(SinaviNetwork networkEditor) {
	this.networkEditor = networkEditor;
}

public Sinavi3InternalFrame getGestionDureeManeuvreEcluse_() {
	return gestionDureeManeuvreEcluse_;
}

public void setGestionDureeManeuvreEcluse_(
		Sinavi3InternalFrame gestionDureeManeuvreEcluse_) {
	this.gestionDureeManeuvreEcluse_ = gestionDureeManeuvreEcluse_;
}
  
  
}
