package org.fudaa.fudaa.sinavi3;

import java.text.ParseException;
import java.util.ArrayList;

import javax.swing.table.AbstractTableModel;

import com.memoire.bu.BuDialogError;

public class LoiJournaliereTableModel extends AbstractTableModel{

	
	
	 /**
	   * Donn�es de la loi journaliere
	   */
	  ArrayList loiJournaliere_ = new ArrayList();

	  
	  CoupleLoiDeterministe nouveauCouple=null;

	private Sinavi3DataSimulation donnees_;
	  
	  
	public LoiJournaliereTableModel(ArrayList _loiJournaliere, Sinavi3DataSimulation _donnee){
		loiJournaliere_=_loiJournaliere;
		donnees_=_donnee;
	}
	
	
	public int getColumnCount() {
		// TODO Auto-generated method stub
		return 1;
	}

	public int getRowCount() {
		// TODO Auto-generated method stub
		return loiJournaliere_.size()+1;
	}

	
	public String formatHeure(double value) {
		String data  = "" + value;
		if(data.contains(","))
			data = data.replace(",", ":");
		if(data.contains("."))
			data = data.replace(".", ":");
		
		return data;
		
	}
	public double unformatHeure(String data) throws ParseException {
		if(data.contains(":"))
			data = data.replace(":", ".");
		if(data.contains(","))
			data = data.replace(",", ".");
		double res = Double.parseDouble(data);
		return res;
		
	}
	
	public Object getValueAt(int rowIndex, int columnIndex) {
		
		if(rowIndex==getRowCount()-1)
			{
			if(nouveauCouple!=null)
				{
					if(columnIndex==0 && nouveauCouple.temps_!=-1)
						return formatHeure(nouveauCouple.temps_);									
				}
				return "";
			}
		
		if(columnIndex==0)
			return formatHeure(((CoupleLoiDeterministe) this.loiJournaliere_.get(rowIndex)).temps_);
		return "";
		
	}

	
	public String getColumnName(int column) {
		if(column==0)
			return "Horaire";
		return "";
	}

	
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		// TODO Auto-generated method stub
		return true;
	}

	
	public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
		
		if(aValue=="") {loiJournaliere_.remove(this.loiJournaliere_.get(rowIndex)); this.fireTableStructureChanged(); return;}
		double temps=-1;
		
		try {
			if(columnIndex==0)
				temps = unformatHeure(aValue.toString());
				//temps=Double.parseDouble((String)aValue);
		
		} catch (NumberFormatException e) {
			new BuDialogError(donnees_.application_.getApp(), Sinavi3Implementation.isSinavi_,
                    "La saisie n'est pas correcte.").activate();
            return;
		} catch (ParseException e) {
			new BuDialogError(donnees_.application_.getApp(), Sinavi3Implementation.isSinavi_,
                    "La saisie n'est pas correcte.").activate();
            return;
		}
		
		
		if(columnIndex==0){
						//-- TEST HORAIRE --//
				
			if(temps>24 || temps<0){
				new BuDialogError(donnees_.application_.getApp(), Sinavi3Implementation.isSinavi_,
				"L'horaire doit �tre compris entre 0 et 24 heures.").activate();
				return;
			}
			
			//-- TEST FORMATTAGE HORAIRE --//
			String contenu=(String)aValue;

			if(contenu.lastIndexOf(".")!=-1){
				String unite=contenu.substring(contenu.lastIndexOf(".")+1, contenu.length());
				if(unite.length()>2){
					new BuDialogError(donnees_.application_.getApp(), Sinavi3Implementation.isSinavi_,
					"Il doit y avoir deux chiffres au maximum apr�s la virgule.").activate();

					return;
				}


				float valUnite=Float.parseFloat(unite);
				if(valUnite>=60){
					new BuDialogError(donnees_.application_.getApp(), Sinavi3Implementation.isSinavi_,
					"Les unit�s doivent �tre inf�rieures � 60 minutes.").activate();
					return;
				}
			}
		}// fin test formattage horaire
				
		//-- MODE AJOUT --//
		if(rowIndex==loiJournaliere_.size())
		{
			//-- ajout de l'�l�ment --//
			if(nouveauCouple==null){
				nouveauCouple=new CoupleLoiDeterministe(-1,-1);
			}
			
			
			//-- ajout de la valeur selon la colonne --//
			if(columnIndex==0)
				nouveauCouple.temps_=temps;
			
			//-- test si le nouveau couple est complet, on l'ajoute � la liste des couples --//
			if(nouveauCouple.temps_!=-1){
				loiJournaliere_.add(new CoupleLoiDeterministe(0, nouveauCouple.temps_));
				//reinitialisation du couple
				nouveauCouple=null;
				//la structure du butable doit changer: il faut le faire recalculer car il a une ligne de plus
				this.fireTableStructureChanged();
			}
			
		}
		else
		{
			//-- MODE MODIFICATION --//
			CoupleLoiDeterministe coupleAmodifier= (CoupleLoiDeterministe) this.loiJournaliere_.get(rowIndex);
		      
			if(columnIndex==0)
				coupleAmodifier.temps_=temps;
				
			//mise a jour du contenu de la cellule
			this.fireTableCellUpdated(rowIndex, columnIndex);
		}
		
		
	}
	
	

}
