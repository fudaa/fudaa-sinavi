package org.fudaa.fudaa.sinavi3;

/**
 * 
 */

import java.awt.GridLayout;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.border.Border;

/**
 * Classe qui derive de FrameSaisieHoraires et permet de saisir des horaires complet (en semaine, samedi, dimanche et
 * jour fris)
 * 
 * @author Adrien Hadoux
 */
public class Sinavi3FrameSaisieHorairesComplet extends Sinavi3FrameSaisieHorairesResume {

  /**
   * table des differents menus de saisie des horaires
   */
  JTabbedPane typesHoraires_ = new JTabbedPane();

  /*
   * Les differents panneaux pour chaque saisie d'horaires
   */
  JPanel sglobal_;
  JPanel dglobal_;
  JPanel fglobal_;

  /**
   * JText du samedi
   */
  JTextField screneau1debut_ = new JTextField(3);
  JTextField screneau1fin_ = new JTextField(3);
  JTextField screneau2debut_ = new JTextField(3);
  JTextField screneau2fin_ = new JTextField(3);
  JTextField screneau3debut_ = new JTextField(3);
  JTextField screneau3fin_ = new JTextField(3);

  /**
   * JText du dimanche
   */
  JTextField dcreneau1debut_ = new JTextField(3);
  JTextField dcreneau1fin_ = new JTextField(3);
  JTextField dcreneau2debut_ = new JTextField(3);
  JTextField dcreneau2fin_ = new JTextField(3);
  JTextField dcreneau3debut_ = new JTextField(3);
  JTextField dcreneau3fin_ = new JTextField(3);

  /**
   * JText des jours fériés
   */
  JTextField fcreneau1debut_ = new JTextField(3);
  JTextField fcreneau1fin_ = new JTextField(3);
  JTextField fcreneau2debut_ = new JTextField(3);
  JTextField fcreneau2fin_ = new JTextField(3);
  JTextField fcreneau3debut_ = new JTextField(3);
  JTextField fcreneau3fin_ = new JTextField(3);

  // bordures
  Border raisedBevel_ = BorderFactory.createRaisedBevelBorder();
  Border loweredBevel_ = BorderFactory.createLoweredBevelBorder();
  Border compound_ = BorderFactory.createCompoundBorder(raisedBevel_, loweredBevel_);
  Border bordnormal_ = BorderFactory.createEtchedBorder();

  /**
   * Constructeur de la fenetre
   * 
   * @see Sinavi3FrameSaisieHoraires
   * @param h horaire qui va etre saisi via la fenetre
   */

  Sinavi3FrameSaisieHorairesComplet(final Sinavi3Horaire h) {
    // modification fenetre mere
    super(h);
    setTitle("Saisie horaires complets");
    this.remove(global_);
    typesHoraires_.addTab("Horaires de semaine", this.global_);
    setBorder(BorderFactory.createCompoundBorder(BorderFactory.createRaisedBevelBorder(), BorderFactory.createLoweredBevelBorder()));
    
    /**
     * Menu samedi
     */
    this.screneau1debut_
        .setToolTipText("Saisissez le nombre correspondant  l'horaire d'ouverture. Par exemple, si vous voulez rentrer 8h46, tapez 8.46");
    this.screneau2debut_
        .setToolTipText("Saisissez le nombre correspondant  l'horaire d'ouverture. Par exemple, si vous voulez rentrer 8h46, tapez 8.46");
    this.screneau1fin_
        .setToolTipText("Saisissez le nombre correspondant  l'horaire de fermeture. Par exemple, si vous voulez rentrer 8h46, tapez 8.46");
    this.screneau2fin_
        .setToolTipText("Saisissez le nombre correspondant  l'horaire de fermeture. Par exemple, si vous voulez rentrer 8h46, tapez 8.46");
    this.screneau3debut_
        .setToolTipText("Saisissez le nombre correspondant  l'horaire d'ouverture. Par exemple, si vous voulez rentrer 8h46, tapez 8.46");
    this.screneau3fin_
        .setToolTipText("Saisissez le nombre correspondant  l'horaire de fermeture. Par exemple, si vous voulez rentrer 8h46, tapez 8.46");

    if (horaire_.samediCreneau1HeureArrivee != -1 && horaire_.samediCreneau2HeureArrivee != -1
        && horaire_.samediCreneau3HeureArrivee != -1 && horaire_.samediCreneau1HeureDep != -1
        && horaire_.samediCreneau2HeureDep != -1 && horaire_.samediCreneau3HeureDep != -1) {
      // valeur recuperee de la structure horaire
      this.screneau1debut_.setText("" + (float) horaire_.samediCreneau1HeureDep);
      this.screneau1fin_.setText("" + (float) horaire_.samediCreneau1HeureArrivee);
      this.screneau2debut_.setText("" + (float) horaire_.samediCreneau2HeureDep);
      this.screneau2fin_.setText("" + (float) horaire_.samediCreneau2HeureArrivee);
      this.screneau3debut_.setText("" + (float) horaire_.samediCreneau3HeureDep);
      this.screneau3fin_.setText("" + (float) horaire_.samediCreneau3HeureArrivee);

    } else {
      // valeur par defaut:
      this.screneau1debut_.setText("0.0");
      this.screneau1fin_.setText("24.0");
      this.screneau2debut_.setText("0.0");
      this.screneau2fin_.setText("0.0");
      this.screneau3debut_.setText("0.0");
      this.screneau3fin_.setText("0.0");
    }

    this.screneau1debut_.addFocusListener(new FocusAdapter() {
      public void focusLost(final FocusEvent e) {
        System.out.print("test validite de Longueur Min");
        // test de validit si on a rentr quelque chose
        if (!screneau1debut_.getText().equals("")) {
          try {
            final float i = Float.parseFloat(screneau1debut_.getText());
            if (i < 0) {
              JOptionPane.showMessageDialog(null, "Erreur!! Cet horaire est ngatif!!!!", "Avertissement",
                  JOptionPane.ERROR_MESSAGE);
              screneau1debut_.setText("");
            } else if (i >= 24) {
              JOptionPane.showMessageDialog(null, "Erreur!! Cet horaire doit etre compris entre 0 et 24!!!!",
                  "Avertissement", JOptionPane.ERROR_MESSAGE);
              screneau1debut_.setText("");
            }

          } catch (final NumberFormatException nfe) {
            JOptionPane.showMessageDialog(null, "Erreur!! Ce nombre n'existe pas!!!!", "Avertissement",
                JOptionPane.ERROR_MESSAGE);
            screneau1debut_.setText("");
          }
        }
      }
    });

    this.screneau1fin_.addFocusListener(new FocusAdapter() {
      public void focusLost(final FocusEvent e) {
        System.out.print("test validite de Longueur Min");
        // test de validit si on a rentr quelque chose
        if (!screneau1fin_.getText().equals("")) {
          try {
            final float i = Float.parseFloat(screneau1fin_.getText());
            if (i < 0) {
              JOptionPane.showMessageDialog(null, "Erreur!! Cet horaire est ngatif!!!!", "Avertissement",
                  JOptionPane.ERROR_MESSAGE);
              screneau1fin_.setText("");
            } else if (i > 24) {
              JOptionPane.showMessageDialog(null, "Erreur!! Cet horaire doit etre compris entre 0 et 24!!!!",
                  "Avertissement", JOptionPane.ERROR_MESSAGE);
              screneau1fin_.setText("");
            } else {
              // horaire fin inferieur a horaire de depart:
              if (i < Float.parseFloat(screneau1debut_.getText())) {
                JOptionPane.showMessageDialog(null,
                    "Erreur!! Cet horaire de fin est infererieur a l'horaire de deoart!!!!", "Avertissement",
                    JOptionPane.ERROR_MESSAGE);
                screneau1fin_.setText("");

              }
            }

          } catch (final NumberFormatException nfe) {
            JOptionPane.showMessageDialog(null, "Erreur!! Ce nombre n'existe pas!!!!", "Avertissement",
                JOptionPane.ERROR_MESSAGE);
            screneau1fin_.setText("");
          }
        }
      }
    });

    this.screneau2debut_.addFocusListener(new FocusAdapter() {
      public void focusLost(final FocusEvent e) {
        System.out.print("test validite de Longueur Min");
        // test de validit si on a rentr quelque chose
        if (!screneau2debut_.getText().equals("")) {
          try {
            final float i = Float.parseFloat(screneau2debut_.getText());
            if (i < 0) {
              JOptionPane.showMessageDialog(null, "Erreur!! Cet horaire est ngatif!!!!", "Avertissement",
                  JOptionPane.ERROR_MESSAGE);
              screneau2debut_.setText("");
            } else if (i >= 24) {
              JOptionPane.showMessageDialog(null, "Erreur!! Cet horaire doit etre compris entre 0 et 24!!!!",
                  "Avertissement", JOptionPane.ERROR_MESSAGE);
              screneau2debut_.setText("");
            } else // cas des intervalles qui se chevauchent
            if (i > Float.parseFloat(screneau1debut_.getText()) && i < Float.parseFloat(screneau1fin_.getText())) {
              JOptionPane
                  .showMessageDialog(
                      null,
                      "Erreur!! Cet horaire est inclus dans le premier crneau!!!! Les crneaux sont des intervalles de temps distincts...",
                      "Avertissement", JOptionPane.ERROR_MESSAGE);
              screneau2debut_.setText("");
            }

          } catch (final NumberFormatException nfe) {
            JOptionPane.showMessageDialog(null, "Erreur!! Ce nombre n'existe pas!!!!", "Avertissement",
                JOptionPane.ERROR_MESSAGE);
            screneau2debut_.setText("");
          }
        }
      }
    });

    this.screneau2fin_.addFocusListener(new FocusAdapter() {
      public void focusLost(final FocusEvent e) {
        System.out.print("test validite de Longueur Min");
        // test de validit si on a rentr quelque chose
        if (!screneau2fin_.getText().equals("")) {
          try {
            final float i = Float.parseFloat(screneau2fin_.getText());
            if (i < 0) {
              JOptionPane.showMessageDialog(null, "Erreur!! Cet horaire est ngatif!!!!", "Avertissement",
                  JOptionPane.ERROR_MESSAGE);
              screneau2fin_.setText("");
            } else if (i > 24) {
              JOptionPane.showMessageDialog(null, "Erreur!! Cet horaire doit etre compris entre 0 et 24!!!!",
                  "Avertissement", JOptionPane.ERROR_MESSAGE);
              screneau2fin_.setText("");
            } else {
              // horaire fin inferieur a horaire de depart:
              if (i < Float.parseFloat(screneau2debut_.getText())) {
                JOptionPane.showMessageDialog(null,
                    "Erreur!! Cet horaire de fin est infererieur a l'horaire de deoart!!!!", "Avertissement",
                    JOptionPane.ERROR_MESSAGE);
                screneau2fin_.setText("");

              } else // cas des intervalles qui se chevauchent
              if (i > Float.parseFloat(screneau1debut_.getText()) && i < Float.parseFloat(screneau1fin_.getText())) {
                JOptionPane
                    .showMessageDialog(
                        null,
                        "Erreur!! Cet horaire est inclus dans le premier crneau!!!! Les crneaux sont des intervalles de temps distincts...",
                        "Avertissement", JOptionPane.ERROR_MESSAGE);
                screneau2fin_.setText("");
              }

            }

          } catch (final NumberFormatException nfe) {
            JOptionPane.showMessageDialog(null, "Erreur!! Ce nombre n'existe pas!!!!", "Avertissement",
                JOptionPane.ERROR_MESSAGE);
            screneau2fin_.setText("");
          }
        }
      }
    });

    this.screneau3debut_.addFocusListener(new FocusAdapter() {
      public void focusLost(final FocusEvent e) {
        System.out.print("test validite de Longueur Min");
        // test de validit si on a rentr quelque chose
        if (!screneau3debut_.getText().equals("")) {
          try {
            final float i = Float.parseFloat(screneau3debut_.getText());
            if (i < 0) {
              JOptionPane.showMessageDialog(null, "Erreur!! Cet horaire est ngatif!!!!", "Avertissement",
                  JOptionPane.ERROR_MESSAGE);
              screneau3debut_.setText("");
            } else if (i >= 24) {
              JOptionPane.showMessageDialog(null, "Erreur!! Cet horaire doit etre compris entre 0 et 24!!!!",
                  "Avertissement", JOptionPane.ERROR_MESSAGE);
              screneau3debut_.setText("");
            } else // cas des intervalles qui se chevauchent
            if (i > Float.parseFloat(screneau1debut_.getText()) && i < Float.parseFloat(screneau1fin_.getText())) {
              JOptionPane
                  .showMessageDialog(
                      null,
                      "Erreur!! Cet horaire est inclus dans le premier crneau!!!! Les crneaux sont des intervalles de temps distincts...",
                      "Avertissement", JOptionPane.ERROR_MESSAGE);
              screneau3debut_.setText("");
            } else // cas des intervalles qui se chevauchent
            if (i > Float.parseFloat(screneau2debut_.getText()) && i < Float.parseFloat(screneau2fin_.getText())) {
              JOptionPane
                  .showMessageDialog(
                      null,
                      "Erreur!! Cet horaire est inclus dans le deuxieme crneau!!!! Les crneaux sont des intervalles de temps distincts...",
                      "Avertissement", JOptionPane.ERROR_MESSAGE);
              screneau3debut_.setText("");
            }
          } catch (final NumberFormatException nfe) {
            JOptionPane.showMessageDialog(null, "Erreur!! Ce nombre n'existe pas!!!!", "Avertissement",
                JOptionPane.ERROR_MESSAGE);
            screneau3debut_.setText("");
          }
        }
      }
    });

    this.screneau3fin_.addFocusListener(new FocusAdapter() {
      public void focusLost(final FocusEvent e) {
        System.out.print("test validite de Longueur Min");
        // test de validit si on a rentr quelque chose
        if (!screneau3fin_.getText().equals("")) {
          try {
            final float i = Float.parseFloat(screneau3fin_.getText());
            if (i < 0) {
              JOptionPane.showMessageDialog(null, "Erreur!! Cet horaire est ngatif!!!!", "Avertissement",
                  JOptionPane.ERROR_MESSAGE);
              screneau3fin_.setText("");
            } else if (i > 24) {
              JOptionPane.showMessageDialog(null, "Erreur!! Cet horaire doit etre compris entre 0 et 24!!!!",
                  "Avertissement", JOptionPane.ERROR_MESSAGE);
              screneau3fin_.setText("");
            } else {
              // horaire fin inferieur a horaire de depart:
              if (i < Float.parseFloat(screneau3debut_.getText())) {
                JOptionPane.showMessageDialog(null,
                    "Erreur!! Cet horaire de fin est infererieur a l'horaire de deoart!!!!", "Avertissement",
                    JOptionPane.ERROR_MESSAGE);
                screneau3fin_.setText("");

              } else // cas des intervalles qui se chevauchent
              if (i > Float.parseFloat(screneau1debut_.getText()) && i < Float.parseFloat(screneau1fin_.getText())) {
                JOptionPane
                    .showMessageDialog(
                        null,
                        "Erreur!! Cet horaire est inclus dans le premier crneau!!!! Les crneaux sont des intervalles de temps distincts...",
                        "Avertissement", JOptionPane.ERROR_MESSAGE);
                screneau3fin_.setText("");
              } else // cas des intervalles qui se chevauchent
              if (i > Float.parseFloat(screneau2debut_.getText()) && i < Float.parseFloat(screneau2fin_.getText())) {
                JOptionPane
                    .showMessageDialog(
                        null,
                        "Erreur!! Cet horaire est inclus dans le deuxieme cr�neau!!!! Les crneaux sont des intervalles de temps distincts...",
                        "Avertissement", JOptionPane.ERROR_MESSAGE);
                screneau3fin_.setText("");
              }
            }

          } catch (final NumberFormatException nfe) {
            JOptionPane.showMessageDialog(null, "Erreur!! Ce nombre n'existe pas!!!!", "Avertissement",
                JOptionPane.ERROR_MESSAGE);
            screneau3fin_.setText("");
          }
        }
      }
    });

    /**
     * ********************************************** Organisation des donnes dans la frame:
     * **********************************************
     */
    sglobal_ = new JPanel();
    sglobal_.setLayout(new GridLayout(4, 1));

    // panel de saisie du premier creneau horaire
    final JPanel sch1 = new JPanel();

    sch1.add(new JLabel(" Creneau 1: Horaire: "));
    sch1.add(this.screneau1debut_);
    sch1.add(new JLabel("�"));
    sch1.add(this.screneau1fin_);
    sch1.add(new JLabel("HEURES:MINUTES"));
    sch1.setBorder(bordnormal_);
    sglobal_.add(sch1);

    // panel de saisie du deuxieme horaire
    final JPanel sch2 = new JPanel();
    sch2.add(new JLabel(" Creneau 2: Horaire: "));
    sch2.add(this.screneau2debut_);
    sch2.add(new JLabel("�"));
    sch2.add(this.screneau2fin_);
    sch2.add(new JLabel("HEURES:MINUTES"));
    sch2.setBorder(bordnormal_);
    sglobal_.add(sch2);

    // panel de saisie du troisieme horaire
    final JPanel sch4 = new JPanel();
    sch4.add(new JLabel(" Creneau 3: Horaire: "));
    sch4.add(this.screneau3debut_);
    sch4.add(new JLabel("�"));
    sch4.add(this.screneau3fin_);
    sch4.add(new JLabel("HEURES:MINUTES"));
    sch4.setBorder(bordnormal_);
    sglobal_.add(sch4);

    // panel qui contient le bouton de validation:
    final JPanel sch3 = new JPanel();

    sglobal_.setBorder(compound_);
    sglobal_.add(sch3);
    this.typesHoraires_.add("samedi", sglobal_);

    /**
     * Menu dimanche
     */
    this.dcreneau1debut_
        .setToolTipText("Saisissez le nombre correspondant  l'horaire d'ouverture. Par exemple, si vous voulez rentrer 8h46, tapez 8.46");
    this.dcreneau2debut_
        .setToolTipText("Saisissez le nombre correspondant  l'horaire d'ouverture. Par exemple, si vous voulez rentrer 8h46, tapez 8.46");
    this.dcreneau1fin_
        .setToolTipText("Saisissez le nombre correspondant  l'horaire de fermeture. Par exemple, si vous voulez rentrer 8h46, tapez 8.46");
    this.dcreneau2fin_
        .setToolTipText("Saisissez le nombre correspondant  l'horaire de fermeture. Par exemple, si vous voulez rentrer 8h46, tapez 8.46");
    this.dcreneau3debut_
        .setToolTipText("Saisissez le nombre correspondant  l'horaire d'ouverture. Par exemple, si vous voulez rentrer 8h46, tapez 8.46");
    this.dcreneau3fin_
        .setToolTipText("Saisissez le nombre correspondant  l'horaire de fermeture. Par exemple, si vous voulez rentrer 8h46, tapez 8.46");

    if (horaire_.dimancheCreneau1HeureArrivee != -1 && horaire_.dimancheCreneau2HeureArrivee != -1
        && horaire_.dimancheCreneau3HeureArrivee != -1 && horaire_.dimancheCreneau1HeureDep != -1
        && horaire_.dimancheCreneau2HeureDep != -1 && horaire_.dimancheCreneau3HeureDep != -1) {
      // valeur recuperee de la structure horaire
      this.dcreneau1debut_.setText("" + (float) horaire_.dimancheCreneau1HeureDep);
      this.dcreneau1fin_.setText("" + (float) horaire_.dimancheCreneau1HeureArrivee);
      this.dcreneau2debut_.setText("" + (float) horaire_.dimancheCreneau2HeureDep);
      this.dcreneau2fin_.setText("" + (float) horaire_.dimancheCreneau2HeureArrivee);
      this.dcreneau3debut_.setText("" + (float) horaire_.dimancheCreneau3HeureDep);
      this.dcreneau3fin_.setText("" + (float) horaire_.dimancheCreneau3HeureArrivee);

    } else {
      // valeur par defaut:
      this.dcreneau1debut_.setText("0.0");
      this.dcreneau1fin_.setText("24.0");
      this.dcreneau2debut_.setText("0.0");
      this.dcreneau2fin_.setText("0.0");
      this.dcreneau3debut_.setText("0.0");
      this.dcreneau3fin_.setText("0.0");
    }

    this.dcreneau1debut_.addFocusListener(new FocusAdapter() {
      public void focusLost(final FocusEvent e) {
        System.out.print("test validite de Longueur Min");
        // test de validit si on a rentr quelque chose
        if (!dcreneau1debut_.getText().equals("")) {
          try {
            final float i = Float.parseFloat(dcreneau1debut_.getText());
            if (i < 0) {
              JOptionPane.showMessageDialog(null, "Erreur!! Cet horaire est ngatif!!!!");
              dcreneau1debut_.setText("");
            } else if (i >= 24) {
              JOptionPane.showMessageDialog(null, "Erreur!! Cet horaire doit etre compris entre 0 et 24!!!!",
                  "Avertissement", JOptionPane.ERROR_MESSAGE);
              dcreneau1debut_.setText("");
            }

          } catch (final NumberFormatException nfe) {
            JOptionPane.showMessageDialog(null, "Erreur!! Ce nombre n'existe pas!!!!", "Avertissement",
                JOptionPane.ERROR_MESSAGE);
            dcreneau1debut_.setText("");
          }
        }
      }
    });

    this.dcreneau1fin_.addFocusListener(new FocusAdapter() {
      public void focusLost(final FocusEvent e) {
        System.out.print("test validite de Longueur Min");
        // test de validit si on a rentr quelque chose
        if (!dcreneau1fin_.getText().equals("")) {
          try {
            final float i = Float.parseFloat(dcreneau1fin_.getText());
            if (i < 0) {
              JOptionPane.showMessageDialog(null, "Erreur!! Cet horaire est ngatif!!!!", "Avertissement",
                  JOptionPane.ERROR_MESSAGE);
              dcreneau1fin_.setText("");
            } else if (i > 24) {
              JOptionPane.showMessageDialog(null, "Erreur!! Cet horaire doit etre compris entre 0 et 24!!!!",
                  "Avertissement", JOptionPane.ERROR_MESSAGE);
              dcreneau1fin_.setText("");
            } else {
              // horaire fin inferieur a horaire de depart:
              if (i < Float.parseFloat(dcreneau1debut_.getText())) {
                JOptionPane.showMessageDialog(null,
                    "Erreur!! Cet horaire de fin est infererieur a l'horaire de deoart!!!!", "Avertissement",
                    JOptionPane.ERROR_MESSAGE);
                dcreneau1fin_.setText("");

              }
            }

          } catch (final NumberFormatException nfe) {
            JOptionPane.showMessageDialog(null, "Erreur!! Ce nombre n'existe pas!!!!", "Avertissement",
                JOptionPane.ERROR_MESSAGE);
            dcreneau1fin_.setText("");
          }
        }
      }
    });

    this.dcreneau2debut_.addFocusListener(new FocusAdapter() {
      public void focusLost(final FocusEvent e) {
        System.out.print("test validite de Longueur Min");
        // test de validit si on a rentr quelque chose
        if (!dcreneau2debut_.getText().equals("")) {
          try {
            final float i = Float.parseFloat(dcreneau2debut_.getText());
            if (i < 0) {
              JOptionPane.showMessageDialog(null, "Erreur!! Cet horaire est ngatif!!!!", "Avertissement",
                  JOptionPane.ERROR_MESSAGE);
              dcreneau2debut_.setText("");
            } else if (i >= 24) {
              JOptionPane.showMessageDialog(null, "Erreur!! Cet horaire doit etre compris entre 0 et 24!!!!",
                  "Avertissement", JOptionPane.ERROR_MESSAGE);
              dcreneau2debut_.setText("");
            } else // cas des intervalles qui se chevauchent
            if (i > Float.parseFloat(dcreneau1debut_.getText()) && i < Float.parseFloat(dcreneau1fin_.getText())) {
              JOptionPane
                  .showMessageDialog(
                      null,
                      "Erreur!! Cet horaire est inclus dans le premier crneau!!!! Les crneaux sont des intervalles de temps distincts...",
                      "Avertissement", JOptionPane.ERROR_MESSAGE);
              dcreneau2debut_.setText("");
            }

          } catch (final NumberFormatException nfe) {
            JOptionPane.showMessageDialog(null, "Erreur!! Ce nombre n'existe pas!!!!", "Avertissement",
                JOptionPane.ERROR_MESSAGE);
            dcreneau2debut_.setText("");
          }
        }
      }
    });

    this.dcreneau2fin_.addFocusListener(new FocusAdapter() {
      public void focusLost(final FocusEvent e) {
        System.out.print("test validite de Longueur Min");
        // test de validit si on a rentr quelque chose
        if (!dcreneau2fin_.getText().equals("")) {
          try {
            final float i = Float.parseFloat(dcreneau2fin_.getText());
            if (i < 0) {
              JOptionPane.showMessageDialog(null, "Erreur!! Cet horaire est ngatif!!!!", "Avertissement",
                  JOptionPane.ERROR_MESSAGE);
              dcreneau2fin_.setText("");
            } else if (i > 24) {
              JOptionPane.showMessageDialog(null, "Erreur!! Cet horaire doit etre compris entre 0 et 24!!!!",
                  "Avertissement", JOptionPane.ERROR_MESSAGE);
              dcreneau2fin_.setText("");
            } else {
              // horaire fin inferieur a horaire de depart:
              if (i < Float.parseFloat(dcreneau2debut_.getText())) {
                JOptionPane.showMessageDialog(null,
                    "Erreur!! Cet horaire de fin est infererieur a l'horaire de deoart!!!!", "Avertissement",
                    JOptionPane.ERROR_MESSAGE);
                dcreneau2fin_.setText("");

              } else // cas des intervalles qui se chevauchent
              if (i > Float.parseFloat(dcreneau1debut_.getText()) && i < Float.parseFloat(dcreneau1fin_.getText())) {
                JOptionPane
                    .showMessageDialog(
                        null,
                        "Erreur!! Cet horaire est inclus dans le premier crneau!!!! Les crneaux sont des intervalles de temps distincts...",
                        "Avertissement", JOptionPane.ERROR_MESSAGE);
                dcreneau2fin_.setText("");
              }

            }

          } catch (final NumberFormatException nfe) {
            JOptionPane.showMessageDialog(null, "Erreur!! Ce nombre n'existe pas!!!!", "Avertissement",
                JOptionPane.ERROR_MESSAGE);
            dcreneau2fin_.setText("");
          }
        }
      }
    });

    this.dcreneau3debut_.addFocusListener(new FocusAdapter() {
      public void focusLost(final FocusEvent e) {
        System.out.print("test validite de Longueur Min");
        // test de validit si on a rentr quelque chose
        if (!dcreneau3debut_.getText().equals("")) {
          try {
            final float i = Float.parseFloat(dcreneau3debut_.getText());
            if (i < 0) {
              JOptionPane.showMessageDialog(null, "Erreur!! Cet horaire est ngatif!!!!", "Avertissement",
                  JOptionPane.ERROR_MESSAGE);
              dcreneau3debut_.setText("");
            } else if (i >= 24) {
              JOptionPane.showMessageDialog(null, "Erreur!! Cet horaire doit etre compris entre 0 et 24!!!!",
                  "Avertissement", JOptionPane.ERROR_MESSAGE);
              dcreneau3debut_.setText("");
            } else // cas des intervalles qui se chevauchent
            if (i > Float.parseFloat(dcreneau1debut_.getText()) && i < Float.parseFloat(dcreneau1fin_.getText())) {
              JOptionPane
                  .showMessageDialog(
                      null,
                      "Erreur!! Cet horaire est inclus dans le premier crneau!!!! Les crneaux sont des intervalles de temps distincts...",
                      "Avertissement", JOptionPane.ERROR_MESSAGE);
              dcreneau3debut_.setText("");
            } else // cas des intervalles qui se chevauchent
            if (i > Float.parseFloat(dcreneau2debut_.getText()) && i < Float.parseFloat(dcreneau2fin_.getText())) {
              JOptionPane
                  .showMessageDialog(
                      null,
                      "Erreur!! Cet horaire est inclus dans le deuxieme crneau!!!! Les crneaux sont des intervalles de temps distincts...",
                      "Avertissement", JOptionPane.ERROR_MESSAGE);
              dcreneau3debut_.setText("");
            }
          } catch (final NumberFormatException nfe) {
            JOptionPane.showMessageDialog(null, "Erreur!! Ce nombre n'existe pas!!!!");
            dcreneau3debut_.setText("");
          }
        }
      }
    });

    this.dcreneau3fin_.addFocusListener(new FocusAdapter() {
      public void focusLost(final FocusEvent e) {
        System.out.print("test validite de Longueur Min");
        // test de validit si on a rentr quelque chose
        if (!dcreneau3fin_.getText().equals("")) {
          try {
            final float i = Float.parseFloat(dcreneau3fin_.getText());
            if (i < 0) {
              JOptionPane.showMessageDialog(null, "Erreur!! Cet horaire est ngatif!!!!", "Avertissement",
                  JOptionPane.ERROR_MESSAGE);
              dcreneau3fin_.setText("");
            } else if (i > 24) {
              JOptionPane.showMessageDialog(null, "Erreur!! Cet horaire doit etre compris entre 0 et 24!!!!",
                  "Avertissement", JOptionPane.ERROR_MESSAGE);
              dcreneau3fin_.setText("");
            } else {
              // horaire fin inferieur a horaire de depart:
              if (i < Float.parseFloat(dcreneau3debut_.getText())) {
                JOptionPane.showMessageDialog(null,
                    "Erreur!! Cet horaire de fin est infererieur a l'horaire de deoart!!!!", "Avertissement",
                    JOptionPane.ERROR_MESSAGE);
                dcreneau3fin_.setText("");

              } else // cas des intervalles qui se chevauchent
              if (i > Float.parseFloat(dcreneau1debut_.getText()) && i < Float.parseFloat(dcreneau1fin_.getText())) {
                JOptionPane
                    .showMessageDialog(
                        null,
                        "Erreur!! Cet horaire est inclus dans le premier crneau!!!! Les crneaux sont des intervalles de temps distincts...",
                        "Avertissement", JOptionPane.ERROR_MESSAGE);
                dcreneau3fin_.setText("");
              } else // cas des intervalles qui se chevauchent
              if (i > Float.parseFloat(dcreneau2debut_.getText()) && i < Float.parseFloat(dcreneau2fin_.getText())) {
                JOptionPane
                    .showMessageDialog(
                        null,
                        "Erreur!! Cet horaire est inclus dans le deuxieme crneau!!!! Les crneaux sont des intervalles de temps distincts...",
                        "Avertissement", JOptionPane.ERROR_MESSAGE);
                dcreneau3fin_.setText("");
              }

            }

          } catch (final NumberFormatException nfe) {
            JOptionPane.showMessageDialog(null, "Erreur!! Ce nombre n'existe pas!!!!", "Avertissement",
                JOptionPane.ERROR_MESSAGE);
            dcreneau3fin_.setText("");
          }
        }
      }
    });

    /**
     * ********************************************** Organisation des donnes dans la frame:
     * **********************************************
     */
    dglobal_ = new JPanel();
    dglobal_.setLayout(new GridLayout(4, 1));

    // panel de saisie du premier creneau horaire
    final JPanel dch1 = new JPanel();

    dch1.add(new JLabel(" Creneau 1: Horaire: "));
    dch1.add(this.dcreneau1debut_);
    dch1.add(new JLabel("-"));
    dch1.add(this.dcreneau1fin_);
    dch1.add(new JLabel("HEURES:MINUTES"));
    dch1.setBorder(bordnormal_);
    dglobal_.add(dch1);

    // panel de saisie du deuxieme horaire
    final JPanel dch2 = new JPanel();
    dch2.add(new JLabel(" Creneau 2: Horaire: "));
    dch2.add(this.dcreneau2debut_);
    dch2.add(new JLabel("-"));
    dch2.add(this.dcreneau2fin_);
    dch2.add(new JLabel("HEURES:MINUTES"));
    dch2.setBorder(bordnormal_);
    dglobal_.add(dch2);

    // panel qui contient le troisieme horaire:
    final JPanel dch3 = new JPanel();
    dch3.add(new JLabel(" Creneau 3: Horaire: "));
    dch3.add(this.dcreneau3debut_);
    dch3.add(new JLabel("-"));
    dch3.add(this.dcreneau3fin_);
    dch3.add(new JLabel("HEURES:MINUTES"));
    dch3.setBorder(bordnormal_);
    dglobal_.add(dch3);
    // sch3.add(this.validation);
    dglobal_.add(dch3);

    dglobal_.add(new JLabel(""));
    dglobal_.setBorder(compound_);
    this.typesHoraires_.add("dimanche", dglobal_);

    /**
     * Menu ferie
     */
    this.fcreneau1debut_
        .setToolTipText("Saisissez le nombre correspondant  l'horaire d'ouverture. Par exemple, si vous voulez rentrer 8h46, tapez 8.46");
    this.fcreneau2debut_
        .setToolTipText("Saisissez le nombre correspondant  l'horaire d'ouverture. Par exemple, si vous voulez rentrer 8h46, tapez 8.46");
    this.fcreneau1fin_
        .setToolTipText("Saisissez le nombre correspondant  l'horaire de fermeture. Par exemple, si vous voulez rentrer 8h46, tapez 8.46");
    this.fcreneau2fin_
        .setToolTipText("Saisissez le nombre correspondant  l'horaire de fermeture. Par exemple, si vous voulez rentrer 8h46, tapez 8.46");
    this.fcreneau3debut_
        .setToolTipText("Saisissez le nombre correspondant  l'horaire d'ouverture. Par exemple, si vous voulez rentrer 8h46, tapez 8.46");
    this.fcreneau3fin_
        .setToolTipText("Saisissez le nombre correspondant  l'horaire de fermeture. Par exemple, si vous voulez rentrer 8h46, tapez 8.46");

    if (horaire_.ferieCreneau1HeureArrivee != -1 && horaire_.ferieCreneau2HeureArrivee != -1
        && horaire_.ferieCreneau3HeureArrivee != -1 && horaire_.ferieCreneau1HeureDep != -1
        && horaire_.ferieCreneau2HeureDep != -1 && horaire_.ferieCreneau3HeureDep != -1) {
      // valeur recuperee de la structure horaire
      this.fcreneau1debut_.setText("" + (float) horaire_.ferieCreneau1HeureDep);
      this.fcreneau1fin_.setText("" + (float) horaire_.ferieCreneau1HeureArrivee);
      this.fcreneau2debut_.setText("" + (float) horaire_.ferieCreneau2HeureDep);
      this.fcreneau2fin_.setText("" + (float) horaire_.ferieCreneau2HeureArrivee);
      this.fcreneau3debut_.setText("" + (float) horaire_.ferieCreneau3HeureDep);
      this.fcreneau3fin_.setText("" + (float) horaire_.ferieCreneau3HeureArrivee);

    } else {
      // valeur par defaut:
      this.fcreneau1debut_.setText("0.0");
      this.fcreneau1fin_.setText("24.0");
      this.fcreneau2debut_.setText("0.0");
      this.fcreneau2fin_.setText("0.0");
      this.fcreneau3debut_.setText("0.0");
      this.fcreneau3fin_.setText("0.0");

    }

    this.fcreneau1debut_.addFocusListener(new FocusAdapter() {
      public void focusLost(final FocusEvent e) {
        System.out.print("test validite de Longueur Min");
        // test de validit si on a rentr quelque chose
        if (!fcreneau1debut_.getText().equals("")) {
          try {
            final float i = Float.parseFloat(fcreneau1debut_.getText());
            if (i < 0) {
              JOptionPane.showMessageDialog(null, "Erreur!! Cet horaire est ngatif!!!!", "Avertissement",
                  JOptionPane.ERROR_MESSAGE);
              fcreneau1debut_.setText("");
            } else if (i >= 24) {
              JOptionPane.showMessageDialog(null, "Erreur!! Cet horaire doit etre compris entre 0 et 24!!!!",
                  "Avertissement", JOptionPane.ERROR_MESSAGE);
              fcreneau1debut_.setText("");
            }

          } catch (final NumberFormatException nfe) {
            JOptionPane.showMessageDialog(null, "Erreur!! Ce nombre n'existe pas!!!!", "Avertissement",
                JOptionPane.ERROR_MESSAGE);
            fcreneau1debut_.setText("");
          }
        }
      }
    });

    this.fcreneau1fin_.addFocusListener(new FocusAdapter() {
      public void focusLost(final FocusEvent e) {
        System.out.print("test validite de Longueur Min");
        // test de validit si on a rentr quelque chose
        if (!fcreneau1fin_.getText().equals("")) {
          try {
            final float i = Float.parseFloat(fcreneau1fin_.getText());
            if (i < 0) {
              JOptionPane.showMessageDialog(null, "Erreur!! Cet horaire est ngatif!!!!", "Avertissement",
                  JOptionPane.ERROR_MESSAGE);
              fcreneau1fin_.setText("");
            } else if (i > 24) {
              JOptionPane.showMessageDialog(null, "Erreur!! Cet horaire doit etre compris entre 0 et 24!!!!",
                  "Avertissement", JOptionPane.ERROR_MESSAGE);
              fcreneau1fin_.setText("");
            } else {
              // horaire fin inferieur a horaire de depart:
              if (i < Float.parseFloat(fcreneau1debut_.getText())) {
                JOptionPane.showMessageDialog(null,
                    "Erreur!! Cet horaire de fin est infererieur a l'horaire de deoart!!!!", "Avertissement",
                    JOptionPane.ERROR_MESSAGE);
                fcreneau1fin_.setText("");

              }
            }

          } catch (final NumberFormatException nfe) {
            JOptionPane.showMessageDialog(null, "Erreur!! Ce nombre n'existe pas!!!!", "Avertissement",
                JOptionPane.ERROR_MESSAGE);
            fcreneau1fin_.setText("");
          }
        }
      }
    });

    this.fcreneau2debut_.addFocusListener(new FocusAdapter() {
      public void focusLost(final FocusEvent e) {
        System.out.print("test validite de Longueur Min");
        // test de validit si on a rentr quelque chose
        if (!fcreneau2debut_.getText().equals("")) {
          try {
            final float i = Float.parseFloat(fcreneau2debut_.getText());
            if (i < 0) {
              JOptionPane.showMessageDialog(null, "Erreur!! Cet horaire est ngatif!!!!", "Avertissement",
                  JOptionPane.ERROR_MESSAGE);
              fcreneau2debut_.setText("");
            } else if (i >= 24) {
              JOptionPane.showMessageDialog(null, "Erreur!! Cet horaire doit etre compris entre 0 et 24!!!!",
                  "Avertissement", JOptionPane.ERROR_MESSAGE);
              fcreneau2debut_.setText("");
            } else // cas des intervalles qui se chevauchent
            if (i > Float.parseFloat(fcreneau1debut_.getText()) && i < Float.parseFloat(fcreneau1fin_.getText())) {
              JOptionPane
                  .showMessageDialog(
                      null,
                      "Erreur!! Cet horaire est inclus dans le premier crneau!!!! Les crneaux sont des intervalles de temps distincts...",
                      "Avertissement", JOptionPane.ERROR_MESSAGE);
              fcreneau2debut_.setText("");
            }

          } catch (final NumberFormatException nfe) {
            JOptionPane.showMessageDialog(null, "Erreur!! Ce nombre n'existe pas!!!!", "Avertissement",
                JOptionPane.ERROR_MESSAGE);
            fcreneau2debut_.setText("");
          }
        }
      }
    });

    this.fcreneau2fin_.addFocusListener(new FocusAdapter() {
      public void focusLost(final FocusEvent e) {
        System.out.print("test validite de Longueur Min");
        // test de validit si on a rentr quelque chose
        if (!fcreneau2fin_.getText().equals("")) {
          try {
            final float i = Float.parseFloat(fcreneau2fin_.getText());
            if (i < 0) {
              JOptionPane.showMessageDialog(null, "Erreur!! Cet horaire est ngatif!!!!", "Avertissement",
                  JOptionPane.ERROR_MESSAGE);
              fcreneau2fin_.setText("");
            } else if (i > 24) {
              JOptionPane.showMessageDialog(null, "Erreur!! Cet horaire doit etre compris entre 0 et 24!!!!",
                  "Avertissement", JOptionPane.ERROR_MESSAGE);
              fcreneau2fin_.setText("");
            } else {
              // horaire fin inferieur a horaire de depart:
              if (i < Float.parseFloat(fcreneau2debut_.getText())) {
                JOptionPane.showMessageDialog(null,
                    "Erreur!! Cet horaire de fin est infererieur a l'horaire de deoart!!!!", "Avertissement",
                    JOptionPane.ERROR_MESSAGE);
                fcreneau2fin_.setText("");

              } else // cas des intervalles qui se chevauchent
              if (i > Float.parseFloat(fcreneau1debut_.getText()) && i < Float.parseFloat(screneau1fin_.getText())) {
                JOptionPane
                    .showMessageDialog(
                        null,
                        "Erreur!! Cet horaire est inclus dans le premier crneau!!!! Les crneaux sont des intervalles de temps distincts...",
                        "Avertissement", JOptionPane.ERROR_MESSAGE);
                fcreneau2fin_.setText("");
              }

            }

          } catch (final NumberFormatException nfe) {
            JOptionPane.showMessageDialog(null, "Erreur!! Ce nombre n'existe pas!!!!", "Avertissement",
                JOptionPane.ERROR_MESSAGE);
            fcreneau2fin_.setText("");
          }
        }
      }
    });

    this.fcreneau3debut_.addFocusListener(new FocusAdapter() {
      public void focusLost(final FocusEvent e) {
        System.out.print("test validite de Longueur Min");
        // test de validit si on a rentr quelque chose
        if (!fcreneau3debut_.getText().equals("")) {
          try {
            final float i = Float.parseFloat(fcreneau3debut_.getText());
            if (i < 0) {
              JOptionPane.showMessageDialog(null, "Erreur!! Cet horaire est ngatif!!!!", "Avertissement",
                  JOptionPane.ERROR_MESSAGE);
              fcreneau3debut_.setText("");
            } else if (i >= 24) {
              JOptionPane.showMessageDialog(null, "Erreur!! Cet horaire doit etre compris entre 0 et 24!!!!",
                  "Avertissement", JOptionPane.ERROR_MESSAGE);
              fcreneau3debut_.setText("");
            } else // cas des intervalles qui se chevauchent
            if (i > Float.parseFloat(fcreneau1debut_.getText()) && i < Float.parseFloat(fcreneau1fin_.getText())) {
              JOptionPane
                  .showMessageDialog(
                      null,
                      "Erreur!! Cet horaire est inclus dans le premier cr�neau!!!! Les cr�neaux sont des intervalles de temps distincts...",
                      "Avertissement", JOptionPane.ERROR_MESSAGE);
              fcreneau3debut_.setText("");
            } else // cas des intervalles qui se chevauchent
            if (i > Float.parseFloat(fcreneau2debut_.getText()) && i < Float.parseFloat(fcreneau2fin_.getText())) {
              JOptionPane
                  .showMessageDialog(
                      null,
                      "Erreur!! Cet horaire est inclus dans le deuxieme cr�neau!!!! Les crneaux sont des intervalles de temps distincts...",
                      "Avertissement", JOptionPane.ERROR_MESSAGE);
              fcreneau3debut_.setText("");
            }
          } catch (final NumberFormatException nfe) {
            JOptionPane.showMessageDialog(null, "Erreur!! Ce nombre n'existe pas!!!!", "Avertissement",
                JOptionPane.ERROR_MESSAGE);
            fcreneau3debut_.setText("");
          }
        }
      }
    });

    this.fcreneau3fin_.addFocusListener(new FocusAdapter() {
      public void focusLost(final FocusEvent e) {
        System.out.print("test validite de Longueur Min");
        // test de validit si on a rentr quelque chose
        if (!fcreneau3fin_.getText().equals("")) {
          try {
            final float i = Float.parseFloat(fcreneau3fin_.getText());
            if (i < 0) {
              JOptionPane.showMessageDialog(null, "Erreur!! Cet horaire est ngatif!!!!", "Avertissement",
                  JOptionPane.ERROR_MESSAGE);
              fcreneau3fin_.setText("");
            } else if (i > 24) {
              JOptionPane.showMessageDialog(null, "Erreur!! Cet horaire doit etre compris entre 0 et 24!!!!",
                  "Avertissement", JOptionPane.ERROR_MESSAGE);
              fcreneau3fin_.setText("");
            } else {
              // horaire fin inferieur a horaire de depart:
              if (i < Float.parseFloat(fcreneau3debut_.getText())) {
                JOptionPane.showMessageDialog(null,
                    "Erreur!! Cet horaire de fin est infererieur a l'horaire de depart!!!!", "Avertissement",
                    JOptionPane.ERROR_MESSAGE);
                fcreneau3fin_.setText("");

              } else // cas des intervalles qui se chevauchent
              if (i > Float.parseFloat(fcreneau1debut_.getText()) && i < Float.parseFloat(screneau1fin_.getText())) {
                JOptionPane
                    .showMessageDialog(
                        null,
                        "Erreur!! Cet horaire est inclus dans le premier creneau!!!! Les crneaux sont des intervalles de temps distincts...",
                        "Avertissement", JOptionPane.ERROR_MESSAGE);
                fcreneau3fin_.setText("");
              } else // cas des intervalles qui se chevauchent
              if (i > Float.parseFloat(fcreneau2debut_.getText()) && i < Float.parseFloat(screneau2fin_.getText())) {
                JOptionPane
                    .showMessageDialog(
                        null,
                        "Erreur!! Cet horaire est inclus dans le premier creneau!!!! Les crneaux sont des intervalles de temps distincts...",
                        "Avertissement", JOptionPane.ERROR_MESSAGE);
                fcreneau3fin_.setText("");
              }
            }

          } catch (final NumberFormatException nfe) {
            JOptionPane.showMessageDialog(null, "Erreur!! Ce nombre n'existe pas!!!!", "Avertissement",
                JOptionPane.ERROR_MESSAGE);
            fcreneau3fin_.setText("");
          }
        }
      }
    });

    /**
     * ********************************************** Organisation des donnes dans la frame:
     * **********************************************
     */
    fglobal_ = new JPanel();
    fglobal_.setLayout(new GridLayout(4, 1));

    // panel de saisie du premier creneau horaire
    final JPanel fch1 = new JPanel();

    fch1.add(new JLabel(" Creneau 1: Horaire: "));
    fch1.add(this.fcreneau1debut_);
    fch1.add(new JLabel("-"));
    fch1.add(this.fcreneau1fin_);
    fch1.add(new JLabel("HEURES:MINUTES"));
    fch1.setBorder(bordnormal_);
    fglobal_.add(fch1);

    // panel de saisie du deuxieme horaire
    final JPanel fch2 = new JPanel();
    fch2.add(new JLabel(" Creneau 2: Horaire: "));
    fch2.add(this.fcreneau2debut_);
    fch2.add(new JLabel("-"));
    fch2.add(this.fcreneau2fin_);
    fch2.add(new JLabel("HEURES:MINUTES"));
    fch2.setBorder(bordnormal_);
    fglobal_.add(fch2);

    // panel qui contient le bouton de validation:
    final JPanel fch3 = new JPanel();
    fch3.add(new JLabel(" Creneau 3: Horaire: "));
    fch3.add(this.fcreneau3debut_);
    fch3.add(new JLabel("-"));
    fch3.add(this.fcreneau3fin_);
    fch3.add(new JLabel("HEURES:MINUTES"));
    fch3.setBorder(bordnormal_);
    fglobal_.add(fch3);
    fglobal_.add(new JLabel(""));
    fglobal_.setBorder(compound_);
    this.typesHoraires_.add("jours feries", fglobal_);

    // **************************************************************************

    this.getContentPane().add(this.typesHoraires_);

    this.validate();

  }

  /**
   * Methode booleene de validation des donnes saisies
   */
  boolean controleCreationHoraire() {

    if (this.creneau1debut_.getText().equals("")) {
      JOptionPane.showMessageDialog(null, "Erreur!!Semaine Creneau 1: heure de depart manquant!!!!", "Avertissement",
          JOptionPane.ERROR_MESSAGE);
      return false;
    }
    if (this.creneau1fin_.getText().equals("")) {
      JOptionPane.showMessageDialog(null, "Erreur!! Semaine Creneau 1: heure de fin manquant!!!!", "Avertissement",
          JOptionPane.ERROR_MESSAGE);
      return false;
    }

    if (this.creneau2debut_.getText().equals("")) {
      JOptionPane.showMessageDialog(null, "Erreur!!Semaine Creneau 2: heure de depart manquant!!!!", "Avertissement",
          JOptionPane.ERROR_MESSAGE);
      return false;
    }
    if (this.creneau2fin_.getText().equals("")) {
      JOptionPane.showMessageDialog(null, "Erreur!!Semaine Creneau 2: heure de fin manquant!!!!", "Avertissement",
          JOptionPane.ERROR_MESSAGE);
      return false;
    }
    if (this.creneau3debut_.getText().equals("")) {
      JOptionPane.showMessageDialog(null, "Erreur!!Semaine Creneau 3: heure de depart manquant!!!!", "Avertissement",
          JOptionPane.ERROR_MESSAGE);
      return false;
    }
    if (this.creneau3fin_.getText().equals("")) {
      JOptionPane.showMessageDialog(null, "Erreur!!Semaine Creneau 3: heure de fin manquant!!!!", "Avertissement",
          JOptionPane.ERROR_MESSAGE);
      return false;
    }

    if (this.screneau1debut_.getText().equals("")) {
      JOptionPane.showMessageDialog(null, "Erreur!!Samedi Creneau 1: heure de depart manquant!!!!", "Avertissement",
          JOptionPane.ERROR_MESSAGE);
      return false;
    }
    if (this.screneau1fin_.getText().equals("")) {
      JOptionPane.showMessageDialog(null, "Erreur!!Samedi Creneau 1: heure de fin manquant!!!!", "Avertissement",
          JOptionPane.ERROR_MESSAGE);
      return false;
    }

    if (this.screneau2debut_.getText().equals("")) {
      JOptionPane.showMessageDialog(null, "Erreur!!Samedi Creneau 2: heure de depart manquant!!!!", "Avertissement",
          JOptionPane.ERROR_MESSAGE);
      return false;
    }
    if (this.screneau2fin_.getText().equals("")) {
      JOptionPane.showMessageDialog(null, "Erreur!!Samedi Creneau 2: heure de fin manquant!!!!", "Avertissement",
          JOptionPane.ERROR_MESSAGE);
      return false;
    }
    if (this.screneau3debut_.getText().equals("")) {
      JOptionPane.showMessageDialog(null, "Erreur!!Samedi Creneau 3: heure de depart manquant!!!!", "Avertissement",
          JOptionPane.ERROR_MESSAGE);
      return false;
    }
    if (this.screneau3fin_.getText().equals("")) {
      JOptionPane.showMessageDialog(null, "Erreur!!Samedi Creneau 3: heure de fin manquant!!!!", "Avertissement",
          JOptionPane.ERROR_MESSAGE);
      return false;
    }

    if (this.dcreneau1debut_.getText().equals("")) {
      JOptionPane.showMessageDialog(null, "Erreur!!Dimanche Creneau 1: heure de depart manquant!!!!", "Avertissement",
          JOptionPane.ERROR_MESSAGE);
      return false;
    }
    if (this.dcreneau1fin_.getText().equals("")) {
      JOptionPane.showMessageDialog(null, "Erreur!!Dimanche Creneau 1: heure de fin manquant!!!!", "Avertissement",
          JOptionPane.ERROR_MESSAGE);
      return false;
    }

    if (this.dcreneau2debut_.getText().equals("")) {
      JOptionPane.showMessageDialog(null, "Erreur!!Dimanche Creneau 2: heure de depart manquant!!!!", "Avertissement",
          JOptionPane.ERROR_MESSAGE);
      return false;
    }
    if (this.dcreneau2fin_.getText().equals("")) {
      JOptionPane.showMessageDialog(null, "Erreur!!Dimanche Creneau 2: heure de fin manquant!!!!", "Avertissement",
          JOptionPane.ERROR_MESSAGE);
      return false;
    }
    if (this.dcreneau3debut_.getText().equals("")) {
      JOptionPane.showMessageDialog(null, "Erreur!!Dimanche Creneau 3: heure de depart manquant!!!!", "Avertissement",
          JOptionPane.ERROR_MESSAGE);
      return false;
    }
    if (this.dcreneau3fin_.getText().equals("")) {
      JOptionPane.showMessageDialog(null, "Erreur!!Dimanche Creneau 3: heure de fin manquant!!!!", "Avertissement",
          JOptionPane.ERROR_MESSAGE);
      return false;
    }

    if (this.fcreneau1debut_.getText().equals("")) {
      JOptionPane.showMessageDialog(null, "Erreur!!Jours fériés Creneau 1: heure de depart manquant!!!!",
          "Avertissement", JOptionPane.ERROR_MESSAGE);
      return false;
    }
    if (this.fcreneau1fin_.getText().equals("")) {
      JOptionPane.showMessageDialog(null, "Erreur!!Jours fériés  Creneau 1: heure de fin manquant!!!!",
          "Avertissement", JOptionPane.ERROR_MESSAGE);
      return false;
    }

    if (this.fcreneau2debut_.getText().equals("")) {
      JOptionPane.showMessageDialog(null, "Erreur!!Jours fériés  Creneau 2: heure de depart manquant!!!!",
          "Avertissement", JOptionPane.ERROR_MESSAGE);
      return false;
    }
    if (this.fcreneau2fin_.getText().equals("")) {
      JOptionPane.showMessageDialog(null, "Erreur!!Jours fériés  Creneau 2: heure de fin manquant!!!!",
          "Avertissement", JOptionPane.ERROR_MESSAGE);
      return false;
    }

    if (this.fcreneau3debut_.getText().equals("")) {
      JOptionPane.showMessageDialog(null, "Erreur!!Jours fériés  Creneau 3: heure de depart manquant!!!!",
          "Avertissement", JOptionPane.ERROR_MESSAGE);
      return false;
    }
    if (this.fcreneau3fin_.getText().equals("")) {
      JOptionPane.showMessageDialog(null, "Erreur!!Jours fériés  Creneau 3: heure de fin manquant!!!!",
          "Avertissement", JOptionPane.ERROR_MESSAGE);
      return false;
    }
    // tous les tests ont t ngatifs, les donnes sont donc correctes.
    return true;
  }

  /**
   * Methode de controle de creation des horaires
   */

  void creationHoraire() {

    if (controleCreationHoraire()) {
      System.out.println("yes: tous les controles ont t v�rifi�, les donn�es sont coh�rentes!!");

      // creation d'un nouvel objet horaire:
      horaire_.semaineCreneau1HeureDep = Float.parseFloat(this.creneau1debut_.getText());
      horaire_.semaineCreneau1HeureArrivee = Float.parseFloat(this.creneau1fin_.getText());
      horaire_.semaineCreneau2HeureDep = Float.parseFloat(this.creneau2debut_.getText());
      horaire_.semaineCreneau2HeureArrivee = Float.parseFloat(this.creneau2fin_.getText());
      horaire_.semaineCreneau3HeureDep = Float.parseFloat(this.creneau3debut_.getText());
      horaire_.semaineCreneau3HeureArrivee = Float.parseFloat(this.creneau3fin_.getText());

      horaire_.samediCreneau1HeureDep = Float.parseFloat(this.screneau1debut_.getText());
      horaire_.samediCreneau1HeureArrivee = Float.parseFloat(this.screneau1fin_.getText());
      horaire_.samediCreneau2HeureDep = Float.parseFloat(this.screneau2debut_.getText());
      horaire_.samediCreneau2HeureArrivee = Float.parseFloat(this.screneau2fin_.getText());
      horaire_.samediCreneau3HeureDep = Float.parseFloat(this.screneau3debut_.getText());
      horaire_.samediCreneau3HeureArrivee = Float.parseFloat(this.screneau3fin_.getText());

      horaire_.dimancheCreneau1HeureDep = Float.parseFloat(this.dcreneau1debut_.getText());
      horaire_.dimancheCreneau1HeureArrivee = Float.parseFloat(this.dcreneau1fin_.getText());
      horaire_.dimancheCreneau2HeureDep = Float.parseFloat(this.dcreneau2debut_.getText());
      horaire_.dimancheCreneau2HeureArrivee = Float.parseFloat(this.dcreneau2fin_.getText());
      horaire_.dimancheCreneau3HeureDep = Float.parseFloat(this.dcreneau3debut_.getText());
      horaire_.dimancheCreneau3HeureArrivee = Float.parseFloat(this.dcreneau3fin_.getText());

      horaire_.ferieCreneau1HeureDep = Float.parseFloat(this.fcreneau1debut_.getText());
      horaire_.ferieCreneau1HeureArrivee = Float.parseFloat(this.fcreneau1fin_.getText());
      horaire_.ferieCreneau2HeureDep = Float.parseFloat(this.fcreneau2debut_.getText());
      horaire_.ferieCreneau2HeureArrivee = Float.parseFloat(this.fcreneau2fin_.getText());
      horaire_.ferieCreneau3HeureDep = Float.parseFloat(this.fcreneau3debut_.getText());
      horaire_.ferieCreneau3HeureArrivee = Float.parseFloat(this.fcreneau3fin_.getText());

      // verification des donnes saisies:
      horaire_.affichage();

      // destruction de la frame:
      this.dispose();

    }

  }

}
