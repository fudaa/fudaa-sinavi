/**
 *@creation 14 nov. 06
 *@modification $Dates$
 *@license  GNU General Public Licence 2
 *@copyright   (c)1998-2006 CETMEF 2 bd Gambetta F -60231 Compiegne
 *@mail   devel@fudaa.fr 
 */
package org.fudaa.fudaa.sinavi3;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.ByteArrayInputStream;
import java.io.IOException;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.ButtonGroup;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;

import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.image.CtuluImageExport;
import org.fudaa.dodico.corba.sinavi3.SParametresCoupleDistribution;
import org.fudaa.ebli.graphe.BGraphe;
import org.fudaa.fudaa.ressource.FudaaResource;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import com.memoire.bu.BuButton;
import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuScrollPane;
import com.memoire.bu.BuTabbedPane;
import com.memoire.bu.BuTable;
import com.memoire.fu.FuLog;

/**
 * classe de gestion des resultats de la generation des bateaux propose 2 onglets: le premier propose un affichage.
 * 
 * @version $Version$
 * @author Adrien Hadoux
 */
public class Sinavi3ResultatsDureesParcours extends Sinavi3InternalFrame /*implements ActionListener */{

  /**
   * ensemble des donn�es du tableau sous la forme de data.
   */
  Object[][] data_;
  
  /**
   * ensemble des donn�es du tableau de distribution sous la forme de data.
   */
  double[][][] dataDistrib_;
  
  Object[][] dataDistribAffich_;
  
  /**
   * Graphe des attentes.
   */
  BGraphe graphe_ = new BGraphe();

  /**
   * Graphe de distribution des attentes.
   */
  ChartPanel grapheDistrib_;

  /**
   * panels principaux de la fenetre.
   */
  BuPanel panelPrincipal_ = new BuPanel();
  BuPanel panelPrincipalClassique_ = new BuPanel();
  BuPanel panelPrincipalDistrib_ = new BuPanel();
  
  /**
   * Panel de selection des preferences.
   */
  BuPanel selectionPanel_ = new BuPanel();
  BuPanel selectionPanel1_;
  String[] listeElt_ = { "tron�on", "�cluse" };
  JComboBox listeTypesDepart_ = new JComboBox(listeElt_);
  JComboBox listeElementDepart_ = new JComboBox();
  JComboBox listeTypesArrivee_ = new JComboBox(listeElt_);
  JComboBox listeElementArrivee_ = new JComboBox();
  JComboBox listeGaresDepart_ = new JComboBox();
  JComboBox listeGaresArrivee_ = new JComboBox();
  String[] listeSens_ = { "avalant", "montant" };
  JComboBox sens_ = new JComboBox(listeSens_);
 
  /**
   * Panels des options: type affichages, colonnes � faire figurer.
   */
  BuPanel optionPanelClassique_ = new BuPanel(new BorderLayout());
  BuPanel optionPanelDistrib_ = new BuPanel(new BorderLayout());

  String[] typeGraphe_ = { "Lignes", "Barres"};
  JComboBox choixTypeGraphe_ = new JComboBox(typeGraphe_);

  JCheckBox choixCumul_ = new JCheckBox("Dur�es cumul�es", false);
  JRadioButton choixNombre_ = new JRadioButton("Nombres", true);
  JRadioButton choixPercent_ = new JRadioButton("Pourcentages", false);
  
  JCheckBox[] tableauChoixNavires_;
  JCheckBox[] tableauChoixNavires2_;

  Sinavi3TextFieldInteger valSeuilHeure_ = new Sinavi3TextFieldInteger(3);
  Sinavi3TextFieldInteger valSeuilMinute_ = new Sinavi3TextFieldInteger(2);
  JCheckBox valideSeuil_ = new JCheckBox("Seuil:", false);
  int valeurSeuil_;
  boolean seuil_;
  
  Sinavi3TextFieldInteger valSeuilDistribHeure_ = new Sinavi3TextFieldInteger(3);
  Sinavi3TextFieldInteger valSeuilDistribMinute_ = new Sinavi3TextFieldInteger(2);
  JCheckBox valideSeuilDistrib_ = new JCheckBox("Seuil:", false);
  int valeurSeuilDistrib_;
  boolean seuilDistrib_;

  /**
   * Tableau r�capitulatif des r�sultats de la simulation
   */
  BuTable tableau_;
  
  /**
   * Tableau de distribution des r�sultats de la simulation
   */
  BuTable tableauDistrib_;

  String[] titreTableau_ = { "Cat�gorie", "Nombre de bateaux", "Dur�e minimale", "Dur�e moyenne", "Dur�e maximale"};
  
  String[] titreTableauDistrib_;

  /**
   * Panels tabbed qui g�re les onglets d'affichage des r�sultats:
   */
  BuTabbedPane panelPrincipalAffichage_ = new BuTabbedPane();
  BuTabbedPane panelPrincipalAffichageClassique_ = new BuTabbedPane();
  BuTabbedPane panelPrincipalAffichageDistrib_ = new BuTabbedPane();
  
  /**
   * Panel contenant le bouton quitter
   */
  BuPanel panelQuitter_ = new BuPanel();

  /**
   * Panel cniotenant le tableau et les boutns de controles
   */
  BuPanel panelGestionTableau_ = new BuPanel();
  
  /**
   * Panel contenant le tableau de distribution et les boutons de controle
   */
  BuPanel panelGestionTableauDistrib_ = new BuPanel();

  /**
   * panel de gestion du tableau et des diff�rents boutons
   */
  BuPanel panelTableau_ = new BuPanel();

  /**
   * panel de gestion des boutons
   */
  BuPanel controlPanel_ = new BuPanel();
  
  /**
   * panel de gestion des boutons du tableau des distributions
   */
  BuPanel controlPanelTableauDistrib_ = new BuPanel();
  
  /**
   * Panel de gestion des boutons des courbes
   */
  BuPanel controlPanelCourbes_ = new BuPanel();

  /**
   * panel de gestion des boutons du graphe de distribution.
   */
  BuPanel controlPanelGrapheDistrib_ = new BuPanel();

  /**
   * panel de gestion des courbes
   */
  BuPanel panelCourbe_ = new BuPanel(new BorderLayout());

  /**
   * panels de gestion des distributions
   */
  BuPanel panelTableauDistrib_ = new BuPanel();
  BuPanel panelGrapheDistrib_ = new BuPanel(new BorderLayout());

  /**
   * combolist qui permet de selectionenr les lignes deu tableau a etre affich�es:
   */
  JComboBox listeNavires_ = new JComboBox();

  /**
   * buoton de generation des resultats
   */
  private final BuButton exportationExcel_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_generer"), "Exporter");
  final BuButton exportationGraphe_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_generer"), "Exporter");
  private final BuButton exportationTableauDistrib_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_generer"), "Exporter");
  private final BuButton exportationGrapheDistrib_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_generer"), "Exporter");

  private final BuButton quitter_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_quitter"), "Quitter");
  private final BuButton lancerRecherche_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_oui"), "Rechercher");
  private final BuButton rafraichirSeuil_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_rafraichir"), "");
  private final BuButton rafraichirSeuilDistrib_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_rafraichir"), "");

  Border raisedBevel_ = BorderFactory.createRaisedBevelBorder();
  Border loweredBevel_ = BorderFactory.createLoweredBevelBorder();
  Border compound_ = BorderFactory.createCompoundBorder(raisedBevel_, loweredBevel_);
  Border bordnormal_ = BorderFactory.createEtchedBorder();
  JLabel texteDuree_ = new JLabel("");
  JLabel texteDuree2_ = new JLabel("");

  /**
   * donnees de la simulation
   */
  Sinavi3DataSimulation donnees_;

  /**
   * constructeur de la sous fenetre de gestion des resultats:
   */
  Sinavi3ResultatsDureesParcours(final Sinavi3DataSimulation _donnees) {
    super("Dur�es de parcours", true, true, true, true);

    // recuperation des donn�es de la simulation
    donnees_ = _donnees;

    // premier calcul par defaut execut�: entre le chenal 0 et le chenal 0 dans l horaire entre 0 et 24
    Sinavi3AlgorithmeDureesParcours.calcul2(donnees_, 0, 0, 0, 0, 0);

    setSize(820, 600);
    setBorder(Sinavi3Bordures.compound_);
    this.getContentPane().setLayout(new /* BorderLayout() */GridLayout(1, 1));
    this.panelPrincipal_.setLayout(new BorderLayout());
    this.getContentPane().add(this.panelPrincipal_/* ,BorderLayout.CENTER */);
    this.panelPrincipal_.add(this.selectionPanel_, BorderLayout.NORTH);

    this.panelPrincipal_.add(this.panelPrincipalAffichage_, BorderLayout.CENTER);
    panelPrincipalAffichage_.addTab("R�sultats de synth�se (extremums, moyennes)", this.panelPrincipalClassique_);
    panelPrincipalAffichage_.addTab("Distribution des dur�es", this.panelPrincipalDistrib_);
    
    this.panelPrincipal_.add(panelQuitter_, BorderLayout.SOUTH);
    panelQuitter_.add(quitter_);

    this.panelPrincipalClassique_.setLayout(new BorderLayout());
    this.panelPrincipalClassique_.add(this.optionPanelClassique_, BorderLayout.WEST);
    this.panelPrincipalClassique_.add(panelPrincipalAffichageClassique_, BorderLayout.CENTER);
    panelPrincipalAffichageClassique_.addTab("Tableau", FudaaResource.FUDAA.getIcon("crystal_arbre"), panelGestionTableau_);
    panelPrincipalAffichageClassique_.addTab("Exploitation graphique", FudaaResource.FUDAA.getIcon("crystal_graphe"), panelCourbe_);
    
    this.panelPrincipalDistrib_.setLayout(new BorderLayout());
    this.panelPrincipalDistrib_.add(this.optionPanelDistrib_, BorderLayout.WEST);
    this.panelPrincipalDistrib_.add(panelPrincipalAffichageDistrib_, BorderLayout.CENTER);
    panelPrincipalAffichageDistrib_.addTab("Tableau", FudaaResource.FUDAA.getIcon("crystal_arbre"), panelGestionTableauDistrib_);
    panelPrincipalAffichageDistrib_.addTab("Graphe", FudaaResource.FUDAA.getIcon("crystal_graphe"), panelGrapheDistrib_);

    tableauChoixNavires_ = new JCheckBox[this.donnees_.listeBateaux_.listeNavires_.size()];
    for (int i = 0; i < this.donnees_.listeBateaux_.listeNavires_.size(); i++) {
      this.tableauChoixNavires_[i] = new JCheckBox(this.donnees_.listeBateaux_.retournerNavire(i).nom, true);
      this.tableauChoixNavires_[i].addActionListener(this);
    }
    
    tableauChoixNavires2_ = new JCheckBox[this.donnees_.listeBateaux_.listeNavires_.size()];
    for (int i = 0; i < this.donnees_.listeBateaux_.listeNavires_.size(); i++) {
      this.tableauChoixNavires2_[i] = new JCheckBox(this.donnees_.listeBateaux_.retournerNavire(i).nom, true);
      this.tableauChoixNavires2_[i].addActionListener(this);
    }
    
    for (int i = 0; i < this.donnees_.listeGare_.listeGares_.size(); i++) {
      this.listeGaresDepart_.addItem(this.donnees_.listeGare_.retournerGare(i));
      this.listeGaresArrivee_.addItem(this.donnees_.listeGare_.retournerGare(i));

    }
    this.listeGaresDepart_.addActionListener(this);
    this.listeGaresArrivee_.addActionListener(this);

    /*******************************************************************************************************************
     * gestion du panel de selection
     ******************************************************************************************************************/
    this.selectionPanel_.setLayout(new GridLayout(2, 1));

    selectionPanel1_ = new BuPanel(new FlowLayout(FlowLayout.LEFT));
    selectionPanel1_.add(new JLabel("Dur�es de parcours"));
    texteDuree_.setText("entre l'entr�e de l'�l�ment");
    selectionPanel1_.add(texteDuree_);
    selectionPanel1_.add(this.listeTypesDepart_);
    selectionPanel1_.add(this.listeElementDepart_);
    texteDuree2_.setText(" et la sortie de l'�l�ment");
    selectionPanel1_.add(texteDuree2_);
    selectionPanel1_.add(this.listeTypesArrivee_);
    selectionPanel1_.add(this.listeElementArrivee_);

    this.selectionPanel_.add(selectionPanel1_);

    final BuPanel selectionPanel2 = new BuPanel(new FlowLayout(FlowLayout.LEFT));
    selectionPanel2.add(new JLabel("Sens de parcours:"));
    selectionPanel2.add(this.sens_);
    selectionPanel2.add(lancerRecherche_);

    this.selectionPanel_.add(selectionPanel2);

    final TitledBorder bordurea = BorderFactory.createTitledBorder(BorderFactory
    		.createEtchedBorder(EtchedBorder.LOWERED), "D�finition de la recherche");
    this.selectionPanel_.setBorder(bordurea);

    // listener des liste box
    final ActionListener _remplissageElement = new ActionListener() {
      public void actionPerformed(ActionEvent _e) {

        int selection = 0;
        if (_e.getSource() == listeTypesDepart_) {
          selection = listeTypesDepart_.getSelectedIndex();
        } else {
          selection = listeTypesArrivee_.getSelectedIndex();
        }
        // "quai","ecluse","chenal","cercle","bassin"
        switch (selection) {
        case 1:
          if (_e.getSource() == listeTypesDepart_) {
            listeElementDepart_.removeAllItems();
            for (int i = 0; i < donnees_.listeEcluse_.listeEcluses_.size(); i++) {
              listeElementDepart_.addItem(donnees_.listeEcluse_.retournerEcluse(i).nom_);
            }
            listeElementDepart_.validate();
          } else {
            listeElementArrivee_.removeAllItems();
            for (int i = 0; i < donnees_.listeEcluse_.listeEcluses_.size(); i++) {
              listeElementArrivee_.addItem(donnees_.listeEcluse_.retournerEcluse(i).nom_);
            }
            listeElementArrivee_.validate();
          }
          break;
        case 0:
          if (_e.getSource() == listeTypesDepart_) {
            listeElementDepart_.removeAllItems();
            for (int i = 0; i < donnees_.listeBief_.listeBiefs_.size(); i++) {
              listeElementDepart_.addItem(donnees_.listeBief_.retournerBief(i).nom_);
            }
            listeElementDepart_.validate();
          } else {
            listeElementArrivee_.removeAllItems();
            for (int i = 0; i < donnees_.listeBief_.listeBiefs_.size(); i++) {
              listeElementArrivee_.addItem(donnees_.listeBief_.retournerBief(i).nom_);
            }
            listeElementArrivee_.validate();
          }
          break;
        default:
          break;
        }

      }
    };
    this.listeTypesDepart_.addActionListener(_remplissageElement);
    this.listeTypesArrivee_.addActionListener(_remplissageElement);
    this.listeElementDepart_.addActionListener(this);
    this.listeElementArrivee_.addActionListener(this);

    this.listeTypesDepart_.setSelectedIndex(0);
    this.listeTypesArrivee_.setSelectedIndex(0);
    this.listeElementArrivee_.setSelectedIndex(0);
   
    this.lancerRecherche_.addActionListener(this);
    
    /*******************************************************************************************************************
     * gestion du panel des options pour les r�sultats de synth�se
     ******************************************************************************************************************/
    BuGridLayout lo=new BuGridLayout(1,5,0,true,false,false,false);
    final BuPanel panoption = new BuPanel(lo);
    final BuScrollPane panoptionGestionScroll = new BuScrollPane(panoption);
    this.optionPanelClassique_.add(panoptionGestionScroll);

    final Box bVert2 = Box.createVerticalBox();
    for (int i = 0; i < this.tableauChoixNavires_.length; i++) {
      bVert2.add(this.tableauChoixNavires_[i]);
    }
    final TitledBorder bordure2 = BorderFactory.createTitledBorder(BorderFactory
        .createEtchedBorder(EtchedBorder.LOWERED), "Cat�gories");
    bVert2.setBorder(bordure2);
    panoption.add(bVert2);

    /*******************************************************************************************************************
     * gestion du panel des options relatives aux distributions
     ******************************************************************************************************************/
    
    BuPanel panoptionDistrib = new BuPanel(lo);
    final BuScrollPane panoptionDistribGestionScroll = new BuScrollPane(panoptionDistrib);
    this.optionPanelDistrib_.add(panoptionDistribGestionScroll);

    ButtonGroup bg=new ButtonGroup();
    bg.add(this.choixNombre_);
    bg.add(this.choixPercent_);
    
    final Box bVertDistrib = Box.createVerticalBox();
    panoptionDistrib.add(bVertDistrib);
    bVertDistrib.add(this.choixCumul_);
    bVertDistrib.add(this.choixNombre_);
    bVertDistrib.add(this.choixPercent_);
    
    final TitledBorder bordureDistrib1 = BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED), "Options");
        bVertDistrib.setBorder(bordureDistrib1);
    
    final Box bVertDistrib2 = Box.createVerticalBox();
    for (int i = 0; i < this.tableauChoixNavires2_.length; i++) {
      bVertDistrib2.add(this.tableauChoixNavires2_[i]);
    }

    final TitledBorder bordureDistrib2 = BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED), "Cat�gories");
    bVertDistrib2.setBorder(bordureDistrib2);
    panoptionDistrib.add(bVertDistrib2);
    
    
    /*******************************************************************************************************************
     * gestion du panel tableau panelGestionTableau_
     ******************************************************************************************************************/

    // etape 1: architecture du panel panelGestionTableau_
    panelGestionTableau_.setLayout(new BorderLayout());

    // definition d un panel ascenceur pour stocer le tableau:
   
    // ajout au centre du panel qui contiendra le tableau d affichage
    this.panelGestionTableau_.add(panelTableau_, BorderLayout.CENTER);

    // panel qui contient les differents boutons
    this.controlPanel_.add(exportationExcel_);
    final Border bordureTab = BorderFactory.createEtchedBorder();
    controlPanel_.setBorder(bordureTab);
    this.panelGestionTableau_.add(controlPanel_, BorderLayout.SOUTH);

    // etape 2: remplissage du comboliste avec les noms des navires
    this.listeNavires_.addItem("Tous");
    for (int i = 0; i < donnees_.listeBateaux_.listeNavires_.size(); i++) {
      this.listeNavires_.addItem("" + donnees_.listeBateaux_.retournerNavire(i).nom);
    }

    // etape 3: gestion de l affichage du tableau de donn�es
    affichageTableau(-1);

    // etape 4: listener du combolist afin de pouvoir selectionner le navire qui nous interesse
    this.listeNavires_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent _e) {
        // evenement du clic sur le bouton
        final int val = listeNavires_.getSelectedIndex();
        affichageTableau(val - 1);

      }
    });

    // bouton qui permet de generer le contenu du tableau en ficheir excel:
    this.exportationExcel_.setToolTipText("Exporte le contenu du tableau dans un fichier xls");
    exportationExcel_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent _e) {
    	  Sinavi3GestionResultats.exportTableau(Sinavi3ResultatsDureesParcours.this, titreTableau_, data_);
         // Sinavi3GestionResultats.exportReport(_donnees, true,false,false,false,false,false);
      }
    });

    /*******************************************************************************************************************
     * gestion du panel courbes panelCourbe_
     ******************************************************************************************************************/

    final String descriptionGraphe = affichageGraphe();
    this.graphe_.setFluxDonnees(new ByteArrayInputStream(descriptionGraphe.getBytes()));
    this.panelCourbe_.add(this.graphe_, BorderLayout.CENTER);

    // bouton de generation du fichier image
    exportationGraphe_.setToolTipText("Exporte le graphe au format image");
    exportationGraphe_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent _e) {

        CtuluImageExport.exportImageFor(donnees_.application_, graphe_);
      }
    });
    
    rafraichirSeuil_.setEnabled(false);
    rafraichirSeuil_.setToolTipText("Actualise la valeur du seuil graphique");

    // creation du panel des boutons des courbes:
    this.panelCourbe_.add(controlPanelCourbes_, BorderLayout.SOUTH);

    this.controlPanelCourbes_.add(exportationGraphe_);
    this.controlPanelCourbes_.add(new JLabel("          "));
    this.controlPanelCourbes_.add(valideSeuil_);
    this.controlPanelCourbes_.add(valSeuilHeure_);
    this.controlPanelCourbes_.add(new JLabel("h"));
    this.controlPanelCourbes_.add(valSeuilMinute_);
    this.controlPanelCourbes_.add(new JLabel("min"));
    this.controlPanelCourbes_.add(rafraichirSeuil_);
    this.controlPanelCourbes_.add(new JLabel("          "));
    this.controlPanelCourbes_.add(choixTypeGraphe_);
    
    choixTypeGraphe_.setToolTipText("Modifie le style de la repr�sentation graphique");
    
    final Border bordure = BorderFactory.createEtchedBorder();
    controlPanelCourbes_.setBorder(bordure);

    valideSeuil_.addActionListener(this);
    rafraichirSeuil_.addActionListener(this);
    choixTypeGraphe_.addActionListener(this);

    /** listener des boutons quitter */
    this.quitter_.setToolTipText("Ferme la sous-fen�tre");
    final ActionListener actionQuitter = new ActionListener() {
      public void actionPerformed(ActionEvent _e) {
        Sinavi3ResultatsDureesParcours.this.windowClosed();
      }
    };
    this.quitter_.addActionListener(actionQuitter);

    
    /*******************************************************************************************************************
     * gestion du panel du tableau de distribution panelTableauDistrib_
     ******************************************************************************************************************/

    // etape 1: architecture du panel panelGestionTableau_
    panelGestionTableauDistrib_.setLayout(new BorderLayout());

    // definition d un panel ascenceur pour stocer le tableau:
    final JScrollPane ascDistrib = new JScrollPane(this.panelTableauDistrib_);

    // ajout au centre du panel qui contiendra le tableau d affichage
    this.panelGestionTableauDistrib_.add(ascDistrib, BorderLayout.CENTER);
    this.controlPanelTableauDistrib_.add(exportationTableauDistrib_);
    final Border bordureTabDis = BorderFactory.createEtchedBorder();
    controlPanelTableauDistrib_.setBorder(bordureTabDis);
    this.panelGestionTableauDistrib_.add(controlPanelTableauDistrib_, BorderLayout.SOUTH);

    // etape 3: gestion de l affichage du tableau de donn�es
    // remarque : cette m�thode sera syst�matiquement appel�e afni d'op�rer un changement:
    affichageTableauDistribution();
    
    // bouton qui permet de generer le contenu du tableau en fichier xls:
    this.exportationTableauDistrib_.setToolTipText("Exporte le contenu du tableau dans un fichier xls");
    exportationTableauDistrib_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent _e) {
    	  Sinavi3GestionResultats.exportTableau(Sinavi3ResultatsDureesParcours.this, titreTableauDistrib_, dataDistribAffich_);
      }
    });
    
    /*******************************************************************************************************************
     * gestion du panel du graphe de distribution panelGrapheDistrib_
     ******************************************************************************************************************/
    affichageGrapheDistrib(false);
    this.panelGrapheDistrib_.add(grapheDistrib_,BorderLayout.CENTER);
    this.panelGrapheDistrib_.add(this.controlPanelGrapheDistrib_, BorderLayout.SOUTH);
    
    rafraichirSeuilDistrib_.setEnabled(false);
    rafraichirSeuilDistrib_.setToolTipText("Actualise la valeur du seuil graphique");
    
    choixCumul_.addActionListener(this);
    choixPercent_.addActionListener(this);
    choixNombre_.addActionListener(this);
    
    BuPanel exportDistrib = new BuPanel();
    exportDistrib.add(exportationGrapheDistrib_);
    this.controlPanelGrapheDistrib_.add(exportationGrapheDistrib_);
    this.controlPanelGrapheDistrib_.add(new JLabel("          "));
    this.controlPanelGrapheDistrib_.add(valideSeuilDistrib_);
    this.controlPanelGrapheDistrib_.add(valSeuilDistribHeure_);
    this.controlPanelGrapheDistrib_.add(new JLabel("h"));
    this.controlPanelGrapheDistrib_.add(valSeuilDistribMinute_);
    this.controlPanelGrapheDistrib_.add(new JLabel("min"));
    this.controlPanelGrapheDistrib_.add(rafraichirSeuilDistrib_);
    
    final Border bordure0 = BorderFactory.createEtchedBorder();
    controlPanelGrapheDistrib_.setBorder(bordure0);
    
    valideSeuilDistrib_.addActionListener(this);
    
    this.exportationGrapheDistrib_.setToolTipText("Exporte le graphe au format image");
    exportationGrapheDistrib_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent _e) {
    	  try {
			grapheDistrib_.doSaveAs();
		} catch (IOException e) {
			FuLog.error(e);
		}
      }
    });
    
  }
  
  public static Object[] computeDataAverage(Sinavi3DataSimulation data, int element1, int typeElement1, int element2, int typeElement2, int sens) {
   Object[][] result =  computeData(data, element1, typeElement1, element2, typeElement2,  sens);
  return result[result.length-1];
   
}
  
public static Object[][] computeData(Sinavi3DataSimulation data, int element1, int typeElement1, int element2, int typeElement2, int sens) {
   Sinavi3AlgorithmeDureesParcours.calcul2(data, typeElement1, element1, typeElement2, element2, sens);
  return computeData(data, -1,null);
   
}
  public static Object[][] computeData(Sinavi3DataSimulation data, int indiceBateau, JCheckBox[] tableauChoixNavires_) {
       Object[][] data_ = new Object[data.listeBateaux_.listeNavires_.size()+1][6];
    
    int totalNb = 0;
    double minDuree = 9999999;
    double maxDuree = 0;
    double totalDuree = 0;
    
    int indiceNavire = 0;
    if (indiceBateau < 0) {

      for (int i = 0; i < data.listeBateaux_.listeNavires_.size(); i++) {
        if (tableauChoixNavires_== null || tableauChoixNavires_[i].isSelected()) {
          data_[indiceNavire][0] = data.listeBateaux_.retournerNavire(i).nom;
          if (data.params_.ResultatsCompletsSimulation.DureeParcoursCategorie[i].dureeParcoursTotale != 0) {
            // ecriture des donn�es calcul�es pour les dur�es de parcours
            // si les cases correspondantes ont �t� coch�es:
            int indiceColonne = 1;
            data_[indiceNavire][indiceColonne++] = ""
            	+ FonctionsSimu.diviserSimu(data.params_.ResultatsCompletsSimulation.DureeParcoursCategorie[i].nombreNavires);
            data_[indiceNavire][indiceColonne++] = ""
            	+ Sinavi3TraduitHoraires
            	.traduitMinutesEnHeuresMinutes3((float) data.params_.ResultatsCompletsSimulation.DureeParcoursCategorie[i].dureeMinimumParcours);
            data_[indiceNavire][indiceColonne++] = ""
            	+ Sinavi3TraduitHoraires
            	.traduitMinutesEnHeuresMinutes3((float) (data.params_.ResultatsCompletsSimulation.DureeParcoursCategorie[i].dureeParcoursTotale / data.params_.ResultatsCompletsSimulation.DureeParcoursCategorie[i].nombreNavires));
            data_[indiceNavire][indiceColonne++] = ""
            	+ Sinavi3TraduitHoraires
            	.traduitMinutesEnHeuresMinutes3((float) data.params_.ResultatsCompletsSimulation.DureeParcoursCategorie[i].dureeMaximumParcours);
            data_[indiceNavire][indiceColonne++] = ""
            	+ Sinavi3TraduitHoraires
            	.traduitMinutesEnHeuresMinutes3((float) data.params_.ResultatsCompletsSimulation.DureeParcoursCategorie[i].dureeParcoursTotale);
          }
          totalNb += data.params_.ResultatsCompletsSimulation.DureeParcoursCategorie[i].nombreNavires;
          if (data.params_.ResultatsCompletsSimulation.DureeParcoursCategorie[i].dureeMinimumParcours < minDuree && data.params_.ResultatsCompletsSimulation.DureeParcoursCategorie[i].dureeMinimumParcours > 0)
        	  minDuree = data.params_.ResultatsCompletsSimulation.DureeParcoursCategorie[i].dureeMinimumParcours;
          if (data.params_.ResultatsCompletsSimulation.DureeParcoursCategorie[i].dureeMaximumParcours > maxDuree) maxDuree = data.params_.ResultatsCompletsSimulation.DureeParcoursCategorie[i].dureeMaximumParcours;
          totalDuree += data.params_.ResultatsCompletsSimulation.DureeParcoursCategorie[i].dureeParcoursTotale;
          indiceNavire++;
        }// fin du if: si le navire est selectionn� par l utilisateur:
      }
      
      // remplissage de la ligne TOTAL
      if (minDuree == 9999999) minDuree = 0;
      data_[indiceNavire][0] = "TOTAL";
      int indiceColonne = 1;
      data_[indiceNavire][indiceColonne++] = "" + FonctionsSimu.diviserSimu(totalNb);
      data_[indiceNavire][indiceColonne++] = "" + Sinavi3TraduitHoraires.traduitMinutesEnHeuresMinutes3((float) minDuree);
      data_[indiceNavire][indiceColonne++] = "" + Sinavi3TraduitHoraires.traduitMinutesEnHeuresMinutes3((float) totalDuree/totalNb);
      data_[indiceNavire][indiceColonne++] = "" + Sinavi3TraduitHoraires.traduitMinutesEnHeuresMinutes3((float) maxDuree);
      data_[indiceNavire][indiceColonne++] = "" + Sinavi3TraduitHoraires.traduitMinutesEnHeuresMinutes3((float) totalDuree);

    } else if (indiceBateau < data.listeBateaux_.listeNavires_.size()) {
      // on affiche uniquement la ligne selectionn� par le combolist:
      data_ = new Object[1][6];
      data_[0][0] = data.listeBateaux_.retournerNavire(indiceBateau).nom;
      // on complete les don�nes par le tableau de resultats:
      // data[0][0]..........
      int indiceColonne = 1;
      data_[0][indiceColonne++] = ""
    	  + FonctionsSimu.diviserSimu(data.params_.ResultatsCompletsSimulation.DureeParcoursCategorie[indiceBateau].nombreNavires);
      data_[0][indiceColonne++] = ""
    	  + Sinavi3TraduitHoraires
    	  .traduitMinutesEnHeuresMinutes3((float) data.params_.ResultatsCompletsSimulation.DureeParcoursCategorie[indiceBateau].dureeMinimumParcours);
      data_[0][indiceColonne++] = ""
    	  + Sinavi3TraduitHoraires
    	  .traduitMinutesEnHeuresMinutes3((float) (data.params_.ResultatsCompletsSimulation.DureeParcoursCategorie[indiceBateau].dureeParcoursTotale / data.params_.ResultatsCompletsSimulation.DureeParcoursCategorie[indiceBateau].nombreNavires));
      data_[0][indiceColonne++] = ""
    	  + Sinavi3TraduitHoraires
    	  .traduitMinutesEnHeuresMinutes3((float) data.params_.ResultatsCompletsSimulation.DureeParcoursCategorie[indiceBateau].dureeMaximumParcours);
      data_[0][indiceColonne++] = ""
    	  + Sinavi3TraduitHoraires
    	  .traduitMinutesEnHeuresMinutes3((float) data.params_.ResultatsCompletsSimulation.DureeParcoursCategorie[indiceBateau].dureeParcoursTotale);
    }
    
    return data_;
  }


  /**
   * Methode d'affichage et de rafraichissement du tableau.
   * 
   * @param val entier qui indique le num�ro de la cat�gorie de navire � afficher si ce parametre vaut -1 alorso n
   *          affiche la totalit� des navires
   */
  void affichageTableau(final int _val) {
    // affichage du tableau

    // operation magique qui permet de determiner ce tableau

    // etape 2: g�n�rer la liste des donn�es � afficher dans le tableau via une matrice de type object
    // ici le nombre de colonnes est de 2 puisqu'il s'agit d'un affichage du nom de la cat�gorie et de son nombre de
    // navires
    data_ =  computeData(donnees_,_val, tableauChoixNavires_);
    
    // etape 3: creation du tableau a partir des donn�es g�n�r�es ci dessus:
    this.tableau_ = new BuTable(data_, this.titreTableau_) {
      public boolean isCellEditable(final int _row, final int _col) {
        return false;
      }
    };

    // etape 4: ajout sdu tableau cr�� dans l'interface
    org.fudaa.fudaa.sinavi.factory.ColumnAutoSizer.sizeColumnsToFit(tableau_);
    tableau_.revalidate();
    this.panelTableau_.removeAll();
    this.panelTableau_.setLayout(new BorderLayout());
    this.panelTableau_.add(tableau_.getTableHeader(), BorderLayout.PAGE_START);
    this.panelTableau_.add(new JScrollPane(this.tableau_), BorderLayout.CENTER);
    tableau_.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);

    // mise a jour de l'affichage
    this.revalidate();
    this.updateUI();

  }
  
  /**
   * Methode d'affichage du tableau de distribution.
   */
  void affichageTableauDistribution() {

	final int nbIntervalles = this.donnees_.params_.ResultatsCompletsSimulation.nombreIntervallesDistribution;
	
    // Objet dataDistrib : 4 tableaux : [0] non cumul� en nb, [1] non cumul� en %, [2] cumul� en nb, [3] cumul� en %
	dataDistrib_ = new double[4][nbIntervalles+1][this.donnees_.listeBateaux_.listeNavires_.size()+1];
    
	// Cr�ation des titres de lignes et de colonnes
	String[] nomsBateaux = new String[this.donnees_.listeBateaux_.listeNavires_.size()];
    String[] intervalles = new String[nbIntervalles+1];
    int indiceBateau = 0;
    for (int i = 0; i < nbIntervalles; i++)
    {
    	intervalles[i] = "" + Sinavi3TraduitHoraires.traduitMinutesEnHeuresMinutes3((float) (this.donnees_.params_.ResultatsCompletsSimulation.DureeParcoursCategorie[0].valeurIntervalleDistribution * i))
    			+ " � " + Sinavi3TraduitHoraires.traduitMinutesEnHeuresMinutes3((float) (this.donnees_.params_.ResultatsCompletsSimulation.DureeParcoursCategorie[0].valeurIntervalleDistribution * (i+1)));
    }
    
    int value; //entier de stockage temporaire
    int[] valueCumulHorizontal = new int[nbIntervalles+1]; // Cumul sur chaque ligne
    int[] valueCumulVertical = new int[donnees_.listeBateaux_.listeNavires_.size()+2]; // Cumul sur chaque colonne
    int nbBateauxTotal = 0;
    intervalles[nbIntervalles] = "TOTAL";
    for (int j = 0; j < donnees_.listeBateaux_.listeNavires_.size(); j++)
    {
    	if (this.tableauChoixNavires2_[j].isSelected())
    	{
   			for (int i = 0; i < nbIntervalles; i++)
   			{
   				value = this.donnees_.params_.ResultatsCompletsSimulation.DureeParcoursCategorie[j].distribution[i];
   				valueCumulHorizontal[i] += value;
   				valueCumulVertical[indiceBateau] += value;
   				dataDistrib_[0][i][indiceBateau] = FonctionsSimu.diviserSimu(value);
   				dataDistrib_[2][i][indiceBateau] = FonctionsSimu.diviserSimu(valueCumulVertical[indiceBateau]);
   			}
   			//valueCumulHorizontal[nbIntervalles] += valueCumulVertical[indiceBateau];
   			nomsBateaux[indiceBateau] = this.donnees_.listeBateaux_.retournerNavire(j).nom;
   			nbBateauxTotal += this.donnees_.params_.ResultatsCompletsSimulation.DureeParcoursCategorie[j].nombreNavires;
   			dataDistrib_[0][nbIntervalles][indiceBateau] = FonctionsSimu.diviserSimu(this.donnees_.params_.ResultatsCompletsSimulation.DureeParcoursCategorie[j].nombreNavires);
   			dataDistrib_[2][nbIntervalles][indiceBateau] = FonctionsSimu.diviserSimu(this.donnees_.params_.ResultatsCompletsSimulation.DureeParcoursCategorie[j].nombreNavires);
    		indiceBateau++;
    	}
    	else
    	{
    		for (int i = 0; i < nbIntervalles; i++)
    		{
    			value = this.donnees_.params_.ResultatsCompletsSimulation.DureeParcoursCategorie[j].distribution[i];
    			valueCumulHorizontal[i] += value;
    		}
    		nbBateauxTotal += this.donnees_.params_.ResultatsCompletsSimulation.DureeParcoursCategorie[j].nombreNavires;
    	}
    }
    
    final int nombreCategories = indiceBateau; // r�cup�ration de la largeur du tableau
    
    // Construction du vecteur des titres de colonnes
    this.titreTableauDistrib_ = new String[nombreCategories+2];
    this.titreTableauDistrib_[0] = "Dur�e";
    this.titreTableauDistrib_[nombreCategories+1] = "Flotte enti�re";
    for (int j = 0; j < nombreCategories; j++)
    {
    	this.titreTableauDistrib_[j+1] = nomsBateaux[j];
    }
    
    //Remplissage de la colonne TOTAL
    value = 0;
    int valueCumulTotal = 0;
    for (int i = 0; i < nbIntervalles; i++)
    {
    	dataDistrib_[0][i][nombreCategories] = FonctionsSimu.diviserSimu(valueCumulHorizontal[i]);
    	valueCumulTotal += valueCumulHorizontal[i];
    	dataDistrib_[2][i][nombreCategories] = FonctionsSimu.diviserSimu(valueCumulTotal);
    }
    dataDistrib_[0][nbIntervalles][nombreCategories] = FonctionsSimu.diviserSimu(nbBateauxTotal);
    dataDistrib_[2][nbIntervalles][nombreCategories] = FonctionsSimu.diviserSimu(nbBateauxTotal);
    
    // A partir de dataDistrib[0] et [2] (valeurs en nombre), on en d�duit [1] et [3] (valeurs en pourcentage)
    for (int i = 0; i < nbIntervalles+1; i++)
    {
    	for (int j = 0; j < nombreCategories+1; j++)
    	{
    		dataDistrib_[1][i][j] = Math.round(dataDistrib_[0][i][j] * 100. / dataDistrib_[0][nbIntervalles][j]);
    		dataDistrib_[3][i][j] = Math.round(dataDistrib_[2][i][j] * 100. / dataDistrib_[2][nbIntervalles][j]);
    	}
    }
    
    // Construction du tableau que l'on souhaite afficher
    dataDistribAffich_ = new Object[nbIntervalles+1][nombreCategories+2];
    for (int i = 0; i < nbIntervalles+1; i++)
    {
    	dataDistribAffich_[i][0] = intervalles[i];
    	for (int j = 1; j < nombreCategories+2; j++)
    	{
    		if (dataDistrib_[0][nbIntervalles][j-1] > 0) // On �limine les z�ros pour all�ger le tableau
    		{
    			if (choixCumul_.isSelected())
    			{
    				if (choixPercent_.isSelected()) dataDistribAffich_[i][j] = "" + (int) dataDistrib_[3][i][j-1] + "%";
    				else dataDistribAffich_[i][j] = "" + (int) dataDistrib_[2][i][j-1];
    			}
    			else
    			{
    				if (choixPercent_.isSelected()) dataDistribAffich_[i][j] = "" + (int) dataDistrib_[1][i][j-1] + "%";
    				else dataDistribAffich_[i][j] = "" + (int) dataDistrib_[0][i][j-1];
    			}
    		}
    		else dataDistribAffich_[i][j] = "";
    	}
    }
    
    //Creation du tableau a partir des donn�es g�n�r�es ci dessus:
    this.tableauDistrib_ = new BuTable(dataDistribAffich_, this.titreTableauDistrib_) {
      public boolean isCellEditable(final int _row, final int _col) {
        return false;
      }
    };

    // etape 4: ajout sdu tableau cr�� dans l'interface
    tableauDistrib_.revalidate();
    this.panelTableauDistrib_.removeAll();
    this.panelTableauDistrib_.setLayout(new BorderLayout());
    this.panelTableauDistrib_.add(tableauDistrib_.getTableHeader(), BorderLayout.PAGE_START);
    this.panelTableauDistrib_.add(this.tableauDistrib_, BorderLayout.CENTER);
    // this.panelTableau_.add(this.controlPanel_,BorderLayout.SOUTH);

    // mise a jour de l'affichage
    this.revalidate();
    this.updateUI();

  }
  
  
  /**
   * Methode de rafraichissement du tableau de distribution.
   */
  void rafraichissementTableauDistribution() {
	  // Construction du tableau que l'on souhaite afficher
	  final int nbIntervalles = this.donnees_.params_.ResultatsCompletsSimulation.nombreIntervallesDistribution;
	  final int nombreCategories = dataDistribAffich_[0].length-2;
	  for (int i = 0; i < nbIntervalles+1; i++)
	  {
		  for (int j = 1; j < nombreCategories+2; j++)
		  {
			  if (dataDistrib_[0][nbIntervalles][j-1] > 0) // On �limine les z�ros pour all�ger le tableau
			  {
				  if (choixCumul_.isSelected())
				  {
					  if (choixPercent_.isSelected()) dataDistribAffich_[i][j] = "" + (int) dataDistrib_[3][i][j-1] + "%";
					  else dataDistribAffich_[i][j] = "" + (int) dataDistrib_[2][i][j-1];
				  }
				  else
				  {
					  if (choixPercent_.isSelected()) dataDistribAffich_[i][j] = "" + (int) dataDistrib_[1][i][j-1] + "%";
					  else dataDistribAffich_[i][j] = "" + (int) dataDistrib_[0][i][j-1];
				  }
			  }
			  else dataDistribAffich_[i][j] = "";
		  }
	  }
	    
	  // etape 4: ajout du tableau cr�� dans l'interface
	  tableauDistrib_.revalidate();
	  this.panelTableauDistrib_.removeAll();
	  this.panelTableauDistrib_.setLayout(new BorderLayout());
	  this.panelTableauDistrib_.add(tableauDistrib_.getTableHeader(), BorderLayout.PAGE_START);
	  this.panelTableauDistrib_.add(this.tableauDistrib_, BorderLayout.CENTER);
	  // this.panelTableau_.add(this.controlPanel_,BorderLayout.SOUTH);

	  // mise a jour de l'affichage
	  this.revalidate();
	  this.updateUI();
  }
  

  /**
   * Methode qui permet de d�crire le graphe � afficher.
   * 
   * @return chaine: chaine qui contient la des cription de la chaine de caracteres.
   */
  String affichageGraphe() {
	  
	  boolean echelleHeures=false;
	  if (Sinavi3AlgorithmeDureesParcours.determineDureeMaxi(donnees_) >= 240) echelleHeures=true;
	  
	  boolean histo = false;
	  if (this.choixTypeGraphe_.getSelectedIndex() == 1) histo = true;

    // determiner el nombre de cat�gories de navires selectionn�s
    int nbNavires = 0;
    for (int k = 0; k < this.tableauChoixNavires_.length; k++) {
      if (this.tableauChoixNavires_[k].isSelected()) {
        nbNavires++;
      }
    }

    // variable qui permet de determiner a quel indice du navire on est car on en prends pas tous les indices de navires
    int indiceNavire = 0;

    String g = "";

    g += "graphe\n{\n";
    if (this.listeElementDepart_.getSelectedIndex() == this.listeElementArrivee_.getSelectedIndex()
        && this.listeTypesDepart_.getSelectedIndex() == this.listeTypesArrivee_.getSelectedIndex()) {
      g += "  titre \"Dur�es de parcours de l'�l�ment " + (String) this.listeElementDepart_.getSelectedItem()
          + " \"\n";
    } else {
      g += "  titre \"Dur�es de parcours de " + (String) this.listeElementDepart_.getSelectedItem() + " � "
          + (String) this.listeElementArrivee_.getSelectedItem() + " dans le sens "
          + (String) this.sens_.getSelectedItem() + " \"\n";
    }
    g += "  sous-titre \"Etude par cat�gorie de bateaux\"\n";
    g += "  animation non\n";
    g += "  legende " + "oui" + "\n";
    
    g += " marges\n {\n";
    g += " gauche 100\n"; g += " droite 100\n"; g += " haut 50\n"; g += " bas 30\n }\n";

    g += "  axe\n  {\n"; // abscisses
    g += "    titre \"Cat�gorie" + "\"\n";
    g += "    orientation " + "horizontal" + "\n";
    g += "    graduations non\n";
    g += "    minimum " + 0 + "\n";
    g += "    maximum " + (nbNavires + 2)// (this.donnees_.categoriesNavires_.listeNavires_.size()+3)
        + "\n";
    g += "  }\n";

    g += "  axe\n  {\n"; // Ordonn�es
    g += "    titre \"Dur�e" + "\"\n";
    g += "    unite \"";
    if (echelleHeures) g += "heures"; else g += "minutes";
    g += "\"\n";
    g += "    orientation " + "vertical" + "\n";
    g += "    graduations oui\n";
    g += "    minimum " + 0 + "\n";
    g += "    maximum ";
    if (echelleHeures) g+=(Sinavi3AlgorithmeDureesParcours.determineDureeMaxi(donnees_))/60;
    else g+= (Sinavi3AlgorithmeDureesParcours.determineDureeMaxi(donnees_));
    g += "\n";
    g += "  }\n";
    /*
     * for (int i= 0; i < this.donnees_.categoriesNavires_.listeNavires_.size(); i++) { //A RESERVER POUR GERER
     * PLUSIEURS COURBES
     */

    // ******************************debut histo max************************************************
      g += "  courbe\n  {\n";
      g += "    titre \"" + "dur�es maximales"
      /*
       * + "/" + choixString(choix, i, 1) + "/" + choixString(choix, i, 2)
       */
      + "\"\n";
      g += "    type ";
      if (histo) g += "histogramme"; else g += "courbe";
      g += "\n";
      g += "    aspect\n {\n";
      g += "contour.largeur 1 \n";
      g += "surface.couleur  	BB0000 \n";
      g += "texte.couleur 000000 \n";
      g += "contour.couleur ";
      if (histo) g += "000000"; else g += "BB0000";
      g += " \n";
      g += "    }\n";
      g += "    valeurs\n    {\n";

      indiceNavire = 0;
      for (int n = 0; n < this.donnees_.listeBateaux_.listeNavires_.size(); n++) {
        if (this.tableauChoixNavires_[n].isSelected()) {
          g += (indiceNavire + 1)// numero de la cat�gorie

              + " ";
          if(echelleHeures) g+= (this.donnees_.params_.ResultatsCompletsSimulation.DureeParcoursCategorie[n].dureeMaximumParcours)/60;
          else g+= this.donnees_.params_.ResultatsCompletsSimulation.DureeParcoursCategorie[n].dureeMaximumParcours;

              g+= "\n etiquette  \n \"" + this.donnees_.listeBateaux_.retournerNavire(n).nom + "\" \n"/* + */
              + "\n";
          indiceNavire++;
        }
      }// fin du pour remplissage des coordonn�es
      g += "    }\n";
      g += "  }\n";

    // //******************************fin histo max************************************************

    // ******************************debut histo moy************************************************
      g += "  courbe\n  {\n";
      g += "    titre \"" + "dur�es moyennes"
      + "\"\n";
      g += "    type ";
      if (histo) g += "histogramme"; else g += "courbe";
      g += "\n";
      g += "    aspect\n {\n";
      g += "contour.largeur 1 \n";
      g += "surface.couleur BB8800 \n";
      g += "texte.couleur 000000 \n";
      g += "contour.couleur ";
      if (histo) g += "000000"; else g += "BB8800";
      g += " \n";
      g += "    }\n";
      g += "    valeurs\n    {\n";

      indiceNavire = 0;
      for (int n = 0; n < this.donnees_.listeBateaux_.listeNavires_.size(); n++) {
        if (this.tableauChoixNavires_[n].isSelected()) {
          g += (indiceNavire + 1)// numero de la cat�gorie
              + " ";
          if (this.donnees_.params_.ResultatsCompletsSimulation.DureeParcoursCategorie[n].nombreNavires==0) g+=0;
          else {
        	  if (echelleHeures) g+= ((float) (this.donnees_.params_.ResultatsCompletsSimulation.DureeParcoursCategorie[n].dureeParcoursTotale / this.donnees_.params_.ResultatsCompletsSimulation.DureeParcoursCategorie[n].nombreNavires))/60;
        	  else g+= ((float) (this.donnees_.params_.ResultatsCompletsSimulation.DureeParcoursCategorie[n].dureeParcoursTotale / this.donnees_.params_.ResultatsCompletsSimulation.DureeParcoursCategorie[n].nombreNavires));
        	  }
          g += "\n";
          indiceNavire++;
        }

      }// fin du pour remplissage des coordonn�es
      g += "    }\n";
      g += "  }\n";

    // //******************************fin histo moy************************************************

    // ******************************debut histo min************************************************
      g += "  courbe\n  {\n";
      g += "    titre \"" + "dur�es minimales"
      + "\"\n";
      g += "    type ";
      if (histo) g += "histogramme"; else g += "courbe";
      g += "\n";
      g += "    aspect\n {\n";
      g += "contour.largeur 1 \n";
      g += "surface.couleur BBCC00 \n";
      g += "texte.couleur 000000 \n";
      g += "contour.couleur ";
      if (histo) g += "000000"; else g += "BBCC00";
      g += " \n";
      g += "    }\n";
      g += "    valeurs\n    {\n";

      indiceNavire = 0;

      for (int n = 0; n < this.donnees_.listeBateaux_.listeNavires_.size(); n++) {
        if (this.tableauChoixNavires_[n].isSelected()) {
          g += (indiceNavire + 1)// numero de la cat�gorie
              + " ";
          if (echelleHeures) g+=((float) this.donnees_.params_.ResultatsCompletsSimulation.DureeParcoursCategorie[n].dureeMinimumParcours)/60;
          else g+=((float) this.donnees_.params_.ResultatsCompletsSimulation.DureeParcoursCategorie[n].dureeMinimumParcours);
          g += "\n";
          indiceNavire++;
        }
      }// fin du pour remplissage des coordonn�es
      g += "    }\n";
      g += "  }\n";
    // //******************************fin histo min************************************************
    
    if (seuil_) {
        /**
         * declaration d'un seuil
         */
        g += " contrainte\n";
        g += "{\n";
        // a mettre le seuil
        g += "titre \"seuil \"\n";
        g += " type max\n";
        if (echelleHeures) g += " valeur " + (valeurSeuil_/60.) + CtuluLibString.LINE_SEP_SIMPLE;// /la valeur ordonn�e du seuil
        else g += " valeur " + valeurSeuil_ + CtuluLibString.LINE_SEP_SIMPLE;

        g += " \n }\n";
        // }//fin du for
      }

    return g;
  }

  /**
   * Listener principal des elements de la frame: tres important pour les composant du panel de choix des �l�ments.
   * 
   * @param _ev evenements qui apelle cette fonction
   */

  public void actionPerformed(final ActionEvent _ev) {
    final Object source = _ev.getSource();

    // action commune a tous les �v�nements: redimensionnement de la fenetre
    final Dimension actuelDim = this.getSize();
    final Point pos = this.getLocation();

    if (source == this.choixCumul_ || source == this.choixPercent_ || source == this.choixNombre_)
    {
    	rafraichissementTableauDistribution();
    	affichageGrapheDistrib(true);
    }
    
    if (source == this.lancerRecherche_) {
      
      // lancement des calculs pour les dur�es de parcours:
      Sinavi3AlgorithmeDureesParcours
          .calcul2(donnees_, this.listeTypesDepart_.getSelectedIndex(), this.listeElementDepart_.getSelectedIndex(),
              this.listeTypesArrivee_.getSelectedIndex(), this.listeElementArrivee_.getSelectedIndex(), this.sens_
                  .getSelectedIndex()/* ,Float.parseFloat(this.horaireDeb_.getText()),Float.parseFloat(this.horaireFin_.getText()) */);
      // mise a jour des affichages:
      affichageTableau(this.listeNavires_.getSelectedIndex() - 1);
      final String descriptionCourbes = this.affichageGraphe();
      this.graphe_.setFluxDonnees(new ByteArrayInputStream(descriptionCourbes.getBytes()));
      affichageTableauDistribution();
      affichageGrapheDistrib(true);

      // }
    } else if (source == this.valideSeuil_ || source == this.rafraichirSeuil_) {
    	this.rafraichirSeuil_.setEnabled(this.valideSeuil_.isSelected());
    	if (this.valideSeuil_.isSelected() && !this.valSeuilHeure_.getText().equals("") && !this.valSeuilMinute_.getText().equals("")) {
    		// booleen passe a true
    		this.seuil_ = true;
    		// on recupere al valeure du seuil choisie par l utilisateur
    		valeurSeuil_ = 60*((int) Float.parseFloat(this.valSeuilHeure_.getText())) + (int) Float.parseFloat(this.valSeuilMinute_.getText());
    		// on redesssinne l histogramme en tenant compte du seuil de l utilisateur
    		final String descriptionGraphe = this.affichageGraphe();
    		this.graphe_.setFluxDonnees(new ByteArrayInputStream(descriptionGraphe.getBytes()));
    	} else {
    		// booleen passe a false
    		this.seuil_ = false;
    		// on redesssinne l histogramme en tenant compte du seuil de l utilisateur
    		final String descriptionGraphe = this.affichageGraphe();
    		this.graphe_.setFluxDonnees(new ByteArrayInputStream(descriptionGraphe.getBytes()));
    	}
      
    } else if (source == this.valideSeuilDistrib_ || source == this.rafraichirSeuilDistrib_) {
    	this.rafraichirSeuilDistrib_.setEnabled(this.valideSeuilDistrib_.isSelected());
    	if (this.valideSeuilDistrib_.isSelected() && !this.valSeuilDistribHeure_.getText().equals("") && !this.valSeuilDistribMinute_.getText().equals("")) {
    		// booleen passe a true
    		this.seuilDistrib_ = true;
    		// on recupere la valeur du seuil choisie par l utilisateur
    		valeurSeuilDistrib_ = 60*((int) Float.parseFloat(this.valSeuilDistribHeure_.getText())) + (int) Float.parseFloat(this.valSeuilDistribMinute_.getText());
    		// on regenere le graphe
    		affichageGrapheDistrib(true);
    	} else {
    		// booleen passe a false
    		this.seuilDistrib_ = false;
    		// on regenere le graphe
    		affichageGrapheDistrib(true);
    	}
      
    } else if (source == this.rafraichirSeuilDistrib_ && !this.valSeuilDistribHeure_.getText().equals("") && !this.valSeuilDistribMinute_.getText().equals("")) {
    	valeurSeuilDistrib_ = 60*((int) Float.parseFloat(this.valSeuilDistribHeure_.getText())) + (int) Float.parseFloat(this.valSeuilDistribMinute_.getText());
    	affichageGrapheDistrib(true);
    
    } else if (source == this.choixTypeGraphe_) {
    	final String descriptionCourbes = this.affichageGraphe();
        this.graphe_.setFluxDonnees(new ByteArrayInputStream(descriptionCourbes.getBytes()));
    }

    boolean trouve = false;
    for (int k = 0; k < this.tableauChoixNavires_.length && !trouve; k++) {
      if (source == this.tableauChoixNavires_[k]) {
        trouve = true;
        affichageTableau(-1);
        // mise a jour des courbes
        final String descriptionCourbes = this.affichageGraphe();
        this.graphe_.setFluxDonnees(new ByteArrayInputStream(descriptionCourbes.getBytes()));

      }
    }
    
    trouve = false;
    for (int k = 0; k < this.tableauChoixNavires2_.length && !trouve; k++) {
      if (source == this.tableauChoixNavires2_[k]) {
        trouve = true;
        affichageTableauDistribution();
        affichageGrapheDistrib(true);
      }
    }

    // on redimensionne la fenetre comme elle etais avant manipulation des elements graphique
    this.setSize(actuelDim);
    this.setLocation(pos);
  }

  /**
   * Methode qui s active lorsque l'on quitte l'application.
   */
  protected void windowClosed() {
    System.out.print("Fermeture de la fenetre des durees de parcours");
    dispose();
  }
  
  
  /**
   * M�thode permettant d'afficher un graphe de distribution de dur�es de parcours.
   * @param _refresh Param�tre permettant de choisir un rafraichissement du graphe existant plut�t que la construction d'un graphe nouveau
   */
  public void affichageGrapheDistrib(boolean _refresh){
	
	  boolean percent = false;
	  if (choixPercent_.isSelected()) percent=true;
	  
	  final int valeurIntervalle = this.donnees_.params_.ResultatsCompletsSimulation.DureeParcoursCategorie[0].valeurIntervalleDistribution; // [0] car intervalle identique pour toutes cat�gories ; attention : pourrait ne pas �tre le cas lors de prochains dev
	  
	  boolean echelleHeures=false;
	  if (Sinavi3AlgorithmeDureesParcours.determineDureeMaxi(donnees_) >= 480) echelleHeures=true;
	  
	  int nbCategoriesAffichees=0;
	  for(int i=0;i<donnees_.listeBateaux_.listeNavires_.size();i++)
	  {
		  if (this.tableauChoixNavires2_[i].isSelected()) nbCategoriesAffichees++;
	  }
	
	  String[] noms=new String[nbCategoriesAffichees];
	  short[][] valeurs = new short[nbCategoriesAffichees][];
	  int[] nombreBateaux = new int[nbCategoriesAffichees];
	  int cpt=0;
	  for(int i=0;i<donnees_.listeBateaux_.listeNavires_.size();i++)
	  {
		  if (this.tableauChoixNavires2_[i].isSelected())
		  {
			  noms[cpt] = donnees_.listeBateaux_.retournerNavire(i).nom;
			  valeurs[cpt] = this.donnees_.params_.ResultatsCompletsSimulation.DureeParcoursCategorie[i].distribution;
			  nombreBateaux[cpt] = this.donnees_.params_.ResultatsCompletsSimulation.DureeParcoursCategorie[i].nombreNavires;
			  cpt++;
		  }
	  }
	  
	  JFreeChart chart;
	  if (this.choixCumul_.isSelected()) chart = Sinavi3JFreeChartGraphe.genererGrapheDistributionCumuleeDurees("Distribution cumul�e des dur�es de parcours", valeurIntervalle, valeurs, noms, nombreBateaux, echelleHeures, percent);
	  else chart = Sinavi3JFreeChartGraphe.genererHistoDistributionDurees("Distribution des dur�es de parcours", valeurIntervalle, valeurs, noms, nombreBateaux, echelleHeures, percent);
	  
	  if (!_refresh) this.grapheDistrib_ = new ChartPanel(chart);
	  else this.grapheDistrib_.setChart(chart);
	  
	  if (seuilDistrib_) Sinavi3JFreeChartGraphe.afficherSeuil(grapheDistrib_, valeurSeuilDistrib_, echelleHeures);
	  
  }

  
}
