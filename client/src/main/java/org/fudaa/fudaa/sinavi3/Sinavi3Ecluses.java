package org.fudaa.fudaa.sinavi3;

import java.util.ArrayList;

/**
 * Classe de gestion des cheneaux permet de stocker les donn�es relatives au bassin
 * 
 * @author Adrien Hadoux
 */

public class Sinavi3Ecluses {

  /**
   * Liste de string contenant les noms des bassins
   */
  ArrayList<Sinavi3Ecluse> listeEcluses_ = new ArrayList<Sinavi3Ecluse>();

  /**
   * Methode d'ajout d'une ecluse:
   * 
   * @param c ecluse � ajouter
   */

  public void ajout(final Sinavi3Ecluse c) {

    listeEcluses_.add(c);

  }

  /**
   * Methode qui retourne la i eme ecluse
   * 
   * @param i indice de l'ecluse du tableau d'�cluses a retourner
   * @return un objet de type ecluse qui pourra etre modifi et renvoy
   */

  public Sinavi3Ecluse retournerEcluse(final int i) {
    if (i < this.listeEcluses_.size()) {
      return (Sinavi3Ecluse) this.listeEcluses_.get(i);
    } else {
      return null;
    }
  }

  /**
   * Methode de suppression du n ieme element de la liste de ecluses
   * 
   * @param n
   */
  public void suppression(final int n) {

    this.listeEcluses_.remove(n);

  }

  /**
   * Methode de modification de la n ieme ecluse par l'�cluse entr�e en parametre d entr�e
   * 
   * @param n indice de l'�cluse a modifier
   * @param c ecluse a remplacer
   */
  public void modification(final int n, final Sinavi3Ecluse c) {
    this.listeEcluses_.set(n, c);

  }

  public boolean existeDoublon(final String _nomComposant, final int k) {
    for (int i = 0; i < this.listeEcluses_.size(); i++) {
      if (i != k) {
        if (this.retournerEcluse(i).nom_.equals(_nomComposant)) {
          return true;
        }
      }

    }

    // arriv� a ce stade; on a pas touv� de doublons, le nom n'existe donc pas!
    return false;

  }

  public int retourneIndice(final String nomEcluse) {
    for (int i = 0; i < this.listeEcluses_.size(); i++) {
      if (this.retournerEcluse(i).nom_.equals(nomEcluse)) {
        return i;
      }
    }

    // arriv� a ce stade on a rien retrouv�
    return -1;
  }

public ArrayList<Sinavi3Ecluse> getListeEcluses() {
	return listeEcluses_;
}

public void setListeEcluses_(ArrayList listeEcluses_) {
	this.listeEcluses_ = listeEcluses_;
}

}
