package org.fudaa.fudaa.sinavi3;

import org.fudaa.dodico.corba.sinavi3.SParametresResultatsAttente;
import org.fudaa.dodico.corba.sinavi3.SParametresResultatsAttenteCategorie;

/**
 * classe permettant de calculer l'ensemble des attentes trajet afin de s'affranchir du fichier historique.
 * 
 * @version $Version$
 * @author hadoux
 */
public class Sinavi3AlgorithmeTOUTESAttentesTrajet {

  public static void calcul(final Sinavi3DataSimulation _d, Sinavi3ProgressFrame _fenetreProgression) {

	  if(_fenetreProgression != null)    
              _fenetreProgression.miseAjourBarreProgression(85, "Calcul des attentes par trajet");
	  
    // 0: determiner le place totale a allouer:
    _d.params_.ResultatsCompletsSimulation.TOUTESAttenteTrajet = new SParametresResultatsAttente[
        ( _d.listeBief_.listeBiefs_.size() + _d.listeEcluse_.listeEcluses_.size() ) * ( _d.listeBief_.listeBiefs_.size() +  _d.listeEcluse_.listeEcluses_.size() + 1 ) ];
    int compteur = 0;

    // 2: chenaux et ecluses dans les 3 sens
    for (int i = 0; i < _d.listeBief_.listeBiefs_.size(); i++) {
      for (int j = 0; j < _d.listeEcluse_.listeEcluses_.size(); j++) {
        /* *************** SENS Avalant ************************* */
        // allocation memoire
        _d.params_.ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur] = new SParametresResultatsAttente();

        _d.params_.ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].typeElement = 0;
        _d.params_.ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].indiceElement = i;

        _d.params_.ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].typeElement2 = 1;
        _d.params_.ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].indiceElement2 = j;

        _d.params_.ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].sens = 0;
        // lancement des calculs du trajet correspondant:
        Sinavi3AlgorithmeAttentesGenerales.calculTrajet(_d, 0, i, 1, j, 0);

        // allocation memoire du tableau des attentes par trajet
        _d.params_.ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories = new SParametresResultatsAttenteCategorie[_d.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories.length];
        // recopiage des donn�es dans la structure
        attributionValeursAttentes(_d.params_.ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur], _d);
        compteur++;
        /* *************** SENS Montant ************************* */
        // allocation memoire
        _d.params_.ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur] = new SParametresResultatsAttente();

        _d.params_.ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].typeElement = 0;
        _d.params_.ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].indiceElement = i;

        _d.params_.ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].typeElement2 = 1;
        _d.params_.ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].indiceElement2 = j;

        _d.params_.ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].sens = 1;
        // lancement des calculs du trajet correspondant:
        Sinavi3AlgorithmeAttentesGenerales.calculTrajet(_d, 0, i, 1, j, 1);

        // allocation memoire du tableau des attentes par trajet
        _d.params_.ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories = new SParametresResultatsAttenteCategorie[_d.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories.length];
        // recopiage des donn�es dans la structure
        attributionValeursAttentes(_d.params_.ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur], _d);
        compteur++;
       
      }
    }
    
    if(_fenetreProgression != null)   
        _fenetreProgression.miseAjourBarreProgression(88, "Calcul des attentes par trajet");

    // 7: chenaux et chenaux dasn les 3 sens
    for (int i = 0; i < _d.listeBief_.listeBiefs_.size(); i++) {
      for (int j = i; j < _d.listeBief_.listeBiefs_.size(); j++) {
        /* *************** SENS Avalant ************************* */
        // allocation memoire
        _d.params_.ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur] = new SParametresResultatsAttente();

        _d.params_.ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].typeElement = 0;
        _d.params_.ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].indiceElement = i;

        _d.params_.ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].typeElement2 = 0;
        _d.params_.ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].indiceElement2 = j;

        _d.params_.ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].sens = 0;
        // lancement des calculs du trajet correspondant:
        Sinavi3AlgorithmeAttentesGenerales.calculTrajet(_d, 0, i, 0, j, 0);

        // allocation memoire du tableau des attentes par trajet
        _d.params_.ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories = new SParametresResultatsAttenteCategorie[_d.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories.length];
        // recopiage des donn�es dans la structure
        attributionValeursAttentes(_d.params_.ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur], _d);
        compteur++;
        
        /* *************** SENS Montant ************************* */
        // allocation memoire
        _d.params_.ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur] = new SParametresResultatsAttente();

        _d.params_.ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].typeElement = 0;
        _d.params_.ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].indiceElement = i;

        _d.params_.ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].typeElement2 = 0;
        _d.params_.ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].indiceElement2 = j;

        _d.params_.ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].sens = 1;
        // lancement des calculs du trajet correspondant:
        Sinavi3AlgorithmeAttentesGenerales.calculTrajet(_d, 0, i, 0, j, 1);

        // allocation memoire du tableau des attentes par trajet
        _d.params_.ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories = new SParametresResultatsAttenteCategorie[_d.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories.length];
        // recopiage des donn�es dans la structure
        attributionValeursAttentes(_d.params_.ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur], _d);
        compteur++;

      }
    }

    if(_fenetreProgression != null) 
        _fenetreProgression.miseAjourBarreProgression(92, "Calcul des attentes par trajet");
   
    // 9: ecluses et ecluses dasn les 3 sens
    for (int i = 0; i < _d.listeEcluse_.listeEcluses_.size(); i++) {
      for (int j = i; j < _d.listeEcluse_.listeEcluses_.size(); j++) {
        /* *************** SENS Avalant ************************* */
        // allocation memoire
        _d.params_.ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur] = new SParametresResultatsAttente();

        _d.params_.ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].typeElement = 1;
        _d.params_.ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].indiceElement = i;

        _d.params_.ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].typeElement2 = 1;
        _d.params_.ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].indiceElement2 = j;

        _d.params_.ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].sens = 0;
        // lancement des calculs du trajet correspondant:
        Sinavi3AlgorithmeAttentesGenerales.calculTrajet(_d, 1, i, 1, j, 0);

        // allocation memoire du tableau des attentes par trajet
        _d.params_.ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories = new SParametresResultatsAttenteCategorie[_d.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories.length];
        // recopiage des donn�es dans la structure
        attributionValeursAttentes(_d.params_.ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur], _d);
        compteur++;
        /* *************** SENS Montant ************************* */
        // allocation memoire
        _d.params_.ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur] = new SParametresResultatsAttente();

        _d.params_.ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].typeElement = 1;
        _d.params_.ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].indiceElement = i;

        _d.params_.ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].typeElement2 = 1;
        _d.params_.ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].indiceElement2 = j;

        _d.params_.ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].sens = 1;
        // lancement des calculs du trajet correspondant:
        Sinavi3AlgorithmeAttentesGenerales.calculTrajet(_d, 1, i, 1, j, 1);

        // allocation memoire du tableau des attentes par trajet
        _d.params_.ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur].tableauAttenteCategories = new SParametresResultatsAttenteCategorie[_d.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories.length];
        // recopiage des donn�es dans la structure
        attributionValeursAttentes(_d.params_.ResultatsCompletsSimulation.TOUTESAttenteTrajet[compteur], _d);
        compteur++;
        
      }
    }

  }// fin void calcul
  
  public static void attributionValeursAttentes(SParametresResultatsAttente _attentesTrajets, final Sinavi3DataSimulation _data) {
	  _attentesTrajets.valeurIntervalleDistribution = _data.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.valeurIntervalleDistribution;
	  for (int k = 0; k < _data.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories.length; k++) {
		  _attentesTrajets.tableauAttenteCategories[k] = new SParametresResultatsAttenteCategorie();
		  _attentesTrajets.tableauAttenteCategories[k].attenteAccesMaxi = _data.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteAccesMaxi;
		  _attentesTrajets.tableauAttenteCategories[k].attenteAccesMini = _data.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteAccesMini;
		  _attentesTrajets.tableauAttenteCategories[k].attenteAccesTotale = _data.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteAccesTotale;
		  _attentesTrajets.tableauAttenteCategories[k].attenteAccesDistrib = _data.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteAccesDistrib;
		  _attentesTrajets.tableauAttenteCategories[k].attenteMegaTotale = _data.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteMegaTotale;
		  _attentesTrajets.tableauAttenteCategories[k].attenteOccupMaxi = _data.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteOccupMaxi;
		  _attentesTrajets.tableauAttenteCategories[k].attenteOccupMini = _data.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteOccupMini;
		  _attentesTrajets.tableauAttenteCategories[k].attenteOccupTotale = _data.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteOccupTotale;
		  _attentesTrajets.tableauAttenteCategories[k].attenteOccupDistrib = _data.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteOccupDistrib;
		  _attentesTrajets.tableauAttenteCategories[k].attentePanneMaxi = _data.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attentePanneMaxi;
		  _attentesTrajets.tableauAttenteCategories[k].attentePanneMini = _data.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attentePanneMini;
		  _attentesTrajets.tableauAttenteCategories[k].attentePanneTotale = _data.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attentePanneTotale;
		  _attentesTrajets.tableauAttenteCategories[k].attentePanneDistrib = _data.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attentePanneDistrib;
		  _attentesTrajets.tableauAttenteCategories[k].attenteSecuMaxi = _data.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteSecuMaxi;
		  _attentesTrajets.tableauAttenteCategories[k].attenteSecuMini = _data.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteSecuMini;
		  _attentesTrajets.tableauAttenteCategories[k].attenteSecuTotale = _data.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteSecuTotale;
		  _attentesTrajets.tableauAttenteCategories[k].attenteSecuDistrib = _data.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteSecuDistrib;
		  _attentesTrajets.tableauAttenteCategories[k].attenteTotaleMaxi = _data.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteTotaleMaxi;
		  _attentesTrajets.tableauAttenteCategories[k].attenteTotaleMini = _data.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteTotaleMini;
		  _attentesTrajets.tableauAttenteCategories[k].attenteTotaleDistrib = _data.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].attenteTotaleDistrib;
		  _attentesTrajets.tableauAttenteCategories[k].nbNaviresAtenteOccup = _data.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAtenteOccup;
		  _attentesTrajets.tableauAttenteCategories[k].nbNaviresAttenteAcces = _data.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAttenteAcces;
		  _attentesTrajets.tableauAttenteCategories[k].nbNaviresAttentePanne = _data.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAttentePanne;
		  _attentesTrajets.tableauAttenteCategories[k].nbNaviresAttenteSecu = _data.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAttenteSecu;
		  _attentesTrajets.tableauAttenteCategories[k].nbNaviresAttenteTotale = _data.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nbNaviresAttenteTotale;
		  _attentesTrajets.tableauAttenteCategories[k].nombreNaviresTotal = _data.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[k].nombreNaviresTotal;
	  }
  }

}
