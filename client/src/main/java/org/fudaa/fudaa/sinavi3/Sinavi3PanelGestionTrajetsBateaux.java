package org.fudaa.fudaa.sinavi3;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JApplet;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;

import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.table.CtuluTableExcelWriter;
import org.fudaa.fudaa.ressource.FudaaResource;
import org.fudaa.fudaa.sinavi.factory.ColumnAutoSizer;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuDialogConfirmation;
import com.memoire.bu.BuDialogError;
import com.memoire.bu.BuDialogMessage;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuTable;
import com.memoire.fu.FuLog;

/**
 * 
 * Interface de saisie des trajets. Cette interface affiche les trajets par bateaux et l'interface de saisie, modification des trajets.
 *@version $Version$
 * @author hadoux
 *
 */


public class Sinavi3PanelGestionTrajetsBateaux extends Sinavi3InternalFrame{


	//-- donnees de la simulation --//
	Sinavi3DataSimulation donnees_;

	//-- Mode modification --//
	boolean MODIF_ON=false;
	int indiceTrajetAmodifier=0;
	
	
	//-- tablea ud'affichage des trajets --//
	BuTable tableau_;

	//-- modele d'affichage des données --//
	Sinavi3Modeletrajets modeleTrajet;

	//-- liste de choix des bateaux: quand on clique dessus, cela affiche les trajets de cette catégorie --//
	JComboBox listeBateaux=new JComboBox();


	//-- Panel du formulaire --//
	Box panelFormulaire=Box.createVerticalBox();

	//--panel d'affichage --//
	BuPanel panelAffichage=new BuPanel(new BorderLayout());

	//--SplitPane qui contient les 2 panels formulaires et affichage --//
	BuPanel conteneur=new BuPanel(new GridLayout(2,1));

	private final BuButton impression_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_generer"), "Exporter");
	private final BuButton modification_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_maj"), "Modifier");
	private final BuButton quitter_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_oui"), "Quitter");
	private final BuButton suppression_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_detruire"), "Supprimer");
	private final BuButton boutonSaisie_ = new BuButton(FudaaResource.FUDAA.getIcon("ajouter"), "Ajouter");

	/**  Composants du formulaire**/
	/**Trajet du bateau**/


	Sinavi3FrameSaisieLoiJournaliere fenetreLoiJournaliere_ = null;
	Sinavi3FrameSaisieLoiDeterministe fenetreLoideter_ = null;
	JComboBox dgGareDepart = new JComboBox();
	JComboBox dgGareArrivee = new JComboBox();
	JComboBox dgSens = new JComboBox(new String[]{"avalant","montant" });

	String[] choixLoi_ = { "Erlang", "Deterministe", "Journaliere" };
	JComboBox choixLoiGenerationNav_ = new JComboBox(choixLoi_);
	JLabel labelChoixLoi=new JLabel("<html><body>format de saisie: <bold>HH:mm</bold> <br /> " +
			"ex : une durée d’ <bold>'une journée et 2 heures 58 minutes' s’écrira '26:58'</bold> </body></html> ");
	
	// loi erlang
	final BuButton configurationLoi_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_maj"), "Configurer");
	Sinavi3TextFieldInteger nbBateauxAttendus_ = new Sinavi3TextFieldInteger(3);
	Sinavi3TextFieldFloat loiErlangEcartType_ = new Sinavi3TextFieldFloat(3);
	Sinavi3TextFieldDuree erlangdebut=new Sinavi3TextFieldDuree(3);
	Sinavi3TextFieldDuree erlangfin=new Sinavi3TextFieldDuree(3);
	String[] tabloi_ = { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10" };
	JComboBox loiGenerationNavErlang_ = new JComboBox(tabloi_);
	ArrayList loiDeterministe_ = new ArrayList();
	JLabel messNbaviresAttendus = new JLabel("Nombre de bateaux attendus sur la durée de la simulation:");
	final BuButton validerNavire_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_oui"), "Ajouter");
	final JRadioButton radioErlangLoi = new JRadioButton("Ordre loi d'Erlang:");
	final JRadioButton radioErlangEcart = new JRadioButton("Rapport σ/m (écart type/moyenne:");
	final JPanel panelErlang =new JPanel(new BorderLayout());
	final JPanel panelNbNaviresAttendus = new JPanel();
	final BuPanel creneauxErlang=new BuPanel();
	
	
	public void setErlangLoi(boolean isErlangLoi){
		if(isErlangLoi){
			radioErlangEcart.setSelected(false);
			radioErlangLoi.setSelected(true);
			loiGenerationNavErlang_.setEnabled(true);
			loiErlangEcartType_.setEnabled(false);
		}else {
			radioErlangEcart.setSelected(true);
			radioErlangLoi.setSelected(false);
			loiGenerationNavErlang_.setEnabled(false);
			loiErlangEcartType_.setEnabled(true);
		}
	}
	
	public Sinavi3PanelGestionTrajetsBateaux(Sinavi3DataSimulation _d){
		super("", true, true, true, true);
		
		setSize(700,630);
		donnees_=_d;	
		modeleTrajet=new Sinavi3Modeletrajets(donnees_);

		//-- infos de l'interface--//
		setTitle("Définition des trajets");
		this.dgGareDepart.setToolTipText("Gare de départ des bateaux");
		erlangdebut.setToolTipText("Heure de début de la génération d'Erlang");
		erlangfin.setToolTipText("Heure de fin de la génération d'Erlang");
		this.dgGareArrivee.setToolTipText("Gare d'arrivée des bateaux");
		this.dgSens.setToolTipText("Sens du trajet du bateau");
		choixLoiGenerationNav_.setToolTipText("Choix du type de loi de génération de bateaux");
	    this.modification_
	         .setToolTipText("Modifie le trajet sélectionné dans le tableau ci-dessus");
	     this.quitter_.setToolTipText("Ferme la sous-fenêtre");
	     this.suppression_
	         .setToolTipText("Supprime le trajet sélectionné dans le tableau ci-dessus");
	      this.impression_
	         .setToolTipText("Exporte le contenu du tableau au format xls");
	      boutonSaisie_.setToolTipText("Ajoute un nouveau trajet");



		//-- Architecture de l'interface --//
		conteneur.add(panelAffichage);
		conteneur.add(panelFormulaire);

		//conteneur.setDividerLocation(100);
		this.setLayout(new GridLayout(1,1));
		this.add(conteneur);


		//-- Creation du panel Formulaire
		majSaisie();
		initialiser();
		listeBateaux.setSelectedIndex(0);

		Box boxLargeurs2=Box.createVerticalBox();
		boxLargeurs2.setBorder(BorderFactory.createTitledBorder(Sinavi3Bordures.compound_, "Trajet des bateaux"));
		panelFormulaire.add(boxLargeurs2);
		final JPanel pdg31a = new JPanel();
		pdg31a.add(new JLabel("Gare de départ:"));
		pdg31a.add(this.dgGareDepart);
		pdg31a.add(new JLabel(" Gare d'arrivée:"));
		pdg31a.add(this.dgGareArrivee);
		pdg31a.add(new JLabel(" Sens de parcours:"));
		pdg31a.add(this.dgSens);
		boxLargeurs2.add(pdg31a);
		//	Loi generation de navires
		JPanel panelLoigeneration=new JPanel(new BorderLayout());
		panelLoigeneration.setBorder(BorderFactory.createTitledBorder(Sinavi3Bordures.compound_, "Loi de génération des bateaux"));
		panelFormulaire.add(panelLoigeneration);
		final JPanel mscdtype3 = new JPanel();
		mscdtype3.add(new JLabel("Type de loi:"));
		mscdtype3.add(this.choixLoiGenerationNav_);
		mscdtype3.add(this.configurationLoi_);
		mscdtype3.add(this.labelChoixLoi);
		labelChoixLoi.setVisible(false);
		
		JPanel panelErlang1 =new JPanel(new FlowLayout(FlowLayout.LEFT));
		JPanel panelErlang2 =new JPanel(new FlowLayout(FlowLayout.LEFT));
		panelErlang.setSize(200, 100);
		mscdtype3.add(panelErlang);
		
		
		 ActionListener radioErlangListener = new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				setErlangLoi(e.getSource()== radioErlangLoi);
				
			}
		};
		radioErlangLoi.addActionListener(radioErlangListener) ;
		radioErlangEcart.addActionListener(radioErlangListener) ;
		 
		panelErlang1.add(radioErlangLoi);
		//panelErlang1.add(new JLabel("Ordre loi d'Erlang:"));
		panelErlang1.add(this.loiGenerationNavErlang_);
		panelErlang.add(panelErlang1,BorderLayout.CENTER);
		
		panelErlang2.add(radioErlangEcart);		
		//panelErlang2.add(new JLabel("Rapport σ/m (écart type/moyenne:"));
		panelErlang2.add(this.loiErlangEcartType_);
		panelErlang.add(panelErlang2,BorderLayout.SOUTH);
		
		panelLoigeneration.add(mscdtype3, BorderLayout.NORTH);
		
		messNbaviresAttendus = new JLabel("Nombre de bateaux attendus sur la durée de la simulation:");
		messNbaviresAttendus.setForeground(Color.RED);
		panelNbNaviresAttendus.add(messNbaviresAttendus);
		panelNbNaviresAttendus.add(this.nbBateauxAttendus_);
		panelLoigeneration.add(panelNbNaviresAttendus, BorderLayout.CENTER);
		configurationLoi_.setEnabled(false);
		//-- ajout du bouton de validation --//
		
		
		creneauxErlang.add(new JLabel("Créneau de génération:"));
		creneauxErlang.add(erlangdebut);
		creneauxErlang.add(new JLabel("-"));
		creneauxErlang.add(erlangfin);
		creneauxErlang.add(new JLabel("hrs.min"));
		//creneauxErlang.add(validerNavire_);
		panelLoigeneration.add(creneauxErlang, BorderLayout.SOUTH);

		panelFormulaire.add(validerNavire_);
		
		BuPanel actionPanel=new BuPanel();
		actionPanel.setBorder(Sinavi3Bordures.compound_);
		actionPanel.add(quitter_);
		actionPanel.add(boutonSaisie_);
		actionPanel.add(modification_);
		actionPanel.add(suppression_);
		actionPanel.add(impression_);
		panelFormulaire.add(actionPanel);
		

		//--panel affichage --//
		BuPanel selectionNavire=new BuPanel(new FlowLayout());
		tableau_=new BuTable(modeleTrajet);
		ColumnAutoSizer.sizeColumnsToFit(tableau_);
		selectionNavire.add(new JLabel("Catégorie de bateaux:"));
		selectionNavire.add(listeBateaux);
		selectionNavire.setBorder(BorderFactory.createTitledBorder("Choix de la catégorie de bateaux"));
		panelAffichage.add(selectionNavire,BorderLayout.NORTH);
		panelAffichage.add(new JScrollPane(tableau_),BorderLayout.CENTER);
		
		

		//-- listeners --//
		validerNavire_.addActionListener(new ActionListener() {

			public void actionPerformed(final ActionEvent e) {
				ajouterTrajet();
			}});
		
		//début ajout par fargeix
		
		configurationLoi_.addActionListener(new ActionListener() {
			public void actionPerformed(final ActionEvent e) {
				final int choixLoi = choixLoiGenerationNav_.getSelectedIndex();
				if (choixLoi == 1) {
					labelChoixLoi.setVisible(true);
					if (fenetreLoideter_ == null) {
						FuLog.debug("interface nulle");
						
						fenetreLoideter_ = new Sinavi3FrameSaisieLoiDeterministe(donnees_, loiDeterministe_, null, 
								"<html><body>format de saisie: <bold>HH:mm</bold> <br /> ex : une durée d’ <bold>'une journée et 2 heures 58 minutes' s’écrira '26:58'</bold> </body></html> ", new Dimension(500, 300));
						fenetreLoideter_.setVisible(true);
						donnees_.application_.addInternalFrame(fenetreLoideter_);
					} else {
						FuLog.debug("interface fermée");
						if (fenetreLoideter_.isClosed()) {
							donnees_.application_.addInternalFrame(fenetreLoideter_);
						} else {
							FuLog.debug("interface cas de figur restant autre que null et fermeture");
							donnees_.application_.activateInternalFrame(fenetreLoideter_);
							donnees_.application_.addInternalFrame(fenetreLoideter_);
						}
					}
				}
				else if (choixLoi == 2) {
					labelChoixLoi.setVisible(true);
					if (fenetreLoiJournaliere_ == null) {
						FuLog.debug("interface nulle");
						fenetreLoiJournaliere_ = new Sinavi3FrameSaisieLoiJournaliere(donnees_, loiDeterministe_, null,
								"<html><body>format de saisie: <bold>HH:mm</bold> <br /> " +
										"ex : une durée d’ <bold>'une journée et 2 heures 58 minutes' s’écrira '26:58'</bold> </body></html> ",new Dimension(500, 300));
						fenetreLoiJournaliere_.setVisible(true);
						donnees_.application_.addInternalFrame(fenetreLoiJournaliere_);
					} else {
						FuLog.debug("interface fermée");
						if (fenetreLoiJournaliere_.isClosed()) {
							donnees_.application_.addInternalFrame(fenetreLoiJournaliere_);
						} else {
							FuLog.debug("interface cas de figur restant autre que null et fermeture");
							donnees_.application_.activateInternalFrame(fenetreLoiJournaliere_);
							donnees_.application_.addInternalFrame(fenetreLoiJournaliere_);
						}
					}
				}else {
					labelChoixLoi.setVisible(false);
				}
			}
		});
		
		//fin ajout par fargeix

		this.choixLoiGenerationNav_.addActionListener(new ActionListener() {

			public void actionPerformed(final ActionEvent e) {

				final int choixLoi = choixLoiGenerationNav_.getSelectedIndex();

				if (choixLoi == 0) {
					labelChoixLoi.setVisible(false);
					// Cas 0: loi d erlang
					messNbaviresAttendus.setForeground(Color.RED);
					nbBateauxAttendus_.setEnabled(true);
					erlangdebut.setEnabled(true);
					erlangfin.setEnabled(true);
					// ecartMoyenEntre2Arrivees_.setEnabled(true);
					setErlangLoi(true);
					panelErlang.setVisible(true);
					creneauxErlang.setVisible(true);
					panelNbNaviresAttendus.setVisible(true);
					configurationLoi_.setEnabled(false);

				} else if (choixLoi == 1) {
					labelChoixLoi.setVisible(true);
					// cas 1: loi deterministe
					messNbaviresAttendus.setForeground(Color.BLACK);

					nbBateauxAttendus_.setEnabled(false);
					erlangdebut.setEnabled(false);
					erlangfin.setEnabled(false);
					
					loiErlangEcartType_.setEnabled(false);
					panelErlang.setVisible(false);
					panelNbNaviresAttendus.setVisible(false);
					creneauxErlang.setVisible(false);
					configurationLoi_.setEnabled(true);

					if (fenetreLoideter_ == null) {
						FuLog.debug("interface nulle");
						fenetreLoideter_ = new Sinavi3FrameSaisieLoiDeterministe(donnees_, loiDeterministe_, null,
								"<html><body>format de saisie: <bold>HH:mm</bold> <br /> " +
								"ex : une durée d’ <bold>'une journée et 2 heures 58 minutes' s’écrira '26:58'</bold> </body></html> ",new Dimension(500, 300));

						// System.out.println("55555");
						fenetreLoideter_.setVisible(true);
						donnees_.application_.addInternalFrame(fenetreLoideter_);
					} else {
						FuLog.debug("interface fermée");
						if (fenetreLoideter_.isClosed()) {
							donnees_.application_.addInternalFrame(fenetreLoideter_);

						} else {
							FuLog.debug("interface cas de figur restant autre que null et fermeture");
							donnees_.application_.activateInternalFrame(fenetreLoideter_);
							donnees_.application_.addInternalFrame(fenetreLoideter_);

						}
					}

				} else if (choixLoi == 2) {
					labelChoixLoi.setVisible(true);
					// cas 2: loi journaliere
					messNbaviresAttendus.setForeground(Color.BLACK);
					nbBateauxAttendus_.setEnabled(false);
					erlangdebut.setEnabled(false);
					erlangfin.setEnabled(false);
					
					panelErlang.setVisible(false);
					panelNbNaviresAttendus.setVisible(false);
					loiErlangEcartType_.setEnabled(false);
					creneauxErlang.setVisible(false);
					configurationLoi_.setEnabled(true);

					if (fenetreLoiJournaliere_ == null) {
						FuLog.debug("interface nulle");
						fenetreLoiJournaliere_ = new Sinavi3FrameSaisieLoiJournaliere(donnees_, loiDeterministe_, null,
								"<html><body>format de saisie: <bold>HH:mm</bold> <br /> " +
								"ex : une durée d’ <bold>'une journée et 2 heures 58 minutes' s’écrira '26:58'</bold> </body></html> ",new Dimension(500, 300));

						// System.out.println("55555");
						fenetreLoiJournaliere_.setVisible(true);
						donnees_.application_.addInternalFrame(fenetreLoiJournaliere_);
					} else {
						FuLog.debug("interface fermée");
						if (fenetreLoiJournaliere_.isClosed()) {
							donnees_.application_.addInternalFrame(fenetreLoiJournaliere_);

						} else {
							FuLog.debug("interface cas de figur restant autre que null et fermeture");
							donnees_.application_.activateInternalFrame(fenetreLoiJournaliere_);
							donnees_.application_.addInternalFrame(fenetreLoiJournaliere_);

						}
					}

				}

			}

		});

		//-- lorsque lo'n change de bateau, cela modifie automatiquement l'affichage des trajets --//
		listeBateaux.addActionListener(new ActionListener() {

			public void actionPerformed(final ActionEvent e) {

				int bateau=listeBateaux.getSelectedIndex();
				
				//-- mise a jour dans le modele du tableau du bateau dont le trajet interesse --//
				modeleTrajet.setBateauVisualise(bateau);
				//-- mise a jour du modele du tableau et donc de son contenu--//
				modeleTrajet.fireTableStructureChanged();		
				
				
					
					
					
					
					
				
				//-- reinitialisation du formulaire --//
				initialiser();
				
			}

		});
		
		
		quitter_.addActionListener(new ActionListener() {

			public void actionPerformed(final ActionEvent e) {

				dispose();

	}});
		
		boutonSaisie_.addActionListener(new ActionListener() {

			public void actionPerformed(final ActionEvent e) {

				initialiser();

	}});
		
		impression_.addActionListener(new ActionListener() {
		      public void actionPerformed(final ActionEvent e) {
		          tableau_.editCellAt(0, 0);
		          // generation sous forme d'un fichier excel:
		          File fichier;
		          // definition d un file chooser
		          final JFileChooser fc = new JFileChooser();
		          final int returnVal = fc.showOpenDialog(Sinavi3PanelGestionTrajetsBateaux.this);

		          if (returnVal == JFileChooser.APPROVE_OPTION) {
		            fichier = fc.getSelectedFile();
		            final File f = CtuluLibFile.appendExtensionIfNeeded(fichier, "xls");

		            // on récupere l abstrct model du tableau contenant les données

		            /**
		             * creation d un abstract model implémentant l'interface CtuluTableModelInterface
		             */
		            final Sinavi3ModeleExcel modele;
		            
		          	  modele= modeleTrajet;
		           
		            
		            /**
		             * on essaie d 'ecrire en format excel
		             */
		            final CtuluTableExcelWriter ecrivain = new CtuluTableExcelWriter(modele, f);

		            try {
		              ecrivain.write(null);
		              new BuDialogMessage(donnees_.application_.getApp(), donnees_.application_.isSinavi_, 
		              		"Fichier Excel généré avec succès.").activate();

		            } catch (final RowsExceededException _err) {
		              FuLog.error(_err);
		            } catch (final WriteException _err) {
		              FuLog.error(_err);
		            } catch (final IOException _err) {
		              FuLog.error(_err);
		            }

		          }// fin du if si le composant est bon
		        }

		      });


		suppression_.addActionListener(new ActionListener() {
			public void actionPerformed(final ActionEvent e) {
				// 1) recuperation du numero de ligne du quai via la jtable
				final int numTrajet = tableau_.getSelectedRow();
				if (numTrajet == -1) {
					new BuDialogError(donnees_.application_.getApp(), Sinavi3Implementation.isSinavi_,
							"Erreur!! Vous devez cliquer sur le trajet a\n supprimer dans le tableau d'affichage!")
					.activate();

				}

				else {

					// on s occupe de la supresion des catégories de navires:
					// 1)on demande confirmation:
					final int confirmation = new BuDialogConfirmation(donnees_.application_.getApp(),
							Sinavi3Implementation.isSinavi_, "Etes-vous certain de vouloir supprimer le trajet ?").activate();

					if (confirmation == 0) {
						// 2)on supprime le trajet correspondant a la suppression
						donnees_.listeBateaux_.retournerNavire(listeBateaux.getSelectedIndex()).listeTrajet_.supprimerTrajet(numTrajet);
						//-- mise a jour du modele --//
						modeleTrajet.fireTableStructureChanged();
					}
				}

			}});
		
		
		modification_.addActionListener(new ActionListener() {
			public void actionPerformed(final ActionEvent e) {
				// 1) recuperation du numero de ligne du quai via la jtable
				final int numTrajet = tableau_.getSelectedRow();
				if (numTrajet == -1) {
					new BuDialogError(donnees_.application_.getApp(), Sinavi3Implementation.isSinavi_,
							"Erreur!! Vous devez cliquer sur le trajet a\n modifier dans le tableau d'affichage!")
					.activate();

				}

				else {
					
					//--modeModification --//
					modeModification(numTrajet);
					
				}
			

		}});
	
		            
	}

	/**methode de mise a jour des composants graphiques tel que les combobox par exemple **/
	void majSaisie() {

		this.dgGareDepart.removeAllItems();
		this.dgGareArrivee.removeAllItems();
		


		for (int i = 0; i < this.donnees_.listeGare_.listeGares_.size(); i++) {
			this.dgGareDepart.addItem(this.donnees_.listeGare_.retournerGare(i));
			this.dgGareArrivee.addItem(this.donnees_.listeGare_.retournerGare(i));
		}
		
		
			listeBateaux.removeAllItems();
			for (int i = 0; i < this.donnees_.listeBateaux_.listeNavires_.size(); i++) {
				this.listeBateaux.addItem(this.donnees_.listeBateaux_.retournerNavire(i).nom);

			}
			this.listeBateaux.addItem("Voir tous");


	}

	/**dans cette méthode figure l'initialisation de l'ensemble des composants **/
	public void initialiser(){
		
		
		if(fenetreLoideter_!=null)
			fenetreLoideter_.setVisible(false);
		if(fenetreLoiJournaliere_!=null)
			fenetreLoiJournaliere_.setVisible(false);
		fenetreLoideter_=null;
		fenetreLoiJournaliere_=null;
		this.dgGareDepart.setSelectedIndex(0);
		this.dgGareArrivee.setSelectedIndex(1);
		this.dgSens.setSelectedIndex(0);
		this.erlangdebut.setText(""+0);
		this.erlangfin.setText(""+24);

		choixLoiGenerationNav_.setSelectedIndex(0);
		this.loiGenerationNavErlang_.setSelectedIndex(0);
		loiErlangEcartType_.setText("");
		radioErlangLoi.setSelected(true);
		radioErlangEcart.setSelected(false);
		setErlangLoi(true);
		nbBateauxAttendus_.setText("");
		nbBateauxAttendus_.setEnabled(true);
		erlangdebut.setEnabled(true);
		erlangfin.setEnabled(true);
		this.loiDeterministe_ = new ArrayList();

		validerNavire_.setText("Ajouter");
		MODIF_ON=false;

		
		
	}

	/** methode qui remplit automatiquement les informations**/
	public void modeModification(int numTrajet){
		
		indiceTrajetAmodifier=numTrajet;
		
		//-- recuperation du trajet --//
		Sinavi3Trajet trajet=donnees_.listeBateaux_.retournerNavire(listeBateaux.getSelectedIndex()).listeTrajet_.retournerTrajet(numTrajet);
		
		
		
		
		
		//-- remplissage des données du trajet dans le formulaire --//
		MODIF_ON=true;
		if (trajet.gareDepart_ >= this.donnees_.listeGare_.listeGares_.size()) this.dgGareDepart.setSelectedIndex(0); else this.dgGareDepart.setSelectedIndex(trajet.gareDepart_);
		if (trajet.gareArrivee_ >= this.donnees_.listeGare_.listeGares_.size()) this.dgGareArrivee.setSelectedIndex(0); else this.dgGareArrivee.setSelectedIndex(trajet.gareArrivee_);
		this.dgSens.setSelectedIndex(trajet.sens_);
		choixLoiGenerationNav_.setSelectedIndex(trajet.typeLoiGenerationNavires_);
		
		if(fenetreLoideter_!=null)
			fenetreLoideter_.setVisible(false);
		if(fenetreLoiJournaliere_!=null)
			fenetreLoiJournaliere_.setVisible(false);
		fenetreLoideter_=null;
		fenetreLoiJournaliere_=null;
		
		if(trajet.typeLoiGenerationNavires_==0)
			{
			nbBateauxAttendus_.setText(""+trajet.nbBateauxattendus);
			this.loiGenerationNavErlang_.setSelectedIndex(trajet.loiErlangGenerationNavire-1);	
			
			loiErlangEcartType_.setText(""+ (float)trajet.loiErlangEcartType);
			radioErlangLoi.setSelected(trajet.getTypeErlangLoi()==0);
			radioErlangEcart.setSelected(trajet.getTypeErlangLoi()==1);
			
			erlangdebut.setText(""+trajet.erlangDebut);
			erlangfin.setText(""+trajet.erlangFin);
			}
		else
			{
				nbBateauxAttendus_.setText("");
				erlangdebut.setText("");
				erlangfin.setText("");
			}
		this.loiDeterministe_=trajet.loiDeterministeOUjournaliere_;
		
		validerNavire_.setText("Modifier");
	}


	boolean verificationSaisie(){

		if(nbBateauxAttendus_.getText().equals("") && choixLoiGenerationNav_.getSelectedIndex()==0)
			{
			new BuDialogError(donnees_.application_.getApp(), donnees_.application_.isSinavi_,
			"Le nombre de navires attendus est manquant.").activate();
				return false;
			}
		if(erlangdebut.getText().equals("") && choixLoiGenerationNav_.getSelectedIndex()==0)
		{
		new BuDialogError(donnees_.application_.getApp(), donnees_.application_.isSinavi_,
		"heure début erlang manquante.").activate();
			return false;
		}
		if(erlangfin.getText().equals("") && choixLoiGenerationNav_.getSelectedIndex()==0)
		{
		new BuDialogError(donnees_.application_.getApp(), donnees_.application_.isSinavi_,
		"heure fin erlang manquante.").activate();
			return false;
		}
		if(dgGareArrivee.getSelectedIndex()==dgGareDepart.getSelectedIndex())
		{
			new BuDialogError(donnees_.application_.getApp(), donnees_.application_.isSinavi_,
			"les gares départ et arrivée doivent être différentes.").activate();
				return false;
			
		}

		return true;
	}

	/**Methode qui consiste à ajouter un trajet ou alors modifier un trajet dans le cas ou c'est necessaire **/
	public void ajouterTrajet(){

		if(verificationSaisie())
		{

			//-- recuperation de la liste trajet du bateau correspondant: --//
			Sinavi3Trajets listeTrajet=donnees_.listeBateaux_.retournerNavire(listeBateaux.getSelectedIndex()).listeTrajet_;
			
			//-- creation d'un nouveu trajet --//
			Sinavi3Trajet nouveauTrajet=new Sinavi3Trajet();
			
			nouveauTrajet.gareDepart_=dgGareDepart.getSelectedIndex();
			nouveauTrajet.gareArrivee_=dgGareArrivee.getSelectedIndex();
			nouveauTrajet.sens_=dgSens.getSelectedIndex();
			nouveauTrajet.loiDeterministeOUjournaliere_=loiDeterministe_;
			nouveauTrajet.loiErlangGenerationNavire=loiGenerationNavErlang_.getSelectedIndex()+1;
			if(loiErlangEcartType_.getText() != null && loiErlangEcartType_.getText().length()>0)
				nouveauTrajet.loiErlangEcartType=Double.parseDouble(loiErlangEcartType_.getText());
			
			if(radioErlangEcart.isSelected())
				nouveauTrajet.typeErlangLoi = 1;
			else 
				nouveauTrajet.typeErlangLoi = 0;
			
			if(choixLoiGenerationNav_.getSelectedIndex()==0) {
				nouveauTrajet.nbBateauxattendus=Integer.parseInt(nbBateauxAttendus_.getText());
				nouveauTrajet.erlangDebut=Double.parseDouble(erlangdebut.getText());
				nouveauTrajet.erlangFin=Double.parseDouble(erlangfin.getText());
			}
				
			nouveauTrajet.typeLoiGenerationNavires_=choixLoiGenerationNav_.getSelectedIndex();
			
			
			//--test du mode modification --//
			
			if(!MODIF_ON){

				//-- mode ajout --//
				
				
				//-- ajout du trajet dans la liste des trajets
				listeTrajet.ajouterTrajet(nouveauTrajet);
				
				
				
			}
			else{
				//-- mode modification --//
				listeTrajet.modifierTrajet(nouveauTrajet, indiceTrajetAmodifier);
				//-- fin du menu modification, on reinitialise au mode ajout --//
				MODIF_ON=false;
			}

			//--mise a jour du modèle --//
			modeleTrajet.fireTableStructureChanged();
			
			//-- on reinitialise les données--//
			initialiser();

		}
		


	}



}
