package org.fudaa.fudaa.sinavi3;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Observer;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EtchedBorder;

import com.memoire.bu.BuBrowserFrame;
import com.memoire.bu.BuButton;
import com.memoire.bu.BuDialogConfirmation;
import com.memoire.bu.BuDialogError;
import com.memoire.bu.BuDialogMessage;
import com.memoire.bu.BuDialogWarning;
import com.memoire.fu.FuLog;

import org.apache.commons.io.FileUtils;
import org.fudaa.dodico.corba.sinavi2.SSimulationSinavi2;
import org.fudaa.dodico.corba.sinavi3.*;
import org.fudaa.dodico.corba.sipor.SParametresHorairesNavires2;
import org.fudaa.dodico.fortran.FortranWriter;
import org.fudaa.fudaa.commun.projet.FudaaFiltreFichier;
import org.fudaa.fudaa.commun.projet.FudaaProjet;
import org.fudaa.fudaa.ressource.FudaaResource;
import org.fudaa.fudaa.sinavi3.structures.DureeManeuvreEcluse;
import org.fudaa.fudaa.sinavi3.structures.SinaviStructureReglesRemplissageSAS;
import org.fudaa.fudaa.sinavi3.structures.DureeManeuvreEcluse.DureeParcours;
import org.fudaa.fudaa.sinavi3.ui.panel.SinaviPanelDureeManeuvresEcluses;
import org.fudaa.dodico.corba.sinavi3.Sinavi3ResultatGarage;
/**
 * Classe fondamentale de l'application sinavi. Donnes globales de la simulation cette classe regroupe les donn�es
 * suivante: - reference de l'implementation sinavi - les classes des diff�rentes
 * donn�es(quai,ecluses,cheneaux,cercles,cat�gories de navires...) - la d�finition du projet sinavi - les structure
 * SParametres destin�es � la jonction Fortran/java - les m�thodes de sauvegardes / chargement des donn�es du projet -
 * les m�thodes de transfert classes donn�es => structures (et r�ciproquement)
 * 
 * @author Adrien Hadoux
 */

public class Sinavi3DataSimulation {

	// definitions des attributs

	/**
	 * Projet sinavi.
	 */
	protected FudaaProjet projet_;

	/**
	 * Structure regroupant toutes les donn�es du projet Fudaa sinavi:IDL.
	 */
	protected SParametresSinavi32 params_ = new SParametresSinavi32();

	/**
	 * Structure des resultats de la simulation IDL.
	 */
	Sinavi3ResultatListeevenementsSimu listeResultatsSimu_;
	
	/**
	 * Structure des resultats d'occupation de la simulation IDL.
	 */
	Sinavi3ResultatListeOccupations listeResultatsOccupationsSimu_;
	
	/**
	 * Structure des resultats de bassinees de la simulation IDL.
	 */
	Sinavi3ResultatListeBassinees listeResultatsBassineesSimu_;

	
	List<Sinavi3ResultatConsoEau> listeResultatsConsommationEau_;
	
	List<Sinavi3ResultatGarage> listeResultatsGarages;

	/**
	 * Tableau de catgories de Navires.
	 */
	Sinavi3Navires listeBateaux_ = new Sinavi3Navires();

	
	/**
	 * les donn�es des gares.
	 */
	Sinavi3Gares listeGare_ = new Sinavi3Gares();


	/**
	 * Les donn�es des cheneaux.
	 */
	Sinavi3Biefs listeBief_ = new Sinavi3Biefs();

	/**
	 * Les donn�es des ecluses.
	 */
	Sinavi3Ecluses listeEcluse_ = new Sinavi3Ecluses();

	/**
	 * Structure de gestion des vitesses des bateaux dans les tron�ons.
	 */
	Sinavi3StrutureReglesDureesParcours reglesVitesseBiefAvalant_ = new Sinavi3StrutureReglesDureesParcours();
	Sinavi3StrutureReglesDureesParcours reglesVitesseBiefMontant_ = new Sinavi3StrutureReglesDureesParcours();
	SinaviStructureReglesRemplissageSAS reglesRemplissageSAS_ = new SinaviStructureReglesRemplissageSAS();
	/**
	 * Structure des dur�es de manoeuvres dasn les ecluses
	 */
	
	/*Sinavi3StrutureReglesDureesParcours durManeuvreEclEntrant_= new Sinavi3StrutureReglesDureesParcours();
	Sinavi3StrutureReglesDureesParcours durManeuvreEclSortant_ = new Sinavi3StrutureReglesDureesParcours();
*/
	DureeManeuvreEcluse dureeManeuvresEcluses = new DureeManeuvreEcluse();

	/**
	 * Liste d'horaires pour les categories de navires ces horaires ne sont pas enregistr� dans les sauvegardes, ils
	 * servent juste a rendre la saisie plus rapide et ergonomique.
	 */
	// FRED: JAMAIS UTILISE
//	ArrayList listeHorairesNavires_ = new ArrayList();

	/**
	 * Interface parent sinavi.
	 */
	final Sinavi3Implementation application_;

	/**
	 * sous fenetre de verification des donn�es saisies.
	 */
	Sinavi3InternalFrame frameVerif_;



	/**
	 * Structure GENARR pour lire les navires g�n�r�es
	 * par l'executable GENARR. 
	 */
	GenarrListeNavires genarr_=new GenarrListeNavires();


	public Map<Integer, Integer> mapBassineeEcluses = new HashMap<Integer, Integer>();
	
	/** DESIGN PATTERN Observer-Observable.
	 * Observable du modele**/
	private static Sinavi3ObservableSupport observable=null;

	/** methode d acces a l observable * */
	public static Sinavi3ObservableSupport getObservable() {
		if (observable == null)
			observable = new Sinavi3ObservableSupport();
		return observable;
	}

	/** 
	 * Fonction permettant d'enregistrer un Observer
	 * qui sera pr�venu lors de la modification d'une
	 * donn�es du mod�le.
	 * 
	 * @param ob Observer � enregistrer
	 */
	public void addObservers(Observer ob) {
		getObservable().addObserver(ob);
	}

	/** 
	 * Fonction permettant de sp�cifier aux Observer qu'une
	 * partie des donn�es du mod�le ont �t� modifi�es
	 * 
	 * @param modelPart Partie du mod�le ayant �t� modifi�e.
	 */
	public static void setProperty(Object modelPart) {
		// appel aux methodes de mise a jour de chaque panel mode expert
		getObservable().notifyObservers(modelPart);
	}

        public Sinavi3DataSimulation() {
            this(null);
        }

	public Sinavi3DataSimulation(final Sinavi3Implementation _application) {

		application_ = _application;

		// initialisation du projet a null
		this.projet_ = null;

		// initialisation du graphe de topologie
		this.params_.grapheTopologie = new SParametresGrapheTopologies();
		this.params_.grapheTopologie.nbArcs = 0;
		this.params_.grapheTopologie.graphe = new SParametresGrapheTopologie[2000];

		// initialisation des donn�es g�n�rales
		this.params_.donneesGenerales = new SParametresDonneesGenerales2();
		final java.util.Random random = new java.util.Random();
		int valal = random.nextInt();
		if (valal < 0) {
			valal = valal * -1;
		}
		this.params_.donneesGenerales.graine = valal;
		this.params_.donneesGenerales.jourDepart = 1;
		this.params_.donneesGenerales.nombreJours = 365;
		this.params_.donneesGenerales.nombreJoursFeries = 0;
		this.params_.donneesGenerales.piedDePilote = 10;



		// initialisation des structures de r�sultats:
		this.params_.ResultatsCompletsSimulation = new SParametresResultatsCompletSimulation();

	}

	private  String projectName_ = "sinavi";
	private final String oldProjectName_ = "nom";

    public void setProjectName(final String res) {
        projectName_ = res;
    }
        
public void creer() {

		// Cr�ation d'un nouveau projet et tentative d'ouverture

		/**
		 * creation d un nouveau projet pour datasimulation
		 */
		this.projet_ = new FudaaProjet(this.application_.getApp(), new FudaaFiltreFichier(projectName_));

		/**
		 * Initialisation du projet
		 */
		this.projet_.creer();

		final FudaaProjet projet = this.projet_;

		if (!projet.estConfigure()) {
			projet.fermer(); // cr�ation de projet annul�e
		} else { // nouveau projet cr��
			String nomFichierVoulu = projet.getFichier();
			// ajoute l'extension .sinavi si elle manque
			if (nomFichierVoulu.lastIndexOf(".sinavi") != (nomFichierVoulu.length() - 7)) {
				projet.setFichier(projet.getFichier() + ".sinavi");
				nomFichierVoulu = projet.getFichier();
			}
			if (this.application_.projets_.containsKey(nomFichierVoulu)) {
				new BuDialogError(this.application_.getApp(), this.application_.getInformationsSoftware(),
						"Fichier de meme nom d�j� ouvert").activate();
				projet.fermer();
				return;
			}

			// A: initialisation des donn�es
//			donneesGenerales_ = new Sinavi3DonneesGenerales();
			listeBateaux_ = new Sinavi3Navires();

//			listeHoraire_ = new Sinavi3ListeHoraires();
//			nbHoraires_ = 0;
			listeGare_ = new Sinavi3Gares();

			listeBief_ = new Sinavi3Biefs();
			listeEcluse_ = new Sinavi3Ecluses();
			// final Sinavi3StrutureReglesDureesParcours reglesDureesParcoursChenal_ = new Sinavi3StrutureReglesDureesParcours();
			// final Sinavi3StrutureReglesDureesParcours reglesDureesParcoursCercle_ = new Sinavi3StrutureReglesDureesParcours();
//			listeHorairesNavires_ = new ArrayList();

			// Ajoute le projet qui vient d'etre ouvert et en fait le projet courant.
			this.application_.liste_.ajouteSimulation(nomFichierVoulu, projet);
			this.application_.liste_.setSelectedValue(nomFichierVoulu, true);

			this.application_.outils_.creationDonnees();

			this.application_.activerCommandesSimulation();
			this.application_.activeActionsExploitation();

			  //-- on ouvre par d�faut le r�seau --//
			  getApplication().getNetworkEditor().cleanAllNetwork();
		      getApplication().activerNetworkEditor();
			
		}

	}

	void enregistrer() {
		/**
		 * 1) recopiage des donn�es de datasimulation dans le projet via les SParametres
		 */
		setParametresProjet();

		/**
		 * 2)ajout des parametre recopi� pr�c�demment
		 */
		this.projet_.addParam(Sinavi3Resource.parametres, this.params_);

		/**
		 * 4)enregistrement du projet
		 */
		projet_.setEnrResultats(Sinavi3Preferences.SINAVI.getBooleanProperty("enregistrerResultats"));
		// enregistrement des donn�es
		this.projet_.enregistre();
		
		 //-- enregistrement du graphe --//
	    if(getApplication().getNetworkEditor().hasChanged()) {
	    	try {
				getApplication().getNetworkEditor().saveGraph(getProjet_().getFichier() + ".network");
			} catch (IOException e) {
				FuLog.error(e);
			}
	    }
		

	}

	void enregistrerSous() {

		/**
		 * 1) recopiage des donn�es de datasimulation dans le projet via les SParametres
		 */
		setParametresProjet();

		/**
		 * 2)ajout des parametre recopi� pr�c�demment
		 */
		this.projet_.addParam(Sinavi3Resource.parametres, this.params_);

		/** 3) appel de la m�thode creer() */
		this.projet_.creer();

		/**
		 * 4)enregistrement du projet
		 */
		projet_.setEnrResultats(Sinavi3Preferences.SINAVI.getBooleanProperty("enregistrerResultats"));
		// enregistrement des donn�es
		this.projet_.enregistre();

	}

	/**
	 * Methode d'ouverture d'un projet de type Fudaa Projet.
	 */

public	void ouvrir(String fichier) {

		// mise en m�moire de l'ancien projet avant ouverture d'un nouveau:
		// TRES UTILE DANS LA MESURE OU L OUVERTURE DONNE LIEU A UN ECHEC
		// ON PEUT AINSI RETABLIR L ANCIEN PROJET QUI LUI EST STABLE !!!
		String nomAncienProjet;
		if (projet_ != null) {
			nomAncienProjet = projet_.getFichier();
		} else {
			nomAncienProjet = null;
		}

		String repertoire=null;


		// Creation d'un nouveau projet et tentative d'ouverture
		repertoire = System.getProperty("user.dir") + File.separator + "exemples" + File.separator
		+ projectName_;

		/**
		 * 1)ouverture du projet Fudaa
		 */
		// FIXME:
		// fred aie equals(null) impossible
		// comment est-ce possible repertoire==null alors qu'il vient d'etre initialise
		// le reste aussi est bizarre car toujours faux. En effet, repertoire vaut au moins blablalab/exemples/blablle
		if (/* repertoire == null || repertoire.equals(null) || */repertoire.equals("null")
				|| repertoire.equals("null.sinavi")) {
			new BuDialogError(this.application_.getApp(), this.application_.getInformationsSoftware(),
			"Interuption du chargement du fichier").activate();




			return;
		}

		// A) creation du projet
		try {

			/*
    	String filtres[]=new String[2];
      filtres[0]=projectName_;
      filtres[1]=oldProjectName_;
			 */                        
			projet_ = new FudaaProjet(this.application_.getApp(), new FudaaFiltreFichier(projectName_));

			/*
      //-- cas special: ancien projet portant l'extension nom: on active la moulinette pour transformer le fichier en .sinavi --//
      String extensionFic=projet_.getFichier().substring(projet_.getFichier().length()-10,projet_.getFichier().length()-7);
      System.out.println("extension: "+extensionFic+"\nnom complet: "+projet_.getFichier());
      if(extensionFic.equals(oldProjectName_)){


      	 new BuDialogError(this.application_.getApp(), this.application_.getInformationsSoftware(),
           "Ancien format de fichier.\n ce format va etre convertit en fichier .sinavi").activate();

      	 //-- lecture de la structure sinavi 2 --//
      	 String nomProjetSansExtensions=projet_.getFichier().substring(0,projet_.getFichier().length()-11);
      	 System.out.println("nom projet sans extension: "+nomProjetSansExtensions);
      	 //-- transformation du nom du projet --//
      	 String nomProjet=nomProjetSansExtensions+"."+oldProjectName_;
      	 System.out.println("nom du fichier avec extension old: "+nomProjet);
      	 projet_.setFichier(nomProjet);

      	 //-- enregistrement du projet --//
      	 //sous le nouveau nom
      	 projet_.setFichier(nomProjetSansExtensions+"."+projectName_);
      	 System.out.println("nom du fichier avec extension new: "+nomProjetSansExtensions+"."+projectName_);
      	 //enregistrer();

       return;
      }
			 */

			// B) ouverture du projet

			if(fichier==null)
				//-- option ouvrir --//
				this.projet_.ouvrir();
			else
				//- option reouvrir --//
				this.projet_.ouvrir(fichier);

		} catch (final java.lang.ClassCastException c) {
			// fred il y a erreur
			projet_ = null;
			new BuDialogError(this.application_.getApp(), this.application_.getInformationsSoftware(),
			"Ce fichier ne contient pas de donn�es Sinavi valides.").activate();

		}
		// fred donc si projet_ ==null on arrete
		if (projet_ == null) return;

		/**
		 * 2) recuperation des donn�es dans le projet
		 */



		// B: recuperation des donnees du fichier de chargement:
		this.params_ = (SParametresSinavi32) this.projet_.getParam(Sinavi3Resource.parametres);

		if (this.params_ == null) {
			projet_ = null;
	/*		new BuDialogError(this.application_.getApp(), this.application_.getInformationsSoftware(),
					"Vous n'avez pas selectionn� de fichier valide!!\n\n Ouverture impossible").activate();*/
			// on reinitialise el projet a null pour eviter tout type de conflits
			// this.projet_=null;
			initParams();
			// Retour sur un precedent projet dans le cas ou la liste de projet est non vide
			if (!this.application_.projets_.isEmpty() && nomAncienProjet != null) {
				// on fais appel a la methode changerProjet qui
				// permet de relancer un projet existant non foireux
				// on relance l'ancien projet:
				changerProjet((FudaaProjet) this.application_.projets_.get(nomAncienProjet));

			}

			return;
		}

		/**
		 * 3)recopiage des don�nes du projet dans DataSimulation
		 */
		getParametresProjet();

		// new BuDialogMessage(this.application_.getApp(), this.application_.getInformationsSoftware(), "Param�tres charg�s
		// avec
		// succ�s").activate();

		/**
		 * 4) activation des commandes des fenetres de l application sinavi
		 */
		this.activerCommandesNiveau();

		/** methode qui permet l exploiattion des resultats */
		// this.application_.activeActionsExploitation();
		/**
		 * 5) verification de la configuration du projet
		 */
		if (!projet_.estConfigure()) {
			projet_.fermer();
		} else {
			// 1)ouverture du projet r�ussie
			// 2)on v�rifie que le projet n est pas d�ja ouvert:
			System.out.println("Ouverture r�ussie de");
			final String nomFichierVoulu = this.projet_.getFichier();
			if (this.application_.projets_.containsKey(nomFichierVoulu)) {
				new BuDialogError(this.application_.getApp(), this.application_.getInformationsSoftware(),
						"Fichier d�j� ouvert").activate();
				this.projet_.fermer();
				return;
			}

			/**
			 * 6) ajout dans la liste des projets
			 */
			// Ajoute le projet qui vient d'etre ouvert et en fait le projet courant.
			this.application_.liste_.ajouteSimulation(nomFichierVoulu, this.projet_);
			this.application_.liste_.setSelectedValue(nomFichierVoulu, true);


			/** 7) ajout dans la liste des fichiers a reouvrir du ficheir ouvert **/
			final String r = projet_.getFichier();
			this.application_.getMainMenuBar().addRecentFile(r,  "");
			Sinavi3Preferences.SINAVI.writeIniFile();
			//AlbePreferences.ALBE.writeIniFile();
			
			//-- affiche le graphe au chargement --//
		    getApplication().activerNetworkEditor();
		      
			

		}

	}

	protected void initParams() {
		this.params_ = new SParametresSinavi32();
		// initialisation du graphe de topologie
		this.params_.grapheTopologie = new SParametresGrapheTopologies();
		this.params_.grapheTopologie.nbArcs = 0;
		this.params_.grapheTopologie.graphe = new SParametresGrapheTopologie[2000];
		// initialisation desdon�nes g�n�rales
		this.params_.donneesGenerales = new SParametresDonneesGenerales2();
		final java.util.Random random = new java.util.Random();
		int valal = random.nextInt();
		if (valal < 0) {
			valal = valal * -1;
		}
		this.params_.donneesGenerales.graine = valal;
		this.params_.donneesGenerales.jourDepart = 1;
		this.params_.donneesGenerales.nombreJours = 365;
		this.params_.donneesGenerales.nombreJoursFeries = 0;
		this.params_.donneesGenerales.piedDePilote = 10;

		// initialisation des structures de r�sultats:
		this.params_.ResultatsCompletsSimulation = new SParametresResultatsCompletSimulation();
	}

	/**
	 * methode appel�e lors du clic sur la liste des simulations de l'application.
	 * 
	 * @Param: nouveau projet projet correspondant a la ligne selectionnee dans la liste de selection des simulations
	 */
	void changerProjet(final FudaaProjet _nouveauProjet) {

		// on commence par reinitialiser la totalit� des sous interfaces:
		this.application_.initialiserToutesLesInterfaces();

		try {
			projet_ = _nouveauProjet;
			// B) ouverture du projet
			// this.projet_.ouvrir();

		} catch (final java.lang.ClassCastException c) {

			new BuDialogError(this.application_.getApp(), this.application_.getInformationsSoftware(),
					"Ce fichier ne contient pas de donn�es Sinavi valides.").activate();

		}

		/**
		 * 2) recuperation des donn�es dans le projet
		 */

		// B: recuperation des donnees du fichier de chargement:
		this.params_ = (SParametresSinavi32) this.projet_.getParam(Sinavi3Resource.parametres);

		if (this.params_ == null) {
			new BuDialogError(this.application_.getApp(), this.application_.getInformationsSoftware(),
					"Ce fichier ne contient aucun param�tre.").activate();
			initParams();
			return;
		}

		/**
		 * 3)recopie des don�nes du projet dans DataSimulation
		 */
		getParametresProjet();
                this.application_.activerNetworkEditor();
		new BuDialogMessage(this.application_.getApp(), this.application_.getInformationsSoftware(), "Simulation: "
				+ projet_.getFichier() + "\nParam�tres charg�s avec succ�s.").activate();

	}

	/**
	 * Methode permettant de dupliquer la simulation.
	 */
	void dupliquerSimulation() {

		File previousSimulation = new File(this.projet_.getFichier());
		
		/**
		 * creation d un nouveau projet pour datasimulation
		 */
		this.projet_ = new FudaaProjet(this.application_.getApp(), new FudaaFiltreFichier(projectName_));

		/**
		 * Initialisation du projet
		 */
		this.projet_.creer();
		
		String folder = this.projet_.getFichier().replace(".sinavi","");
		File directory = new File(folder);
		if(!directory.exists()){
			directory.mkdir();
		}
		if(directory.exists()) {
			this.projet_.setFichier(directory.getPath()+File.separator+directory.getName());
		}
		
		/**
		 * Reinitialisation des parametres puisque mal charg�s
		 */
		final SParametresSinavi32 copieParams = new SParametresSinavi32();
		// initialisation du graphe de topologie
		copieParams.grapheTopologie = new SParametresGrapheTopologies();
		copieParams.grapheTopologie.nbArcs = 0;
		copieParams.grapheTopologie.graphe = new SParametresGrapheTopologie[2000];
		// initialisation desdon�nes g�n�rales
		copieParams.donneesGenerales = new SParametresDonneesGenerales2();
		copieParams.donneesGenerales.jourDepart = 1;
		copieParams.donneesGenerales.nombreJours = 365;
		copieParams.donneesGenerales.nombreJoursFeries = 0;
		copieParams.donneesGenerales.piedDePilote = 10;


		/**
		 * recopiage des donn�es des mar��s recopiage du graphe recopiage des donn�es g�n�rales
		 */
		copieParams.donneesGenerales.jourDepart = this.params_.donneesGenerales.jourDepart;
		copieParams.donneesGenerales.nombreJours = params_.donneesGenerales.nombreJours;
		copieParams.donneesGenerales.nombreJoursFeries = params_.donneesGenerales.nombreJoursFeries;
		copieParams.donneesGenerales.piedDePilote = params_.donneesGenerales.piedDePilote;
		copieParams.donneesGenerales.tableauJoursFeries = new int[copieParams.donneesGenerales.nombreJoursFeries];
		for (int k = 0; k < copieParams.donneesGenerales.nombreJoursFeries; k++) {
			copieParams.donneesGenerales.tableauJoursFeries[k] = params_.donneesGenerales.tableauJoursFeries[k];
		}




		copieParams.grapheTopologie.nbArcs = params_.grapheTopologie.nbArcs;
		copieParams.grapheTopologie.nbGaresDessinnees = params_.grapheTopologie.nbGaresDessinnees;
		for (int k = 0; k < copieParams.grapheTopologie.nbArcs; k++) {
			copieParams.grapheTopologie.graphe[k] = new SParametresGrapheTopologie();
			copieParams.grapheTopologie.graphe[k].nomConnection = params_.grapheTopologie.graphe[k].nomConnection;
			copieParams.grapheTopologie.graphe[k].numGare1 = params_.grapheTopologie.graphe[k].numGare1;
			copieParams.grapheTopologie.graphe[k].numGare2 = params_.grapheTopologie.graphe[k].numGare2;
			copieParams.grapheTopologie.graphe[k].typeConnection = params_.grapheTopologie.graphe[k].typeConnection;
			copieParams.grapheTopologie.graphe[k].xGare1 = params_.grapheTopologie.graphe[k].xGare1;
			copieParams.grapheTopologie.graphe[k].xGare2 = params_.grapheTopologie.graphe[k].xGare2;
			copieParams.grapheTopologie.graphe[k].yGare1 = params_.grapheTopologie.graphe[k].yGare1;
			copieParams.grapheTopologie.graphe[k].yGare2 = params_.grapheTopologie.graphe[k].yGare2;

		}

		// les parametres recopies deviennent les parametres de base qui vont recevoir el contenu des vecteurs
		// dynamiques dans la methode enregistrer()
                if(params_.ResultatsCompletsSimulation != null) {
                 copieParams.ResultatsCompletsSimulation = params_.ResultatsCompletsSimulation;
                }
		this.params_ = copieParams;
		// 2) sauvegarde du projet dupliqu�
		this.enregistrer();

		final String nomSimu = this.projet_.getFichier();
		// 3) ajout dans la liste des imulation du projet dupliqu�:
		this.application_.liste_.ajouteSimulation(nomSimu, this.projet_);
		this.application_.liste_.setSelectedValue(nomSimu, true);
		
		this.copyFilePreviousSimulation(previousSimulation);

	}

	private void copyFilePreviousSimulation(final File previousSimulation) {
		String folderParent = previousSimulation.getParent();
		if(folderParent !=null) {
			final File folder = new File(folderParent);
			final String fileToCheck = previousSimulation.getName();
			for(File f: folder.listFiles()) {
				if(f.getName()!= null && f.getName().contains(fileToCheck)) {
					copyThisFileToSimulation(f,fileToCheck);
				}
			}
			
			
		}
		
	}

	private void copyThisFileToSimulation(final File f, final String fileName) {
		File currentSimu = new File(this.projet_.getFichier());
		if(currentSimu.exists() && currentSimu.getParent() != null) {
			String nameToChange = f.getName().replace(fileName, new File(this.projet_.getFichier()).getName());
		    File destFile = new File (currentSimu.getParent()+File.separator + nameToChange);
			try {
				FileUtils.copyFile(f, destFile);
			} catch (IOException e) {
				
				e.printStackTrace();
			}
		}
		
	}

	/**
	 * methode de recopiage des donn�es du projet charg� dans la structure dataSimuation.
	 */
	public void getParametresProjet() {

		// A: initialisation des donn�es
//		donneesGenerales_ = new Sinavi3DonneesGenerales();
		listeBateaux_ = new Sinavi3Navires();

//		listeHoraire_ = new Sinavi3ListeHoraires();
//		nbHoraires_ = 0;
		listeGare_ = new Sinavi3Gares();

		listeBief_ = new Sinavi3Biefs();
		listeEcluse_ = new Sinavi3Ecluses();
		reglesVitesseBiefAvalant_ = new Sinavi3StrutureReglesDureesParcours();
		reglesVitesseBiefMontant_= new Sinavi3StrutureReglesDureesParcours();
		dureeManeuvresEcluses = new DureeManeuvreEcluse();
		reglesRemplissageSAS_ = new SinaviStructureReglesRemplissageSAS();
//		listeHorairesNavires_ = new ArrayList();

		/** recopiage des donn�es des gares */

		/** 0)recopiage des donn�es associ�es a la matrice de parcours */
		for (int i = 0; i < this.params_.nbLignesDureeParcoursChenaux; i++) {
			this.reglesVitesseBiefAvalant_.ajoutLigne(this.params_.nbColonnesDureeParcoursChenaux);
			this.reglesVitesseBiefMontant_.ajoutLigne(this.params_.nbColonnesDureeParcoursChenaux);
		}
		

		/** 0)recopiage des donn�es associ�es a la matrice de remplissage SAS */
	    for (int i = 0; i < this.params_.nbLignesRemplissageSAS; i++) {
	        this.reglesRemplissageSAS_.ajoutLigne(this.params_.nbColonnesRemplissageSAS);
	      }
	       
	      for (int i = 0; i < this.params_.nbLignesRemplissageSAS; i++) {
	        for (int j = 0; j < this.params_.nbColonnesRemplissageSAS; j++) {
	          this.reglesRemplissageSAS_.modifierValeur(this.params_.matriceRemplissageSAS[i][j], i, j);
	        }
	      }
		

		for (int i = 0; i < this.params_.nbLignesDureeParcoursChenaux; i++) {
			for (int j = 0; j < this.params_.nbColonnesDureeParcoursChenaux; j++) {
				this.reglesVitesseBiefAvalant_.modifierValeur(this.params_.matriceDureeParcoursChenaux[i][j], i, j);
				this.reglesVitesseBiefMontant_.modifierValeur(this.params_.matriceDureeParcoursChenauxMontant[i][j], i, j);
			}
		}

		
		/** 1) les donn�es associ�es aux gares: */

		// a) recopiage des donn�es de SParametresGares dans la structure de Sinavi3DataSimulation:
		for (int i = 0; i < this.params_.gares.nbGares; i++) {
			this.listeGare_.ajout(this.params_.gares.listeGares[i].nom);
		}

		/** 2) les donn�es associ�es aux bassins: */



		/** 3) les donn�es associ�es aux ecluses: */
		initEcluse();

		/** 4) les donn�es associ�es aux cheneaux: */

		initChenaux();



		/** 7) les donn�es associ�es aux categories de navire: */

		initNavires();
		
		
		dureeManeuvresEcluses.initDataFromCorba(this, this.params_.matriceDurManeuvreEclEntrant,
				this.params_.matriceDurManeuvreEclSortant, 
				this.params_.matriceDurManeuvreEclEntrant2, 
				this.params_.matriceDurManeuvreEclSortant2);
		
	

	}
	
	
	
	private void initBateauxHoraires(int indice, final Sinavi3Bateau bateau) {
	    bateau.creneauxJournaliers_.lundiCreneau1HeureArrivee = this.getParams_().navires.listeNavires[indice].h.lundiCreneau1HeureArrivee;
	    bateau.creneauxJournaliers_.lundiCreneau1HeureDep = this.getParams_().navires.listeNavires[indice].h.lundiCreneau1HeureDep;
	    bateau.creneauxJournaliers_.lundiCreneau2HeureArrivee = this.getParams_().navires.listeNavires[indice].h.lundiCreneau2HeureArrivee;
	    bateau.creneauxJournaliers_.lundiCreneau2HeureDep = this.getParams_().navires.listeNavires[indice].h.lundiCreneau2HeureDep;
	    bateau.creneauxJournaliers_.lundiCreneau3HeureArrivee = this.getParams_().navires.listeNavires[indice].h.lundiCreneau3HeureArrivee;
	    bateau.creneauxJournaliers_.lundiCreneau3HeureDep = this.getParams_().navires.listeNavires[indice].h.lundiCreneau3HeureDep;

	    bateau.creneauxJournaliers_.mardiCreneau1HeureArrivee = this.getParams_().navires.listeNavires[indice].h.mardiCreneau1HeureArrivee;
	    bateau.creneauxJournaliers_.mardiCreneau1HeureDep = this.getParams_().navires.listeNavires[indice].h.mardiCreneau1HeureDep;
	    bateau.creneauxJournaliers_.mardiCreneau2HeureArrivee = this.getParams_().navires.listeNavires[indice].h.mardiCreneau2HeureArrivee;
	    bateau.creneauxJournaliers_.mardiCreneau2HeureDep = this.getParams_().navires.listeNavires[indice].h.mardiCreneau2HeureDep;
	    bateau.creneauxJournaliers_.mardiCreneau3HeureArrivee = this.getParams_().navires.listeNavires[indice].h.mardiCreneau3HeureArrivee;
	    bateau.creneauxJournaliers_.mardiCreneau3HeureDep = this.getParams_().navires.listeNavires[indice].h.mardiCreneau3HeureDep;

	    bateau.creneauxJournaliers_.mercrediCreneau1HeureArrivee = this.getParams_().navires.listeNavires[indice].h.mercrediCreneau1HeureArrivee;
	    bateau.creneauxJournaliers_.mercrediCreneau1HeureDep = this.getParams_().navires.listeNavires[indice].h.mercrediCreneau1HeureDep;
	    bateau.creneauxJournaliers_.mercrediCreneau2HeureArrivee = this.getParams_().navires.listeNavires[indice].h.mercrediCreneau2HeureArrivee;
	    bateau.creneauxJournaliers_.mercrediCreneau2HeureDep = this.getParams_().navires.listeNavires[indice].h.mercrediCreneau2HeureDep;
	    bateau.creneauxJournaliers_.mercrediCreneau3HeureArrivee = this.getParams_().navires.listeNavires[indice].h.mercrediCreneau3HeureArrivee;
	    bateau.creneauxJournaliers_.mercrediCreneau3HeureDep = this.getParams_().navires.listeNavires[indice].h.mercrediCreneau3HeureDep;

	    bateau.creneauxJournaliers_.jeudiCreneau1HeureArrivee = this.getParams_().navires.listeNavires[indice].h.jeudiCreneau1HeureArrivee;
	    bateau.creneauxJournaliers_.jeudiCreneau1HeureDep = this.getParams_().navires.listeNavires[indice].h.jeudiCreneau1HeureDep;
	    bateau.creneauxJournaliers_.jeudiCreneau2HeureArrivee = this.getParams_().navires.listeNavires[indice].h.jeudiCreneau2HeureArrivee;
	    bateau.creneauxJournaliers_.jeudiCreneau2HeureDep = this.getParams_().navires.listeNavires[indice].h.jeudiCreneau2HeureDep;
	    bateau.creneauxJournaliers_.jeudiCreneau3HeureArrivee = this.getParams_().navires.listeNavires[indice].h.jeudiCreneau3HeureArrivee;
	    bateau.creneauxJournaliers_.jeudiCreneau3HeureDep = this.getParams_().navires.listeNavires[indice].h.jeudiCreneau3HeureDep;

	    bateau.creneauxJournaliers_.vendrediCreneau1HeureArrivee = this.getParams_().navires.listeNavires[indice].h.vendrediCreneau1HeureArrivee;
	    bateau.creneauxJournaliers_.vendrediCreneau1HeureDep = this.getParams_().navires.listeNavires[indice].h.vendrediCreneau1HeureDep;
	    bateau.creneauxJournaliers_.vendrediCreneau2HeureArrivee = this.getParams_().navires.listeNavires[indice].h.vendrediCreneau2HeureArrivee;
	    bateau.creneauxJournaliers_.vendrediCreneau2HeureDep = this.getParams_().navires.listeNavires[indice].h.vendrediCreneau2HeureDep;
	    bateau.creneauxJournaliers_.vendrediCreneau3HeureArrivee = this.getParams_().navires.listeNavires[indice].h.vendrediCreneau3HeureArrivee;
	    bateau.creneauxJournaliers_.vendrediCreneau3HeureDep = this.getParams_().navires.listeNavires[indice].h.vendrediCreneau3HeureDep;

	    bateau.creneauxJournaliers_.samediCreneau1HeureArrivee = this.getParams_().navires.listeNavires[indice].h.samediCreneau1HeureArrivee;
	    bateau.creneauxJournaliers_.samediCreneau1HeureDep = this.getParams_().navires.listeNavires[indice].h.samediCreneau1HeureDep;
	    bateau.creneauxJournaliers_.samediCreneau2HeureArrivee = this.getParams_().navires.listeNavires[indice].h.samediCreneau2HeureArrivee;
	    bateau.creneauxJournaliers_.samediCreneau2HeureDep = this.getParams_().navires.listeNavires[indice].h.samediCreneau2HeureDep;
	    bateau.creneauxJournaliers_.samediCreneau3HeureArrivee = this.getParams_().navires.listeNavires[indice].h.samediCreneau3HeureArrivee;
	    bateau.creneauxJournaliers_.samediCreneau3HeureDep = this.getParams_().navires.listeNavires[indice].h.samediCreneau3HeureDep;

	    bateau.creneauxJournaliers_.dimancheCreneau1HeureArrivee = this.getParams_().navires.listeNavires[indice].h.dimancheCreneau1HeureArrivee;
	    bateau.creneauxJournaliers_.dimancheCreneau1HeureDep = this.getParams_().navires.listeNavires[indice].h.dimancheCreneau1HeureDep;
	    bateau.creneauxJournaliers_.dimancheCreneau2HeureArrivee = this.getParams_().navires.listeNavires[indice].h.dimancheCreneau2HeureArrivee;
	    bateau.creneauxJournaliers_.dimancheCreneau2HeureDep = this.getParams_().navires.listeNavires[indice].h.dimancheCreneau2HeureDep;
	    bateau.creneauxJournaliers_.dimancheCreneau3HeureArrivee = this.getParams_().navires.listeNavires[indice].h.dimancheCreneau3HeureArrivee;
	    bateau.creneauxJournaliers_.dimancheCreneau3HeureDep = this.getParams_().navires.listeNavires[indice].h.dimancheCreneau3HeureDep;

	    bateau.creneauxJournaliers_.ferieCreneau1HeureArrivee = this.getParams_().navires.listeNavires[indice].h.ferieCreneau1HeureArrivee;
	    bateau.creneauxJournaliers_.ferieCreneau1HeureDep = this.getParams_().navires.listeNavires[indice].h.ferieCreneau1HeureDep;
	    bateau.creneauxJournaliers_.ferieCreneau2HeureArrivee = this.getParams_().navires.listeNavires[indice].h.ferieCreneau2HeureArrivee;
	    bateau.creneauxJournaliers_.ferieCreneau2HeureDep = this.getParams_().navires.listeNavires[indice].h.ferieCreneau2HeureDep;
	    bateau.creneauxJournaliers_.ferieCreneau3HeureArrivee = this.getParams_().navires.listeNavires[indice].h.ferieCreneau3HeureArrivee;
	    bateau.creneauxJournaliers_.ferieCreneau3HeureDep = this.getParams_().navires.listeNavires[indice].h.ferieCreneau3HeureDep;
	  }
	

	private void initNavires() {
		for (int i = 0; i < this.params_.navires.nombreNavires; i++) {
			final Sinavi3Bateau nouveau = new Sinavi3Bateau();
			nouveau.nom = this.params_.navires.listeNavires[i].nom;
			nouveau.priorite = this.params_.navires.listeNavires[i].priorite;
			nouveau.largeurMax = this.params_.navires.listeNavires[i].largeurMax;
			nouveau.vitesseDefautMontant = this.params_.navires.listeNavires[i].vitesseMontant;
			nouveau.longueurMax = this.params_.navires.listeNavires[i].longueurMax;
			nouveau.vitesseDefautAvalant = this.params_.navires.listeNavires[i].vitesseAvalant;
			nouveau.dureeAttenteMaximaleAdmissible = this.params_.navires.listeNavires[i].dureeAttenteMaxAdmissible;

			nouveau.tirantEau = this.params_.navires.listeNavires[i].tirantEauEntree;
			nouveau.typeTirant = this.params_.navires.listeNavires[i].typeTirant;

			// horaires journaliers et creneaux d ouverture
			initBateauxHoraires(i, nouveau);



			//--    les trajets des navires --//

			//retrouver l indice de la gare
			for(int t=0;t<this.params_.navires.listeNavires[i].GareDepart.length;t++)
			{
				//-- creation d'un objet de type trajet --//
				Sinavi3Trajet trajet=new Sinavi3Trajet();

				trajet.gareDepart_=this.params_.navires.listeNavires[i].GareDepart[t];
				trajet.erlangDebut=this.params_.navires.listeNavires[i].erlangDebut[t];
				trajet.erlangFin=this.params_.navires.listeNavires[i].erlangFin[t];
				trajet.gareArrivee_ =this.params_.navires.listeNavires[i].GareArrivee[t];
				trajet.sens_=this.params_.navires.listeNavires[i].sens[t];
				trajet.typeLoiGenerationNavires_=this.params_.navires.listeNavires[i].typeLoiGenerationNavires[t];      
				//cas 0: loi d erlang
				trajet.nbBateauxattendus=this.params_.navires.listeNavires[i].nbNaviresAttendus[t];

				trajet.loiErlangGenerationNavire=this.params_.navires.listeNavires[i].loiErlangGeneration[t];
				trajet.loiErlangEcartType =this.params_.navires.listeNavires[i].loiErlangEcartType[t];
				trajet.typeErlangLoi =this.params_.navires.listeNavires[i].typeErlangLoi[t];
				
				//cas 1: loi JOURNALIERE
				if (this.params_.navires.listeNavires[i].typeLoiGenerationNavires[t] == 2) {
					//-- creation d'un arraylist qui contiendra l'ensemble des couples --//
					ArrayList listeJournaliere=new ArrayList();
					for (int k = 0; k < this.params_.navires.listeNavires[i].TableauloiJournaliere[t].length; k++) {
						if(this.params_.navires.listeNavires[i].TableauloiJournaliere[t][k]!=null){
							listeJournaliere.add(new CoupleLoiDeterministe(1,
									this.params_.navires.listeNavires[i].TableauloiJournaliere[t][k].horaire));
						}
					}
					//-- ajout de la liste journaliere dans la liste de listes --//
					trajet.loiDeterministeOUjournaliere_=listeJournaliere;
				}
				//cas 2: loi deterministe
				if (this.params_.navires.listeNavires[i].typeLoiGenerationNavires[t] == 1) {
					//-- creation d'un arraylist qui contiendra l'ensemble des couples --//
					ArrayList listeJournaliere=new ArrayList();
					for (int k = 0; k < this.params_.navires.listeNavires[i].TableauloiDeterministe[t].length; k++) {
						if(this.params_.navires.listeNavires[i].TableauloiDeterministe[t][k]!=null){
							listeJournaliere.add(new CoupleLoiDeterministe(this.params_.navires.listeNavires[i].TableauloiDeterministe[t][k].jour,
									this.params_.navires.listeNavires[i].TableauloiDeterministe[t][k].horaire));
						}

					}
					//-- ajout de la liste journaliere dans la liste de listes --//
					trajet.loiDeterministeOUjournaliere_=listeJournaliere;
				}

				//-- ajout du trajet dans la liste des trajets --//
				nouveau.listeTrajet_.ajouterTrajet(trajet);

			}


			this.listeBateaux_.ajout(nouveau);
		}
	}



	private void initChenaux() {
		for (int i = 0; i < this.params_.cheneaux.nbCheneaux; i++) {
			final Sinavi3Bief nouveau = new Sinavi3Bief(this.params_.cheneaux.listeCheneaux[i].dimensionMatriceCroisement);
			nouveau.nom_ = this.params_.cheneaux.listeCheneaux[i].nom;
			// nouveau.longueur_=this.params_.cheneaux.listeCheneaux[i].longueur;
			nouveau.profondeur_ = this.params_.cheneaux.listeCheneaux[i].profondeur;
			nouveau.longueur_ = this.params_.cheneaux.listeCheneaux[i].longueur;
			nouveau.largeur_ = this.params_.cheneaux.listeCheneaux[i].largeur;
			nouveau.vitesse_ = this.params_.cheneaux.listeCheneaux[i].vitesse;

			// nouveau.vitesse_=this.params_.cheneaux.listeCheneaux[i].vitesse;
			nouveau.gareAmont_ = this.params_.cheneaux.listeCheneaux[i].gareAmont;
			nouveau.gareAval_ = this.params_.cheneaux.listeCheneaux[i].gareAval;


			// loi indispo
			nouveau.dureeIndispo_ = this.params_.cheneaux.listeCheneaux[i].dureeIndispo;
			nouveau.loiIndispo_ = this.params_.cheneaux.listeCheneaux[i].loiIndispo;
			nouveau.typeLoi_ = this.params_.cheneaux.listeCheneaux[i].typeLoi;
			// cas 0: loi d erlang
			nouveau.frequenceMoyenne_ = this.params_.cheneaux.listeCheneaux[i].frequenceMoyenne;
			nouveau.loiFrequence_ = this.params_.cheneaux.listeCheneaux[i].loiFrequence;
			if (nouveau.typeLoi_ == 2) {
				for (int k = 0; k < this.params_.cheneaux.listeCheneaux[i].nombreHorairesJournaliers; k++) {

					nouveau.loiDeterministe_.add(new CoupleLoiDeterministe(1,
							this.params_.cheneaux.listeCheneaux[i].TableauloiJournaliere[k].horaire));

				}
			}

			// cas 2: loi deterministe

			// allocation de memoire pour le tableau de loi deterministe correspondant a al taille du vecteur

			// recopiage des donn�es une par une
			if (nouveau.typeLoi_ == 1) {
				for (int k = 0; k < this.params_.cheneaux.listeCheneaux[i].nombreCouplesDeterministes; k++) {

					nouveau.loiDeterministe_.add(new CoupleLoiDeterministe(
							this.params_.cheneaux.listeCheneaux[i].TableauloiDeterministe[k].jour,
							this.params_.cheneaux.listeCheneaux[i].TableauloiDeterministe[k].horaire));

				}
			}







			// recopiage des horaire
			nouveau.h_.semaineCreneau1HeureArrivee = this.params_.cheneaux.listeCheneaux[i].h.semaineCreneau1HeureArrivee;
			nouveau.h_.semaineCreneau1HeureDep = this.params_.cheneaux.listeCheneaux[i].h.semaineCreneau1HeureDep;
			nouveau.h_.semaineCreneau2HeureArrivee = this.params_.cheneaux.listeCheneaux[i].h.semaineCreneau2HeureArrivee;
			nouveau.h_.semaineCreneau2HeureDep = this.params_.cheneaux.listeCheneaux[i].h.semaineCreneau2HeureDep;
			nouveau.h_.semaineCreneau3HeureArrivee = this.params_.cheneaux.listeCheneaux[i].h.semaineCreneau3HeureArrivee;
			nouveau.h_.semaineCreneau3HeureDep = this.params_.cheneaux.listeCheneaux[i].h.semaineCreneau3HeureDep;

			// recopiage des regels de navigations:
			for (int k = 0; k < this.params_.cheneaux.listeCheneaux[i].dimensionMatriceCroisement; k++) {
				for (int l = 0; l < this.params_.cheneaux.listeCheneaux[i].dimensionMatriceCroisement; l++) {
					nouveau.reglesCroisement_.modification(this.params_.cheneaux.listeCheneaux[i].matriceCroisement[k][l],
							k, l);
					nouveau.reglesTrematage_.modification(this.params_.cheneaux.listeCheneaux[i].matriceTrematage[k][l], k, l);
				}
			}

			this.listeBief_.ajout(nouveau);
		}
	}

	private void initEcluse() {
		for (int i = 0; i < this.params_.ecluses.nbEcluses; i++) {
			final Sinavi3Ecluse nouveau = new Sinavi3Ecluse();
			nouveau.nom_ = this.params_.ecluses.listeEcluses[i].nom;
			nouveau.largeur_ = this.params_.ecluses.listeEcluses[i].largeur;
			nouveau.longueur_ = this.params_.ecluses.listeEcluses[i].longueur;
			nouveau.hauteurchute = this.params_.ecluses.listeEcluses[i].hauteurChute;
			nouveau.dureeManoeuvreEntrant_ = (int) this.params_.ecluses.listeEcluses[i].dureeDefManoeuvreEntrant;
			nouveau.dureeManoeuvreSortant_ = (int) this.params_.ecluses.listeEcluses[i].dureeDefManoeuvreSortant;
			
			nouveau.dureeManoeuvreEntrant2_ = (int) this.params_.ecluses.listeEcluses[i].dureeDefManoeuvreEntrant2;
			nouveau.dureeManoeuvreSortant2_ = (int) this.params_.ecluses.listeEcluses[i].dureeDefManoeuvreSortant2;
			
			
			nouveau.profondeur_ = this.params_.ecluses.listeEcluses[i].profondeur;
			nouveau.tempsFausseBassinneeMontant_ = (int) this.params_.ecluses.listeEcluses[i].tempsFausseBassineeMontant;
			nouveau.tempsFausseBassinneeAvalant_ = (int) this.params_.ecluses.listeEcluses[i].tempsFausseBassineeAvalant;

			nouveau.gareAmont_ = this.params_.ecluses.listeEcluses[i].gareAmont;
			nouveau.gareAval_ = this.params_.ecluses.listeEcluses[i].gareAval;

			// loi indispo
			nouveau.dureeIndispo_ = this.params_.ecluses.listeEcluses[i].dureeIndispo;
			nouveau.coefficientBassinEpargne_ = this.params_.ecluses.listeEcluses[i].coefficientBassinEpargne;
			nouveau.loiIndispo_ = this.params_.ecluses.listeEcluses[i].loiIndispo;
			nouveau.typeLoi_ = this.params_.ecluses.listeEcluses[i].typeLoi;
			// cas 0: loi d erlang
			nouveau.frequenceMoyenne_ = this.params_.ecluses.listeEcluses[i].frequenceMoyenne;
			nouveau.loiFrequence_ = this.params_.ecluses.listeEcluses[i].loiFrequence;
			if (nouveau.typeLoi_ == 2) {
				for (int k = 0; k < this.params_.ecluses.listeEcluses[i].nombreHorairesJournaliers; k++) {

					nouveau.loiDeterministe_.add(new CoupleLoiDeterministe(1,
							this.params_.ecluses.listeEcluses[i].TableauloiJournaliere[k].horaire));

				}
			}

			// cas 2: loi deterministe

			// allocation de memoire pour le tableau de loi deterministe correspondant a al taille du vecteur

			// recopiage des donn�es une par une
			if (nouveau.typeLoi_ == 1) {
				for (int k = 0; k < this.params_.ecluses.listeEcluses[i].nombreCouplesDeterministes; k++) {

					nouveau.loiDeterministe_.add(new CoupleLoiDeterministe(
							this.params_.ecluses.listeEcluses[i].TableauloiDeterministe[k].jour,
							this.params_.ecluses.listeEcluses[i].TableauloiDeterministe[k].horaire));

				}
			}

			// recopiage des Horaires
			/*
			 * nouveau.h_.dimancheCreneau1HeureArrivee=this.params_.ecluses.listeEcluses[i].h.dimancheCreneau1HeureArrivee;
			 * nouveau.h_.dimancheCreneau1HeureDep=this.params_.ecluses.listeEcluses[i].h.dimancheCreneau1HeureDep;
			 * nouveau.h_.dimancheCreneau2HeureArrivee=this.params_.ecluses.listeEcluses[i].h.dimancheCreneau2HeureArrivee;
			 * nouveau.h_.dimancheCreneau2HeureDep=this.params_.ecluses.listeEcluses[i].h.dimancheCreneau2HeureDep;
			 * nouveau.h_.dimancheCreneau3HeureArrivee=this.params_.ecluses.listeEcluses[i].h.dimancheCreneau3HeureArrivee;
			 * nouveau.h_.dimancheCreneau3HeureDep=this.params_.ecluses.listeEcluses[i].h.dimancheCreneau3HeureDep;
			 * nouveau.h_.ferieCreneau1HeureArrivee=this.params_.ecluses.listeEcluses[i].h.ferieCreneau1HeureArrivee;
			 * nouveau.h_.ferieCreneau1HeureDep=this.params_.ecluses.listeEcluses[i].h.ferieCreneau1HeureDep;
			 * nouveau.h_.ferieCreneau2HeureArrivee=this.params_.ecluses.listeEcluses[i].h.ferieCreneau2HeureArrivee;
			 * nouveau.h_.ferieCreneau2HeureDep=this.params_.ecluses.listeEcluses[i].h.ferieCreneau2HeureDep;
			 * nouveau.h_.ferieCreneau3HeureArrivee=this.params_.ecluses.listeEcluses[i].h.ferieCreneau3HeureArrivee;
			 * nouveau.h_.ferieCreneau3HeureDep=this.params_.ecluses.listeEcluses[i].h.ferieCreneau3HeureDep;
			 * nouveau.h_.samediCreneau1HeureArrivee=this.params_.ecluses.listeEcluses[i].h.samediCreneau1HeureArrivee;
			 * nouveau.h_.samediCreneau1HeureDep=this.params_.ecluses.listeEcluses[i].h.samediCreneau1HeureDep;
			 * nouveau.h_.samediCreneau2HeureArrivee=this.params_.ecluses.listeEcluses[i].h.samediCreneau2HeureArrivee;
			 * nouveau.h_.samediCreneau2HeureDep=this.params_.ecluses.listeEcluses[i].h.samediCreneau2HeureDep;
			 * nouveau.h_.samediCreneau3HeureArrivee=this.params_.ecluses.listeEcluses[i].h.samediCreneau3HeureArrivee;
			 * nouveau.h_.samediCreneau3HeureDep=this.params_.ecluses.listeEcluses[i].h.samediCreneau3HeureDep;
			 */
			nouveau.h_.semaineCreneau1HeureArrivee = this.params_.ecluses.listeEcluses[i].h.semaineCreneau1HeureArrivee;
			nouveau.h_.semaineCreneau1HeureDep = this.params_.ecluses.listeEcluses[i].h.semaineCreneau1HeureDep;
			nouveau.h_.semaineCreneau2HeureArrivee = this.params_.ecluses.listeEcluses[i].h.semaineCreneau2HeureArrivee;
			nouveau.h_.semaineCreneau2HeureDep = this.params_.ecluses.listeEcluses[i].h.semaineCreneau2HeureDep;
			nouveau.h_.semaineCreneau3HeureArrivee = this.params_.ecluses.listeEcluses[i].h.semaineCreneau3HeureArrivee;
			nouveau.h_.semaineCreneau3HeureDep = this.params_.ecluses.listeEcluses[i].h.semaineCreneau3HeureDep;

			this.listeEcluse_.ajout(nouveau);
		}
	}

	/**
	 * methode de recopiage des donn�es de la structure dataSimuation dans le projet qui sera ensuite enregistr�.
	 */
	public void setParametresProjet() {

		/** recopiage des donn�es des gares */

		/** 0)recopiage des donn�es associ�es a la matrice de parcours */
		this.params_.nbColonnesDureeParcoursChenaux = this.listeBateaux_.listeNavires_.size();
		this.params_.nbLignesDureeParcoursChenaux = this.reglesVitesseBiefAvalant_.matriceDuree.size();

		// allocation de la memoire pour la matrice
		this.params_.matriceDureeParcoursChenaux = new double[this.params_.nbLignesDureeParcoursChenaux][this.params_.nbColonnesDureeParcoursChenaux];
		this.params_.matriceDureeParcoursChenauxMontant = new double[this.params_.nbLignesDureeParcoursChenaux][this.params_.nbColonnesDureeParcoursChenaux];

		this.params_.matriceDurManeuvreEclEntrant= new double[this.listeEcluse_.listeEcluses_.size()][this.listeBateaux_.listeNavires_.size()];
		this.params_.matriceDurManeuvreEclSortant = new double[this.listeEcluse_.listeEcluses_.size()][this.listeBateaux_.listeNavires_.size()];
		this.params_.matriceDurManeuvreEclEntrant2= new double[this.listeEcluse_.listeEcluses_.size()][this.listeBateaux_.listeNavires_.size()];
		this.params_.matriceDurManeuvreEclSortant2 = new double[this.listeEcluse_.listeEcluses_.size()][this.listeBateaux_.listeNavires_.size()];


		for (int i = 0; i < this.params_.nbLignesDureeParcoursChenaux; i++) {
			for (int j = 0; j < this.params_.nbColonnesDureeParcoursChenaux; j++) {
				this.params_.matriceDureeParcoursChenaux[i][j] = this.reglesVitesseBiefAvalant_.retournerValeur(i, j);
				this.params_.matriceDureeParcoursChenauxMontant[i][j] = this.reglesVitesseBiefMontant_.retournerValeur(i, j);
			}
		}
		//System.out.println("Voici les dur�es de maneuvres ecluses");
		for (int i = 0; i < this.getListeEcluse().getListeEcluses().size(); i++) {
			//System.out.println("");
			for (int j = 0; j < this.getListeBateaux_().NombreNavires(); j++) {
				DureeParcours duree =  this.dureeManeuvresEcluses.getDureeParcours(this.getListeEcluse().retournerEcluse(i),
						this.getListeBateaux_().retournerNavire(j));
				if(duree != null) {
					this.params_.matriceDurManeuvreEclEntrant[i][j] = duree.dureeEntrant1;
					this.params_.matriceDurManeuvreEclSortant[i][j] = duree.dureeSortant1;
					this.params_.matriceDurManeuvreEclEntrant2[i][j] = duree.dureeEntrant2;
					this.params_.matriceDurManeuvreEclSortant2[i][j] = duree.dureeSortant2;
				}
			}
		}


		 /** 0)recopiage des donn�es associ�es a la matrice de SAS */
	    this.getParams_().nbColonnesRemplissageSAS = this.listeBateaux_.listeNavires_.size();
	    this.getParams_().nbLignesRemplissageSAS = this.reglesRemplissageSAS_.matriceDelais.size();

	    // allocation de la memoire pour la matrice
	    this.getParams_().matriceRemplissageSAS = new double[this.getParams_().nbLignesRemplissageSAS][this.getParams_().nbColonnesRemplissageSAS];

	    for (int i = 0; i < this.getParams_().nbLignesRemplissageSAS; i++) {
	      for (int j = 0; j < this.getParams_().nbColonnesRemplissageSAS; j++) {
	        this.getParams_().matriceRemplissageSAS[i][j] = this.reglesRemplissageSAS_.retournerValeur(i, j);
	      }
	    }
	    
		
		
		/** 1) les donn�es associ�es aux gares: */
		// a) recopiage du nombre de gares
		setParametresGares();


		/** 3) les donn�es associ�es aux ecluses: */
		// a)recopiage du nombre d'ecluses et allocation de memoire
		setParametresEcluses();

		/** 4) les donn�es associ�es aux cheneaux: */
		setParametresChenaux();


		/** 7) les donn�es associ�es aux categories de navire: */
		setParametresNavires();

	}
	
	
	private void setParametresHorairesBateau(int indice, final Sinavi3Bateau bateau) {
	    this.getParams_().navires.listeNavires[indice].h = new org.fudaa.dodico.corba.sinavi3.SParametresHorairesNavires2();
	    this.getParams_().navires.listeNavires[indice].h.lundiCreneau1HeureArrivee = bateau.creneauxJournaliers_.lundiCreneau1HeureArrivee;
	    this.getParams_().navires.listeNavires[indice].h.lundiCreneau1HeureDep = bateau.creneauxJournaliers_.lundiCreneau1HeureDep;
	    this.getParams_().navires.listeNavires[indice].h.lundiCreneau2HeureArrivee = bateau.creneauxJournaliers_.lundiCreneau2HeureArrivee;
	    this.getParams_().navires.listeNavires[indice].h.lundiCreneau2HeureDep = bateau.creneauxJournaliers_.lundiCreneau2HeureDep;
	    this.getParams_().navires.listeNavires[indice].h.lundiCreneau3HeureArrivee = bateau.creneauxJournaliers_.lundiCreneau3HeureArrivee;
	    this.getParams_().navires.listeNavires[indice].h.lundiCreneau3HeureDep = bateau.creneauxJournaliers_.lundiCreneau3HeureDep;

	    this.getParams_().navires.listeNavires[indice].h.mardiCreneau1HeureArrivee = bateau.creneauxJournaliers_.mardiCreneau1HeureArrivee;
	    this.getParams_().navires.listeNavires[indice].h.mardiCreneau1HeureDep = bateau.creneauxJournaliers_.mardiCreneau1HeureDep;
	    this.getParams_().navires.listeNavires[indice].h.mardiCreneau2HeureArrivee = bateau.creneauxJournaliers_.mardiCreneau2HeureArrivee;
	    this.getParams_().navires.listeNavires[indice].h.mardiCreneau2HeureDep = bateau.creneauxJournaliers_.mardiCreneau2HeureDep;
	    this.getParams_().navires.listeNavires[indice].h.mardiCreneau3HeureArrivee = bateau.creneauxJournaliers_.mardiCreneau3HeureArrivee;
	    this.getParams_().navires.listeNavires[indice].h.mardiCreneau3HeureDep = bateau.creneauxJournaliers_.mardiCreneau3HeureDep;

	    this.getParams_().navires.listeNavires[indice].h.mercrediCreneau1HeureArrivee = bateau.creneauxJournaliers_.mercrediCreneau1HeureArrivee;
	    this.getParams_().navires.listeNavires[indice].h.mercrediCreneau1HeureDep = bateau.creneauxJournaliers_.mercrediCreneau1HeureDep;
	    this.getParams_().navires.listeNavires[indice].h.mercrediCreneau2HeureArrivee = bateau.creneauxJournaliers_.mercrediCreneau2HeureArrivee;
	    this.getParams_().navires.listeNavires[indice].h.mercrediCreneau2HeureDep = bateau.creneauxJournaliers_.mercrediCreneau2HeureDep;
	    this.getParams_().navires.listeNavires[indice].h.mercrediCreneau3HeureArrivee = bateau.creneauxJournaliers_.mercrediCreneau3HeureArrivee;
	    this.getParams_().navires.listeNavires[indice].h.mercrediCreneau3HeureDep = bateau.creneauxJournaliers_.mercrediCreneau3HeureDep;

	    this.getParams_().navires.listeNavires[indice].h.jeudiCreneau1HeureArrivee = bateau.creneauxJournaliers_.jeudiCreneau1HeureArrivee;
	    this.getParams_().navires.listeNavires[indice].h.jeudiCreneau1HeureDep = bateau.creneauxJournaliers_.jeudiCreneau1HeureDep;
	    this.getParams_().navires.listeNavires[indice].h.jeudiCreneau2HeureArrivee = bateau.creneauxJournaliers_.jeudiCreneau2HeureArrivee;
	    this.getParams_().navires.listeNavires[indice].h.jeudiCreneau2HeureDep = bateau.creneauxJournaliers_.jeudiCreneau2HeureDep;
	    this.getParams_().navires.listeNavires[indice].h.jeudiCreneau3HeureArrivee = bateau.creneauxJournaliers_.jeudiCreneau3HeureArrivee;
	    this.getParams_().navires.listeNavires[indice].h.jeudiCreneau3HeureDep = bateau.creneauxJournaliers_.jeudiCreneau3HeureDep;

	    this.getParams_().navires.listeNavires[indice].h.vendrediCreneau1HeureArrivee = bateau.creneauxJournaliers_.vendrediCreneau1HeureArrivee;
	    this.getParams_().navires.listeNavires[indice].h.vendrediCreneau1HeureDep = bateau.creneauxJournaliers_.vendrediCreneau1HeureDep;
	    this.getParams_().navires.listeNavires[indice].h.vendrediCreneau2HeureArrivee = bateau.creneauxJournaliers_.vendrediCreneau2HeureArrivee;
	    this.getParams_().navires.listeNavires[indice].h.vendrediCreneau2HeureDep = bateau.creneauxJournaliers_.vendrediCreneau2HeureDep;
	    this.getParams_().navires.listeNavires[indice].h.vendrediCreneau3HeureArrivee = bateau.creneauxJournaliers_.vendrediCreneau3HeureArrivee;
	    this.getParams_().navires.listeNavires[indice].h.vendrediCreneau3HeureDep = bateau.creneauxJournaliers_.vendrediCreneau3HeureDep;

	    this.getParams_().navires.listeNavires[indice].h.samediCreneau1HeureArrivee = bateau.creneauxJournaliers_.samediCreneau1HeureArrivee;
	    this.getParams_().navires.listeNavires[indice].h.samediCreneau1HeureDep = bateau.creneauxJournaliers_.samediCreneau1HeureDep;
	    this.getParams_().navires.listeNavires[indice].h.samediCreneau2HeureArrivee = bateau.creneauxJournaliers_.samediCreneau2HeureArrivee;
	    this.getParams_().navires.listeNavires[indice].h.samediCreneau2HeureDep = bateau.creneauxJournaliers_.samediCreneau2HeureDep;
	    this.getParams_().navires.listeNavires[indice].h.samediCreneau3HeureArrivee = bateau.creneauxJournaliers_.samediCreneau3HeureArrivee;
	    this.getParams_().navires.listeNavires[indice].h.samediCreneau3HeureDep = bateau.creneauxJournaliers_.samediCreneau3HeureDep;

	    this.getParams_().navires.listeNavires[indice].h.dimancheCreneau1HeureArrivee = bateau.creneauxJournaliers_.dimancheCreneau1HeureArrivee;
	    this.getParams_().navires.listeNavires[indice].h.dimancheCreneau1HeureDep = bateau.creneauxJournaliers_.dimancheCreneau1HeureDep;
	    this.getParams_().navires.listeNavires[indice].h.dimancheCreneau2HeureArrivee = bateau.creneauxJournaliers_.dimancheCreneau2HeureArrivee;
	    this.getParams_().navires.listeNavires[indice].h.dimancheCreneau2HeureDep = bateau.creneauxJournaliers_.dimancheCreneau2HeureDep;
	    this.getParams_().navires.listeNavires[indice].h.dimancheCreneau3HeureArrivee = bateau.creneauxJournaliers_.dimancheCreneau3HeureArrivee;
	    this.getParams_().navires.listeNavires[indice].h.dimancheCreneau3HeureDep = bateau.creneauxJournaliers_.dimancheCreneau3HeureDep;

	    this.getParams_().navires.listeNavires[indice].h.ferieCreneau1HeureArrivee = bateau.creneauxJournaliers_.ferieCreneau1HeureArrivee;
	    this.getParams_().navires.listeNavires[indice].h.ferieCreneau1HeureDep = bateau.creneauxJournaliers_.ferieCreneau1HeureDep;
	    this.getParams_().navires.listeNavires[indice].h.ferieCreneau2HeureArrivee = bateau.creneauxJournaliers_.ferieCreneau2HeureArrivee;
	    this.getParams_().navires.listeNavires[indice].h.ferieCreneau2HeureDep = bateau.creneauxJournaliers_.ferieCreneau2HeureDep;
	    this.getParams_().navires.listeNavires[indice].h.ferieCreneau3HeureArrivee = bateau.creneauxJournaliers_.ferieCreneau3HeureArrivee;
	    this.getParams_().navires.listeNavires[indice].h.ferieCreneau3HeureDep = bateau.creneauxJournaliers_.ferieCreneau3HeureDep;
	  }
	

	private void setParametresNavires() {
		// a)recopiage du nombre de cheneaux et allocation de memoire
		this.params_.navires = new SParametresNavires2();
		this.params_.navires.nombreNavires = this.listeBateaux_.listeNavires_.size();
		this.params_.navires.listeNavires = new SParametresNavire2[this.listeBateaux_.listeNavires_.size()];
		// b)recopiage

		for (int i = 0; i < this.params_.navires.nombreNavires; i++) {
			final Sinavi3Bateau copie = this.listeBateaux_.retournerNavire(i);

			// allocation d'un navire:
			this.params_.navires.listeNavires[i] = new SParametresNavire2();

			this.params_.navires.listeNavires[i].nom = copie.nom;
			this.params_.navires.listeNavires[i].dureeAttenteMaxAdmissible = copie.dureeAttenteMaximaleAdmissible;
			
			this.params_.navires.listeNavires[i].priorite = copie.priorite;
			this.params_.navires.listeNavires[i].largeurMax = copie.largeurMax;
			this.params_.navires.listeNavires[i].vitesseMontant = copie.vitesseDefautMontant;
			this.params_.navires.listeNavires[i].longueurMax = copie.longueurMax;
			this.params_.navires.listeNavires[i].vitesseAvalant = copie.vitesseDefautAvalant;

			this.params_.navires.listeNavires[i].tirantEauEntree = copie.tirantEau;
			this.params_.navires.listeNavires[i].typeTirant = copie.typeTirant;

			setParametresHorairesBateau(i, copie);


			//---------------------- TRAJETS: recopiage des trajets ----------------------------------//



			//    allocation de memoire pour le tableau de loi deterministe correspondant a al taille du vecteur

			this.params_.navires.listeNavires[i].TableauloiJournaliere= new SParametresCoupleLoiJournaliere2[copie.listeTrajet_.size()][copie.listeTrajet_.maxLoisaisieTrajet()];
			this.params_.navires.listeNavires[i].TableauloiDeterministe= new SParametresCoupleLoiDeterministe2[copie.listeTrajet_.size()][copie.listeTrajet_.maxLoisaisieTrajet()];
			this.params_.navires.listeNavires[i].GareDepart=new int[copie.listeTrajet_.size()];
			this.params_.navires.listeNavires[i].GareArrivee=new int[copie.listeTrajet_.size()];
			this.params_.navires.listeNavires[i].erlangDebut=new double[copie.listeTrajet_.size()];
			this.params_.navires.listeNavires[i].erlangFin=new double[copie.listeTrajet_.size()];
			this.params_.navires.listeNavires[i].sens=new int[copie.listeTrajet_.size()];
			this.params_.navires.listeNavires[i].typeLoiGenerationNavires=new int[copie.listeTrajet_.size()];
			this.params_.navires.listeNavires[i].nbNaviresAttendus=new int[copie.listeTrajet_.size()];
			this.params_.navires.listeNavires[i].loiErlangGeneration=new int[copie.listeTrajet_.size()];
			this.params_.navires.listeNavires[i].loiErlangEcartType=new double[copie.listeTrajet_.size()];
			this.params_.navires.listeNavires[i].typeErlangLoi=new int[copie.listeTrajet_.size()];
			
			//remplissage poru tous les trajets
			for(int t=0;t<copie.listeTrajet_.size();t++)
			{
				//-- recuperation du trajet � recopier --//
				Sinavi3Trajet trajet=copie.listeTrajet_.retournerTrajet(t);

				this.params_.navires.listeNavires[i].GareDepart[t] = trajet.gareDepart_;
				this.params_.navires.listeNavires[i].erlangDebut[t] = trajet.erlangDebut;
				this.params_.navires.listeNavires[i].erlangFin[t] = trajet.erlangFin;
				this.params_.navires.listeNavires[i].GareArrivee[t] = trajet.gareArrivee_;
				this.params_.navires.listeNavires[i].sens[t] = trajet.sens_;

				// cas 0: loi d erlang
				this.params_.navires.listeNavires[i].typeLoiGenerationNavires[t] = trajet.typeLoiGenerationNavires_;
				this.params_.navires.listeNavires[i].nbNaviresAttendus[t] = trajet.nbBateauxattendus;
				this.params_.navires.listeNavires[i].loiErlangGeneration[t] = trajet.loiErlangGenerationNavire;
				this.params_.navires.listeNavires[i].loiErlangEcartType[t] = trajet.loiErlangEcartType;
				this.params_.navires.listeNavires[i].typeErlangLoi[t] = trajet.typeErlangLoi;




				//-- cas loi journaliere --//
				if (this.params_.navires.listeNavires[i].typeLoiGenerationNavires[t]== 2) {

					// recopiage des donn�es une par une
					ArrayList listeJournaliere= trajet.loiDeterministeOUjournaliere_;

					for (int k = 0; k < listeJournaliere.size(); k++) {
						this.params_.navires.listeNavires[i].TableauloiJournaliere[t][k] = new SParametresCoupleLoiJournaliere2();

						this.params_.navires.listeNavires[i].TableauloiJournaliere[t][k].horaire = ((CoupleLoiDeterministe)listeJournaliere.get(k)).temps_;

					}

				}
				else
					// cas 2: loi deterministe
					if (this.params_.navires.listeNavires[i].typeLoiGenerationNavires[t]== 1) {

						// recopiage des donn�es une par une
						ArrayList listeDeter= trajet.loiDeterministeOUjournaliere_;

						for (int k = 0; k < listeDeter.size(); k++) {
							this.params_.navires.listeNavires[i].TableauloiDeterministe[t][k] = new SParametresCoupleLoiDeterministe2();

							this.params_.navires.listeNavires[i].TableauloiDeterministe[t][k].horaire = ((CoupleLoiDeterministe)listeDeter.get(k)).temps_;
							this.params_.navires.listeNavires[i].TableauloiDeterministe[t][k].jour = ((CoupleLoiDeterministe)listeDeter.get(k)).jour_;

						}

					}



			}



		}
	}





	private void setParametresChenaux() {
		// a)recopiage du nombre de cheneaux et allocation de memoire
		this.params_.cheneaux = new SParametresCheneaux2();
		this.params_.cheneaux.nbCheneaux = this.listeBief_.listeBiefs_.size();
		this.params_.cheneaux.listeCheneaux = new SParametresChenal2[this.listeBief_.listeBiefs_.size()];
		// b)recopiage
		for (int i = 0; i < this.listeBief_.listeBiefs_.size(); i++) {
			// allocation memoire nouveau chenal
			this.params_.cheneaux.listeCheneaux[i] = new SParametresChenal2();
			// recuperation de l'�l�ment � copier
			final Sinavi3Bief copie = this.listeBief_.retournerBief(i);

			this.params_.cheneaux.listeCheneaux[i].nom = copie.nom_;
			// this.params_.cheneaux.listeCheneaux[i].longueur=copie.longueur_;
			this.params_.cheneaux.listeCheneaux[i].profondeur = copie.profondeur_;
			this.params_.cheneaux.listeCheneaux[i].longueur = copie.longueur_;
			this.params_.cheneaux.listeCheneaux[i].largeur = copie.largeur_;
			this.params_.cheneaux.listeCheneaux[i].vitesse = copie.vitesse_;

			// this.params_.cheneaux.listeCheneaux[i].vitesse=copie.vitesse_;
			this.params_.cheneaux.listeCheneaux[i].gareAmont = copie.gareAmont_;
			this.params_.cheneaux.listeCheneaux[i].gareAval = copie.gareAval_;


			this.params_.cheneaux.listeCheneaux[i].dureeIndispo = copie.dureeIndispo_;
			this.params_.cheneaux.listeCheneaux[i].loiIndispo = copie.loiIndispo_;
			// Les donn�es des lois de frequence a recopier
			// le type de loi
			this.params_.cheneaux.listeCheneaux[i].typeLoi = copie.typeLoi_;

			// cas 0: loi d erlang
			this.params_.cheneaux.listeCheneaux[i].loiFrequence = copie.loiFrequence_;
			this.params_.cheneaux.listeCheneaux[i].frequenceMoyenne = copie.frequenceMoyenne_;

			if (copie.typeLoi_ == 2) {
				// allocation de memoire pour le tableau de loi deterministe correspondant a al taille du vecteur
				this.params_.cheneaux.listeCheneaux[i].TableauloiJournaliere = new SParametresCoupleLoiJournaliere2[copie.loiDeterministe_
				                                                                                                    .size()];
				// recopiage du nombre de donn�es de la loi deterministe
				this.params_.cheneaux.listeCheneaux[i].nombreHorairesJournaliers = copie.loiDeterministe_.size();

				// recopiage des donn�es une par une

				for (int k = 0; k < copie.loiDeterministe_.size(); k++) {
					this.params_.cheneaux.listeCheneaux[i].TableauloiJournaliere[k] = new SParametresCoupleLoiJournaliere2();

					this.params_.cheneaux.listeCheneaux[i].TableauloiJournaliere[k].horaire = ((CoupleLoiDeterministe) copie.loiDeterministe_
							.get(k)).temps_;

				}

			}
			// cas 2: loi deterministe

			if (copie.typeLoi_ == 1) {
				// allocation de memoire pour le tableau de loi deterministe correspondant a al taille du vecteur
				this.params_.cheneaux.listeCheneaux[i].TableauloiDeterministe = new SParametresCoupleLoiDeterministe2[copie.loiDeterministe_
				                                                                                                      .size()];
				// recopiage du nombre de donn�es de al loi deterministe
				this.params_.cheneaux.listeCheneaux[i].nombreCouplesDeterministes = copie.loiDeterministe_.size();

				// recopiage des donn�es une par une
				for (int k = 0; k < copie.loiDeterministe_.size(); k++) {
					this.params_.cheneaux.listeCheneaux[i].TableauloiDeterministe[k] = new SParametresCoupleLoiDeterministe2();

					this.params_.cheneaux.listeCheneaux[i].TableauloiDeterministe[k].horaire = ((CoupleLoiDeterministe) copie.loiDeterministe_
							.get(k)).temps_;
					this.params_.cheneaux.listeCheneaux[i].TableauloiDeterministe[k].jour = ((CoupleLoiDeterministe) copie.loiDeterministe_
							.get(k)).jour_;

				}
			}





			// recopiage des Horaires
			this.params_.cheneaux.listeCheneaux[i].h = new SParametresHoraires2();

			this.params_.cheneaux.listeCheneaux[i].h.semaineCreneau1HeureArrivee = copie.h_.semaineCreneau1HeureArrivee;
			this.params_.cheneaux.listeCheneaux[i].h.semaineCreneau1HeureDep = copie.h_.semaineCreneau1HeureDep;
			this.params_.cheneaux.listeCheneaux[i].h.semaineCreneau2HeureArrivee = copie.h_.semaineCreneau2HeureArrivee;
			this.params_.cheneaux.listeCheneaux[i].h.semaineCreneau2HeureDep = copie.h_.semaineCreneau2HeureDep;
			this.params_.cheneaux.listeCheneaux[i].h.semaineCreneau3HeureArrivee = copie.h_.semaineCreneau3HeureArrivee;
			this.params_.cheneaux.listeCheneaux[i].h.semaineCreneau3HeureDep = copie.h_.semaineCreneau3HeureDep;

			// les regles de navigations
			// recopiage des regels de navigations:
			// 1)dimension de la matrice
			this.params_.cheneaux.listeCheneaux[i].dimensionMatriceCroisement = copie.reglesCroisement_.vecteurDeVecteur_
			.size();
			// 2)allocation de memoire
			this.params_.cheneaux.listeCheneaux[i].matriceCroisement = new boolean[copie.reglesCroisement_.vecteurDeVecteur_
			                                                                       .size()][copie.reglesCroisement_.vecteurDeVecteur_.size()];
			this.params_.cheneaux.listeCheneaux[i].matriceTrematage = new boolean[copie.reglesTrematage_.vecteurDeVecteur_
			                                                                      .size()][copie.reglesTrematage_.vecteurDeVecteur_.size()];
			// 3)recopiage de la matrice
			for (int k = 0; k < this.params_.cheneaux.listeCheneaux[i].dimensionMatriceCroisement; k++) {
				for (int l = 0; l < this.params_.cheneaux.listeCheneaux[i].dimensionMatriceCroisement; l++) {
					this.params_.cheneaux.listeCheneaux[i].matriceCroisement[k][l] = copie.reglesCroisement_.retournerAij(
							k, l).booleanValue();
					this.params_.cheneaux.listeCheneaux[i].matriceTrematage[k][l] = copie.reglesTrematage_.retournerAij(
							k, l).booleanValue();
				}
			}

		}
	}

	private void setParametresEcluses() {
		this.params_.ecluses = new SParametresEcluses2();
		this.params_.ecluses.nbEcluses = this.listeEcluse_.listeEcluses_.size();
		this.params_.ecluses.listeEcluses = new SParametresEcluse2[this.listeEcluse_.listeEcluses_.size()];
		// b)recopiage
		for (int i = 0; i < this.listeEcluse_.listeEcluses_.size(); i++) {
			// allocation memoire nouvelel ecluse
			this.params_.ecluses.listeEcluses[i] = new SParametresEcluse2();
			// recuperation de l'�l�ment � copier
			final Sinavi3Ecluse copie = this.listeEcluse_.retournerEcluse(i);

			this.params_.ecluses.listeEcluses[i].nom = copie.nom_;
			this.params_.ecluses.listeEcluses[i].largeur = copie.largeur_;
			this.params_.ecluses.listeEcluses[i].longueur = copie.longueur_;
			this.params_.ecluses.listeEcluses[i].hauteurChute = copie.hauteurchute;
			this.params_.ecluses.listeEcluses[i].dureeDefManoeuvreEntrant = copie.dureeManoeuvreEntrant_;
			this.params_.ecluses.listeEcluses[i].dureeDefManoeuvreSortant = copie.dureeManoeuvreSortant_;
			
			this.params_.ecluses.listeEcluses[i].dureeDefManoeuvreEntrant2 = copie.dureeManoeuvreEntrant2_;
			this.params_.ecluses.listeEcluses[i].dureeDefManoeuvreSortant2 = copie.dureeManoeuvreSortant2_;
			
			this.params_.ecluses.listeEcluses[i].gareAmont = copie.gareAmont_;
			this.params_.ecluses.listeEcluses[i].gareAval = copie.gareAval_;
			this.params_.ecluses.listeEcluses[i].profondeur = copie.profondeur_;
			this.params_.ecluses.listeEcluses[i].tempsFausseBassineeMontant = copie.tempsFausseBassinneeMontant_;
			this.params_.ecluses.listeEcluses[i].tempsFausseBassineeAvalant = copie.tempsFausseBassinneeAvalant_;

			this.params_.ecluses.listeEcluses[i].dureeIndispo = copie.dureeIndispo_;
			this.params_.ecluses.listeEcluses[i].coefficientBassinEpargne = copie.coefficientBassinEpargne_;
			this.params_.ecluses.listeEcluses[i].loiIndispo = copie.loiIndispo_;

			// Les donn�es des lois de frequence a recopier

			// le type de loi
			this.params_.ecluses.listeEcluses[i].typeLoi = copie.typeLoi_;

			// cas 0: loi d erlang
			this.params_.ecluses.listeEcluses[i].loiFrequence = copie.loiFrequence_;
			this.params_.ecluses.listeEcluses[i].frequenceMoyenne = copie.frequenceMoyenne_;

			if (copie.typeLoi_ == 2) {
				// allocation de memoire pour le tableau de loi deterministe correspondant a al taille du vecteur
				this.params_.ecluses.listeEcluses[i].TableauloiJournaliere = new SParametresCoupleLoiJournaliere2[copie.loiDeterministe_
				                                                                                                  .size()];
				// recopiage du nombre de donn�es de la loi deterministe
				this.params_.ecluses.listeEcluses[i].nombreHorairesJournaliers = copie.loiDeterministe_.size();

				// recopiage des donn�es une par une

				for (int k = 0; k < copie.loiDeterministe_.size(); k++) {
					this.params_.ecluses.listeEcluses[i].TableauloiJournaliere[k] = new SParametresCoupleLoiJournaliere2();

					this.params_.ecluses.listeEcluses[i].TableauloiJournaliere[k].horaire = ((CoupleLoiDeterministe) copie.loiDeterministe_
							.get(k)).temps_;

				}

			}
			// cas 2: loi deterministe

			if (copie.typeLoi_ == 1) {
				// allocation de memoire pour le tableau de loi deterministe correspondant a al taille du vecteur
				this.params_.ecluses.listeEcluses[i].TableauloiDeterministe = new SParametresCoupleLoiDeterministe2[copie.loiDeterministe_
				                                                                                                    .size()];
				// recopiage du nombre de donn�es de al loi deterministe
				this.params_.ecluses.listeEcluses[i].nombreCouplesDeterministes = copie.loiDeterministe_.size();

				// recopiage des donn�es une par une
				for (int k = 0; k < copie.loiDeterministe_.size(); k++) {
					this.params_.ecluses.listeEcluses[i].TableauloiDeterministe[k] = new SParametresCoupleLoiDeterministe2();

					this.params_.ecluses.listeEcluses[i].TableauloiDeterministe[k].horaire = ((CoupleLoiDeterministe) copie.loiDeterministe_
							.get(k)).temps_;
					this.params_.ecluses.listeEcluses[i].TableauloiDeterministe[k].jour = ((CoupleLoiDeterministe) copie.loiDeterministe_
							.get(k)).jour_;

				}
			}

			this.params_.ecluses.listeEcluses[i].h = new SParametresHoraires2();
			// recopiage des Horaires
			/*
			 * this.params_.ecluses.listeEcluses[i].h.dimancheCreneau1HeureArrivee=copie.h_.dimancheCreneau1HeureArrivee;
			 * this.params_.ecluses.listeEcluses[i].h.dimancheCreneau1HeureDep=copie.h_.dimancheCreneau1HeureDep;
			 * this.params_.ecluses.listeEcluses[i].h.dimancheCreneau2HeureArrivee=copie.h_.dimancheCreneau2HeureArrivee;
			 * this.params_.ecluses.listeEcluses[i].h.dimancheCreneau2HeureDep=copie.h_.dimancheCreneau2HeureDep;
			 * this.params_.ecluses.listeEcluses[i].h.dimancheCreneau3HeureArrivee=copie.h_.dimancheCreneau3HeureArrivee;
			 * this.params_.ecluses.listeEcluses[i].h.dimancheCreneau3HeureDep=copie.h_.dimancheCreneau3HeureDep;
			 * this.params_.ecluses.listeEcluses[i].h.ferieCreneau1HeureArrivee=copie.h_.ferieCreneau1HeureArrivee;
			 * this.params_.ecluses.listeEcluses[i].h.ferieCreneau1HeureDep=copie.h_.ferieCreneau1HeureDep;
			 * this.params_.ecluses.listeEcluses[i].h.ferieCreneau2HeureArrivee=copie.h_.ferieCreneau2HeureArrivee;
			 * this.params_.ecluses.listeEcluses[i].h.ferieCreneau2HeureDep=copie.h_.ferieCreneau2HeureDep;
			 * this.params_.ecluses.listeEcluses[i].h.ferieCreneau3HeureArrivee=copie.h_.ferieCreneau3HeureArrivee;
			 * this.params_.ecluses.listeEcluses[i].h.ferieCreneau3HeureDep=copie.h_.ferieCreneau3HeureDep;
			 * this.params_.ecluses.listeEcluses[i].h.samediCreneau1HeureArrivee=copie.h_.samediCreneau1HeureArrivee;
			 * this.params_.ecluses.listeEcluses[i].h.samediCreneau1HeureDep=copie.h_.samediCreneau1HeureDep;
			 * this.params_.ecluses.listeEcluses[i].h.samediCreneau2HeureArrivee=copie.h_.samediCreneau2HeureArrivee;
			 * this.params_.ecluses.listeEcluses[i].h.samediCreneau2HeureDep=copie.h_.samediCreneau2HeureDep;
			 * this.params_.ecluses.listeEcluses[i].h.samediCreneau3HeureArrivee=copie.h_.samediCreneau3HeureArrivee;
			 * this.params_.ecluses.listeEcluses[i].h.samediCreneau3HeureDep=copie.h_.samediCreneau3HeureDep;
			 */
			this.params_.ecluses.listeEcluses[i].h.semaineCreneau1HeureArrivee = copie.h_.semaineCreneau1HeureArrivee;
			this.params_.ecluses.listeEcluses[i].h.semaineCreneau1HeureDep = copie.h_.semaineCreneau1HeureDep;
			this.params_.ecluses.listeEcluses[i].h.semaineCreneau2HeureArrivee = copie.h_.semaineCreneau2HeureArrivee;
			this.params_.ecluses.listeEcluses[i].h.semaineCreneau2HeureDep = copie.h_.semaineCreneau2HeureDep;
			this.params_.ecluses.listeEcluses[i].h.semaineCreneau3HeureArrivee = copie.h_.semaineCreneau3HeureArrivee;
			this.params_.ecluses.listeEcluses[i].h.semaineCreneau3HeureDep = copie.h_.semaineCreneau3HeureDep;

		}
	}


	private void setParametresGares() {
		this.params_.gares = new SParametresGares();
		this.params_.gares.nbGares = this.listeGare_.listeGares_.size();
		this.params_.gares.listeGares = new SParametresGare[this.listeGare_.listeGares_.size()];
		// b) recopiage
		for (int i = 0; i < this.listeGare_.listeGares_.size(); i++) {
			this.params_.gares.listeGares[i] = new SParametresGare(this.listeGare_.retournerGare(i));
		}
	}

	void activerCommandesNiveau() {

		// par defaut toujours activer la saisies des don�nes generales
		// cette saisie donne lieu au niveau suivant
		this.application_.setEnabledForAction("DONNEESGENERALES", true);

		/**
		 * Activation de panel saisies topologie regles de navigation
		 */
		this.application_.activerCommandesSimulation();

	}

	/**
	 * Methode qui verifie toutes les donn�es avant lancement de la simulation.
	 * 
	 * @return true les �l�ments sont coh�rents, saisie completement et aucune exception relevee
	 */
	boolean verificationDonnees() {
		if (frameVerif_ != null) {
			if (!frameVerif_.isClosed()) {
				return false;
			}
		}

		/**
		 * creation de la frame d'afficahge des verifications
		 */
		final JLabel[] tableauVerif = new JLabel[800];
		final JPanel panelMessage = new JPanel();
		final JScrollPane asc = new JScrollPane(panelMessage);
		final JPanel controlPanel = new JPanel();
		final BuButton validation = new BuButton(FudaaResource.FUDAA.getIcon("crystal_oui"), "Valider");
		final BuButton quitter = new BuButton(FudaaResource.FUDAA.getIcon("crystal_quitter"), "Quitter");

		/* final Sinavi3InternalFrame */frameVerif_ = new Sinavi3InternalFrame("", true, true, true, true);

		quitter.addActionListener(new ActionListener() {
			public void actionPerformed(final ActionEvent _e) {
				frameVerif_.dispose();
				frameVerif_ = null;

			}
		});
		validation.addActionListener(new ActionListener() {
			public void actionPerformed(final ActionEvent _e) {
				frameVerif_.dispose();
				frameVerif_ = null;
			}

		});

		panelMessage.setLayout(new GridLayout(800, 1));
		for (int i = 0; i < 800; i++) {
			tableauVerif[i] = new JLabel("");
			tableauVerif[i].setBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED));

			panelMessage.add(tableauVerif[i]);

		}
		controlPanel.add(quitter);
		controlPanel.add(validation);

		frameVerif_.getContentPane().setLayout(new BorderLayout());
		frameVerif_.getContentPane().add(asc, BorderLayout.CENTER);
		frameVerif_.getContentPane().add(controlPanel, BorderLayout.SOUTH);
		// bouton ne devient accessible que si la totalit� des test ont �t� r�alis�� avec succ�s
		validation.setEnabled(false);
		frameVerif_.setTitle("V�rification des donn�es de simulation");
		frameVerif_.setSize(650, 500);

		frameVerif_.setVisible(true);
		this.application_.addInternalFrame(frameVerif_);
		/** fin mise en page de la frame* */

		// test 1: on verifie qu il existe un nombre minimum de donnees avant lancement de la simulation:
		tableauVerif[0].setForeground(Color.BLUE);
		String font = getDefaultFontName();
		if (this.listeGare_.listeGares_.size() >= 2) {
			tableauVerif[0].setText("Test 1... REUSSITE: nombre de gares sup�rieur � 2");
		} else {
			tableauVerif[0].setFont(new Font(font, Font.ITALIC, 13));
			tableauVerif[0].setForeground(Color.RED);
			tableauVerif[0].setText("Test 1... ECHEC: nombre de gares inf�rieur � 2");
			new BuDialogError(this.application_.getApp(), this.application_.getInformationsSoftware(),
			"Erreur! Il faut au moins 2 gares.").activate();
			return false;
		}


		tableauVerif[1].setForeground(Color.BLUE);
		if (this.listeBief_.listeBiefs_.size() >= 1) {
			tableauVerif[1].setText("Test 2... REUSSITE: au moins un tron�on a �t� d�fini");
		} else {
			tableauVerif[1].setFont(new Font(font, Font.ITALIC, 13));
			tableauVerif[1].setForeground(Color.RED);
			tableauVerif[1].setText("Test 2... ECHEC: aucun tron�on n'a �t� d�fini");
			new BuDialogError(this.application_.getApp(), this.application_.getInformationsSoftware(),
			"Erreur! Il faut au moins un tron�on.").activate();
			return false;
		}
		tableauVerif[2].setForeground(Color.BLUE);
		if (this.listeEcluse_.listeEcluses_.size() >= 1) {
			tableauVerif[2].setText("Test 3... REUSSITE: au moins une �cluse a �t� d�finie");
		} else {
			tableauVerif[2].setFont(new Font(font, Font.ITALIC, 13));
			tableauVerif[2].setForeground(Color.RED);
			tableauVerif[2].setText("Test 3... AVERTISSEMENT: aucune �cluse n'a �t� d�finie");


		}


		tableauVerif[3].setForeground(Color.BLUE);
		if (this.listeBateaux_.listeNavires_.size() >= 1) {
			tableauVerif[3].setText("Test 4... REUSSITE: au moins une cat�gorie de bateaux a �t� d�finie");
		} else {
			tableauVerif[3].setFont(new Font(font, Font.ITALIC, 13));
			tableauVerif[3].setForeground(Color.RED);
			tableauVerif[3].setText("Test 4... ECHEC: aucune cat�gorie de bateaux n'a �t� d�finie");
			new BuDialogError(this.application_.getApp(), this.application_.getInformationsSoftware(),
			"Erreur! Il faut au moins une categorie de bateau").activate();
			return false;
		}
		tableauVerif[4].setForeground(Color.BLUE);
		int h=listeBateaux_.existeTrajetPourChaqueNavires();
		if(h!=-1){
			new BuDialogError(this.application_.getApp(),application_.isSinavi_,"Aucun trajet n'a �t� d�fini pour la cat�gorie\n" +listeBateaux_.retournerNavire(h).nom+
			".\nUn trajet peut �tre d�fini via le menu \"Navigation\"").activate();
			tableauVerif[4].setFont(new Font(font, Font.ITALIC, 13));
			tableauVerif[4].setForeground(Color.RED);
			tableauVerif[4].setText("Test 5... AVERTISSEMENT: aucun trajet n'a �t� d�fini pour certaines cat�gories de bateaux");
			//return false;
		}
		else
			tableauVerif[4].setText("Test 5... REUSSITE: au moins un trajet par cat�gorie de bateaux a �t� d�fini");
		int compteur = 5;
		// test 2: verification des gares orphelines
		tableauVerif[compteur].setForeground(Color.BLACK);
		tableauVerif[compteur++].setText("Test 6: Signalement des gares orphelines...");
		tableauVerif[compteur].setForeground(Color.BLUE);
		compteur = suppressionGaresOrphelines(tableauVerif, compteur);

		// test 3: verification de la saisie dees regles de parcours: chanaux
		tableauVerif[compteur].setForeground(Color.BLUE);
		if (!verifVitesseAvalantChenal()) {
			tableauVerif[compteur].setFont(new Font(font, Font.ITALIC, 13));
			tableauVerif[compteur].setForeground(Color.RED);
			tableauVerif[compteur++].setText("Test 7... AVERTISSEMENT: Certaines vitesses avalantes dans les tron�ons sont nulles");
			new BuDialogError(this.application_.getApp(), this.application_.getInformationsSoftware(),
					"Attention! Certaines vitesses avalantes dans les tron�ons sont nulles." + "\nElles peuvent �tre red�finies via le menu \"Navigation\"."
					).activate();

		} else {
			tableauVerif[compteur++].setText("Test 7... REUSSITE: Les vitesses avalantes dans les tron�ons sont valides");
		}

		tableauVerif[compteur].setForeground(Color.BLUE);
		if (!verifVitesseMontantChenal()) {
			tableauVerif[compteur].setFont(new Font(font, Font.ITALIC, 13));
			tableauVerif[compteur].setForeground(Color.RED);
			tableauVerif[compteur++].setText("Test 8... AVERTISSEMENT: Certaines vitesses montantes dans les tron�ons sont nulles");
			new BuDialogError(this.application_.getApp(), this.application_.getInformationsSoftware(),
					"Attention! Certaines vitesses avalantes dans les tron�ons sont nulles." + "\nElles peuvent �tre red�finies via le menu \"Navigation\"."
					).activate();

		} else {
			tableauVerif[compteur++].setText("Test 8... REUSSITE: Les vitesses montantes dans les tron�ons sont valides");
		}

		tableauVerif[compteur].setForeground(Color.BLUE);


		// test 5: passage navires ecluses

		tableauVerif[compteur].setForeground(Color.BLACK);
		tableauVerif[compteur++].setText("Test 9: Signalement des cat�gories de bateaux incompatibles avec les dimensions des �cluses...");
		tableauVerif[compteur].setForeground(Color.BLUE);
		int anciencpt = compteur;
		compteur = verifPassageEcluses(tableauVerif, compteur);
		if (anciencpt != compteur) {
			new BuDialogWarning(this.application_.getApp(), this.application_.getInformationsSoftware(), "Attention ! Certaines cat�gories de bateaux sont incompatibles avec les dimensions de certaines �cluses").activate();
		} else {
			tableauVerif[compteur++].setText("Test 9... REUSSITE: toutes les cat�gories de bateaux sont compatibles avec les dimensions des �cluses");
		}

		tableauVerif[compteur].setForeground(Color.BLACK);
		tableauVerif[compteur++]
		             .setText("Test 10: Signalement des tirants d'eau des bateaux incompatibles avec la profondeur des tron�ons...");
		tableauVerif[compteur].setForeground(Color.BLUE);
		int previousCpt = compteur;
		compteur = verifPassageChenaux(tableauVerif, compteur);

		if (compteur != previousCpt) {

		} else {
			tableauVerif[compteur++]
			             .setText("Test 10... REUSSITE: tous les tirants d'eau des bateaux sont compatibles avec la profondeur des tron�ons");
		}




		File fichier=new File(this.projet_.getFichier()+".arriv");
		if(!fichier.exists()){
			new BuDialogError(null,Sinavi3Implementation.isSinavi_,"Le fichier de g�n�ration est introuvable.\nLa g�n�ration des bateaux peut �tre relanc�e via le menu \"G�n�ration\".").activate();
			tableauVerif[compteur].setForeground(Color.RED);
			tableauVerif[compteur].setFont(new Font(font, Font.ITALIC, 13));
			tableauVerif[compteur++].setText("Test 11... ECHEC: Le fichier de g�n�ration des bateaux est introuvable");

			return false;
		}
		tableauVerif[compteur].setForeground(Color.BLUE);
		tableauVerif[compteur++].setText("Test 11... REUSSITE: Le fichier de g�n�ration des bateaux existe");







		new BuDialogMessage(this.application_.getApp(), this.application_.getInformationsSoftware(),
		"Les donn�es ont �t� v�rifi�es.\nLa simulation peut maintenant �tre d�marr�e.").activate();
		// tous les test ont ete effectuees, le lancement de la simulation peut avoir lieu
		// bouton de validation devient accessible: ce bouton declenche l'onglet simulation
		validation.setEnabled(true);
		return true;
	}

	private String getDefaultFontName() {
		return "Helvetica";
	}

	int verifPassageChenaux(final JLabel[] _t, int _cpt1) {
		int cpt = _cpt1;
		boolean probleme = false;
		// boolean validiteMethode = true;
		for (int k = 0; k < this.listeBief_.listeBiefs_.size(); k++) {
			probleme = false;
			String descriptifProb = " WARNING: tron�on " + this.listeBief_.retournerBief(k).nom_ + " profondeur de "
			+ (float) this.listeBief_.retournerBief(k).profondeur_;
			for (int l = 0; l < this.listeBateaux_.listeNavires_.size(); l++) {
				if (this.listeBateaux_.retournerNavire(l).tirantEau > this.listeBief_.retournerBief(k).profondeur_) {
					// validiteMethode = false;
					probleme = true;
					descriptifProb = descriptifProb + getDescCategorie() + this.listeBateaux_.retournerNavire(l).nom
					+ " ne passe pas(tirant eau entree=" + (float) this.listeBateaux_.retournerNavire(l).tirantEau
					+ " M)";
					_t[cpt].setForeground(Color.RED);
					_t[cpt].setFont(new Font(getDefaultFontName(), Font.ITALIC, 13));
					_t[cpt++]
					   .setText(getStringCategorie() + this.listeBateaux_.retournerNavire(l).nom
							   + " a un tirant d'eau incompatible avec les dimensions du bief "
							   + this.listeBief_.retournerBief(k).nom_);

				}

			}
			if (probleme) {
				new BuDialogError(this.application_.getApp(), this.application_.getInformationsSoftware(), descriptifProb)
				.activate();
			}

		}

		return cpt;
	}

	private String getStringCategorie() {
		return "la cat�gorie ";
	}

	private String getDescCategorie() {
		return "\n- la cat�gorie ";
	}



	int verifPassageEcluses(final JLabel[] _t, int _cpt1) {
		int cpt = _cpt1;
		int nombreProblemes;
		String categoriesProblemes;
		String descriptifProblemes;

		for (int k = 0; k < this.listeEcluse_.listeEcluses_.size(); k++) {
			
			nombreProblemes = 0;
			categoriesProblemes = "";
			
			for (int l = 0; l < this.listeBateaux_.listeNavires_.size(); l++) {
				
				if ( (this.listeBateaux_.retournerNavire(l).largeurMax > this.listeEcluse_.retournerEcluse(k).largeur_) || (this.listeBateaux_.retournerNavire(l).longueurMax >= this.listeEcluse_.retournerEcluse(k).longueur_) ) {
					nombreProblemes ++;
					if (nombreProblemes > 1) categoriesProblemes += ", ";
					categoriesProblemes += this.listeBateaux_.retournerNavire(l).nom;
				}

			}
			
			if (nombreProblemes > 0) {
				
				descriptifProblemes = "Les dimensions de l'�cluse " + this.listeEcluse_.retournerEcluse(k).nom_ + " sont incompatibles avec";
				if (nombreProblemes > 1) descriptifProblemes += " les cat�gories de bateaux ";
				else descriptifProblemes += " la cat�gorie de bateaux ";
				descriptifProblemes += categoriesProblemes;
				
				_t[cpt].setForeground(Color.RED);
				_t[cpt].setFont(new Font(getDefaultFontName(), Font.ITALIC, 13));
				_t[cpt++].setText(descriptifProblemes);
				
			}
		}

		return cpt;
	}

	/**
	 * Methode de verification des gares orphelines: cette m�thode v�rifie les gares qui n'ont pas de navires ayant poru
	 * gare de d�part et qui ne sont impliqu�e dans aucune topologies...
	 */
	int suppressionGaresOrphelines(final JLabel[] _t, int _cpt) {
		int cpt = _cpt;
		for (int i = 0; i < this.listeGare_.listeGares_.size(); i++) {
			// etape 1: on part du principe que la gare est orpheline
			boolean orpheline = true;

			// etape 2: verification d'existance dans les categories de navires
			for (int k = 0; k < this.listeBateaux_.listeNavires_.size() && orpheline; k++){ 

				for(int r=0;r<this.listeBateaux_.retournerNavire(k).listeTrajet_.size();r++){
					if (this.listeBateaux_.retournerNavire(k).listeTrajet_.retournerTrajet(r).gareDepart_ == i) {
						orpheline = false;
					}
				}
				for(int r=0;r<this.listeBateaux_.retournerNavire(k).listeTrajet_.size();r++){
					if (this.listeBateaux_.retournerNavire(k).listeTrajet_.retournerTrajet(r).gareArrivee_ == i) {
						orpheline = false;
					}
				}
			}

			// etape 3: verification d'existence dans les cheneaux
			for (int k = 0; k < this.listeBief_.listeBiefs_.size() && orpheline; k++) {
				if (this.listeBief_.retournerBief(k).gareAmont_ == i) {
					orpheline = false;
				} else if (this.listeBief_.retournerBief(k).gareAval_ == i) {
					orpheline = false;
				}
			}

			// etape 4: verification d'existence des ecluses
			for (int k = 0; k < this.listeEcluse_.listeEcluses_.size() && orpheline; k++) {
				if (this.listeEcluse_.retournerEcluse(k).gareAmont_ == i) {
					orpheline = false;
				} else if (this.listeEcluse_.retournerEcluse(k).gareAval_ == i) {
					orpheline = false;
				}
			}





			// testons si il la gare est orpheline: demande a l utilisateur de la supprimer:
			if (orpheline) {
				_t[cpt].setForeground(Color.RED);
				_t[cpt].setFont(new Font(getDefaultFontName(), Font.ITALIC, 13));
				_t[cpt++].setText("la gare " + this.listeGare_.retournerGare(i) + " est une gare orpheline");
				String nomgare=this.listeGare_.retournerGare(i);
				final int reponse = new BuDialogConfirmation(this.application_.getApp(), this.application_
						.getInformationsSoftware(), "Voulez vous supprimer la gare orpheline " + nomgare
						+ "?").activate();
				if (reponse == 0) {
					// 1)on supprime la gare de num�ro i
					this.listeGare_.suppression(i);
					// 2)correction des topologies des biefs et �cluses
	        		for (int k = 0; k < this.listeBief_.listeBiefs_.size(); k++) {
	            		if (this.listeBief_.retournerBief(k).gareAmont_ > i) this.listeBief_.retournerBief(k).gareAmont_ --;
	            		if (this.listeBief_.retournerBief(k).gareAval_ > i) this.listeBief_.retournerBief(k).gareAval_ --;
	            	}
	            	for (int k = 0; k < this.listeEcluse_.listeEcluses_.size(); k++) {
	            		if (this.listeEcluse_.retournerEcluse(k).gareAmont_ > i) this.listeEcluse_.retournerEcluse(k).gareAmont_--;
	            		if (this.listeEcluse_.retournerEcluse(k).gareAval_ > i) this.listeEcluse_.retournerEcluse(k).gareAval_--;
	            	}
					new BuDialogMessage(this.application_.getApp(), this.application_.getInformationsSoftware(),
							"La gare orpheline " + nomgare + " a �t� supprim�e avec succ�s.").activate();
					i--;
				}

			}

		}// fin du pour

		return cpt;

	}

	/**
	 * Methode de verification de la coh�rence des don�es saisies poru les dur�es de parcours.
	 */
	boolean verifVitesseAvalantChenal() {
		for (int i = 0; i < this.listeBief_.listeBiefs_.size(); i++) {
			// final ArrayList vecteurLigne = (ArrayList) this.reglesDureesParcoursChenal_.matriceDuree.get(i);
			for (int j = 0; j < this.listeBateaux_.listeNavires_.size(); j++) {
				// double val=((Double)vecteurLigne.get(j)).doubleValue();
				// System.out.println("taille vecteur ligne "+j+": "+vecteurLigne.size());
				// System.out.println("val("+i+","+j+")= "+val);
				if ((float) this.reglesVitesseBiefAvalant_.retournerValeur(i, j) == (float) 0.0) {
					return false;
				}
			}

		}

		// arriv� a ce stade il n y a plsu de doute possible; les donn�es sont bioen correctes et nnon vides
		return true;

	}

	boolean verifVitesseMontantChenal() {
		for (int i = 0; i < this.listeBief_.listeBiefs_.size(); i++) {
			// final ArrayList vecteurLigne = (ArrayList) this.reglesDureesParcoursChenal_.matriceDuree.get(i);
			for (int j = 0; j < this.listeBateaux_.listeNavires_.size(); j++) {
				// double val=((Double)vecteurLigne.get(j)).doubleValue();
				// System.out.println("taille vecteur ligne "+j+": "+vecteurLigne.size());
				// System.out.println("val("+i+","+j+")= "+val);
				if ((float) this.reglesVitesseBiefMontant_.retournerValeur(i, j) == (float) 0.0) {
					return false;
				}
			}

		}

		// arriv� a ce stade il n y a plsu de doute possible; les donn�es sont bioen correctes et nnon vides
		return true;

	}




	/**
	 * Methode qui place le niveau de s�curite de l'application au plus bas.
	 * Consequence: on doit redemarrer le test de v�rification des coh�rences des donn�es.
	 * Utiliser: on utilise cette m�thode dans le cas ou l'on modifie des donn�es.
	 *
	 */
	public void baisserNiveauSecurite(){

		//-- lors de la modification de donn�es, on remet le niveau de s�curit� de l'appli au minimum --//
		this.application_.baisserNiveauSecurite();

	}

	/**
	 * Idem que precedemment avec nuance.
	 * Methode appliqu�e dans le cas de suppression d'�l�ment ou modification de la topologie.
	 * Cons�quence suppl�mentaire: reinitialisation du dessin du port
	 *
	 */
	public void baisserNiveauSecurite2(){

		//-- lors de la modification de donn�es, on remet le niveau de s�curit� de l'appli au minimum --//
		this.application_.baisserNiveauSecurite();

		//-- On reinitialise le dessin du port --//
		this.application_.activerModelisation();
		this.application_.gestionModelisation_.setVisible(false);
		this.application_.gestionModelisation_.initialisationDessin();
	} 


	public static void ecritDonneestrajets(Sinavi3Navires c, String nomFichier) throws IOException {

		//creation du fichier des cat�gories des navires:
		final FortranWriter f = new FortranWriter(new FileWriter(nomFichier + ".trajet"));





		// format du fichier : 40 caract�res de donn�es + 1 s�parateur + 100 caract�res de commentaire
		final int[] fmt = new int[] { 40, 1, 100 };
		// ecriture du nombre de cat�gories
		f.stringField(0, new Integer(c.nombresTrajetsTotal()).toString());
		f.stringField(2, "nombre de trajets totaux toutes categories");
		f.writeFields(fmt);
		for (int i = 0; i < c.listeNavires_.size(); i++) {

			//-- recuperation du bateau --//
			Sinavi3Bateau bateau=c.retournerNavire(i);
			f.stringField(0, bateau.nom);
			f.stringField(2, "nom du bateau");
			f.writeFields(fmt);
			f.stringField(0, new Integer(bateau.listeTrajet_.size()).toString());
			f.stringField(2, "nombre de trajet du bateau "+bateau.nom);
			f.writeFields(fmt);


			for(int k=0;k<bateau.listeTrajet_.size();k++){
				//-- recuperation du trajet --//
				Sinavi3Trajet trajet=bateau.listeTrajet_.retournerTrajet(k);
				//-- on incr�mente l'indice des gares car dans Fortran les indices commencent � 1 et non 0 --//
				f.stringField(0, new Integer(trajet.gareDepart_+1).toString());
				f.stringField(2, "gare depart");
				f.writeFields(fmt);
				f.stringField(0, new Integer(trajet.gareArrivee_+1).toString());
				f.stringField(2, "gare arrivee");
				f.writeFields(fmt);
				if(trajet.sens_==0)
					f.stringField(0, "A");
				else
					f.stringField(0, "M");
				f.stringField(2, "sens");
				f.writeFields(fmt);
				if(trajet.typeLoiGenerationNavires_==0)
					f.stringField(0, "E");
				else
					if(trajet.typeLoiGenerationNavires_==1)
						f.stringField(0, "D");
					else
						f.stringField(0, "J");
				f.stringField(2, "type loi generation");
				f.writeFields(fmt);
				if(trajet.typeLoiGenerationNavires_==0)
				{
					//-- cas loi d erlang --//	
					f.stringField(0, new Integer(trajet.typeErlangLoi).toString());
					f.stringField(2, "type loi erlang=0 ou ecart type=1 ");
					f.writeFields(fmt);	
					
					if(trajet.typeErlangLoi ==0) {
						f.stringField(0, new Integer(trajet.loiErlangGenerationNavire).toString());
						f.stringField(2, "ordre loi erlang generation ");
						f.writeFields(fmt);	
					} else {
						f.stringField(0, new Double(trajet.loiErlangEcartType).toString());
						f.stringField(2, "ecart type Erlang ");
						f.writeFields(fmt);	
					}
					f.stringField(0, new Double(trajet.erlangDebut).toString());
					f.stringField(2, "horaire erlang debut");
					f.writeFields(fmt);
					f.stringField(0, new Double(trajet.erlangFin).toString());
					f.stringField(2, "horaire erlang fin");
					f.writeFields(fmt);
					f.stringField(0, new Integer(trajet.nbBateauxattendus).toString());
					f.stringField(2, "nombre bateaux attendus");
					f.writeFields(fmt);

				}
				else
					if(trajet.typeLoiGenerationNavires_==1)
					{
						f.stringField(0, new Integer(trajet.loiDeterministeOUjournaliere_.size()).toString());
						f.stringField(2, "nombre de couples deterministes");
						f.writeFields(fmt);
						//-- loi deterministe --//
						for(int h=0;h<trajet.loiDeterministeOUjournaliere_.size();h++)
						{
							f.stringField(0, new Integer(((CoupleLoiDeterministe)trajet.loiDeterministeOUjournaliere_.get(h)).jour_).toString()+" "+new Double(((CoupleLoiDeterministe)trajet.loiDeterministeOUjournaliere_.get(h)).temps_).toString());
							f.stringField(2, "couple loi deterministe "+(h+1));
							f.writeFields(fmt);

						}



					}
					else
					{
						f.stringField(0, new Integer(trajet.loiDeterministeOUjournaliere_.size()).toString());
						f.stringField(2, "nombre d'horaires journaliers");
						f.writeFields(fmt);
						//-- loi deterministe --//
						for(int h=0;h<trajet.loiDeterministeOUjournaliere_.size();h++)
						{
							f.stringField(0, new Double( ((CoupleLoiDeterministe)trajet.loiDeterministeOUjournaliere_.get(h)).temps_).toString());
							f.stringField(2, "horaire journalier "+(h+1));
							f.writeFields(fmt);

						}



					} 

			}

		}

//		fermeture du fichier de donn�es des cat�gories
		f.flush();
		f.close();
	}




	/**
	 * Methode de test de g�n�ration d un fichier html.
	 */
	void testHtml() {

		final String codeHTML = "<body bgcolor=\"#CCCCCC\">"
			+ "<p align=\"center\"><font color=\"#000099\" size=\"6\"> Rapport d'erreurs"
			+ "<table bgcolor=\"#CCCCCC\" border=\"5\" bordercolor=\"#333399\">" + " <TR><td>l1 </td>"
			+ "<td> l2 </img> </td> <td>l3 </td><td> l4 </td></tr><tr><td>l5 </td><td> l6 </td>"
			+ "<td>l7 </td> <td> l8 </td> </tr> <tr> <td>l9 </td><td> l10</td><td>l11 </td><td> l12 </td>"
			+ "</tr></TABLE> </center></body>";

		BuBrowserFrame fHtml = new BuBrowserFrame(this.application_);

		fHtml.setHtmlSource(codeHTML, "rapport erreurs");
		this.application_.addInternalFrame(fHtml);

	}



	//--methode qui consiste � r�cup�rer un ancien fichier sinavi et le convertir en nouveau sinavi
	public void moulinetteRecuperationSinavi2(){

		final int confirmation = new BuDialogConfirmation(application_.getApp(),
				Sinavi3Implementation.isSinavi_, "Vous �tes sur le point de charger un ancien fichier sinavi\n(fichier .nom)" +
				". Cette action �crasera l'ancien projet existant." +
				"\nLes lois de g�n�ration des trajets, biefs et �cluses ne pourront pas �tre r�cup�r�s." +
				"\nVoulez-vous vraiment continuer ?").activate();

		if (confirmation == 0) {

			try {


				/*Sinavi2Implementation sinavi2 = new Sinavi2Implementation();
        	final Fudaa f = new Fudaa();
        	f.startApp(sinavi2);


	     FudaaProjet projetOld = new FudaaProjet(sinavi2.getApp(), new FudaaFiltreFichier("nom"));
				 */

				FudaaProjet projetOld = new FudaaProjet("Sinavi2", new FudaaFiltreFichier("nom"));
				//-- option ouvrir --//
				projetOld.ouvrir2();



				//fred donc si projet_ ==null on arrete
				if (projetOld == null) return;


				/**
				 * 2) recuperation des donn�es dans le projet
				 */



				// B: recuperation des donnees du fichier de chargement:
				System.out.println("yahoo");
				ArrayList listeSimulationsSinavi2_ = new ArrayList();
				listeSimulationsSinavi2_ = (ArrayList) projetOld.getParam("par");

				if(listeSimulationsSinavi2_==null)
					System.out.println("erf on me renvoie un truc nul!");
				System.out.println("test 1: fichier: "+projetOld.getFichier());

				/*
	    for (int simu = 0; simu < listeSimulationsSinavi2_.size(); simu++) {
	         }
				 */
				//-- recuperation d'une simulation � la fois --//
				SSimulationSinavi2 simuOld=(SSimulationSinavi2) listeSimulationsSinavi2_.get(0);

				//A: initialisation des donn�es
//				donneesGenerales_ = new Sinavi3DonneesGenerales();
				listeBateaux_ = new Sinavi3Navires();

//				listeHoraire_ = new Sinavi3ListeHoraires();
//				nbHoraires_ = 0;
				listeGare_ = new Sinavi3Gares();

				listeBief_ = new Sinavi3Biefs();
				listeEcluse_ = new Sinavi3Ecluses();
				reglesVitesseBiefAvalant_ = new Sinavi3StrutureReglesDureesParcours();
				reglesVitesseBiefMontant_= new Sinavi3StrutureReglesDureesParcours();
				dureeManeuvresEcluses = new DureeManeuvreEcluse();
				
				//-- recopiage des donn�es --//

				int maxGare=0;
				
				
				//-- les donn�es g�n�rales --//
				this.params_.donneesGenerales.nombreJours=simuOld.parametresGeneration.dureeSimulation;
				this.params_.donneesGenerales.graine=simuOld.parametresGeneration.graine;

				//-- les bateaux --//
				for(int i=0;i<simuOld.parametresBateau.length;i++)
				{
					Sinavi3Bateau newBateau=new Sinavi3Bateau();

					newBateau.nom=simuOld.parametresBateau[i].identification;
					newBateau.largeurMax=simuOld.parametresBateau[i].largeur;
					newBateau.longueurMax=simuOld.parametresBateau[i].longueur;
					newBateau.vitesseDefautAvalant=simuOld.parametresBateau[i].vitesseDescendantParDefaut;
					newBateau.vitesseDefautMontant=simuOld.parametresBateau[i].vitesseMontantParDefaut;
					
					newBateau.creneauxJournaliers_.semaineCreneau1HeureDep=simuOld.parametresBateau[i].debutNavigation;
					newBateau.creneauxJournaliers_.semaineCreneau1HeureArrivee=simuOld.parametresBateau[i].finNavigation;
					newBateau.tirantEau=simuOld.parametresBateau[i].tirantDeau;
					newBateau.typeTirant=simuOld.parametresBateau[i].identification.substring(simuOld.parametresBateau[i].identification.length()-1);
					System.out.println("le type tirant du bateau est: "+newBateau.typeTirant);
					newBateau.priorite=0;
					
						
					
					//-- ajout du bateau dans la structure de bateaux --//
					this.listeBateaux_.ajout(newBateau);
				}

				//-- les tron�ons --//
				for(int i=0;i<simuOld.parametresBief.length;i++)
				{
					Sinavi3Bief newBief= new Sinavi3Bief(simuOld.parametresBateau.length);

					newBief.nom_=simuOld.parametresBief[i].identification;
					System.out.println("nom du tron�on "+i+": "+newBief.nom_);
					newBief.gareAmont_=simuOld.parametresBief[i].gareEnAmont;
					newBief.gareAval_=simuOld.parametresBief[i].gareEnAval;
					if(maxGare<newBief.gareAval_)maxGare=newBief.gareAval_;
					if(maxGare<newBief.gareAmont_)maxGare=newBief.gareAmont_;
					newBief.profondeur_=simuOld.parametresBief[i].hauteur;
					newBief.largeur_=simuOld.parametresBief[i].largeur;
					newBief.longueur_=simuOld.parametresBief[i].longueur;
					newBief.vitesse_=simuOld.parametresBief[i].vitesse;

					//recopiage loi erlang
					if(simuOld.parametresBief[i].loiE!=null){
						newBief.loiFrequence_=simuOld.parametresBief[i].loiE.ordreEcart;
						newBief.frequenceMoyenne_=simuOld.parametresBief[i].loiE.nbJoursDEcart;
						newBief.dureeIndispo_=simuOld.parametresBief[i].loiE.duree.heure+simuOld.parametresBief[i].loiE.duree.minutes/100.0;
						newBief.loiIndispo_=simuOld.parametresBief[i].loiE.ordreDuree;
					}
					newBief.typeLoi_=0;


					listeBief_.ajout(newBief);	



				}

				//-- les ecluses --//
				for(int i=0;i<simuOld.parametresEcluse.length;i++)
				{
					Sinavi3Ecluse newEcluse=new Sinavi3Ecluse();

					newEcluse.nom_=simuOld.parametresEcluse[i].identification;
					newEcluse.tempsFausseBassinneeAvalant_=(int) simuOld.parametresEcluse[i].dureeBassineeDescendante;
					newEcluse.tempsFausseBassinneeMontant_=(int) simuOld.parametresEcluse[i].dureeBassineeMontante;
					newEcluse.dureeManoeuvreEntrant_=(int) simuOld.parametresEcluse[i].dureeManoeuvresEnEntree;
					newEcluse.dureeManoeuvreSortant_=(int) simuOld.parametresEcluse[i].dureeManoeuvresEnSortie;
					newEcluse.gareAmont_=simuOld.parametresEcluse[i].gareEnAmont;
					newEcluse.gareAval_=simuOld.parametresEcluse[i].gareEnAval;
					if(maxGare<newEcluse.gareAval_)maxGare=newEcluse.gareAval_;
					if(maxGare<newEcluse.gareAmont_)maxGare=newEcluse.gareAmont_;
					newEcluse.largeur_=simuOld.parametresEcluse[i].largeur;
					newEcluse.longueur_=simuOld.parametresEcluse[i].longueur;
					newEcluse.profondeur_=simuOld.parametresEcluse[i].profondeur;

					//recopiage des loi erlang
					//recopiage loi erlang
					if(simuOld.parametresEcluse[i].loiE!=null){
						newEcluse.loiFrequence_=simuOld.parametresEcluse[i].loiE.ordreEcart;
						newEcluse.frequenceMoyenne_=simuOld.parametresEcluse[i].loiE.nbJoursDEcart;
						newEcluse.dureeIndispo_=simuOld.parametresEcluse[i].loiE.duree.heure+simuOld.parametresEcluse[i].loiE.duree.minutes/100.0;
						newEcluse.loiIndispo_=simuOld.parametresEcluse[i].loiE.ordreDuree;
					}
					newEcluse.typeLoi_=0;

					listeEcluse_.ajout(newEcluse);

				}

				//--creation des gares
				for(int i=0;i<maxGare;i++)
				{
					listeGare_.ajout("Gare "+(i+1));
				}

				//RECUPERATION DES VITESSES
				//-- premiere etape; initialisation des donn�es --//	
				for (int i = 0; i < this.listeBief_.listeBiefs_.size(); i++) {
					this.reglesVitesseBiefAvalant_.ajoutLigne(this.listeBateaux_.listeNavires_.size());
					this.reglesVitesseBiefMontant_.ajoutLigne(this.listeBateaux_.listeNavires_.size());
				}

				System.out.println("nb de tron�ons en tout: "+listeBief_.listeBiefs_.size());
				//-- ajout de la vitesse min d�faut pour chaque vitesse (min entre bateau et tron�on) --//
				for (int j = 0; j < listeBateaux_.listeNavires_.size(); j++) {
					this.reglesVitesseBiefAvalant_.modifierValeurAvalantNavire(j,this);
					this.reglesVitesseBiefMontant_.modifierValeurMontantNavire(j,this);
				}
				//--remplacement des vitesses pour les tron�ons de l'interface vitesse de l'onglet fonctionnement de sinavi2 --//
				for(int i=0;i<simuOld.parametresVitesse.length;i++){

					int indiceBateau=listeBateaux_.retournerIndiceNavire(simuOld.parametresVitesse[i].bateau);
					int indiceBief= listeBief_.retourneIndice(simuOld.parametresVitesse[i].bief);
					System.out.println(" indice trouve bateau:"+indiceBateau+ "Nom: "+simuOld.parametresVitesse[i].bateau);
					System.out.println("indice trouve bief:"+indiceBief+ "Nom: "+simuOld.parametresVitesse[i].bief);
					System.out.println("vitesse av:"+simuOld.parametresVitesse[i].vitesseDesAvalants);
					//-- modification des vitesses avalant et sortant pour le bief et navire choisi par la saisie de l'utilisateur --//
					this.reglesVitesseBiefAvalant_.modifierValeur(simuOld.parametresVitesse[i].vitesseDesAvalants, indiceBief,indiceBateau);
					this.reglesVitesseBiefMontant_.modifierValeur(simuOld.parametresVitesse[i].vitesseDesMontants, indiceBief,indiceBateau);


				}

				


				//RECUPERATION DES CROISEMENTS
				for(int i=0;i<simuOld.parametresCroisement.length;i++)
				{
					int bief=listeBief_.retourneIndice(simuOld.parametresCroisement[i].bief);
					int bateau1=listeBateaux_.retournerIndiceNavire(simuOld.parametresCroisement[i].type1);// je suppose que dans sinavi type1 corespond au navire de ligne 
					int bateau2=listeBateaux_.retournerIndiceNavire(simuOld.parametresCroisement[i].type2);
					boolean bool=simuOld.parametresCroisement[i].ouiNon;
					//--modification du croisement saisi (la modification est sym�trique)--//
					listeBief_.retournerBief(bief).reglesCroisement_.modification(bool, bateau1, bateau2);


				}
				//RECUPERATION DES TREMATAGES
				for(int i=0;i<simuOld.parametresTrematage.length;i++)
				{
					int bief=listeBief_.retourneIndice(simuOld.parametresTrematage[i].bief);
					int bateau1=listeBateaux_.retournerIndiceNavire(simuOld.parametresTrematage[i].type1);// je suppose que dans sinavi type1 corespond au navire de ligne 
					int bateau2=listeBateaux_.retournerIndiceNavire(simuOld.parametresTrematage[i].type2);
					boolean bool=simuOld.parametresTrematage[i].ouiNon;
					//--modification du Trematage saisi (la modification est sym�trique)--//
					listeBief_.retournerBief(bief).reglesTrematage_.modification(bool, bateau1, bateau2);


				}

				//RECUPERATION DES TRAJETS
				for(int i=0;i<simuOld.parametresTrajet.length;i++)
				{
					int bateau= listeBateaux_.retournerIndiceNavire(simuOld.parametresTrajet[i].bateau);
					boolean avalantmontant= simuOld.parametresTrajet[i].avalantMontant;

					//-- recuperation de la liste trajet du bateau correspondant: --//
					Sinavi3Trajets listeTrajet=listeBateaux_.retournerNavire(bateau).listeTrajet_;
					Sinavi3Trajet nouveauTrajet=new Sinavi3Trajet();
					nouveauTrajet.gareDepart_=simuOld.parametresTrajet[i].gareDepart;
					nouveauTrajet.gareArrivee_=simuOld.parametresTrajet[i].gareArrivee;
					if(!avalantmontant)
						nouveauTrajet.sens_=0;
					else
						nouveauTrajet.sens_=1;


					//Creation d une loi d erlang par defaut car on ne peut recuperer les donn�es (incompatibles)
					nouveauTrajet.typeLoiGenerationNavires_=0;
					if(simuOld.parametresTrajet[i].loiE!=null){
						nouveauTrajet.nbBateauxattendus=simuOld.parametresTrajet[i].loiE.nbreBateauxAttendus;
						nouveauTrajet.erlangDebut=simuOld.parametresTrajet[i].loiE.heureDebutGeneration.heure+simuOld.parametresTrajet[i].loiE.heureDebutGeneration.minutes/100.0;
						nouveauTrajet.erlangFin=simuOld.parametresTrajet[i].loiE.heureFinGeneration.heure+simuOld.parametresTrajet[i].loiE.heureFinGeneration.minutes/100.0;
						nouveauTrajet.loiErlangGenerationNavire=simuOld.parametresTrajet[i].loiE.ordreDeLaLoi;
					}
					//-- ajout du trajet dans la liste des trajets
					listeTrajet.ajouterTrajet(nouveauTrajet);

				}



				//Ajoute le projet qui vient d'etre ouvert et en fait le projet courant.
				//  this.application_.liste_.ajouteSimulation(nomFichierVoulu, this.projet_);
				//  this.application_.liste_.setSelectedValue(nomFichierVoulu, true);


				//-- changement de fichier: incr�mentation de 1 pour chaque sous simulation:
				//projet_.setFichier(projet_.getFichier()+simu+".sinavi");

				//-- enregistrement du de la simulation --//
				enregistrer();



				//-- mise en garde de la pertinence de la moulinette... --//
				new BuDialogMessage(this.application_.getApp(), this.application_.getInformationsSoftware(),
						"Donnees de l'ancien projet recuperees. \n ATTENTION: les lois de generation des ecluses, des  tron�ons et des trajets ne" +
				"\n peuvent pas etre recuperees car ils different.\nIl faudra les modifier manuellement.").activate();




			} catch (final java.lang.ClassCastException c) {


				new BuDialogError(this.application_.getApp(), this.application_.getInformationsSoftware(),
						"Ce fichier ne contient pas de donn�es sinavi valides!").activate();

			}	


		}


	}

	public FudaaProjet getProjet_() {
		return projet_;
	}

	public void setProjet_(FudaaProjet projet_) {
		this.projet_ = projet_;
	}

	public SParametresSinavi32 getParams_() {
		return params_;
	}

	public void setParams_(SParametresSinavi32 params_) {
		this.params_ = params_;
	}

	public Sinavi3ResultatListeevenementsSimu getListeResultatsSimu_() {
		return listeResultatsSimu_;
	}

	public void setListeResultatsSimu_(
			Sinavi3ResultatListeevenementsSimu listeResultatsSimu_) {
		this.listeResultatsSimu_ = listeResultatsSimu_;
	}

	public Sinavi3ResultatListeOccupations getListeResultatsOccupationsSimu_() {
		return listeResultatsOccupationsSimu_;
	}

	public void setListeResultatsOccupationsSimu_(
			Sinavi3ResultatListeOccupations listeResultatsOccupationsSimu_) {
		this.listeResultatsOccupationsSimu_ = listeResultatsOccupationsSimu_;
	}

	public Sinavi3ResultatListeBassinees getListeResultatsBassineesSimu_() {
		return listeResultatsBassineesSimu_;
	}

	public void setListeResultatsBassineesSimu_(
			Sinavi3ResultatListeBassinees listeResultatsBassineesSimu_) {
		this.listeResultatsBassineesSimu_ = listeResultatsBassineesSimu_;
	}

	public Sinavi3Navires getListeBateaux_() {
		return listeBateaux_;
	}

	public void setListeBateaux_(Sinavi3Navires listeBateaux_) {
		this.listeBateaux_ = listeBateaux_;
	}

	public Sinavi3Gares getListeGare_() {
		return listeGare_;
	}

	public void setListeGare_(Sinavi3Gares listeGare_) {
		this.listeGare_ = listeGare_;
	}

	public Sinavi3Biefs getListeBief_() {
		return listeBief_;
	}

	public void setListeBief_(Sinavi3Biefs listeBief_) {
		this.listeBief_ = listeBief_;
	}

	public Sinavi3Ecluses getListeEcluse() {
		return listeEcluse_;
	}

	public void setListeEcluse_(Sinavi3Ecluses listeEcluse_) {
		this.listeEcluse_ = listeEcluse_;
	}

	public Sinavi3StrutureReglesDureesParcours getReglesVitesseBiefAvalant_() {
		return reglesVitesseBiefAvalant_;
	}

	public void setReglesVitesseBiefAvalant_(
			Sinavi3StrutureReglesDureesParcours reglesVitesseBiefAvalant_) {
		this.reglesVitesseBiefAvalant_ = reglesVitesseBiefAvalant_;
	}

	public Sinavi3StrutureReglesDureesParcours getReglesVitesseBiefMontant_() {
		return reglesVitesseBiefMontant_;
	}

	public void setReglesVitesseBiefMontant_(
			Sinavi3StrutureReglesDureesParcours reglesVitesseBiefMontant_) {
		this.reglesVitesseBiefMontant_ = reglesVitesseBiefMontant_;
	}

	

	public Sinavi3Implementation getApplication() {
		return application_;
	}

	public SinaviStructureReglesRemplissageSAS getReglesRemplissageSAS() {
		return reglesRemplissageSAS_;
	}

	public void setReglesRemplissageSAS_(
			SinaviStructureReglesRemplissageSAS reglesRemplissageSAS_) {
		this.reglesRemplissageSAS_ = reglesRemplissageSAS_;
	}

	public List<Sinavi3ResultatConsoEau> getListeResultatsConsommationEau_() {
		return listeResultatsConsommationEau_;
	}

	public void setListeResultatsConsommationEau_(
			List<Sinavi3ResultatConsoEau> listeResultatsConsommationEau_) {
		this.listeResultatsConsommationEau_ = listeResultatsConsommationEau_;
	}

	
	public double getMaxConsommationEau(){
		double max = 0;
		if(listeResultatsConsommationEau_ != null ){
			for(Sinavi3ResultatConsoEau val: listeResultatsConsommationEau_) {
				if(val!= null && val.consommationEau>max) {
					max = val.consommationEau;
				}
			}
		} 
		return max;
	}

	public Sinavi3InternalFrame getFrameVerif_() {
		return frameVerif_;
	}

	public void setFrameVerif_(Sinavi3InternalFrame frameVerif_) {
		this.frameVerif_ = frameVerif_;
	}

	public GenarrListeNavires getGenarr_() {
		return genarr_;
	}

	public void setGenarr_(GenarrListeNavires genarr_) {
		this.genarr_ = genarr_;
	}

	public Sinavi3Ecluses getListeEcluse_() {
		return listeEcluse_;
	}

	public SinaviStructureReglesRemplissageSAS getReglesRemplissageSAS_() {
		return reglesRemplissageSAS_;
	}

	public Sinavi3Implementation getApplication_() {
		return application_;
	}

	public String getProjectName_() {
		return projectName_;
	}

	public String getOldProjectName_() {
		return oldProjectName_;
	}

	
	public void notifyAddEcluse(final Sinavi3Ecluse e) {
		this.getDureeManeuvresEcluses().addEcluse(e, this);
		getReglesRemplissageSAS().ajoutLigne(getListeBateaux_().getListeNavires_().size());
	    if(getApplication().getGestionDureeManeuvreEcluse_() != null)
	    	   ((SinaviPanelDureeManeuvresEcluses)getApplication().getGestionDureeManeuvreEcluse_()).buildTable(this);
	}
	
	
	public void notifyAddBateau(final Sinavi3Bateau b) {
		
		this.reglesVitesseBiefAvalant_.ajoutNavireAvalantt(this);
		this.reglesVitesseBiefMontant_.ajoutNavireMontant(this);
		this.getDureeManeuvresEcluses().addBateau(b);
		this.getReglesRemplissageSAS().ajoutNavire();
		if(getApplication().getGestionDureeManeuvreEcluse_() != null)
			 ((SinaviPanelDureeManeuvresEcluses)getApplication().getGestionDureeManeuvreEcluse_()).buildTable(this);
		
	}
	public DureeManeuvreEcluse getDureeManeuvresEcluses() {
		return dureeManeuvresEcluses;
	}

	public void setDureeManeuvresEcluses(DureeManeuvreEcluse dureeManeuvresEcluses) {
		this.dureeManeuvresEcluses = dureeManeuvresEcluses;
	}

	public List<Sinavi3ResultatGarage> getListeGarages() {
		return listeResultatsGarages;
	}

	public void setListeGarages(List<Sinavi3ResultatGarage> listeGarages) {
		this.listeResultatsGarages = listeGarages;
	}

}
