package org.fudaa.fudaa.sinavi3;

import java.awt.CardLayout;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JInternalFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.table.CtuluTableExcelWriter;
import org.fudaa.fudaa.ressource.FudaaResource;
import org.jdesktop.swingx.ScrollPaneSelector;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuDialogConfirmation;
import com.memoire.bu.BuDialogError;
import com.memoire.fu.FuLog;

/**
 * INterface Graphique principale pour la visualisation de Navires: Cette interface permet de : - visualiser les
 * diffrentes catgories de Navires saisis - ajouter une catgorie de Navire et ses donnes correspondantes - modifier une
 * catgorie de Navire pralablement cree - supprimer une catgorie de Navire qui ne convient pas
 * 
 * @author Adrien Hadoux
 */

public class Sinavi3VisualiserBateaux extends Sinavi3InternalFrame {

  // attributs

  /**
   * Donnes de la simulation
   */
  Sinavi3DataSimulation donnees_;

  /**
   * Layout cardlayout pour affichage des donnes
   */
  CardLayout pile_;

  /**
   * Le panel de base qui contient tous les differents panels contient un layout de type CardLayout
   */
  JPanel principalPanel_;

  /**
   * Panel d'affichage des differents Navires saisis layout classique flow layout ou grid layout
   */
  Sinavi3PanelAffichageBateaux affichagePanel_;

  /**
   * ascenseur pour le panel d'affichage
   */
  JScrollPane ascAff_;

  /**
   * panel qui contient l'ascenceur (OPTIONNEL!!!!!!!!!!)
   */
  JPanel conteneurAffichage_;

  /**
   * Panel de saisie des donnes relative aux Navires
   */

  Sinavi3PanelSaisieBateaux SaisieNavirePanel_;

  /**
   * Panel de commande: panel qui contient les differnets boutons responsable de: -ajout -suppression -modification des
   * Navires
   */
  JPanel controlePanel_;

  /**
   * Boutton de selection de la saisie
   */
  private final BuButton boutonSaisie_ = new BuButton(FudaaResource.FUDAA.getIcon("ajouter"), "ajout");
  private final BuButton boutonAffichage_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_voir"), "voir");
  private final BuButton modification_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_maj"), "modif");
  private final BuButton quitter_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_oui"), "quitter");
  private final BuButton suppression_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_detruire"), "suppr");
  private final BuButton duplication_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_ranger"), "dupliquer");
  private final BuButton impression_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_generer"), "Excel");

  
  /**
   * Combo de visualisation des Navires deja saisis
   */
  JComboBox listeNaviresSaisis_ = new JComboBox();

  boolean alterneTitre_ = true;

  /**
   * Constructeur de la Jframe
   */

  Sinavi3VisualiserBateaux(final Sinavi3DataSimulation d) {
    super("", true, true, true, true);
    // recuperation des donnes
    donnees_ = d;

    setTitle("Visualisation des Categories de bateaux");

    // location de la JFrame:
    // this.setLocationRelativeTo(this.getParent());
    setSize(770, 540);
    setBorder(Sinavi3Bordures.compound_);
    
    /**
     * tooltiptext des boutons
     */
    this.boutonAffichage_.setToolTipText("permet de visualiser la totalit� des donn�es sous forme d'un tableau");
    this.boutonSaisie_
        .setToolTipText("permet de saisir une nouvelle donn�e afin de l'ajouter � l'ensemble des param�tres");
    this.modification_
        .setToolTipText("permet de modifier un �l�ment: il faut dabord cliquer sur l'�l�ment � modifier dans le menu \"voir\"");
    this.quitter_.setToolTipText("cliquez sur ce bouton pour fermer la sous fen�tre");
    this.suppression_
        .setToolTipText("permet de supprimer une donn�e: cliquez d'abord sur l'�l�ment � supprimer dans le menu \"voir\"");
    this.duplication_
        .setToolTipText("permet de dupliquer une donn�e: cliquez d'abord sur l'�l�ment � dupliquer dans le menu \"voir\"");
    this.impression_
        .setToolTipText("permet d'importer le contenu des donn�es dans un fichier excel que l'on pourra par la suite imprimer");

    // definition des differents Layout
    final Container contenu = getContentPane();

    this.principalPanel_ = new JPanel();

    pile_ = new CardLayout(30, 10);
    this.principalPanel_.setLayout(pile_);

    // definition du propre panel d'affichage des Navires
    this.affichagePanel_ = new Sinavi3PanelAffichageBateaux(donnees_);

    // definition de l ascenceur pour le panel d'affichage
    this.ascAff_ = new JScrollPane(affichagePanel_);
    
    //utilisation de swingx
    //ScrollPaneSelector.installScrollPaneSelector(this.ascAff_);
    
    
    // definition du panel de saisie d'un bateau

    SaisieNavirePanel_ = new Sinavi3PanelSaisieBateaux(donnees_, this);

    this.controlePanel_ = new JPanel();
    this.controlePanel_.setLayout(new FlowLayout());

    // ajout des 2 panel d'affichage et de saisie d'un Navire
    this.principalPanel_.add(affichagePanel_, "affichage");
    final JScrollPane pasc = new JScrollPane(this.SaisieNavirePanel_);
    //utilisation de swingx
    ScrollPaneSelector.installScrollPaneSelector(pasc);
    
    this.principalPanel_.add(pasc, "saisie");

    // ajout des panel dans la frame
    contenu.add(principalPanel_);
    contenu.add(controlePanel_, "South");

    // definition des Bubuttons:

    quitter_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        Sinavi3VisualiserBateaux.this.windowClosed();

      }
    });

    // ajout des boutons dans le panel de controle
    controlePanel_.add(quitter_);

    boutonSaisie_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        SaisieNavirePanel_.initialiser();
        pile_.last(principalPanel_);
        setTitle("Saisie d'une nouvelle categorie de bateau");
        SaisieNavirePanel_.setBorder(Sinavi3Bordures.navire);
        validate();

      }
    });

    // ajout des boutons dans le panel de controle
    controlePanel_.add(boutonSaisie_);

    boutonAffichage_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        pile_.first(principalPanel_);

        alterneTitre_ = true;
        setTitle("Visualisation des Catgories de bateaux");
        
        validate();

      }
    });

    // ajout des boutons dans le panel de controle
    controlePanel_.add(boutonAffichage_);

    modification_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {

        // 1) recuperation du numero de ligne du quai via la jtable
        System.out.println("La ligne selectionnee est: " + affichagePanel_.tableau_.getSelectedRow());
        final int numNavire = affichagePanel_.tableau_.getSelectedRow();
        if (numNavire == -1) {
          new BuDialogError(donnees_.application_.getApp(), Sinavi3Implementation.isSinavi_,
              "Vous devez cliquer sur la cat�gorie de bateaux �\nmodifier dans le tableau d'affichage.")
              .activate();

        } else {
          // 2.5 changmeent de fenetre
          pile_.last(principalPanel_);
          setTitle("Modification d'une cat�gorie de bateau");
          validate();

          // 2)appel a la mehode de modification de PanelSaisieNavire(a ecrire): met boolean MODIF=true
          SaisieNavirePanel_.MODE_MODIFICATION(numNavire);
          SaisieNavirePanel_.setBorder(Sinavi3Bordures.navire2);

          // 3)lors de la sauvegarde , utilise le booleen pour remplacer(methode set des donn�es) au lieu de add
        }// fin du if

      }
    });

    // ajout des boutons dans le panel de controle
    controlePanel_.add(modification_);

    suppression_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        // 1) recuperation du numero de ligne du quai via la jtable
        System.out.println("La ligne selectionnee est: " + affichagePanel_.tableau_.getSelectedRow());
        final int numNavire = affichagePanel_.tableau_.getSelectedRow();
        if (numNavire == -1) {
          new BuDialogError(donnees_.application_.getApp(), Sinavi3Implementation.isSinavi_,
              "Vous devez cliquer sur la cat�gorie de bateaux �\nsupprimer dans le tableau d'affichage.")
              .activate();

        }

        else {

          // on s occupe de la supresion des cat�gories de navires:
          // 1)on demande confirmation:
          final int confirmation = new BuDialogConfirmation(donnees_.application_.getApp(),
              Sinavi3Implementation.isSinavi_, "Etes-vous certain de vouloir supprimer la cat�gorie de bateaux "
                  + donnees_.listeBateaux_.retournerNavire(numNavire).nom + " ?").activate();

          if (confirmation == 0) {
            // 2)on supprime le numero du quai correspondant a la suppression
        	  Sinavi3Bateau bateau = donnees_.listeBateaux_.retournerNavire(numNavire);
        	  
            donnees_.listeBateaux_.suppression(numNavire);

            /**
             * Suppression de la ligne et colonne correspondante des regles de navigations pour chaque chenal et chaque
             * cercle
             */
            for (int i = 0; i < donnees_.listeBief_.listeBiefs_.size(); i++) {
              final Sinavi3Bief chenal = donnees_.listeBief_.retournerBief(i);
              chenal.reglesCroisement_.suppressionNavire(numNavire);
              chenal.reglesTrematage_.suppressionNavire(numNavire);
            }
            

            /**
             * Regles durees passage: suppression de la colonne pour la cat�gorie de navire
             */
            donnees_.reglesVitesseBiefAvalant_.suppressionNavire(numNavire);
            donnees_.reglesVitesseBiefMontant_.suppressionNavire(numNavire);
            
            /**
             * Regles durees maneuvres: suppression de la colonne pour la cat�gorie de navire
             */
            donnees_.getDureeManeuvresEcluses().deleteBateau(bateau);
         
            donnees_.getReglesRemplissageSAS().suppressionNavire(numNavire);
            
            // 3)mise a jour de l affichage:
            affichagePanel_.maj(donnees_);
            
            //--On baisse le niveau de s�curit� pour forcer le test de coh�rence globale --//
            d.baisserNiveauSecurite2();

          }// fin cas ou l on confirme la suppression

        }// fin du else

      }
    });
    controlePanel_.add(suppression_);
    // controlePanel.setBorder(this.raisedBevel);

    this.duplication_.addActionListener(new ActionListener() {

      public void actionPerformed(final ActionEvent e) {
        System.out.println("test duplication: ");
        final int numNavire = affichagePanel_.tableau_.getSelectedRow();
        if (numNavire == -1) {
          new BuDialogError(donnees_.application_.getApp(), Sinavi3Implementation.isSinavi_,
              "Vous devez cliquer sur la cat�gorie de bateaux �\ndupliquer dans le tableau d'affichage.")
              .activate();

        } else {

          // on s occupe de la duplication des quais:
          // 1)on demande confirmation:
          String confirmation = "";
          confirmation = JOptionPane.showInputDialog(null, "Nom de la cat�gorie de bateaux � dupliquer: ");

          if (donnees_.listeBateaux_.existeDoublon(confirmation, -1)) {
            new BuDialogError(donnees_.application_.getApp(), Sinavi3Implementation.isSinavi_, "Erreur!! Nom deja pris!!")
                .activate();
            return;
          }

          if (!confirmation.equals("")) {
            // 2)ON RECOPIE TOUTES LES DONNEES DUPLIQUEES dans un nouveau quai quel on ajoute
            final Sinavi3Bateau nouveau = new Sinavi3Bateau();
            final Sinavi3Bateau navireAdupliquer = donnees_.listeBateaux_.retournerNavire(numNavire);

            
            
            nouveau.longueurMax = navireAdupliquer.longueurMax;
            nouveau.largeurMax = navireAdupliquer.largeurMax;
            nouveau.vitesseDefautMontant = navireAdupliquer.vitesseDefautMontant;
            nouveau.vitesseDefautAvalant = navireAdupliquer.vitesseDefautAvalant;
            nouveau.dureeAttenteMaximaleAdmissible = navireAdupliquer.dureeAttenteMaximaleAdmissible;
            nouveau.creneauxJournaliers_.recopie(navireAdupliquer.creneauxJournaliers_);
            
            
            nouveau.tirantEau = navireAdupliquer.tirantEau;
            nouveau.typeTirant = navireAdupliquer.typeTirant;
            
           
           //-- dupication des trajets --//
            nouveau.listeTrajet_=navireAdupliquer.listeTrajet_.dupliquerTrajets();
                        
             

            // on donne le nouveau nom du quai a dupliquer
            nouveau.nom = confirmation;

            // on ajoute le nouveau quai dupliqu�
            donnees_.listeBateaux_.ajout(nouveau);

            /**
             * Creation d'un vecteur ligne regles de navigations suppl�mentaire pour chaque chenal et pour chaque
             * cercle:
             */
            for (int i = 0; i < donnees_.listeBief_.listeBiefs_.size(); i++) {
              final Sinavi3Bief chenal = donnees_.listeBief_.retournerBief(i);

              chenal.reglesCroisement_.ajoutNavire();
              chenal.reglesTrematage_.ajoutNavire();
            }
            
            /**
             * Ajout d'une colonne regles dur�es parcours
             */
            donnees_.notifyAddBateau(nouveau);
            
            
            
            // 3)mise a jour de l affichage:
            affichagePanel_.maj(donnees_);
          }// fin cas ou l on confirme la suppression
        }

      }
    });
    this.controlePanel_.add(duplication_);

    impression_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        File fichier;
        // definition d un file chooser
        final JFileChooser fc = new JFileChooser();
        final int returnVal = fc.showSaveDialog(Sinavi3VisualiserBateaux.this);

        if (returnVal == JFileChooser.APPROVE_OPTION) {
          fichier = fc.getSelectedFile();
          final File f = CtuluLibFile.appendExtensionIfNeeded(fichier, "xls");

          // on r�cupere l abstrct model du tableau contenant les donn�es

          /**
           * creation d un abstract model impl�mentant l'interface CtuluTableModelInterface
           */

          final Sinavi3ModeleExcel modele = new Sinavi3ModeleExcel();

          // creation du tableau des titres des colonnes en fonction de la fenetre d affichage desecluses
          modele.nomColonnes_ = affichagePanel_.titreColonnes_;

          /**
           * transformation du model du tableau deja rempli pour le nuoveau model cr�e
           */

          /**
           * recopiage des titres des colonnes
           */
          // initialisation de la taille de data
          modele.data_ = new Object[donnees_.listeBateaux_.listeNavires_.size() + 2][affichagePanel_.titreColonnes_.length];

          for (int i = 0; i < affichagePanel_.titreColonnes_.length; i++) {

            // ecriture des nom des colonnes:
            modele.data_[0][i] = affichagePanel_.titreColonnes_[i];

          }

          /**
           * recopiage des donn�es
           */
          for (int i = 0; i < donnees_.listeBateaux_.listeNavires_.size(); i++) {
            modele.data_[i + 2] = affichagePanel_.constructLine(i);
          }

          modele.setNbRow(donnees_.listeBateaux_.listeNavires_.size() + 2);

          /**
           * on essaie d 'ecrire en format excel
           */
          final CtuluTableExcelWriter ecrivain = new CtuluTableExcelWriter(modele, f);

          try {
            ecrivain.write(null);

          } catch (final RowsExceededException _err) {
            FuLog.error(_err);
          } catch (final WriteException _err) {
            FuLog.error(_err);
          } catch (final IOException _err) {
            FuLog.error(_err);
          }

        }// fin du if si le composant est bon

      }// fin de la methode public actionPerformed

    });

    this.controlePanel_.add(impression_);

    // combo de choix des Navires:
    // etape 1: Initialisation des donnes deja saisies
    for (int j = 0; j < donnees_.listeBateaux_.listeNavires_.size(); j++) {
      this.listeNaviresSaisis_.addItem(((Sinavi3Bateau) donnees_.listeBateaux_.listeNavires_.get(j)).nom);
    }

    // listener lors du choix dela combo Box
    this.listeNaviresSaisis_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        System.out.println("bateau selectionn!!");
        /**
         * SUITE
         */
      }
    });

    /**
     * *************************************************************************************** THE PITI MENU QUI SERVENT
     * A RIEN ***************************************************************************************
     */

    /**
     * choix agreable des cat�gories de navire via un jcombobox
     */
    // controlePanel.add(new JLabel(" Choix de la catgorie: "));
    // controlePanel.add(this.listeNaviresSaisis);

    // ********************************************** test rendu

    // petite barre de menu agreable
    final JMenuBar menuBar = new JMenuBar();
    final JMenu menuFile = new JMenu("Fichier");
    final JMenuItem menuFileExit = new JMenuItem("Quitter");
    final JMenu menuOption = new JMenu("Options");
    final JMenu menuInfo = new JMenu("A propos de");

    // menuFile.set.setLabel("Fichier");
    // menuOption.setLabel("Options");
    // menuFileExit.setLabel("Quitter");
    // menuInfo.setLabel("A propos de");

    menuFileExit.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        Sinavi3VisualiserBateaux.this.windowClosed();
      }
    });
    menuFile.add(menuFileExit);
    menuBar.add(menuFile);
    menuBar.add(menuOption);
    menuBar.add(menuInfo);
    setJMenuBar(menuBar);

    // gestion de la fermeture de la frame:
    /*
     * this.addWindowListener ( new WindowAdapter() { public void windowClosing(WindowEvent e) {
     * SiporVisualiserNavires.this.windowClosed(); } } );
     */

    // affichage de la frame

    setVisible(true);

  }

  /**
   * Methode qui s active lorsque l'on quitte l'application
   */
  protected void windowClosed() {
    // verif.stop(); //stop n est aps sur on le modifie donc par une autre variable

    // desactivation du thread
    // dureeVieThread=false;

    System.out.print("Fin du programme! a bientot!!");
    dispose();
  }

}
