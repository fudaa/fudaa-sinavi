/**
 *@creation 14 nov. 06
 *@modification $Dates$
 *@license  GNU General Public Licence 2
 *@copyright   (c)1998-2006 CETMEF 2 bd Gambetta F -60231 Compiegne
 *@mail   devel@fudaa.fr 
 */
package org.fudaa.fudaa.sinavi3;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.ByteArrayInputStream;
import java.io.IOException;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.ButtonGroup;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;
import javax.swing.table.TableColumn;

import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.image.CtuluImageExport;
import org.fudaa.ebli.graphe.BGraphe;
import org.fudaa.fudaa.ressource.FudaaResource;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuScrollPane;
import com.memoire.bu.BuTabbedPane;
import com.memoire.bu.BuTable;
import com.memoire.fu.FuLog;

/**
 * classe de gestion des resultats de la generation des bateaux propose 2 onglets: le premier propose un affichage.
 * 
 * @version $Version$
 * @author Adrien Hadoux
 */
public class Sinavi3ResultatsAttenteTrajet extends Sinavi3InternalFrame /*implements ActionListener */{

  /**
   * ensemble des donn�es du tableau sous la forme de data.
   */
  Object[][] data_;
  
  /**
   * Ensemble des donn�es du tableau de distribution sous la forme de data.
   */
  double[][][] dataDistrib_;
  
  double[][][] dataDistribParCategorie_;
  
  double[][][] dataDistribParAttente_;
  
  Object[][] dataDistribAffich_;

  /**
   * Graphe associ�e aux r�sultats de la g�n�ration de bateaux.
   */
  BGraphe graphe_ = new BGraphe();

  /**
   * Camembert des proportions.
   */
  ChartPanel camembert_;
  
  /**
   * Panel de gestion du camembert.
   */
  BuPanel panelCamembert_=new BuPanel(new BorderLayout());
  
  ChartPanel grapheDistrib_;
  
  /**
   * panels principaux de la fenetre.
   */
  BuPanel panelPrincipal_ = new BuPanel();
  BuPanel panelPrincipalClassique_ = new BuPanel();
  BuPanel panelPrincipalDistrib_ = new BuPanel();

  /**
   * Panel de selection des preferences.
   */
  BuPanel selectionPanel_ = new BuPanel();
  BuPanel selectionPanel1_;
  String[] listeElt_ = { "tron�on", "�cluse"};
  JComboBox listeTypesDepart_ = new JComboBox(listeElt_);
  JComboBox listeElementDepart_ = new JComboBox();
  JComboBox listeTypesArrivee_ = new JComboBox(listeElt_);
  JComboBox listeElementArrivee_ = new JComboBox();

  String[] chaineSens_ = { "avalant", "montant"};
  JComboBox sens_ = new JComboBox(chaineSens_);

  /**
   * Panel des options: type affichages, colonnes � faire figurer.
   */
  BuPanel optionPanelTableau_ = new BuPanel(new BorderLayout());
  BuPanel optionPanelDistrib_ = new BuPanel(new BorderLayout());
  
  String[] typeGraphe_ = { "Lignes", "Barres"};
  JComboBox choixTypeGraphe_ = new JComboBox(typeGraphe_);

  JCheckBox choixNbNavires_ = new JCheckBox("nombre bateaux", true);
  JCheckBox choixTotalAttente_ = new JCheckBox("Attentes totales", true);

  JCheckBox choixMarees_ = new JCheckBox("Attentes mar�es", true);
  JCheckBox choixSecurite_ = new JCheckBox("Attentes de s�curit�", true);
  JCheckBox choixAcces_ = new JCheckBox("Attentes d'acc�s", true);
  JCheckBox choixOccupation_ = new JCheckBox("Attentes d'occupation", true);
  JCheckBox choixPannes_ = new JCheckBox("Attentes d'indisponibilit�", true);
  
  BuPanel optionPanelGraph_ = new BuPanel(new BorderLayout());
  JRadioButton choixTotalAttente2_ = new JRadioButton("Attentes totales", true);
  JRadioButton choixSecurite2_ = new JRadioButton("Attentes de s�curit�", false);
  JRadioButton choixAcces2_ = new JRadioButton("Attentes d'acc�s", false);
  JRadioButton choixOccupation2_ = new JRadioButton("Attentes d'occupation", false);
  JRadioButton choixPannes2_ = new JRadioButton("Attentes d'indisponibilit�", false);
  
  JCheckBox choixCumul_ = new JCheckBox("Dur�es cumul�es", false);
  JRadioButton choixNombre_ = new JRadioButton("Nombres", true);
  JRadioButton choixPercent_ = new JRadioButton("Pourcentages", false);
  JRadioButton choixCategorie_ = new JRadioButton("Par cat�gorie", true);
  JRadioButton choixTypeAttente_ = new JRadioButton("Par type d'attente", false);
  
  JCheckBox[] tableauChoixNavires_;
  JCheckBox[] tableauChoixNavires2_;
  JCheckBox[] tableauChoixNaviresDistrib_;

  Sinavi3TextFieldInteger valSeuilHeure_ = new Sinavi3TextFieldInteger(3);
  Sinavi3TextFieldInteger valSeuilMinute_ = new Sinavi3TextFieldInteger(2);
  JCheckBox valideSeuil_ = new JCheckBox("Seuil:", false);
  int valeurSeuil_;
  boolean seuil_;
  
  Sinavi3TextFieldInteger valSeuilDistribHeure_ = new Sinavi3TextFieldInteger(3);
  Sinavi3TextFieldInteger valSeuilDistribMinute_ = new Sinavi3TextFieldInteger(2);
  JCheckBox valideSeuilDistrib_ = new JCheckBox("Seuil:", false);
  int valeurSeuilDistrib_;
  boolean seuilDistrib_;

  /**
   * Tableau r�capitulatif des r�sultats de la simulation.
   */
  BuTable tableau_;
  
  /**
   * Tableau de distribution des r�sultats de la simulation
   */
  BuTable tableauDistrib_;

  String[] titreTableau_ = { "Cat�gorie", "Nombre de bateaux", "Att. s�curit�: total", "Att. s�curit�: moyenne", "Att. s�curit�: nb. bateaux", "Att. s�curit�: % bateaux", "Att. s�curit�: moy./flotte", "Att. acc�s: total", "Att. acc�s: moyenne",
      "Att. acc�s: nb. bateaux", "Att. acc�s: % bateaux", "Att. acc�s: moy./flotte", "Att. occup.: total", "Att. occup.: moyenne", "Att. occup.: nb. bateaux", "Att. occup.: % bateaux", "Att. occup.: moy./flotte",
      "Att. indisp.: total", "Att. indisp.: moyenne", "Att. indisp.: nb. bateaux",  "Att. indisp.: % bateaux","Att. indisp.: moy./flotte", "Attente totale", "Attente moyenne",
      "Nb. bateaux ayant attendu", "% bateaux ayant attendu", "Attente moyenne sur flotte" };
  
  String[] titreTableauDistrib_;

  /**
   * Panels tabbed qui g�rent les 2 onglets, ie les 2 versions d'affichage des r�sultats.
   */
  BuTabbedPane panelPrincipalAffichage_ = new BuTabbedPane();
  BuTabbedPane panelPrincipalAffichageClassique_ = new BuTabbedPane();
  BuTabbedPane panelPrincipalAffichageDistrib_ = new BuTabbedPane();
  
  /**
   * Panel contenant le bouton quitter
   */
  BuPanel panelQuitter_ = new BuPanel();

  /**
   * Panel contenant la repr�sentation graphique
   */
  BuPanel panelGraphAffichage_ = new BuPanel(new BorderLayout());

    
  /**
   * Panel cniotenant le tableau et les boutns de controles
   */
  BuPanel panelGestionTableau_ = new BuPanel();
  
  /**
   * Panel contenant le tableau de distribution et les boutons de controle
   */
  BuPanel panelGestionTableauDistrib_ = new BuPanel();

  /**
   * panel de gestion du tableau et de ses options
   */
  BuPanel panelTableau_ = new BuPanel();
  
  /**
   * panel d'affichage du tableau
   */
  BuPanel panelTableauAffichage_ = new BuPanel();

  /**
   * panel de gestion des boutons
   */
  BuPanel controlPanel_ = new BuPanel();
  
  /**
   * panel de gestion des boutons du tableau des distributions
   */
  BuPanel controlPanelTableauDistrib_ = new BuPanel();

  /**
   * panel de gestion des boutons du camembert
   */
  BuPanel controlPanelCamembert_ = new BuPanel();
  
  /**
   * Panel de gestion des boutons des courbes
   */
  BuPanel controlPanelCourbes_ = new BuPanel();

  /**
   * panel de gestion des boutons du graphe de distribution.
   */
  BuPanel controlPanelGrapheDistrib_ = new BuPanel();
  
  /**
   * panel de gestion des repr�sentations graphiques
   */
  BuPanel panelGraph_ = new BuPanel();
  
  /**
   * panel de gestion des repr�sentations graphiques
   */
  BuPanel panelResult_ = new BuPanel();

  /**
   * panel de gestion des courbes
   */
  BuPanel panelCourbe_ = new BuPanel();

  /**
   * panels de gestion des distributions
   */
  BuPanel panelTableauDistrib_ = new BuPanel();
  BuPanel panelGrapheDistrib_ = new BuPanel(new BorderLayout());

  /**
   * combolist qui permet de selectionenr les lignes deu tableau a etre affich�es:
   */
  JComboBox listeNavires_ = new JComboBox();
  JComboBox listeNaviresCamembert_=new JComboBox();
  JComboBox listeNaviresDistrib_=new JComboBox();
  /**
   * buoton de generation des resultats
   */
  private final BuButton exportationExcel_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_generer"), "Exporter");
  final BuButton exportationGraphe_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_generer"), "Exporter");
  private final BuButton exportationTableauDistrib_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_generer"), "Exporter");
  private final BuButton exportationGrapheDistrib_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_generer"), "Exporter");
  
  private final BuButton quitter_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_quitter"), "Quitter");
  private final BuButton lancerRecherche_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_oui"), "Rechercher");
  private final BuButton rafraichirSeuil_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_rafraichir"), "");
  private final BuButton rafraichirSeuilDistrib_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_rafraichir"), "");

  Border raisedBevel_ = BorderFactory.createRaisedBevelBorder();
  Border loweredBevel_ = BorderFactory.createLoweredBevelBorder();
  Border compound_ = BorderFactory.createCompoundBorder(raisedBevel_, loweredBevel_);
  Border bordnormal_ = BorderFactory.createEtchedBorder();
  /**
   * donnees de la simulation.
   */
   Sinavi3DataSimulation donnees_;

  /**
   * Constructeur de la sous fenetre de gestion des resultats.
   */
  public Sinavi3ResultatsAttenteTrajet(final Sinavi3DataSimulation _donnees) {
    super("Attentes par trajet", true, true, true, true);

    // recuperation des donn�es de la simulation
    donnees_ = _donnees;

    // premier calcul par defaut execut�: entre le bief 0 et le bief 0 dans le sens aval
    Sinavi3AlgorithmeAttentesGenerales.calculTrajetDepuisListeTrajet(donnees_, 0, 0, 0, 0, 0);

    setSize(820, 600);
    setBorder(Sinavi3Bordures.compound_);
    this.getContentPane().setLayout(new /* BorderLayout() */GridLayout(1, 1));
    this.panelPrincipal_.setLayout(new BorderLayout());
    this.getContentPane().add(this.panelPrincipal_/* ,BorderLayout.CENTER */);
    this.panelPrincipal_.add(this.selectionPanel_, BorderLayout.NORTH);
    this.panelPrincipal_.add(this.panelPrincipalAffichage_, BorderLayout.CENTER);
    panelPrincipalAffichage_.addTab("R�sultats de synth�se (extremums, moyennes)", this.panelPrincipalClassique_);
    panelPrincipalAffichage_.addTab("Distribution des attentes", this.panelPrincipalDistrib_);
    
    this.panelPrincipal_.add(panelQuitter_, BorderLayout.SOUTH);
    panelQuitter_.add(quitter_);
    
    this.panelPrincipalClassique_.setLayout(new BorderLayout());
    this.panelPrincipalClassique_.add(this.optionPanelTableau_, BorderLayout.WEST);
    this.panelPrincipalClassique_.add(panelPrincipalAffichageClassique_, BorderLayout.CENTER);
    panelPrincipalAffichageClassique_.addTab("Tableau", FudaaResource.FUDAA.getIcon("crystal_arbre"), panelTableau_);
    panelPrincipalAffichageClassique_.addTab("R�partition par type d'attente", FudaaResource.FUDAA.getIcon("crystal_graphe"), panelCamembert_);
    panelPrincipalAffichageClassique_.addTab("Exploitation graphique", FudaaResource.FUDAA.getIcon("crystal_graphe"), panelGraph_);
    
    this.panelPrincipalDistrib_.setLayout(new BorderLayout());
    this.panelPrincipalDistrib_.add(this.optionPanelDistrib_, BorderLayout.WEST);
    this.panelPrincipalDistrib_.add(panelPrincipalAffichageDistrib_, BorderLayout.CENTER);
    panelPrincipalAffichageDistrib_.addTab("Tableau", FudaaResource.FUDAA.getIcon("crystal_arbre"), panelGestionTableauDistrib_);
    panelPrincipalAffichageDistrib_.addTab("Graphe", FudaaResource.FUDAA.getIcon("crystal_graphe"), panelGrapheDistrib_);

    tableauChoixNavires_ = new JCheckBox[this.donnees_.listeBateaux_.listeNavires_.size()];
    for (int i = 0; i < this.donnees_.listeBateaux_.listeNavires_.size(); i++) {
      this.tableauChoixNavires_[i] = new JCheckBox(this.donnees_.listeBateaux_.retournerNavire(i).nom, true);
      this.tableauChoixNavires_[i].addActionListener(this);
    }
    
    tableauChoixNavires2_ = new JCheckBox[this.donnees_.listeBateaux_.listeNavires_.size()];
    for (int i = 0; i < this.donnees_.listeBateaux_.listeNavires_.size(); i++) {
      this.tableauChoixNavires2_[i] = new JCheckBox(this.donnees_.listeBateaux_.retournerNavire(i).nom, true);
      this.tableauChoixNavires2_[i].addActionListener(this);
    }
    
    tableauChoixNaviresDistrib_ = new JCheckBox[this.donnees_.listeBateaux_.listeNavires_.size()];
    for (int i = 0; i < this.donnees_.listeBateaux_.listeNavires_.size(); i++) {
      this.tableauChoixNaviresDistrib_[i] = new JCheckBox(this.donnees_.listeBateaux_.retournerNavire(i).nom, true);
      this.tableauChoixNaviresDistrib_[i].addActionListener(this);
    }
    
    /*******************************************************************************************************************
     * gestion du panel de selection
     ******************************************************************************************************************/
    this.selectionPanel_.setLayout(new GridLayout(2, 1));

    selectionPanel1_ = new BuPanel(new FlowLayout(FlowLayout.LEFT));
    selectionPanel1_.add(new JLabel("Attentes � cumuler entre l'entr�e de l'�l�ment"));
    selectionPanel1_.add(this.listeTypesDepart_);
    selectionPanel1_.add(this.listeElementDepart_);
    selectionPanel1_.add(new JLabel(" et la sortie de l'�l�ment"));
    selectionPanel1_.add(this.listeTypesArrivee_);
    selectionPanel1_.add(this.listeElementArrivee_);
    this.selectionPanel_.add(selectionPanel1_);

    final BuPanel selectionPanel2 = new BuPanel(new FlowLayout(FlowLayout.LEFT));
    selectionPanel2.add(new JLabel("Sens de parcours:"));
    selectionPanel2.add(this.sens_);
    selectionPanel2.add(lancerRecherche_);

    final TitledBorder bordurea = BorderFactory.createTitledBorder(BorderFactory
            .createEtchedBorder(EtchedBorder.LOWERED), "D�finition de la recherche");
    this.selectionPanel_.setBorder(bordurea);
    
    this.selectionPanel_.add(selectionPanel2);

    final ActionListener _remplissageElement = new ActionListener() {
      public void actionPerformed(ActionEvent _e) {

        int selection = 0;
        if (_e.getSource() == listeTypesDepart_) {
          selection = listeTypesDepart_.getSelectedIndex();
        } else {
          selection = listeTypesArrivee_.getSelectedIndex();
        }

        switch (selection) {
        case 1:
          if (_e.getSource() == listeTypesDepart_) {
            listeElementDepart_.removeAllItems();
            for (int i = 0; i < donnees_.listeEcluse_.listeEcluses_.size(); i++) {
              listeElementDepart_.addItem(donnees_.listeEcluse_.retournerEcluse(i).nom_);
            }
            listeElementDepart_.validate();
          } else {
            listeElementArrivee_.removeAllItems();
            for (int i = 0; i < donnees_.listeEcluse_.listeEcluses_.size(); i++) {
              listeElementArrivee_.addItem(donnees_.listeEcluse_.retournerEcluse(i).nom_);
            }
            listeElementArrivee_.validate();
          }
          break;
        case 0:
          if (_e.getSource() == listeTypesDepart_) {
            listeElementDepart_.removeAllItems();
            for (int i = 0; i < donnees_.listeBief_.listeBiefs_.size(); i++) {
              listeElementDepart_.addItem(donnees_.listeBief_.retournerBief(i).nom_);
            }
            listeElementDepart_.validate();
          } else {
            listeElementArrivee_.removeAllItems();
            for (int i = 0; i < donnees_.listeBief_.listeBiefs_.size(); i++) {
              listeElementArrivee_.addItem(donnees_.listeBief_.retournerBief(i).nom_);
            }
            listeElementArrivee_.validate();
          }
          break;
        default:
          break;
        }

      }
    };
    this.listeTypesDepart_.addActionListener(_remplissageElement);
    this.listeTypesArrivee_.addActionListener(_remplissageElement);
    this.listeTypesDepart_.setSelectedIndex(0);
    this.listeElementDepart_.setSelectedIndex(0);
    this.listeTypesArrivee_.setSelectedIndex(0);
    this.listeElementArrivee_.setSelectedIndex(0);
 
    this.lancerRecherche_.addActionListener(this);
    
    /*******************************************************************************************************************
     * gestion du panel des options pour les r�sultats de synth�se
     ******************************************************************************************************************/

    BuGridLayout lo=new BuGridLayout(1,5,0,true,false,false,false);
    final BuPanel panoption = new BuPanel(lo);
    final BuScrollPane panoptionGestionScroll = new BuScrollPane(panoption);
    this.optionPanelTableau_.add(panoptionGestionScroll);

    final Box bVert = Box.createVerticalBox();
    bVert.add(new JLabel(""));
    final TitledBorder bordure1 = BorderFactory.createTitledBorder(BorderFactory
        .createEtchedBorder(EtchedBorder.LOWERED), "Types d'attentes");
    bVert.setBorder(bordure1);
    panoption.add(bVert);

    bVert.add(this.choixSecurite_);
    bVert.add(this.choixAcces_);
    bVert.add(this.choixOccupation_);
    bVert.add(this.choixPannes_);
    bVert.add(this.choixTotalAttente_);

    final Box bVert2 = Box.createVerticalBox();
    bVert2.setBorder(this.bordnormal_);
    for (int i = 0; i < this.tableauChoixNavires_.length; i++) {
      bVert2.add(this.tableauChoixNavires_[i]);
    }
    final TitledBorder bordure2 = BorderFactory.createTitledBorder(BorderFactory
        .createEtchedBorder(EtchedBorder.LOWERED), "Cat�gories");
    bVert2.setBorder(bordure2);
    panoption.add(bVert2);

    this.choixAcces_.addActionListener(this);
    this.choixSecurite_.addActionListener(this);
    this.choixNbNavires_.addActionListener(this);
    this.choixTotalAttente_.addActionListener(this);
    this.choixOccupation_.addActionListener(this);
    this.choixPannes_.addActionListener(this);
    
    /*******************************************************************************************************************
     * gestion du panel des options graphiques
     ******************************************************************************************************************/

    final BuPanel panoptionGraph = new BuPanel(lo);
    final BuScrollPane panoptionGraphScroll = new BuScrollPane(panoptionGraph);
    this.optionPanelGraph_.add(panoptionGraphScroll);
    
    final Box bVertgraph1 = Box.createVerticalBox();
    panoptionGraph.add(bVertgraph1);

    ButtonGroup bg1=new ButtonGroup();
    bg1.add(choixSecurite2_);
    bg1.add(choixAcces2_);
    bg1.add(choixOccupation2_);
    bg1.add(choixPannes2_);
    bg1.add(this.choixTotalAttente2_);
    bg1.add(choixAcces2_);
    
    bVertgraph1.add(new JLabel(""));
    final TitledBorder borduregraph1 = BorderFactory.createTitledBorder(BorderFactory
        .createEtchedBorder(EtchedBorder.LOWERED), "Types d'attentes");
    bVertgraph1.setBorder(borduregraph1);
    bVertgraph1.add(this.choixSecurite2_);
    bVertgraph1.add(this.choixAcces2_);
    bVertgraph1.add(this.choixOccupation2_);
    bVertgraph1.add(this.choixPannes2_);
    bVertgraph1.add(this.choixTotalAttente2_);

    final Box bVertgraph2 = Box.createVerticalBox();
    panoptionGraph.add(bVertgraph2);
    bVertgraph2.setBorder(this.bordnormal_);
    for (int i = 0; i < this.tableauChoixNavires2_.length; i++) {
      bVertgraph2.add(this.tableauChoixNavires2_[i]);
    }
    final TitledBorder borduregraph2 = BorderFactory.createTitledBorder(BorderFactory
        .createEtchedBorder(EtchedBorder.LOWERED), "Cat�gories");
    bVertgraph2.setBorder(borduregraph2);

    // listener des checkbox de choix des options d affichage
    this.choixTypeGraphe_.addActionListener(this);
    this.choixAcces2_.addActionListener(this);
    this.choixSecurite2_.addActionListener(this);
    this.choixTotalAttente2_.addActionListener(this);
    this.choixOccupation2_.addActionListener(this);
    this.choixPannes2_.addActionListener(this);
    
    /*******************************************************************************************************************
     * gestion du panel des options relatives aux distributions
     ******************************************************************************************************************/
    
    final BuPanel panoptionDistrib = new BuPanel(lo);
    final BuScrollPane panoptionDistribGestionScroll = new BuScrollPane(panoptionDistrib);
    this.optionPanelDistrib_.add(panoptionDistribGestionScroll);

    ButtonGroup bgDistrib=new ButtonGroup();
    bgDistrib.add(this.choixCategorie_);
    bgDistrib.add(this.choixTypeAttente_);
    
    ButtonGroup bgDistrib1=new ButtonGroup();
    bgDistrib1.add(this.choixNombre_);
    bgDistrib1.add(this.choixPercent_);
    
    final Box bVertDistrib = Box.createVerticalBox();
    panoptionDistrib.add(bVertDistrib);
    bVertDistrib.add(this.choixCategorie_);
    bVertDistrib.add(this.choixTypeAttente_);
    
    final TitledBorder bordureDistrib = BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED), "Analyse");
    bVertDistrib.setBorder(bordureDistrib);
    
    final Box bVertDistrib1 = Box.createVerticalBox();
    panoptionDistrib.add(bVertDistrib1);
    bVertDistrib1.add(this.choixCumul_);
    bVertDistrib1.add(this.choixNombre_);
    bVertDistrib1.add(this.choixPercent_);
    
    final TitledBorder bordureDistrib1 = BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED), "Options");
        bVertDistrib1.setBorder(bordureDistrib1);
    
    final Box bVertDistrib2 = Box.createVerticalBox();
    for (int i = 0; i < this.tableauChoixNaviresDistrib_.length; i++) {
      bVertDistrib2.add(this.tableauChoixNaviresDistrib_[i]);
    }
    final TitledBorder bordureDistrib2 = BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED), "Cat�gories");
    bVertDistrib2.setBorder(bordureDistrib2);
    panoptionDistrib.add(bVertDistrib2);
    
    final Box bVertDistrib3 = Box.createVerticalBox();
    bVertDistrib3.add(this.listeNaviresDistrib_);
    final TitledBorder bordureDistrib3 = BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED), "Cat�gories");
    bVertDistrib3.setBorder(bordureDistrib3);
    
    this.listeNaviresDistrib_.addItem("Toutes");
    for (int i = 0; i < donnees_.listeBateaux_.listeNavires_.size(); i++) {
    	this.listeNaviresDistrib_.addItem("" + donnees_.listeBateaux_.retournerNavire(i).nom);
    }
    
    ActionListener typeAnalyse = new ActionListener() {
    	public void actionPerformed(final ActionEvent _e) {
    		if (choixTypeAttente_.isSelected()) {
    			panoptionDistrib.remove(bVertDistrib2);
        		panoptionDistrib.add(bVertDistrib3);
    		} else {
    			panoptionDistrib.remove(bVertDistrib3);
    			panoptionDistrib.add(bVertDistrib2);
    		}
    		affichageTableauDistribution();
    		affichageGrapheDistrib(true);
    	}
    };
    
    this.choixTypeAttente_.addActionListener(typeAnalyse);
    this.choixCategorie_.addActionListener(typeAnalyse);
    
    this.listeNaviresDistrib_.addActionListener(this);
    this.choixCumul_.addActionListener(this);
    this.choixPercent_.addActionListener(this);
    this.choixNombre_.addActionListener(this);
    
    /*******************************************************************************************************************
     * gestion du panel courbes panelTableau_
     ******************************************************************************************************************/
    this.panelTableau_.setLayout(new BorderLayout());
    this.panelTableau_.add(this.optionPanelTableau_, BorderLayout.WEST);
    this.panelTableau_.add(this.panelGestionTableau_, BorderLayout.CENTER);
    
    /*******************************************************************************************************************
     * gestion du panel tableau panelGestionTableau_
     ******************************************************************************************************************/

    // etape 1: architecture du panel panelGestionTableau_ et du panel ascenseur qui le contiendra
    panelGestionTableau_.setLayout(new BorderLayout());
    final JScrollPane asc = new JScrollPane(this.panelTableauAffichage_);

    // ajout au centre du panel qui contiendra le tableau d affichage
    this.panelGestionTableau_.add(asc, BorderLayout.CENTER);

    // panel qui contient les differents boutons
    this.controlPanel_.add(exportationExcel_);
    this.panelGestionTableau_.add(this.controlPanel_, BorderLayout.SOUTH);
    
    final Border bordureTab = BorderFactory.createEtchedBorder();
    controlPanel_.setBorder(bordureTab);
    
    //panel du camembert
    affichageCamembert(0, false);
    this.panelCamembert_.add(camembert_,BorderLayout.CENTER);
    
    BuPanel bpc=new BuPanel(new FlowLayout(FlowLayout.LEFT));
    bpc.add(new JLabel("Cat�gorie de bateaux:"));
    bpc.add(listeNaviresCamembert_);
    this.panelCamembert_.add(bpc,BorderLayout.NORTH);
    
    //this.controlPanelCamembert_.add(quitter4_);
    this.panelCamembert_.add(this.controlPanelCamembert_, BorderLayout.SOUTH);
    
    // etape 2: remplissage du comboliste avec les noms des navires
    this.listeNavires_.addItem("Tous");
    for (int i = 0; i < donnees_.listeBateaux_.listeNavires_.size(); i++) {
      this.listeNavires_.addItem("" + donnees_.listeBateaux_.retournerNavire(i).nom);
      this.listeNaviresCamembert_.addItem("" + donnees_.listeBateaux_.retournerNavire(i).nom);
    }

    // etape 3: gestion de l affichage du tableau de donn�es
    affichageTableau(-1);

    // etape 4: listener du combolist afin de pouvoir selectionner le navire qui nous interesse
    this.listeNavires_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent _e) {
        // evenement du clic sur le bouton
        final int val = listeNavires_.getSelectedIndex();
        affichageTableau(val - 1);

      }
    });

    this.listeNaviresCamembert_.addActionListener(new ActionListener() {
        public void actionPerformed(final ActionEvent _e) {
          // evenement du clic sur le bouton
          final int val = listeNaviresCamembert_.getSelectedIndex();
          affichageCamembert(val, true);

        }
      });
    
    // bouton qui permet de generer le contenu du tableau en ficheir excel:
    this.exportationExcel_.setToolTipText("Exporte le contenu du tableau au format xls");
    exportationExcel_.addActionListener(new ActionListener() {
    	public void actionPerformed(final ActionEvent _e) {
      	  Sinavi3GestionResultats.exportTableau(Sinavi3ResultatsAttenteTrajet.this, titreTableau_, data_);
         //  Sinavi3GestionResultats.exportReport(_donnees, false,true,false,false,false,false);
        }
    });

    /*******************************************************************************************************************
     * gestion du panel courbes panelGraph_
     ******************************************************************************************************************/
    this.panelGraph_.setLayout(new BorderLayout());
    this.panelGraph_.add(this.optionPanelGraph_, BorderLayout.WEST);
    this.panelGraph_.add(this.panelGraphAffichage_, BorderLayout.CENTER);
    
    /*******************************************************************************************************************
     * gestion du panel courbes panelCourbe_
     ******************************************************************************************************************/

    final String descriptionGraphe = affichageGraphe();
    this.graphe_.setFluxDonnees(new ByteArrayInputStream(descriptionGraphe.getBytes()));
    this.panelGraphAffichage_.add(this.graphe_, BorderLayout.CENTER);

    // bouton de generation du fichier image
    exportationGraphe_.setToolTipText("Exporte le graphe au format image");
    exportationGraphe_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent _e) {
        CtuluImageExport.exportImageFor(donnees_.application_, graphe_);
      }
    });
    
    rafraichirSeuil_.setEnabled(false);
    rafraichirSeuil_.setToolTipText("Actualise la valeur du seuil graphique");

    // creation du panel des boutons des courbes:
    this.panelGraphAffichage_.add(this.controlPanelCourbes_, BorderLayout.SOUTH);

    BuPanel exportGraphe = new BuPanel();
    exportGraphe.add(exportationGraphe_);
    this.controlPanelCourbes_.add(exportationGraphe_);
    this.controlPanelCourbes_.add(new JLabel("          "));
    this.controlPanelCourbes_.add(valideSeuil_);
    this.controlPanelCourbes_.add(valSeuilHeure_);
    this.controlPanelCourbes_.add(new JLabel("h"));
    this.controlPanelCourbes_.add(valSeuilMinute_);
    this.controlPanelCourbes_.add(new JLabel("min"));
    this.controlPanelCourbes_.add(rafraichirSeuil_);
    this.controlPanelCourbes_.add(new JLabel("          "));
    this.controlPanelCourbes_.add(choixTypeGraphe_);
    
    choixTypeGraphe_.setToolTipText("Modifie le style de la repr�sentation graphique");
    
    final Border bordure = BorderFactory.createEtchedBorder();
    controlPanelCourbes_.setBorder(bordure);

    valideSeuil_.addActionListener(this);
    rafraichirSeuil_.addActionListener(this);

    /** listener des boutons quitter */
    this.quitter_.setToolTipText("Ferme la sous-fen�tre");

    final ActionListener actionQuitter = new ActionListener() {
      public void actionPerformed(ActionEvent _e) {
        Sinavi3ResultatsAttenteTrajet.this.windowClosed();
      }
    };
    
    this.quitter_.addActionListener(actionQuitter);
    
    
    /*******************************************************************************************************************
     * gestion du panel tableau panelTableauDistrib_
     ******************************************************************************************************************/

    // etape 1: architecture du panel panelGestionTableau_
    panelGestionTableauDistrib_.setLayout(new BorderLayout());

    // definition d un panel ascenceur pour stocer le tableau:
    final JScrollPane ascDistrib = new JScrollPane(this.panelTableauDistrib_);

    // ajout au centre du panel qui contiendra le tableau d affichage
    this.panelGestionTableauDistrib_.add(ascDistrib, BorderLayout.CENTER);
    this.controlPanelTableauDistrib_.add(exportationTableauDistrib_);
    this.panelGestionTableauDistrib_.add(controlPanelTableauDistrib_, BorderLayout.SOUTH);

    // etape 3: gestion de l affichage du tableau de donn�es
    // remarque : cette m�thode sera syst�matiquement appel�e afni d'op�rer un changement:
    affichageTableauDistribution();
    
    // bouton qui permet de generer le contenu du tableau en fichier xls:
    this.exportationTableauDistrib_.setToolTipText("Exporte le contenu du tableau dans un fichier xls");
    exportationTableauDistrib_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent _e) {
    	  Sinavi3GestionResultats.exportTableau(Sinavi3ResultatsAttenteTrajet.this, titreTableauDistrib_, dataDistribAffich_);
      }
    });
    
    /*******************************************************************************************************************
     * gestion du panel courbes panelGrapheDistrib_
     ******************************************************************************************************************/
    affichageGrapheDistrib(false);
    this.panelGrapheDistrib_.add(grapheDistrib_,BorderLayout.CENTER);
    this.panelGrapheDistrib_.add(this.controlPanelGrapheDistrib_, BorderLayout.SOUTH);
    
    rafraichirSeuilDistrib_.setEnabled(false);
    rafraichirSeuilDistrib_.setToolTipText("Actualise la valeur du seuil graphique");
    
    BuPanel exportDistrib = new BuPanel();
    exportDistrib.add(exportationGrapheDistrib_);
    this.controlPanelGrapheDistrib_.add(exportationGrapheDistrib_);
    this.controlPanelGrapheDistrib_.add(new JLabel("          "));
    this.controlPanelGrapheDistrib_.add(valideSeuilDistrib_);
    this.controlPanelGrapheDistrib_.add(valSeuilDistribHeure_);
    this.controlPanelGrapheDistrib_.add(new JLabel("h"));
    this.controlPanelGrapheDistrib_.add(valSeuilDistribMinute_);
    this.controlPanelGrapheDistrib_.add(new JLabel("min"));
    this.controlPanelGrapheDistrib_.add(rafraichirSeuilDistrib_);
    
    final Border bordure3 = BorderFactory.createEtchedBorder();
    controlPanelGrapheDistrib_.setBorder(bordure3);
    
    valideSeuilDistrib_.addActionListener(this);
    rafraichirSeuilDistrib_.addActionListener(this);
    
    this.exportationGrapheDistrib_.setToolTipText("Exporte le graphe au format image");
    exportationGrapheDistrib_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent _e) {
    	  try {
			grapheDistrib_.doSaveAs();
		} catch (IOException e) {
			FuLog.error(e);
		}
      }
    });

  }
  
  
  public static Object[] computeDataAverage(Sinavi3DataSimulation data, int element1, int typeElement1, int element2, int typeElement2, int sens) {
  Object[][] result =  computeData(data,   element1,  typeElement1,  element2,  typeElement2,  sens);
  return result[result.length-1];
   
}
  
public static Object[][] computeData(Sinavi3DataSimulation data, int element1, int typeElement1, int element2, int typeElement2, 
        int sens) {   
    
  Sinavi3AlgorithmeAttentesGenerales.calculTrajetDepuisListeTrajet(data, typeElement1,element1, typeElement2,element2, sens);
  return computeData(data, -1,null,-1, true,true,true,true,true,true);
   
}

   public static Object[][] computeData(Sinavi3DataSimulation data, int val, JCheckBox[] tableauChoixNavires_, int nbColonnes, boolean choixSecurite, boolean choixAcces,
          boolean choixOccupation,boolean choixPannes, boolean choixTotalAttente, boolean choixNbNavires ) {
       if(nbColonnes == -1) {
           String[] titreTableau_ = { "Cat�gorie", "Nombre de bateaux", "Att. s�curit�: total", "Att. s�curit�: moyenne", "Att. s�curit�: nb. bateaux", "Att. s�curit�: % bateaux", "Att. s�curit�: moy./flotte", "Att. acc�s: total", "Att. acc�s: moyenne",
      "Att. acc�s: nb. bateaux", "Att. acc�s: % bateaux", "Att. acc�s: moy./flotte", "Att. occup.: total", "Att. occup.: moyenne", "Att. occup.: nb. bateaux", "Att. occup.: % bateaux", "Att. occup.: moy./flotte",
      "Att. indisp.: total", "Att. indisp.: moyenne", "Att. indisp.: nb. bateaux",  "Att. indisp.: % bateaux","Att. indisp.: moy./flotte", "Attente totale", "Attente moyenne",
      "Nb. bateaux ayant attendu", "% bateaux ayant attendu", "Attente moyenne sur flotte" };
           nbColonnes = titreTableau_.length;
       }
       Object[][] data_ = new Object[data.listeBateaux_.listeNavires_.size()+3][nbColonnes];
       

    int totalSecurite=0;
    int nbTotalBateauxSecurite=0;
    int nbBateauxSecu =0;
    int totalAcces=0;
    int nbTotalBateauxAcces=0;
    int nbBateauxAcces =0;
    int totalOccupation=0;
    int nbTotalBateauxOccupation=0;
    int nbBateauxOccupation =0;
    int totalIndisponibilite=0;
    int nbTotalBateauxPannes=0;
    int nbBateauxPannes =0;
    int totaltotal=0;
    int nbTotalBateauxTotalAttente=0;
    int nbBateauxTotalAtente =0;
    
    int indiceTbaleau = 0;
    if (val < 0) {

      for (int i = 0; i < data.listeBateaux_.listeNavires_.size(); i++) {
        if (tableauChoixNavires_ == null ||tableauChoixNavires_[i].isSelected()) {
          data_[indiceTbaleau][0] = data.listeBateaux_.retournerNavire(i).nom;

          // ecriture des donn�es calcul�es pour les dur�es de parcours
          // si les cases correspondantes ont �t� coch�es:
          int indiceColonne = 1;
          if (choixNbNavires) {
            data_[indiceTbaleau][indiceColonne++] = ""
                + FonctionsSimu.diviserSimu(data.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[i].nombreNaviresTotal);          }

         
          if (choixSecurite) {
            if (data.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[i].attenteSecuTotale != 0) {
              // attente totale
              data_[indiceTbaleau][indiceColonne++] = ""
                  + Sinavi3TraduitHoraires
                      .traduitMinutesEnHeuresMinutes3((float) FonctionsSimu.diviserSimu(data.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[i].attenteSecuTotale));
              // moyenne attente sur les navires qui attendent
              data_[indiceTbaleau][indiceColonne++] = ""
                  + Sinavi3TraduitHoraires
                      .traduitMinutesEnHeuresMinutes3((float) (data.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[i].attenteSecuTotale /(float) data.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[i].nbNaviresAttenteSecu));
              // nombre de navires qui attendent
              nbBateauxSecu = FonctionsSimu.diviserSimu(data.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[i].nbNaviresAttenteSecu);
              nbTotalBateauxSecurite = nbTotalBateauxSecurite + nbBateauxSecu;	
              data_[indiceTbaleau][indiceColonne++] = "" + nbBateauxSecu;
              data_[indiceTbaleau][indiceColonne++] = "" + Math.round((float) data.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[i].nbNaviresAttenteSecu
                      / data.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[i].nombreNaviresTotal * 100) + "%";

              // moyenne attentes sur la flotte
              data_[indiceTbaleau][indiceColonne++] = ""
                  + Sinavi3TraduitHoraires
                      .traduitMinutesEnHeuresMinutes3((float) (data.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[i].attenteSecuTotale /(float) data.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[i].nombreNaviresTotal));
            } else {
              indiceColonne += 4;
            }
          }

          if (choixAcces) {
            if (data.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[i].attenteAccesTotale != 0) {
              // attente totale
              data_[indiceTbaleau][indiceColonne++] = ""
                  + Sinavi3TraduitHoraires
                      .traduitMinutesEnHeuresMinutes3((float) FonctionsSimu.diviserSimu(data.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[i].attenteAccesTotale));
              // moyenne attente sur les navires qui attendent
              data_[indiceTbaleau][indiceColonne++] = ""
                  + Sinavi3TraduitHoraires
                      .traduitMinutesEnHeuresMinutes3((float) (data.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[i].attenteAccesTotale /(float) data.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[i].nbNaviresAttenteAcces));
              // nombre de navires qui attendent
              nbBateauxAcces = FonctionsSimu.diviserSimu(data.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[i].nbNaviresAttenteAcces);
              nbTotalBateauxAcces = nbTotalBateauxAcces + nbBateauxAcces;	
              data_[indiceTbaleau][indiceColonne++] = ""
                  + nbBateauxAcces;
              data_[indiceTbaleau][indiceColonne++] = 
                  + Math.round((float) data.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[i].nbNaviresAttenteAcces
                      / data.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[i].nombreNaviresTotal * 100)
                  + "%";

              // moyenne attentes sur la flotte
              data_[indiceTbaleau][indiceColonne++] = ""
                  + Sinavi3TraduitHoraires
                      .traduitMinutesEnHeuresMinutes3((float) (data.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[i].attenteAccesTotale / (float)data.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[i].nombreNaviresTotal));
            } else {
              indiceColonne += 4;
            }
          }

          if (choixOccupation) {
            if (data.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[i].attenteOccupTotale != 0) {
              // attente totale
              data_[indiceTbaleau][indiceColonne++] = ""
                  + Sinavi3TraduitHoraires
                      .traduitMinutesEnHeuresMinutes3((float) FonctionsSimu.diviserSimu(data.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[i].attenteOccupTotale));
              // moyenne attente sur les navires qui attendent
              data_[indiceTbaleau][indiceColonne++] = ""
                  + Sinavi3TraduitHoraires
                      .traduitMinutesEnHeuresMinutes3((float) (data.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[i].attenteOccupTotale / (float)data.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[i].nbNaviresAtenteOccup));
              // nombre de navires qui attendent
              nbBateauxOccupation =  FonctionsSimu.diviserSimu(data.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[i].nbNaviresAtenteOccup);
              nbTotalBateauxOccupation = nbTotalBateauxOccupation + nbBateauxOccupation;
              data_[indiceTbaleau][indiceColonne++] = ""
                  + nbBateauxOccupation;
              data_[indiceTbaleau][indiceColonne++] = "" + Math.round((float) data.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[i].nbNaviresAtenteOccup
                      / data.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[i].nombreNaviresTotal * 100)
                  + "%";

              // moyenne attentes sur la flotte
              data_[indiceTbaleau][indiceColonne++] = ""
                  + Sinavi3TraduitHoraires
                      .traduitMinutesEnHeuresMinutes3((float) (data.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[i].attenteOccupTotale / (float)data.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[i].nombreNaviresTotal));
            } else {
              indiceColonne += 4;
            }
          }

          if (choixPannes) {
            if (data.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[i].attentePanneTotale != 0) {
              // attente totale
              data_[indiceTbaleau][indiceColonne++] = ""
                  + Sinavi3TraduitHoraires
                      .traduitMinutesEnHeuresMinutes3((float)FonctionsSimu.diviserSimu( data.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[i].attentePanneTotale));
              // moyenne attente sur les navires qui attendent
              data_[indiceTbaleau][indiceColonne++] = ""
                  + Sinavi3TraduitHoraires
                      .traduitMinutesEnHeuresMinutes3((float) (data.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[i].attentePanneTotale / (float)data.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[i].nbNaviresAttentePanne));
              // nombre de navires qui attendent
              nbBateauxPannes = FonctionsSimu.diviserSimu(data.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[i].nbNaviresAttentePanne);
              nbTotalBateauxPannes = nbTotalBateauxPannes + nbBateauxPannes;
              data_[indiceTbaleau][indiceColonne++] = "" + nbBateauxPannes;
              data_[indiceTbaleau][indiceColonne++] = ""
                  + Math.round((float) data.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[i].nbNaviresAttentePanne
                      / data.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[i].nombreNaviresTotal * 100)
                  + "%";

              // moyenne attentes sur la flotte
              System.out.println("moyenen indispo sur flotte avant trans en h.min: "+((float) (data.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[i].attentePanneTotale) / (float)data.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[i].nombreNaviresTotal));
              
              data_[indiceTbaleau][indiceColonne++] = ""
                  + Sinavi3TraduitHoraires
                      .traduitMinutesEnHeuresMinutes3(((float) (data.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[i].attentePanneTotale) / (float)data.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[i].nombreNaviresTotal));
            } else {
              indiceColonne += 4;
            }
          }

          if (choixTotalAttente) {
            if (data.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[i].attenteMegaTotale != 0) {
              // attente totale
              data_[indiceTbaleau][indiceColonne++] = ""
                  + Sinavi3TraduitHoraires
                      .traduitMinutesEnHeuresMinutes3((float) FonctionsSimu.diviserSimu(data.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[i].attenteMegaTotale));
              // moyenne attente sur les navires qui attendent
              data_[indiceTbaleau][indiceColonne++] = ""
                  + Sinavi3TraduitHoraires
                      .traduitMinutesEnHeuresMinutes3((float) (data.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[i].attenteMegaTotale / (float)data.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[i].nbNaviresAttenteTotale));
              // nombre de navires qui attendent
              nbBateauxTotalAtente = FonctionsSimu.diviserSimu(data.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[i].nbNaviresAttenteTotale);
              nbTotalBateauxTotalAttente = nbTotalBateauxTotalAttente + nbBateauxTotalAtente;
              
              data_[indiceTbaleau][indiceColonne++] = "" + nbBateauxTotalAtente;
              
              data_[indiceTbaleau][indiceColonne++] = ""
                          + Math.round((float) data.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[i].nbNaviresAttenteTotale
                      / data.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[i].nombreNaviresTotal * 100)
                  + "%";

              // moyenne attentes sur la flotte
              data_[indiceTbaleau][indiceColonne++] = ""
                  + Sinavi3TraduitHoraires
                      .traduitMinutesEnHeuresMinutes3((float) (data.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[i].attenteMegaTotale / (float)data.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[i].nombreNaviresTotal));
            } else {
              indiceColonne += 4;
            }
          }
          totalAcces+=FonctionsSimu.diviserSimu(data.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[i].attenteAccesTotale);
          totalSecurite+=FonctionsSimu.diviserSimu(data.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[i].attenteSecuTotale);
          totalOccupation+=FonctionsSimu.diviserSimu(data.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[i].attenteOccupTotale);
          totalIndisponibilite+=FonctionsSimu.diviserSimu(data.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[i].attentePanneTotale);
          totaltotal+=FonctionsSimu.diviserSimu(data.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[i].attenteMegaTotale);
          
          indiceTbaleau++;

        }// fin du if si la cat�gorie a �t� selectionn�e pour etre affich�e
      }

      int cl=2;
      data_[++indiceTbaleau][0]="TOTAL: "+(indiceTbaleau-1)+" cat�gories affich�es";
      
      if (choixSecurite) {
          data_[indiceTbaleau][cl]="Total Securite";
          data_[indiceTbaleau+1][cl]=Sinavi3TraduitHoraires.traduitMinutesEnHeuresMinutes3(totalSecurite);	
          cl+=2;
          data_[indiceTbaleau][cl]="Total Navires S�curit�";
          data_[indiceTbaleau+1][cl]= ""+nbTotalBateauxSecurite;
          cl+=3;
          }
      if (choixAcces) {
      data_[indiceTbaleau][cl]="Total Acc�s";
      data_[indiceTbaleau+1][cl]=Sinavi3TraduitHoraires.traduitMinutesEnHeuresMinutes3(totalAcces);
      cl+=2;
      data_[indiceTbaleau][cl]="Total Navires Acc�s";
      data_[indiceTbaleau+1][cl]= ""+nbTotalBateauxAcces;
      cl+=3;
      }
     
      if (choixOccupation) {
          data_[indiceTbaleau][cl]="Total Occupation";
          data_[indiceTbaleau+1][cl]=Sinavi3TraduitHoraires.traduitMinutesEnHeuresMinutes3(totalOccupation);	
          cl+=2;
          data_[indiceTbaleau][cl]="Total Navires Occupation";
          data_[indiceTbaleau+1][cl]= ""+nbTotalBateauxOccupation;
          cl+=3;
          }
      if (choixPannes) {
          data_[indiceTbaleau][cl]="Total Pannes";
          data_[indiceTbaleau+1][cl]=Sinavi3TraduitHoraires.traduitMinutesEnHeuresMinutes3(totalIndisponibilite);	
          cl+=2;
          data_[indiceTbaleau][cl]="Total Navires Pannes";
          data_[indiceTbaleau+1][cl]= ""+nbTotalBateauxPannes;
          cl+=3;
          }
      if (choixTotalAttente) {
          data_[indiceTbaleau][cl]="Total toutes attentes";
          data_[indiceTbaleau+1][cl]=Sinavi3TraduitHoraires.traduitMinutesEnHeuresMinutes3(totaltotal);	
          cl+=2;
          data_[indiceTbaleau][cl]="Total Navires toutes attentes";
          data_[indiceTbaleau+1][cl]= ""+nbTotalBateauxTotalAttente;
          }
      
    } else if (val < data.listeBateaux_.listeNavires_.size()) {
      // on affiche uniquement la ligne selectionn� par le combolist:
      data_ = new Object[1][nbColonnes];
      data_[0][0] = data.listeBateaux_.retournerNavire(val).nom;
      // on complete les don�nes par le tableau de resultats:
      // data[0][0]..........
      int indiceColonne = 1;
      if (choixNbNavires) {
        data_[0][indiceColonne++] = ""
            + FonctionsSimu.diviserSimu(data.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[val].nombreNaviresTotal);
      }

      
      if (choixSecurite) {
        // attente totale
        data_[0][indiceColonne++] = ""
            + Sinavi3TraduitHoraires
                .traduitMinutesEnHeuresMinutes3(FonctionsSimu.diviserSimu(data.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[val].attenteSecuTotale));
        // moyenne attente sur les navires qui attendent
        data_[0][indiceColonne++] = ""
            + Sinavi3TraduitHoraires
                .traduitMinutesEnHeuresMinutes3((float) (data.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[val].attenteSecuTotale / data.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[val].nbNaviresAttenteSecu));
        // nombre de navires qui attendent
        data_[0][indiceColonne++] = ""
            + FonctionsSimu.diviserSimu(data.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[val].nbNaviresAttenteSecu)
            + " ("
            + Math.round((float) data.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[val].nbNaviresAttenteSecu
                / data.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[val].nombreNaviresTotal * 100)
            + "%)";

        // moyenne attentes sur la flotte
        data_[0][indiceColonne++] = ""
            + Sinavi3TraduitHoraires
                .traduitMinutesEnHeuresMinutes3((float) (data.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[val].attenteSecuTotale / data.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[val].nombreNaviresTotal));

      }

      if (choixAcces) {
        // attente totale
        data_[0][indiceColonne++] = ""
            + Sinavi3TraduitHoraires
                .traduitMinutesEnHeuresMinutes3((float) FonctionsSimu.diviserSimu(data.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[val].attenteAccesTotale));
        // moyenne attente sur les navires qui attendent
        data_[0][indiceColonne++] = ""
            + Sinavi3TraduitHoraires
                .traduitMinutesEnHeuresMinutes3((float) (data.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[val].attenteAccesTotale / data.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[val].nbNaviresAttenteAcces));
        // nombre de navires qui attendent
        data_[0][indiceColonne++] = ""
            + FonctionsSimu.diviserSimu( data.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[val].nbNaviresAttenteAcces)
            + " ("
            + Math.round((float) data.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[val].nbNaviresAttenteAcces
                / data.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[val].nombreNaviresTotal * 100)
            + "%)";

        // moyenne attentes sur la flotte
        data_[0][indiceColonne++] = ""
            + Sinavi3TraduitHoraires
                .traduitMinutesEnHeuresMinutes3((float) (data.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[val].attenteAccesTotale / data.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[val].nombreNaviresTotal));

      }

      if (choixOccupation) {
        // attente totale
        data_[0][indiceColonne++] = ""
            + Sinavi3TraduitHoraires
                .traduitMinutesEnHeuresMinutes3((float) FonctionsSimu.diviserSimu(data.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[val].attenteOccupTotale));
        // moyenne attente sur les navires qui attendent
        data_[0][indiceColonne++] = ""
            + Sinavi3TraduitHoraires
                .traduitMinutesEnHeuresMinutes3((float) (data.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[val].attenteOccupTotale / data.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[val].nbNaviresAtenteOccup));
        // nombre de navires qui attendent
        data_[0][indiceColonne++] = ""
            + FonctionsSimu.diviserSimu(data.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[val].nbNaviresAtenteOccup)
            + " ("
            + Math.round((float) data.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[val].nbNaviresAtenteOccup
                / data.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[val].nombreNaviresTotal * 100)
            + "%)";
        // moyenne attentes sur la flotte
        data_[0][indiceColonne++] = ""
            + Sinavi3TraduitHoraires
                .traduitMinutesEnHeuresMinutes3((float) (data.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[val].attenteOccupTotale / data.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[val].nombreNaviresTotal));

      }

      if (choixPannes) {
        // attente totale
        data_[0][indiceColonne++] = ""
            + Sinavi3TraduitHoraires
                .traduitMinutesEnHeuresMinutes3((float) FonctionsSimu.diviserSimu(data.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[val].attentePanneTotale));
        // moyenne attente sur les navires qui attendent
        data_[0][indiceColonne++] = ""
            + Sinavi3TraduitHoraires
                .traduitMinutesEnHeuresMinutes3((float) (data.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[val].attentePanneTotale / data.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[val].nbNaviresAttentePanne));
        // nombre de navires qui attendent
        data_[0][indiceColonne++] = ""
            + FonctionsSimu.diviserSimu(data.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[val].nbNaviresAttentePanne)
            + " ("
            + Math.round((float) data.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[val].nbNaviresAttentePanne
                / data.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[val].nombreNaviresTotal * 100)
            + "%)";
        // moyenne attentes sur la flotte
       data_[0][indiceColonne++] = ""
            + Sinavi3TraduitHoraires
                .traduitMinutesEnHeuresMinutes3(((float) (data.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[val].attentePanneTotale) / data.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[val].nombreNaviresTotal));

      }

      if (choixTotalAttente) {
        // attente totale
        data_[0][indiceColonne++] = ""
            + Sinavi3TraduitHoraires
                .traduitMinutesEnHeuresMinutes3((float) FonctionsSimu.diviserSimu(data.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[val].attenteMegaTotale));
        // moyenne attente sur les navires qui attendent
        data_[0][indiceColonne++] = ""
            + Sinavi3TraduitHoraires
                .traduitMinutesEnHeuresMinutes3((float) (data.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[val].attenteMegaTotale / data.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[val].nbNaviresAttenteTotale));
        // nombre de navires qui attendent
        data_[0][indiceColonne++] = ""
            + FonctionsSimu.diviserSimu(data.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[val].nbNaviresAttenteTotale)
            + " ("
            + Math.round((float) data.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[val].nbNaviresAttenteTotale
                / data.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[val].nombreNaviresTotal * 100)
            + "%)";
        // moyenne attentes sur la flotte
        data_[0][indiceColonne++] = ""
            + Sinavi3TraduitHoraires
                .traduitMinutesEnHeuresMinutes3(((float) (data.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[val].attenteMegaTotale) / data.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[val].nombreNaviresTotal));

      }

    }
  
    return data_;
   }
  /**
   * Methode d'affichage du tableau remarque: cete m�thode sert aussi de rafraichissement du tableau.
   * 
   * @param val entier qui indique le num�ro de la cat�gorie de navire � afficher si ce parametre vaut -1 alorso n
   *          affiche la totalit� des navires
   */
  public void affichageTableau(final int val) {
    // affichage du tableau

    // g�n�rer la liste des donn�es � afficher dans le tableau via une matrice de type object
    // ici le nombre de colonnes est de 2 puisqu'il s'agit d'un affichage du nom de la cat�gorie et de son nombre de
    // navires
    data_ =  computeData(donnees_, val, tableauChoixNavires_, titreTableau_.length, choixSecurite_.isSelected(), choixAcces_.isSelected(), 
            choixOccupation_.isSelected(), choixPannes_.isSelected(), choixTotalAttente_.isSelected(), choixNbNavires_.isSelected());
    
    
    // etape 3: creation du tableau a partir des donn�es g�n�r�es ci dessus:
    this.tableau_ = new BuTable(data_, this.titreTableau_) {
      public boolean isCellEditable(final int _row, final int _col) {
        return false;
      }
    };

    // etape 3.5: dimensionnement des colonnes du tableau 
    for(int i=0; i<tableau_.getModel().getColumnCount();i++){
        TableColumn column = tableau_.getColumnModel().getColumn(i);
               column.setPreferredWidth(150); 
        }
    
    // etape 4: ajout sdu tableau cr�� dans l'interface
    org.fudaa.fudaa.sinavi.factory.ColumnAutoSizer.sizeColumnsToFit(tableau_);
    tableau_.revalidate();
    this.panelTableauAffichage_.removeAll();
    this.panelTableauAffichage_.setLayout(new BorderLayout());
    this.panelTableauAffichage_.add(tableau_.getTableHeader(), BorderLayout.PAGE_START);
    this.panelTableauAffichage_.add(this.tableau_, BorderLayout.CENTER);

    // mise a jour de l'affichage
    this.revalidate();
    this.updateUI();

  }
  
  /**
   * Methode d'affichage du tableau de distribution.
   */
  void affichageTableauDistribution() {
	  
	  final int nbIntervalles = this.donnees_.params_.ResultatsCompletsSimulation.nombreIntervallesDistribution;
	  
	  String[] intervalles = new String[nbIntervalles+1];
	  for (int i = 0; i < nbIntervalles; i++)
	  {
		  intervalles[i] = "" + Sinavi3TraduitHoraires.traduitMinutesEnHeuresMinutes3((this.donnees_.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.valeurIntervalleDistribution * i))
		  + " � " + Sinavi3TraduitHoraires.traduitMinutesEnHeuresMinutes3((this.donnees_.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.valeurIntervalleDistribution * (i+1)));
	  }
	  intervalles[nbIntervalles] = "TOTAL";
	  
	  if (this.choixCategorie_.isSelected()) {
		  calculTableauDistributionParCategorie();
		  dataDistrib_ = dataDistribParCategorie_;
	  }
	  else if (this.choixTypeAttente_.isSelected()) {
		  calculTableauDistributionParAttente();
		  dataDistrib_ = dataDistribParAttente_;
	  } else { return; }
  
	  // Construction du tableau que l'on souhaite afficher
	  dataDistribAffich_ = new Object[dataDistrib_[0].length][this.titreTableauDistrib_.length];
	  for (int i = 0; i < dataDistrib_[0].length; i++)
	  {
		  dataDistribAffich_[i][0] = intervalles[i];
		  for (int j = 1; j < this.titreTableauDistrib_.length; j++)
		  {
			  if (dataDistrib_[0][nbIntervalles][j-1] > 0) // On �limine colonnes de z�ros pour all�ger le tableau
			  {
	    			if (choixCumul_.isSelected())
	    			{
	    				if (choixPercent_.isSelected()) dataDistribAffich_[i][j] = "" + (int) dataDistrib_[3][i][j-1] + "%";
	    				else dataDistribAffich_[i][j] = "" + (int) dataDistrib_[2][i][j-1];
	    			}
	    			else
	    			{
	    				if (choixPercent_.isSelected()) dataDistribAffich_[i][j] = "" + (int) dataDistrib_[1][i][j-1] + "%";
	    				else dataDistribAffich_[i][j] = "" + (int) dataDistrib_[0][i][j-1];
	    			}
	    		}
	    		else dataDistribAffich_[i][j] = "";
	    	}
	    }
	    
	    //Creation du tableau a partir des donn�es g�n�r�es ci dessus:
	    this.tableauDistrib_ = new BuTable(dataDistribAffich_, this.titreTableauDistrib_) {
	      public boolean isCellEditable(final int _row, final int _col) {
	        return false;
	      }
	    };

	    // etape 4: ajout sdu tableau cr�� dans l'interface
	    tableauDistrib_.revalidate();
	    this.panelTableauDistrib_.removeAll();
	    this.panelTableauDistrib_.setLayout(new BorderLayout());
	    this.panelTableauDistrib_.add(tableauDistrib_.getTableHeader(), BorderLayout.PAGE_START);
	    this.panelTableauDistrib_.add(this.tableauDistrib_, BorderLayout.CENTER);

	    // mise a jour de l'affichage
	    this.revalidate();
	    this.updateUI();
  }
  
  /**
   * Methode de rafraichissement du tableau de distribution.
   */
  void rafraichissementTableauDistribution() {
	  // Construction du tableau que l'on souhaite afficher
	  final int nbIntervalles = this.donnees_.params_.ResultatsCompletsSimulation.nombreIntervallesDistribution;
	  final int nombreColonnes = dataDistribAffich_[0].length;
	  for (int i = 0; i < nbIntervalles+1; i++)
	  {
		  for (int j = 1; j < nombreColonnes; j++)
		  {
			  if (dataDistrib_[0][nbIntervalles][j-1] > 0) // On �limine les z�ros pour all�ger le tableau
			  {
				  if (choixCumul_.isSelected())
				  {
					  if (choixPercent_.isSelected()) dataDistribAffich_[i][j] = "" + (int) dataDistrib_[3][i][j-1] + "%";
					  else dataDistribAffich_[i][j] = "" + (int) dataDistrib_[2][i][j-1];
				  }
				  else
				  {
					  if (choixPercent_.isSelected()) dataDistribAffich_[i][j] = "" + (int) dataDistrib_[1][i][j-1] + "%";
					  else dataDistribAffich_[i][j] = "" + (int) dataDistrib_[0][i][j-1];
				  }
			  }
			  else dataDistribAffich_[i][j] = "";
		  }
	  }
	    
	  // etape 4: ajout du tableau cr�� dans l'interface
	  tableauDistrib_.revalidate();
	  this.panelTableauDistrib_.removeAll();
	  this.panelTableauDistrib_.setLayout(new BorderLayout());
	  this.panelTableauDistrib_.add(tableauDistrib_.getTableHeader(), BorderLayout.PAGE_START);
	  this.panelTableauDistrib_.add(this.tableauDistrib_, BorderLayout.CENTER);

	  // mise a jour de l'affichage
	  this.revalidate();
	  this.updateUI();
  }
  
  
  /**
   * M�thode de calcul du tableau de distribution des attentes totales par cat�gorie de bateaux.
   */
  void calculTableauDistributionParCategorie() {

	final int nbIntervalles = this.donnees_.params_.ResultatsCompletsSimulation.nombreIntervallesDistribution;

    // Objets dataDistrib : 4 tableaux : [0] non cumul� en nb, [1] non cumul� en %, [2] cumul� en nb, [3] cumul� en %
	dataDistribParCategorie_ = new double[4][nbIntervalles+1][this.donnees_.listeBateaux_.listeNavires_.size()+1];
    
	// Cr�ation des titres de lignes et de colonnes
	String[] nomsBateaux = new String[this.donnees_.listeBateaux_.listeNavires_.size()];
    int indiceBateau = 0;
    
    int value; //entier de stockage temporaire
    int[] valueCumulHorizontal = new int[nbIntervalles+1]; // Cumul sur chaque ligne
    int[] valueCumulVertical = new int[donnees_.listeBateaux_.listeNavires_.size()+2]; // Cumul sur chaque colonne
    int nbBateauxTotal = 0;
    for (int j = 0; j < donnees_.listeBateaux_.listeNavires_.size(); j++)
    {
    	if (this.tableauChoixNaviresDistrib_[j].isSelected())
    	{
   			for (int i = 0; i < nbIntervalles; i++)
   			{
   				value = this.donnees_.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[j].attenteTotaleDistrib[i];
   				valueCumulHorizontal[i] += value;
   				valueCumulVertical[indiceBateau] += value;
   				dataDistribParCategorie_[0][i][indiceBateau] = FonctionsSimu.diviserSimu(value);
   				dataDistribParCategorie_[2][i][indiceBateau] = FonctionsSimu.diviserSimu(valueCumulVertical[indiceBateau]);
   			}
   			nomsBateaux[indiceBateau] = this.donnees_.listeBateaux_.retournerNavire(j).nom;
   			nbBateauxTotal += this.donnees_.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[j].nombreNaviresTotal;
   			dataDistribParCategorie_[0][nbIntervalles][indiceBateau] = FonctionsSimu.diviserSimu(this.donnees_.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[j].nombreNaviresTotal);
   			dataDistribParCategorie_[2][nbIntervalles][indiceBateau] = FonctionsSimu.diviserSimu(this.donnees_.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[j].nombreNaviresTotal);
    		indiceBateau++;
    	}
    	else
    	{
    		for (int i = 0; i < nbIntervalles; i++)
    		{
    			value = this.donnees_.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[j].attenteTotaleDistrib[i];
    			valueCumulHorizontal[i] += value;
    		}
    		nbBateauxTotal += this.donnees_.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[j].nombreNaviresTotal;
    	}
    }
    
    final int nombreCategories = indiceBateau; // r�cup�ration de la largeur du tableau
    
    // Construction du vecteur des titres de colonnes
    this.titreTableauDistrib_ = new String[nombreCategories+2];
    this.titreTableauDistrib_[0] = "Dur�e (att. totales)";
    this.titreTableauDistrib_[nombreCategories+1] = "Flotte enti�re";
    for (int j = 0; j < nombreCategories; j++)
    {
    	this.titreTableauDistrib_[j+1] = nomsBateaux[j];
    }
    
    //Remplissage de la colonne TOTAL
    value = 0;
    int valueCumulTotal = 0;
    for (int i = 0; i < nbIntervalles; i++)
    {
    	dataDistribParCategorie_[0][i][nombreCategories] = FonctionsSimu.diviserSimu(valueCumulHorizontal[i]);
    	valueCumulTotal += valueCumulHorizontal[i];
    	dataDistribParCategorie_[2][i][nombreCategories] = FonctionsSimu.diviserSimu(valueCumulTotal);
    }
    dataDistribParCategorie_[0][nbIntervalles][nombreCategories] = FonctionsSimu.diviserSimu(nbBateauxTotal);
    dataDistribParCategorie_[2][nbIntervalles][nombreCategories] = FonctionsSimu.diviserSimu(nbBateauxTotal);
    
    // A partir de dataDistrib[0] et [2] (valeurs en nombre), on en d�duit [1] et [3] (valeurs en pourcentage)
    for (int i = 0; i < nbIntervalles+1; i++)
    {
    	for (int j = 0; j < nombreCategories+1; j++)
    	{
    		dataDistribParCategorie_[1][i][j] = Math.round(dataDistribParCategorie_[0][i][j] * 100. / dataDistribParCategorie_[0][nbIntervalles][j]);
    		dataDistribParCategorie_[3][i][j] = Math.round(dataDistribParCategorie_[2][i][j] * 100. / dataDistribParCategorie_[2][nbIntervalles][j]);
    	}
    }
    
  }
  
  
  /**
   * M�thode de calcul du tableau de distribution par type d'attente pour une cat�gorie donn�e.
   */
  void calculTableauDistributionParAttente() {

	final int nbIntervalles = this.donnees_.params_.ResultatsCompletsSimulation.nombreIntervallesDistribution;
	final int categ = this.listeNaviresDistrib_.getSelectedIndex() - 1;

    // Objets dataDistrib : 4 tableaux : [0] non cumul� en nb, [1] non cumul� en %, [2] cumul� en nb, [3] cumul� en %
	dataDistribParAttente_ = new double[4][nbIntervalles+1][5];
    
	// Cr�ation des titres de lignes et de colonnes
	String[] typesAttentes = {"Attentes d'acc�s", "Attentes d'indisponibilit�", "Attentes d'occupation", "Attentes de s�curit�", "Attentes totales"};
    String[] intervalles = new String[nbIntervalles];
    for (int i = 0; i < nbIntervalles; i++)
    {
    	intervalles[i] = "" + Sinavi3TraduitHoraires.traduitMinutesEnHeuresMinutes3((this.donnees_.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.valeurIntervalleDistribution * i))
    			+ " � " + Sinavi3TraduitHoraires.traduitMinutesEnHeuresMinutes3((this.donnees_.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.valeurIntervalleDistribution * (i+1)));
    }
    
    int[] value = new int[typesAttentes.length]; //entier de stockage temporaire
    int[] valueCumulVertical = new int[typesAttentes.length]; // Cumul sur chaque colonne
    for (int i = 0; i < nbIntervalles; i++)
    {
    	if (categ != -1) {
    		value[0] = this.donnees_.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[categ].attenteAccesDistrib[i];
        	value[1] = this.donnees_.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[categ].attentePanneDistrib[i];
        	value[2] = this.donnees_.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[categ].attenteOccupDistrib[i];
        	value[3] = this.donnees_.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[categ].attenteSecuDistrib[i];
        	value[4] = this.donnees_.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[categ].attenteTotaleDistrib[i];
    	} else {
    		value = new int[typesAttentes.length];
    		for (int j = 0; j < donnees_.listeBateaux_.listeNavires_.size(); j++) {
    			value[0] += this.donnees_.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[j].attenteAccesDistrib[i];
            	value[1] += this.donnees_.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[j].attentePanneDistrib[i];
            	value[2] += this.donnees_.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[j].attenteOccupDistrib[i];
            	value[3] += this.donnees_.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[j].attenteSecuDistrib[i];
            	value[4] += this.donnees_.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[j].attenteTotaleDistrib[i];
    		}
    	}
    	for (int k = 0; k < typesAttentes.length; k++) {
    		valueCumulVertical[k] += value[k];
    		dataDistribParAttente_[0][i][k] = FonctionsSimu.diviserSimu(value[k]);
    		dataDistribParAttente_[2][i][k] = FonctionsSimu.diviserSimu(valueCumulVertical[k]);
    	}
    	
    }
    for (int k = 0; k < typesAttentes.length; k++) {
		dataDistribParAttente_[0][nbIntervalles][k] = FonctionsSimu.diviserSimu(valueCumulVertical[k]);
		dataDistribParAttente_[2][nbIntervalles][k] = FonctionsSimu.diviserSimu(valueCumulVertical[k]);
	}
    
    // Construction du vecteur des titres de colonnes
    this.titreTableauDistrib_ = new String[typesAttentes.length + 1];
    this.titreTableauDistrib_[0] = "Dur�e";
    for (int j = 0; j < typesAttentes.length; j++)
    {
    	this.titreTableauDistrib_[j+1] = typesAttentes[j];
    }
    
    // A partir de dataDistrib[0] et [2] (valeurs en nombre), on en d�duit [1] et [3] (valeurs en pourcentage)
    for (int i = 0; i < nbIntervalles+1; i++)
    {
    	for (int j = 0; j < typesAttentes.length; j++)
    	{
    		dataDistribParAttente_[1][i][j] = Math.round(dataDistribParAttente_[0][i][j] * 100. / dataDistribParAttente_[0][nbIntervalles][j]);
    		dataDistribParAttente_[3][i][j] = Math.round(dataDistribParAttente_[2][i][j] * 100. / dataDistribParAttente_[2][nbIntervalles][j]);
    	}
    }
  
  }
  

  /**
   * Methode qui permet de d�crire le graphe � afficher.
   * 
   * @param _histo: si true, on affiche un histogramme plutot qu'un graphe classique
   * @return chaine: chaine qui contient la des cription de la chaine de caracteres.
   */
  String affichageGraphe() {
	  
	boolean echelleHeures = false;
	if (Sinavi3AlgorithmeAttentesGenerales.determineAttenteMaxTrajet(donnees_) >= 240) echelleHeures=true;
	
	boolean histo = false;
	if (this.choixTypeGraphe_.getSelectedIndex() == 1) histo = true;

	// determiner el nombre de cat�gories de navires selectionn�s
	int nbNavires = 0;
	for (int k = 0; k < this.tableauChoixNavires2_.length; k++) {
	  if (this.tableauChoixNavires2_[k].isSelected()) {
		  nbNavires++;
	  }
	}

    // variable qui permet de determiner a quel indice du navire on est car on en prends pas tous les indices de navires
    int indiceNavire = 0;

    String g = "";

    g += "graphe\n{\n";
    if (this.listeElementDepart_.getSelectedItem() == this.listeElementArrivee_.getSelectedItem()) {
    	g += "  titre \"Attentes au sein de l'�l�ment " + (String) this.listeElementDepart_.getSelectedItem();
    } else {
    	g += "  titre \"Attentes sur le trajet de " + (String) this.listeElementDepart_.getSelectedItem() + " � "
        + (String) this.listeElementArrivee_.getSelectedItem();
    }
    if (this.sens_.getSelectedIndex() == 0) {
      g += " dans le sens avalant";
    } else if (this.sens_.getSelectedIndex() == 1) {
      g += " dans le sens montant";
    } else {
      g += " dans les deux sens cumul�s";
    }
    g += " \"\n";

    if (this.choixSecurite2_.isSelected()) {
      g += "  sous-titre \"Attentes de s�curit�\"\n";
    } else if (this.choixAcces2_.isSelected()) {
      g += "  sous-titre \"Attentes d'acc�s\"\n";
    } else if (this.choixOccupation2_.isSelected()) {
      g += "  sous-titre \"Attentes d'occupation\"\n";
    } else if (this.choixPannes2_.isSelected()) {
      g += "  sous-titre \"Attentes d'indisponibilit�\"\n";
    } else if (this.choixTotalAttente2_.isSelected()) {
      g += "  sous-titre \"Attentes totales\"\n";
    }

    g += "  animation non\n";
    g += "  legende " + "oui" + "\n";
    
    g += " marges\n {\n";
    g += " gauche 100\n"; g += " droite 100\n"; g += " haut 50\n"; g += " bas 30\n }\n";

    g += "  axe\n  {\n"; // abscisses
    g += "    titre \"Cat�gorie" + "\"\n";
    g += "    orientation " + "horizontal" + "\n";
    g += "    graduations non\n";
    g += "    minimum " + 0 + "\n";
    g += "    maximum " + (nbNavires + 2)// (this.donnees_.categoriesNavires_.listeNavires_.size()+3)
        + "\n";
    g += "  }\n";

    g += "  axe\n  {\n"; // Ordonn�es
    g += "    titre \"Dur�e" + "\"\n";
    g += "    unite \"";
    if (echelleHeures) g += "heures"; else g += "minutes";
    g += "\"\n";
    g += "    orientation " + "vertical" + "\n";
    g += "    graduations oui\n";
    g += "    minimum " + 0 + "\n";
    g += "    maximum ";
    if (echelleHeures) g+= (Sinavi3AlgorithmeAttentesGenerales.determineAttenteMaxTrajet(donnees_))/60;
    else g+= (Sinavi3AlgorithmeAttentesGenerales.determineAttenteMaxTrajet(donnees_));
        /*g+= /*Sinavi3TraduitHoraires.traduitMinutesEnHeuresMinutes(((float) Sinavi3AlgorithmeAttentesGenerales
            .determineAttenteMaxTrajet(donnees_)))/60
        // ********************ATTTTTTTTTTTTTEEEEEEENNNNNNNNNNTTTTTTTTION A DEFINIR DES QU ON CONNAIS LE TABELAU/ ON
        // DETERMINE LE MAX
        + "\n";*/
    g += "\n";
    g += "  }\n";
    /*
     * for (int i= 0; i < this.donnees_.categoriesNavires_.listeNavires_.size(); i++) { //A RESERVER POUR GERER
     * PLUSIEURS COURBES
     */

    // ******************************debut histo max************************************************
    g += "  courbe\n  {\n";
    g += "    titre \"";

   if (this.choixSecurite2_.isSelected()) {
      g += "attentes de s�curit� maximales";
    } else if (this.choixAcces2_.isSelected()) {
      g += "attentes d'acc�s maximales";
    } else if (this.choixOccupation2_.isSelected()) {
      g += "attentes d'occupation maximales";
    } else if (this.choixPannes2_.isSelected()) {
      g += "attentes d'indisponibilit� maximales";
    } else if (this.choixTotalAttente2_.isSelected()) {
      g += "attentes totales maximales";
    }
    // g += "dur�es maxi";
    /*
     * + "/" + choixString(choix, i, 1) + "/" + choixString(choix, i, 2)
     */
    g += "\"\n";
    g += "    type ";
    if (histo) g += "histogramme"; else g += "courbe";
    g += "\n";
    g += "    aspect\n {\n";
    g += "contour.largeur 1 \n";
    g += "surface.couleur  	BB0000 \n";
    g += "texte.couleur 000000 \n";
    g += "contour.couleur ";
    if (histo) g += "000000"; else g += "BB0000";
    g += " \n";
    g += "    }\n";
    g += "    valeurs\n    {\n";
    /*
     * final Vector coordX= (Vector)_courbes.get(i * 2); final Vector coordY= (Vector)_courbes.get(i * 2 + 1); for (int
     * n= 0;(n < coordX.size()) || (n < coordY.size()); n++) {
     */
    indiceNavire = 0;
    for (int n = 0; n < this.donnees_.listeBateaux_.listeNavires_.size(); n++) {
      if (this.tableauChoixNavires2_[n].isSelected()) {
        g += (indiceNavire + 1)// numero de la cat�gorie
            + " ";
        indiceNavire++;
        if (this.choixSecurite2_.isSelected()) {
        	if (echelleHeures) g += ((float) this.donnees_.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[n].attenteSecuMaxi)/60;
        	else g += ((float) this.donnees_.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[n].attenteSecuMaxi);
        } else if (this.choixAcces2_.isSelected()) {
        	if (echelleHeures) g += ((float) this.donnees_.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[n].attenteAccesMaxi)/60;
        	else g += ((float) this.donnees_.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[n].attenteAccesMaxi);
        } else if (this.choixOccupation2_.isSelected()) {
        	if (echelleHeures) g += ((float) this.donnees_.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[n].attenteOccupMaxi)/60;
        	else g += ((float) this.donnees_.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[n].attenteOccupMaxi);
        } else if (this.choixPannes2_.isSelected()) {
        	if (echelleHeures) g += ((float) this.donnees_.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[n].attentePanneMaxi)/60;
        	else g += ((float) this.donnees_.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[n].attentePanneMaxi);
        } else if (this.choixTotalAttente2_.isSelected()) {
        	if (echelleHeures) g += ((float) this.donnees_.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[n].attenteTotaleMaxi)/60;
        	else g += ((float) this.donnees_.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[n].attenteTotaleMaxi);
        }

        g += "\n etiquette  \n \"" + this.donnees_.listeBateaux_.retournerNavire(n).nom + "\" \n"/* + */
            + "\n";
      }
    }// din du pour remplissage des coordonn�es
    g += "    }\n";
    g += "  }\n";

    // //******************************fin histo max************************************************

    // ******************************debut histo moy************************************************

    g += "  courbe\n  {\n";
    g += "    titre \"";

     if (this.choixSecurite2_.isSelected()) {
      g += "attentes de s�curit� moyennes";
    } else if (this.choixAcces2_.isSelected()) {
      g += "attentes d'acc�s moyennes";
    } else if (this.choixOccupation2_.isSelected()) {
      g += "attentes d'occupation moyennes";
    } else if (this.choixPannes2_.isSelected()) {
      g += "attentes d'indisponibilit� moyennes";
    } else if (this.choixTotalAttente2_.isSelected()) {
      g += "attentes totales moyennes";
    }

    /*
     * + "/" + choixString(choix, i, 1) + "/" + choixString(choix, i, 2)
     */
    g += "\"\n";
    g += "    type ";
    if (histo) g += "histogramme"; else g += "courbe";
    g += "\n";
    g += "    aspect\n {\n";
    g += "contour.largeur 1 \n";
    g += "surface.couleur BB8800 \n";
    g += "texte.couleur 000000 \n";
    g += "contour.couleur ";
    if (histo) g += "000000"; else g += "BB8800";
    g += " \n";
    g += "    }\n";
    g += "    valeurs\n    {\n";
    /*
     * final Vector coordX= (Vector)_courbes.get(i * 2); final Vector coordY= (Vector)_courbes.get(i * 2 + 1); for (int
     * n= 0;(n < coordX.size()) || (n < coordY.size()); n++) {
     */
    indiceNavire = 0;
    for (int n = 0; n < this.donnees_.listeBateaux_.listeNavires_.size(); n++) {
      if (this.tableauChoixNavires2_[n].isSelected()) {
        g += (indiceNavire + 1)// numero de la cat�gorie
            + " ";
        indiceNavire++;
        if (this.choixSecurite2_.isSelected()) {
        	if (this.donnees_.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[n].nbNaviresAttenteSecu == 0) g+=0;
        	else {
        		if (echelleHeures) g +=((float) (this.donnees_.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[n].attenteSecuTotale / this.donnees_.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[n].nbNaviresAttenteSecu))/60;
        		else g +=((float) (this.donnees_.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[n].attenteSecuTotale / this.donnees_.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[n].nbNaviresAttenteSecu));
        	}
        } else if (this.choixAcces2_.isSelected()) {
        	if (this.donnees_.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[n].nbNaviresAttenteAcces == 0) g+=0;
        	else {
        		if (echelleHeures) g += ((float) (this.donnees_.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[n].attenteAccesTotale / this.donnees_.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[n].nbNaviresAttenteAcces))/60;
        		else g += ((float) (this.donnees_.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[n].attenteAccesTotale / this.donnees_.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[n].nbNaviresAttenteAcces));
        	}
        } else if (this.choixOccupation2_.isSelected()) {
        	if (this.donnees_.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[n].nbNaviresAtenteOccup == 0) g+=0;
        	else {
        		if (echelleHeures) g +=((float) (this.donnees_.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[n].attenteOccupTotale / this.donnees_.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[n].nbNaviresAtenteOccup))/60;
        		else g +=((float) (this.donnees_.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[n].attenteOccupTotale / this.donnees_.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[n].nbNaviresAtenteOccup));
        	}
        } else if (this.choixPannes2_.isSelected()) {
        	if (this.donnees_.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[n].nbNaviresAttentePanne == 0) g+=0;
        	else {
        		if (echelleHeures) g += ((float) (this.donnees_.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[n].attentePanneTotale / this.donnees_.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[n].nbNaviresAttentePanne))/60;
        		else g += ((float) (this.donnees_.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[n].attentePanneTotale / this.donnees_.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[n].nbNaviresAttentePanne));
        	}
        } else if (this.choixTotalAttente2_.isSelected()) {
        	if (this.donnees_.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[n].nbNaviresAttenteTotale == 0) g+=0;
        	else {
        		if (echelleHeures) g += ((float) (this.donnees_.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[n].attenteMegaTotale / this.donnees_.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[n].nbNaviresAttenteTotale))/60;
        		else g += ((float) (this.donnees_.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[n].attenteMegaTotale / this.donnees_.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[n].nbNaviresAttenteTotale));
        	}
        }

        g += "\n";
      }
    }// din du pour remplissage des coordonn�es
    g += "    }\n";
    g += "  }\n";

    // //******************************fin histo moy************************************************

    // ******************************debut histo min************************************************

    g += "  courbe\n  {\n";
    g += "    titre \"";
    if (this.choixSecurite2_.isSelected()) {
      g += "attentes de s�curit� minimales";
    } else if (this.choixAcces2_.isSelected()) {
      g += "attentes d'acc�s minimales";
    } else if (this.choixOccupation2_.isSelected()) {
      g += "attentes d'occupation minimales";
    } else if (this.choixPannes2_.isSelected()) {
      g += "attentes d'indisponibilit� minimales";
    } else if (this.choixTotalAttente2_.isSelected()) {
      g += "attentes totales minimales";
    }

    g += "\"\n";
    g += "    type ";
    if (histo) g += "histogramme"; else g += "courbe";
    g += "\n";
    g += "    aspect\n {\n";
    g += "contour.largeur 1 \n";
    g += "surface.couleur BBCC00 \n";
    g += "texte.couleur 000000 \n";
    g += "contour.couleur ";
    if (histo) g += "000000"; else g += "BBCC00";
    g += " \n";
    g += "    }\n";
    g += "    valeurs\n    {\n";
    /*
     * final Vector coordX= (Vector)_courbes.get(i * 2); final Vector coordY= (Vector)_courbes.get(i * 2 + 1); for (int
     * n= 0;(n < coordX.size()) || (n < coordY.size()); n++) {
     */
    indiceNavire = 0;
    for (int n = 0; n < this.donnees_.listeBateaux_.listeNavires_.size(); n++) {
      if (this.tableauChoixNavires2_[n].isSelected()) {
        g += (indiceNavire + 1)// numero de la cat�gorie
            + " ";
        indiceNavire++;

         if (this.choixSecurite2_.isSelected()) {
        	 if (echelleHeures) g += ((float) this.donnees_.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[n].attenteSecuMini)/60;
        	 else g += ((float) this.donnees_.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[n].attenteSecuMini);
        } else if (this.choixAcces2_.isSelected()) {
        	if (echelleHeures) g += ((float) this.donnees_.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[n].attenteAccesMini)/60;
        	else g += ((float) this.donnees_.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[n].attenteAccesMini);
        } else if (this.choixOccupation2_.isSelected()) {
        	if (echelleHeures) g += ((float) this.donnees_.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[n].attenteOccupMini)/60;
        	else g += ((float) this.donnees_.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[n].attenteOccupMini);
        } else if (this.choixPannes2_.isSelected()) {
        	if (echelleHeures) g += ((float) this.donnees_.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[n].attentePanneMini)/60;
        	else g += ((float) this.donnees_.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[n].attentePanneMini);
        } else if (this.choixTotalAttente2_.isSelected()) {
        	if (echelleHeures) g += ((float) this.donnees_.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[n].attenteTotaleMini)/60;
        	else g += ((float) this.donnees_.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[n].attenteTotaleMini);
        }

        g += "\n";

      }
    }// din du pour remplissage des coordonn�es
    g += "    }\n";
    g += "  }\n";

    // //******************************fin histo min************************************************

    if (seuil_) {
      /**
       * declaration d'un seuil
       */
      g += " contrainte\n";
      g += "{\n";
      // a mettre le seuil
      g += "titre \"seuil \"\n";
      // str+="orientation horizontal \n";
      g += " type max\n";
      //g += " valeur " + valeurSeuil + CtuluLibString.LINE_SEP_SIMPLE;// /la valeur ordonn�e du seuil
      if(echelleHeures) g += " valeur " + (valeurSeuil_/60.) + CtuluLibString.LINE_SEP_SIMPLE;// /la valeur ordonn�e du seuil
      else g += " valeur " + valeurSeuil_ + CtuluLibString.LINE_SEP_SIMPLE;

      g += " \n }\n";
      // }//fin du for
    }

    return g;
  }

  
  public void affichageCamembert(int _categ, boolean _refresh){
	  
	  int nbAttentes=4;
	  
	  int[] tab = new int[nbAttentes];
	  String[] libelles=new String[nbAttentes];
	  int cpt=0;
	  
	  if (this.donnees_.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[_categ].attenteAccesTotale==0) tab[cpt]=0;
	  else tab[cpt]=(int) Math.round(this.donnees_.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[_categ].attenteAccesTotale / this.donnees_.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[_categ].nbNaviresAttenteAcces);
	  libelles[cpt++]="Attentes d'acc�s [minutes]";  
	  if (this.donnees_.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[_categ].attenteOccupTotale==0) tab[cpt]=0;
	  else tab[cpt]=(int) Math.round(this.donnees_.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[_categ].attenteOccupTotale / this.donnees_.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[_categ].nbNaviresAtenteOccup);
	  libelles[cpt++]="Attentes d'occupation [minutes]";   
	  if (this.donnees_.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[_categ].attentePanneTotale==0) tab[cpt]=0;
	  else tab[cpt]=(int) Math.round(this.donnees_.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[_categ].attentePanneTotale / this.donnees_.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[_categ].nbNaviresAttentePanne);
	  libelles[cpt++]="Attentes d'indisponibilit� [minutes]";   
	  if (this.donnees_.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[_categ].attenteSecuTotale==0) tab[cpt]=0;
	  else tab[cpt]=(int) Math.round(this.donnees_.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[_categ].attenteSecuTotale / this.donnees_.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[_categ].nbNaviresAttenteSecu);
	  libelles[cpt++]="Attentes de s�curit� [minutes]"; 
		
	  String titre;
	  if (this.listeElementDepart_.getSelectedItem() == this.listeElementArrivee_.getSelectedItem()) {
		  titre = "Attentes au sein de l'�l�ment " + (String) this.listeElementDepart_.getSelectedItem();
	  } else {
		  titre = "Attentes sur le trajet de " + (String) this.listeElementDepart_.getSelectedItem() + " � " + (String) this.listeElementArrivee_.getSelectedItem();
	  }
	  titre += " pour la cat�gorie " + donnees_.listeBateaux_.retournerNavire(_categ).nom;
	  JFreeChart chart;
	  chart = Sinavi3JFreeChartCamembert.genererCamembert(tab, libelles, titre, false);
	  if (!_refresh) this.camembert_ = new ChartPanel(chart);
	  else this.camembert_.setChart(chart);
  }
  
  /**
   * Listener principal des elements de la frame: tres important pour les composant du panel de choix des �l�ments.
   * 
   * @param ev evenements qui apelle cette fonction
   */

  public void actionPerformed(final ActionEvent ev) {
    final Object source = ev.getSource();

    // action commune a tous les �v�nements: redimensionnement de la fenetre
    final Dimension actuelDim = this.getSize();
    final Point pos = this.getLocation();
    if (source == this.choixTotalAttente_ || source == this.choixNbNavires_ || source == this.choixAcces_
        || source == this.choixSecurite_ || source == this.choixOccupation_
        || source == this.choixPannes_)
 	{
      // clic sur un checkBox:
      // construction de la colonne des titres
      if ( !this.choixSecurite_.isSelected() && !this.choixAcces_.isSelected()
          && !this.choixOccupation_.isSelected() && !this.choixPannes_.isSelected()
          && !this.choixTotalAttente_.isSelected()) {
        this.choixTotalAttente_.setSelected(true);
      }

      int compteurColonnes = 0;
      if (this.choixAcces_.isSelected()) {
        compteurColonnes += 4;
      }
      
      if (this.choixSecurite_.isSelected()) {
        compteurColonnes += 4;
      }
      if (this.choixNbNavires_.isSelected()) {
        compteurColonnes++;
      }
      if (this.choixTotalAttente_.isSelected()) {
        compteurColonnes += 4;
      }
      if (this.choixOccupation_.isSelected()) {
        compteurColonnes += 4;
      }
      if (this.choixPannes_.isSelected()) {
        compteurColonnes += 4;
      }

      this.titreTableau_ = new String[compteurColonnes + 1];
      int indiceColonne = 0;
      this.titreTableau_[indiceColonne++] = "Cat�gorie";
      if (this.choixNbNavires_.isSelected()) {
        this.titreTableau_[indiceColonne++] = "Nombre de bateaux";
      }

      
      if (this.choixSecurite_.isSelected()) {
        this.titreTableau_[indiceColonne++] = "Att. s�curit�: total";
        this.titreTableau_[indiceColonne++] = "Att. s�curit�: moyenne";
        this.titreTableau_[indiceColonne++] = "Att. s�curit�: nb. bateaux";
        this.titreTableau_[indiceColonne++] = "Att. s�curit�: moy./flotte";
      }
      if (this.choixAcces_.isSelected()) {
        this.titreTableau_[indiceColonne++] = "Att. acc�s: total";
        this.titreTableau_[indiceColonne++] = "Att. acc�s: moyenne";
        this.titreTableau_[indiceColonne++] = "Att. acc�s: nb. bateaux";
        this.titreTableau_[indiceColonne++] = "Att. acc�s: moy./flotte";
      }
      if (this.choixOccupation_.isSelected()) {
        this.titreTableau_[indiceColonne++] = "Att. occup.: total";
        this.titreTableau_[indiceColonne++] = "Att. occup.: moyenne";
        this.titreTableau_[indiceColonne++] = "Att. occup.: nb. bateaux";
        this.titreTableau_[indiceColonne++] = "Att. occup.: moy./flotte";
      }
      if (this.choixPannes_.isSelected()) {
        this.titreTableau_[indiceColonne++] = "Att. indisp.: total";
        this.titreTableau_[indiceColonne++] = "Att. indisp.: moyenne";
        this.titreTableau_[indiceColonne++] = "Att. indisp.: nb. bateaux";
        this.titreTableau_[indiceColonne++] = "Att. indisp.: moy./flotte";
      }
      if (this.choixTotalAttente_.isSelected()) {
        this.titreTableau_[indiceColonne++] = "Attente totale";
        this.titreTableau_[indiceColonne++] = "Attente moyenne";
        this.titreTableau_[indiceColonne++] = "Nb. bateaux ayant attendu";
        this.titreTableau_[indiceColonne++] = "Attente moyenne sur flotte";
      }
      // mise a jour via appel a la fonction affichage avec l'onglet selectionn� des cat�gories
      affichageTableau(this.listeNavires_.getSelectedIndex() - 1);
    } else if (source == this.choixTotalAttente2_ || source == this.choixAcces2_
        || source == this.choixSecurite2_ || source == this.choixOccupation2_ || source == this.choixPannes2_
        || source == this.choixTypeGraphe_) {
      // mise a jour des courbes
      final String descriptionCourbes = this.affichageGraphe();
      this.graphe_.setFluxDonnees(new ByteArrayInputStream(descriptionCourbes.getBytes()));

      //-- mise a jour du camembert --//
      //modifierCamembert(listeNaviresCamembert_.getSelectedIndex());
      
    }
    
    if (source == this.choixCumul_ || source == this.choixPercent_ || source == this.choixNombre_)
    {
    	rafraichissementTableauDistribution();
    	affichageGrapheDistrib(true);
    }
    
    if (source == listeNaviresDistrib_) {
    	affichageTableauDistribution();
    	affichageGrapheDistrib(true);
    }
      
    if (source == this.lancerRecherche_) {
      /**
       * C est ici que le calcul se refait: lors de la validation du bouton de lancement de rercherche: recalcule les
       * attentes pour le nouveau trajet:
       */
    	// lancement des calculs pour les dur�es de parcours:
    	Sinavi3AlgorithmeAttentesGenerales.calculTrajetDepuisListeTrajet(donnees_, this.listeTypesDepart_
    			.getSelectedIndex(), this.listeElementDepart_.getSelectedIndex(), this.listeTypesArrivee_.getSelectedIndex(),
    			this.listeElementArrivee_.getSelectedIndex(), this.sens_.getSelectedIndex());

    	// mise a jour des affichages:
    	affichageTableau(this.listeNavires_.getSelectedIndex() - 1);
    	// mise a jour des courbes
    	final String descriptionCourbes = this.affichageGraphe();
    	this.graphe_.setFluxDonnees(new ByteArrayInputStream(descriptionCourbes.getBytes()));
    	//mise � jour des distributions
    	affichageTableauDistribution();
    	affichageGrapheDistrib(true);
    	//-- mise a jour du camembert --//
    	affichageCamembert(listeNaviresCamembert_.getSelectedIndex(), true);

    } else if (source == this.valideSeuil_ || source == this.rafraichirSeuil_) {
    	this.rafraichirSeuil_.setEnabled(this.valideSeuil_.isSelected());
    	if (this.valideSeuil_.isSelected() && !this.valSeuilHeure_.getText().equals("") && !this.valSeuilMinute_.getText().equals("")) {
    		// booleen passe a true
    		this.seuil_ = true;
    		// on recupere al valeure du seuil choisie par l utilisateur
    		valeurSeuil_ = 60*((int) Float.parseFloat(this.valSeuilHeure_.getText())) + (int) Float.parseFloat(this.valSeuilMinute_.getText());
    		// on redesssinne l histogramme en tenant compte du seuil de l utilisateur
    		final String descriptionGraphe = this.affichageGraphe();
    		this.graphe_.setFluxDonnees(new ByteArrayInputStream(descriptionGraphe.getBytes()));
    	} else {
    		// booleen passe a false
    		this.seuil_ = false;
    		// on redesssinne le graphe sans seuil
    		final String descriptionGraphe = this.affichageGraphe();
    		this.graphe_.setFluxDonnees(new ByteArrayInputStream(descriptionGraphe.getBytes()));
    	}
    } else if (source == this.valideSeuilDistrib_ || source == this.rafraichirSeuilDistrib_) {
    	this.rafraichirSeuilDistrib_.setEnabled(this.valideSeuilDistrib_.isSelected());
    	if (this.valideSeuilDistrib_.isSelected() && !this.valSeuilDistribHeure_.getText().equals("") && !this.valSeuilDistribMinute_.getText().equals("")) {
    		// booleen passe a true
    		this.seuilDistrib_ = true;
    		// on recupere la valeur du seuil choisie par l utilisateur
    		valeurSeuilDistrib_ = 60*((int) Float.parseFloat(this.valSeuilDistribHeure_.getText())) + (int) Float.parseFloat(this.valSeuilDistribMinute_.getText());
    		// on regenere le graphe
    		affichageGrapheDistrib(true);
    	} else {
    		// booleen passe a false
    		this.seuilDistrib_ = false;
    		// on regenere le graphe
    		affichageGrapheDistrib(true);
    	}
        
    }

    // si la source provient d un navire du tableau de checkBox
    boolean trouve = false;
    for (int k = 0; k < this.tableauChoixNavires_.length && !trouve; k++) {
    	if (source == this.tableauChoixNavires_[k]) {
    		trouve = true;
    		affichageTableau(-1);
    	}
    	else if (source == this.tableauChoixNavires2_[k]) {
    		trouve = true;
    		// mise a jour des courbes
    		final String descriptionCourbes = this.affichageGraphe();
    		this.graphe_.setFluxDonnees(new ByteArrayInputStream(descriptionCourbes.getBytes()));
    	}
    	else if (source == this.tableauChoixNaviresDistrib_[k]) {
            trouve = true;
            affichageTableauDistribution();
            affichageGrapheDistrib(true);
        }
    }

    // on redimensionne la fenetre comme elle etais avant manipulation des elements graphique
    this.setSize(actuelDim);
    this.setLocation(pos);
  }// fin de actionPerformed

  /**
   * Methode qui s active lorsque l'on quitte l'application.
   */
  protected void windowClosed() {
    System.out.print("Fin de la fenetre de gestion des cercles!!");
    dispose();
  }
  
  /**
   * M�thode permettant d'afficher un graphe de distribution de dur�es de parcours.
   * @param _refresh Param�tre permettant de choisir un rafraichissement du graphe existant plut�t que la construction d'un graphe nouveau
   */
  public void affichageGrapheDistrib(boolean _refresh){
	
	  String titre;
	  String[] noms;
	  short[][] valeurs;
	  int[] nombreBateaux;
	  final int valeurIntervalle = this.donnees_.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.valeurIntervalleDistribution;
	  
	  boolean percent = false;
	  if (choixPercent_.isSelected()) percent=true;
	  
	  boolean echelleHeures=false;
	  if (Sinavi3AlgorithmeAttentesGenerales.determineAttenteMaxTrajet(donnees_) >= 480) echelleHeures=true;
	  
	  if (this.choixCategorie_.isSelected())
	  {
		  titre = "Distribution";
		  if (this.choixCumul_.isSelected()) titre += " cumul�e";
		  titre += " des attentes totales";
		  int nbCategoriesAffichees=0;
		  for(int i=0;i<donnees_.listeBateaux_.listeNavires_.size();i++)
		  {
			  if (this.tableauChoixNaviresDistrib_[i].isSelected()) nbCategoriesAffichees++;
		  }
		
		  noms = new String[nbCategoriesAffichees];
		  valeurs = new short[nbCategoriesAffichees][];
		  nombreBateaux = new int[nbCategoriesAffichees];
		  int cpt=0;
		  for(int i=0;i<donnees_.listeBateaux_.listeNavires_.size();i++)
		  {
			  if (this.tableauChoixNaviresDistrib_[i].isSelected())
			  {
				  noms[cpt] = donnees_.listeBateaux_.retournerNavire(i).nom;
				  valeurs[cpt] = this.donnees_.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[i].attenteTotaleDistrib;
				  nombreBateaux[cpt] = this.donnees_.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[i].nombreNaviresTotal;
				  cpt++;
			  }
		  }
	  } else {
		  titre = "Distribution";
		  if (this.choixCumul_.isSelected()) titre += " cumul�e";
		  titre += " des attentes";
		  final int categ = this.listeNaviresDistrib_.getSelectedIndex() - 1;
		  if (categ != -1) titre += " pour la cat�gorie " + donnees_.listeBateaux_.retournerNavire(categ).nom;
		  
		  String[] attentes = {"Att. d'acc�s", "Att. d'indisponibilit�", "Att. d'occupation", "Att. de s�curit�", "Att. totales"};
		  noms = attentes;
		  final int nombreIntervalles = this.donnees_.params_.ResultatsCompletsSimulation.nombreIntervallesDistribution;
		  valeurs = new short[5][nombreIntervalles];
		  nombreBateaux = new int[5];
		  
		  if (categ != -1)
		  {
			  valeurs[0] = this.donnees_.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[categ].attenteAccesDistrib;
			  valeurs[1] = this.donnees_.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[categ].attentePanneDistrib;
			  valeurs[2] = this.donnees_.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[categ].attenteOccupDistrib;
			  valeurs[3] = this.donnees_.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[categ].attenteSecuDistrib;
			  valeurs[4] = this.donnees_.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[categ].attenteTotaleDistrib;
			  for (int i = 0; i < 5; i++) {
				  nombreBateaux[i] = this.donnees_.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[categ].nombreNaviresTotal;
			  }
		  } else {
			  for (int i = 0; i < donnees_.listeBateaux_.listeNavires_.size(); i++)
			  {
				  for (int j = 0; j < nombreIntervalles; j++)
				  {
					  valeurs[0][j] += this.donnees_.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[i].attenteAccesDistrib[j];
					  valeurs[1][j] += this.donnees_.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[i].attentePanneDistrib[j];
					  valeurs[2][j] += this.donnees_.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[i].attenteOccupDistrib[j];
					  valeurs[3][j] += this.donnees_.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[i].attenteSecuDistrib[j];
					  valeurs[4][j] += this.donnees_.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[i].attenteTotaleDistrib[j];
				  }
				  for (int k = 0; k < 5; k++) {
					  nombreBateaux[k] += this.donnees_.params_.ResultatsCompletsSimulation.tableauAttenteTrajet.tableauAttenteCategories[i].nombreNaviresTotal;
				  }
			  }
		  }
	  }
	  
	  if (this.listeElementDepart_.getSelectedItem() == this.listeElementArrivee_.getSelectedItem()) {
		  titre += " au sein de l'�l�ment " + (String) this.listeElementDepart_.getSelectedItem();
	  } else {
		  titre += " sur le trajet de " + (String) this.listeElementDepart_.getSelectedItem() + " � " + (String) this.listeElementArrivee_.getSelectedItem();
	  }
	  
	  
	  JFreeChart chart;
	  if (this.choixCumul_.isSelected()) chart = Sinavi3JFreeChartGraphe.genererGrapheDistributionCumuleeDurees(titre, valeurIntervalle, valeurs, noms, nombreBateaux, echelleHeures, percent);
	  else chart = Sinavi3JFreeChartGraphe.genererHistoDistributionDurees(titre, valeurIntervalle, valeurs, noms, nombreBateaux, echelleHeures, percent);
	  
	  if (!_refresh) this.grapheDistrib_ = new ChartPanel(chart);
	  else this.grapheDistrib_.setChart(chart);
	  
	  if (seuilDistrib_) Sinavi3JFreeChartGraphe.afficherSeuil(grapheDistrib_, valeurSeuilDistrib_, echelleHeures);
	  
  }
  
}
