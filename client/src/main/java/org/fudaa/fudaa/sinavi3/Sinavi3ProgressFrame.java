package org.fudaa.fudaa.sinavi3;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.Box;
import javax.swing.JProgressBar;

import com.memoire.bu.BuLabel;
import com.memoire.bu.BuPanel;

public class Sinavi3ProgressFrame extends Sinavi3InternalFrame{

	
	JProgressBar barreProgression_=new JProgressBar(0,100);
	BuLabel tachePrincipale=new BuLabel("Demarrage des calculs...");	
	BuLabel tacheSecondaire=new BuLabel("");	
	
	
	
	public Sinavi3ProgressFrame() {
		super("Ex�cution de la simulation", true,true,true,true);
		
	this.setSize(400, 120);	
	this.setLayout(new BorderLayout());
	
	Box panelPrincipal= Box.createVerticalBox();
	
	panelPrincipal.add(new BuLabel("Ex�cution de la simulation en cours..."));
	panelPrincipal.add(barreProgression_);
	BuPanel panel1=new BuPanel(new FlowLayout(FlowLayout.CENTER));
	panel1.add(new BuLabel("T�che principale : "));
	panel1.add(tachePrincipale);
	panelPrincipal.add(panel1);
	BuPanel panel2=new BuPanel(new FlowLayout(FlowLayout.CENTER));
	panel2.add(new BuLabel("T�che secondaire : "));
	panel2.add(tacheSecondaire);
	panelPrincipal.add(panel2);
	//-- afficher le pourcentage de progression --//
	barreProgression_.setStringPainted(true);
	
	this.add(panelPrincipal,BorderLayout.CENTER);
	
	}
	
	
	public void miseAjourBarreProgression(int x,String principal,String secondaire){
		
		if(x>100)
			return;
		
		
		barreProgression_.setValue(x);
		tachePrincipale.setText(principal);
		tacheSecondaire.setText(secondaire);
		
	}
	
	public void miseAjourBarreProgression(boolean indetermine){
		
		barreProgression_.setIndeterminate(indetermine);
		
	}
	
public void miseAjourBarreProgression(int x,String secondaire){
		
		if(x>100)
			return;
		
		
		barreProgression_.setValue(x);
		tacheSecondaire.setText(secondaire);
		
	}
	
	public void terminaison(){
		dispose();
	}
	

}
