/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.sinavi3.ui.results.reports;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import jxl.format.Colour;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import org.fudaa.fudaa.commun.projet.FudaaProjet;
import org.fudaa.fudaa.sinavi3.Sinavi3AlgorithmeBassinees;
import org.fudaa.fudaa.sinavi3.Sinavi3DataSimulation;
import org.fudaa.fudaa.sinavi3.Sinavi3Ecluse;
import org.fudaa.fudaa.sinavi3.Sinavi3ResultatsAttenteTrajet;
import org.fudaa.fudaa.sinavi3.Sinavi3TraduitHoraires;

/**
 *
 * @author Adrien Hadoux
 */
public class Sinavi3ReportConsoEau extends Sinavi3SheetFactoryHelper{
    
    
    
    

    public static void produceSheetAttentesTrajetsValues(Sinavi3DataSimulation data, List<Sinavi3AlgorithmeBassinees.EcluseBassinneeStat>  stats, WritableSheet sheet, int indiceLigne, 
            int ecluse ) throws WriteException {
      
              
        Sinavi3AlgorithmeBassinees.EcluseBassinneeStat stat = stats.get(ecluse);
        Sinavi3Ecluse ecluseData = data.getListeEcluse().retournerEcluse(ecluse);
       //-- afficher valeurs attente occup --//
       setValueAt(indiceLigne, 3, sheet, stat.bassinnee.min);
       setValueAt(indiceLigne, 4, sheet, stat.bassinnee.moy);
       setValueAt(indiceLigne, 5, sheet, stat.bassinnee.max);
       
       setValueAt(indiceLigne, 7, sheet, stat.fausse.min);
       setValueAt(indiceLigne, 8, sheet, stat.fausse.moy);
       setValueAt(indiceLigne, 9, sheet, stat.fausse.max);
       
       setValueAt(indiceLigne, 11, sheet, stat.getMin());
       setValueAt(indiceLigne, 12, sheet, stat.getMoy());
       setValueAt(indiceLigne, 13, sheet, stat.getMax());
       
       
       //-- calcul conso eau --//
       double consoMini = stat.getMin() / 2 * ecluseData.getLongueur_() * ecluseData.getLargeur_() * ecluseData.getHauteurchute();
       double consoMaxi = stat.getMax()/ 2 * ecluseData.getLongueur_() * ecluseData.getLargeur_() * ecluseData.getHauteurchute();
       double consoMoy = stat.getMoy() / 2 * ecluseData.getLongueur_() * ecluseData.getLargeur_() * ecluseData.getHauteurchute();
       
       setValueAt(indiceLigne, 15, sheet, consoMini);
       setValueAt(indiceLigne, 16, sheet, consoMoy);
       setValueAt(indiceLigne, 17, sheet, consoMaxi);
       
       

    }
    
    
   
    


    public static void produceSheetAttentesTrajetsHeader(WritableSheet sheet, boolean multiSimulation, Sinavi3DataSimulation data) throws WriteException {
        // taille de la colonne
        sheet.setColumnView(0, 30);
        sheet.setColumnView(1, 20);
        sheet.setColumnView(2, 5);
        sheet.setColumnView(6, 5);
        sheet.setColumnView(10, 5);
        sheet.setColumnView(14, 5);
        
        sheet.setColumnView(15, 20);
        sheet.setColumnView(16, 20);
        sheet.setColumnView(17, 20);
        sheet.setRowView(1, 38*20);
        setValueTitleAt(0, 0, sheet, "Bassin�e et conso eau", Colour.WHITE);
        if (!multiSimulation) {
            setValueTitleAt(1, 0, sheet, " R�sultats d'un seul sc�nario de simulation", Colour.WHITE);
            setValueTitleAt(5, 0, sheet, "Sc�nario", Colour.TURQOISE2);
            fusionneCells(5, 0, 2, false, sheet);
        } else {
            setValueTitleAt(1, 0, sheet, "R�sultats de plusieurs sc�narios de simulation", Colour.WHITE);
            setValueTitleAt(5, 0, sheet, "Sc�nario", Colour.TURQOISE2);
            
        }

        setValueTitleAt(5, 1, sheet, "Ecluse", Colour.LIGHT_GREEN);
        fusionneCells(5, 1, 2, false, sheet);
       
        
        setValueTitleAt(5, 3, sheet, "Nb Bassin�es / Jour", Colour.LIGHT_ORANGE);
        fusionneCells(5, 3, 3, true, sheet);
        setValueTitleAt(6, 3, sheet, "Min");
        setValueTitleAt(6, 4, sheet, "Moy");
        setValueTitleAt(6, 5, sheet, "Max");
        
        setValueTitleAt(5, 7, sheet, "Nb Fausses Bassin�es / Jour", Colour.LIGHT_ORANGE);
        fusionneCells(5, 7, 3, true, sheet);
        setValueTitleAt(6, 7, sheet, "Min");
        setValueTitleAt(6, 8, sheet, "Moy");
        setValueTitleAt(6, 9, sheet, "Max");
        
         setValueTitleAt(5, 11, sheet, "Nb Total Bassin�es / Jour", Colour.LIGHT_ORANGE);
        fusionneCells(5, 11, 3, true, sheet);
        setValueTitleAt(6, 11, sheet, "Min");
        setValueTitleAt(6, 12, sheet, "Moy");
        setValueTitleAt(6, 13, sheet, "Max");
        
        setValueTitleAt(5, 15, sheet, "Consommation d'eau / Jour (m3)", Colour.LIGHT_ORANGE);
        fusionneCells(5, 15, 3, true, sheet);
        setValueTitleAt(6, 15, sheet, "Min");
        setValueTitleAt(6, 16, sheet, "Moy");
        setValueTitleAt(6, 17, sheet, "Max");
    }

    public static WritableSheet produceSheetBassinnee(final WritableWorkbook classeur, final Sinavi3DataSimulation data, boolean multiSimulation) throws WriteException {
        WritableSheet sheet = classeur.createSheet("Bassin�es et Conso d'eau", 0);

        produceSheetAttentesTrajetsHeader(sheet, multiSimulation, data);
        List<Sinavi3DataSimulation> listeComparison = new ArrayList<Sinavi3DataSimulation>();
        HashMap<Sinavi3DataSimulation, List<Sinavi3AlgorithmeBassinees.EcluseBassinneeStat>> mapStatsMulti = new HashMap<Sinavi3DataSimulation, List<Sinavi3AlgorithmeBassinees.EcluseBassinneeStat>>();
        if (multiSimulation) {
            for (FudaaProjet projet : data.getApplication().getSimulations()) {
                if (data.getProjet_() != projet) {
                    Sinavi3DataSimulation comparison = getSimulationFromFudaaProjet(projet);
                    listeComparison.add(comparison);
                    List<Sinavi3AlgorithmeBassinees.EcluseBassinneeStat>  statsComp = Sinavi3AlgorithmeBassinees.computeBassinneeStats(comparison);
                    mapStatsMulti.put(comparison, statsComp);
                }
            }
        }

        //-- calculs avalant--//
        int indiceLigne = 8;  
        
        List<Sinavi3AlgorithmeBassinees.EcluseBassinneeStat>  stats = Sinavi3AlgorithmeBassinees.computeBassinneeStats(data);

        for (int tronconE = 0; tronconE < data.getListeEcluse().getListeEcluses().size(); tronconE++) {
           
                setValueAt(indiceLigne, 1, sheet, data.getListeEcluse().retournerEcluse(tronconE).getName());
               produceSheetAttentesTrajetsValues(data, stats, sheet, indiceLigne, tronconE);
               
                if (multiSimulation) {
                    fusionneCells(indiceLigne, 1, (listeComparison.size()+1)*2 -1, false, sheet);
                     setValueTitleAt(indiceLigne, 0, sheet, "Sc�nario de r�f�rence", Colour.WHITE);
                   indiceLigne+=2;
                    for (Sinavi3DataSimulation comparison : listeComparison) {
                        setValueTitleAt(indiceLigne, 0, sheet, "Sc�nario " + comparison.getProjectName_(), Colour.WHITE);
                         List<Sinavi3AlgorithmeBassinees.EcluseBassinneeStat>  statsComp = mapStatsMulti.get(comparison);
                        produceSheetAttentesTrajetsValues(comparison, statsComp, sheet, indiceLigne, tronconE);
                        indiceLigne+=2;
                    }
                }else 
                   {
                indiceLigne+=2;
            }
            
        }
        if (!multiSimulation) {
            setValueTitleAt(7, 0, sheet, "Sc�nario de r�f�rence", Colour.WHITE);
            fusionneCells(7, 0, indiceLigne - 8, false, sheet);
        }
        return sheet;
    }

    
}
