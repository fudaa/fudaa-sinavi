/**
 *@creation 10 oct. 06
 *@modification $Dates$
 *@license  GNU General Public Licence 2
 *@copyright   (c)1998-2006 CETMEF 2 bd Gambetta F -60231 Compiegne
 *@mail   devel@fudaa.fr 
 */

package org.fudaa.fudaa.sinavi3;

/**
 * 
 */

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.Border;

import org.jdesktop.swingx.ScrollPaneSelector;

import com.memoire.bu.BuDialogError;
import com.memoire.bu.BuDialogMessage;

/**
 * Panel de saisie des couples pour la loi deterministe presentation souss forme de tableau: ajout , mise a jour,
 * 
 * 
 * @author Adrien Hadoux
 */
public class Sinavi3FrameSaisieLoiDeterministe extends Sinavi3InternalFrame {

  /**
   * JCombo qui permettra de choisir pour chaque chenal les gares amont et gares avales
   */
  JComboBox ComboGare;

  /**
   * Descriptif des elements des colonnes
   */
  String[] titreColonnes = { "Jour", "Horaire" };

  /**
   * Tableau de type JTable qui contiendra les donn�es des bassins
   */

  JTable tableau;

  /**
   * Bouton de validation des donn�es topolgiques saisies pour le chenal
   */
  JButton validation = new JButton("Valider");
  JButton suppression = new JButton("Supprimer");

  /**
   * Fenetre qui contiendra le panel
   */
  JPanel global = new JPanel();

  /**
   * Bordure du tableau
   */

  /**
   * composant qui recevra le focus a la destructuion de la frame
   */
  Component composant_ = null;

  Border borduretab = BorderFactory.createLoweredBevelBorder();

  /**
   * donn�es de la loi deterministe
   */
  ArrayList loiDeterministe_ = new ArrayList();

  Sinavi3DataSimulation donnees_;

  LoiDeterministeTableModel modeleTableau_;
  
  
  public Sinavi3FrameSaisieLoiDeterministe(final Sinavi3DataSimulation _donnees, final ArrayList _loiDeterministe,
	      final Component c) {
	  	this(_donnees,_loiDeterministe, c, null, null);
  }
  
  /**
   * constructeur du panel d'affichage des bassins
   * 
   * @param d donn�es de la simulation
   * @param _loiDeterministe liste de donn�es de la loi deeterministe
   * @param c composant qui recevra le focus des la validation de la fenetre
   */
  public Sinavi3FrameSaisieLoiDeterministe(final Sinavi3DataSimulation _donnees, final ArrayList _loiDeterministe,
      final Component c, final String comments, final Dimension d) {

    super("", true, true, true, true);
    // recuperation des donn�es de la simulation
    loiDeterministe_ = _loiDeterministe;
    composant_ = c;
    donnees_ = _donnees;
    
    //-- creation du tableau --//
    modeleTableau_=new LoiDeterministeTableModel(loiDeterministe_,donnees_);
    
    global.setLayout(new BorderLayout());

    // remplissage des comboBox en onction des donn�es
    this.remplissage();
    // afichage des elements dans le tableau.
    this.affichage();

    // listener du bouton de validation

    this.validation.addActionListener(new ActionListener() {

      public void actionPerformed(final ActionEvent e) {
    	  /*
        tableau.editCellAt(299, 1);
        tableau.editingCanceled(null);
        miseAjourSaisieDeterministe();
 		*/
    	  new BuDialogMessage(donnees_.application_.getApp(), Sinavi3Implementation.isSinavi_,
    		        "les donn�es ont �t� correctement saisies: " + loiDeterministe_.size() + " couples saisis!! ").activate();

    	  dispose();
      }

    });
    
    
    //ajout par fargeix
    
    this.suppression.addActionListener(new ActionListener() {
		public void actionPerformed(final ActionEvent e) {
			final int numLigne = tableau.getSelectedRow();
			if (numLigne == -1) {}
			else {
				for (int i=numLigne; i<299; i++)
				{
					tableau.getModel().setValueAt(tableau.getModel().getValueAt(i+1, 0), i, 0);
					tableau.getModel().setValueAt(tableau.getModel().getValueAt(i+1, 1), i, 1);
				}
			}
		}});
    
    //fin ajout par fargeix
    

    /**
     * Creation de la fenetre
     */

    setTitle("Loi d�terministe");
    if( d != null) {
    	setSize(d);
    }else
    	setSize(250, 300);
    setBorder(BorderFactory.createCompoundBorder(BorderFactory.createRaisedBevelBorder(), BorderFactory.createLoweredBevelBorder()));
    
    getContentPane().setLayout(new BorderLayout());

    final JScrollPane ascenceur = new JScrollPane(global);
    //  utilisation de swingx
    ScrollPaneSelector.installScrollPaneSelector( ascenceur);
    getContentPane().add(ascenceur, BorderLayout.CENTER);

    final JPanel controlPanel = new JPanel();
   controlPanel.add(validation);
   controlPanel.add(suppression);
    getContentPane().add(controlPanel, BorderLayout.SOUTH);

    if(comments != null) {
    	JLabel label = new JLabel(comments);
    	label.setForeground(Color.blue);
    	label.setBackground(Color.white);
    	getContentPane().add(label, BorderLayout.NORTH);
    }
    
    // affichage de la frame
    setVisible(true);

  }

  /**
   * Methode de remplissage des JComboBox et des donn�es par d�fauts pour chaque objet.
   */
  void remplissage() {

  }

  /**
   * Methode d affichage des composants du jtable et du tableau de combo Cette methode est a impl�menter dans les
   * classes d�riv�es pour chaque composants
   */
  void affichage() {

   

    //-- CREATION DU TABLEAU ET DU MODELE
    this.tableau = new JTable(modeleTableau_) ;
    
    tableau.setBorder(this.borduretab);

    tableau.revalidate();
    // this.removeAll();
    this.global.add(/* ascenceur */tableau.getTableHeader(), BorderLayout.PAGE_START);
    this.global.add(tableau, BorderLayout.CENTER);

    final JPanel controlePanel = new JPanel();
    controlePanel.add(validation);

    global.add(controlePanel, BorderLayout.SOUTH);
    this.revalidate();
    this.updateUI();

  }

  /**
   * Methode qui permet de verifier la pertinence des donn�es saisies: IE: v�rifie que les gares choisies en amont et
   * avales sont bien diff�rentes pour un m�me chenal
   * 
   * @return true si les donn�es sont coh�rentes sinon retourne false et surtout indique a quel endroit se situe
   *         l'erreur de logique de la saisie
   */
  boolean verificationCoherence() {
    String jour = "";
    String horaire = "";
    for (int i = 0; i < 300; i++) {
      if ((String) this.tableau.getModel().getValueAt(i, 0) != null) {
        jour = (String) this.tableau.getModel().getValueAt(i, 0);
      } else {
        jour = "";
      }
      if ((String) this.tableau.getModel().getValueAt(i, 1) != null) {
        horaire = (String) this.tableau.getModel().getValueAt(i, 1);
      } else {
        horaire = "";
      }

      if (!jour.equals("")) {
        if (!horaire.equals("")) {
          // test si le nombre entr�e est bien un nombre r�el:
          try {
            final int nj = Integer.parseInt(jour);
            final float nh = Float.parseFloat(horaire);
            if (nh < 0) {
              new BuDialogError(donnees_.application_.getApp(), Sinavi3Implementation.isSinavi_,
                  "Erreur!! l'horaire ligne " + i + " est negatif!!!!").activate();
              return false;
            } else if (nj < 0) {
              new BuDialogError(donnees_.application_.getApp(), Sinavi3Implementation.isSinavi_, "Erreur!! le jour ligne "
                  + i + " est negatif!!!!").activate();
              return false;
            } else if (nj > donnees_.params_.donneesGenerales.nombreJours) {
              new BuDialogError(
                  donnees_.application_.getApp(),
                  Sinavi3Implementation.isSinavi_,
                  "Erreur!! le jour ligne "
                      + i
                      + " est superieur au nombre total \nde jours de simulation "
                      + donnees_.params_.donneesGenerales.nombreJours
                      + "!!!!\n vous pouvez modifier le nombre de jours de la simulation \nen allant dans l'onglet donn�es g�n�rales \nFermez cette fenetre et ouvrez la pour mettre a jour les donn�es")
                  .activate();
              return false;
            } else if (nh > 24) {
              // le nombre est positif et inferieur a 24
              new BuDialogError(donnees_.application_.getApp(), Sinavi3Implementation.isSinavi_,
                  "Erreur!! l'horaire ligne " + i + " est invalide: superieur a 24!!!!").activate();
              return false;
            } else {
              System.out.println("donnees saisies: jour " + nj + "/ horaire " + nh);
              // on ajoute al donn�e au vecteur ou on modifie selon le cas
              if (i >= this.loiDeterministe_.size()) {
                this.loiDeterministe_.add(new CoupleLoiDeterministe(nj, nh));
                
              } else {
                this.loiDeterministe_.set(i, new CoupleLoiDeterministe(nj, nh));
              }

            }
          } catch (final NumberFormatException nfe) {
            new BuDialogError(donnees_.application_.getApp(), Sinavi3Implementation.isSinavi_, "  l'horaire ligne " + i
                + " saisi n'existe pas!!!!").activate();
            return false;
          }

        }// if les mots sont non vides
        else {
          new BuDialogMessage(donnees_.application_.getApp(), Sinavi3Implementation.isSinavi_,
              "Les donn�es ont �t� correctement saisies: " + i + " couples enregistr�s.").activate();
          dispose();
          return true;

        }
      }
    }

    // arriv� � ce stade de la m�thode , tous les test de non coh�rence ont �chou�, il suit que
    // les donn�es saisies sont bien coh�rentes et l'on peut les ajouter aux cheneaux.
    new BuDialogMessage(donnees_.application_.getApp(), Sinavi3Implementation.isSinavi_,
        "les donn�es ont �t� correctement saisies: " + this.loiDeterministe_.size() + " couples saisis!! ").activate();

    dispose();

    if (this.composant_ != null) {
      this.composant_.requestFocus();
    }

    return true;
  }

  /**
   * Methode qui permet de mettre a jour les gares saiies en amont et vaales pour chacun des cheneaux: v�rifie dans un
   * premier temps l coh�rence des donn�es:
   */

  void miseAjourSaisieDeterministe() {

    /**
     * premiere etape on v�rifie la coh�rence des donn�es saisies:
     */
    this.verificationCoherence();

  }

}
