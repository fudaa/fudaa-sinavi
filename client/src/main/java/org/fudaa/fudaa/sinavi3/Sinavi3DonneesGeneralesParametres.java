/*
 * @file         SiporDonneesGeneralesParametres.java
 * @creation     1999-10-01
 * @modification $Date: 2007-11-23 11:28:03 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.sinavi3;

import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.Vector;

import javax.swing.JComboBox;
import javax.swing.border.EmptyBorder;

import org.fudaa.dodico.corba.sipor.SParametresDonneesGenerales;

import com.memoire.bu.BuCommonInterface;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuTextField;

/**
 * Classe impl�mentant l'onglet "Donn�es g�n�rales" pour les param�tres de sipor.
 * 
 * @version $Revision: 1.1 $ $Date: 2007-11-23 11:28:03 $ by $Author: hadouxad $
 * @author Nicolas Chevalier
 */
public class Sinavi3DonneesGeneralesParametres extends BuPanel {
  // Raccourci vers les donn�es g�n�rales locales de l'application.
  SParametresDonneesGenerales donneesGenerales;
  // Tableaux pour les listes d�roulantes
  final Vector nombresIterations = new Vector();
  final String[] jours = { "Dimanche", "Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi" };
  // Zones de saisie contenues dans l'onglet
  BuTextField tfTitre1 = new BuTextField();
  BuTextField tfTitre2 = new BuTextField();
  BuTextField tfTitre3 = new BuTextField();
  JComboBox boxNombrePassages = new JComboBox(nombresIterations);
  BuTextField tfGraineDepart = BuTextField.createIntegerField();
  JComboBox boxPremierJour = new JComboBox(jours);
  // Premier jour de la simulation
  DureeField dfDureeInitSimul = new DureeField(true, true, true, false);
  // Dur�e d'initialisation de la simulation
  DureeField dfDureeSimulReelle = new DureeField(true, true, true, false);
  // Dur�e de la simulation r�elle
  DureeField dfEcrTpsAtt = new DureeField(false, false, true, true);
  // Ecretage des temps d'attente
  DureeField dfArrHeuReglPrio = new DureeField(false, false, true, true);

  // Arrondi des heures pour les r�gles de priorit�
  /**
   * M�thode utile pour placer un composant dans l'onglet.
   * 
   * @param lm layout manager � utiliser.
   * @param composant composant � placer
   * @param c contraintes � utiliser.
   */
  public void placeComposant(final GridBagLayout lm, final Component composant, final GridBagConstraints c) {
    lm.setConstraints(composant, c);
    add(composant);
  }

  /** Constructeur. appli doit etre instance de SiporImplementation */
  public Sinavi3DonneesGeneralesParametres(final BuCommonInterface appli) {
    super();
    final Sinavi3Implementation implementation = (Sinavi3Implementation) appli.getImplementation();
    donneesGenerales = implementation.outils_.getDonneesGenerales();
    // Cr�ation du layout manager
    final GridBagLayout lm = new GridBagLayout();
    final GridBagConstraints c = new GridBagConstraints();
    // Cr�ation de l'onglet lui-meme
    this.setLayout(lm);
    this.setBorder(new EmptyBorder(5, 5, 5, 5));
    // Contraintes communes � tous les composants
    c.gridy = GridBagConstraints.RELATIVE;
    c.anchor = GridBagConstraints.WEST;
    c.ipadx = 5;
    c.ipady = 5;
    // Ajout des �tiquettes sur l'onglet donn�es g�n�rales
    c.gridx = 0; // Remplir la premi�re colonne
    c.gridheight = 3; // 3 lignes pour l'�tiquette de titre.
    placeComposant(lm, new BuLabel("Titre : "), c);
    c.gridheight = 1; // 1 ligne pour les autres �tiquettes
    placeComposant(lm, new BuLabel("Nombre de passages : "), c);
    placeComposant(lm, new BuLabel("Graine de d�part : "), c);
    placeComposant(lm, new BuLabel("Premier jour de la simulation : "), c);
    placeComposant(lm, new BuLabel("Dur�e d'initialisation de la simulation : "), c);
    placeComposant(lm, new BuLabel("Dur�e de la simulation r�elle : "), c);
    placeComposant(lm, new BuLabel("Ecretage des temps d'attente : "), c);
    placeComposant(lm, new BuLabel("Arrondi des heures pour les r�gles de priorit� : "), c);
    // Largeur des zones de texte
    for (int i = 1; i < 7; i++) {
      nombresIterations.add(new Integer(i));
    }
    tfTitre1.setColumns(20);
    tfTitre2.setColumns(20);
    tfTitre3.setColumns(20);
    tfGraineDepart.setColumns(4);
    // Ajout des zones de texte dans la deuxi�me colonne
    c.gridx = 1;
    placeComposant(lm, tfTitre1, c);
    placeComposant(lm, tfTitre2, c);
    placeComposant(lm, tfTitre3, c);
    placeComposant(lm, boxNombrePassages, c);
    placeComposant(lm, tfGraineDepart, c);
    placeComposant(lm, boxPremierJour, c);
    placeComposant(lm, dfDureeInitSimul, c);
    placeComposant(lm, dfDureeSimulReelle, c);
    placeComposant(lm, dfEcrTpsAtt, c);
    placeComposant(lm, dfArrHeuReglPrio, c);
    setDonneesGenerales(donneesGenerales);
  }

  /**
   * Accesseur de donneesGenerales avec enregistrement pr�alable des donn�es contenues dans l'onglet. Permet de lire les
   * donn�es pr�sentes dans l'onglet.
   */
  public SParametresDonneesGenerales getDonneesGenerales() {
    if (donneesGenerales != null) {
      donneesGenerales.titre1 = tfTitre1.getText();
      donneesGenerales.titre2 = tfTitre2.getText();
      donneesGenerales.titre3 = tfTitre3.getText();
      donneesGenerales.nombrePassages = ((Integer) boxNombrePassages.getSelectedItem()) == null ? 0
          : ((Integer) boxNombrePassages.getSelectedItem()).intValue();
      donneesGenerales.graineDepart = tfGraineDepart.getValue() == null ? 0 : ((Integer) tfGraineDepart.getValue())
          .intValue();
      donneesGenerales.premierJourSimulation = boxPremierJour.getSelectedIndex();
      donneesGenerales.heureDebutReelSimulation = dfDureeInitSimul.getDureeFieldLong();
      donneesGenerales.heureFinSimulation = donneesGenerales.heureDebutReelSimulation
          + dfDureeSimulReelle.getDureeFieldLong();
      donneesGenerales.ecretageTempsAttente = dfEcrTpsAtt.getDureeFieldLong();
      donneesGenerales.arrondiHeuresReglesPriorite = dfArrHeuReglPrio.getDureeFieldLong();
    }
    return donneesGenerales;
  }

  /**
   * Mutateur de donneesGenerales avec affichage des nouvelles valeurs dans l'onglet. Permet de d�finir les donn�es
   * trait�es dans l'onget. Elles sont imm�diatement affich�es.
   */
  public void setDonneesGenerales(final SParametresDonneesGenerales dg) {
    donneesGenerales = dg;
    if (dg != null) {
      tfTitre1.setText(dg.titre1);
      tfTitre2.setText(dg.titre2);
      tfTitre3.setText(dg.titre3);
      boxNombrePassages.setSelectedItem(new Integer(dg.nombrePassages));
      tfGraineDepart.setText(Integer.toString(dg.graineDepart));
      boxPremierJour.setSelectedIndex(dg.premierJourSimulation);
      dfDureeInitSimul.setDureeField(dg.heureDebutReelSimulation);
      dfDureeSimulReelle.setDureeField((dg.heureFinSimulation - dg.heureDebutReelSimulation));
      dfEcrTpsAtt.setDureeField(dg.ecretageTempsAttente);
      dfArrHeuReglPrio.setDureeField(dg.arrondiHeuresReglesPriorite);
    }
  }
}
