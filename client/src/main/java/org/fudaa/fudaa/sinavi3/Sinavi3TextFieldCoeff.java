package org.fudaa.fudaa.sinavi3;

import java.awt.Color;
import java.awt.event.FocusEvent;

import com.memoire.bu.BuDialogError;

/**
 * manage Coefficient between 0 and 1.
 * @author Adrien
 *
 */
public class Sinavi3TextFieldCoeff extends Sinavi3TextField{

	private static final long serialVersionUID = 1L;


	public Sinavi3TextFieldCoeff() {
		super();		
	}



	public Sinavi3TextFieldCoeff(int columns) {
		super(columns);
	}



	public void afterFocus(FocusEvent e){

		String previousContent=this.getText();

		if (previousContent.equals("")) 
			return;
		//-- Traduction en float --//

		try {
			float value=Float.parseFloat(previousContent);
			if(value<0) {
				value = value *-1;
			}
			
			if(value >1) {
				value = convertValue(value);
			}


				this.setText( "" + value );



		} catch (NumberFormatException e1) {
			new BuDialogError(null, Sinavi3Implementation.isSinavi_,
					"La valeur doit �tre un nombre compris entre 0 et 1").activate();
			setText( "" );
			this.requestFocus();
		}

		Sinavi3TextFieldCoeff.this.setForeground(Color.black);
	}


	private float convertValue(float value) {
		
		float total = 10;
		boolean converted=false;
		while (!converted) {
			if(value < total) {
				converted = true;
			} else {
				total = total *10;
			}
				 
		}
		return value / total;
	}
	protected int taille(){
		return 5;
	}


}
