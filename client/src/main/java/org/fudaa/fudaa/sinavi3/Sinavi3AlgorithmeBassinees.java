/**
 *@creation 23 fev. 10
 *@modification $Dates$
 *@license  GNU General Public Licence 2
 *@copyright   (c)1998-2006 CETMEF 2 bd Gambetta F -60231 Compiegne
 *@mail   devel@fudaa.fr 
 */
package org.fudaa.fudaa.sinavi3;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.fudaa.dodico.corba.sinavi3.SParametresResultatBassinee;
import org.fudaa.dodico.corba.sinavi3.SParametresResultatsBassinees;
import org.fudaa.dodico.corba.sinavi3.SParametresResultatsCompletSimulation;
import org.fudaa.dodico.corba.sinavi3.Sinavi3ResultatConsoEau;

/**
 * @version $Version$
 * @author Mederic FARGEIX
 */
public class Sinavi3AlgorithmeBassinees {

	public static void calculApresSimu(final Sinavi3DataSimulation _donnees) {
		
		final SParametresResultatsCompletSimulation resultats = _donnees.params_.ResultatsCompletsSimulation;
		
		final int nombreEcluses = _donnees.listeEcluse_.listeEcluses_.size();
		int element;
		
		resultats.Bassinees = new SParametresResultatsBassinees[nombreEcluses];
		
		int[] nombreEnregistrementsTraites = new int[nombreEcluses];
		int[] nombreEnregistrementsTotal = new int[nombreEcluses];
		
		// On compte le nombre d'enregistrement pour chaque element afin de dimensionner chaque tableau de r�sultat
		for (int i = 0; i < _donnees.listeResultatsBassineesSimu_.listeBassinees.length; i++) {
			if(nombreEnregistrementsTotal.length>_donnees.listeResultatsBassineesSimu_.listeBassinees[i].numero) {
				nombreEnregistrementsTotal[_donnees.listeResultatsBassineesSimu_.listeBassinees[i].numero] ++;
			}
		}
		
		// Allocation m�moire
		for (int j = 0; j < nombreEcluses; j++) {
			
			resultats.Bassinees[j] = new SParametresResultatsBassinees();
			
			resultats.Bassinees[j].indiceElement = j;
			
			resultats.Bassinees[j].resultatsBassineesUnitaires = new SParametresResultatBassinee[nombreEnregistrementsTotal[j]];
			
			for (int k = 0; k < nombreEnregistrementsTotal[j]; k++) {
				resultats.Bassinees[j].resultatsBassineesUnitaires[k] = new SParametresResultatBassinee();
			}
			
		}
		
		for (int i = 0; i < _donnees.listeResultatsBassineesSimu_.listeBassinees.length; i++) {
			
			element = _donnees.listeResultatsBassineesSimu_.listeBassinees[i].numero;
			resultats.Bassinees[element].resultatsBassineesUnitaires[ nombreEnregistrementsTraites[element] ].heure = _donnees.listeResultatsBassineesSimu_.listeBassinees[i].heure;
			resultats.Bassinees[element].resultatsBassineesUnitaires[ nombreEnregistrementsTraites[element] ].sens = _donnees.listeResultatsBassineesSimu_.listeBassinees[i].sens;
			resultats.Bassinees[element].resultatsBassineesUnitaires[ nombreEnregistrementsTraites[element] ].fausseBassinee = _donnees.listeResultatsBassineesSimu_.listeBassinees[i].fausseBassinee;
			
			nombreEnregistrementsTraites[element] ++;
			
		}
		
	}
	
	
	/**
	   * 
	   * lit un fichier de consommation d'eau avec extension .consoEau .
	   * @param nomFichier
	   * @return
	   * @author Adrien Hadoux
	   */
	
	 public static List<Sinavi3ResultatConsoEau> computeConsommationEauSinavi3(Map<Integer, Integer> mapBassineeEcluses, Sinavi3DataSimulation donnees) {
		 List<Sinavi3ResultatConsoEau> listeConsoEau = new  ArrayList<Sinavi3ResultatConsoEau> ();
		 int nbEcluses = donnees.getListeEcluse().listeEcluses_.size();
		 for(int indiceEcluse=0;indiceEcluse<nbEcluses;indiceEcluse++) {
			 Sinavi3ResultatConsoEau consoEau = new Sinavi3ResultatConsoEau();  
			 consoEau.indiceEcluse = indiceEcluse;
			 int nbBassinees = 0;
			 if(mapBassineeEcluses.containsKey(indiceEcluse)) {
				 nbBassinees= mapBassineeEcluses.get(indiceEcluse);
			
			 //-- formule de consommation d'eau: conso = nmb * (larg*long*haut) * (1 - coeff) --//
			 Sinavi3Ecluse ecluse = donnees.getListeEcluse().retournerEcluse(indiceEcluse);
			 consoEau.consommationEau = computeConsommationEauPourEcluse(nbBassinees, ecluse);
			 
			 } else 
				 consoEau.consommationEau = 0;
			 
			 listeConsoEau.add(consoEau);
		 }
		 return listeConsoEau;
	 }
	 
	 public static double computeConsommationEauPourEcluse(int nmb, Sinavi3Ecluse ecluse) {
		 double v= ecluse.largeur_ * ecluse.longueur_ * ecluse.hauteurchute;
		 double coeff = ecluse.coefficientBassinEpargne_;
		 double consommationEau = nmb/2.0 * v *  coeff;
		 return consommationEau;
		 
	 }

         /**
          * Detecte le jour par rapport � l'�v�nement et la date du premier evenement qui correspond au jour 0
          * @param premierevenement
          * @param heure
          * @return 
          */
    private static int getDayOfMoment(double moment) {
        return (int) (moment /60/24);

    }

    public static class BassinneeDuJour {

        int jour;
        int nbBassinees;
    }

    public static class BassinneeStat {

        public  int min;
        public  int max;
        public  double moy;
    }

    public static class EcluseBassinneeStat {

       public BassinneeStat bassinnee;
       public  BassinneeStat fausse;
       public   int indiceEcluse;
       public  String nomEcluse;

        public int getMin() {
            return Math.min(bassinnee.min, fausse.min);
        }

        public int getMax() {
            return Math.max(bassinnee.max, fausse.max);
        }

        public double getMoy() {
            return (bassinnee.moy + fausse.moy) / 2.0;
        }

    }


        private static BassinneeDuJour findBassinneeDuJour(List<BassinneeDuJour> liste, int jour) {
            for(BassinneeDuJour b:liste){
                if(b.jour == jour)
                    return b;
            }
            BassinneeDuJour b = new BassinneeDuJour();
            b.jour = jour;
            b.nbBassinees =0;
            liste.add(b);
            return b;
        }
        
         /**
          * Celacul les min, max et moyenne bassinnees par jour pour chaque ecluse
          * @param data  de simulation
          * @author Adrien Hadoux
          * 
          */
         public static List<EcluseBassinneeStat> computeBassinneeStats(final Sinavi3DataSimulation data) {
             List<EcluseBassinneeStat> results = new ArrayList<EcluseBassinneeStat>();
             SParametresResultatsBassinees[] bassinnees = data.params_.ResultatsCompletsSimulation.Bassinees;
            
              for(int i=0;i<data.getListeEcluse().getListeEcluses().size();i++) {
                  if(i<bassinnees.length) {
                    Sinavi3Ecluse ecluse = data.getListeEcluse().retournerEcluse(i);
                   
                    List<BassinneeDuJour> nombreBassinneeParJour = new ArrayList<BassinneeDuJour>();
                    List<BassinneeDuJour> nombreFausseBassinneeParJour = new ArrayList<BassinneeDuJour>();
                    //TODO calcul des nombre de bassinnee par jour
                    for(int event=0;event<bassinnees[i].resultatsBassineesUnitaires.length;event++) {
                        SParametresResultatBassinee eventBassinne = bassinnees[i].resultatsBassineesUnitaires[event];
                        
                        int day = getDayOfMoment( eventBassinne.heure);
                        
                       if(eventBassinne.fausseBassinee) {
                         BassinneeDuJour b = findBassinneeDuJour(nombreFausseBassinneeParJour, day);
                         b.nbBassinees ++;
                       }else {
                          BassinneeDuJour b = findBassinneeDuJour(nombreBassinneeParJour, day);
                          b.nbBassinees ++;
                       }
                    }
                    
                    
                    //-- calcul des stats --//
                    EcluseBassinneeStat stat = new EcluseBassinneeStat();
                    results.add(stat);
                    stat.indiceEcluse = i;
                    stat.nomEcluse = ecluse.getName();
                    stat.bassinnee = new BassinneeStat();
                    stat.fausse = new BassinneeStat();
                    stat.bassinnee.min = nombreBassinneeParJour.size()>0?nombreBassinneeParJour.get(0).nbBassinees:0;
                    stat.bassinnee.max = nombreBassinneeParJour.size()>0?nombreBassinneeParJour.get(0).nbBassinees:0;
                     stat.fausse.min = nombreFausseBassinneeParJour.size()>0?nombreFausseBassinneeParJour.get(0).nbBassinees:0;
                    stat.fausse.max =  nombreFausseBassinneeParJour.size()>0?nombreFausseBassinneeParJour.get(0).nbBassinees:0;
                    double moyenneBassinees =0;
                    double moyenneFausseBassinnes=0;
                    for(BassinneeDuJour b: nombreBassinneeParJour) {
                        if(b.nbBassinees<stat.bassinnee.min )
                            stat.bassinnee.min  = b.nbBassinees;
                        if(b.nbBassinees>stat.bassinnee.max )
                            stat.bassinnee.max  = b.nbBassinees;
                        moyenneBassinees += b.nbBassinees;
                    }
                    if(nombreBassinneeParJour.size()>0)
                       moyenneBassinees = moyenneBassinees/nombreBassinneeParJour.size();
                    
                       stat.bassinnee.moy = moyenneBassinees;
                    
                    for(BassinneeDuJour b: nombreFausseBassinneeParJour) {
                        if(b.nbBassinees<stat.fausse.min )
                            stat.fausse.min  = b.nbBassinees;
                        if(b.nbBassinees>stat.fausse.max )
                            stat.fausse.max  = b.nbBassinees;
                        moyenneFausseBassinnes += b.nbBassinees;
                    }
                    if(nombreFausseBassinneeParJour.size()>0)
                       moyenneFausseBassinnes = moyenneFausseBassinnes/nombreFausseBassinneeParJour.size();
                       stat.fausse.moy = moyenneFausseBassinnes;
                  }
              }
             
             return results;
         }
	
}
