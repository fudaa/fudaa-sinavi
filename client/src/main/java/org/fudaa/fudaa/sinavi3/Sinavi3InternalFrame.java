package org.fudaa.fudaa.sinavi3;

import javax.swing.JInternalFrame;

import com.memoire.bu.BuIcon;
import com.memoire.bu.BuInternalFrame;

public class Sinavi3InternalFrame extends BuInternalFrame{

	public Sinavi3InternalFrame() {
		super();
		siporStyle();
		// TODO Auto-generated constructor stub
	}

	public Sinavi3InternalFrame(String title, boolean resizable, boolean closable, boolean maximizable, boolean iconifiable) {
		super(title, resizable, closable, maximizable, iconifiable);
		siporStyle();
		// TODO Auto-generated constructor stub
	}

/*	public SiporInternalFrame(String title, boolean resizable, boolean closable, boolean maximizable) {
		super(title, resizable, closable, maximizable);
		siporStyle();
		// TODO Auto-generated constructor stub
	}

	public SiporInternalFrame(String title, boolean resizable, boolean closable) {
		super(title, resizable, closable);
		siporStyle();
		// TODO Auto-generated constructor stub
	}

	public SiporInternalFrame(String title, boolean resizable) {
		super(title, resizable);
		siporStyle();
		// TODO Auto-generated constructor stub
	}

	public SiporInternalFrame(String title) {
		super(title);
		siporStyle();
		// TODO Auto-generated constructor stub
	}
*/
	protected void siporStyle(){
		BuIcon icon=Sinavi3Resource.SINAVI.getIcon("iconesipor");
		
		this.setFrameIcon(icon);
		this.repaint();
		
	}
	
}
