package org.fudaa.fudaa.sinavi3;

/**
 * 
 */
import java.awt.BorderLayout;
import java.awt.Component;
import java.util.EventObject;
import java.util.Observable;
import java.util.Observer;

import javax.swing.BorderFactory;
import javax.swing.CellEditor;
import javax.swing.DefaultCellEditor;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.border.Border;
import javax.swing.event.CellEditorListener;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;

import org.apache.commons.lang.StringUtils;

import com.memoire.bu.BuDialogError;
import com.memoire.bu.BuTable;

// import javax.swing.event.*;
/**
 * Panel d'affichage des donn�es des Navires sous forme d un tableau: avec la
 * possibilit� de modifier ou de supprimer une donn�e: NOTE TRES PRATIQUE
 * SUR LE FONCTIONNEMENT DES BuTable IL FAUT AFFICHER LE HEADER SEPAREMMENT SI
 * ON UTILISE PAS DE JSCROLLPANE DIRECTEMENT ON PEUT POUR CELA UTILISER LE
 * LAYOUT BORDERLAYOUT ET AFFICHER LE TABLEHEADER EN DEBUT DE FICHIER ET
 * AFFICHER LE TABLEAU AU CENTER
 * 
 * @author Adrien Hadoux
 */
public class Sinavi3PanelAffichageBateaux extends JPanel implements Observer {

	String[] titreColonnes_ = { "Nom", "priorite", "Longueur", "Largeur",
			"Vitesse avalant", "vitesse montant", "tirant eau",
			"Type (charge ou lege)", "Lundi.cr.jour.1", "Lundi.cr.jour.2",
			"Lundi.cr.jour.3" };

	/**
	 * Tableau de type BuTable qui contiendra les donn�es des quais
	 */

	Object[][] data_ = {};

	BuTable tableau_ = new BuTable(data_, titreColonnes_);
	TableModel modele_;

	/**
	 * Panel ascenceur
	 */
	JScrollPane ascenceur_;

	/**
	 * Bordure du tableau
	 */

	Border borduretab_ = BorderFactory.createLoweredBevelBorder();

	Sinavi3DataSimulation donnees_;
	

	/**
	 * constructeur du panel d'affichage des quais
	 * 
	 * @param d
	 *            donn�es de la simulation
	 */
	Sinavi3PanelAffichageBateaux(final Sinavi3DataSimulation d) {
		donnees_ = d;
		// -- enregistrement de l'observer aupres de l'observable --//
		d.addObservers(this);
		setLayout(new BorderLayout());

		this.maj(d);

	}

	/**
	 * Methode d'ajout d'une cellule
	 */
	void maj(final Sinavi3DataSimulation d) {

		System.out.println(" taille des navires: "
				+ d.listeBateaux_.listeNavires_.size());
		
		modele_ = new TableModel() {

			public int getRowCount() {
				return d.listeBateaux_.NombreNavires();
			}

			public int getColumnCount() {
				return titreColonnes_.length;
			}

			public String getColumnName(int columnIndex) {
				return titreColonnes_[columnIndex];
			}

			public Class<?> getColumnClass(int columnIndex) {

				return String.class;
			}

			public boolean isCellEditable(int rowIndex, int columnIndex) {
				return true;
			}

			public Object getValueAt(int rowIndex, int columnIndex) {
				
				return constructLine(d.listeBateaux_.retournerNavire(rowIndex))[columnIndex];
			}

			

			public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
				if(StringUtils.isBlank(aValue.toString())) {
					new BuDialogError(donnees_.application_.getApp(), donnees_.application_.isSinavi_, "Saisie manquante")
					.activate();
					return;
				}
				Sinavi3Bateau nav = d.listeBateaux_.retournerNavire(rowIndex);
				try{ 
				switch(columnIndex) {
				case 0: {
					if(d.listeBateaux_.existeDoublon(aValue.toString(), rowIndex)) {
						new BuDialogError(donnees_.application_.getApp(), donnees_.application_.isSinavi_, "Nom deja pris.")
						.activate();
						
					}else
						nav.setNom(aValue.toString());
				}break;
				case 1:nav.setPriorite(Integer.parseInt(aValue.toString()));break;
				case 2:nav.setLongueurMax(Double.parseDouble(aValue.toString()));break;
				case 3:nav.setLargeurMax(Double.parseDouble(aValue.toString()));break;
				case 4:nav.setVitesseDefautAvalant(Double.parseDouble(aValue.toString()));break;
				case 5:nav.setVitesseDefautMontant(Double.parseDouble(aValue.toString()));break;
				case 6:nav.setTirantEau(Double.parseDouble(aValue.toString()));break;
				case 7: nav.setTypeTirant(aValue.toString());break;
				
				}
				} catch(NumberFormatException e) {
					new BuDialogError(donnees_.application_.getApp(), donnees_.application_.isSinavi_, "La valeur doit �tre un nombre.")
					.activate();
				}
			}

			public void addTableModelListener(TableModelListener l) {
				// TODO Auto-generated method stub

			}

			public void removeTableModelListener(TableModelListener l) {
				// TODO Auto-generated method stub

			}

		};

		this.tableau_ = new BuTable(modele_);
		tableau_.setDefaultEditor(Object.class,new Sinavi3CellEditor());
		
		tableau_.getColumnModel().getColumn(1).setCellEditor(new DefaultCellEditor(new JComboBox<String>(new String[]{"0","1","2"})));
		tableau_.getColumnModel().getColumn(7).setCellEditor(new DefaultCellEditor(new JComboBox<String>(new String[]{"C","L"})));
		TableCellEditor editorCreneau = new TableCellEditor() {
			
			public boolean stopCellEditing() {
			return true;
			}
			
			public boolean shouldSelectCell(EventObject anEvent) {
				return true;
			}
			
			public void removeCellEditorListener(CellEditorListener l) {
			}
			
			public boolean isCellEditable(EventObject anEvent) {
				return true;
			}
			
			public Object getCellEditorValue() {
					return null;
			}
			
			public void cancelCellEditing() {
			}
			
			public void addCellEditorListener(CellEditorListener l) {
			}
			
			public Component getTableCellEditorComponent(JTable table, Object value,
					boolean isSelected, int row, int column) {					
				Sinavi3Bateau nav = d.listeBateaux_.retournerNavire(row);
				donnees_.application_.addInternalFrame(new Sinavi3FrameSaisieHorairesCompletSemaine(nav.creneauxJournaliers_,false));
					
				return null;
			}
		}; 
		tableau_.getColumnModel().getColumn(8).setCellEditor(editorCreneau);
		tableau_.getColumnModel().getColumn(9).setCellEditor(editorCreneau);
		tableau_.getColumnModel().getColumn(10).setCellEditor(editorCreneau);
		
		tableau_.getTableHeader();
		tableau_.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		tableau_.setBorder(this.borduretab_);

		for (int i = 0; i < titreColonnes_.length; i++) {
			TableColumn column = tableau_.getColumnModel().getColumn(i);
			column.setPreferredWidth(150);
		}

		TableColumn column = tableau_.getColumnModel().getColumn(1);
		column.setPreferredWidth(50);
		column = tableau_.getColumnModel().getColumn(2);
		column.setPreferredWidth(75);
		tableau_.getColumnModel().getColumn(3).setPreferredWidth(75);
		tableau_.getColumnModel().getColumn(4).setPreferredWidth(75);
		org.fudaa.fudaa.sinavi.factory.ColumnAutoSizer
				.sizeColumnsToFit(tableau_);
		tableau_.revalidate();
		this.removeAll();
		this.add(/* ascenceur */tableau_.getTableHeader(), BorderLayout.NORTH);
		this.add(new JScrollPane(tableau_), BorderLayout.CENTER);
		tableau_.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);

		this.revalidate();
		this.updateUI();
	}
	public Object[] constructLine(int line) {
		return constructLine(donnees_.getListeBateaux_().retournerNavire(line));
	}
	
	public Object[] constructLine(Sinavi3Bateau nav) {
		Object[] data = new Object[titreColonnes_.length];
		int cpt = 0;
		data[cpt++] = nav.nom;
		data[cpt++] = "" + nav.priorite;

		data[cpt++] = "" + (float) nav.longueurMax;
		data[cpt++] = "" + (float) nav.largeurMax;
		data[cpt++] = "" + (float) nav.vitesseDefautAvalant;
		data[cpt++] = "" + (float) nav.vitesseDefautMontant;

		data[cpt++] = "" + (float) nav.tirantEau;
		data[cpt++] = "" + nav.typeTirant;

		data[cpt++] = ""
				+ Sinavi3Horaire
						.format(nav.creneauxJournaliers_.lundiCreneau1HeureDep)
				+ " � "
				+ Sinavi3Horaire
						.format(nav.creneauxJournaliers_.lundiCreneau1HeureArrivee);
		data[cpt++] = ""
				+ Sinavi3Horaire
						.format(nav.creneauxJournaliers_.lundiCreneau2HeureDep)
				+ " � "
				+ Sinavi3Horaire
						.format(nav.creneauxJournaliers_.lundiCreneau2HeureArrivee);
		data[cpt++] = ""
				+ Sinavi3Horaire
						.format(nav.creneauxJournaliers_.lundiCreneau3HeureDep)
				+ " � "
				+ Sinavi3Horaire
						.format(nav.creneauxJournaliers_.lundiCreneau3HeureArrivee);
		
		return data;
	}

	public void update(Observable o, Object arg) {
		// TODO Auto-generated method stub
		if (arg.equals("gare"))
			maj(donnees_);
	}

}
