/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.sinavi3.ui.results.reports;

import java.util.ArrayList;
import java.util.List;
import jxl.format.Alignment;
import jxl.format.Border;
import jxl.format.BorderLineStyle;
import jxl.format.Colour;
import jxl.format.VerticalAlignment;
import jxl.write.BoldStyle;
import jxl.write.DateFormat;
import jxl.write.Label;
import jxl.write.NumberFormat;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WriteException;
import org.fudaa.dodico.corba.sinavi3.SParametresSinavi32;
import org.fudaa.fudaa.commun.projet.FudaaProjet;
import org.fudaa.fudaa.sinavi3.Sinavi3DataSimulation;
import org.fudaa.fudaa.sinavi3.Sinavi3Resource;

/**
 * Helpers for each
 * @author Adrien Hadoux
 */
public class Sinavi3SheetFactoryHelper {
     protected static WritableCellFormat cellFormatLight;
    protected static WritableCellFormat cellFormatTitle;
    protected static WritableCellFormat numberFormat;
    protected static WritableCellFormat integerFormat;
    protected static WritableCellFormat dateFormat;
    protected static List<WritableCellFormat> listeTitleFormat = new ArrayList<WritableCellFormat>();
 public static void setValueAt(int ligne, int colonne, WritableSheet sheet, int value) throws WriteException {
        sheet.addCell(new jxl.write.Number(colonne, ligne, value, cellFormatLight));
    }
    public static void setValueAt(int ligne, int colonne, WritableSheet sheet, double value) throws WriteException {
        sheet.addCell(new jxl.write.Number(colonne, ligne, value, numberFormat));
    }

    public static void setValueAt(int ligne, int colonne, WritableSheet sheet, String value) throws WriteException {
        setValueAt(ligne, colonne, sheet, value, cellFormatLight);
    }

    public static void setValueDateAt(int ligne, int colonne, WritableSheet sheet, String value) throws WriteException {

        setValueAt(ligne, colonne, sheet, value, dateFormat);
    }

    public static void setValueAt(int ligne, int colonne, WritableSheet sheet, String value, WritableCellFormat format) throws WriteException {
        sheet.addCell(new jxl.write.Label(colonne, ligne, value, format));
    }

    public static void setValueTitleAt(int ligne, int colonne, WritableSheet sheet, String value) throws WriteException {
        setValueTitleAt(ligne, colonne, sheet, value, Colour.YELLOW);
    }

    public static void setValueTitleAt(int ligne, int colonne, WritableSheet sheet, String value, Colour color) throws WriteException {
        WritableCellFormat format = findFormatByColour(color);
        sheet.addCell(new Label(colonne, ligne, value, format));
    }

    public static WritableCellFormat findFormatByColour(Colour color) throws WriteException {
        for (WritableCellFormat format : listeTitleFormat) {
            if (format.getBackgroundColour() == color) {
                return format;
            }

        }
        WritableFont cellFont = new WritableFont(WritableFont.TIMES, 12,WritableFont.BOLD);
        cellFont.setColour(Colour.BLACK);
        WritableCellFormat newFormatTitle = new WritableCellFormat(cellFont);
        newFormatTitle.setBackground(color);
        newFormatTitle.setAlignment(Alignment.CENTRE);
        newFormatTitle.setVerticalAlignment(VerticalAlignment.CENTRE);
        newFormatTitle.setBorder(Border.ALL, BorderLineStyle.THIN);
        newFormatTitle.setWrap(true);
        listeTitleFormat.add(newFormatTitle);
        return newFormatTitle;

    }

    public static void fusionneCells(int ligne, int colonne, int nbCells, boolean horizontal, WritableSheet sheet) throws WriteException {
        if(nbCells<=1)
            return;
        if (horizontal) {
            sheet.mergeCells(colonne, ligne, (colonne + nbCells - 1), ligne);
        } else {
            sheet.mergeCells(colonne, ligne, colonne, (ligne + nbCells - 1));
        }
    }
    
    
    
    public static void initFormat() throws WriteException {
        WritableFont cellFont = new WritableFont(WritableFont.TIMES, 12);
        cellFont.setColour(Colour.BLACK);
        cellFormatLight = new WritableCellFormat(cellFont);
        cellFormatLight.setAlignment(Alignment.CENTRE);
        cellFormatLight.setVerticalAlignment(VerticalAlignment.CENTRE);
        cellFormatLight.setBorder(Border.ALL, BorderLineStyle.THIN);
        cellFormatLight.setWrap(true);

        cellFormatTitle = new WritableCellFormat(cellFont);
        cellFormatTitle.setBackground(Colour.YELLOW);
        cellFormatTitle.setAlignment(Alignment.CENTRE);
        cellFormatTitle.setVerticalAlignment(VerticalAlignment.CENTRE);
        cellFormatTitle.setBorder(Border.ALL, BorderLineStyle.THIN);
        cellFormatTitle.setWrap(true);

        numberFormat = new WritableCellFormat(cellFont, new NumberFormat("#0.00"));
        numberFormat.setAlignment(Alignment.CENTRE);
        numberFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
        numberFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
        numberFormat.setWrap(true);

        integerFormat = new WritableCellFormat(cellFont, new NumberFormat("#0.0"));
        integerFormat.setAlignment(Alignment.CENTRE);
        integerFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
        integerFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
        integerFormat.setWrap(true);

        DateFormat dtDuration = new DateFormat("hh:mm");
        dateFormat = new WritableCellFormat(dtDuration);
        dateFormat.setAlignment(Alignment.CENTRE);
        dateFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
        dateFormat.setBorder(Border.ALL, BorderLineStyle.THIN);
        dateFormat.setWrap(true);
    }
    
    
    public static Sinavi3DataSimulation getSimulationFromFudaaProjet(final FudaaProjet projet) {
        SParametresSinavi32 params = (SParametresSinavi32) projet.getParam(Sinavi3Resource.parametres);
        Sinavi3DataSimulation data = new Sinavi3DataSimulation();
        data.setParams_(params);
        data.getParametresProjet();
        data.setProjectName(projet.getInformations().titre);
        return data;
    }
    
    
    public static int getIntValue(Object[] data, int indice) {
        if(data[indice] == null )
            return -1;
        return Integer.parseInt(data[indice].toString());
    }
    
}
