/*
 * @file         SiporResource.java
 * @creation     1999-10-01
 * @modification $Date: 2007-11-23 11:28:02 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.sinavi3;

import org.fudaa.fudaa.ressource.FudaaResource;

import com.memoire.bu.BuResource;

/**
 * Les ressources de Sipor.
 * 
 * @version $Revision: 1.1 $ $Date: 2007-11-23 11:28:02 $ by $Author: hadouxad $
 * @author Nicolas Chevalier
 */
public class Sinavi3Resource extends FudaaResource {
  protected Sinavi3Resource(BuResource _parent) {
		super(_parent);
		// TODO Auto-generated constructor stub
	}
/** structure de parametres (SParametresSipor) */
  public final static String parametres = "parametres";
  /** T�moin pour l'utilisation du bassin 1 (bool�en). */
  public final static String utilisationBassin1 = "utilisationBassin1";
  /** T�moin pour l'utilisation du bassin 2 (bool�en). */
  public final static String utilisationBassin2 = "utilisationBassin2";
  /** T�moin pour l'utilisation du bassin 3 (bool�en). */
  public final static String utilisationBassin3 = "utilisationBassin3";
  /** R�sultats des it�rations (SResultatsIterations). */
  public final static String resultats = "resultats";
  public final static Sinavi3Resource SINAVI = new Sinavi3Resource(FudaaResource.FUDAA);
}
