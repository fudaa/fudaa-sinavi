package org.fudaa.fudaa.sinavi3;

import jxl.write.Label;
import jxl.write.Number;
import jxl.write.WritableCell;

import org.fudaa.ctulu.CtuluLibString;

public class Sinavi3Modeletrajets extends Sinavi3ModeleExcel {

	
	//-- donnees de la simulation --//
	private Sinavi3DataSimulation donnees_;
	
	//-- Bateaux visualisé --//
	private int bateauVisualise=0;
	
	private boolean afficheTout=false;
	private int compteurTotal=0;
	Object[][] data=null;
	//-- le contenu des colonnes du modele du tableau --//
	private String[] titreColonnes_={"Catégorie","Gare de départ","Gare d'arrivée","Sens","Type de loi","Ordre ou σ/m","Horaire Erlang","Nb. bateaux attendus"};
	
	
	public Sinavi3Modeletrajets(Sinavi3DataSimulation _d){
		donnees_=_d;
	}
	
	public int getColumnCount() {
		// TODO Auto-generated method stub
		
		return titreColonnes_.length;
	}

	public int getRowCount() {
		// TODO Auto-generated method stub
		if(!afficheTout)
		return donnees_.listeBateaux_.retournerNavire(bateauVisualise).listeTrajet_.size()   ;
		else
			return compteurTotal;
	}

	
	//-- getter du bateau visualisé --//
	public int getBateauVisualise() {
		return bateauVisualise;
	}

	public void setBateauVisualise(int bateauVisualise) {
		
		if(bateauVisualise<donnees_.listeBateaux_.listeNavires_.size()){
		this.bateauVisualise = bateauVisualise;
		afficheTout=false;
		}
		else {
			afficheTout=true;
//		-- affichage de l'ensemble des trajets --//
		
		compteurTotal=0;
		int maxTrajet=0;
		for(int i=0;i<donnees_.listeBateaux_.listeNavires_.size();i++)
		if(donnees_.listeBateaux_.retournerNavire(i).listeTrajet_.size()>maxTrajet)
			maxTrajet=donnees_.listeBateaux_.retournerNavire(i).listeTrajet_.size();
		
		data=new Object[donnees_.listeBateaux_.listeNavires_.size()*maxTrajet][8];
		
		for(int i=0;i<donnees_.listeBateaux_.listeNavires_.size();i++){
			
			Sinavi3Trajets trajets=donnees_.listeBateaux_.retournerNavire(i).listeTrajet_;
			
			for(int k=0;k<trajets.size();k++)
			{
				Sinavi3Trajet trajet=trajets.retournerTrajet(k);
				data[compteurTotal][0]=donnees_.listeBateaux_.retournerNavire(i).nom;
				data[compteurTotal][1]=donnees_.listeGare_.retournerGare(trajet.gareDepart_);
				data[compteurTotal][2]=donnees_.listeGare_.retournerGare(trajet.gareArrivee_);
				if(trajet.sens_==0)
				data[compteurTotal][3]="avalant";
				else data[compteurTotal][3]="montant";
				if(trajet.typeLoiGenerationNavires_==0)
					data[compteurTotal][4]="erlang";
				else if(trajet.typeLoiGenerationNavires_==1)
					data[compteurTotal][4]="déterministe";
				else data[compteurTotal][4]="journaliere";
				if(trajet.typeLoiGenerationNavires_==0){
					data[compteurTotal][5]=""+trajet.loiErlangGenerationNavire;
					data[compteurTotal][6]=trajet.erlangDebut+"-"+trajet.erlangFin+"H.Min";
					data[compteurTotal][7]=""+trajet.nbBateauxattendus;
				}
				compteurTotal++;
			}
			
			
			
		}}
		
	}
	
	
	/** retourne le double correspondant au parcours **/
	public Object getValueAt(int i, int j) {
		
		//-- recuperation du bateau qui nous interesse --//
		Sinavi3Bateau bateau;
		if(!afficheTout){
		bateau=donnees_.listeBateaux_.retournerNavire(this.bateauVisualise);
		
		
		switch(j){
		case 0:	return bateau.nom;
		case 1:	return donnees_.listeGare_.retournerGare(bateau.listeTrajet_.retournerTrajet(i).gareDepart_);
		case 2:	return donnees_.listeGare_.retournerGare(bateau.listeTrajet_.retournerTrajet(i).gareArrivee_);
		case 3:	if(bateau.listeTrajet_.retournerTrajet(i).sens_==0)
					return "avalant";
					else return "montant";
		case 4:	if(bateau.listeTrajet_.retournerTrajet(i).typeLoiGenerationNavires_==0)
			return "erlang";
					else if(bateau.listeTrajet_.retournerTrajet(i).typeLoiGenerationNavires_==2)
			return "journaliere";
			else return "deterministe";
		case 5:	if(bateau.listeTrajet_.retournerTrajet(i).typeLoiGenerationNavires_==0)
			return ""+getErlangOrEcartType(bateau,i);
					else return "";
		case 6:if(bateau.listeTrajet_.retournerTrajet(i).typeLoiGenerationNavires_==0)
			return ""+bateau.listeTrajet_.retournerTrajet(i).erlangDebut+"-"+bateau.listeTrajet_.retournerTrajet(i).erlangFin;
		else return "";
		case 7:if(bateau.listeTrajet_.retournerTrajet(i).typeLoiGenerationNavires_==0)
			return ""+bateau.listeTrajet_.retournerTrajet(i).nbBateauxattendus;
		else return "";
		default: return null;				
		}
		}
		else{
			//-- on affiche la totalité des trajets de la flotte --//
			return data[i][j];
			
		}
	}

	
	private String getErlangOrEcartType(Sinavi3Bateau bateau, final int i) {
		if(bateau.listeTrajet_.retournerTrajet(i).typeErlangLoi==0)
			return "" + bateau.listeTrajet_.retournerTrajet(i).loiErlangGenerationNavire;
		else 
			return "" + bateau.listeTrajet_.retournerTrajet(i).loiErlangEcartType;
	}

	//-- jamais éditable!! --//
	public boolean isCellEditable(int row, int col) {
		return false;
	}

	public String getColumnName(final int _i) {
	    return titreColonnes_[_i];

	  }
	
	public int getMaxCol() {
	    // TODO Auto-generated method stub
	    return getColumnCount();
	  }

	  /**
	   * retourne le nombre de ligne
	   */
	  public int getMaxRow() {
	    // TODO Auto-generated method stub
	    return getRowCount();
	  }
	  
	  public WritableCell getExcelWritable(final int _row, final int _col, int _rowXls, int _colXls) {
		    final int r = _row;
		    final int c = _col;
		    /*
		     * if (column_ != null) c = column_[c]; if (row_ != null) { r = row_[r]; }
		     */
		    final Object o = getValueAt(r,c);
		    if (o == null) {
		      return null;
		    }
		    String s = o.toString();
		    if(CtuluLibString.isEmpty(s)) return null;
		    try {
		      return new Number(_colXls, _rowXls, Double.parseDouble(s));
		    } catch (final NumberFormatException e) {}
		    return new Label(_colXls, _rowXls, s);

		    // return cell;
		  }
}
