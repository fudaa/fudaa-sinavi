/*
 * @file         SiporApplication.java
 * @creation     1999-10-01
 * @modification $Date: 2007-11-23 11:28:02 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.sinavi3;

import com.memoire.bu.BuApplication;

/**
 * L'application cliente Sipor.
 * 
 * @version $Revision: 1.1 $ $Date: 2007-11-23 11:28:02 $ by $Author: hadouxad $
 * @author Nicolas Chevalier , Bertrand Audinet
 */
public class Sinavi3Application extends BuApplication {
  public Sinavi3Application() {
    super();
    setImplementation(new Sinavi3Implementation());
  }
}
