/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.sinavi3.ui.results.reports;

import java.util.ArrayList;
import java.util.List;
import jxl.format.Colour;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import org.fudaa.fudaa.commun.projet.FudaaProjet;
import org.fudaa.fudaa.sinavi3.Sinavi3DataSimulation;
import org.fudaa.fudaa.sinavi3.Sinavi3ResultatsAttenteTrajet;
import org.fudaa.fudaa.sinavi3.Sinavi3TraduitHoraires;

/**
 *
 * @author Adrien Hadoux
 */
public class Sinavi3ReportAttemtesTrajets extends Sinavi3SheetFactoryHelper{
    
    
    
    public static class ValueAttente {
         int nbBateauxTotalSens;
         int nbNavires;
         String totalAttente;
         float totalAttenteValue;
         float moyenne;
         float moyenneFlotte;
         float moyennePercentFlotte;
    }

    public static List<ValueAttente> produceSheetAttentesTrajetsValues(Sinavi3DataSimulation data, WritableSheet sheet, int indiceLigne, 
            int tronconE, int tronconS, int typeE, int typeS, int sens) throws WriteException {
    Object[][] results = null;
        //--sens avalant --//
        results = Sinavi3ResultatsAttenteTrajet.computeData(data, tronconE, typeE, tronconS, typeS, sens);
        List<ValueAttente> values = new ArrayList<ValueAttente>();
        ValueAttente v = new ValueAttente();
        values.add(v);
         //-- afficher la flotte --//
        v.nbBateauxTotalSens =0;
        for(int i=0;i<results.length;i++) {
            if(results[i][1] != null)
            v.nbBateauxTotalSens = v.nbBateauxTotalSens +getIntValue(results[i],1);
        } 
        int nbBateauxTotalSens = v.nbBateauxTotalSens;
        setValueAt(indiceLigne, 5, sheet, v.nbBateauxTotalSens);
        
        
        
        Object[] dataTotal = results[results.length-1];
        //-- afficher valeurs attente secu --//
        v.nbBateauxTotalSens = nbBateauxTotalSens;
        v.nbNavires = getIntValue(dataTotal,4);
        v.totalAttente = dataTotal[2].toString();
        v.totalAttenteValue = Sinavi3TraduitHoraires.traduitReverseMinutes(v.totalAttente);
        v.moyenne = v.nbNavires != 0?v.totalAttenteValue /    (float)v.nbNavires:0;     
        v.moyenneFlotte = v.nbBateauxTotalSens != 0?v.totalAttenteValue /    (float)v.nbBateauxTotalSens:0;     
        v.moyennePercentFlotte = v.nbBateauxTotalSens != 0?(v.nbNavires /(float)v.nbBateauxTotalSens)*100.0f:0;     
        setValueDateAt(indiceLigne, 7, sheet, v.totalAttente);
        setValueAt(indiceLigne, 8, sheet, v.nbNavires);
        setValueDateAt(indiceLigne, 9, sheet, Sinavi3TraduitHoraires.traduitMinutesEnHeuresMinutes3(v.moyenne));
        setValueDateAt(indiceLigne, 10, sheet, Sinavi3TraduitHoraires.traduitMinutesEnHeuresMinutes3(v.moyenneFlotte));
        setValueAt(indiceLigne,11, sheet, v.moyennePercentFlotte);
        
        //-- afficher valeurs attente acces --//
        v = new ValueAttente();
        values.add(v);
        v.nbBateauxTotalSens = nbBateauxTotalSens;
        v.nbNavires = getIntValue(dataTotal,9);
        v.totalAttente = dataTotal[7].toString();
        v.totalAttenteValue = Sinavi3TraduitHoraires.traduitReverseMinutes(v.totalAttente);
        v.moyenne = v.nbNavires != 0?v.totalAttenteValue /    (float)v.nbNavires:0;     
        v.moyenneFlotte = v.nbBateauxTotalSens != 0?v.totalAttenteValue /    (float)v.nbBateauxTotalSens:0;     
        v.moyennePercentFlotte = v.nbBateauxTotalSens != 0?(v.nbNavires /(float)v.nbBateauxTotalSens)*100.0f:0;     
        setValueDateAt(indiceLigne, 13, sheet, v.totalAttente);
        setValueAt(indiceLigne, 14, sheet, v.nbNavires);
        setValueDateAt(indiceLigne, 15, sheet, Sinavi3TraduitHoraires.traduitMinutesEnHeuresMinutes3(v.moyenne));
        setValueDateAt(indiceLigne, 16, sheet, Sinavi3TraduitHoraires.traduitMinutesEnHeuresMinutes3(v.moyenneFlotte));
        setValueAt(indiceLigne,17, sheet, v.moyennePercentFlotte);
        
       //-- afficher valeurs attente occup --//
        v = new ValueAttente();
        values.add(v);
        v.nbBateauxTotalSens = nbBateauxTotalSens;
        v.nbNavires = getIntValue(dataTotal,14);
        v.totalAttente = dataTotal[12].toString();
        v.totalAttenteValue = Sinavi3TraduitHoraires.traduitReverseMinutes(v.totalAttente);
        v.moyenne = v.nbNavires != 0?v.totalAttenteValue /    (float)v.nbNavires:0;     
        v.moyenneFlotte = v.nbBateauxTotalSens != 0?v.totalAttenteValue /    (float)v.nbBateauxTotalSens:0;     
        v.moyennePercentFlotte = v.nbBateauxTotalSens != 0?(v.nbNavires /(float)v.nbBateauxTotalSens)*100.0f:0;     
        setValueDateAt(indiceLigne, 19, sheet, v.totalAttente);
        setValueAt(indiceLigne, 20, sheet, v.nbNavires);    
        setValueDateAt(indiceLigne, 21, sheet, Sinavi3TraduitHoraires.traduitMinutesEnHeuresMinutes3(v.moyenne));
        setValueDateAt(indiceLigne, 22, sheet, Sinavi3TraduitHoraires.traduitMinutesEnHeuresMinutes3(v.moyenneFlotte));
        setValueAt(indiceLigne,23, sheet, v.moyennePercentFlotte);
        
        //-- afficher valeurs attente indispo --//
        v = new ValueAttente();
        values.add(v);
        v.nbBateauxTotalSens = nbBateauxTotalSens;
        v.nbNavires = getIntValue(dataTotal,19);
        v.totalAttente = dataTotal[17].toString();
        v.totalAttenteValue = Sinavi3TraduitHoraires.traduitReverseMinutes(v.totalAttente);
        v.moyenne = v.nbNavires != 0?v.totalAttenteValue /    (float)v.nbNavires:0;     
        v.moyenneFlotte = v.nbBateauxTotalSens != 0?v.totalAttenteValue /    (float)v.nbBateauxTotalSens:0;     
        v.moyennePercentFlotte = v.nbBateauxTotalSens != 0?(v.nbNavires /(float)v.nbBateauxTotalSens)*100.0f:0;     
        setValueDateAt(indiceLigne, 25, sheet, v.totalAttente);
        setValueAt(indiceLigne, 26, sheet, v.nbNavires);
        setValueDateAt(indiceLigne, 27, sheet, Sinavi3TraduitHoraires.traduitMinutesEnHeuresMinutes3(v.moyenne));
        setValueDateAt(indiceLigne, 28, sheet, Sinavi3TraduitHoraires.traduitMinutesEnHeuresMinutes3(v.moyenneFlotte));
        setValueAt(indiceLigne,29, sheet, v.moyennePercentFlotte);
        
       //-- afficher valeurs attente totale --//
        v = new ValueAttente();
        values.add(v);
        v.nbBateauxTotalSens = nbBateauxTotalSens;
        v.nbNavires = getIntValue(dataTotal,24);
        v.totalAttente = dataTotal[22].toString();
        v.totalAttenteValue = Sinavi3TraduitHoraires.traduitReverseMinutes(v.totalAttente);
        v.moyenne = v.nbNavires != 0?v.totalAttenteValue /    (float)v.nbNavires:0;     
        v.moyenneFlotte = v.nbBateauxTotalSens != 0?v.totalAttenteValue /    (float)v.nbBateauxTotalSens:0;     
        v.moyennePercentFlotte = v.nbBateauxTotalSens != 0?(v.nbNavires /(float)v.nbBateauxTotalSens)*100.0f:0;     
        setValueDateAt(indiceLigne, 31, sheet, v.totalAttente);
        setValueAt(indiceLigne, 32, sheet, v.nbNavires);
        setValueDateAt(indiceLigne, 33, sheet, Sinavi3TraduitHoraires.traduitMinutesEnHeuresMinutes3(v.moyenne));
        setValueDateAt(indiceLigne, 34, sheet,Sinavi3TraduitHoraires.traduitMinutesEnHeuresMinutes3( v.moyenneFlotte));
        setValueAt(indiceLigne,35, sheet, v.moyennePercentFlotte);
        
       
        
        return values;

    }
    
    
    public static  ValueAttente compute2sens(ValueAttente avalant, ValueAttente montant) {
        ValueAttente res = new ValueAttente();
         int nbBateauxTotalSens = (avalant.nbBateauxTotalSens + montant.nbBateauxTotalSens) / 2;
       
        res.totalAttenteValue = (avalant.totalAttenteValue + montant.totalAttenteValue)/2.0f;
        res.totalAttente =  Sinavi3TraduitHoraires.traduitMinutesEnHeuresMinutes3( res.totalAttenteValue );
        res.nbNavires = (avalant.nbNavires + montant.nbNavires)/2;
        res.moyenne = res.nbNavires!=0?(res.totalAttenteValue/ (float)res.nbNavires):0;
        res.moyenneFlotte = nbBateauxTotalSens!=0?(res.totalAttenteValue/ (float)nbBateauxTotalSens):0;
        res.moyennePercentFlotte = nbBateauxTotalSens!=0?((float)((double)res.nbNavires/ (double)nbBateauxTotalSens)*100.0f):0;
        return res;
    }
    
    public static void produceSheetAttentesTrajetsValues(Sinavi3DataSimulation data, WritableSheet sheet, int indiceLigne,
            List<ValueAttente> avalant, List<ValueAttente> montant) throws WriteException {

        
        int nbBateauxTotalSens = (avalant.get(0).nbBateauxTotalSens + montant.get(0).nbBateauxTotalSens) / 2;
        setValueAt(indiceLigne, 5, sheet, nbBateauxTotalSens);
        
        
        //-- afficher valeurs attente secu --//
        int cpt = 0;
        ValueAttente v = compute2sens(avalant.get(cpt), montant.get(cpt));
        cpt++;
        setValueDateAt(indiceLigne, 7, sheet, v.totalAttente);
        setValueAt(indiceLigne, 8, sheet, v.nbNavires);
        setValueDateAt(indiceLigne, 9, sheet, Sinavi3TraduitHoraires.traduitMinutesEnHeuresMinutes3(v.moyenne));
        setValueDateAt(indiceLigne, 10, sheet, Sinavi3TraduitHoraires.traduitMinutesEnHeuresMinutes3(v.moyenneFlotte));
        setValueAt(indiceLigne, 11, sheet, v.moyennePercentFlotte);

        //-- afficher valeurs attente acces --//
        v = compute2sens(avalant.get(cpt), montant.get(cpt));
        cpt++;
        setValueDateAt(indiceLigne, 13, sheet, v.totalAttente);
        setValueAt(indiceLigne, 14, sheet, v.nbNavires);
        setValueDateAt(indiceLigne, 15, sheet, Sinavi3TraduitHoraires.traduitMinutesEnHeuresMinutes3(v.moyenne));
        setValueDateAt(indiceLigne, 16, sheet, Sinavi3TraduitHoraires.traduitMinutesEnHeuresMinutes3(v.moyenneFlotte));
        setValueAt(indiceLigne, 17, sheet, v.moyennePercentFlotte);

        //-- afficher valeurs attente occup --//
        v = compute2sens(avalant.get(cpt), montant.get(cpt));
        cpt++;
        setValueDateAt(indiceLigne, 19, sheet, v.totalAttente);
        setValueAt(indiceLigne, 20, sheet, v.nbNavires);
        setValueDateAt(indiceLigne, 21, sheet, Sinavi3TraduitHoraires.traduitMinutesEnHeuresMinutes3(v.moyenne));
        setValueDateAt(indiceLigne, 22, sheet, Sinavi3TraduitHoraires.traduitMinutesEnHeuresMinutes3(v.moyenneFlotte));
        setValueAt(indiceLigne, 23, sheet, v.moyennePercentFlotte);

        //-- afficher valeurs attente indispo --//
        v = compute2sens(avalant.get(cpt), montant.get(cpt));
        cpt++;
        setValueDateAt(indiceLigne, 25, sheet, v.totalAttente);
        setValueAt(indiceLigne, 26, sheet, v.nbNavires);
        setValueDateAt(indiceLigne, 27, sheet, Sinavi3TraduitHoraires.traduitMinutesEnHeuresMinutes3(v.moyenne));
        setValueDateAt(indiceLigne, 28, sheet, Sinavi3TraduitHoraires.traduitMinutesEnHeuresMinutes3(v.moyenneFlotte));
        setValueAt(indiceLigne, 29, sheet, v.moyennePercentFlotte);

        //-- afficher valeurs attente totale --//
        v = compute2sens(avalant.get(cpt), montant.get(cpt));
        cpt++;
        setValueDateAt(indiceLigne, 31, sheet, v.totalAttente);
        setValueAt(indiceLigne, 32, sheet, v.nbNavires);
        setValueDateAt(indiceLigne, 33, sheet,Sinavi3TraduitHoraires.traduitMinutesEnHeuresMinutes3( v.moyenne));
        setValueDateAt(indiceLigne, 34, sheet, Sinavi3TraduitHoraires.traduitMinutesEnHeuresMinutes3(v.moyenneFlotte));
        setValueAt(indiceLigne, 35, sheet, v.moyennePercentFlotte);



    }
    
    public static void produceSheetAttentesTrajetsValues(Sinavi3DataSimulation data, WritableSheet sheet, int indiceLigne, int tronconE, int tronconS, int typeE, int typeS) throws WriteException {
        
        
         setValueDateAt(indiceLigne, 4, sheet, "Avalants");
        setValueDateAt(indiceLigne+1, 4, sheet, "Montants");
        setValueDateAt(indiceLigne+2, 4, sheet, "Moy. / 2 sens");
        
        //-- ecrire la ligne correspondante au sens avalant =0 --//
        List<ValueAttente> avalant = produceSheetAttentesTrajetsValues(data, sheet, indiceLigne, tronconE, tronconS, typeE, typeS,0);
        
        //-- ecrire la ligne correspondante au sens montant = 1 --//
        List<ValueAttente> montant = produceSheetAttentesTrajetsValues(data, sheet, indiceLigne+1, tronconE, tronconS, typeE, typeS,1);
        
        //-- les formules s'�crivent � la troisi�me ligne
        indiceLigne= indiceLigne +2;
        produceSheetAttentesTrajetsValues(data, sheet, indiceLigne, avalant, montant);
        

    }

    public static void produceSheetAttentesTrajetsHeader(WritableSheet sheet, boolean multiSimulation, Sinavi3DataSimulation data) throws WriteException {
        // taille de la colonne
        sheet.setColumnView(0, 30);
        sheet.setColumnView(1, 5);
        sheet.setColumnView(6, 5);
        sheet.setColumnView(2, 20);
        sheet.setColumnView(3, 20);
        sheet.setColumnView(4, 20);
        sheet.setColumnView(5, 20);
        sheet.setRowView(6, 38*20);
        sheet.setRowView(1, 38*20);
        setValueTitleAt(0, 0, sheet, "Attente totale", Colour.WHITE);
        if (!multiSimulation) {
            setValueTitleAt(1, 0, sheet, " R�sultats d'un seul sc�nario de simulation", Colour.WHITE);
            setValueTitleAt(5, 0, sheet, "Sc�nario", Colour.TURQOISE2);
            fusionneCells(5, 0, 2, false, sheet);
        } else {
            setValueTitleAt(1, 0, sheet, "R�sultats de plusieurs sc�narios de simulation", Colour.WHITE);
            setValueTitleAt(5, 0, sheet, "Sc�nario", Colour.TURQOISE2);
            
        }

        setValueTitleAt(5, 2, sheet, "Trajet", Colour.LIGHT_GREEN);
        fusionneCells(5, 2, 2, true, sheet);
        setValueTitleAt(6, 2, sheet, "Entr�e �l�ment 1", Colour.GREEN);
        setValueTitleAt(6, 3, sheet, "Sortie �l�ment 2", Colour.GREEN);

        setValueTitleAt(5, 4, sheet, "Sens", Colour.TURQOISE2);
        setValueTitleAt(5, 5, sheet, "Flotte", Colour.TURQOISE2);
        fusionneCells(5, 4, 2, false, sheet);
        fusionneCells(5, 5, 2, false, sheet);
        
        setValueTitleAt(5, 7, sheet, "Attente s�curit�", Colour.LIGHT_ORANGE);
        fusionneCells(5, 7, 5, true, sheet);
        setValueTitleAt(6, 7, sheet, "Total(h:min)");
        setValueTitleAt(6, 8, sheet, "Nb Bat");
        setValueTitleAt(6, 9, sheet, "Moy. / Nb Bat");
        setValueTitleAt(6, 10, sheet, "Moy. / Flotte");
        setValueTitleAt(6, 11, sheet, "% Nb bat / flotte");
        
        setValueTitleAt(5, 13, sheet, "Attente acc�s", Colour.LIGHT_ORANGE);
        fusionneCells(5, 13, 5, true, sheet);
        setValueTitleAt(6, 13, sheet, "Total(h:min)");
        setValueTitleAt(6, 14, sheet, "Nb Bat");
        setValueTitleAt(6, 15, sheet, "Moy. / Nb Bat");
        setValueTitleAt(6, 16, sheet, "Moy. / Flotte");
        setValueTitleAt(6, 17, sheet, "% Nb bat / flotte");
        
        
        setValueTitleAt(5, 19, sheet, "Attente occupation", Colour.LIGHT_ORANGE);
        fusionneCells(5, 19, 5, true, sheet);
        setValueTitleAt(6, 19, sheet, "Total(h:min)");
        setValueTitleAt(6, 20, sheet, "Nb Bat");
        setValueTitleAt(6, 21, sheet, "Moy. / Nb Bat");
        setValueTitleAt(6, 22, sheet, "Moy. / Flotte");
        setValueTitleAt(6, 23, sheet, "% Nb bat / flotte");
        
        setValueTitleAt(5, 25, sheet, "Attente Indisponibilit�", Colour.LIGHT_ORANGE);
        fusionneCells(5, 25, 5, true, sheet);
        setValueTitleAt(6, 25, sheet, "Total(h:min)");
        setValueTitleAt(6, 26, sheet, "Nb Bat");
        setValueTitleAt(6, 27, sheet, "Moy. / Nb Bat");
        setValueTitleAt(6, 28, sheet, "Moy. / Flotte");
        setValueTitleAt(6, 29, sheet, "% Nb bat / flotte");
        
        setValueTitleAt(5, 31, sheet, "Attente Totale", Colour.LIGHT_ORANGE);
        fusionneCells(5, 31, 5, true, sheet);
        setValueTitleAt(6, 31, sheet, "Total(h:min)");
        setValueTitleAt(6, 32, sheet, "Nb Bat");
        setValueTitleAt(6, 33, sheet, "Moy. / Nb Bat");
        setValueTitleAt(6, 34, sheet, "Moy. / Flotte");
        setValueTitleAt(6, 35, sheet, "% Nb bat / flotte");
      
    }

    public static WritableSheet produceSheetAttentesTrajets(final WritableWorkbook classeur, final Sinavi3DataSimulation data, boolean multiSimulation, 
            List<Sinavi3SheetProducer.TrajetReport> listeTrajets) throws WriteException {
        WritableSheet sheet = classeur.createSheet("Attente Totale", 0);

        produceSheetAttentesTrajetsHeader(sheet, multiSimulation, data);
        List<Sinavi3DataSimulation> listeComparison = new ArrayList<Sinavi3DataSimulation>();
        if (multiSimulation) {
            for (FudaaProjet projet : data.getApplication().getSimulations()) {
                if (data.getProjet_() != projet) {
                    Sinavi3DataSimulation comparison = getSimulationFromFudaaProjet(projet);
                    listeComparison.add(comparison);
                }
            }
        }

        //-- calculs avalant--//
        int indiceLigne = 8;
        if(listeTrajets != null) {
        
            for (Sinavi3SheetProducer.TrajetReport trajet: listeTrajets) {
                setValueAt(indiceLigne, 2, sheet, trajet.etypeElement==0?data.getListeBief_().retournerBief(trajet.eelement).getName(): data.getListeEcluse().retournerEcluse(trajet.eelement).getName());
                setValueAt(indiceLigne, 3, sheet, trajet.stypeElement==0?data.getListeBief_().retournerBief(trajet.selement).getName(): data.getListeEcluse().retournerEcluse(trajet.selement).getName());
            
                produceSheetAttentesTrajetsValues(data, sheet, indiceLigne, trajet.eelement, trajet.selement, trajet.etypeElement,trajet.stypeElement);
                
                if (multiSimulation) {
                    fusionneCells(indiceLigne, 2, (listeComparison.size()+1)*3 + listeComparison.size(), false, sheet);
                    fusionneCells(indiceLigne, 3, (listeComparison.size()+1)*3 +listeComparison.size(), false, sheet);
                    setValueTitleAt(indiceLigne, 0, sheet, "Sc�nario de r�f�rence", Colour.WHITE);
                    fusionneCells(indiceLigne, 0, 3, false, sheet);
                    indiceLigne+=4;
                    for (Sinavi3DataSimulation comparison : listeComparison) {
                        setValueTitleAt(indiceLigne, 0, sheet, "Sc�nario " + comparison.getProjectName_(), Colour.WHITE);
                         fusionneCells(indiceLigne, 0, 3, false, sheet);
                        produceSheetAttentesTrajetsValues(comparison, sheet, indiceLigne, trajet.eelement, trajet.selement,trajet.etypeElement, trajet.stypeElement);
                        indiceLigne+=4;
                    }
                }else {
                fusionneCells(indiceLigne, 2, 3, false, sheet);
                fusionneCells(indiceLigne, 3, 3, false, sheet);
                indiceLigne+=4;
            }
          }
            
        }else {
            
        
        for (int tronconE = 0; tronconE < data.getListeBief_().getListeBiefs_().size(); tronconE++) {
            for (int tronconS = 0; tronconS < data.getListeBief_().getListeBiefs_().size(); tronconS++) {
                setValueAt(indiceLigne, 2, sheet, data.getListeBief_().retournerBief(tronconE).getName());
                setValueAt(indiceLigne, 3, sheet, data.getListeBief_().retournerBief(tronconS).getName());
                                
                produceSheetAttentesTrajetsValues(data, sheet, indiceLigne, tronconE, tronconS, 0, 0);
                
                if (multiSimulation) {
                    fusionneCells(indiceLigne, 2, (listeComparison.size()+1)*3 + listeComparison.size(), false, sheet);
                    fusionneCells(indiceLigne, 3, (listeComparison.size()+1)*3 +listeComparison.size(), false, sheet);
                    setValueTitleAt(indiceLigne, 0, sheet, "Sc�nario de r�f�rence", Colour.WHITE);
                    indiceLigne+=4;
                    for (Sinavi3DataSimulation comparison : listeComparison) {
                        setValueTitleAt(indiceLigne, 0, sheet, "Sc�nario " + comparison.getProjectName_(), Colour.WHITE);
                        produceSheetAttentesTrajetsValues(comparison, sheet, indiceLigne, tronconE, tronconS, 0, 0);
                        indiceLigne+=4;
                    }
                }else {
                fusionneCells(indiceLigne, 2, 3, false, sheet);
                fusionneCells(indiceLigne, 3, 3, false, sheet);
                indiceLigne+=4;
            }
                    

            }
            for (int tronconS = 0; tronconS < data.getListeEcluse().getListeEcluses().size(); tronconS++) {
                setValueAt(indiceLigne, 2, sheet, data.getListeBief_().retournerBief(tronconE).getName());
                setValueAt(indiceLigne, 3, sheet, data.getListeBief_().retournerBief(tronconS).getName());
                produceSheetAttentesTrajetsValues(data, sheet, indiceLigne, tronconE, tronconS, 0, 1);
              
                if (multiSimulation) {
                    fusionneCells(indiceLigne, 2, (listeComparison.size()+1)*3+listeComparison.size(), false, sheet);
                    fusionneCells(indiceLigne, 3, (listeComparison.size()+1)*3+listeComparison.size(), false, sheet);
                    setValueTitleAt(indiceLigne, 0, sheet, "Sc�nario de r�f�rence", Colour.WHITE);
                    indiceLigne+=4;
                    for (Sinavi3DataSimulation comparison : listeComparison) {
                        setValueTitleAt(indiceLigne, 0, sheet, "Sc�nario " + comparison.getProjectName_(), Colour.WHITE);
                        produceSheetAttentesTrajetsValues(comparison, sheet, indiceLigne, tronconE, tronconS, 0, 0);
                       indiceLigne+=4;
                    }
                }else 
                   {
                fusionneCells(indiceLigne, 2, 3, false, sheet);
                fusionneCells(indiceLigne, 3, 3, false, sheet);
                indiceLigne+=4;
            }
            }
        }

        for (int tronconE = 0; tronconE < data.getListeEcluse().getListeEcluses().size(); tronconE++) {
            for (int tronconS = 0; tronconS < data.getListeBief_().getListeBiefs_().size(); tronconS++) {
                setValueAt(indiceLigne, 2, sheet, data.getListeBief_().retournerBief(tronconE).getName());
                setValueAt(indiceLigne, 3, sheet, data.getListeBief_().retournerBief(tronconS).getName());
                produceSheetAttentesTrajetsValues(data, sheet, indiceLigne, tronconE, tronconS, 1, 0);
            

                if (multiSimulation) {
                    fusionneCells(indiceLigne, 2, (listeComparison.size()+1)*3+listeComparison.size(), false, sheet);
                    fusionneCells(indiceLigne, 3, (listeComparison.size()+1)*3+listeComparison.size(), false, sheet);
                    setValueTitleAt(indiceLigne, 0, sheet, "Sc�nario de r�f�rence", Colour.WHITE);
                    indiceLigne+=4;
                    for (Sinavi3DataSimulation comparison : listeComparison) {
                        setValueTitleAt(indiceLigne, 0, sheet, "Sc�nario " + comparison.getProjectName_(), Colour.WHITE);
                        produceSheetAttentesTrajetsValues(comparison, sheet, indiceLigne, tronconE, tronconS, 0, 0);
                        indiceLigne+=4;
                    }
                }else 
                    {
                fusionneCells(indiceLigne, 2, 3, false, sheet);
                fusionneCells(indiceLigne, 3, 3, false, sheet);
                indiceLigne+=4;
            }
            }
            for (int tronconS = 0; tronconS < data.getListeEcluse().getListeEcluses().size(); tronconS++) {
                setValueAt(indiceLigne, 2, sheet, data.getListeBief_().retournerBief(tronconE).getName());
                setValueAt(indiceLigne, 3, sheet, data.getListeBief_().retournerBief(tronconS).getName());
                produceSheetAttentesTrajetsValues(data, sheet, indiceLigne, tronconE, tronconS, 1, 1);
               
                if (multiSimulation) {
                    fusionneCells(indiceLigne, 2, (listeComparison.size()+1)*3+listeComparison.size(), false, sheet);
                    fusionneCells(indiceLigne, 3, (listeComparison.size()+1)*3+listeComparison.size(), false, sheet);
                    setValueTitleAt(indiceLigne, 0, sheet, "Sc�nario de r�f�rence", Colour.WHITE);
                   indiceLigne+=4;
                    for (Sinavi3DataSimulation comparison : listeComparison) {
                        setValueTitleAt(indiceLigne, 0, sheet, "Sc�nario " + comparison.getProjectName_(), Colour.WHITE);
                        produceSheetAttentesTrajetsValues(comparison, sheet, indiceLigne, tronconE, tronconS, 0, 0);
                        indiceLigne+=4;
                    }
                }else 
                   {
                fusionneCells(indiceLigne, 2, 3, false, sheet);
                fusionneCells(indiceLigne, 3, 3, false, sheet);
                indiceLigne+=4;
            }
            }
        }
        
        }
        if (!multiSimulation) {
            setValueTitleAt(7, 0, sheet, "Sc�nario de r�f�rence", Colour.WHITE);
            fusionneCells(7, 0, indiceLigne - 8, false, sheet);
        }
        return sheet;
    }

    
}
