package org.fudaa.fudaa.sinavi3;

/**
 * 
 */

import java.awt.CardLayout;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

import javax.swing.BorderFactory;
import javax.swing.JFileChooser;
import javax.swing.JInternalFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.table.CtuluTableExcelWriter;
import org.fudaa.ebli.network.simulationNetwork.SimulationNetworkEditor;
import org.fudaa.fudaa.ressource.FudaaResource;
import org.jdesktop.swingx.ScrollPaneSelector;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuDialogConfirmation;
import com.memoire.bu.BuDialogError;
import com.memoire.fu.FuLog;

/**
 * @author Adrien Hadoux
 */

public class Sinavi3VisualiserEcluses extends Sinavi3InternalFrame {

  /**
   * Layout cardlayout pour affichage des donnes
   */
  CardLayout pile_;

  /**
   * Le panel de base qui contient tous les differents panels contient un layout de type CardLayout
   */
  JPanel principalPanel_;

  /**
   * Panel d'affichage des differents Navires saisis layout classique flow layout ou grid layout
   */
  Sinavi3PanelAffichageEcluse affichagePanel_;

  /**
   * ascenseur pour le panel d'affichage
   */
  JScrollPane ascAff_;

  /**
   * panel qui contient l'ascenceur (OPTIONNEL!!!!!!!!!!)
   */
  JPanel conteneurAffichage_;

  /**
   * Panel de saisie des donnes relative aux Navires
   */

  Sinavi3PanelSaisieEcluse SaisieEclusePanel_;

  /**
   * Panel de commande: panel qui contient les differnets boutons responsable de: -ajout -suppression -modification des
   * Navires
   */
  JPanel controlePanel_;

  /**
   * Boutton de selection de la saisie
   */
  private final BuButton boutonSaisie_ = new BuButton(FudaaResource.FUDAA.getIcon("ajouter"), "ajout");
  private final BuButton boutonAffichage_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_voir"), "voir");
  private final BuButton modification_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_maj"), "modif");
  private final BuButton quitter_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_oui"), "quitter");
  private final BuButton suppression_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_detruire"), "suppr");
  private final BuButton duplication_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_ranger"), "dupliquer");
  private final BuButton impression_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_generer"), "Excel");
 
  /**
   * Donnees
   */
  Sinavi3DataSimulation donnees_;

  boolean alterneTitre_ = true;

  /**
   * Constructeur de la Jframe
   */

  Sinavi3VisualiserEcluses(final Sinavi3DataSimulation d) {

    super("", true, true, true, true);
    // recuperation des donnes
    donnees_ = d;

    setTitle("Visualisation des Ecluses");

    // location de la JFrame:
    // this.setLocationRelativeTo(this.getParent());
    setSize(790, 675);
    setBorder(BorderFactory.createCompoundBorder(BorderFactory.createRaisedBevelBorder(), BorderFactory.createLoweredBevelBorder()));
    /**
     * tooltiptext des boutons
     */
    this.boutonAffichage_.setToolTipText("permet de visualiser la totalit� des donn�es sous forme d'un tableau");
    this.boutonSaisie_
        .setToolTipText("permet de saisir une nouvelle donn�e afin de l'ajouter � l'ensemble des param�tres");
    this.modification_
        .setToolTipText("permet de modifier un �l�ment: il faut dabord cliquer sur l'�l�ment � modifier dans le menu \"voir\"");
    this.quitter_.setToolTipText("cliquez sur ce bouton pour fermer la sous fen�tre");
    this.suppression_
        .setToolTipText("permet de supprimer une donn�e: cliquez d'abord sur l'�l�ment � supprimer dans le menu \"voir\"");
    this.duplication_
        .setToolTipText("permet de dupliquer une donn�e: cliquez d'abord sur l'�l�ment � dupliquer dans le menu \"voir\"");
    this.impression_
        .setToolTipText("permet d'importer le contenu des donn�es dans un fichier excel que l'on pourra par la suite imprimer");

    // definition des differents Layout
    final Container contenu = getContentPane();

    this.principalPanel_ = new JPanel();

    pile_ = new CardLayout(30, 10);
    this.principalPanel_.setLayout(pile_);

    // definition du propre panel d'affichage des Navires
    this.affichagePanel_ = new Sinavi3PanelAffichageEcluse(donnees_);

    // definition de l ascenceur pour le panel d'affichage

    this.ascAff_ = new JScrollPane(affichagePanel_);
    //  utilisation de swingx
    ScrollPaneSelector.installScrollPaneSelector(this.ascAff_);
    // definition du panel de saisie d'un bateau
    // FrameSaisieQuais fq=new FrameSaisieQuais(donnees.listebassin);
    // fq.setVisible(false);
    SaisieEclusePanel_ = new Sinavi3PanelSaisieEcluse(donnees_, this);

    this.controlePanel_ = new JPanel();
    this.controlePanel_.setLayout(new FlowLayout());

    // ajout des 2 panel d'affichage et de saisie d'un Navire
    this.principalPanel_.add(affichagePanel_, "affichage");
    this.principalPanel_.add(this.SaisieEclusePanel_, "saisie");

    // ajout des panel dans la frame
    contenu.add(principalPanel_);
    contenu.add(controlePanel_, "South");

    /**
     * ******************************************************************************** THE CONTROL PANEL YEAH
     * *******************************************************************************
     */

    quitter_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        Sinavi3VisualiserEcluses.this.windowClosed();

      }
    });
    controlePanel_.add(quitter_);

    boutonSaisie_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        pile_.last(principalPanel_);

        // 1) changmement de titre
        setTitle("Saisie d'une nouvelle ecluse");
        validate();

        SaisieEclusePanel_.initialiser();
        SaisieEclusePanel_.setBorder(Sinavi3Bordures.ecluse);
        // indique a la fenetre de saisie qu il ne s agira en aucun ca d une modification
        SaisieEclusePanel_.UPDATE = false;
      }
    });

    // ajout des boutons dans le panel de controle
    controlePanel_.add(boutonSaisie_);

    boutonAffichage_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        pile_.first(principalPanel_);

        alterneTitre_ = true;
        setTitle("Visualisation des Ecluses");

        System.out.print("affichage des nouvelles donn�es:");
        affichagePanel_.maj(donnees_);
        affichagePanel_.revalidate();
        validate();
        // System.out.print("wwwwwwwwwwwwwww");
        // new VisualiserQuais(donnees);
        // dispose();
      }
    });

    // ajout des boutons dans le panel de controle
    controlePanel_.add(boutonAffichage_);

    modification_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {

        // 1) recuperation du numero de ligne du quai via la jtable
        System.out.println("La ligne selectionnee est: " + affichagePanel_.tableau_.getSelectedRow());
        final int numEcluse = affichagePanel_.tableau_.getSelectedRow();
        if (numEcluse == -1) {
          new BuDialogError(donnees_.application_.getApp(), Sinavi3Implementation.isSinavi_,
              "Erreur!! Vous devez cliquer sur l'ecluse a\n modifier dans le tableau d'affichage!").activate();
        } else {
          // 2.5 changmeent de fenetre
          pile_.last(principalPanel_);
          setTitle("Modification d'une ecluse");
          SaisieEclusePanel_.setBorder(Sinavi3Bordures.ecluse2);
          validate();

          // 2)appel a la mehode de modification de PanelSaisieQuai(a ecrire): met boolean MODIF=true
          SaisieEclusePanel_.MODE_MODIFICATION(numEcluse);

          // 3)lors de la sauvegarde , utilise le booleen pour remplacer(methode set des donn�es) au lieu de add
        }// fin du if

      }
    });

    // ajout des boutons dans le panel de controle
    controlePanel_.add(modification_);

    suppression_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        // 1) recuperation du numero de ligne du quai via la jtable
        System.out.println("La ligne selectionnee est: " + affichagePanel_.tableau_.getSelectedRow());
        final int numEcluse = affichagePanel_.tableau_.getSelectedRow();
        if (numEcluse == -1) {
          new BuDialogError(donnees_.application_.getApp(), Sinavi3Implementation.isSinavi_,
              "Erreur!! Vous devez cliquer sur l'ecluse a\n supprimer dans le tableau d'affichage!").activate();
        } else {

          // on s occupe de la supresion des quais:
          // 1)on demande confirmation:
          final int confirmation = new BuDialogConfirmation(donnees_.application_.getApp(),
              Sinavi3Implementation.isSinavi_, "�tes vous sur de supprimer l'�cluse "
                  + donnees_.listeEcluse_.retournerEcluse(numEcluse).nom_).activate();

          if (confirmation == 0) {
            // 2)on supprime le numero du quai correspondant a la suppression
        	  Sinavi3Ecluse ecluse = donnees_.getListeEcluse().retournerEcluse(numEcluse);
            donnees_.listeEcluse_.suppression(numEcluse);
            // 3)mise a jour de l affichage:
            affichagePanel_.maj(donnees_);
            
            /**
             * suppression d une ligne de la matrice des dur�es de parcours
             */
            
            donnees_.getDureeManeuvresEcluses().deleteEcluse(ecluse);           
           
            donnees_.getReglesRemplissageSAS().SuprimerLigne(numEcluse);
            
            
            //-- delete element in network --//
            donnees_.getApplication().getNetworkEditor().deleteNetworkElement(
            		SimulationNetworkEditor.DEFAULT_VALUE_ECLUSE,
            		ecluse);
            
            //--On baisse le niveau de s�curit� pour forcer le test de coh�rence globale --//
            d.baisserNiveauSecurite2();

            
          }// fin cas ou l on confirme la suppression

        }// fin du else

      }
    });
    controlePanel_.add(suppression_);
    // controlePanel.setBorder(this.raisedBevel);

    this.duplication_.addActionListener(new ActionListener() {

      public void actionPerformed(final ActionEvent e) {
        System.out.println("test duplication: ");
        final int numEcluse = affichagePanel_.tableau_.getSelectedRow();
        if (numEcluse == -1) {
          new BuDialogError(donnees_.application_.getApp(), Sinavi3Implementation.isSinavi_,
              "Erreur!! Vous devez cliquer sur l'ecluse a\n dupliquer dans le tableau d'affichage!").activate();

        } else {

          // on s occupe de la duplication des ecluses:
          // 1)on demande confirmation:
          String confirmation = "";
          confirmation = JOptionPane.showInputDialog(null, "Nom de l'�cluse dupliqu�e: ");
          if (donnees_.listeEcluse_.existeDoublon(confirmation, -1)) {
            new BuDialogError(donnees_.application_.getApp(), Sinavi3Implementation.isSinavi_,
                "Erreur!! Nom deja pris!!!!").activate();
            return;
          }

          if (!confirmation.equals("")) {
            // 2)ON RECOPIE TOUTES LES DONNEES DUPLIQUEES dans une nouvelle ecluse quel on ajoute
            final Sinavi3Ecluse nouveau = new Sinavi3Ecluse();
            final Sinavi3Ecluse ecluseAdupliquer = donnees_.listeEcluse_.retournerEcluse(numEcluse);

            nouveau.largeur_ = ecluseAdupliquer.largeur_;
            nouveau.longueur_ = ecluseAdupliquer.longueur_;
            nouveau.hauteurchute = ecluseAdupliquer.hauteurchute;
            nouveau.tempsFausseBassinneeMontant_ = ecluseAdupliquer.tempsFausseBassinneeMontant_;
            nouveau.dureeManoeuvreSortant_ = ecluseAdupliquer.dureeManoeuvreSortant_;
            nouveau.dureeManoeuvreEntrant_ = ecluseAdupliquer.dureeManoeuvreEntrant_;
            nouveau.coefficientBassinEpargne_ = ecluseAdupliquer.coefficientBassinEpargne_;
            nouveau.h_ = ecluseAdupliquer.h_;
            nouveau.tempsFausseBassinneeAvalant_ = ecluseAdupliquer.tempsFausseBassinneeAvalant_;
            nouveau.profondeur_ = ecluseAdupliquer.profondeur_;
            nouveau.h_.recopie(ecluseAdupliquer.h_);

            // loi d indispo
            nouveau.dureeIndispo_ = ecluseAdupliquer.dureeIndispo_;
            nouveau.loiIndispo_ = ecluseAdupliquer.loiIndispo_;
            // cas de la loi sur la frequence
            if (ecluseAdupliquer.typeLoi_ == 0) {
              nouveau.typeLoi_ = 0;
              nouveau.frequenceMoyenne_ = ecluseAdupliquer.frequenceMoyenne_;
              nouveau.loiFrequence_ = ecluseAdupliquer.loiFrequence_;
            } else if (ecluseAdupliquer.typeLoi_ == 1) {

              nouveau.typeLoi_ = 1;
              for (int i = 0; i < ecluseAdupliquer.loiDeterministe_.size(); i++) {
                final CoupleLoiDeterministe c = new CoupleLoiDeterministe(
                    (CoupleLoiDeterministe) ecluseAdupliquer.loiDeterministe_.get(i));
                nouveau.loiDeterministe_.add(c);

              }

            } else if (ecluseAdupliquer.typeLoi_ == 2) {
              // cas loi journaliere
              nouveau.typeLoi_ = 2;
              for (int i = 0; i < ecluseAdupliquer.loiDeterministe_.size(); i++) {
                final CoupleLoiDeterministe c = new CoupleLoiDeterministe(
                    (CoupleLoiDeterministe) ecluseAdupliquer.loiDeterministe_.get(i));
                nouveau.loiDeterministe_.add(c);

              }

            }

            // on donne le nouveau nom du quai a dupliquer
            nouveau.nom_ = confirmation;

            // on ajoute le nouveau quai dupliqu�
            donnees_.listeEcluse_.ajout(nouveau);

            
            
            /**
             * Regles durees de parcours ajout d une ligne
             */
            
            donnees_.notifyAddEcluse(nouveau);
            
            // 3)mise a jour de l affichage:
            affichagePanel_.maj(donnees_);
            
            //-- add element in network --//
            donnees_.getApplication().getNetworkEditor().addNewNetworkElement(
            		SimulationNetworkEditor.DEFAULT_VALUE_ECLUSE,
            		nouveau);
            
          }// fin cas ou l on confirme la suppression
        }

      }
    });
    this.controlePanel_.add(duplication_);

    /**
     * Bouton impression format excel
     */
    impression_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        File fichier;
        // definition d un file chooser
        final JFileChooser fc = new JFileChooser();
        final int returnVal = fc.showSaveDialog(Sinavi3VisualiserEcluses.this);

        if (returnVal == JFileChooser.APPROVE_OPTION) {
          fichier = fc.getSelectedFile();
          final File f = CtuluLibFile.appendExtensionIfNeeded(fichier, "xls");

          // on r�cupere l abstrct model du tableau contenant les donn�es

          /**
           * creation d un abstract model impl�mentant l'interface CtuluTableModelInterface
           */

          final Sinavi3ModeleExcel modele = new Sinavi3ModeleExcel();

          // creation du tableau des titres des colonnes en fonction de la fenetre d affichage desecluses
          modele.nomColonnes_ = affichagePanel_.titreColonnes;

          /**
           * transformation du model du tableau deja rempli pour le nuoveau model cr�e
           */

          /**
           * recopiage des titres des colonnes
           */
          // initialisation de la taille de data
          modele.data_ = new Object[donnees_.listeEcluse_.listeEcluses_.size() + 2][affichagePanel_.titreColonnes.length];

          for (int i = 0; i < affichagePanel_.titreColonnes.length; i++) {

            // ecriture des nom des colonnes:
            modele.data_[0][i] = affichagePanel_.titreColonnes[i];

          }

          /**
           * recopiage des donn�es
           */
          for (int i = 0; i < donnees_.listeEcluse_.listeEcluses_.size(); i++) {
            modele.data_[i + 2] = affichagePanel_.constructLine(i);
          }

          modele.setNbRow(donnees_.listeEcluse_.listeEcluses_.size() + 2);

          /**
           * on essaie d 'ecrire en format excel
           */
          final CtuluTableExcelWriter ecrivain = new CtuluTableExcelWriter(modele, f);

          try {
            ecrivain.write(null);

          } catch (final RowsExceededException _err) {
            FuLog.error(_err);
          } catch (final WriteException _err) {
            FuLog.error(_err);
          } catch (final IOException _err) {
            FuLog.error(_err);
          }

        }// fin du if si le composant est bon

      }// fin de la methode public actionPerformed

    });

    this.controlePanel_.add(impression_);

    /**
     * *************************************************************************************** THE PITI MENU QUI SERVENT
     * A RIEN ***************************************************************************************
     */

    // petite barre de menu agreable
    final JMenuBar menuBar = new JMenuBar();
    final JMenu menuFile = new JMenu("Fichier");
    final JMenuItem menuFileExit = new JMenuItem("Quitter");
    final JMenu menuOption = new JMenu("Options");
    final JMenu menuInfo = new JMenu("A propos de");

    // menuFile.set.setLabel("Fichier");
    // menuOption.setLabel("Options");
    // menuFileExit.setLabel("Quitter");
    // menuInfo.setLabel("A propos de");

    menuFileExit.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        Sinavi3VisualiserEcluses.this.windowClosed();
      }
    });
    menuFile.add(menuFileExit);
    menuBar.add(menuFile);
    menuBar.add(menuOption);
    menuBar.add(menuInfo);
    setJMenuBar(menuBar);

    // gestion de la fermeture de la frame:
    /*
     * this.addWindowListener ( new WindowAdapter() { public void windowClosing(WindowEvent e) {
     * SiporVisualiserEcluses.this.windowClosed(); } } );
     */
    // affichage de la frame
    setVisible(true);

  }

  /**
   * Methode qui s active lorsque l'on quitte l'application
   */
  protected void windowClosed() {
    // verif.stop(); //stop n est aps sur on le modifie donc par une autre variable

    // desactivation du thread
    // dureeVieThread=false;

    System.out.print("Fin du programme! a bientot!!");
    dispose();
  }

public Sinavi3PanelAffichageEcluse getAffichagePanel_() {
	return affichagePanel_;
}

public void setAffichagePanel_(Sinavi3PanelAffichageEcluse affichagePanel_) {
	this.affichagePanel_ = affichagePanel_;
}

}
