package org.fudaa.fudaa.sinavi3;

/**
 * 
 */

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import org.fudaa.ebli.network.simulationNetwork.DefaultNetworkUserObject;
import org.fudaa.ebli.network.simulationNetwork.SimulationNetworkEditor;
import org.fudaa.fudaa.ressource.FudaaResource;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuDialogError;
import com.memoire.bu.BuDialogConfirmation;

/**
 * Fenetre principale de saisie et d affichage des Gares on g�re toutes les donn�es relatives aux gares via ces
 * interfaces
 * 
 * @author Adrien Hadoux
 */

public class Sinavi3VisualiserGares extends Sinavi3InternalFrame {

  // attributs

  /*
   * Les panels
   */

  JPanel controlePanel = new JPanel();

  // panel d affichage global:
  JPanel global = new JPanel();

  /**
   * Panel de saisie des diff�rentes gares
   */
  Sinavi3PanelSaisieGare saisiegarePanel_;

  /**
   * Panel d affichage des duff�rents bassins sous forme de Jtable necessaire pour recup�rer l indice du bateau
   * selectionn�
   */

  Sinavi3PanelAffichageGares affichagePanel_;

  /**
   * Ascenceur pour le jtable
   */
  JScrollPane ascenceur;
  // composants:

  private final BuButton boutonSaisie_ = new BuButton(FudaaResource.FUDAA.getIcon("ajouter"), "Ajouter");
  private final BuButton modification_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_maj"), "Modifier");
  private final BuButton quitter_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_quitter"), "Quitter");
  final BuButton suppression_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_detruire"), "Supprimer");

  JLabel mode_ = new JLabel("Ajout d'une gare:");

  /**
   * Parametres de la simulation
   */
  Sinavi3DataSimulation donnees_;

  
  /**
   * Constructeur de la fenetre prend en parametre les donn�es de la simulation
   * 
   * @param d donn�es de la simulation
   */
  Sinavi3VisualiserGares(final Sinavi3DataSimulation d) {

    super("", true, true, true, true);

    donnees_ = d;
    setTitle("Gestion des gares");
    setSize(500, 350);
    this.getContentPane().setLayout(new BorderLayout());
    setBorder(Sinavi3Bordures.compound_);
    /**
     * tooltiptext des boutons
     */
    this.boutonSaisie_
        .setToolTipText("Permet la saisie d'une nouvelle gare");
    this.modification_
        .setToolTipText("Permet la modification de la gare s�lectionn�e ci-dessus");
    this.quitter_.setToolTipText("Ferme la sous-fen�tre");
    this.suppression_
        .setToolTipText("Supprime la gare s�lectionn�e ci-dessus");

    /**
     * ***************************************************************** Gestion des controles de la fenetre
     */

    this.modification_.addActionListener(new ActionListener() {

      public void actionPerformed(final ActionEvent e) {
        // 1) recuperation du numero de ligne de la gare via la jtable
        System.out.println("La ligne selectionnee est: " + affichagePanel_.tableau_.getSelectedRow());
        final int numGare = affichagePanel_.tableau_.getSelectedRow();
        if (numGare == -1) {
        	new BuDialogError(donnees_.application_.getApp(), Sinavi3Implementation.isSinavi_,
            "Aucune gare n'est s�lectionn�e.").activate();
        } else {

          // 2)appel a la mehode de modification de PanelSaisieQuai(a ecrire): met boolean MODIF=true
          saisiegarePanel_.MODE_MODIFICATION(numGare);

        }// fin du if

      }
    });

    suppression_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        // 1) recuperation du numero de ligne de la gare via la jtable
        System.out.println("La ligne selectionnee est: " + affichagePanel_.tableau_.getSelectedRow());
        final int numGare = affichagePanel_.tableau_.getSelectedRow();
        if (numGare == -1) {
        	new BuDialogError(donnees_.application_.getApp(), Sinavi3Implementation.isSinavi_,
              "Aucune gare n'est s�lectionn�e.").activate();
        } else {
        	
        	// On v�rifie que la gare n'est pas une gare amont ou aval d'un tron�on ou d'une �cluse
        	for (int i = 0; i < donnees_.listeBief_.listeBiefs_.size(); i++) {
        		if (numGare == donnees_.listeBief_.retournerBief(i).gareAmont_ || numGare == donnees_.listeBief_.retournerBief(i).gareAval_) {
        			new BuDialogError(donnees_.application_.getApp(), Sinavi3Implementation.isSinavi_,
        					"Cette gare est la gare amont ou aval du bief " + donnees_.listeBief_.retournerBief(i).nom_ + " et ne peut donc pas �tre supprim�e.").activate();
        			return;
        		}
        	}
        	for (int i = 0; i < donnees_.listeEcluse_.listeEcluses_.size(); i++) {
        		if (numGare == donnees_.listeEcluse_.retournerEcluse(i).gareAmont_ || numGare == donnees_.listeEcluse_.retournerEcluse(i).gareAval_) {
        			new BuDialogError(donnees_.application_.getApp(), Sinavi3Implementation.isSinavi_,
        					"Cette gare est la gare amont ou aval de l'�cluse " + donnees_.listeBief_.retournerBief(i).nom_ + " et ne peut donc pas �tre supprim�e.").activate();
        			return;
        		}
        	}

        	// On demande confirmation
        	final int confirmation = new BuDialogConfirmation(donnees_.application_.getApp(),
        			Sinavi3Implementation.isSinavi_, "Voulez-vous vraiment supprimer la gare "
        			+ donnees_.listeGare_.retournerGare(numGare) + " ?").activate();

        	if (confirmation == 0) {
        		 String nomGare =  donnees_.getListeGare_().retournerGare(numGare);
        		// 1)on supprime la gare de num�ro numGare
        		donnees_.listeGare_.suppression(numGare);
        		
        		// 2)correction des topologies des tron�ons et �cluses
        		for (int i = 0; i < donnees_.listeBief_.listeBiefs_.size(); i++) {
            		if (donnees_.listeBief_.retournerBief(i).gareAmont_ > numGare) donnees_.listeBief_.retournerBief(i).gareAmont_ --;
            		if (donnees_.listeBief_.retournerBief(i).gareAval_ > numGare) donnees_.listeBief_.retournerBief(i).gareAval_ --;
            	}
            	for (int i = 0; i < donnees_.listeEcluse_.listeEcluses_.size(); i++) {
            		if (donnees_.listeEcluse_.retournerEcluse(i).gareAmont_ > numGare) donnees_.listeEcluse_.retournerEcluse(i).gareAmont_--;
            		if (donnees_.listeEcluse_.retournerEcluse(i).gareAval_ > numGare) donnees_.listeEcluse_.retournerEcluse(i).gareAval_--;
            	}
        		
        		// 3)mise a jour de l affichage:
        		affichagePanel_.maj(donnees_);
        		
        		 //-- delete element in network --//
                donnees_.getApplication().getNetworkEditor().deleteNetworkElement(
                		SimulationNetworkEditor.DEFAULT_VALUE_GARE,
                		new DefaultNetworkUserObject(nomGare, true));
        		
          }// fin cas ou l on confirme la suppression

        }// fin du else

      }
    });

    quitter_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        Sinavi3VisualiserGares.this.windowClosed();

      }
    });

    /**
     * ****************************************************************** Affichage des composants:
     */

    // affichage des composants:
    global.setLayout(new BorderLayout());

    // panel d affichage des donn�es du bassin
    affichagePanel_ = new Sinavi3PanelAffichageGares(donnees_);
    this.ascenceur = new JScrollPane(this.affichagePanel_);
    global.add(this.ascenceur, BorderLayout.CENTER);

    // panel de saisie des donn�es du bassin
    this.saisiegarePanel_ = new Sinavi3PanelSaisieGare(donnees_, this);

    final JPanel inter = new JPanel();
    inter.setLayout(new GridLayout(1, 3));
    final JPanel affichMode = new JPanel();
    affichMode.add(this.mode_);
    inter.add(affichMode);

    inter.add(this.saisiegarePanel_);
    inter.add(new JLabel(""));
    inter.setBorder(Sinavi3Bordures.compound_);
    global.add(inter, BorderLayout.SOUTH);

    this.getContentPane().add(global, BorderLayout.CENTER);

    // le panel de controle:

    this.controlePanel.add(quitter_);
    //this.controlePanel.add(boutonSaisie_);
    this.controlePanel.add(modification_);
    this.controlePanel.add(suppression_);

    this.controlePanel.setBorder(Sinavi3Bordures.compound_);
    this.getContentPane().add(controlePanel, BorderLayout.SOUTH);

    setVisible(true);

    // petite barre de menu agreable
    final JMenuBar menuBar = new JMenuBar();
    final JMenu menuFile = new JMenu("Fichier");
    final JMenuItem menuFileExit = new JMenuItem("Quitter");
    final JMenu menuOption = new JMenu("Options");
    final JMenu menuInfo = new JMenu("A propos de");

    // menuFile.set.setLabel("Fichier");
    // menuOption.setLabel("Options");
    // menuFileExit.setLabel("Quitter");
    // menuInfo.setLabel("A propos de");

    menuFileExit.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        Sinavi3VisualiserGares.this.windowClosed();
      }
    });
    menuFile.add(menuFileExit);
    menuBar.add(menuFile);
    menuBar.add(menuOption);
    menuBar.add(menuInfo);
    setJMenuBar(menuBar);

    // gestion de la fermeture de la frame:
    /*
     * this.addWindowListener ( new WindowAdapter() { public void windowClosing(WindowEvent e) {
     * SiporVisualiserGares.this.windowClosed(); } } );
     */
  }

  /**
   * Methode qui s active lorsque l'on quitte l'application
   */
  protected void windowClosed() {
    // verif.stop(); //stop n est aps sur on le modifie donc par une autre variable

    // desactivation du thread
    // dureeVieThread=false;

    System.out.print("Fermeture de la fenetre de gestion des gares.");
    dispose();
  }

}
