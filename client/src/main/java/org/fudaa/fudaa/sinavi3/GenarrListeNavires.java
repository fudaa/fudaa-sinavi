package org.fudaa.fudaa.sinavi3;

import java.io.EOFException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;

import com.memoire.bu.BuDialogError;
import com.memoire.bu.BuDialogMessage;
import com.memoire.fu.FuLog;

import org.fudaa.dodico.fortran.FortranReader;
import org.fudaa.dodico.fortran.FortranWriter;
import org.fudaa.dodico.objet.CDodico;

/**
 * Classe de gestion des navires g�n�r�s par genarr. Contient les methodes de lecture du fichier g�n�r� par Genarr.
 * Contient les m�thodes de manipulation de la liste des navires.
 * 
 * @version $Version$
 * @author hadoux
 */

public class GenarrListeNavires {

  private ArrayList listeNavires_ = new ArrayList();

  public GenarrListeNavires() {

  }

  public int taille() {
    return listeNavires_.size();
  }

  public void ajout(GenarrNavire gn) {
    listeNavires_.add(gn);
  }

  public GenarrNavire suppression(int indice) {
    return (GenarrNavire) listeNavires_.remove(indice);
  }

  public GenarrNavire retourner(int indice) {
    return (GenarrNavire) listeNavires_.get(indice);
  }

  public void modifier(GenarrNavire gn, int indice) {
    GenarrNavire newgn = new GenarrNavire(gn);

    listeNavires_.set(indice, newgn);
  }

  /**
   * Methode de lecture du fichier de g�n�ration de navires de genarr. Il faut lancer l'executable au prealable afin de
   * g�n�rer le fichier
   * 
   * @param nomFichier
   */
  public boolean lectureFichierGenarr(String nomFichier) {

    // -- Nettoyage de la liste des navires --//
    this.listeNavires_.clear();

    // -- ajout de l'extension du fichier genarr: .arriv --//
    nomFichier = nomFichier + ".arriv";

    File fichier = new File(nomFichier);

    if (!fichier.exists()) {
      new BuDialogError(
          null,
          Sinavi3Implementation.isSinavi_,
          "Le fichier "
              + nomFichier
              + "\n de g�n�ration est introuvable.\nLa g�n�ration de bateaux peut �tre lanc�e � nouveau via la menu \"G�n�ration\".")
          .activate();
      return false;
    }

    // structure finale qui contiendra la totalit� des navires
    FortranReader fr;
    int cpt = 1;
    try {
      fr = new FortranReader(new FileReader(nomFichier));

      try {
        while (true) {
          fr.readFields();

          if (fr.getNumberOfFields() > 2) {
            // -- recopiage des parametres dans le champs --//

            // -- allocation m�moire pour les donn�es d un anvire --//

            // -- recopiage des donn�es du fichier --//
            int jour = fr.intField(0);
            int heure = fr.intField(1);
            int minutes = fr.intField(2);
            // int secondes=fr.intField(3);
            String sens = fr.stringField(3);
            int categorie = fr.intField(4) - 1;
            int gareDep = fr.intField(5) - 1;
            int gareArr = fr.intField(6) - 1;

            // -- ajout d'un objet de type navire genarr dans la liste des donn�es --//
            this.ajout(new GenarrNavire(cpt++, categorie, jour, heure, minutes, sens, gareDep, gareArr));

          }
        }
      } catch (final EOFException e) {
        FuLog.debug("fin de fichier");
        return true;
      }

    }// -- fin ddu try du fortranreader --//
    catch (FileNotFoundException e1) {} catch (final Exception ex) {
      CDodico.exception(GenarrListeNavires.class, ex);
      new BuDialogError(null, Sinavi3Implementation.isSinavi_,
          "Erreur dans la lecture du fichier de bateaux g�n�r�s (genarr.x).").activate();
      return false;
    }

    return true;
  }

  public void ecritureFichierGenarr(String nomFichier) {

    // --IMPORTANT: tri de la liste --//
    this.trierNaviresChronologiquement();

    nomFichier = nomFichier + ".arriv";

    try {
      // creation du fichier:
      final FortranWriter f = new FortranWriter(new FileWriter(nomFichier));

      // format du fichier : 40 caract�res de donn�es + 1 s�parateur + 100 caract�res de commentaire
      final int[] fmt = new int[] { 4, 1, 2, 1, 2, 1, 2, 1, 3, 1, 4, 1, 4 };

      for (int i = 0; i < this.taille(); i++) {
        // ecriture du nombre de jours de la simulation
        f.stringField(0, new Integer(retourner(i).jour).toString());
        f.stringField(2, new Integer(retourner(i).heure).toString());
        f.stringField(4, new Integer(retourner(i).minute).toString());
        f.stringField(6, new String(retourner(i).sens));
        f.stringField(8, new Integer(retourner(i).categorie + 1).toString());
        f.stringField(10, new Integer(retourner(i).gareDep + 1).toString());
        f.stringField(12, new Integer(retourner(i).gareArriv + 1).toString());
        f.writeFields(fmt);
      }
      // fermeture du fichier de donn�es g�n�rales
      f.flush();
      f.close();
      new BuDialogMessage(null, Sinavi3Implementation.isSinavi_, "Le fichier a �t� modifi� avec succ�s.").activate();

    }// -- fin ddu try du fortranreader --//
    catch (FileNotFoundException e1) {} catch (final Exception ex) {
      CDodico.exception(GenarrListeNavires.class, ex);
      new BuDialogError(null, Sinavi3Implementation.isSinavi_,
          "Erreur dans la lecture du fichier de bateaux g�n�r�s (genarr.x).").activate();

    }

  }

  /**
   * Methode de tri de la liste des navires par cat�gories de navire
   */
  public void trier() {

    for (int i = 0; i < this.taille(); i++)
      for (int j = i + 1; j < this.taille(); j++)
        if (retourner(j).categorie < retourner(i).categorie) {
          GenarrNavire temp = retourner(j);
          modifier(retourner(i), j);
          modifier(temp, i);
        }
    /*
     * else if(retourner(j).categorie==retourner(i).categorie) if(retourner(j).navire<retourner(i).navire) {
     * GenarrNavire temp=retourner(j); modifier(retourner(i),j); modifier(temp,i);
     * 
     * }
     */

  }

  /**
   * Methode de trie par rapport au num�ro des navires
   */

  public void trierNavires() {
    for (int i = 0; i < this.taille(); i++)
      for (int j = i + 1; j < this.taille(); j++)
        if (retourner(j).navire < retourner(i).navire) {
          GenarrNavire temp = retourner(j);
          modifier(retourner(i), j);
          modifier(temp, i);
        }
  }

  /**
   * Methode importante de tri des navires par ordre chronologique. Cet ordre est indispensable au bon fonctionnement du
   * noyau de calcul.
   */
  public void trierNaviresChronologiquement() {
    for (int i = 0; i < this.taille(); i++)
      for (int j = i + 1; j < this.taille(); j++)
        if (retourner(j).jour < retourner(i).jour) {
          GenarrNavire temp = retourner(j);
          modifier(retourner(i), j);
          modifier(temp, i);
        } else if (retourner(j).jour == retourner(i).jour) {

          if (retourner(j).heure < retourner(i).heure) {
            GenarrNavire temp = retourner(j);
            modifier(retourner(i), j);
            modifier(temp, i);
          } else if (retourner(j).heure == retourner(i).heure) {
            if (retourner(j).minute < retourner(i).minute) {
              GenarrNavire temp = retourner(j);
              modifier(retourner(i), j);
              modifier(temp, i);
            }
          }

        }
  }

}
