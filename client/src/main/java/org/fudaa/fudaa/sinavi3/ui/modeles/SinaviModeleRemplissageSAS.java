package org.fudaa.fudaa.sinavi3.ui.modeles;

import java.util.Observable;
import java.util.Observer;

import jxl.write.Label;
import jxl.write.Number;
import jxl.write.WritableCell;

import com.memoire.bu.BuDialogError;
import com.memoire.bu.BuDialogWarning;

import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.fudaa.sinavi3.Sinavi3DataSimulation;
import org.fudaa.fudaa.sinavi3.Sinavi3Implementation;

public class SinaviModeleRemplissageSAS extends SinaviModeleExcel implements Observer{

  Sinavi3DataSimulation donnees_;
  String[] titreColonnes_;
  
  public SinaviModeleRemplissageSAS(Sinavi3DataSimulation _d){
  donnees_=_d;
  donnees_.addObservers(this);
  titreColonnes_ = new String[donnees_.getListeBateaux_().getListeNavires_().size() + 1];

    // initialisation des titres
    titreColonnes_[0] = "";
    for (int i = 0; i < donnees_.getListeBateaux_().getListeNavires_().size(); i++) {
      titreColonnes_[i + 1] = donnees_.getListeBateaux_().retournerNavire(i).getNom();
    }
  
  }
  
  
  public int getColumnCount() {
    return this.donnees_.getListeBateaux_().getListeNavires_().size()+1;
  }

  public int getRowCount() {
    return this.donnees_.getListeEcluse().getListeEcluses().size();
  }

  
  
  /** retourne le double correspondant au parcours **/
  public Object getValueAt(int i, int j) {
    
    if(j==0)
          return donnees_.getListeEcluse().retournerEcluse(i).getNom();
        else
            return ""+this.donnees_.getReglesRemplissageSAS().retournerValeur(i, j-1);
  }

  
  
  /**Recuperation des donnees (saisie par utilisateur) par le tableau et analyse pour validation **/
  public void setValueAt(Object value, int rowIndex, int columnIndex) {
    double valeur;
    try {
      valeur = Double.parseDouble((String)value);
      
      
      //-- Gestion des erreurs --//
    if(valeur < 0) {
            new BuDialogError(donnees_.getApplication().getApp(), Sinavi3Implementation.informationsSoftware(),
                    "Erreur!!  la valeur entre:\n l'�cluse: " + this.donnees_.getListeEcluse().retournerEcluse(rowIndex).getNom()
                        + "\n et \n le navire: " + this.donnees_.getListeBateaux_().retournerNavire(columnIndex-1).getNom()
                        + "\nest n�gatif.").activate();
                return;
              }
    
    //-- test si l'utilisateur entre des centi�mes --//
    if(((String)value).lastIndexOf(".")!=-1){
    String min=((String)value).substring(((String)value).lastIndexOf(".")+1);
    int minutes=Integer.parseInt(min);
    if(minutes/100!=0)
    {
      new BuDialogError(donnees_.getApplication().getApp(), Sinavi3Implementation.informationsSoftware(),
                  "La valeur entre:\n l'�cluse: " + this.donnees_.getListeEcluse().retournerEcluse(rowIndex).getNom()
                      + "\n et le navire: " + this.donnees_.getListeBateaux_().retournerNavire(columnIndex-1).getNom()
                      + " n'est pas coh�rente: \n2 chiffres apr�s la virgule maximum sont attendus.").activate();

              return;
    }
    
    }
    
    } catch (NumberFormatException e) {
      new BuDialogError(donnees_.getApplication().getApp(), Sinavi3Implementation.informationsSoftware(),
                  "La valeur entre:\n l'�cluse: " + this.donnees_.getListeEcluse().retournerEcluse(rowIndex).getNom()
                      + "\n et \n le navire: " + this.donnees_.getListeBateaux_().retournerNavire(columnIndex-1).getNom()
                      + "\n n'est pas coh�rente: ce n'est pas un r�el.").activate();

              return;
    }
    
    
    
    
    //mise a jour du ArrayList durres de parcours (cot� m�tier)
    this.donnees_.getReglesRemplissageSAS().modifierValeur(valeur, rowIndex, columnIndex-1);
        
    
    //-- regle d'alerte si le d�lais d�passe 30 minutes --//
    if(valeur>0.3f) {
    new BuDialogWarning(
            donnees_.getApplication().getApp(),
            donnees_.getApplication().informationsSoftware(),
            "Attention: \nLe d�lais d'optimisation du SAS saisie " +
            "\n est sup�rieur � 30 minutes.\n" +
            "Cliquez de nouveau sur le bouton de validation pour valider")
            .activate();
    }
    //mise a jour du tableau (cot� graphique)
    fireTableCellUpdated(rowIndex, columnIndex);
    
  }
  
  public boolean isCellEditable(int row, int col) {
        //Note that the data/cell address is constant,
        //no matter where the cell appears onscreen.
        if (col < 1 ) {
            return false;
        } else {
            return true;
        }
    }


  /**Methode qui permet de recuperer els noms des colonnes du tableau **/
  public String getColumnName(int column) {
    
    return titreColonnes_[column];
  }

  
  public int getMaxCol() {
      return getColumnCount();
    }

    /**
     * retourne le nombre de ligne
     */
    public int getMaxRow() {
      return getRowCount();
    }

    /**
     * retoune un tableau pour le format excel Celui-ci sera utilis� avec la fonction write
     * 
     * @see org.fudaa.ctulu.table.CtuluTableModelInterface#getExcelWritable(int, int)
     */
    public WritableCell getExcelWritable(final int _row, final int _col, int _rowXls, int _colXls) {
      final int r = _row;
      final int c = _col;
      final Object o = getValueAt(r,c);
      if (o == null) {
        return null;
      }
      String s = o.toString();
      if(CtuluLibString.isEmpty(s)) return null;
      try {
        return new Number(_colXls, _rowXls, Double.parseDouble(s));
      } catch (final NumberFormatException e) {}
      return new Label(_colXls, _rowXls, s);

      // return cell;
    }


    public void update(Observable o, Object arg) {
      if(arg.equals("ecluse") ||arg.equals("navire"))
        this.fireTableDataChanged();
      
    }


  public Sinavi3DataSimulation getDonnees_() {
    return donnees_;
  }


  public void setDonnees_(Sinavi3DataSimulation donnees_) {
    this.donnees_ = donnees_;
  }


  public String[] getTitreColonnes_() {
    return titreColonnes_;
  }


  public void setTitreColonnes_(String[] titreColonnes_) {
    this.titreColonnes_ = titreColonnes_;
  }
  

}
