package org.fudaa.fudaa.sinavi3.sinavi3Network;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.fudaa.ebli.network.simulationNetwork.DefaultNetworkUserObject;
import org.fudaa.ebli.network.simulationNetwork.NetworkDaoFactory;
import org.fudaa.ebli.network.simulationNetwork.NetworkUserObject;
import org.fudaa.ebli.network.simulationNetwork.SimulationNetworkEditor;
import org.fudaa.fudaa.sinavi3.Sinavi3Bief;
import org.fudaa.fudaa.sinavi3.Sinavi3DataSimulation;
import org.fudaa.fudaa.sinavi3.Sinavi3Ecluse;
import org.fudaa.fudaa.sinavi3.structures.DefaultStructure;

import com.mxgraph.model.mxCell;
import com.mxgraph.view.mxGraph;
/**
 * Dao factory to map graph data to application data.
 * @author Adrien
 *
 */
public class Sinavi3NetworkDaoFactory implements NetworkDaoFactory{

	private final Sinavi3DataSimulation donnees ;
	
	private boolean hasChanged = false;
	
	public Sinavi3NetworkDaoFactory(Sinavi3DataSimulation donnees) {
		this.donnees = donnees;
	}
	
	public NetworkUserObject createElement(mxCell element) {
		hasChanged = true;
		NetworkUserObject createdElement = null;
		if(  SimulationNetworkEditor.DEFAULT_VALUE_GARE.equals(element.getValue())) {
			String nomGare = DefaultStructure.getDefaultName(null, donnees.getListeGare_().getListeGares_().size());
			donnees.getListeGare_().ajout(nomGare);
			createdElement = new DefaultNetworkUserObject(nomGare, true);
		} else if(  SimulationNetworkEditor.DEFAULT_VALUE_BIEF.equals(element.getValue())) {
			Sinavi3Bief chenal =new Sinavi3Bief(donnees.getListeBateaux_().getListeNavires_().size());
			chenal.setNom_(DefaultStructure.getDefaultName(chenal,donnees.getListeBief_().getListeBiefs_().size()));
			donnees.getListeBief_().ajout(chenal);
			donnees.getReglesVitesseBiefAvalant_().ajoutLigneAvalant(donnees.getListeBateaux_().getListeNavires_().size(),donnees);
			donnees.getReglesVitesseBiefMontant_().ajoutLigneMontant(donnees.getListeBateaux_().getListeNavires_().size(),donnees);
		     createdElement = chenal;
		}else if(  SimulationNetworkEditor.DEFAULT_VALUE_ECLUSE.equals(element.getValue())) {
			Sinavi3Ecluse ecluse =new Sinavi3Ecluse();
			ecluse.setNom_(DefaultStructure.getDefaultName(ecluse , donnees.getListeEcluse().getListeEcluses().size()+1));
			donnees.getListeEcluse().ajout(ecluse);
			
			donnees.notifyAddEcluse(ecluse);
			
			
			createdElement = ecluse;
		}
		donnees.getApplication().updateNetworkData();
		return createdElement;
	}

	public NetworkUserObject deleteElement(mxCell element) {
		hasChanged = true;
		NetworkUserObject deletedElement = null;
		
		if(!(element.getValue() instanceof NetworkUserObject)) {
			return null;
		}
		NetworkUserObject object = (NetworkUserObject) element.getValue();
		if(object instanceof DefaultNetworkUserObject) {
			DefaultNetworkUserObject data = (DefaultNetworkUserObject) object;
			if(data.isNode() && donnees.getListeGare_().getListeGares_().contains(data.getName())) {
				donnees.getListeGare_().getListeGares_().remove(data.getName());
			} else {
				//do nothing...
			}
			deletedElement = data;
		} else 
			
			if(object instanceof Sinavi3Bief) {
				Sinavi3Bief chenal = (Sinavi3Bief) object;
				int numData = donnees.getListeBief_().getListeBiefs_().indexOf(chenal);
				if(numData != -1) {
					donnees.getListeBief_().suppression(numData);	           
					donnees.getReglesVitesseBiefAvalant_().SuprimerLigne(numData);
					donnees.getReglesVitesseBiefMontant_().SuprimerLigne(numData);
				}
				deletedElement = chenal;
				
			}
			 else 
					if(object instanceof Sinavi3Ecluse) {
						Sinavi3Ecluse ecluse = (Sinavi3Ecluse) object;
						int numData = donnees.getListeEcluse_().getListeEcluses().indexOf(ecluse);
						if(numData != -1) {
							donnees.getListeEcluse_().suppression(numData);	  
							donnees.getDureeManeuvresEcluses().deleteEcluse(ecluse);							
							donnees.getReglesRemplissageSAS().SuprimerLigne(numData);
						}
						deletedElement = ecluse;
					}
					
		donnees.getApplication().updateNetworkData();
		return deletedElement;
	}

	public Object connectElement(mxCell edge, mxCell terminal, boolean unconnect, boolean amont) {
		hasChanged = true;
		//-- terminal must be a node (= gare) or else we have a problem --//
		if(!(terminal.getValue() instanceof DefaultNetworkUserObject) || ! ((DefaultNetworkUserObject)terminal.getValue()).isNode()) {
			return null;
		}
		DefaultNetworkUserObject gare = (DefaultNetworkUserObject)terminal.getValue();
		int indiceGare = donnees.getListeGare_().getListeGares_().indexOf(gare.getName());		
		if(! (edge.getValue() instanceof NetworkUserObject)) {
			return null;
		
		}
			NetworkUserObject object = (NetworkUserObject) edge.getValue();
			
			if(object instanceof Sinavi3Bief) {
				Sinavi3Bief chenal = (Sinavi3Bief) object;
				if(amont) {
					chenal.setGareAmont_(unconnect?0:indiceGare);
				} else
					chenal.setGareAval_(unconnect?0:indiceGare);
			} else
				if(object instanceof Sinavi3Ecluse) {
					Sinavi3Ecluse ecluse = (Sinavi3Ecluse) object;
					if(amont) {
						ecluse.setGareAmont_(unconnect?0:indiceGare);
					} else
						ecluse.setGareAval_(unconnect?0:indiceGare);
				}
		return "";
	}


	public Object loadNetworkMapping(mxGraph graph,HashMap<mxCell,NetworkUserObject> mappingUserObject ,HashMap<mxCell, mxCell[]> mappingNetworkConnection ) {
		hasChanged = true;
		mappingUserObject.clear();
		mappingNetworkConnection.clear();
		
		Object[] allCells = graph.getChildCells(graph.getDefaultParent(), true, true);
		
		List<mxCell> listeGare  = new ArrayList<mxCell>();
		
		for(Object data:allCells){
			if(data instanceof mxCell) {
				mxCell cell = (mxCell) data;
				
				if(cell.getValue() instanceof NetworkUserObject) {
					NetworkUserObject userObject = (NetworkUserObject)cell.getValue() ;
					//-- gare --//
					if(userObject instanceof DefaultNetworkUserObject) {
						donnees.getListeGare_().retrouverGare(userObject.getName());
						DefaultNetworkUserObject gare = new DefaultNetworkUserObject(userObject.getName(), true);
						mappingUserObject.put(cell, gare);
						cell.setValue(gare);
						listeGare.add(cell);
					} else if(userObject instanceof Sinavi3Bief) {
						int indiceChenal = donnees.getListeBief_().retourneIndice(userObject.getName());
						if(indiceChenal>=0){
							mappingUserObject.put(cell, donnees.getListeBief_().retournerBief(indiceChenal));
							cell.setValue(donnees.getListeBief_().retournerBief(indiceChenal));
						}
					} else if(userObject instanceof Sinavi3Ecluse) {
						int indiceEcluse = donnees.getListeEcluse().retourneIndice(userObject.getName());
						if(indiceEcluse>=0) {
							mappingUserObject.put(cell, donnees.getListeEcluse().retournerEcluse(indiceEcluse));
							cell.setValue(donnees.getListeEcluse().retournerEcluse(indiceEcluse));
						}
					}   
				}
			}
		}
		
		//-- deduire la topologie depuis les gares --//
		for(mxCell gare:listeGare) {			
			for(int i=0; i< gare.getEdgeCount();i++) {
				if( gare.getEdgeAt(i) instanceof mxCell) {
					mxCell edge = (mxCell) gare.getEdgeAt(i);					
					if( ! mappingNetworkConnection.containsKey(edge)) {
						mxCell[] connections = new mxCell[2];
						mappingNetworkConnection.put(edge, connections);
					}
					mxCell[] connections = mappingNetworkConnection.get(edge);
					//TODO gestion amont=0 et aval=1 � am�liorer
					int indiceGare = donnees.getListeGare_().retrouverGare(((NetworkUserObject)gare.getValue()).getName());
					NetworkUserObject userObjectEdge = (NetworkUserObject)edge.getValue() ;
					int indiceGareAmontUserObject=0;
					if(userObjectEdge instanceof Sinavi3Bief) {
						indiceGareAmontUserObject = ((Sinavi3Bief)userObjectEdge).getGareAmont_();
					} else if(userObjectEdge instanceof Sinavi3Ecluse) {
						indiceGareAmontUserObject = ((Sinavi3Ecluse)userObjectEdge).getGareAmont_();
					} 
					//-- pour savoir si on met en gare amont ou aval, on fait un double check avec les donn�es business -> 0: amont et 1 = aval --//	
					if(indiceGareAmontUserObject == indiceGare)
						connections[0] = gare;
					else 
						connections[1] = gare;
			}
		}
		}		
		return null;
	}

	public boolean hasChanged() {
		
		return hasChanged;
	}

	

}
