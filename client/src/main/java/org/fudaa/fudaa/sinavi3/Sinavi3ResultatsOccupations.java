/**
 *@creation 19 jan. 10
 *@modification $Dates$
 *@license  GNU General Public Licence 2
 *@copyright   (c)1998-2006 CETMEF 2 bd Gambetta F -60231 Compiegne
 *@mail   devel@fudaa.fr 
 */
package org.fudaa.fudaa.sinavi3;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.text.DecimalFormat;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.ButtonGroup;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;
import javax.swing.table.TableColumn;

import org.fudaa.ctulu.image.CtuluImageExport;
import org.fudaa.dodico.corba.sinavi3.SParametresCoupleDistribution;
import org.fudaa.fudaa.ressource.FudaaResource;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuDialogError;
import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuScrollPane;
import com.memoire.bu.BuTabbedPane;
import com.memoire.bu.BuTable;
import com.memoire.fu.FuLog;

/**
 * classe de gestion des resultats de la generation des bateaux propose 2 onglets: le premier propose un affichage
 * 
 * @version $Version$
 * @author Mederic FARGEIX
 */
public class Sinavi3ResultatsOccupations extends Sinavi3InternalFrame {

  /**
   * ensemble des donn�es du tableau sous la forme de data
   */
  Object[][] data;
  
  int[][] dataDistrib_;
  
  ChartPanel grapheDistrib_;

  /**
   * panel principal de la fenetre
   */
  BuPanel panelPrincipal_ = new BuPanel();

  /**
   * Panel de selection des preferences
   */
  BuPanel selectionPanel_ = new BuPanel();
  BuPanel selectionPanel1;
  
  JLabel typesElementsLabel_;
  JLabel sensLabel_;
  /** Comboliste de selection de l'objet �tudi� */
  String[] listeTypesElementsTxt_ = {"gare", "�cluse"};
  JComboBox listeTypesElements_ = new JComboBox(listeTypesElementsTxt_);
  JComboBox listeElements_ = new JComboBox();
  int elementChoisi_;

  String[] chaine_sens = {"avalant", "montant"};
  String[] chaine_position = {"amont", "aval"};
  JComboBox listeSens_ = new JComboBox();
  
  /**
   * Vecteurs assurant la correspondance entre un index correspondant � la recherche et le num�ro d'une gare
   * listeNumerosGaresMontant/Avalant[indexListeDeroulante] = num�roGare
   */
  int[] listeNumerosGaresAvalant_;
  int[] listeNumerosGaresMontant_;
  
  /**
   * Tableaux donnant, pour une gare, le nombre et la d�signation des �cluses dont elle est amont/aval
   */
  int[] gareEstAmontDeNombreEcluses_;
  int[][] gareEstAmontDeEcluses_;
  int[] gareEstAvalDeNombreEcluses_;
  int[][] gareEstAvalDeEcluses_;
  
  /**
   * Panel des options: type affichages, colonnes � faire figurer:
   */
  BuPanel optionPanel_ = new BuPanel(new BorderLayout());

  JCheckBox[] tableauChoixNavires_;

  /**
   * Panel des options de distribution
   */
  BuPanel optionPanelDistrib_ = new BuPanel(new BorderLayout());
  
  JComboBox choixBateauxDistrib_ = new JComboBox();

  /**
   * Tableau r�capitulatif des r�sultats de la simulation
   */
  BuTable tableau_;
  JScrollPane tableauDefil_;

  String[] titreTableau_;
  
  /**
   * Tableau d'entiers qui contiendra les valeurs maxi d'occupation pour l'�l�ment s�lectionn� (utile pour les distributions)
   */
  int[] occupationMaxi_;
  
  /**
   * Panel contenant le bouton quitter
   */
  BuPanel panelQuitter_ = new BuPanel();

  /**
   * Panel tabbed qui g�re les 2 onglets, ie les 2 versions d'affichage des r�sultats:
   */
  BuTabbedPane panelPrincipalAffichage_ = new BuTabbedPane();
  
  BuPanel panelGrapheDistrib_ = new BuPanel(new BorderLayout());
  
  /**
   * Panel cniotenant le tableau et les boutns de controles
   */
  BuPanel panelGestionTableau_ = new BuPanel();
  
  BuPanel panelGestionGrapheDistrib_ = new BuPanel();

  /**
   * panel de gestion du tableau et des diff�rents boutons
   */
  BuPanel panelTableau_ = new BuPanel();
  BuPanel panelTableauAffichage_ = new BuPanel();

  /**
   * panel de gestion des boutons
   */
  BuPanel controlPanel_ = new BuPanel();
  
  BuPanel controlPanelGrapheDistrib_ = new BuPanel();

  /**
   * combolist qui permet de selectionenr les colonnes du tableau a etre affich�es:
   */
  JComboBox ListeNavires_ = new JComboBox();

  /**
   * buoton de generation des resultats
   */
  private final BuButton exportationExcel_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_generer"), "Exporter");
  
  final BuButton exportGrapheDistrib_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_generer"), "Exporter");

  private final BuButton quitter_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_quitter"), "Quitter");
  private final BuButton lancerRecherche_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_oui"), "Rechercher");

  Border raisedBevel_ = BorderFactory.createRaisedBevelBorder();
  Border loweredBevel_ = BorderFactory.createLoweredBevelBorder();
  Border compound_ = BorderFactory.createCompoundBorder(raisedBevel_, loweredBevel_);
  Border bordnormal_ = BorderFactory.createEtchedBorder();
  /**
   * donnees de la simulation
   */
  Sinavi3DataSimulation donnees_;

  /**
   * constructeur de la sous fenetre de gestion des resultats:
   */
  Sinavi3ResultatsOccupations(final Sinavi3DataSimulation _donnees) {
    super("R�sultats d'occupation", true, true, true, true);

    // recuperation des donn�es de la simulation
    donnees_ = _donnees;
    
    this.occupationMaxi_ = new int[this.donnees_.listeBateaux_.listeNavires_.size() + 1]; // 1 par cat�gorie + total

    setSize(820, 600);
    setBorder(Sinavi3Bordures.compound_);
    this.getContentPane().setLayout(new GridLayout(1, 1));
    this.panelPrincipal_.setLayout(new BorderLayout());
    this.getContentPane().add(this.panelPrincipal_);
    this.panelPrincipal_.add(this.selectionPanel_, BorderLayout.NORTH);
    this.panelPrincipal_.add(panelQuitter_, BorderLayout.SOUTH);
    panelQuitter_.add(quitter_);

    this.panelPrincipal_.add(this.optionPanel_, BorderLayout.WEST);
    this.panelPrincipal_.add(this.panelPrincipalAffichage_, BorderLayout.CENTER);

    // ajout du tableau dans le panel tabbed
    panelPrincipalAffichage_.addTab("R�sultats d�taill�s", FudaaResource.FUDAA.getIcon("crystal_arbre"), panelTableau_);
    panelPrincipalAffichage_.addTab("Graphe de distribution", FudaaResource.FUDAA.getIcon("crystal_graphe"), panelGestionGrapheDistrib_);

    tableauChoixNavires_ = new JCheckBox[this.donnees_.listeBateaux_.listeNavires_.size()];
    for (int i = 0; i < this.donnees_.listeBateaux_.listeNavires_.size(); i++) {
      this.tableauChoixNavires_[i] = new JCheckBox(this.donnees_.listeBateaux_.retournerNavire(i).nom, true);
      this.tableauChoixNavires_[i].addActionListener(this);
    }
    
    for (int i = 0; i < this.donnees_.listeBateaux_.listeNavires_.size(); i++) {
    	choixBateauxDistrib_.addItem(this.donnees_.listeBateaux_.retournerNavire(i).nom);
    }
    choixBateauxDistrib_.addItem("Flotte enti�re");
    
    // Listes d'�cluses amont ou aval de gares
    final String[] listeEclusesGareAmont_ = new String[donnees_.listeGare_.listeGares_.size()]; // Liste d�roulante qui sera affich�e si l'utilisateur choisit gare amont
    this.listeNumerosGaresAvalant_ = new int[donnees_.listeGare_.listeGares_.size()]; // listeNumerosGaresAvalant[indexListeDeroulante] = num�roGare
    this.gareEstAmontDeNombreEcluses_ = new int[donnees_.listeGare_.listeGares_.size()];
    this.gareEstAmontDeEcluses_ = new int[donnees_.listeGare_.listeGares_.size()][donnees_.listeEcluse_.listeEcluses_.size()];
    int indexGareAmont_ = 0;
    final String[] listeEclusesGareAval_ = new String[donnees_.listeGare_.listeGares_.size()]; // Liste d�roulante qui sera affich�e si l'utilisateur choisit le sens montant
    this.listeNumerosGaresMontant_ = new int[donnees_.listeGare_.listeGares_.size()]; // listeNumerosGaresMontant[indexListeDeroulante] = num�roGare
    this.gareEstAvalDeNombreEcluses_ = new int[donnees_.listeGare_.listeGares_.size()];
    this.gareEstAvalDeEcluses_ = new int[donnees_.listeGare_.listeGares_.size()][donnees_.listeEcluse_.listeEcluses_.size()];
    int indexGareAval_ = 0;
    int nombreEclusesVoisines_;
    
    for (int i = 0; i < donnees_.listeGare_.listeGares_.size(); i++) { // Pour chaque gare, on v�rifie qu'elle est concern�e par la requete (cad en amont -resp aval- d'une/plusieurs ecluse)
    	
    	for (int j = 0; j < donnees_.listeEcluse_.listeEcluses_.size(); j++) {
    		if (i == donnees_.listeEcluse_.retournerEcluse(j).gareAmont_) {
    			this.gareEstAmontDeEcluses_[i][ this.gareEstAmontDeNombreEcluses_[i] ++ ] = j;
    			this.listeNumerosGaresAvalant_[indexGareAmont_] = i;
    		} else if (i == donnees_.listeEcluse_.retournerEcluse(j).gareAval_) {
    			this.gareEstAvalDeEcluses_[i][ this.gareEstAvalDeNombreEcluses_[i] ++ ] = j;
    			this.listeNumerosGaresMontant_[indexGareAval_] = i;
    		}
    	}
    	
    	nombreEclusesVoisines_ = this.gareEstAmontDeNombreEcluses_[i];
    	if (nombreEclusesVoisines_ > 0) {
    		for (int k = 0; k < nombreEclusesVoisines_; k++) {
    			if (k == 0) listeEclusesGareAmont_[indexGareAmont_] = "";
    			else if (k < nombreEclusesVoisines_-1) listeEclusesGareAmont_[indexGareAmont_] += ", ";
    			else if (k == nombreEclusesVoisines_-1) listeEclusesGareAmont_[indexGareAmont_] += " et ";
    			listeEclusesGareAmont_[indexGareAmont_] += donnees_.listeEcluse_.retournerEcluse(this.gareEstAmontDeEcluses_[i][k]).nom_;
    		}
    		indexGareAmont_ ++;
    	}
    	
    	nombreEclusesVoisines_ = this.gareEstAvalDeNombreEcluses_[i];
    	if (nombreEclusesVoisines_ > 0) {
    		for (int k = 0; k < nombreEclusesVoisines_; k++) {
    			if (k == 0) listeEclusesGareAval_[indexGareAval_] = "";
    			else if (k < nombreEclusesVoisines_-1) listeEclusesGareAval_[indexGareAmont_] += ", ";
    			else if (k == nombreEclusesVoisines_-1) listeEclusesGareAval_[indexGareAmont_] += " et ";
    			listeEclusesGareAval_[indexGareAval_] += donnees_.listeEcluse_.retournerEcluse(this.gareEstAvalDeEcluses_[i][k]).nom_;
    		}
    		indexGareAval_ ++;
    	}
    	
    }
    final int nombreGaresAvalant_ = indexGareAmont_;
    final int nombreGaresMontant_ = indexGareAval_;
    
    

    /*******************************************************************************************************************
     * gestion du panel de selection
     ******************************************************************************************************************/
    this.selectionPanel_.setLayout(new GridLayout(2, 1));
    
    this.typesElementsLabel_ = new JLabel("Occupations au sein d'un �l�ment de type");
    this.sensLabel_ = new JLabel(" dans le sens");

    selectionPanel1 = new BuPanel(new FlowLayout(FlowLayout.LEFT));
    selectionPanel1.add(this.typesElementsLabel_);
    selectionPanel1.add(this.listeTypesElements_);
    
//    selectionPanel1.add(new JLabel(" dans le sens"));
//    selectionPanel1.add(this.listeSens_);
    
    
    this.selectionPanel_.add(selectionPanel1);
    
    final BuPanel selectionPanel2 = new BuPanel(new FlowLayout(FlowLayout.LEFT));
//    selectionPanel2.add(new JLabel("El�ment"));
//    selectionPanel2.add(this.listeElements_);
    this.selectionPanel_.add(selectionPanel2);
//    selectionPanel2.add(lancerRecherche_);

    final TitledBorder bordurea = BorderFactory.createTitledBorder(BorderFactory
    		.createEtchedBorder(EtchedBorder.LOWERED), "D�finition de la recherche");
    this.selectionPanel_.setBorder(bordurea);
    
    // listener des liste box
    
    final ActionListener RemplissageTypeElement = new ActionListener() {
    	public void actionPerformed(final ActionEvent e) {
    		
    		final Object source = e.getSource();

    		int selectionType = listeTypesElements_.getSelectedIndex();
    		
    		if (selectionType == 1) { // Cas �cluse
    			listeElements_.removeAllItems();
    			for (int i = 0; i < donnees_.listeEcluse_.listeEcluses_.size(); i++) {
    				listeElements_.addItem(donnees_.listeEcluse_.retournerEcluse(i).nom_);
    			}
    			listeElements_.validate();
    			selectionPanel2.removeAll();
    			selectionPanel2.add(new JLabel("Ecluse"));
    		    selectionPanel2.add(listeElements_);
    		    selectionPanel2.add(new JLabel(" dans le sens"));
    		    listeSens_.removeAllItems();
    		    listeSens_.addItem(chaine_sens[0]);
    		    listeSens_.addItem(chaine_sens[1]);
    		    selectionPanel2.add(listeSens_);
    		    selectionPanel2.add(lancerRecherche_);
    		    selectionPanel2.validate();
    		} else { // Cas gare
    			selectionPanel2.removeAll();
    			selectionPanel2.add(new JLabel("Gare"));
    			listeSens_.removeAllItems();
    		    listeSens_.addItem(chaine_position[0]);
    		    listeSens_.addItem(chaine_position[1]);
    			selectionPanel2.add(listeSens_);
    			listeSens_.setSelectedIndex(0);
    			selectionPanel2.add(new JLabel(" de(s) �cluse(s)"));
    			selectionPanel2.add(listeElements_);
    			selectionPanel2.add(lancerRecherche_);
    			selectionPanel2.validate();
    		}
    	}
    };
    
    final ActionListener RemplissagePositionGare = new ActionListener() {
    	public void actionPerformed(final ActionEvent e) {
    		
    		int selectionType = listeTypesElements_.getSelectedIndex();
    		int selectionPosition = listeSens_.getSelectedIndex();

    		switch (selectionType) {
    		case 1: //Ecluses
    			break;
    		case 0: //Gares
    			listeElements_.removeAllItems();
    			switch(selectionPosition) {
    			case 0: //Sens avalant
    				for (int i = 0; i < nombreGaresAvalant_; i++) {
    					listeElements_.addItem(listeEclusesGareAmont_[i]);
    				}
    				listeElements_.validate();
        			break;
    			case 1: //Sens montant
    				for (int i = 0; i < nombreGaresMontant_; i++) {
    					listeElements_.addItem(listeEclusesGareAval_[i]);
    				}
    				listeElements_.validate();
        			break;
    			}
    		}
    	}
    };
    this.listeTypesElements_.addActionListener(RemplissageTypeElement);
    this.listeTypesElements_.setSelectedIndex(0);
    this.listeSens_.addActionListener(RemplissagePositionGare);
    this.listeSens_.setSelectedIndex(0);

    this.lancerRecherche_.addActionListener(this);
    
    /*******************************************************************************************************************
     * gestion du panel des options du tableau
     ******************************************************************************************************************/

    BuGridLayout lo=new BuGridLayout(1,5,0,true,false,false,false);
    final BuPanel panoption = new BuPanel(lo);
    final BuScrollPane panoptionGestionScroll = new BuScrollPane(panoption);

    final Box bVert = Box.createVerticalBox();
    panoption.add(bVert);
    bVert.setBorder(this.bordnormal_);
    for (int i = 0; i < this.tableauChoixNavires_.length; i++) {
      bVert.add(this.tableauChoixNavires_[i]);
    }
    final TitledBorder bordure2 = BorderFactory.createTitledBorder(BorderFactory
        .createEtchedBorder(EtchedBorder.LOWERED), "Cat�gories");
    bVert.setBorder(bordure2);
    
    this.optionPanel_.add(panoptionGestionScroll);
    
    /*******************************************************************************************************************
     * gestion du panel des options pour les r�sultats de distribution
     ******************************************************************************************************************/

    BuGridLayout loGr=new BuGridLayout(1,5,0,true,false,false,false);
    final BuPanel panoptionDistrib = new BuPanel(loGr);
    final BuScrollPane panoptionDistribScroll = new BuScrollPane(panoptionDistrib);
    this.optionPanelDistrib_.add(panoptionDistribScroll);

    final Box bVertDistrib = Box.createVerticalBox();
    panoptionDistrib.add(bVertDistrib);
    bVertDistrib.add(new JLabel(""));
    final TitledBorder bordureDistrib = BorderFactory.createTitledBorder(BorderFactory
        .createEtchedBorder(EtchedBorder.LOWERED), "Cat�gorie");
    bVertDistrib.setBorder(bordureDistrib);
    bVertDistrib.add(choixBateauxDistrib_);

    // listener des checkbox de choix des options d affichage
    this.choixBateauxDistrib_.addActionListener(this);
    
    /*******************************************************************************************************************
     * gestion du panel tableau panelGestionTableau_
     ******************************************************************************************************************/

    this.panelTableau_.setLayout(new BorderLayout());
    this.panelTableau_.add(this.optionPanel_, BorderLayout.WEST);
    this.panelTableau_.add(this.panelGestionTableau_, BorderLayout.CENTER);
    
    // etape 1: architecture du panel panelGestionTableau_
    this.panelGestionTableau_.setLayout(new BorderLayout());
    this.panelGestionTableau_.add(this.panelTableauAffichage_, BorderLayout.CENTER);

    // panel qui contient les differents boutons
    this.controlPanel_.add(exportationExcel_);
    this.panelGestionTableau_.add(this.controlPanel_, BorderLayout.SOUTH);

    // etape 2: remplissage du comboliste avec les noms des navires
    this.ListeNavires_.addItem("Tous");
    for (int i = 0; i < donnees_.listeBateaux_.listeNavires_.size(); i++) {
      this.ListeNavires_.addItem("" + donnees_.listeBateaux_.retournerNavire(i).nom);
    }

    // etape 3: gestion de l affichage du tableau de donn�es
    // remarque : cette m�thode sera syst�matiquement appel�e afni d'op�rer un changement:
    calculApresRecherche();
    affichageTableau(false);

    // etape 4: listener du combolist afin de pouvoir selectionner le navire qui nous interesse
    // a noter que la selection va faire surligner le navire souhait�
    this.ListeNavires_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        // evenement du clic sur le bouton
        affichageTableau(true);

      }
    });

    // bouton qui permet de generer le contenu du tableau en ficheir excel:
    this.exportationExcel_.setToolTipText("Exporte le contenu du tableau dans un fichier xls");
    exportationExcel_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent _e) {
    	 Sinavi3GestionResultats.exportTableau(Sinavi3ResultatsOccupations.this, titreTableau_, data);
           //Sinavi3GestionResultats.exportReport(_donnees, false,false,true,false,false,false);
      }
    });

    /** listener des boutons quitter */
    this.quitter_.setToolTipText("Ferme la sous-fen�tre");
    final ActionListener actionQuitter = new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        Sinavi3ResultatsOccupations.this.windowClosed();
      }
    };
    this.quitter_.addActionListener(actionQuitter);
    
    
    /*******************************************************************************************************************
     * gestion du panel graphe distribution panelGestionGrapheDistrib_
     ******************************************************************************************************************/
    
    affichageGrapheDistrib(false);
    
    this.panelGestionGrapheDistrib_.setLayout(new BorderLayout());
    this.panelGestionGrapheDistrib_.add(this.optionPanelDistrib_, BorderLayout.WEST);
    this.panelGestionGrapheDistrib_.add(this.panelGrapheDistrib_,BorderLayout.CENTER);
    
    this.panelGrapheDistrib_.add(this.grapheDistrib_, BorderLayout.CENTER);
    
    this.exportGrapheDistrib_.setToolTipText("Exporte le graphe au format image");
    this.exportGrapheDistrib_.addActionListener(new ActionListener() {
    	public void actionPerformed(final ActionEvent _e) {
    		try {
    			grapheDistrib_.doSaveAs();
    		} catch (IOException e) {
    			FuLog.error(e);
    		}
    	}
    });
    
    this.panelGrapheDistrib_.add(this.controlPanelGrapheDistrib_, BorderLayout.SOUTH);
    this.controlPanelGrapheDistrib_.add(this.exportGrapheDistrib_);

  }

  void calculApresRecherche() {
	  
	  int occupLigneCategorie;
	  int totalOccupLigne;
	  
	  // Recherche de l'�l�ment s�lectionn� par l'utilisateur
	  int element = 0;
	  this.elementChoisi_ = -1;
	  if (this.listeTypesElements_.getSelectedIndex() == 0) { // Cas des gares : num�rotation particuli�re
		  if (this.listeSens_.getSelectedIndex() == 0) { // Sens avalant
			  element = this.listeNumerosGaresAvalant_[this.listeElements_.getSelectedIndex()];
		  } else {
			  element = this.listeNumerosGaresMontant_[this.listeElements_.getSelectedIndex()];
		  }
	  } else {
		  element = this.listeElements_.getSelectedIndex();
	  }
	  for (int k = 0; k < this.donnees_.params_.ResultatsCompletsSimulation.Occupations.length; k++) {
		  if (this.donnees_.params_.ResultatsCompletsSimulation.Occupations[k].typeElement == this.listeTypesElements_.getSelectedIndex()
				  && this.donnees_.params_.ResultatsCompletsSimulation.Occupations[k].indiceElement == element) {
			  this.elementChoisi_ = k;
		  }
	  }
	  if (this.elementChoisi_ == -1) {
		  new BuDialogError(donnees_.application_.getApp(), Sinavi3Implementation.isSinavi_, "Erreur!! L'�l�ment selectionn� est introuvable...").activate();
	  }

	  int nombreLignes = this.donnees_.params_.ResultatsCompletsSimulation.Occupations[this.elementChoisi_].resultatsOccupationsUnitaires.length;
	  
	  int nombreLignesSens = 0;
	  for (int i = 0; i < nombreLignes; i++) {
		  if (this.donnees_.params_.ResultatsCompletsSimulation.Occupations[this.elementChoisi_].resultatsOccupationsUnitaires[i].sens == this.listeSens_.getSelectedIndex()) {
			  nombreLignesSens ++;
		  }
	  }

	  
	  // D�termination du tableau de r�sultats bruts, ainsi que du vecteur occupationMaxi_ qui sera utile pour les distributions
	  
	  String[] nomCategories = new String[this.donnees_.listeBateaux_.listeNavires_.size()];
	  final int nbColAvData = 3; // colonnes ref, jour, heure
	  final int nbColApData = 1; // colonne total
	  data = new Object[nombreLignesSens][this.donnees_.listeBateaux_.listeNavires_.size() + nbColAvData + nbColApData];
	  
	  for (int j = 0; j < this.donnees_.listeBateaux_.listeNavires_.size(); j++) {
		  nomCategories[j] = this.donnees_.listeBateaux_.retournerNavire(j).nom;
		  this.occupationMaxi_[j] = 0;
	  }
	  
	  int indiceLigne = 0;
	  String codeSens = this.chaine_sens[this.listeSens_.getSelectedIndex()].substring(0, 1).toUpperCase(); // On r�cup�re "A" ou "M" pour "avalant" ou "montant", dans le but de construire le code bassin�e
	  for (int i = 0; i < nombreLignes; i++) {
	    	
		  if (this.donnees_.params_.ResultatsCompletsSimulation.Occupations[this.elementChoisi_].resultatsOccupationsUnitaires[i].sens == this.listeSens_.getSelectedIndex()) {
	    		
			  totalOccupLigne = 0;
	        	
			  // Construction de la r�f�rence de bassin�e
			  DecimalFormat dfEcluse = new DecimalFormat();
			  dfEcluse.setMinimumIntegerDigits(String.valueOf(this.donnees_.params_.ecluses.nbEcluses).length());
			  DecimalFormat dfBassinee = new DecimalFormat();
			  dfBassinee.setMinimumIntegerDigits(String.valueOf(nombreLignesSens).length());
			  dfBassinee.setGroupingUsed(false);
			  data[indiceLigne][0] = dfEcluse.format(this.donnees_.params_.ResultatsCompletsSimulation.Occupations[this.elementChoisi_].resultatsOccupationsUnitaires[i].ecluseConcernee + 1) + codeSens + "/" + dfBassinee.format(this.donnees_.params_.ResultatsCompletsSimulation.Occupations[this.elementChoisi_].resultatsOccupationsUnitaires[i].refBassinee + 1); // +1 et +1 pour avoir une num�rotation de 1 � N
			  
			  data[indiceLigne][1] = Sinavi3TraduitHoraires.traduitMinutesEnJoursHeuresMinutes((float) this.donnees_.params_.ResultatsCompletsSimulation.Occupations[this.elementChoisi_].resultatsOccupationsUnitaires[i].heure)[0];
			  data[indiceLigne][2] = Sinavi3TraduitHoraires.traduitMinutesEnJoursHeuresMinutes((float) this.donnees_.params_.ResultatsCompletsSimulation.Occupations[this.elementChoisi_].resultatsOccupationsUnitaires[i].heure)[1];
	        	
			  for (int j = 0; j < this.donnees_.listeBateaux_.listeNavires_.size(); j++) {
	        		
				  occupLigneCategorie = this.donnees_.params_.ResultatsCompletsSimulation.Occupations[this.elementChoisi_].resultatsOccupationsUnitaires[i].nombreBateauxCategorie[j];
				  totalOccupLigne += occupLigneCategorie;
	        		
				  data[indiceLigne][nbColAvData + j] = "" + occupLigneCategorie;
	        		
				  // On en profite pour r�cup�rer les valeurs maxi qui seront utiles pour les distributions
				  if (this.occupationMaxi_[j] < occupLigneCategorie) this.occupationMaxi_[j] = occupLigneCategorie;
	        		
			  }
	        	
			  data[indiceLigne][nbColAvData + this.donnees_.listeBateaux_.listeNavires_.size()] = "" + totalOccupLigne;
			  indiceLigne ++;
	        	
			  // On en profite pour r�cup�rer la valeur totale maxi qui sera utile pour les distributions
			  if(this.occupationMaxi_[this.donnees_.listeBateaux_.listeNavires_.size()] < totalOccupLigne) this.occupationMaxi_[this.donnees_.listeBateaux_.listeNavires_.size()] = totalOccupLigne;
	        	
		  }
	    	
	  }
	    
	  this.titreTableau_ = new String[this.donnees_.listeBateaux_.listeNavires_.size() + nbColAvData + nbColApData];
	  this.titreTableau_[0] = "R�f.";
	  this.titreTableau_[1] = "Jour";
	  this.titreTableau_[2] = "Heure";
	  for (int k = 0; k < this.donnees_.listeBateaux_.listeNavires_.size(); k++) {
		  this.titreTableau_[nbColAvData + k] = nomCategories[k];
	  }
	  this.titreTableau_[nbColAvData + this.donnees_.listeBateaux_.listeNavires_.size()] = "Total";
	  
	  
	  // D�termination de dataDistrib_
	  
	  this.dataDistrib_ = new int[this.donnees_.listeBateaux_.listeNavires_.size() + 1][this.occupationMaxi_[this.donnees_.listeBateaux_.listeNavires_.size()]+1]; //maxi+1 car on doit aller de 0 � maxi
	  //  for (int j = 0; j <= this.donnees_.listeBateaux_.listeNavires_.size(); j++) this.dataDistrib_[j] = new int[this.occupationMaxi_[j]+1];
	  
	  for (int i = 0; i < nombreLignes; i++) {
		  
		  if (this.donnees_.params_.ResultatsCompletsSimulation.Occupations[this.elementChoisi_].resultatsOccupationsUnitaires[i].sens == this.listeSens_.getSelectedIndex()) {
			  totalOccupLigne = 0;
			  for (int j = 0; j < this.donnees_.listeBateaux_.listeNavires_.size(); j++) {
				  occupLigneCategorie = this.donnees_.params_.ResultatsCompletsSimulation.Occupations[this.elementChoisi_].resultatsOccupationsUnitaires[i].nombreBateauxCategorie[j];
				  totalOccupLigne += occupLigneCategorie;
				  this.dataDistrib_[j][occupLigneCategorie] ++;
			  }
			  dataDistrib_[this.donnees_.listeBateaux_.listeNavires_.size()][totalOccupLigne] ++;
		  }
		  
	  }
	  
  }
  
  
  /**
   * Methode d'affichage du tableau remarque: cete m�thode sert aussi de rafraichissement du tableau
   * 
   */
  void affichageTableau(boolean _refresh) {

    /**
     * Recherche des donn�es associ�es � l'�l�ment choisi par l utilisateur: on recherche dasn el tableau qui stocke
     * tous les �l�ments, l indice de l �l�ment dont on veut les r�sultats:
     */

	  if (!_refresh) {
		  this.tableau_ = new BuTable(data, this.titreTableau_) {
			  public boolean isCellEditable(final int row, final int col) {
				  return false;
			  }
		  };
		  this.tableauDefil_ = new JScrollPane(this.tableau_);
	  }
    
	  for (int j = 0; j < this.donnees_.listeBateaux_.listeNavires_.size(); j++) {
		  if (this.tableauChoixNavires_[j].isSelected()) {
			  tableau_.showColumn(j+1);
		  } else {
			  tableau_.hideColumn(j+1);
		  }
	  }

	  // etape 3.5: dimensionnement des colonnes du tableau 
	  tableau_.getColumnModel().getColumn(0).setPreferredWidth(100);
	  tableau_.getColumnModel().getColumn(1).setPreferredWidth(50);
	  tableau_.getColumnModel().getColumn(2).setPreferredWidth(80);
	  for(int i=3; i<tableau_.getModel().getColumnCount();i++){
		  TableColumn column = tableau_.getColumnModel().getColumn(i);
		  column.setPreferredWidth(80); 
	  }
    
    
	  // etape 4: ajout du tableau cr�� dans l'interface
	  org.fudaa.fudaa.sinavi.factory.ColumnAutoSizer.sizeColumnsToFit(tableau_);
	  tableau_.revalidate();
	  this.panelTableauAffichage_.removeAll();
	  this.panelTableauAffichage_.setLayout(new BorderLayout());
	  this.panelTableauAffichage_.add(tableau_.getTableHeader(), BorderLayout.PAGE_START);
	  this.panelTableauAffichage_.add(new JScrollPane(this.tableauDefil_), BorderLayout.CENTER);
	  tableau_.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
	  // mise a jour de l'affichage
	  this.revalidate();
	  this.updateUI();

  }
  
  
  /**
   * M�thode permettant d'afficher un graphe de distribution de dur�es de parcours.
   * @param _refresh Param�tre permettant de choisir un rafraichissement du graphe existant plut�t que la construction d'un graphe nouveau
   */
  public void affichageGrapheDistrib(boolean _refresh){
	  
	  boolean percent = true; // on veut les valeurs d'ordonn�es en pourcentage de bassin�es
	  
	  // On choisit de n'afficher qu'une seule cat � la fois (cas ComboList), mais cela pourra �voluer par la suite (cas CheckBox)
	  int nbCategoriesAffichees = 1;
	  /* Cas CheckBox ***********
	   * int nbCategoriesAffichees = 0;
	   * int valeurMax = 0;
	   * for(int i=0; i<donnees_.listeBateaux_.listeNavires_.size(); i++) {
	   * if (this.tableauChoixNavires_[i].isSelected()) {
	   * nbCategoriesAffichees++;
	   * if (valeurMax < this.occupationMaxi_[i]) valeurMax = this.occupationMaxi_[i];
	   * }
	   * if (valeurMax < this.occupationMaxi_[donnees_.listeBateaux_.listeNavires_.size()]) valeurMax = this.occupationMaxi_[donnees_.listeBateaux_.listeNavires_.size()];
	   * nbCategoriesAffichees ++; // Catgorie "total"
	  }*/
	  int valeurMax = this.occupationMaxi_[this.choixBateauxDistrib_.getSelectedIndex()];
	  
	  
	  // On ne veut pas que l'axe des abscisses soit trop "zoom�", donc on impose au moins 8 graduations
	  int valeurMaxGraphe;
	  if (valeurMax < 8) valeurMaxGraphe = 8; else valeurMaxGraphe = valeurMax;
	
	  // R�cup�ration des valeurs calcul�es pr�c�demment, en limitant aux cat�gories s�lectionn�es
	  String[] noms = new String[nbCategoriesAffichees];
	  int[][] valeurs = new int[1][valeurMaxGraphe+1];
	  /* Cas CheckBox
	   * int[][] valeurs = new int[nbCategoriesAffichees][valeurMaxGraphe+1];
	   * int cpt=0;
	   * for(int i=0; i<donnees_.listeBateaux_.listeNavires_.size(); i++) {
	   * 	if (this.tableauChoixNavires_[i].isSelected()) {
	   * 		noms[cpt] = donnees_.listeBateaux_.retournerNavire(i).nom;
	   * 		for (int j = 0; j < valeurMax+1; j++) valeurs[cpt][j] = this.dataDistrib_[i][j];
	   * 		cpt++;
	   * 	}
	   * }
	   * for (int j = 0; j < valeurMax+1; j++) valeurs[cpt][j] = this.dataDistrib_[donnees_.listeBateaux_.listeNavires_.size()][j];
	   * noms[cpt] = "Flotte enti�re";
	   */
	  noms[0] = this.choixBateauxDistrib_.getSelectedItem().toString();
	  for (int j = 0; j < valeurMax+1; j++) valeurs[0][j] = this.dataDistrib_[this.choixBateauxDistrib_.getSelectedIndex()][j];
	  
	  // Construction du vecteur contenant le nombre total de bassin�es (valeur identique pour toutes les cat�gories)
	  int[] totaux = new int[nbCategoriesAffichees];
	  for (int i = 0; i < nbCategoriesAffichees; i++) totaux[i] = this.donnees_.params_.ResultatsCompletsSimulation.Occupations[this.elementChoisi_].resultatsOccupationsUnitaires.length;
	  
	  String titre;
	  int numeroGare, nombreEclusesVoisines;
	  if (this.listeTypesElements_.getSelectedIndex() == 0) {
		  titre = "Distribution des occupations de la gare ";
		  if (this.listeSens_.getSelectedIndex()==0) /* Cas gare amont */ {
			  titre += "amont ";
			  numeroGare = this.listeNumerosGaresAvalant_[this.listeElements_.getSelectedIndex()];
			  nombreEclusesVoisines = this.gareEstAmontDeNombreEcluses_[numeroGare];
		  }
		  else /* Cas gare aval */ {
			  titre += "aval ";
			  numeroGare = this.listeNumerosGaresMontant_[this.listeElements_.getSelectedIndex()];
			  nombreEclusesVoisines = this.gareEstAvalDeNombreEcluses_[numeroGare];
		  }
		  if (nombreEclusesVoisines == 1) titre += "de l'�cluse ";
		  else titre += "des �cluses ";
		  titre += this.listeElements_.getSelectedItem();
	  }
	  else titre = "Distribution des occupations de l'�cluse " + this.listeElements_.getSelectedItem() + " dans le sens " + this.listeSens_.getSelectedItem();
	  
	  JFreeChart chart;
	  chart = Sinavi3JFreeChartGraphe.genererHistoDistributionCumuleeCentreSurValeurs(titre, "Nombre de bateaux", "Pourcentage de bassin�es", 1, valeurs, noms, totaux, percent);
	  
	  if (!_refresh) this.grapheDistrib_ = new ChartPanel(chart);
	  else this.grapheDistrib_.setChart(chart);
	  
  }
  

  /**
   * Listener principal des elements de la frame: tres important pour les composant du panel de choix des �l�ments
   * 
   * @param ev evenements qui apelle cette fonction
   */

  public void actionPerformed(final ActionEvent ev) {
	  final Object source = ev.getSource();

	  // action commune a tous les �v�nements: redimensionnement de la fenetre
	  final Dimension actuelDim = this.getSize();
	  final Point pos = this.getLocation();
	  if (source == this.lancerRecherche_) {
		  calculApresRecherche();
		  // mise a jour des affichages:
		  affichageTableau(false);
		  affichageGrapheDistrib(true);
	  }

	  // si la source provient d'un choix de CheckBox de cat�gories du tableau d�taill�
	  boolean trouve = false;
	  for (int k = 0; k < this.tableauChoixNavires_.length && !trouve; k++) {
		  if (source == this.tableauChoixNavires_[k]) {
			  trouve = true;
			  affichageTableau(true);
		  }
	  }
	  
	  // si la source provient d'un choix dans la liste de cat�gories du graphe de distribution
	  if (source == this.choixBateauxDistrib_) affichageGrapheDistrib(true);

	  // on redimensionne la fenetre comme elle etais avant manipulation des elements graphique
	  this.setSize(actuelDim);
	  this.setLocation(pos);
  }// fin de actionPerformed

  /**
   * Methode qui s active lorsque l'on quitte l'application
   */
  protected void windowClosed() {
    System.out.print("Fin de la fenetre de r�sultats d'occupation");
    dispose();
  }
}
