/**
 *@creation 10 oct. 06
 *@modification $Dates$
 *@license  GNU General Public Licence 2
 *@copyright   (c)1998-2006 CETMEF 2 bd Gambetta F -60231 Compiegne
 *@mail   devel@fudaa.fr 
 */
package org.fudaa.fudaa.sinavi3;

/**
 * classe de gestion des donn�es pour les lois deterministe.
 * 
 * @version $Version$
 * @author adrien hadoux
 */
class CoupleLoiDeterministe {
  int jour_;
  double temps_;

  public CoupleLoiDeterministe(final int _jour, final double _temps) {

    this.jour_ = _jour;
    this.temps_ = _temps;
  }

  public CoupleLoiDeterministe(final CoupleLoiDeterministe _c) {

    this.jour_ = _c.jour_;
    this.temps_ = _c.temps_;
  }

}
