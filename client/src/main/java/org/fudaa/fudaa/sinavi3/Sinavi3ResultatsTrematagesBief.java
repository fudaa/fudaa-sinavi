/**
 *@creation 14 nov. 06
 *@modification $Dates$
 *@license  GNU General Public Licence 2
 *@copyright   (c)1998-2006 CETMEF 2 bd Gambetta F -60231 Compiegne
 *@mail   devel@fudaa.fr 
 */
package org.fudaa.fudaa.sinavi3;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JScrollPane;

import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.table.CtuluTableExcelWriter;
import org.fudaa.fudaa.ressource.FudaaResource;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuInternalFrame;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuTabbedPane;
import com.memoire.bu.BuTable;
import com.memoire.fu.FuLog;

/**
 * classe de gestion des resultats de la generation des bateaux propose 2 onglets: le premier propose un affichage
 * 
 * @version $Version$
 * @author Adrien Hadoux
 */

public class Sinavi3ResultatsTrematagesBief extends Sinavi3InternalFrame {

  /**
   * ensemble des donn�es du tableau sous la forme de data
   */
  Object[][] data;

  /**
   * Tableau r�capitulatif des r�sultats de la simulation
   */
  BuTable tableau_;

  String titreTableau_[];/*
                           * ={"Cat�gorie","Num navire","position","heure entree","heure
                           * sortie","depart","arrivee","att.occupation","att.secu","att.indispo"};
                           */

  /**
   * Panel tabbed qui g�re les 2 onglets, ie les 2 versions d'affichage des r�sultats:
   */
  BuTabbedPane panelPrincipal_ = new BuTabbedPane();

  /**
   * Panel cniotenant le tableau et les boutns de controles
   */
  BuPanel panelGestionTableau_ = new BuPanel();
  
  /**
   * Panel contenant la d�finition de la recherche
   */
  BuPanel selectionPanel_ = new BuPanel();

  /**
   * panel de gestion du tableau et des diff�rents boutons
   */
  BuPanel panelTableau_ = new BuPanel();

  /**
   * panel de gestion des boutons
   */
  BuPanel controlPanel_ = new BuPanel();

  /**
   * combolist qui permet de selectionenr les lignes deu tableau a etre affich�es:
   */
  JComboBox ListeBiefs_ = new JComboBox();

  /**
   * buoton de generation des resultats
   */
  private final BuButton exportationExcel_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_generer"), "Exporter");

  private final BuButton quitter_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_quitter"), "Quitter");

  /**
   * donnees de la simulation
   */
  Sinavi3DataSimulation donnees_;

  /**
   * constructeur de la sous fenetre de gestion des resultats:
   */
  public Sinavi3ResultatsTrematagesBief(final Sinavi3DataSimulation _donnees) {
    super("Tr�matages dans les tron�ons", true, true, true, true);

    // recuperation des donn�es de la simulation
    donnees_ = _donnees;

    // calcul des donn�es pour tous les chenaux
    Sinavi3AlgorithmeTrematageChenal.calcul(donnees_);

    setSize(820, 400);
    setBorder(Sinavi3Bordures.compound_);
    this.getContentPane().setLayout(new BorderLayout());

    this.getContentPane().add(this.selectionPanel_, BorderLayout.NORTH);
    this.getContentPane().add(this.panelPrincipal_, BorderLayout.CENTER);

    // ajout du tableau dans le panel tabbed
    panelPrincipal_.addTab("R�sultats d�taill�s", FudaaResource.FUDAA.getIcon("crystal_arbre"), panelGestionTableau_);

    /*******************************************************************************************************************
     * gestion du panel de selection
     ******************************************************************************************************************/
    this.selectionPanel_.setLayout(new GridLayout(1, 1));
    BuPanel selectionPanel1 = new BuPanel(new FlowLayout(FlowLayout.LEFT));
    selectionPanel1.add(new JLabel("Tron�on � �tudier:"));
    selectionPanel1.add(this.ListeBiefs_);
    this.selectionPanel_.add(selectionPanel1);
    
    /*******************************************************************************************************************
     * gestion du panel tableau panelGestionTableau_
     ******************************************************************************************************************/

    // etape 1: architecture du panel panelGestionTableau_
    panelGestionTableau_.setLayout(new BorderLayout());

    // definition d un panel ascenceur pour stocer le tableau:
    final JScrollPane asc = new JScrollPane(this.panelTableau_);

    // ajout au centre du panel qui contiendra le tableau d affichage
    this.panelGestionTableau_.add(asc, BorderLayout.CENTER);

    // panel qui contient les differents boutons
    this.controlPanel_.add(quitter_);
 //   this.controlPanel_.add(new JLabel("Selectionnez le tron�on � visualiser:   "));
 //   this.controlPanel_.add(this.ListeBiefs_);
    this.controlPanel_.add(exportationExcel_);
    this.panelGestionTableau_.add(this.controlPanel_, BorderLayout.SOUTH);

    // etape 2: remplissage du comboliste avec les noms des chenaux

    for (int i = 0; i < donnees_.listeBief_.listeBiefs_.size(); i++) {
      this.ListeBiefs_.addItem("" + donnees_.listeBief_.retournerBief(i).nom_);
    }

    // etape 3: gestion de l affichage du tableau de donn�es
    // remarque : cette m�thode sera syst�matiquement appel�e afni d'op�rer un changement:
    affichageTableau(0);

    // etape 4: listener du combolist afin de pouvoir selectionner le navire qui nous interesse
    // a noter que la selection va faire surligner le navire souhait�
    this.ListeBiefs_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        // evenement du clic sur le bouton
        final int val = ListeBiefs_.getSelectedIndex();
        affichageTableau(val);
        setTitle("Tr�matages dans le tron�on " + ListeBiefs_.getSelectedItem());
      }
    });

    // bouton qui permet de generer le contenu du tableau en ficheir excel:
    this.exportationExcel_
        .setToolTipText("Exporte le contenu du tableau au format xls");
    exportationExcel_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        File fichier;
        final JFileChooser fc = new JFileChooser();
        final int returnVal = fc.showSaveDialog(Sinavi3ResultatsTrematagesBief.this);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
          fichier = fc.getSelectedFile();
          final File f = CtuluLibFile.appendExtensionIfNeeded(fichier, "xls");
          final Sinavi3ModeleExcel modele = new Sinavi3ModeleExcel();
          modele.nomColonnes_ = titreTableau_;
          modele.data_ = new Object[data.length + 2][titreTableau_.length];
          for (int i = 0; i < titreTableau_.length; i++) {
            modele.data_[0][i] = titreTableau_[i];
          }
          /** recopiage des donn�es */
          for (int i = 0; i < data.length; i++) {
            modele.data_[i + 2] = data[i];
          }
          modele.setNbRow(data.length + 2);
          /** on essaie d 'ecrire en format excel */
          final CtuluTableExcelWriter ecrivain = new CtuluTableExcelWriter(modele, f);
          try {
            ecrivain.write(null);
          } catch (final RowsExceededException _err) {
            FuLog.error(_err);
          } catch (final WriteException _err) {
            FuLog.error(_err);
          } catch (final IOException _err) {
            FuLog.error(_err);
          }
        }// fin du if si le composant est bon
      }// fin de la methode public actionPerformed
    });
    /** listener des boutons quitter */
    this.quitter_.setToolTipText("Ferme la sous-fen�tre");
    final ActionListener actionQuitter = new ActionListener() {
      public void actionPerformed(ActionEvent e) {
    	  Sinavi3ResultatsTrematagesBief.this.windowClosed();

      }
    };
    this.quitter_.addActionListener(actionQuitter);

    // ajout d'un menuBar
    // petite barre de menu agreable
 /*   final JMenuBar menuBar = new JMenuBar();
    final JMenu menuFile = new JMenu("Fichier");
    final JMenuItem menuFileExit = new JMenuItem("Quitter");
    final JMenu menuOption = new JMenu("Options");
    final JMenu menuInfo = new JMenu("A propos de");

    menuFileExit.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {
    	  Sinavi3ResultatsTrematagesBief.this.windowClosed();
      }
    });
    menuFile.add(menuFileExit);
    menuBar.add(menuFile);
    menuBar.add(menuOption);
    menuBar.add(menuInfo);
    setJMenuBar(menuBar);
*/
  }

  /**
   * Methode d'affichage du tableau remarque: cete m�thode sert aussi de rafraichissement du tableau
   * 
   * @param val entier qui indique le num�ro du tron�on � afficher
   */
  void affichageTableau(final int val) {
    // ici le nombre de colonnes est de 2 puisqu'il s'agit d'un affichage du nom de la cat�gorie et de son nombre de
    // navires
    data = new Object[this.donnees_.listeBateaux_.listeNavires_.size()][this.donnees_.listeBateaux_.listeNavires_
        .size() + 1];

    titreTableau_ = new String[this.donnees_.listeBateaux_.listeNavires_.size() + 1];
    titreTableau_[0] = "";
    for (int i = 0; i < this.donnees_.listeBateaux_.listeNavires_.size(); i++) {
      titreTableau_[i + 1] = "" + this.donnees_.listeBateaux_.retournerNavire(i).nom;
    }

    for (int i = 0; i < this.donnees_.listeBateaux_.listeNavires_.size(); i++) {
      data[i][0] = "" + this.donnees_.listeBateaux_.retournerNavire(i).nom;
      // calculs avec vecteur
      for (int j = 0; j < this.donnees_.listeBateaux_.listeNavires_.size(); j++) {
        // on r�cup�re la matrice de l'indice val(val= indice du tron�on que lon veut visualiser)
        // cette matrice contient les informations de croisements de la flotte complete dans le tron�on selectionn�
        data[i][j + 1] = ""
            + FonctionsSimu.diviserSimu(this.donnees_.params_.ResultatsCompletsSimulation.tableauDeMatricesCroisementsChenal[val].matriceCroisements[i][j]);
      }
    }

    // etape 3: creation du tableau a partir des donn�es g�n�r�es ci dessus:
    this.tableau_ = new BuTable(data, this.titreTableau_) {
      public boolean isCellEditable(final int row, final int col) {
        return false;
      }
    };

    // etape 4: ajout sdu tableau cr�� dans l'interface
    org.fudaa.fudaa.sinavi.factory.ColumnAutoSizer.sizeColumnsToFit(tableau_);
    tableau_.revalidate();
    this.panelTableau_.removeAll();
    this.panelTableau_.setLayout(new BorderLayout());
    this.panelTableau_.add(tableau_.getTableHeader(), BorderLayout.PAGE_START);
    this.panelTableau_.add(this.tableau_, BorderLayout.CENTER);
    // this.panelTableau_.add(this.controlPanel_,BorderLayout.SOUTH);

    // mise a jour de l'affichage
    this.revalidate();
    this.updateUI();

  }

  /**
   * Methode qui s active lorsque l'on quitte l'application
   */
  protected void windowClosed() {
    // verif.stop(); //stop n est aps sur on le modifie donc par une autre variable

    // desactivation du thread
    // dureeVieThread=false;

    System.out.print("Fin de la fenetre de gestion des cercles!!");
    dispose();
  }
}

