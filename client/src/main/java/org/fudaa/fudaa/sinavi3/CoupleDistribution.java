package org.fudaa.fudaa.sinavi3;

/**
 * classe de gestion des donn�es pour les distributions.
 * 
 * @version $Version$
 * @author Mederic FARGEIX
 */
class CoupleDistribution {
  double duree_;
  int nombre_;

  public CoupleDistribution(final double _duree, final int _nombre) {

    this.duree_ = _duree;
    this.nombre_ = _nombre;
  }

  public CoupleDistribution(final CoupleDistribution _c) {

    this.duree_ = _c.duree_;
    this.nombre_ = _c.nombre_;
  }

}