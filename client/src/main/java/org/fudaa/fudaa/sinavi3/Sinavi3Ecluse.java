package org.fudaa.fudaa.sinavi3;

import java.util.ArrayList;

import org.fudaa.ebli.network.simulationNetwork.NetworkUserObject;

/**
 * Classes regroupant les donn�es des ecluses.
 * 
 * @author Adrien Hadoux
 */
public class Sinavi3Ecluse implements NetworkUserObject{

  String nom_;
  double longueur_;
  double largeur_;
  int tempsFausseBassinneeMontant_;
  int tempsFausseBassinneeAvalant_;
  double profondeur_;
  double hauteurchute=0;
  int dureeManoeuvreEntrant_=-1;
  int dureeManoeuvreSortant_ = -1;
  
  int dureeManoeuvreEntrant2_ =-1;
  int dureeManoeuvreSortant2_ = -1;
  
  Sinavi3Horaire h_ = new Sinavi3Horaire();

  double dureeIndispo_;
  double frequenceMoyenne_;
  double coefficientBassinEpargne_;
  // loi: indice de 1 a 10
  int loiIndispo_=1;
  int loiFrequence_=1;

  /**
   * Type de loi ENTIER QUI PREND LA VALEUR DE LA LOI CHOISIE.<br>: 0 => loi d erlang<br>
   * 1 => deterministe <br>
   * 2 => journaliere par defaut loi d'erlang
   */
  int typeLoi_;

  /**
   * Declaration du tableau de loi deterministe.
   */
  ArrayList loiDeterministe_ = new ArrayList();
  /**
   * Gare amont de l ecluse par defaut mis a zero.
   */
  int gareAmont_;

  /**
   * gare aval mis par defaut a la gare 1.
   */

  int gareAval_ = 1;

  /**
   * constructeur par d�faut de la classe ecluse.
   */
  public Sinavi3Ecluse() {

  }

  /**
   * Constructeur de la classe ecluse pour une saisie des attributs.
   * 
   * @param _nom
   * @param _longueur
   * @param _largeur
   * @param _tempsEclusee
   * @param _tempsFausseBassinnee
   * @param _creneauEtaleAvantPleineMerDeb
   * @param _creneauEtaleAvantPleineMerFin
   * @param _creneauEtaleApresPleineMerDeb
   * @param _creneauEtaleApresPleineMerFin
   * @param _h
   * @param _gareAmont
   * @param _gareAval
   */
  public Sinavi3Ecluse(final String _nom, final double _longueur, final double _largeur, final int _tempsEclusee,
      final int _tempsFausseBassinnee, final int _creneauEtaleAvantPleineMerDeb,
      final double _creneauEtaleAvantPleineMerFin, final double _creneauEtaleApresPleineMerDeb,
      final int _creneauEtaleApresPleineMerFin, final Sinavi3Horaire _h, final int _gareAmont, final int _gareAval) {
    super();
    this.nom_ = _nom;
    this.longueur_ = _longueur;
    this.largeur_ = _largeur;
    this.tempsFausseBassinneeMontant_ = _tempsEclusee;
    this.tempsFausseBassinneeAvalant_ = _tempsFausseBassinnee;
    this.dureeManoeuvreEntrant_ = _creneauEtaleAvantPleineMerDeb;
    this.dureeManoeuvreSortant_ = _creneauEtaleApresPleineMerFin;
    this.h_ = _h;
    this.gareAmont_ = _gareAmont;
    this.gareAval_ = _gareAval;
  }

  String[] affichage() {
    final String[] t = new String[8];
    t[0] = " nom: " + nom_;
    t[1] = "\n longueur: " + (float) longueur_;
    t[2] = "\n largeur: " + (float) largeur_;
    t[3] = "\n temps eclusee: " + (float) tempsFausseBassinneeMontant_;
    t[4] = "\n fausse bassinnee: " + (float) tempsFausseBassinneeAvalant_;
    t[5] = "\n creneau etale:";
    t[6] = "\n        avant pleine mer: " + (float) this.dureeManoeuvreEntrant_;
    t[7] = "\n        apr�s pleine mer: " + (float) this.dureeManoeuvreSortant_;

    return t;
  }

public String getNom() {
	return nom_;
}

public void setNom_(String nom_) {
	this.nom_ = nom_;
}

public double getLongueur_() {
	return longueur_;
}

public void setLongueur_(double longueur_) {
	this.longueur_ = longueur_;
}

public double getLargeur_() {
	return largeur_;
}

public void setLargeur_(double largeur_) {
	this.largeur_ = largeur_;
}

public int getTempsFausseBassinneeMontant_() {
	return tempsFausseBassinneeMontant_;
}

public void setTempsFausseBassinneeMontant_(int tempsFausseBassinneeMontant_) {
	this.tempsFausseBassinneeMontant_ = tempsFausseBassinneeMontant_;
}

public int getTempsFausseBassinneeAvalant_() {
	return tempsFausseBassinneeAvalant_;
}

public void setTempsFausseBassinneeAvalant_(int tempsFausseBassinneeAvalant_) {
	this.tempsFausseBassinneeAvalant_ = tempsFausseBassinneeAvalant_;
}

public double getProfondeur_() {
	return profondeur_;
}

public void setProfondeur_(double profondeur_) {
	this.profondeur_ = profondeur_;
}

public double getHauteurchute() {
	return hauteurchute;
}

public void setHauteurchute(double hauteurchute) {
	this.hauteurchute = hauteurchute;
}

public int getDureeManoeuvreEntrant_() {
	return dureeManoeuvreEntrant_;
}

public void setDureeManoeuvreEntrant_(int dureeManoeuvreEntrant_) {
	this.dureeManoeuvreEntrant_ = dureeManoeuvreEntrant_;
}

public int getDureeManoeuvreSortant_() {
	return dureeManoeuvreSortant_;
}

public void setDureeManoeuvreSortant_(int dureeManoeuvreSortant_) {
	this.dureeManoeuvreSortant_ = dureeManoeuvreSortant_;
}

public Sinavi3Horaire getH_() {
	return h_;
}

public void setH_(Sinavi3Horaire h_) {
	this.h_ = h_;
}

public double getDureeIndispo_() {
	return dureeIndispo_;
}

public void setDureeIndispo_(double dureeIndispo_) {
	this.dureeIndispo_ = dureeIndispo_;
}

public double getFrequenceMoyenne_() {
	return frequenceMoyenne_;
}

public void setFrequenceMoyenne_(double frequenceMoyenne_) {
	this.frequenceMoyenne_ = frequenceMoyenne_;
}

public int getLoiIndispo_() {
	return loiIndispo_;
}

public void setLoiIndispo_(int loiIndispo_) {
	this.loiIndispo_ = loiIndispo_;
}

public int getLoiFrequence_() {
	return loiFrequence_;
}

public void setLoiFrequence_(int loiFrequence_) {
	this.loiFrequence_ = loiFrequence_;
}

public int getTypeLoi_() {
	return typeLoi_;
}

public void setTypeLoi_(int typeLoi_) {
	this.typeLoi_ = typeLoi_;
}

public ArrayList getLoiDeterministe_() {
	return loiDeterministe_;
}

public void setLoiDeterministe_(ArrayList loiDeterministe_) {
	this.loiDeterministe_ = loiDeterministe_;
}

public int getGareAmont_() {
	return gareAmont_;
}

public void setGareAmont_(int gareAmont_) {
	this.gareAmont_ = gareAmont_;
}

public int getGareAval_() {
	return gareAval_;
}

public void setGareAval_(int gareAval_) {
	this.gareAval_ = gareAval_;
}

public double getCoefficientBassinEpargne_() {
	return coefficientBassinEpargne_;
}

public void setCoefficientBassinEpargne_(double coefficientBassinEpargne_) {
	this.coefficientBassinEpargne_ = coefficientBassinEpargne_;
}

public String getName() {
	return nom_;
}

public String getNom_() {
	return nom_;
}

public int getDureeManoeuvreEntrant2_() {
	return dureeManoeuvreEntrant2_;
}

public void setDureeManoeuvreEntrant2_(int dureeManoeuvreEntrant2_) {
	this.dureeManoeuvreEntrant2_ = dureeManoeuvreEntrant2_;
}

public int getDureeManoeuvreSortant2_() {
	return dureeManoeuvreSortant2_;
}

public void setDureeManoeuvreSortant2_(int dureeManoeuvreSortant2_) {
	this.dureeManoeuvreSortant2_ = dureeManoeuvreSortant2_;
}

public void setName(String value) {
	setNom_(value);
	
}

}
