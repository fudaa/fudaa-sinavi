/**
 *@creation 14 nov. 06
 *@modification $Dates$
 *@license  GNU General Public Licence 2
 *@copyright   (c)1998-2006 CETMEF 2 bd Gambetta F -60231 Compiegne
 *@mail   devel@fudaa.fr 
 */
package org.fudaa.fudaa.sinavi3;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.ByteArrayInputStream;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;

import org.fudaa.ctulu.image.CtuluImageExport;
import org.fudaa.ebli.graphe.BGraphe;
import org.fudaa.fudaa.ressource.FudaaResource;
import org.jfree.chart.ChartPanel;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuScrollPane;
import com.memoire.bu.BuTabbedPane;
import com.memoire.bu.BuTable;

/**
 * classe de gestion des resultats de la generation des bateaux propose 2 onglets: le premier propose un affichage.
 * 
 * @version $Version$
 * @author Adrien Hadoux
 */
public class Sinavi3ResultatGenerationBateaux extends Sinavi3InternalFrame /*implements ActionListener */{

  /**
   * Parametre resultat pour cetet interface: il s'agit d'un tableau d'entier: ce tableau contient N cases avec N le
   * nombre de cat�gories de navires et la case i du tableau corresponda au nombre de navires g�n�r�s pour la cat�gorie
   * i
   */
  int[] tabGen;

  /**
   * tableau de checkBox destiner a l utilisateuir pour choisr les navire a visualiser sur les diff�rents supports.
   */
  JCheckBox[] tableauChoixNavires_;

  /**
   * ensemble des donn�es du tableau sous la forme de data
   */
  Object[][] data_;

  /**
   * Graphe associ�e aux r�sultats de la g�n�ration de bateaux
   */
  BGraphe graphe_ = new BGraphe();

  /**
   * Tableau r�capitulatif des r�sultats de la simulation
   */
  BuTable tableau_;

  
  /**
   * Panel qui affiche le camembert
   */
  ChartPanel camembert_;
  
  /**
   * Panel contenant le bouton quitter.
   */
  BuPanel panelQuitter_ = new BuPanel();
  
  /**
   * panel qui affiche l'histogramme 3d
   */
  ChartPanel histo3d_;
  
  
  String[] titreTableau_ = { "Cat�gorie", "Nombre de bateaux g�n�r�s" };

  /**
   * Panel tabbed qui g�re les 2 onglets, ie les 2 versions d'affichage des r�sultats:
   */
  BuTabbedPane panelPrincipal_ = new BuTabbedPane();

  /**
   * Panel cniotenant le tableau et les boutns de controles
   */
  BuPanel panelGestionTableau_ = new BuPanel();

  /**
   * Panel cniotenant le camembert
   */
  BuPanel panelCamembert_ = new BuPanel(new BorderLayout());
  
  /**
   * panel de gestion du tableau et des diff�rents boutons
   */
  BuPanel panelTableau_ = new BuPanel();

  /**
   * panel de gestion des boutons
   */
  BuPanel controlPanel_ = new BuPanel();

  /**
   * Panel des options: type affichages, colonnes � faire figurer:
   */
  BuPanel optionPanel_ = new BuPanel(new BorderLayout());
  
  String[] typeGraphe_ = { "Lignes", "Barres"};
  JComboBox choixTypeGraphe_ = new JComboBox(typeGraphe_);

  /**
   * Panel de gestion des boutons du camembert
   */
  BuPanel controlPanelCamembert_ = new BuPanel();
  
  /**
   * Panel de gestion des boutons des courbes
   */
  BuPanel controlPanelCourbes_ = new BuPanel();

  /**
   * panel de gestion des courbes
   */
  BuPanel panelCourbe_ = new BuPanel();

  /**
   * combolist qui permet de selectionenr les lignes deu tableau a etre affich�es:
   */
  JComboBox ListeNavires_ = new JComboBox();

  /**
   * buoton de generation des resultats
   */
  private final BuButton exportationExcel_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_generer"), "Exporter");

  final BuButton exportationgraphe_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_generer"), "Exporter");

  private final BuButton quitter_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_quitter"), "Quitter");

  Border raisedBevel_ = BorderFactory.createRaisedBevelBorder();
  Border loweredBevel_ = BorderFactory.createLoweredBevelBorder();
  Border compound_ = BorderFactory.createCompoundBorder(raisedBevel_, loweredBevel_);
  Border bordnormal_ = BorderFactory.createEtchedBorder();
  /**
   * donnees de la simulation.
   */
  Sinavi3DataSimulation donnees_;

  /**
   * constructeur de la sous fenetre de gestion des resultats.
   */
  Sinavi3ResultatGenerationBateaux(final Sinavi3DataSimulation _donnees) {
    super("Bateaux g�n�r�s", true, true, true, true);

    // recuperation des donn�es de la simulation
    donnees_ = _donnees;

    setSize(700, 500);
    setBorder(Sinavi3Bordures.compound_);
    this.getContentPane().setLayout(new BorderLayout());
    this.getContentPane().add(this.panelPrincipal_, BorderLayout.CENTER);
    this.getContentPane().add(this.optionPanel_, BorderLayout.WEST);
    this.getContentPane().add(panelQuitter_, BorderLayout.SOUTH);
    panelQuitter_.add(quitter_);
    
    tableauChoixNavires_ = new JCheckBox[this.donnees_.listeBateaux_.listeNavires_.size()];
    for (int i = 0; i < this.donnees_.listeBateaux_.listeNavires_.size(); i++) {
      this.tableauChoixNavires_[i] = new JCheckBox(this.donnees_.listeBateaux_.retournerNavire(i).nom, true);
      this.tableauChoixNavires_[i].addActionListener(this);

    }
    
    //-- creatiion de l'histo 3d --//
    histo3d_=creerHisto3d();
    // ajout du tableau dans le panel tabbed
    panelPrincipal_.addTab("R�sultats d�taill�s", FudaaResource.FUDAA.getIcon("crystal_arbre"), panelGestionTableau_);

    panelPrincipal_.addTab("R�partition par cat�gorie", FudaaResource.FUDAA.getIcon("crystal_graphe"), panelCamembert_);
    
    // ajout des courbes dans le panel de la sous fenetre
    panelPrincipal_.addTab("Exploitation graphique", FudaaResource.FUDAA.getIcon("crystal_graphe"), panelCourbe_);

    //panelPrincipal_.addTab("Histogramme 3D", FudaaResource.FUDAA.getIcon("crystal_graphe"), histo3d_);

    
    /*******************************************************************************************************************
     * gestion du panel tableau panelGestionTableau_
     ******************************************************************************************************************/

    // etape 1: architecture du panel panelGestionTableau_ et ascenseur contenant le tableau
    panelGestionTableau_.setLayout(new BorderLayout());
    final JScrollPane asc = new JScrollPane(this.panelTableau_);
    this.panelGestionTableau_.add(asc, BorderLayout.CENTER);

    // panel qui contient les differents boutons
    this.controlPanel_.add(exportationExcel_);
    this.panelGestionTableau_.add(this.controlPanel_, BorderLayout.SOUTH);
    
    final Border bordureTab = BorderFactory.createEtchedBorder();
    controlPanel_.setBorder(bordureTab);
    
    //panel du cammbert
    camembert_= creerCamembert();
    this.panelCamembert_.add(camembert_,BorderLayout.CENTER);
    this.panelCamembert_.add(this.controlPanelCamembert_, BorderLayout.SOUTH);

    // etape 2: remplissage du comboliste avec les noms des navires
    this.ListeNavires_.addItem("toutes");
    for (int i = 0; i < donnees_.listeBateaux_.listeNavires_.size(); i++) {
      this.ListeNavires_.addItem("" + donnees_.listeBateaux_.retournerNavire(i).nom);
    }

    // etape 3: gestion de l affichage du tableau de donn�es
    // remarque : cette m�thode sera syst�matiquement appel�e afni d'op�rer un changement:
    affichageTableau(-1);

    // etape 4: listener du combolist afin de pouvoir selectionner le navire qui nous interesse
    // a noter que la selection va faire surligner le navire souhait�
    this.ListeNavires_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent _e) {
        // evenement du clic sur le bouton
        final int val = ListeNavires_.getSelectedIndex();
        affichageTableau(val - 1);

      }
    });

    // bouton qui permet de generer le contenu du tableau en ficheir excel:
    this.exportationExcel_
        .setToolTipText("Exporte le contenu du tableau au format xls");
    exportationExcel_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent _e) {
    	  Sinavi3GestionResultats.exportTableau(Sinavi3ResultatGenerationBateaux.this, titreTableau_, data_);
      }// fin de la methode public actionPerformed
    });

    /*******************************************************************************************************************
     * gestion du panel des options
     ******************************************************************************************************************/

    BuGridLayout lo=new BuGridLayout(1,5,0,true,false,false,false);
    final BuPanel panoption = new BuPanel(lo);
    final BuScrollPane panoptionGestionScroll = new BuScrollPane(panoption);
    this.optionPanel_.add(panoptionGestionScroll);

    final Box bVert2 = Box.createVerticalBox();
    panoption.add(bVert2);
    for (int i = 0; i < this.tableauChoixNavires_.length; i++) {
      bVert2.add(this.tableauChoixNavires_[i]);
    }
    final TitledBorder bordure1 = BorderFactory.createTitledBorder(BorderFactory
        .createEtchedBorder(EtchedBorder.LOWERED), "Cat�gories");
    bVert2.setBorder(bordure1);

    /*******************************************************************************************************************
     * gestion du panel courbes panelCourbe_
     ******************************************************************************************************************/
    // etape 1: architecture de la frame
    this.panelCourbe_.setLayout(new BorderLayout());

    // etape 2: creation de la description du graphe
    final String descriptionGraphe = affichageGraphe();

    // etape 3: rattachement du descriptif du graphe au graphe
    this.graphe_.setFluxDonnees(new ByteArrayInputStream(descriptionGraphe.getBytes()));

    // etape 4: affichage du graphe dans le panel associ�
    this.panelCourbe_.add(this.graphe_, BorderLayout.CENTER);

    // etape 5: bouton de generation du fichier image
    exportationgraphe_.setToolTipText("Exporte le graphe au format image");
    exportationgraphe_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent _e) {
        CtuluImageExport.exportImageFor(donnees_.application_, graphe_);
      }
    });

    // etape 6: creation du panel des boutons des courbes:
    this.panelCourbe_.add(this.controlPanelCourbes_, BorderLayout.SOUTH);
    
    this.controlPanelCourbes_.add(exportationgraphe_);
    this.controlPanelCourbes_.add(new JLabel("          "));
    this.controlPanelCourbes_.add(choixTypeGraphe_);
    
    final Border bordure = BorderFactory.createEtchedBorder();
    controlPanelCourbes_.setBorder(bordure);
    
    choixTypeGraphe_.setToolTipText("Modifie le style de la repr�sentation graphique");
    choixTypeGraphe_.addActionListener(this);

    /** listener des boutons quitter */
    this.quitter_.setToolTipText("Ferme la sous-fen�tre");
    final ActionListener actionQuitter = new ActionListener() {
      public void actionPerformed(ActionEvent _e) {
        Sinavi3ResultatGenerationBateaux.this.windowClosed();
      }
    };
    this.quitter_.addActionListener(actionQuitter);

  }

  /**
   * Methode d'affichage du tableau remarque: cete m�thode sert aussi de rafraichissement du tableau.
   * 
   * @param val entier qui indique le num�ro de la cat�gorie de navire � afficher si ce parametre vaut -1 alorso n
   *          affiche la totalit� des navires
   */
  void affichageTableau(final int _val) {
    // affichage du tableau

    /*******************************************************************************************************************
     * HEP HEP!!! C EST ICI QUE L ON EFFECTUE LE CALCUL QUI RETOURNE LE TABLEAU * D'ENTIER DONT CHAQUE CASE I EST LE
     * NOMBRE DE NAVIRES GENERES POUR LA CATEGORIE I * CALCUL EFFECTUE UNIQUEMENT AVEC ELS DONNEEES EN SORTIE DE LA
     * SIMULATION * ******************************************************************************************
     */
    // allocation memoire du tableau de n cases
    this.tabGen = new int[this.donnees_.listeBateaux_.listeNavires_.size()];

    // operation magique qui permet de determiner ce tableau

    // etape 2: g�n�rer la liste des donn�es � afficher dans le tableau via une matrice de type object
    // ici le nombre de colonnes est de 2 puisqu'il s'agit d'un affichage du nom de la cat�gorie et de son nombre de
    // navires
    data_ = new Object[this.donnees_.listeBateaux_.listeNavires_.size() + 1][2];

    if (_val < 0) {
      int compteurNavires = 0;
      int indiceNav = 0;
      for (int i = 0; i < this.donnees_.listeBateaux_.listeNavires_.size(); i++) {
        if (this.tableauChoixNavires_[i].isSelected()) {
          data_[indiceNav][0] = this.donnees_.listeBateaux_.retournerNavire(i).nom;

          data_[indiceNav][1] = "" + FonctionsSimu.diviserSimu(this.donnees_.params_.ResultatsCompletsSimulation.ResultatsGenerationNavires[i]);
          compteurNavires += FonctionsSimu.diviserSimu(this.donnees_.params_.ResultatsCompletsSimulation.ResultatsGenerationNavires[i]);
          indiceNav++;

        }
      }
      data_[indiceNav][0] = "TOTAL";
      data_[indiceNav][1] = "" + compteurNavires;

    } else if (_val < this.donnees_.listeBateaux_.listeNavires_.size()) {
      // on affiche uniquement la ligne selectionn� par le combolist:
      data_ = new Object[1][2];
      data_[0][0] = this.donnees_.listeBateaux_.retournerNavire(_val).nom;
      data_[0][1] = "" + FonctionsSimu.diviserSimu(this.donnees_.params_.ResultatsCompletsSimulation.ResultatsGenerationNavires[_val]);
    }
    // etape 3: creation du tableau a partir des donn�es g�n�r�es ci dessus:
    this.tableau_ = new BuTable(data_, this.titreTableau_) {
      public boolean isCellEditable(final int _row, final int _col) {
        return false;
      }
    };

    // etape 4: ajout sdu tableau cr�� dans l'interface
    org.fudaa.fudaa.sinavi.factory.ColumnAutoSizer.sizeColumnsToFit(tableau_);
    tableau_.revalidate();
    this.panelTableau_.removeAll();
    this.panelTableau_.setLayout(new BorderLayout());
    this.panelTableau_.add(tableau_.getTableHeader(), BorderLayout.PAGE_START);
    this.panelTableau_.add(this.tableau_, BorderLayout.CENTER);
    // this.panelTableau_.add(this.controlPanel_,BorderLayout.SOUTH);

    // mise a jour de l'affichage
    this.revalidate();
    this.updateUI();

  }

  /**
   * Methode qui permet de d�crire le graphe � afficher.
   * 
   * @return chaine: chaine qui contient la des cription de la chaine de caracteres.
   */
  String affichageGraphe() {

	boolean histo = false;
	if (this.choixTypeGraphe_.getSelectedIndex() == 1) histo = true;
	  
    // determiner le nombre de cat�gories de navires selectionn�s
    int nbNavires = 0;
    for (int k = 0; k < this.tableauChoixNavires_.length; k++) {
      if (this.tableauChoixNavires_[k].isSelected()) {
        nbNavires++;
      }
    }

    // variable qui permet de determiner a quel indice du navire on est car on en prends pas tous les indices de navires
    int indiceNavire = 0;

    String g = "";

    g += "graphe\n{\n";
    g += "  titre \"G�n�ration de bateaux\"\n";
    g += "  sous-titre \"Etude par cat�gorie de bateaux\"\n";
    g += "  animation non\n";
    g += "  legende " + "oui" + "\n";
    
    g += " marges\n {\n";
    g += " gauche 100\n"; g += " droite 100\n"; g += " haut 50\n"; g += " bas 30\n }\n";

    g += "  axe\n  {\n"; // abscisses
    g += "    titre \"Cat�gorie" + "\"\n";
    g += "    orientation " + "horizontal" + "\n";
    g += "    graduations non\n";
    g += "    minimum " + 0 + "\n";
    g += "    maximum " + (nbNavires + 2)
        + "\n";
    g += "  }\n";

    g += "  axe\n  {\n"; // Ordonn�es
    g += "    titre \"Bateaux" + "\"\n";
    g += "    unite \"nombre\"\n";
    g += "    orientation " + "vertical" + "\n";
    g += "    graduations oui\n";
    g += "    minimum " + 0 + "\n";
    g += "    maximum " + FonctionsSimu.diviserSimu((Sinavi3AlgorithmeGenerationBateaux.max(donnees_)))
    // ********************ATTTTTTTTTTTTTEEEEEEENNNNNNNNNNTTTTTTTTION A DEFINIR DES QU ON CONNAIS LE TABELAU/ ON
    // DETERMINE LE MAX
        + "\n";
    g += "  }\n";
    /*
     * for (int i= 0; i < this.donnees_.categoriesNavires_.listeNavires_.size(); i++) { //A RESERVER POUR GERER
     * PLUSIEURS COURBES
     */
    g += "  courbe\n  {\n";
    g += "    titre \"" + "nombre de bateaux g�n�r�s"
    /*
     * + "/" + choixString(choix, i, 1) + "/" + choixString(choix, i, 2)
     */
    + "\"\n";
    g += "    type ";
    if (histo) g += "histogramme"; else g += "courbe";
    g += "\n";
    g += "    trace lineaire\n";
    g += "    marqueurs oui\n";
    g += "    aspect\n {\n";
    g += "contour.largeur 1 \n";
    g += "surface.couleur BB8800 \n";
    g += "texte.couleur 000000 \n";
    g += "contour.couleur 000000 \n";
    g += "    }\n";
    g += "    valeurs\n    {\n";
    /*
     * final Vector coordX= (Vector)_courbes.get(i * 2); final Vector coordY= (Vector)_courbes.get(i * 2 + 1); for (int
     * n= 0;(n < coordX.size()) || (n < coordY.size()); n++) {
     */
    indiceNavire = 0;
    for (int n = 0; n < this.donnees_.listeBateaux_.listeNavires_.size(); n++) {
      if (this.tableauChoixNavires_[n].isSelected()) {
        g += (indiceNavire + 1)// numero de la cat�gorie
            + " " +FonctionsSimu.diviserSimu( this.donnees_.params_.ResultatsCompletsSimulation.ResultatsGenerationNavires[n]) // tableau qui contient le nombre de navire pour chaque cat�gorie
            + "\n etiquette  \n \"" + this.donnees_.listeBateaux_.retournerNavire(n).nom + "\" \n"
            + "\n";
        indiceNavire++;
      }
    }// din du pour remplissage des coordonn�es
    g += "    }\n";
    g += "  }\n";
    // }//fin du for

    return g;
  }

    public void actionPerformed(final ActionEvent _ev) {
    final Object source = _ev.getSource();

    // action commune a tous les �v�nements: redimensionnement de la fenetre
    final Dimension actuelDim = this.getSize();
    final Point pos = this.getLocation();

    // si la source provient d un navire du tableau de checkBox
    boolean trouve = false;
    for (int k = 0; k < this.tableauChoixNavires_.length && !trouve; k++) {
      if (source == this.tableauChoixNavires_[k]) {
        trouve = true;
        affichageTableau(-1);
        // mise a jour des courbes
        final String descriptionCourbes = this.affichageGraphe();
        this.graphe_.setFluxDonnees(new ByteArrayInputStream(descriptionCourbes.getBytes()));

       
        modifierCamemberts();
      }
    }
    
    if (source == this.choixTypeGraphe_) {
    	// mise a jour des courbes
        final String descriptionCourbes = this.affichageGraphe();
        this.graphe_.setFluxDonnees(new ByteArrayInputStream(descriptionCourbes.getBytes()));
    }

    // on redimensionne la fenetre comme elle etais avant manipulation des elements graphique
    this.setSize(actuelDim);
    this.setLocation(pos);
  }// fin de actionPerformed

  
  /**
   * Methode d'affichage du camembert correspondant � la proportion de navires de la simulation.
   * @return
   */
  public ChartPanel creerCamembert(){
	  
	  String[] noms=new String[donnees_.listeBateaux_.listeNavires_.size()];
	  for(int i=0;i<donnees_.listeBateaux_.listeNavires_.size();i++) {
		  noms[i]=donnees_.listeBateaux_.retournerNavire(i).nom;
	  }
	  
	  return Sinavi3JFreeChartCamembert.creerCamembert(FonctionsSimu.diviserSimu(this.donnees_.params_.ResultatsCompletsSimulation.ResultatsGenerationNavires), noms, "G�n�ration de bateaux par cat�gorie", false);
	  
  }
  
 /*public ChartPanel creerCamembert3d(){
	  
	  String[] noms=new String[donnees_.categoriesNavires_.listeNavires_.size()];
	  for(int i=0;i<donnees_.categoriesNavires_.listeNavires_.size();i++)
		  noms[i]=donnees_.categoriesNavires_.retournerNavire(i).nom;
	  
	  
	  
	  return SiporJFreeChartCamembert.creerCamembert3d(this.donnees_.params_.ResultatsCompletsSimulation.ResultatsGenerationNavires, noms, "G�n�rations de navires par cat�gories");
	  
  }*/
  
 
 public void modifierCamemberts(){
	 
	  int nbNaviresAffiches=0;
	  for(int i=0;i<donnees_.listeBateaux_.listeNavires_.size();i++) {
		  if (this.tableauChoixNavires_[i].isSelected())
			  nbNaviresAffiches++;
	  }
	  
	  String[] noms=new String[nbNaviresAffiches];
	  int cpt=0;
	  for(int i=0;i<donnees_.listeBateaux_.listeNavires_.size();i++) {
		  if (this.tableauChoixNavires_[i].isSelected())
			  noms[cpt++]=donnees_.listeBateaux_.retournerNavire(i).nom;
	  }
	  
	  int[] tabNavires=new int[nbNaviresAffiches];
	  cpt=0;
	  for(int i=0;i<donnees_.listeBateaux_.listeNavires_.size();i++) {
		  if (this.tableauChoixNavires_[i].isSelected())
			  tabNavires[cpt++]=this.donnees_.params_.ResultatsCompletsSimulation.ResultatsGenerationNavires[i];
	  }
	 
	 Sinavi3JFreeChartCamembert.modifierCamembert(camembert_, FonctionsSimu.diviserSimu(tabNavires), noms, "G�n�ration de bateaux par cat�gorie", false);
	 }
 
 
 public ChartPanel creerHisto3d(){
	  
	 String[] noms=new String[donnees_.listeBateaux_.listeNavires_.size()];
	  int cpt=0;
	  for(int i=0;i<donnees_.listeBateaux_.listeNavires_.size();i++) {
		 	  noms[cpt++]=donnees_.listeBateaux_.retournerNavire(i).nom;
	  }
	  
	  return Sinavi3JFreeChartCamembert.creerHisto3d(this.donnees_.params_.ResultatsCompletsSimulation.ResultatsGenerationNavires, "G�n�ration de bateaux par cat�gorie",noms);
}
  
  /**
   * Methode qui s active lorsque l'on quitte l'application.
   */
  protected void windowClosed() {
    System.out.print("Fin de la fenetre de gestion des cercles!!");
    dispose();
  }
}
