package org.fudaa.fudaa.sinavi3;

import java.awt.Color;
import java.awt.event.FocusEvent;

import com.memoire.bu.BuCharValidator;
import com.memoire.bu.BuDialogError;
import com.memoire.bu.BuStringValidator;
import com.memoire.bu.BuValueValidator;

public class Sinavi3TextFieldInteger extends Sinavi3TextField{


	public Sinavi3TextFieldInteger() {
		super();
		// TODO Auto-generated constructor stub
	}

	
	public Sinavi3TextFieldInteger(int columns) {
		super(columns);
		// TODO Auto-generated constructor stub
	}

	public Sinavi3TextFieldInteger(String text, int columns) {
		super(text, columns);
		// TODO Auto-generated constructor stub
	}

	public Sinavi3TextFieldInteger(String text) {
		super(text);
		// TODO Auto-generated constructor stub
	}

	/**Methode destinee a etre surchargee par les classes h�ritant de SiporTextField.
	 * Permet de g�rer les contr�les apr�s saisie de l'utilisateur **/
	protected void afterFocus(FocusEvent e){

		String contenu=this.getText();

		if (contenu.equals("")) 
			return;

		//-- Traduction en integer --//

		try {
			int valeur=Integer.parseInt(contenu);
			if(valeur<0)
			{
				new BuDialogError(null, Sinavi3Implementation.isSinavi_,
				"Ce nombre est n�gatif").activate();
				setText("");
				this.requestFocus();
			}

		} catch (NumberFormatException e1) {
			new BuDialogError(null, Sinavi3Implementation.isSinavi_,
			"Ce nombre n'est pas un entier.").activate();
			setText("");
			this.requestFocus();
		}

		Sinavi3TextFieldInteger.this.setForeground(Color.black);
	}
	
	protected int taille(){
		return 4;
	}
protected void specifier_validator(){
	this.setCharValidator(BuCharValidator.INTEGER);
    this.setStringValidator(BuStringValidator.INTEGER);
    this.setValueValidator(BuValueValidator.INTEGER);
		
	}
}
