package org.fudaa.fudaa.sinavi3;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

import org.fudaa.fudaa.ressource.FudaaResource;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuDialogError;
import com.memoire.bu.BuDialogMessage;
import com.memoire.bu.BuDialogWarning;
import com.memoire.fu.FuLog;

/**
 * Panel de saisie des Navire Utilise un systeme d'onglet pour pouvoir saisir la totalit des donnes relative aux navires
 * 
 * @author Adrien Hadoux
 */

public class Sinavi3PanelSaisieBateaux extends JPanel {

	// attributs:

	/**
	 * Donnees de la simulation
	 */

	Sinavi3DataSimulation donnees_;

	/*
	 * ****************************************************************************************** Attributs Panel Gnral
	 * ****************************************************************************************** *
	 */
	/**
	 * Panel des Informations Gnrales
	 */
	JPanel panelDonneesGenerales_ = new JPanel();

	static int nbouverture = 0;

	String[] tabloi_ = { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10" };


	

	Sinavi3TextField dgNom_ = new Sinavi3TextField(10);

	/**
	 * loi de generation des navires
	 */
	

	/**Priorité **/	
	String[] choixPriorite_ = { "normale", "élevée","très élevée" };
	JComboBox dgPriorite_ = new JComboBox(choixPriorite_);

	

	/** dimensions bateau **/
	Sinavi3TextFieldFloat dgLargMax_ = new Sinavi3TextFieldFloat(3);
	Sinavi3TextFieldFloat dgLongMax_ = new Sinavi3TextFieldFloat(3);
	Sinavi3TextFieldFloat dgVitesseAvalant_ = new Sinavi3TextFieldFloat(3);
	Sinavi3TextFieldFloat dgVitesseMontant_ = new Sinavi3TextFieldFloat(3);

	/** tirant eau**/
	Sinavi3TextFieldFloat dgTirantEntree_ = new Sinavi3TextFieldFloat(3);
	Sinavi3TextFieldFloat dgTirantSortie_ = new Sinavi3TextFieldFloat(3);

	final BuButton creneauG_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_continuer"), "");

	


	/**
	 * Bouton de validation de la saisie de la cagorie de navire
	 */

	final BuButton validerNavire_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_oui"), "valider");


	/**
	 * Panel principal de gestion des navires c est a partir de ce panel que l on peut effectuer toutes les operations de
	 * mise a jour d affichage par exemple.
	 */
	Sinavi3VisualiserBateaux MENUNAVIRES_;

	/**
	 * BOOLEEN qui precise si le panel de saisie d'une catÃ©gorie de navire est en mode ajout ou en mode modification si
	 * il est en mode modification: le booleen est a true sinon il est a false par defaut le mode modification est a false
	 * donc en mode saisie classique
	 */
	boolean UPDATE = false;

	/**
	 * indice dans la liste des quais du quai a modifier dans le cas ou l on est en mode modificaiton
	 */
	int NAVIRE_A_MODIFIER_;


	Sinavi3Horaire horaireG_ = new Sinavi3Horaire();


	Color vert=new Color(241,241,241);
	Color rouge=new Color(157,194,143);

	private boolean premierAvertissment=true;

	Sinavi3TextFieldDuree dureeAttenteMaxAdmissible_ = new Sinavi3TextFieldDuree(3);

	/**
	 * Constructeur du Panel de saisie:
	 */
	Sinavi3PanelSaisieBateaux(final Sinavi3DataSimulation d, final Sinavi3VisualiserBateaux vn) {

		// recuperation des donnees de la simulation
		donnees_ = d;

		MENUNAVIRES_ = vn;

		nbouverture++;

		/*
		 * ************************************* Creation des controles des donnes gnrales
		 * **************************************
		 */

		dgNom_.setToolTipText("Veuillez entrer le Nom qui correspond  la categorie de bateau");

		this.dgPriorite_.setToolTipText("Veuillez choisir le degre de priorite de cette catgorie de Bateau");
		
		this.dgLongMax_.setToolTipText("NOMBRE Reel correspondant  la taille de cette catgorie ");


		this.dgLargMax_.setToolTipText("NOMBRE Reel correspondant  la largeur de cette catgorie ");
		this.dgVitesseAvalant_.setToolTipText("Vitesse par défaut de cette categorie dans le sens avalant ");
		this.dgVitesseMontant_.setToolTipText("Vitesse par défaut de cette categorie dans le sens montant ");
		
		this.dgTirantEntree_.setToolTipText("NOMBRE REEL en mtres correspondant au tirant d'eau chargé");


		this.dgTirantSortie_.setToolTipText("NOMBRE REEL en mtres correspondant au tirant d'eau lège");

		/*
		 * ************************************* Creation des controles de donnes des quais preferentiels
		 * **************************************
		 */



		// les contrles des differents boites de saisie

		this.validerNavire_.addActionListener(new ActionListener() {

			public void actionPerformed(final ActionEvent e) {
				// lance la mthode de contrle de creation de Navire
				creationBateau();

			}
		});

		creneauG_.setToolTipText("saisissez les créneaux journaliers en cliquant sur ce bouton");

		this.creneauG_.addActionListener(new ActionListener() {
			public void actionPerformed(final ActionEvent e) {
				//donnees_.application_.addInternalFrame(new Sinavi3FrameSaisieHorairesResume(horaireG_));
				donnees_.application_.addInternalFrame(new Sinavi3FrameSaisieHorairesCompletSemaine(horaireG_,false));
			}
		});


		


		/**
		 * ******************************************************************************************** 
		 *                        ARCHITECTURE DU PANEL DE DONNEES GENERALES
		 * ********************************************************************************************
		 */
		final JPanel total = new JPanel();

		this.setBorder(Sinavi3Bordures.navire);
		total.setLayout(new BorderLayout());

		//panel général
		this.panelDonneesGenerales_.setLayout(new GridLayout(2, 1));

		final JPanel pdg1 = new JPanel(new FlowLayout(FlowLayout.CENTER));
		pdg1.add(new JLabel("Nom de la categorie: "));
		pdg1.add(this.dgNom_);
		pdg1.add(new JLabel("Priorite: "));
		pdg1.add(this.dgPriorite_);

		this.panelDonneesGenerales_.add(pdg1);
		
	    JPanel pdg2 =  new JPanel(new FlowLayout(FlowLayout.CENTER));
	    pdg2.add(new JLabel("durée d’attente maximale admissible (H.Min): "));
	    pdg2.add(this.dureeAttenteMaxAdmissible_);
	    this.panelDonneesGenerales_.add(pdg2);
		
		panelDonneesGenerales_.setBorder(BorderFactory.createTitledBorder(Sinavi3Bordures.compound_, "Général"));
		total.add(this.panelDonneesGenerales_,BorderLayout.NORTH);

		//panel du centre
		Box centre=Box.createVerticalBox();

		total.add(centre,BorderLayout.CENTER);


		//panel dimensions
		JPanel panelDimension=new JPanel(new FlowLayout(FlowLayout.CENTER));
		panelDimension.setBorder(BorderFactory.createTitledBorder(Sinavi3Bordures.compound_, "Caractéristiques"));
		centre.add(panelDimension);

		Box boxLongueurs=Box.createVerticalBox();
		boxLongueurs.setBorder(BorderFactory.createTitledBorder(Sinavi3Bordures.bordnormal_, "Dimensions",0,0,null,Sinavi3Implementation.bleuClairSinavi));
		panelDimension.add(boxLongueurs);


		final JPanel pdg22 = new JPanel();
		pdg22.add(new JLabel("Longueur: "));
		pdg22.add(this.dgLongMax_);
		pdg22.add(new JLabel("METRES	"));
		boxLongueurs.add(pdg22);
		final JPanel pdg21 = new JPanel();
		pdg21.add(new JLabel("Largeur: "));
		pdg21.add(this.dgLargMax_);
		pdg21.add(new JLabel("METRES	"));
		boxLongueurs.add(pdg21);

		Box boxLargeurs=Box.createVerticalBox();
		boxLargeurs.setBorder(BorderFactory.createTitledBorder(Sinavi3Bordures.bordnormal_, "Vitesses par défaut",0,0,null,Sinavi3Implementation.bleuClairSinavi));
		panelDimension.add(boxLargeurs);

		final JPanel pdg31 = new JPanel();
		pdg31.add(new JLabel("Vitesse avalant"));
		pdg31.add(this.dgVitesseAvalant_);
		pdg31.add(new JLabel("KM/H"));
		boxLargeurs.add(pdg31);
		final JPanel pdg33 = new JPanel();
		pdg33.add(new JLabel("Vitesse montant"));
		pdg33.add(this.dgVitesseMontant_);
		pdg33.add(new JLabel("KM/H"));
		boxLargeurs.add(pdg33);

		//Tirant eau 
		JPanel paneltirantTrajet=new JPanel(new GridLayout(1,1));
		centre.add(paneltirantTrajet);
		
		JPanel tirantEau=new JPanel(new FlowLayout(FlowLayout.CENTER));
		tirantEau.setBorder(BorderFactory.createTitledBorder(Sinavi3Bordures.compound_, "Tirant d'eau"));
		paneltirantTrajet.add(tirantEau);

		final JPanel plqa = new JPanel();
		plqa.add(new JLabel("Chargé: "));
		plqa.add(this.dgTirantEntree_);
		plqa.add(new JLabel("METRES"));
		plqa.setBorder(Sinavi3Bordures.bordnormal_);
		tirantEau.add(plqa);

		final JPanel plqb = new JPanel();
		plqb.add(new JLabel("Lege: "));
		plqb.add(this.dgTirantSortie_);
		plqb.add(new JLabel("METRES"));
		plqb.setBorder(Sinavi3Bordures.bordnormal_);
		tirantEau.add(plqb);


		
		



		//-- creation du panel Box qui contient le panel de saisie des creneaux et le panel des lois de generation de navires décris ci dessous --//
		Box creneauLoigener= Box.createHorizontalBox();//new JPanel(new GridLayout(1,2));
		centre.add(creneauLoigener);



		//panel des creneaux horaires
		Box panelCreneaux= Box.createVerticalBox();
		panelCreneaux.setBorder(BorderFactory.createTitledBorder(Sinavi3Bordures.compound_, "Creneaux"));
		creneauLoigener.add(panelCreneaux);
		final JPanel plq771 = new JPanel();
		plq771.add(new JLabel("Creneaux de navigation"));
		plq771.add(this.creneauG_);
		plq771.setBorder(Sinavi3Bordures.bordnormal_);
		panelCreneaux.add(plq771);


		

		JPanel panelValidation=new JPanel(new FlowLayout(FlowLayout.CENTER));
		centre.add(panelValidation);
		panelValidation.setBorder(Sinavi3Bordures.compound_);
		panelValidation.add(this.validerNavire_);






		this.add(total);

		this.initialiser();

		this.dgNom_.requestFocus();
		this.dgNom_.selectAll();
	}



	

	/**
	 * Methode qui controle toutes les donnes saisies pour dfinir une nouvelle catgorie de Navire
	 */

	public boolean controle_creationBateau() {

		if (dgNom_.getText().equals("")) {
			new BuDialogError(donnees_.application_.getApp(), donnees_.application_.isSinavi_, "Nom manquant.")
			.activate();
			return false;
		} else if (this.UPDATE
				&& donnees_.listeBateaux_.existeDoublon(dgNom_.getText(), this.NAVIRE_A_MODIFIER_)) {
			new BuDialogError(donnees_.application_.getApp(), donnees_.application_.isSinavi_, "Nom deja pris.")
			.activate();
			return false;
		} else if (!this.UPDATE && donnees_.listeBateaux_.existeDoublon(dgNom_.getText(), -1)) {
			new BuDialogError(donnees_.application_.getApp(), donnees_.application_.isSinavi_, "Nom deja pris.")
			.activate();
			return false;
		}

		/*if (choixLoiGenerationNav_.getSelectedIndex() == 0) {
			if (this.nbBateauxAttendus_.getText().equals("")) {
				new BuDialogError(donnees_.application_.getApp(), donnees_.application_.isSinavi_,
				"Nombre de bateaux attendus manquant.").activate();
				return false;

			}
		}*/


		if (this.dgLongMax_.getText().equals("")) {
			new BuDialogError(donnees_.application_.getApp(), donnees_.application_.isSinavi_,
			"Longueur manquante.").activate();
			return false;
		}

		if (this.dgLargMax_.getText().equals("")) {
			new BuDialogError(donnees_.application_.getApp(), donnees_.application_.isSinavi_,
			"Largeur manquante.").activate();
			return false;
		}
		if (this.dgVitesseAvalant_.getText().equals("")) {
			new BuDialogError(donnees_.application_.getApp(), donnees_.application_.isSinavi_,
			"Vitesse avalant manquante.").activate();
			return false;
		}
		if (this.dgVitesseMontant_.getText().equals("")) {
			new BuDialogError(donnees_.application_.getApp(), donnees_.application_.isSinavi_,
			"Vitesse montant manquante.").activate();
			return false;
		}

		if (this.dgTirantEntree_.getText().equals("") && this.dgTirantSortie_.getText().equals("")) {
			new BuDialogError(donnees_.application_.getApp(), donnees_.application_.isSinavi_,
			"Au moins un tirant d eau doit etre spécifié ").activate();
			return false;
		}
		
		if (!this.dgTirantEntree_.getText().equals("") && !this.dgTirantSortie_.getText().equals("") 
				&& Double.parseDouble(dgTirantEntree_.getText())<Double.parseDouble(dgTirantSortie_.getText())) {
			new BuDialogError(donnees_.application_.getApp(), donnees_.application_.isSinavi_,
			"Le tirant d'eau lege est plus grand que le tirant d'eau chargé.").activate();
			return false;
		}
		
		
		
		// controle de la saisie des horaires:		
		if(this.horaireG_.lundiCreneau1HeureArrivee == -1 || this.horaireG_.lundiCreneau1HeureDep == -1
	              || this.horaireG_.lundiCreneau2HeureArrivee == -1 || this.horaireG_.lundiCreneau2HeureDep == -1)	{
			new BuDialogError(donnees_.application_.getApp(), donnees_.application_.isSinavi_,
			"Créneaux journaliers non saisi correctement").activate();
			return false;

		}
		// tous les tests ont t epuis: les donnes saisies sont bel et bien correctes!!!
		return true;
	}

	void creationBateau() {
		if (controle_creationBateau()) {
			System.out.print("Yes! donnees robustes et cohrentes!!");
			// creation d'un objet Navire
			Sinavi3Bateau bateau = null;
			
			if (!this.UPDATE) 
				bateau = new Sinavi3Bateau();
			else
				bateau = this.donnees_.getListeBateaux_().retournerNavire(NAVIRE_A_MODIFIER_);
				
			// remplissage des donnes ici
			bateau.nom = this.dgNom_.getText();
			bateau.dureeAttenteMaximaleAdmissible = Double.parseDouble(this.dureeAttenteMaxAdmissible_.getText());
			bateau.priorite = this.dgPriorite_.getSelectedIndex();
			bateau.longueurMax = Float.parseFloat(this.dgLongMax_.getText());
			bateau.largeurMax = Float.parseFloat(this.dgLargMax_.getText());
			bateau.vitesseDefautAvalant = Float.parseFloat(this.dgVitesseAvalant_.getText());
			bateau.vitesseDefautMontant = Float.parseFloat(this.dgVitesseMontant_.getText());
				// recuperation des horires
			bateau.creneauxJournaliers_.recopie(this.horaireG_);
			
			//--creation d'un bateau soit Lege soit chargé --//
			if (!this.dgTirantEntree_.getText().equals("")){
				bateau.tirantEau = Double.parseDouble(this.dgTirantEntree_.getText());
				bateau.typeTirant="C";
				if(!UPDATE)
					bateau.nom=bateau.nom+"C";
			}
			else
				{
				bateau.tirantEau = Double.parseDouble(this.dgTirantSortie_.getText());
				bateau.typeTirant="L";
				if(!UPDATE)
					bateau.nom=bateau.nom+"L";
			}
				

			
			
			
			

			// ajout dans la dataSimulation:

			if (!UPDATE) {
				this.donnees_.listeBateaux_.ajout(bateau);
				new BuDialogMessage(donnees_.application_.getApp(), donnees_.application_.isSinavi_, "categorie de bateaux "
						+ bateau.nom + " correctement ajoutee.").activate();
				

				/**
				 * Creation d'un vecteur ligne supplémentaire regles de navigations pour chaque chenal et cercles:
				 */
				for (int i = 0; i < this.donnees_.listeBief_.listeBiefs_.size(); i++) {
					final Sinavi3Bief chenal = this.donnees_.listeBief_.retournerBief(i);

					chenal.reglesCroisement_.ajoutNavire();
					chenal.reglesTrematage_.ajoutNavire();

				}


				/**
				 * Creation d'un vecteur colonne supplementaire
				 */
				
				this.donnees_.notifyAddBateau(bateau);
				
				
				
				//-- test si les 2 champs tirant d'eau ont été saisi, on créée une deuxième catégorie --//
				if (!(this.dgTirantEntree_.getText().equals("")) && !(this.dgTirantSortie_.getText().equals(""))) {
				
				//--on cree la version lege: --//
					bateau = new Sinavi3Bateau();

					// remplissage des donnes ici
					bateau.nom = this.dgNom_.getText()+"L";
					bateau.dureeAttenteMaximaleAdmissible = Double.parseDouble(this.dureeAttenteMaxAdmissible_.getText());
					bateau.priorite = this.dgPriorite_.getSelectedIndex();
					bateau.longueurMax = Float.parseFloat(this.dgLongMax_.getText());
					bateau.largeurMax = Float.parseFloat(this.dgLargMax_.getText());
					bateau.vitesseDefautAvalant = Float.parseFloat(this.dgVitesseAvalant_.getText());
					bateau.vitesseDefautMontant = Float.parseFloat(this.dgVitesseMontant_.getText());
						// recuperation des horires
					bateau.creneauxJournaliers_.recopie(this.horaireG_);
					
					//--tirant d'eau soit Lege--//
					bateau.tirantEau = Double.parseDouble(this.dgTirantSortie_.getText());
					bateau.typeTirant="L";
					
					//-- on ajoute l'autre version --//
					this.donnees_.listeBateaux_.ajout(bateau);
					new BuDialogMessage(donnees_.application_.getApp(), donnees_.application_.isSinavi_, "categorie de bateaux "
							+ bateau.nom + " correctement ajoutee.").activate();
					

					/**
					 * Creation d'un vecteur ligne supplémentaire regles de navigations pour chaque chenal et cercles:
					 */
					for (int i = 0; i < this.donnees_.listeBief_.listeBiefs_.size(); i++) {
						final Sinavi3Bief chenal = this.donnees_.listeBief_.retournerBief(i);

						chenal.reglesCroisement_.ajoutNavire();
						chenal.reglesTrematage_.ajoutNavire();

					}


					/**
					 * Creation d'un vecteur colonne supplementaire
					 */
					this.donnees_.notifyAddBateau(bateau);
	
				}//-- fin du cas 2 bateaux à ajouter pour le prix d'un --//
	
			} else {
				//-- recuperation de la liste des trajets avant modification--//
				bateau.listeTrajet_=donnees_.listeBateaux_.retournerNavire(NAVIRE_A_MODIFIER_).listeTrajet_;
				
				donnees_.listeBateaux_.modification(NAVIRE_A_MODIFIER_, bateau);
				
				//-- modification des vitesses si il y a lieu --//
				donnees_.reglesVitesseBiefAvalant_.modifierValeurAvalantNavire(NAVIRE_A_MODIFIER_, donnees_.listeBief_.listeBiefs_.size(), donnees_);
		        donnees_.reglesVitesseBiefMontant_.modifierValeurMontantNavire(NAVIRE_A_MODIFIER_, donnees_.listeBief_.listeBiefs_.size(), donnees_);
		        
				
				new BuDialogMessage(donnees_.application_.getApp(), donnees_.application_.isSinavi_, "categorie de bateaux "
						+ bateau.nom + " correctement modifiee.").activate();
			}
			
			
			
			
			
			//--On baisse le niveau de sécurité pour forcer le test de cohérence globale --//
			donnees_.baisserNiveauSecurite();

			// 2)sauvegarde des donnees
			donnees_.enregistrer();

			// ajout d l horaire dans la liste des horaires des navires(horaire qui pourra par la suite etre
			// selectionné dans une jcombo afin d'accélérer la sasiie et la rendre plus ergonomique
			// SiporHoraire copieVraimentEtNonReference=new SiporHoraire();
			// copieVraimentEtNonReference.recopie(horaire_);
			// donnees_.listeHorairesNavires_.add(copieVraimentEtNonReference);

			// initialisation des parametresml.add(ml6);
			this.initialiser();

			// affichage du tableau modifiÃ© magique!!
			this.MENUNAVIRES_.pile_.first(this.MENUNAVIRES_.principalPanel_);

			// REMISE A ZERO DES COMPOSANTS...
			this.initialiser();

		}

	}

	/**
	 * ******************************* METHODE QUI INDIQUE QUE LA FENETRE DE SAISIE D UNE CATEGORIE DE NAVIRE DEVIENT UNE
	 * FENETRE DE MODIFICATION D UNE CATEGORIE
	 * 
	 * @param numNavire indice de la categorie de navire dans la liste des categories a modifier
	 */

	void MODE_MODIFICATION(final int numNavire) {

		initialiser();
		// 1) passage en mode modification
		this.UPDATE = true;

		// 2) recuperation de l indice de la catÃ©gorie de navire
		this.NAVIRE_A_MODIFIER_ = numNavire;

		// 3) recuperation de la structure de navire

		final Sinavi3Bateau nav = this.donnees_.listeBateaux_.retournerNavire(this.NAVIRE_A_MODIFIER_);

		// 4)remplissage des donnnÃ©es relatives au quai:
		this.dgLongMax_.setText("" + (float) nav.longueurMax);
		this.dgVitesseAvalant_.setText("" + (float) nav.vitesseDefautAvalant);
		this.dgLargMax_.setText("" + (float) nav.largeurMax);
		this.dgVitesseMontant_.setText("" + (float) nav.vitesseDefautMontant);
		this.dgNom_.setText(nav.nom);
		this.dureeAttenteMaxAdmissible_.setText("" + (float)nav.dureeAttenteMaximaleAdmissible);
		
		if(nav.typeTirant.equals("C"))
		{
			this.dgTirantEntree_.setText("" + (float) nav.tirantEau);
			this.dgTirantSortie_.setEnabled(false);
		}
			else
			{
		this.dgTirantSortie_.setText("" + (float) nav.tirantEau);
		this.dgTirantEntree_.setEnabled(false);
			}
		
		// ùmodification du navire: changment du nom du bouton
		this.validerNavire_.setText("modifier");
		// index pointant vers la gare de depart
		this.dgPriorite_.setSelectedIndex(nav.priorite);

		

		this.horaireG_.recopie(nav.creneauxJournaliers_);
		
		

		this.dgNom_.requestFocus();
		this.dgNom_.selectAll();

	}

	/**
	 * Methode d'initialisation du contenu des données:
	 */
	void initialiser() {

		this.dgTirantSortie_.setEnabled(true);
		this.dgTirantEntree_.setEnabled(true);
		
		this.premierAvertissment=true;
		this.horaireG_ = new Sinavi3Horaire();
		
		

		this.dgNom_.setText("bateau " + (donnees_.listeBateaux_.listeNavires_.size() + 1));
		this.dureeAttenteMaxAdmissible_.setText("0");
		this.dgLongMax_.setText("");
		this.dgVitesseMontant_.setText("");
		this.dgVitesseAvalant_.setText("");
		this.dgTirantEntree_.setText("");
		this.dgTirantSortie_.setText("");
		this.validerNavire_.setText("valider");
		this.dgLargMax_.setText("");
		this.dgPriorite_.setSelectedIndex(0);

		
		
		

		// rafraichissement du tableau
		this.MENUNAVIRES_.affichagePanel_.maj(donnees_);

		// rÃ©initialisation du mode modification
		this.UPDATE = false;

		this.dgNom_.requestFocus();
		this.dgNom_.selectAll();

	}

}
