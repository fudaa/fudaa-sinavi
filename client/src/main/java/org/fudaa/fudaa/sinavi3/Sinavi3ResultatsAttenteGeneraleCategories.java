/**
 *@creation 14 nov. 06
 *@modification $Dates$
 *@license  GNU General Public Licence 2
 *@copyright   (c)1998-2006 CETMEF 2 bd Gambetta F -60231 Compiegne
 *@mail   devel@fudaa.fr 
 */
package org.fudaa.fudaa.sinavi3;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.ByteArrayInputStream;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.ButtonGroup;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;
import javax.swing.table.TableColumn;

import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.image.CtuluImageExport;
import org.fudaa.ebli.graphe.BGraphe;
import org.fudaa.fudaa.ressource.FudaaResource;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuDialogError;
import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuScrollPane;
import com.memoire.bu.BuTabbedPane;
import com.memoire.bu.BuTable;

/**
 * classe de gestion des resultats de la generation des bateaux propose 2 onglets: le premier propose un affichage
 * 
 * @version $Version$
 * @author Adrien Hadoux
 */
public class Sinavi3ResultatsAttenteGeneraleCategories extends Sinavi3InternalFrame /*implements ActionListener */{

  /**
   * ensemble des donn�es du tableau sous la forme de data
   */
  Object[][] data_;

  /**
   * Graphe associ�e aux r�sultats de la g�n�ration de bateaux
   */
  BGraphe graphe_ = new BGraphe();

  /**
   * panel principal de la fenetre
   */
  BuPanel panelPrincipal_ = new BuPanel();
  
  /**
   * Panel contenant le bouton quitter.
   */
  BuPanel panelQuitter_ = new BuPanel();

  /**
   * Panel de selection des preferences
   */
  BuPanel selectionPanel_ = new BuPanel();
  BuPanel selectionPanel1;
  /** Comboliste de selection de l element de depart */
  String[] listeElt = { "tron�on", "ecluse", "tous" };
  JComboBox ListeTypesDepart_ = new JComboBox(listeElt);
  JComboBox ListeElementDepart_ = new JComboBox();
  /** Comboliste de selection de l element d'arrivee */
  JComboBox ListeTypesArrivee_ = new JComboBox(listeElt);
  JComboBox ListeElementArrivee_ = new JComboBox();
  /** horaire de depart */
  String[] chaine_sens = { "avalant", "montant", "tous"};
  JComboBox Sens_ = new JComboBox(chaine_sens);

  /**
   * Panel des options: type affichages, colonnes � faire figurer:
   */
  BuPanel optionPanel_ = new BuPanel(new BorderLayout());
  
  String[] typeGraphe_ = { "Lignes", "Barres"};
  JComboBox choixTypeGraphe_ = new JComboBox(typeGraphe_);

  JCheckBox choixNbNavires_ = new JCheckBox("Nombre bateaux", true);
  JCheckBox choixTotalAttente_ = new JCheckBox("Attentes totales", true);
  
  JCheckBox choixSecurite_ = new JCheckBox("Attentes de s�curit�", true);
  JCheckBox choixAcces_ = new JCheckBox("Attentes d'acc�s", true);
  JCheckBox choixOccupation_ = new JCheckBox("Attentes d'occupation", true);
  JCheckBox choixPannes_ = new JCheckBox("Attentes d'indisponibilit�", true);
  
  BuPanel optionPanelgraph_ = new BuPanel(new BorderLayout());
  JRadioButton choixTotalAttente2_ = new JRadioButton("Attentes totales", true);
  JRadioButton choixSecurite2_ = new JRadioButton("Attentes de s�curit�", false);
  JRadioButton choixAcces2_ = new JRadioButton("Attentes d'acc�s", false);
  JRadioButton choixOccupation2_ = new JRadioButton("Attentes d'occupation", false);
  JRadioButton choixPannes2_ = new JRadioButton("Attentes d'indisponibilit�", false);

  // checkbox desz differents types d elements:
  JCheckBox choixChenal_ = new JCheckBox("Tron�ons", true);
  JCheckBox choixChenal2_ = new JCheckBox("Tron�ons", true);
 
  JCheckBox choixEcluse_ = new JCheckBox("Ecluses                 ", true);
  JCheckBox choixEcluse2_ = new JCheckBox("Ecluses                 ", true);
  
  Sinavi3TextFieldInteger valSeuilHeure_ = new Sinavi3TextFieldInteger(3);
  Sinavi3TextFieldInteger valSeuilMinute_ = new Sinavi3TextFieldInteger(2);
  JCheckBox valideSeuil_ = new JCheckBox("Seuil:", false);
  int valeurSeuil_;
  boolean seuil_;

  /**
   * Tableau r�capitulatif des r�sultats de la simulation
   */
  BuTable tableau_;

  String[] titreTableau_ = { "El�ment", "Nombre de bateaux", "Att. s�curit�: total", "Att. s�curit�: moy./flotte", "Att. s�curit�: nb. bateaux", "Att. s�curit�: moyenne", "Att. acc�s: total", "Att. acc�s: moy./flotte",
	      "Att. acc�s: nb. bateaux", "Att. acc�s: moyenne", "Att. occup.: total", "Att. occup.: moy./flotte", "Att. occup.: nb. bateaux", "Att. occup.: moyenne",
	      "Att. indisp.: total", "Att. indisp.: moy./flotte", "Att. indisp.: nb. bateaux", "Att. indisp.: moyenne", "Attente totale", "Attente moyenne sur flotte",
	      "Nb. bateaux ayant attendu", "Attente moyenne" };

  /**
   * Panel tabbed qui g�re les 2 onglets, ie les 2 versions d'affichage des r�sultats:
   */
  BuTabbedPane panelPrincipalAffichage_ = new BuTabbedPane();

  /**
   * Panel tabbed qui g�re les 2 onglets de repr�sentations graphiques
   */
  BuPanel panelGraphAffichage_ = new BuPanel(new BorderLayout());
  
  /**
   * Panel cniotenant le tableau et les boutns de controles
   */
  BuPanel panelGestionTableau_ = new BuPanel();

  /**
   * panel de gestion du tableau et des diff�rents boutons
   */
  BuPanel panelTableau_ = new BuPanel();
  
  /**
   * panel d'affichage du tableau
   */
  BuPanel panelTableauAffichage_ = new BuPanel();

  /**
   * panel de gestion des boutons
   */
  BuPanel controlPanel_ = new BuPanel();

  /**
   * Panel de gestion des boutons des courbes
   */
  BuPanel controlPanelCourbes_ = new BuPanel();

  /**
   * panel de gestion des repr�sentations graphiques
   */
  BuPanel panelGraph_ = new BuPanel();
  
  /**
   * panel de gestion des courbes
   */
  BuPanel panelCourbe_ = new BuPanel();

  /**
   * combolist qui permet de selectionenr les lignes deu tableau a etre affich�es:
   */
  JComboBox ListeNavires_ = new JComboBox();

  /**
   * buoton de generation des resultats
   */
  private final BuButton exportationExcel_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_generer"), "Exporter");

  final BuButton exportationgraphe_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_generer"), "Exporter");

  private final BuButton quitter_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_quitter"), "Quitter");
  private final BuButton lancerRecherche_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_oui"), "Rechercher");
  private final BuButton rafraichirSeuil_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_rafraichir"), "");
  private final BuButton validerElem_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_oui"), "Ok");

  Border raisedBevel_ = BorderFactory.createRaisedBevelBorder();
  Border loweredBevel_ = BorderFactory.createLoweredBevelBorder();
  Border compound_ = BorderFactory.createCompoundBorder(raisedBevel_, loweredBevel_);
  Border bordnormal_ = BorderFactory.createEtchedBorder();
  
  /**
   * donnees de la simulation
   */
  Sinavi3DataSimulation donnees_;

  /**
   * constructeur de la sous fenetre de gestion des resultats:
   */
  Sinavi3ResultatsAttenteGeneraleCategories(final Sinavi3DataSimulation _donnees) {
    super("Attentes par cat�gorie de bateaux", true, true, true, true);

    // recuperation des donn�es de la simulation
    donnees_ = _donnees;

    setSize(820, 600);
    setBorder(Sinavi3Bordures.compound_);
    this.getContentPane().setLayout(new GridLayout(1, 1));
    this.panelPrincipal_.setLayout(new BorderLayout());
    this.getContentPane().add(this.panelPrincipal_);
    this.panelPrincipal_.add(this.selectionPanel_, BorderLayout.NORTH);
    this.panelPrincipal_.add(panelQuitter_, BorderLayout.SOUTH);
    panelQuitter_.add(quitter_);

    this.panelPrincipal_.add(this.optionPanel_, BorderLayout.WEST);
    this.panelPrincipal_.add(this.panelPrincipalAffichage_, BorderLayout.CENTER);

    // ajout du tableau dans le panel tabbed
    panelPrincipalAffichage_.addTab("R�sultats d�taill�s", FudaaResource.FUDAA.getIcon("crystal_arbre"), panelTableau_);

    // ajout des courbes dans le panel de la sous fenetre
    panelPrincipalAffichage_.addTab("Exploitation graphique", FudaaResource.FUDAA.getIcon("crystal_graphe"), panelGraph_);

    // remplissage du comboliste avec les noms des navires
    // this.ListeNavires_.addItem("Tous");
    for (int i = 0; i < donnees_.listeBateaux_.listeNavires_.size(); i++) {
      this.ListeNavires_.addItem("" + donnees_.listeBateaux_.retournerNavire(i).nom);
    }

    /*******************************************************************************************************************
     * gestion du panel de selection
     ******************************************************************************************************************/
    this.selectionPanel_.setLayout(new GridLayout(1, 1));

    selectionPanel1 = new BuPanel(new FlowLayout(FlowLayout.LEFT));
    selectionPanel1.add(new JLabel("Attentes � cumuler pour la cat�gorie de bateaux"));
    selectionPanel1.add(ListeNavires_);
    this.selectionPanel_.add(selectionPanel1);

    selectionPanel1.add(new JLabel(" dans le sens"));
    selectionPanel1.add(this.Sens_);
    selectionPanel1.add(lancerRecherche_);

    final TitledBorder bordurea = BorderFactory.createTitledBorder(BorderFactory
            .createEtchedBorder(EtchedBorder.LOWERED), "D�finition de la recherche");
        this.selectionPanel_.setBorder(bordurea);

    // listener des liste box
    final ActionListener RemplissageElement = new ActionListener() {
      public void actionPerformed(ActionEvent e) {

        int selection = 0;
        if (e.getSource() == ListeTypesDepart_) {
          selection = ListeTypesDepart_.getSelectedIndex();
        } else {
          selection = ListeTypesArrivee_.getSelectedIndex();
        }
        // "gare","quai","ecluse","chenal","cercle","bassin"
        switch (selection) {
        case 2:
          if (e.getSource() == ListeTypesDepart_) {
            ListeElementDepart_.removeAllItems();
            ListeElementDepart_.validate();
          }
          break;
        
        case 1:
          if (e.getSource() == ListeTypesDepart_) {
            ListeElementDepart_.removeAllItems();
            for (int i = 0; i < donnees_.listeEcluse_.listeEcluses_.size(); i++) {
              ListeElementDepart_.addItem(donnees_.listeEcluse_.retournerEcluse(i).nom_);
            }
            ListeElementDepart_.validate();
          } else {
            ListeElementArrivee_.removeAllItems();
            for (int i = 0; i < donnees_.listeEcluse_.listeEcluses_.size(); i++) {
              ListeElementArrivee_.addItem(donnees_.listeEcluse_.retournerEcluse(i).nom_);
            }
            ListeElementArrivee_.validate();
          }
          break;
        case 0:
          if (e.getSource() == ListeTypesDepart_) {
            ListeElementDepart_.removeAllItems();
            for (int i = 0; i < donnees_.listeBief_.listeBiefs_.size(); i++) {
              ListeElementDepart_.addItem(donnees_.listeBief_.retournerBief(i).nom_);
            }
            ListeElementDepart_.validate();
          } else {
            ListeElementArrivee_.removeAllItems();
            for (int i = 0; i < donnees_.listeBief_.listeBiefs_.size(); i++) {
              ListeElementArrivee_.addItem(donnees_.listeBief_.retournerBief(i).nom_);
            }
            ListeElementArrivee_.validate();
          }
          break;
        default:
        	break;
        }
      }
    };
    this.ListeElementDepart_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent _e) {
      }
    });
    this.ListeTypesDepart_.addActionListener(RemplissageElement);
    this.ListeTypesArrivee_.addActionListener(RemplissageElement);
    this.ListeTypesDepart_.setSelectedIndex(1);
    this.ListeTypesArrivee_.setSelectedIndex(0);
    this.Sens_.setSelectedIndex(0);

    this.lancerRecherche_.addActionListener(this);
    
    /*******************************************************************************************************************
     * gestion du panel des options
     ******************************************************************************************************************/
    BuGridLayout lo = new BuGridLayout(1,5,0,true,false,false,false);
    final BuPanel panoption = new BuPanel(lo);
    final BuScrollPane panoptionGestionScroll = new BuScrollPane(panoption);
    final Box bVert = Box.createVerticalBox();
    panoption.add(bVert);
    bVert.add(new JLabel(""));
    bVert.setBorder(this.bordnormal_);
    bVert.add(this.choixSecurite_);
    bVert.add(this.choixAcces_);
    bVert.add(this.choixOccupation_);
    bVert.add(this.choixPannes_);
    bVert.add(this.choixTotalAttente_);
    final TitledBorder bordure1 = BorderFactory.createTitledBorder(BorderFactory
        .createEtchedBorder(EtchedBorder.LOWERED), "Types d'attentes");
    bVert.setBorder(bordure1);
    panoption.add(new JLabel(""));
    panoption.createToolTip();
    final Box bVert2 = Box.createVerticalBox();
    panoption.add(bVert2);
    bVert2.setBorder(this.bordnormal_);
    bVert2.add(this.choixChenal_);
  
    bVert2.add(this.choixEcluse_);
    
    final TitledBorder bordure2 = BorderFactory.createTitledBorder(BorderFactory
        .createEtchedBorder(EtchedBorder.LOWERED), "Types d'�l�ments");
    bVert2.setBorder(bordure2);

    this.optionPanel_.add(panoptionGestionScroll);
    
    // listener des checkbox de choix des options d affichage
    this.choixAcces_.addActionListener(this);
    this.choixSecurite_.addActionListener(this);
    this.choixNbNavires_.addActionListener(this);
    this.choixTotalAttente_.addActionListener(this);
    this.choixOccupation_.addActionListener(this);
    this.choixPannes_.addActionListener(this);
    this.choixChenal_.addActionListener(this);
    this.choixEcluse_.addActionListener(this);
    
    /*******************************************************************************************************************
     * gestion du panel des options graphiques
     ******************************************************************************************************************/

    BuGridLayout loGr=new BuGridLayout(1,5,0,true,false,false,false);
    final BuPanel panoptionGraph = new BuPanel(loGr);
    final BuScrollPane panoptionGraphScroll = new BuScrollPane(panoptionGraph);
    this.optionPanelgraph_.add(panoptionGraphScroll);

    ButtonGroup bg=new ButtonGroup();
    bg.add(choixSecurite2_);
    bg.add(choixAcces2_);
    bg.add(choixOccupation2_);
    bg.add(choixPannes2_);
    bg.add(this.choixTotalAttente2_);
    bg.add(choixAcces2_);
    
    final Box bVertgraph = Box.createVerticalBox();
    panoptionGraph.add(bVertgraph);
    bVertgraph.add(new JLabel(""));
    final TitledBorder borduregraph1 = BorderFactory.createTitledBorder(BorderFactory
        .createEtchedBorder(EtchedBorder.LOWERED), "Types d'attentes");
    bVertgraph.setBorder(borduregraph1);
    bVertgraph.add(this.choixSecurite2_);
    bVertgraph.add(this.choixAcces2_);
    bVertgraph.add(this.choixOccupation2_);
    bVertgraph.add(this.choixPannes2_);
    bVertgraph.add(this.choixTotalAttente2_);

    panoptionGraph.add(new JLabel(""));
    final Box bVertgraph2 = Box.createVerticalBox();
    panoptionGraph.add(bVertgraph2);
    bVertgraph2.setBorder(this.bordnormal_);
    bVertgraph2.add(this.choixChenal2_);
    bVertgraph2.add(this.choixEcluse2_);
    
    final TitledBorder borduregraph2 = BorderFactory.createTitledBorder(BorderFactory
            .createEtchedBorder(EtchedBorder.LOWERED), "Types d'�l�ments");
        bVertgraph2.setBorder(borduregraph2);

    // listener des checkbox de choix des options d affichage
    this.choixTypeGraphe_.addActionListener(this);
    this.choixAcces2_.addActionListener(this);
    this.choixSecurite2_.addActionListener(this);
    this.choixTotalAttente2_.addActionListener(this);
    this.choixOccupation2_.addActionListener(this);
    this.choixPannes2_.addActionListener(this);
    this.choixChenal2_.addActionListener(this);
    this.choixEcluse2_.addActionListener(this);
    
    /*******************************************************************************************************************
     * gestion du panel courbes panelTableau_
     ******************************************************************************************************************/
    this.panelTableau_.setLayout(new BorderLayout());
    this.panelTableau_.add(this.optionPanel_, BorderLayout.WEST);
    this.panelTableau_.add(this.panelGestionTableau_, BorderLayout.CENTER);
    
    /*******************************************************************************************************************
     * gestion du panel tableau panelGestionTableau_
     ******************************************************************************************************************/

    // etape 1: architecture du panel panelGestionTableau_ et du panel ascenseur qui le contiendra
    panelGestionTableau_.setLayout(new BorderLayout());
    final JScrollPane asc = new JScrollPane(this.panelTableauAffichage_);

    // ajout au centre du panel qui contiendra le tableau d affichage
    this.panelGestionTableau_.add(asc, BorderLayout.CENTER);

    // panel qui contient les differents boutons
    this.controlPanel_.add(exportationExcel_);
    this.panelGestionTableau_.add(this.controlPanel_, BorderLayout.SOUTH);
    
    final Border bordureTab = BorderFactory.createEtchedBorder();
    controlPanel_.setBorder(bordureTab);

    affichageTableau();

    this.validerElem_.addActionListener(this);

    // bouton qui permet de generer le contenu du tableau en ficheir excel:
    this.exportationExcel_.setToolTipText("Exporte le contenu du tableau au format xls");
    exportationExcel_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent _e) {
    	  Sinavi3GestionResultats.exportTableau(Sinavi3ResultatsAttenteGeneraleCategories.this, titreTableau_, data_);
      }// fin de la methode public actionPerformed
    });

    /*******************************************************************************************************************
     * gestion du panel courbes panelGraph_
     ******************************************************************************************************************/
    this.panelGraph_.setLayout(new BorderLayout());
    this.panelGraph_.add(this.optionPanelgraph_, BorderLayout.WEST);
    this.panelGraph_.add(this.panelGraphAffichage_, BorderLayout.CENTER);
    
    /*******************************************************************************************************************
     * gestion du panel courbes panelCourbe_
     ******************************************************************************************************************/

    final String descriptionGraphe = affichageGraphe();
    this.graphe_.setFluxDonnees(new ByteArrayInputStream(descriptionGraphe.getBytes()));
    this.panelGraphAffichage_.add(this.graphe_, BorderLayout.CENTER);

    // etape 5: bouton de generation du fichier image
    exportationgraphe_.setToolTipText("Exporte le graphe au format image");
    exportationgraphe_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent _e) {
        CtuluImageExport.exportImageFor(donnees_.application_, graphe_);
      }
    });
    
    rafraichirSeuil_.setEnabled(false);
    rafraichirSeuil_.setToolTipText("Actualise la valeur du seuil graphique");

    // etape 6: creation du panel des boutons des courbes
    this.panelGraphAffichage_.add(this.controlPanelCourbes_, BorderLayout.SOUTH);
    this.controlPanelCourbes_.add(exportationgraphe_);
    this.controlPanelCourbes_.add(new JLabel("          "));
    this.controlPanelCourbes_.add(valideSeuil_);
    this.controlPanelCourbes_.add(valSeuilHeure_);
    this.controlPanelCourbes_.add(new JLabel("h"));
    this.controlPanelCourbes_.add(valSeuilMinute_);
    this.controlPanelCourbes_.add(new JLabel("min"));
    this.controlPanelCourbes_.add(rafraichirSeuil_);
    this.controlPanelCourbes_.add(new JLabel("          "));
    this.controlPanelCourbes_.add(choixTypeGraphe_);
    
    choixTypeGraphe_.setToolTipText("Modifie le style de la repr�sentation graphique");
    
    final Border bordure = BorderFactory.createEtchedBorder();
    controlPanelCourbes_.setBorder(bordure);

    valideSeuil_.addActionListener(this);
    rafraichirSeuil_.addActionListener(this);

    /** listener des boutons quitter */
    this.quitter_.setToolTipText("Ferme la sous-fen�tre");
    final ActionListener actionQuitter = new ActionListener() {
      public void actionPerformed(ActionEvent _e) {
        Sinavi3ResultatsAttenteGeneraleCategories.this.windowClosed();
      }
    };
    this.quitter_.addActionListener(actionQuitter);

  }

  /**
   * Methode d'affichage du tableau remarque: cete m�thode sert aussi de rafraichissement du tableau
   * 
   * @param val entier qui indique le num�ro de la cat�gorie de navire � afficher si ce parametre vaut -1 alorso n
   *          affiche la totalit� des navires
   */
  void affichageTableau() {
    // affichage du tableau

    // indice dtu type de l'element choisi
    int type = -1;
   /* if (this.ListeTypesDepart_.getSelectedIndex() == 2) {
      type = -1;
    } else {
      //-- valeur obtient le type d'�l�ments a afficher 0=> chenal 1=>cercle 2=>ecluse 3=>quai
       
      type = this.ListeTypesDepart_.getSelectedIndex();
    }*/

    final int val = this.ListeNavires_.getSelectedIndex();

    /**
     * Recherche des donn�es associ�es � l'�l�ment choisi par l utilisateur: on recherche dasn el tableau qui stocke
     * tous les �l�ments, l indice de l �l�ment dont on veut les r�sultats:
     */

    // operation magique qui permet de determiner ce tableau
    // etape 2: g�n�rer la liste des donn�es � afficher dans le tableau via une matrice de type object
    // ici le nombre de colonnes est de 2 puisqu'il s'agit d'un affichage du nom de la cat�gorie et de son nombre de
    // navires
    data_ = new Object[this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories.length][26];

    int indiceTbaleau = 0;

    if (type < 0) {

      for (int i = 0; i < this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories.length; i++) {
        // if => on verifie que la ligne a afficher concerne un type dont on veut voir l affichage:
        if ((this.choixChenal_.isSelected() && this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[i].typeElement == 0)
            || (this.choixEcluse_.isSelected() && this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[i].typeElement == 1)
            

        ) {
          if (this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[i].typeElement == 0) {
            data_[indiceTbaleau][0] = this.donnees_.listeBief_
                .retournerBief(donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[i].indiceElement).nom_;
          }  else if (this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[i].typeElement == 1) {
            data_[indiceTbaleau][0] = this.donnees_.listeEcluse_
                .retournerEcluse(donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[i].indiceElement).nom_;
          } 

          // ecriture des donn�es calcul�es pour les dur�es de parcours
          // si les cases correspondantes ont �t� coch�es:
          int indiceColonne = 1;
          if (this.choixNbNavires_.isSelected()) {
            data_[indiceTbaleau][indiceColonne++] = ""
                + FonctionsSimu.diviserSimu(this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[i].tableauAttenteCategories[val].nombreNaviresTotal);
          }

          
          if (this.choixSecurite_.isSelected()) {
            if (this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[i].tableauAttenteCategories[val].attenteSecuTotale == 0) {
              indiceColonne += 4;
            } else {
              // attente totale
              data_[indiceTbaleau][indiceColonne++] = ""
                  + Sinavi3TraduitHoraires
                      .traduitMinutesEnHeuresMinutes3(FonctionsSimu.diviserSimu(this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[i].tableauAttenteCategories[val].attenteSecuTotale));
              // moyenne attentes sur la flotte
              data_[indiceTbaleau][indiceColonne++] = ""
                  + Sinavi3TraduitHoraires
                      .traduitMinutesEnHeuresMinutes3((float) (this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[i].tableauAttenteCategories[val].attenteSecuTotale / this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[i].tableauAttenteCategories[val].nombreNaviresTotal));

              // nombre de navires qui attendent
              data_[indiceTbaleau][indiceColonne++] = ""
                  + FonctionsSimu.diviserSimu(this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[i].tableauAttenteCategories[val].nbNaviresAttenteSecu)
                  + " ("
                  + Math.round((float) this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[i].tableauAttenteCategories[val].nbNaviresAttenteSecu
                      / this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[i].tableauAttenteCategories[val].nombreNaviresTotal * 100)
                  + "%)";
              // moyenne attente sur les navires qui attendent
              data_[indiceTbaleau][indiceColonne++] = ""
                  + Sinavi3TraduitHoraires
                      .traduitMinutesEnHeuresMinutes3((float) (this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[i].tableauAttenteCategories[val].attenteSecuTotale / this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[i].tableauAttenteCategories[val].nbNaviresAttenteSecu));

            }
          }

          if (this.choixAcces_.isSelected()) {
            if (this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[i].tableauAttenteCategories[val].attenteAccesTotale == 0) {
              indiceColonne += 4;
            } else {
              // attente totale
              data_[indiceTbaleau][indiceColonne++] = ""
                  + Sinavi3TraduitHoraires
                      .traduitMinutesEnHeuresMinutes3(FonctionsSimu.diviserSimu( this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[i].tableauAttenteCategories[val].attenteAccesTotale));
              // moyenne attentes sur la flotte
              data_[indiceTbaleau][indiceColonne++] = ""
                  + Sinavi3TraduitHoraires
                      .traduitMinutesEnHeuresMinutes3((float) (this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[i].tableauAttenteCategories[val].attenteAccesTotale / this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[i].tableauAttenteCategories[val].nombreNaviresTotal));

              // nombre de navires qui attendent
              data_[indiceTbaleau][indiceColonne++] = ""
                  + FonctionsSimu.diviserSimu(this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[i].tableauAttenteCategories[val].nbNaviresAttenteAcces)
                  + " ("
                  + Math.round((float) this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[i].tableauAttenteCategories[val].nbNaviresAttenteAcces
                      / this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[i].tableauAttenteCategories[val].nombreNaviresTotal * 100)
                  + "%)";
              // moyenne attente sur les navires qui attendent
              data_[indiceTbaleau][indiceColonne++] = ""
                  + Sinavi3TraduitHoraires
                      .traduitMinutesEnHeuresMinutes3((float) (this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[i].tableauAttenteCategories[val].attenteAccesTotale / this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[i].tableauAttenteCategories[val].nbNaviresAttenteAcces));

            }
          }

          if (this.choixOccupation_.isSelected()) {
            if (this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[i].tableauAttenteCategories[val].attenteOccupTotale == 0) {
              indiceColonne += 4;
            } else {
              // attente totale
              data_[indiceTbaleau][indiceColonne++] = ""
                  + Sinavi3TraduitHoraires
                      .traduitMinutesEnHeuresMinutes3(FonctionsSimu.diviserSimu(this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[i].tableauAttenteCategories[val].attenteOccupTotale));
              // moyenne attentes sur la flotte
              data_[indiceTbaleau][indiceColonne++] = ""
                  + Sinavi3TraduitHoraires
                      .traduitMinutesEnHeuresMinutes3((float) (this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[i].tableauAttenteCategories[val].attenteOccupTotale / this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[i].tableauAttenteCategories[val].nombreNaviresTotal));

              // nombre de navires qui attendent
              data_[indiceTbaleau][indiceColonne++] = ""
                  + FonctionsSimu.diviserSimu( this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[i].tableauAttenteCategories[val].nbNaviresAtenteOccup)
                  + " ("
                  + Math.round((float) this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[i].tableauAttenteCategories[val].nbNaviresAtenteOccup
                      / this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[i].tableauAttenteCategories[val].nombreNaviresTotal * 100)
                  + "%)";
              // moyenne attente sur les navires qui attendent
              data_[indiceTbaleau][indiceColonne++] = ""
                  + Sinavi3TraduitHoraires
                      .traduitMinutesEnHeuresMinutes3((float) (this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[i].tableauAttenteCategories[val].attenteOccupTotale / this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[i].tableauAttenteCategories[val].nbNaviresAtenteOccup));

            }
          }

          if (this.choixPannes_.isSelected()) {
            if (this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[i].tableauAttenteCategories[val].attentePanneTotale == 0) {
              indiceColonne += 4;
            } else {
              // attente totale
              data_[indiceTbaleau][indiceColonne++] = ""
                  + Sinavi3TraduitHoraires
                      .traduitMinutesEnHeuresMinutes3(FonctionsSimu.diviserSimu(this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[i].tableauAttenteCategories[val].attentePanneTotale));
              // moyenne attentes sur la flotte
              data_[indiceTbaleau][indiceColonne++] = ""
                  + Sinavi3TraduitHoraires
                      .traduitMinutesEnHeuresMinutes3((float) (this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[i].tableauAttenteCategories[val].attentePanneTotale / this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[i].tableauAttenteCategories[val].nombreNaviresTotal));

              // nombre de navires qui attendent
              data_[indiceTbaleau][indiceColonne++] = ""
                  + FonctionsSimu.diviserSimu( this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[i].tableauAttenteCategories[val].nbNaviresAttentePanne)
                  + " ("
                  + Math.round((float) this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[i].tableauAttenteCategories[val].nbNaviresAttentePanne
                      / this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[i].tableauAttenteCategories[val].nombreNaviresTotal * 100)
                  + "%)";
              // moyenne attente sur les navires qui attendent
              data_[indiceTbaleau][indiceColonne++] = ""
                  + Sinavi3TraduitHoraires
                      .traduitMinutesEnHeuresMinutes3((float) (this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[i].tableauAttenteCategories[val].attentePanneTotale / this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[i].tableauAttenteCategories[val].nbNaviresAttentePanne));

            }
          }

          if (this.choixTotalAttente_.isSelected()) {
            if (this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[i].tableauAttenteCategories[val].attenteMegaTotale == 0) {
              indiceColonne += 4;
            } else {
              // attente totale
              data_[indiceTbaleau][indiceColonne++] = ""
                  + Sinavi3TraduitHoraires
                      .traduitMinutesEnHeuresMinutes3(FonctionsSimu.diviserSimu(this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[i].tableauAttenteCategories[val].attenteMegaTotale));
              // moyenne attentes sur la flotte
              data_[indiceTbaleau][indiceColonne++] = ""
                  + Sinavi3TraduitHoraires
                      .traduitMinutesEnHeuresMinutes3((float) (this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[i].tableauAttenteCategories[val].attenteMegaTotale / this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[i].tableauAttenteCategories[val].nombreNaviresTotal));

              // nombre de navires qui attendent
              data_[indiceTbaleau][indiceColonne++] = ""
                  + FonctionsSimu.diviserSimu(this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[i].tableauAttenteCategories[val].nbNaviresAttenteTotale)
                  + " ("
                  + Math.round((float) this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[i].tableauAttenteCategories[val].nbNaviresAttenteTotale
                      / this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[i].tableauAttenteCategories[val].nombreNaviresTotal * 100)
                  + "%)";
              // moyenne attente sur les navires qui attendent
              data_[indiceTbaleau][indiceColonne++] = ""
                  + Sinavi3TraduitHoraires
                      .traduitMinutesEnHeuresMinutes3((float) (this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[i].tableauAttenteCategories[val].attenteMegaTotale / this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[i].tableauAttenteCategories[val].nbNaviresAttenteTotale));

            }
          }

          indiceTbaleau++;
        }// fin de l affichage de la ligne si le type est bvien selectionn�
      }// fin du pour

    } else if (type != -1) {

      /**
       * determinons l indice de l element dans le tableau d attente: il suffit de parcourir le tableau et de s'arreter
       * des que le type et l indice du tableau d attente concordent avec ceux saisis:
       */
      int ELEMENTCHOISI = -1;

      for (int k = 0; k < this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories.length; k++) {
        if (this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[k].typeElement == this.ListeTypesDepart_
            .getSelectedIndex()
            && this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[k].indiceElement == this.ListeElementDepart_
                .getSelectedIndex()) {
          ELEMENTCHOISI = k;
        }
      }
      if (ELEMENTCHOISI == -1) {
        new BuDialogError(donnees_.application_.getApp(), donnees_.application_.isSinavi_,
            "Erreur!! nous ne trouvons pas l'el�ment selectionn�...").activate();
      }

      // on affiche uniquement la ligne selectionn� par le combolist:
      data_ = new Object[1][this.titreTableau_.length];

      if (this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].typeElement == 0) {
        data_[0][0] = this.donnees_.listeBief_
            .retournerBief(donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].indiceElement).nom_;
      }  else if (this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].typeElement == 1) {
        data_[0][0] = this.donnees_.listeEcluse_
            .retournerEcluse(donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].indiceElement).nom_;
      } 
      // on complete les don�nes par le tableau de resultats:
      // data[0][0]..........
      int indiceColonne = 1;
      if (this.choixNbNavires_.isSelected()) {
        data_[0][indiceColonne++] = ""
            + FonctionsSimu.diviserSimu( this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[val].nombreNaviresTotal);
      }

      
      if (this.choixSecurite_.isSelected()) {
        // attente totale
        data_[0][indiceColonne++] = ""
            + (float)FonctionsSimu.diviserSimu(this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[val].attenteSecuTotale);
        // moyenne attentes sur la flotte
        data_[0][indiceColonne++] = ""
            + (float) (this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[val].attenteSecuTotale / this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[val].nombreNaviresTotal);

        // nombre de navires qui attendent
        data_[0][indiceColonne++] = ""
            + FonctionsSimu.diviserSimu(this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[val].nbNaviresAttenteSecu)
            + " ("
            + Math.round((float) this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[val].nbNaviresAttenteSecu
                / this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[val].nombreNaviresTotal * 100)
            + "%)";
        // moyenne attente sur les navires qui attendent
        data_[0][indiceColonne++] = ""
            + (float) (this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[val].attenteSecuTotale / this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[val].nbNaviresAttenteSecu);

      }

      if (this.choixAcces_.isSelected()) {
        // attente totale
        data_[0][indiceColonne++] = ""
            + (float)FonctionsSimu.diviserSimu( this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[val].attenteAccesTotale);
        // moyenne attentes sur la flotte
        data_[0][indiceColonne++] = ""
            + (float) (this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[val].attenteAccesTotale / this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[val].nombreNaviresTotal);

        // nombre de navires qui attendent
        data_[0][indiceColonne++] = ""
            + FonctionsSimu.diviserSimu(this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[val].nbNaviresAttenteAcces)
            + " ("
            + Math.round((float) this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[val].nbNaviresAttenteAcces
                / this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[val].nombreNaviresTotal * 100)
            + "%)";
        // moyenne attente sur les navires qui attendent
        data_[0][indiceColonne++] = ""
            + (float) (this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[val].attenteAccesTotale / this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[val].nbNaviresAttenteAcces);

      }

      if (this.choixOccupation_.isSelected()) {
        // attente totale
        data_[0][indiceColonne++] = ""
            + (float) FonctionsSimu.diviserSimu(this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[val].attenteOccupTotale);
        // moyenne attentes sur la flotte
        data_[0][indiceColonne++] = ""
            + (float) (this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[val].attenteOccupTotale / this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[val].nombreNaviresTotal);

        // nombre de navires qui attendent
        data_[0][indiceColonne++] = ""
            + FonctionsSimu.diviserSimu(this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[val].nbNaviresAtenteOccup)
            + " ("
            + Math.round((float) this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[val].nbNaviresAtenteOccup
                / this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[val].nombreNaviresTotal * 100)
            + "%)";
        // moyenne attente sur les navires qui attendent
        data_[0][indiceColonne++] = ""
            + (float) (this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[val].attenteOccupTotale / this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[val].nbNaviresAtenteOccup);

      }

      if (this.choixPannes_.isSelected()) {
        // attente totale
        data_[0][indiceColonne++] = ""
            + (float) FonctionsSimu.diviserSimu(this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[val].attentePanneTotale);

        // moyenne attentes sur la flotte
        data_[0][indiceColonne++] = ""
            + (float) (this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[val].attentePanneTotale / this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[val].nombreNaviresTotal);

        // nombre de navires qui attendent
        data_[0][indiceColonne++] = ""
            + FonctionsSimu.diviserSimu(this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[val].nbNaviresAttentePanne)
            + " ("
            + Math.round((float) this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[val].nbNaviresAttentePanne
                / this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[val].nombreNaviresTotal * 100)
            + "%)";
        // moyenne attente sur les navires qui attendent
        data_[0][indiceColonne++] = ""
            + (float) (this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[val].attentePanneTotale / this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[val].nbNaviresAttentePanne);

      }

      if (this.choixTotalAttente_.isSelected()) {
        // attente totale
        data_[0][indiceColonne++] = ""
            + (float) FonctionsSimu.diviserSimu(this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[val].attenteMegaTotale);

        // moyenne attentes sur la flotte
        data_[0][indiceColonne++] = ""
            + (float) (this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[val].attenteMegaTotale / this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[val].nombreNaviresTotal);

        // nombre de navires qui attendent
        data_[0][indiceColonne++] = ""
            + FonctionsSimu.diviserSimu(this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[val].nbNaviresAttenteTotale)
            + " ("
            + Math.round((float) this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[val].nbNaviresAttenteTotale
                / this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[val].nombreNaviresTotal * 100)
            + "%)";
        // moyenne attente sur les navires qui attendent
        data_[0][indiceColonne++] = ""
            + (float) (this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[val].attenteMegaTotale / this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[val].nbNaviresAttenteTotale);

      }

    }
    // etape 3: creation du tableau a partir des donn�es g�n�r�es ci dessus:
    this.tableau_ = new BuTable(data_, this.titreTableau_) {
      public boolean isCellEditable(final int _row, final int _col) {
        return false;
      }
    };

    // etape 3.5: dimensionnement des colonnes du tableau 
    for(int i=0; i<tableau_.getModel().getColumnCount();i++){
        TableColumn column = tableau_.getColumnModel().getColumn(i);
               column.setPreferredWidth(150); 
        }

    
    // etape 4: ajout sdu tableau cr�� dans l'interface
    org.fudaa.fudaa.sinavi.factory.ColumnAutoSizer.sizeColumnsToFit(tableau_);
    tableau_.revalidate();
    this.panelTableauAffichage_.removeAll();
    this.panelTableauAffichage_.setLayout(new BorderLayout());
    this.panelTableauAffichage_.add(tableau_.getTableHeader(), BorderLayout.PAGE_START);
    this.panelTableauAffichage_.add(this.tableau_, BorderLayout.CENTER);

    // mise a jour de l'affichage
    this.revalidate();
    this.updateUI();

  }

  /**
   * Methode qui permet de d�crire le graphe � afficher.
   * 
   * @return chaine: chaine qui contient la des cription de la chaine de caracteres.
   */
  String affichageGraphe() {
	  
	  boolean echelleHeures=false;
	  if (Sinavi3AlgorithmeAttentesGenerales.determineAttenteMax(donnees_) >= 240) echelleHeures=true;
	  
	  boolean histo = false;
	  if (this.choixTypeGraphe_.getSelectedIndex() == 1) histo = true;

    int nbElements = 0;
    if (this.choixChenal2_.isSelected()) {
      nbElements += this.donnees_.listeBief_.listeBiefs_.size();
    }
    
    if (this.choixEcluse2_.isSelected()) {
      nbElements += this.donnees_.listeEcluse_.listeEcluses_.size();
    }
   

    int compteurElem = 0;
    String g = "";

    g += "graphe\n{\n";
    g += "  titre \"Attentes pour la cat�gorie " + (String) this.ListeNavires_.getSelectedItem();
    if (this.Sens_.getSelectedIndex() == 0) {
      g += " dans le sens avalant";
    } else if (this.Sens_.getSelectedIndex() == 1) {
      g += " dans le sens montant";
    } else {
      g += " dans les 2 sens";
    }
    g += " \"\n";

    if (this.choixSecurite2_.isSelected()) {
      g += "  sous-titre \"Attentes de s�curit�\"\n";
    } else if (this.choixAcces2_.isSelected()) {
      g += "  sous-titre \"Attentes d'acc�s\"\n";
    } else if (this.choixOccupation2_.isSelected()) {
      g += "  sous-titre \"Attentes d'occupation\"\n";
    } else if (this.choixPannes2_.isSelected()) {
      g += "  sous-titre \"Attentes d'indisponibilit�\"\n";
    } else if (this.choixTotalAttente2_.isSelected()) {
      g += "  sous-titre \"Attentes totales\"\n";
    }

    g += "  animation non\n";
    g += "  legende " + "oui" + "\n";
    
    g += " marges\n {\n";
    g += " gauche 100\n"; g += " droite 100\n"; g += " haut 50\n"; g += " bas 30\n }\n";

    g += "  axe\n  {\n"; // abscisses
    g += "    titre \"El�ment" + "\"\n";
    g += "    orientation " + "horizontal" + "\n";
    g += "    graduations non\n";
    g += "    minimum " + 0 + "\n";
    g += "    maximum " + (nbElements + 2) + "\n";
    g += "  }\n";

    g += "  axe\n  {\n"; // Ordonn�es
    g += "    titre \"Dur�e" + "\"\n";
    g += "    unite \"";
    if (echelleHeures) g += "heures"; else g += "minutes";
    g += "\"\n";
    g += "    orientation " + "vertical" + "\n";
    g += "    graduations oui\n";
    g += "    minimum " + 0 + "\n";
    g += "    maximum ";
    if (echelleHeures) g+= (Sinavi3AlgorithmeAttentesGenerales.determineAttenteMax(donnees_))/60;
    else g+= (Sinavi3AlgorithmeAttentesGenerales.determineAttenteMax(donnees_));
    /*g+=Sinavi3TraduitHoraires.traduitMinutesEnHeuresMinutes((float) Sinavi3AlgorithmeAttentesGenerales
            .determineAttenteMax(donnees_))*/;
        // ********************ATTTTTTTTTTTTTEEEEEEENNNNNNNNNNTTTTTTTTION A DEFINIR DES QU ON CONNAIS LE TABELAU/ ON
        // DETERMINE LE MAX
    g+= "\n";
    g += "  }\n";
    /*
     * for (int i= 0; i < this.donnees_.categoriesNavires_.listeNavires_.size(); i++) { //A RESERVER POUR GERER
     * PLUSIEURS COURBES
     */

      // ******************************debut histo max************************************************

      g += "  courbe\n  {\n";
      g += "    titre \"";

      if (this.choixSecurite2_.isSelected()) {
        g += "attentes de s�curit� maximales";
      } else if (this.choixAcces2_.isSelected()) {
        g += "attentes d'acc�s maximales";
      } else if (this.choixOccupation2_.isSelected()) {
        g += "attentes d'occupation maximales";
      } else if (this.choixPannes2_.isSelected()) {
        g += "attentes d'indisponibilit� maximales";
      } else if (this.choixTotalAttente2_.isSelected()) {
        g += "attentes totales maximales";
      }
      g += "\"\n";
      g += "    type ";
      if (histo) g += "histogramme"; else g += "courbe";
      g += "\n";
      g += "    aspect\n {\n";
      g += "contour.largeur 1 \n";
      g += "surface.couleur  	BB0000 \n";
      g += "texte.couleur 000000 \n";
      g += "contour.couleur ";
      if (histo) g += "000000"; else g += "BB0000";
      g += " \n";
      g += "    }\n";
      g += "    valeurs\n    {\n";
      /*
       * final Vector coordX= (Vector)_courbes.get(i * 2); final Vector coordY= (Vector)_courbes.get(i * 2 + 1); for
       * (int n= 0;(n < coordX.size()) || (n < coordY.size()); n++) {
       */
      compteurElem = 0;
      for (int n = 0; n < this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories.length; n++) {
        if ((this.choixChenal2_.isSelected() && this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[n].typeElement == 0)
            || (this.choixEcluse2_.isSelected() && this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[n].typeElement == 1)
            

        ) {
          g += (compteurElem + 1) + " ";
          compteurElem++;
          if (this.choixSecurite2_.isSelected()) {
        	  if (echelleHeures) g += ((float) this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[n].tableauAttenteCategories[this.ListeNavires_.getSelectedIndex()].attenteSecuMaxi)/60;
        	  else g += ((float) this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[n].tableauAttenteCategories[this.ListeNavires_.getSelectedIndex()].attenteSecuMaxi);
          } else if (this.choixAcces2_.isSelected()) {
        	  if (echelleHeures) g += ((float) this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[n].tableauAttenteCategories[this.ListeNavires_.getSelectedIndex()].attenteAccesMaxi)/60;
        	  else g += ((float) this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[n].tableauAttenteCategories[this.ListeNavires_.getSelectedIndex()].attenteAccesMaxi);
          } else if (this.choixOccupation2_.isSelected()) {
        	  if (echelleHeures) g += ((float) this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[n].tableauAttenteCategories[this.ListeNavires_.getSelectedIndex()].attenteOccupMaxi)/60;
        	  else g += ((float) this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[n].tableauAttenteCategories[this.ListeNavires_.getSelectedIndex()].attenteOccupMaxi);
          } else if (this.choixPannes2_.isSelected()) {
        	  if (echelleHeures) g += ((float) this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[n].tableauAttenteCategories[this.ListeNavires_.getSelectedIndex()].attentePanneMaxi)/60;
        	  else g += ((float) this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[n].tableauAttenteCategories[this.ListeNavires_.getSelectedIndex()].attentePanneMaxi);
          } else if (this.choixTotalAttente2_.isSelected()) {
        	  if (echelleHeures) g += ((float) this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[n].tableauAttenteCategories[this.ListeNavires_.getSelectedIndex()].attenteTotaleMaxi)/60;
        	  else g += ((float) this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[n].tableauAttenteCategories[this.ListeNavires_.getSelectedIndex()].attenteTotaleMaxi);
          }

          if (this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[n].typeElement == 0) {
            g += "\n etiquette  \n \""
                + this.donnees_.listeBief_
                    .retournerBief(donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[n].indiceElement).nom_;
          } else if (this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[n].typeElement == 1) {
            g += "\n etiquette  \n \""
                + this.donnees_.listeEcluse_
                    .retournerEcluse(donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[n].indiceElement).nom_;
          }

          g += "\" \n"
          // g+= "\n etiquette \n \"" + this.donnees_.categoriesNavires_.retournerNavire(n).nom + "\" \n"/*+*/
              + "\n";

        }// fin du if => si les elements a saisir sont bien selectionn� par l utilisateur
      }// din du pour remplissage des coordonn�es
      g += "    }\n";
      g += "  }\n";

    // //******************************fin histo max************************************************

    // ******************************debut histo moy************************************************
      g += "  courbe\n  {\n";
      g += "    titre \"";

      if (this.choixSecurite2_.isSelected()) {
        g += "attentes de s�curit� moyennes";
      } else if (this.choixAcces2_.isSelected()) {
        g += "attentes d'acc�s moyennes";
      } else if (this.choixOccupation2_.isSelected()) {
        g += "attentes d'occupation moyennes";
      } else if (this.choixPannes2_.isSelected()) {
        g += "attentes d'indisponibilit� moyennes";
      } else if (this.choixTotalAttente2_.isSelected()) {
        g += "attentes totales moyennes";
      }

      /*
       * + "/" + choixString(choix, i, 1) + "/" + choixString(choix, i, 2)
       */
      g += "\"\n";
      g += "    type ";
      if (histo) g += "histogramme"; else g += "courbe";
      g += "\n";
      g += "    aspect\n {\n";
      g += "contour.largeur 1 \n";
      g += "surface.couleur BB8800 \n";
      g += "texte.couleur 000000 \n";
      g += "contour.couleur ";
      if (histo) g += "000000"; else g += "BB8800";
      g += " \n";
      g += "    }\n";
      g += "    valeurs\n    {\n";

      compteurElem = 0;
      for (int n = 0; n < this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories.length; n++) {
        if ((this.choixChenal2_.isSelected() && this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[n].typeElement == 0)
            || (this.choixEcluse2_.isSelected() && this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[n].typeElement == 1)
            

        ) {
          g += (compteurElem + 1)// numero de la cat�gorie
              + " ";
          compteurElem++;
          if (this.choixSecurite2_.isSelected()) {
        	  if (this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[n].tableauAttenteCategories[this.ListeNavires_
                    .getSelectedIndex()].nbNaviresAttenteSecu == 0) g+=0;
        	  else {
        		  if (echelleHeures) g += ((float) (this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[n].tableauAttenteCategories[this.ListeNavires_
                    .getSelectedIndex()].attenteSecuTotale / this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[n].tableauAttenteCategories[this.ListeNavires_
                    .getSelectedIndex()].nbNaviresAttenteSecu))/60;
        		  else g += ((float) (this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[n].tableauAttenteCategories[this.ListeNavires_
        		    .getSelectedIndex()].attenteSecuTotale / this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[n].tableauAttenteCategories[this.ListeNavires_
        		    .getSelectedIndex()].nbNaviresAttenteSecu));
        	  }
          } else if (this.choixAcces2_.isSelected()) {
        	  if (this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[n].tableauAttenteCategories[this.ListeNavires_
                    .getSelectedIndex()].nbNaviresAttenteAcces == 0) g+=0;
        	  else {
        		  if (echelleHeures) g += ((float) (this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[n].tableauAttenteCategories[this.ListeNavires_
                    .getSelectedIndex()].attenteAccesTotale / this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[n].tableauAttenteCategories[this.ListeNavires_
                    .getSelectedIndex()].nbNaviresAttenteAcces))/60;
        		  else g += ((float) (this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[n].tableauAttenteCategories[this.ListeNavires_
        		    .getSelectedIndex()].attenteAccesTotale / this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[n].tableauAttenteCategories[this.ListeNavires_
        		    .getSelectedIndex()].nbNaviresAttenteAcces));
        	  }
          } else if (this.choixOccupation2_.isSelected()) {
        	  if (this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[n].tableauAttenteCategories[this.ListeNavires_
                    .getSelectedIndex()].nbNaviresAtenteOccup ==0) g+=0;
        	  else {
        		  if (echelleHeures) g += ((float) (this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[n].tableauAttenteCategories[this.ListeNavires_
                    .getSelectedIndex()].attenteOccupTotale / this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[n].tableauAttenteCategories[this.ListeNavires_
                    .getSelectedIndex()].nbNaviresAtenteOccup))/60;
        		  else g += ((float) (this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[n].tableauAttenteCategories[this.ListeNavires_
        		    .getSelectedIndex()].attenteOccupTotale / this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[n].tableauAttenteCategories[this.ListeNavires_
        		    .getSelectedIndex()].nbNaviresAtenteOccup));
        	  }
          } else if (this.choixPannes2_.isSelected()) {
        	  if (this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[n].tableauAttenteCategories[this.ListeNavires_
                    .getSelectedIndex()].nbNaviresAttentePanne == 0) g+=0;
        	  else {
        		  if (echelleHeures) g += ((float) (this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[n].tableauAttenteCategories[this.ListeNavires_
                    .getSelectedIndex()].attentePanneTotale / this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[n].tableauAttenteCategories[this.ListeNavires_
                    .getSelectedIndex()].nbNaviresAttentePanne))/60;
        		  else g += ((float) (this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[n].tableauAttenteCategories[this.ListeNavires_
        		    .getSelectedIndex()].attentePanneTotale / this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[n].tableauAttenteCategories[this.ListeNavires_
        		    .getSelectedIndex()].nbNaviresAttentePanne));
        	  }
          } else if (this.choixTotalAttente2_.isSelected()) {
        	  if (this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[n].tableauAttenteCategories[this.ListeNavires_
                    .getSelectedIndex()].nbNaviresAttenteTotale == 0) g+=0;
        	  else {
        		  if (echelleHeures) g += ((float) (this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[n].tableauAttenteCategories[this.ListeNavires_
                    .getSelectedIndex()].attenteMegaTotale / this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[n].tableauAttenteCategories[this.ListeNavires_
                    .getSelectedIndex()].nbNaviresAttenteTotale))/60;
        		  else g += ((float) (this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[n].tableauAttenteCategories[this.ListeNavires_
        		    .getSelectedIndex()].attenteMegaTotale / this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[n].tableauAttenteCategories[this.ListeNavires_
        		    .getSelectedIndex()].nbNaviresAttenteTotale));
        	  }
          }

          g += "\n";
        }
      }// fin du pour remplissage des coordonn�es
      g += "    }\n";
      g += "  }\n";

    // //******************************fin histo moy************************************************

    // ******************************debut histo min************************************************

      g += "  courbe\n  {\n";
      g += "    titre \"";
      if (this.choixSecurite2_.isSelected()) {
        g += "attentes de s�curit� minimales";
      } else if (this.choixAcces2_.isSelected()) {
        g += "attentes d'acc�s minimales";
      } else if (this.choixOccupation2_.isSelected()) {
        g += "attentes d'occupation minimales";
      } else if (this.choixPannes2_.isSelected()) {
        g += "attentes d'indisponibilit� minimales";
      } else if (this.choixTotalAttente2_.isSelected()) {
        g += "attentes totales minimales";
      }

      g += "\"\n";
      g += "    type ";
      if (histo) g += "histogramme"; else g += "courbe";
      g += "\n";
      g += "    aspect\n {\n";
      g += "contour.largeur 0 \n";
      g += "surface.couleur BBCC00 \n";
      g += "texte.couleur 000000 \n";
      g += "contour.couleur ";
      if (histo) g += "000000"; else g += "BBCC00";
      g += " \n";
      g += "    }\n";
      g += "    valeurs\n    {\n";
      /*
       * final Vector coordX= (Vector)_courbes.get(i * 2); final Vector coordY= (Vector)_courbes.get(i * 2 + 1); for
       * (int n= 0;(n < coordX.size()) || (n < coordY.size()); n++) {
       */
      compteurElem = 0;
      for (int n = 0; n < this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories.length; n++) {
        if ((this.choixChenal2_.isSelected() && this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[n].typeElement == 0)
            || (this.choixEcluse2_.isSelected() && this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[n].typeElement == 1)
            

        ) {
          g += (compteurElem + 1)// numero de la cat�gorie
              + " ";
          compteurElem++;

          if (this.choixSecurite2_.isSelected()) {
        	  if (echelleHeures) g += ((float) this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[n].tableauAttenteCategories[this.ListeNavires_.getSelectedIndex()].attenteSecuMini)/60;
        	  else g += ((float) this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[n].tableauAttenteCategories[this.ListeNavires_.getSelectedIndex()].attenteSecuMini);
          } else if (this.choixAcces2_.isSelected()) {
        	  if (echelleHeures) g += ((float) this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[n].tableauAttenteCategories[this.ListeNavires_.getSelectedIndex()].attenteAccesMini)/60;
        	  else g += ((float) this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[n].tableauAttenteCategories[this.ListeNavires_.getSelectedIndex()].attenteAccesMini);
          } else if (this.choixOccupation2_.isSelected()) {
        	  if (echelleHeures) g += ((float) this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[n].tableauAttenteCategories[this.ListeNavires_.getSelectedIndex()].attenteOccupMini)/60;
        	  else g += ((float) this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[n].tableauAttenteCategories[this.ListeNavires_.getSelectedIndex()].attenteOccupMini);
          } else if (this.choixPannes2_.isSelected()) {
        	  if (echelleHeures) g += ((float) this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[n].tableauAttenteCategories[this.ListeNavires_.getSelectedIndex()].attentePanneMini)/60;
        	  else g += ((float) this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[n].tableauAttenteCategories[this.ListeNavires_.getSelectedIndex()].attentePanneMini);
          } else if (this.choixTotalAttente2_.isSelected()) {
        	  if (echelleHeures) g += ((float) this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[n].tableauAttenteCategories[this.ListeNavires_.getSelectedIndex()].attenteTotaleMini)/60;
        	  else g += ((float) this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[n].tableauAttenteCategories[this.ListeNavires_.getSelectedIndex()].attenteTotaleMini);
          }

          g += "\n";

        }
      }// din du pour remplissage des coordonn�es
      g += "    }\n";
      g += "  }\n";

    // //******************************fin histo min************************************************

    if (seuil_) {
      /**
       * declaration d'un seuil
       */
      g += " contrainte\n";
      g += "{\n";
      // a mettre le seuil
      g += "titre \"seuil \"\n";
      // str+="orientation horizontal \n";
      g += " type max\n";
      //g += " valeur " + Sinavi3TraduitHoraires.traduitHeuresMinutesEnHeures(valeurSeuil) + CtuluLibString.LINE_SEP_SIMPLE;// /la valeur ordonn�e du seuil
      if(echelleHeures) g += " valeur " + (valeurSeuil_/60.) + CtuluLibString.LINE_SEP_SIMPLE;// /la valeur ordonn�e du seuil
      else g += " valeur " + valeurSeuil_ + CtuluLibString.LINE_SEP_SIMPLE;

      g += " \n }\n";
      // }//fin du for
    }

    return g;
  }

  /**
   * Listener principal des elements de la frame: tres important pour les composant du panel de choix des �l�ments
   * 
   * @param _ev evenement qui apelle cette fonction
   */

  public void actionPerformed(final ActionEvent _ev) {
    final Object source = _ev.getSource();

    // action commune a tous les �v�nements: redimensionnement de la fenetre
    final Dimension actuelDim = this.getSize();
    final Point pos = this.getLocation();

    if (source == this.choixTotalAttente_ || source == this.choixNbNavires_ || source == this.choixAcces_
        || source == this.choixSecurite_ || source == this.choixOccupation_
        || source == this.choixPannes_)
    {
      // clic sur un checkBox:
      // construction de la colonne des titres

      if (!this.choixSecurite_.isSelected() && !this.choixAcces_.isSelected()
          && !this.choixOccupation_.isSelected() && !this.choixPannes_.isSelected()
          && !this.choixTotalAttente_.isSelected()) {
        this.choixTotalAttente_.setSelected(true);
      }

      int compteurColonnes = 0;
      if (this.choixAcces_.isSelected()) {
        compteurColonnes += 4;
      }
      
      if (this.choixSecurite_.isSelected()) {
        compteurColonnes += 4;
      }
      if (this.choixNbNavires_.isSelected()) {
        compteurColonnes++;
      }
      if (this.choixTotalAttente_.isSelected()) {
        compteurColonnes += 4;
      }
      if (this.choixOccupation_.isSelected()) {
        compteurColonnes += 4;
      }
      if (this.choixPannes_.isSelected()) {
        compteurColonnes += 4;
      }

      this.titreTableau_ = new String[compteurColonnes + 1];
      int indiceColonne = 0;
      this.titreTableau_[indiceColonne++] = "El�ment";
      if (this.choixNbNavires_.isSelected()) {
        this.titreTableau_[indiceColonne++] = "Nombre de bateaux";
      }
      
      if (this.choixSecurite_.isSelected()) {
        this.titreTableau_[indiceColonne++] = "Att. s�curit�: total";
        this.titreTableau_[indiceColonne++] = "Att. s�curit�: moy./flotte";
        this.titreTableau_[indiceColonne++] = "Att. s�curit�: nb. bateaux";
        this.titreTableau_[indiceColonne++] = "Att. s�curit�: moyenne";
      }
      if (this.choixAcces_.isSelected()) {
        this.titreTableau_[indiceColonne++] = "Att. acc�s: total";
        this.titreTableau_[indiceColonne++] = "Att. acc�s: moy./flotte";
        this.titreTableau_[indiceColonne++] = "Att. acc�s: nb. bateaux";
        this.titreTableau_[indiceColonne++] = "Att. acc�s: moyenne";
      }
      if (this.choixOccupation_.isSelected()) {
        this.titreTableau_[indiceColonne++] = "Att. occup.: total";
        this.titreTableau_[indiceColonne++] = "Att. occup.: moy./flotte";
        this.titreTableau_[indiceColonne++] = "Att. occup.: nb. bateaux";
        this.titreTableau_[indiceColonne++] = "Att. occup.: moyenne";
      }
      if (this.choixPannes_.isSelected()) {
        this.titreTableau_[indiceColonne++] = "Att. indisp.: total";
        this.titreTableau_[indiceColonne++] = "Att. indisp.: moy./flotte";
        this.titreTableau_[indiceColonne++] = "Att. indisp.: nb. bateaux";
        this.titreTableau_[indiceColonne++] = "Att. indisp.: moyenne";
      }
      if (this.choixTotalAttente_.isSelected()) {
        this.titreTableau_[indiceColonne++] = "Attente totale";
        this.titreTableau_[indiceColonne++] = "Attente moyenne sur flotte";
        this.titreTableau_[indiceColonne++] = "Nb. bateaux ayant attendu";
        this.titreTableau_[indiceColonne++] = "Attente moyenne";
      }
      // mise a jour via appel a la fonction affichage avec l'onglet selectionn� des cat�gories
      affichageTableau();
    } else if(source == this.choixTotalAttente2_ || source == this.choixAcces2_
        || source == this.choixSecurite2_ || source == this.choixOccupation2_
        || source == this.choixPannes2_) {
      // mise a jour des courbes
      final String descriptionCourbes = this.affichageGraphe();
      this.graphe_.setFluxDonnees(new ByteArrayInputStream(descriptionCourbes.getBytes()));

    } else if (source == this.lancerRecherche_ || source == validerElem_) {
     
      // lancement des calculs pour les dur�es de parcours:
      Sinavi3AlgorithmeAttentesGenerales.calcul(donnees_, this.Sens_.getSelectedIndex());
      // SiporCalculDureesParcours.calcul(donnees_,(String)this.ListeElementDepart_.getSelectedItem(),(String)this.ListeElementArrivee_.getSelectedItem(),Float.parseFloat(this.horaireDeb_.getText()),Float.parseFloat(this.horaireFin_.getText()));
      // mise a jour des affichages:
      affichageTableau(/* this.ListeNavires_.getSelectedIndex()-1 */);
      // mise a jour des courbes
      final String descriptionCourbes = this.affichageGraphe();
      this.graphe_.setFluxDonnees(new ByteArrayInputStream(descriptionCourbes.getBytes()));

    } else if (source == this.valideSeuil_ || source == this.rafraichirSeuil_) {
    	this.rafraichirSeuil_.setEnabled(this.valideSeuil_.isSelected());
    	if (this.valideSeuil_.isSelected() && !this.valSeuilHeure_.getText().equals("") && !this.valSeuilMinute_.getText().equals("")) {
    		// booleen passe a true
    		this.seuil_ = true;
    		// on recupere al valeure du seuil choisie par l utilisateur
    		valeurSeuil_ = 60*((int) Float.parseFloat(this.valSeuilHeure_.getText())) + (int) Float.parseFloat(this.valSeuilMinute_.getText());
    		// on redesssinne l histogramme en tenant compte du seuil de l utilisateur
    		final String descriptionGraphe = this.affichageGraphe();
    		this.graphe_.setFluxDonnees(new ByteArrayInputStream(descriptionGraphe.getBytes()));
    	} else {
    		// booleen passe a false
    		this.seuil_ = false;
    		// on redesssinne le graphe sans seuil
    		final String descriptionGraphe = this.affichageGraphe();
    		this.graphe_.setFluxDonnees(new ByteArrayInputStream(descriptionGraphe.getBytes()));
    	}
    } else if (source == this.choixChenal_ || source == this.choixEcluse_) {
    	// on relance l'affichage qui va se charger d afficher lees don�nes selon leut type.
    	affichageTableau(/* this.ListeNavires_.getSelectedIndex()-1 */);
    }
    else if (source == this.choixChenal2_ || source == this.choixEcluse2_ || source == this.choixTypeGraphe_) {
    	String descriptionGraphe = this.affichageGraphe();
    	this.graphe_.setFluxDonnees(new ByteArrayInputStream(descriptionGraphe.getBytes()));
    }

    // on redimensionne la fenetre comme elle etais avant manipulation des elements graphique
    this.setSize(actuelDim);
    this.setLocation(pos);
  }

  /**
   * Methode qui s active lorsque l'on quitte l'application
   */
  protected void windowClosed() {
    // verif.stop(); //stop n est aps sur on le modifie donc par une autre variable

    // desactivation du thread
    // dureeVieThread=false;

    System.out.print("Fin de la fenetre de gestion des cercles!!");
    dispose();
  }
}
