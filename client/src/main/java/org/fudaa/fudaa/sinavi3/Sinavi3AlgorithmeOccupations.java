/**
 *@creation 18 jan. 10
 *@modification $Dates$
 *@license  GNU General Public Licence 2
 *@copyright   (c)1998-2006 CETMEF 2 bd Gambetta F -60231 Compiegne
 *@mail   devel@fudaa.fr 
 */
package org.fudaa.fudaa.sinavi3;

import org.fudaa.dodico.corba.sinavi3.SParametresResultatOccupationUnitaire;
import org.fudaa.dodico.corba.sinavi3.SParametresResultatsCompletSimulation;
import org.fudaa.dodico.corba.sinavi3.SParametresResultatsOccupations;

/**
 * @version $Version$
 * @author Mederic FARGEIX
 */
public class Sinavi3AlgorithmeOccupations {

	public static void calculApresSimu(final Sinavi3DataSimulation _donnees) {
		
		final SParametresResultatsCompletSimulation resultats = _donnees.params_.ResultatsCompletsSimulation;
		
		final int nombreGares = _donnees.listeGare_.listeGares_.size();
		final int nombreEcluses = _donnees.listeEcluse_.listeEcluses_.size();
		int element;
		
		resultats.Occupations = new SParametresResultatsOccupations[ nombreGares + nombreEcluses ];
		// Occupations sera organis� ainsi : gare0, gare1, gare2, ... gare nombreGares-1, ecluse 0, ecluse 1, ... ecluse nombreEcluses-1
		
		int[] nombreEnregistrementsTraites = new int[ nombreGares + nombreEcluses ];
		int[] nombreEnregistrementsTotal = new int [nombreGares + nombreEcluses];
		
		// On compte le nombre d'enregistrement pour chaque element afin de dimensionner chaque tableau de r�sultat
		for (int i = 0; i < _donnees.listeResultatsOccupationsSimu_.listeOccupations.length; i++) {
			element = numElementTableau( _donnees.listeResultatsOccupationsSimu_.listeOccupations[i].type, _donnees.listeResultatsOccupationsSimu_.listeOccupations[i].numero, nombreGares );
			nombreEnregistrementsTotal[element] ++;
		}
		
		// Allocation m�moire
		for (int j = 0; j < nombreGares + nombreEcluses; j++) {
			
			resultats.Occupations[j] = new SParametresResultatsOccupations();
			
			if (j < nombreGares) {
				resultats.Occupations[j].typeElement = 0;
				resultats.Occupations[j].indiceElement = j;
			} else {
				resultats.Occupations[j].typeElement = 1;
				resultats.Occupations[j].indiceElement = j - nombreGares;
			}
			
			resultats.Occupations[j].resultatsOccupationsUnitaires = new SParametresResultatOccupationUnitaire[ nombreEnregistrementsTotal[j] ];
			
			for (int k = 0; k < nombreEnregistrementsTotal[j]; k++) {
				resultats.Occupations[j].resultatsOccupationsUnitaires[k] = new SParametresResultatOccupationUnitaire();
				resultats.Occupations[j].resultatsOccupationsUnitaires[k].nombreBateauxCategorie = new short[_donnees.params_.navires.nombreNavires];
			}
			
		}
		
		for (int i = 0; i < _donnees.listeResultatsOccupationsSimu_.listeOccupations.length; i++) {
			
			element = numElementTableau( _donnees.listeResultatsOccupationsSimu_.listeOccupations[i].type, _donnees.listeResultatsOccupationsSimu_.listeOccupations[i].numero, nombreGares);
			resultats.Occupations[element].resultatsOccupationsUnitaires[ nombreEnregistrementsTraites[element] ].heure = _donnees.listeResultatsOccupationsSimu_.listeOccupations[i].heure;
			resultats.Occupations[element].resultatsOccupationsUnitaires[ nombreEnregistrementsTraites[element] ].sens = _donnees.listeResultatsOccupationsSimu_.listeOccupations[i].sens;
			resultats.Occupations[element].resultatsOccupationsUnitaires[ nombreEnregistrementsTraites[element] ].ecluseConcernee = _donnees.listeResultatsOccupationsSimu_.listeOccupations[i].ecluseConcernee;
			resultats.Occupations[element].resultatsOccupationsUnitaires[ nombreEnregistrementsTraites[element] ].refBassinee = _donnees.listeResultatsOccupationsSimu_.listeOccupations[i].refBassinee;
			
			resultats.Occupations[element].resultatsOccupationsUnitaires[ nombreEnregistrementsTraites[element] ].nombreBateauxCategorie = 
					new short[_donnees.listeResultatsOccupationsSimu_.listeOccupations[i].nombreBateauxCategorie.length];
			
			for (int j = 0; j < _donnees.listeResultatsOccupationsSimu_.listeOccupations[i].nombreBateauxCategorie.length; j++) {
				resultats.Occupations[element].resultatsOccupationsUnitaires[ nombreEnregistrementsTraites[element] ].nombreBateauxCategorie[j] = 
						_donnees.listeResultatsOccupationsSimu_.listeOccupations[i].nombreBateauxCategorie[j];
			}
			
			nombreEnregistrementsTraites[element] ++;
			
		}
		
	}
	
	public static int numElementTableau(int _typeElement, int _numElement, int _nombreGares) { // Donne la correspondance entre G0,G1,G2...E0,E1,E2... et 1,2,3...n,n+1,n+2...
		
		if (_typeElement == 0) return _numElement; // cas gare
		else return _nombreGares + _numElement; //cas ecluse
			
	}
	
}
