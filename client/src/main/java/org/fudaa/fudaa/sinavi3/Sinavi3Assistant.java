/*
 * @file         SiporAssistant.java
 * @creation     1999-11-03
 * @modification $Date: 2007-11-23 11:27:49 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.sinavi3;

import com.memoire.bu.BuAssistant;

/**
 * L'assistant du client Sipor. Rien de plus que BuAssistant.
 * 
 * @version $Revision: 1.1 $ $Date: 2007-11-23 11:27:49 $ by $Author: hadouxad $
 * @author Nicolas Chevalier
 */
public class Sinavi3Assistant extends BuAssistant {}
