/*
 * @file         MethodesUtiles.java
 * @creation     1999-10-01
 * @modification $Date: 2007-11-23 11:28:09 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.sinavi3;

import java.awt.Component;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.swing.JFileChooser;
import javax.swing.filechooser.FileFilter;

import com.memoire.bu.BuBrowserFrame;
import com.memoire.bu.BuCommonInterface;
import com.memoire.bu.BuMainPanel;
import com.memoire.bu.BuTaskOperation;
import com.memoire.fu.FuLib;

/**
 * Un ensemble de m�thodes utiles.
 * 
 * @version $Revision: 1.1 $ $Date: 2007-11-23 11:28:09 $ by $Author: hadouxad $
 * @author Nicolas Chevalier
 */
public final class MethodesUtiles {

  private MethodesUtiles() {}

  /** Idem enregistrer ( final String nomFichier, final byte[] contenu, final BuCommonInterface app). */
  public static void enregistrer(final String nomFichier, final String contenu, final BuCommonInterface app) {
    enregistrer(nomFichier, contenu.getBytes(), app);
  }

  /**
   * Permet d'�crire un fichier sur disque. Cela cr�� une nouvelle tache dans "app" avec un indicateur de progression de
   * l'enregistrement.
   * 
   * @param nomFichier nom du fichier � �crire.
   * @param contenu
   * @param app application dans laquelle la tache sera cr��e.
   */
  public static void enregistrer(final String nomFichier, final byte[] contenu, final BuCommonInterface app) {
    new BuTaskOperation(app, "Enregistrer") {
      public void act() {
        enregistrerProgressivement(nomFichier, contenu, app);
      }
    }.start();
  }

  /**
   * Pour enregistrer le code html de browser dans un fichier. Cela cr�� une nouvelle tache dans app avec indication de
   * la progression.
   * 
   * @param ext extensions accept�es.
   * @param browser fenetre dont le code doit etre enrgistr�.
   * @param app application dans laquelle la tache sera cr��e.
   */
  public static void enregistrerSous(final String[] ext, final BuBrowserFrame browser, final BuCommonInterface app) {
    final String nomFichier = MethodesUtiles.choisirFichierEnregistrement(ext, browser);
    if (nomFichier != null) {
      MethodesUtiles.enregistrer(nomFichier, browser.getHtmlSource(), app);
      browser.putClientProperty("NomFichier", nomFichier);
    }
  }

  /** Idem enregistrerProgressivement ( String nomFichier, byte[] contenu, BuCommonInterface app ). */
  public static void enregistrerProgressivement(final String nomFichier, final String contenu,
      final BuCommonInterface app) {
    enregistrerProgressivement(nomFichier, contenu.getBytes(), app);
  }

  /**
   * Permet d'�crire un fichier sur disque. Il y a indication de la progression dans "app"
   * 
   * @param nomFichier nom du fichier � �crire.
   * @param contenu
   * @param app application dans laquelle sera indiqu�e la progression.
   */
  public static void enregistrerProgressivement(final String nomFichier, final byte[] contenu,
      final BuCommonInterface app) {
    final int longueur = contenu.length; // Longueur de la chaine
    final int nbCar = longueur / 100;
    // Nombre de caract�res � �crire � chacune des 10 op�rations
    int index = 0; // index de progression dans la tableau
    if (app == null) {
      throw (new IllegalArgumentException("app ne peut pas etre null"));
    }
    final BuMainPanel mp = app.getMainPanel();
    //Fred si exception le flux ne sera jamais ferme
    BufferedOutputStream buffer =null;
    try {
      //final FileOutputStream fichier = ;
      buffer = new BufferedOutputStream(new FileOutputStream(nomFichier, false), longueur < 1024 ? longueur : 1024);
      for (int i = 0; i < 100; i++) {
        buffer.write(contenu, index, nbCar);
        index += nbCar;
        mp.setProgression(i);
      }
      buffer.write(contenu, index, longueur - index);
      mp.setProgression(100);
      mp.setProgression(0);
      //dans le finally
      //buffer.close();
      //ichier.close();
    } catch (final IOException ex) {
      System.err.println("enregistrerProgressivement : ");
      ex.printStackTrace();
    }
    finally{
      FuLib.safeClose(buffer);
    }
  }

  /**
   * Affiche une boite de dialogue pour choisir un nom de fichier pour un enregistrement.
   * 
   * @return nom du fichier choisi.
   * @param extensionAcceptee extension possible.
   * @param parent propri�taire de la boite de dialogue affich�e.
   */
  public static String choisirFichierEnregistrement(final String extensionAcceptee, final Component parent) {
    final String[] tab = { extensionAcceptee };
    return choisirFichierEnregistrement(tab, parent);
  }

  /**
   * Affiche une boite de dialogue pour choisir un nom de fichier pour un enregistrement.
   * 
   * @return nom du fichier choisi.
   * @param extensionsAcceptees tableau des extensions possibles.
   * @param parent propri�taire de la boite de dialogue affich�e.
   */
  public static String choisirFichierEnregistrement(final String[] extensionsAcceptees, final Component parent) {
    return choisirFichierEnregistrement(extensionsAcceptees, parent, null);
  }

  /**
   * Affiche une boite de dialogue pour choisir un nom de fichier pour un enregistrement.
   * 
   * @return nom du fichier choisi.
   * @param extensionsAcceptees tableau des extensions possibles.
   * @param parent propri�taire de la boite de dialogue affich�e.
   * @param repertoire repertoire par edfaut de la boite de dialogue.
   */
  public static String choisirFichierEnregistrement(final String[] extensionsAcceptees, final Component parent,
      final String repertoire) {
    return choisirFichier(extensionsAcceptees, parent, repertoire, 0);
  }

  /**
   * Affiche une boite de dialogue pour choisir un nom de fichier.
   * 
   * @return nom du fichier choisi.
   * @param mode 0 correspond � un enregistrement et 1 � une ouverture.
   * @param repertoire nom du r�pertoire par d�faut de la boite de dialogue.
   * @param parent propri�taire de la boite de dialogue.
   * @param extensionsAcceptees tableau des extensions qui sont accept�es.
   */
  public static String choisirFichier(final String[] extensionsAcceptees, final Component parent,
      final String repertoire, final int mode) {
    final JFileChooser chooser = (repertoire != null ? new JFileChooser(repertoire) : new JFileChooser());
    final FileFilter filtre = new FileFilter() {
      public String getDescription() {
        String texte = "";
        for (int i = 0; i < extensionsAcceptees.length; i++) {
          texte += " *." + extensionsAcceptees[i];
        }
        return texte;
      }

      public boolean accept(File f) {
        boolean retour = f.isDirectory();
        String nom = f.getName().toLowerCase();
        for (int i = 0; i < extensionsAcceptees.length; i++) {
          if (nom.endsWith(extensionsAcceptees[i])) {
            retour = true;
          }
        }
        return retour;
      }
    };
    chooser.setFileFilter(filtre);
    int returnVal;
    if (mode == 0) {
      returnVal = chooser.showSaveDialog(parent);
    } else if (mode == 1) {
      returnVal = chooser.showOpenDialog(parent);
    } else {
      return null;
    }
    if (returnVal == JFileChooser.APPROVE_OPTION) {
      return chooser.getSelectedFile().getAbsolutePath();
    }
    return null;
  }
}
