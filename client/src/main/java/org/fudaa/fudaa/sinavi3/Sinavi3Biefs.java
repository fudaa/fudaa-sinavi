package org.fudaa.fudaa.sinavi3;

import java.util.ArrayList;

/**
 * Classe de gestion des cheneaux permet de stocker les donn�es relatives au bassin.
 * 
 * @author Adrien Hadoux
 */
public class Sinavi3Biefs {

  /**
   * Liste de string contenant les noms des bassins.
   */
  ArrayList listeBiefs_ = new ArrayList();

  /**
   * Methode d'ajout d'un chenal.
   * 
   * @param _c chenal � ajouter
   */

  public  void ajout(final Sinavi3Bief _c) {

    listeBiefs_.add(_c);
    //-- Notification aux vues pour se mettre � jour --//
    Sinavi3DataSimulation.setProperty("chenal");
  }

  /**
   * Methode qui retourne le i eme chenal.
   * 
   * @param _i indice du cercle du tableau de cheneaux a retourner
   * @return un objet de type string qui pourra etre modifi et renvoy
   */

  public Sinavi3Bief retournerBief(final int _i) {
    if (_i < this.listeBiefs_.size()) {
      return (Sinavi3Bief) this.listeBiefs_.get(_i);
    }
    return null;
  }

  /**
   * Methode de suppression du n ieme element de la liste de cheneaux.
   * 
   * @param _n
   */
  public void suppression(final int _n) {

    listeBiefs_.remove(_n);
    //-- Notification aux vues pour se mettre � jour --//
    Sinavi3DataSimulation.setProperty("chenal");
  }

  /**
   * Methode de modification du n ieme chenal par le chenal entr� en parametre d entr�.
   * 
   * @param _n indice du chenal a modifier
   * @param _c chenal a remplacer
   */
  public void modification(final int _n, final Sinavi3Bief _c) {
    this.listeBiefs_.set(_n, _c);
    //-- Notification aux vues pour se mettre � jour --//
    Sinavi3DataSimulation.setProperty("chenal");
  }

  /**
   * methode qui permet de determiner si le nom d'un bassin est unique ou s'il appartient deja a la liste des bassins.
   */
  public  boolean existeDoublon(final String _nomComposant, final int _k) {
    for (int i = 0; i < this.listeBiefs_.size(); i++) {
      if (i != _k) {
        if (this.retournerBief(i).nom_.equals(_nomComposant)) {
          return true;
        }
      }

    }

    // arriv� a ce stade; on a pas touv� de doublons, le nom n'existe donc pas!
    return false;

  }

  public int retourneIndice(final String _nomChenal) {
    for (int i = 0; i < this.listeBiefs_.size(); i++) {
      if (this.retournerBief(i).nom_.equals(_nomChenal)) {
        return i;
      }
    }

    // arriv� a ce stade on a rien retrouv�
    return -1;
  }

public ArrayList getListeBiefs_() {
	return listeBiefs_;
}

public void setListeBiefs_(ArrayList listeBiefs_) {
	this.listeBiefs_ = listeBiefs_;
}

}
