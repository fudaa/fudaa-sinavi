package org.fudaa.fudaa.sinavi3.ui.panel;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;
import java.util.Enumeration;
import java.util.EventObject;
import java.util.List;

import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JTree;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableModel;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellEditor;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;

import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.table.CtuluTableExcelWriter;
import org.fudaa.fudaa.ressource.FudaaResource;
import org.fudaa.fudaa.sinavi.factory.ColumnAutoSizer;
import org.fudaa.fudaa.sinavi3.Sinavi3Bateau;
import org.fudaa.fudaa.sinavi3.Sinavi3Bordures;
import org.fudaa.fudaa.sinavi3.Sinavi3CellEditorDureeParcours;
import org.fudaa.fudaa.sinavi3.Sinavi3DataSimulation;
import org.fudaa.fudaa.sinavi3.Sinavi3Ecluse;
import org.fudaa.fudaa.sinavi3.Sinavi3Implementation;
import org.fudaa.fudaa.sinavi3.Sinavi3InternalFrame;
import org.fudaa.fudaa.sinavi3.Sinavi3ModeleExcel;
import org.fudaa.fudaa.sinavi3.Sinavi3PanelDureeManeuvresEcluses;
import org.fudaa.fudaa.sinavi3.Sinavi3TextField;
import org.fudaa.fudaa.sinavi3.Sinavi3TextFieldFloat;
import org.fudaa.fudaa.sinavi3.structures.DureeManeuvreEcluse.DureeParcours;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuDialogError;
import com.memoire.bu.BuDialogMessage;
import com.memoire.bu.BuTable;
import com.memoire.fu.FuLog;


/**
 * Panel d'affichage du tree des dur�es de maneuvres.
 * @author Adrien
 *
 */
public class SinaviPanelDureeManeuvresEcluses extends Sinavi3InternalFrame{


	BuTable tableau =new BuTable();
	JComboBox<String> dataToModify = new JComboBox<String>(new String[]{"Maneuvre entrant","Seconde maneuvre entrant","Maneuvre sortant","Seconde maneuvre sortant"} );
	 private final BuButton impression_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_generer"), "Excel");
	 

	
	final Sinavi3DataSimulation data ;
	public SinaviPanelDureeManeuvresEcluses(final Sinavi3DataSimulation data) {
		this.data = data;
		setTitle("Duree de maneuvre des bateaux dans les ecluses en entrant (en Min.sec) ");
		setSize(600, 500);
	    setBorder(Sinavi3Bordures.compound_);
	    getContentPane().setLayout(new BorderLayout());
	    
		buildTable(data);
		
		//-- add button --//
		
		JPanel buttonPanel = new JPanel();
		
		final BuButton valider = new BuButton("Valider",FudaaResource.FUDAA.getIcon("crystal_oui"));
		
		buttonPanel.add(dataToModify);
		
		
		buttonPanel.add(valider);
		this.getContentPane().add(buttonPanel,BorderLayout.SOUTH);
		valider.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				new BuDialogMessage(data.getApplication().getApp(), Sinavi3Implementation.isSinavi_,
			              "Les donn�es ont �t� correctement saisies.").activate();
				dispose();
				
			}
		});

		
		this.impression_.addActionListener(new ActionListener() {
		      public void actionPerformed(final ActionEvent e) {
		      exportTable();
		      }

		    });
		
		dataToModify.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				tableau.repaint();
				
			}
		});
		
	}
	
	

	public void exportTable() {
		  this.tableau.editCellAt(0, 0);
	        // generation sous forme d'un fichier excel:
	        File fichier;
	        // definition d un file chooser
	        final JFileChooser fc = new JFileChooser();
	        final int returnVal = fc.showOpenDialog(SinaviPanelDureeManeuvresEcluses.this);

	        if (returnVal == JFileChooser.APPROVE_OPTION) {
	          fichier = fc.getSelectedFile();
	          final File f = CtuluLibFile.appendExtensionIfNeeded(fichier, "xls");

	          // on r�cupere l abstrct model du tableau contenant les donn�es

	          /**
	           * creation d un abstract model impl�mentant l'interface CtuluTableModelInterface
	           */
	         
	          TableModelDureeManeuvre model = (TableModelDureeManeuvre) tableau.getModel();
	          
	          /**
	           * on essaie d 'ecrire en format excel
	           */
	          final CtuluTableExcelWriter ecrivain = new CtuluTableExcelWriter(model, f);

	          try {
	            ecrivain.write(null);
	            new BuDialogMessage(data.getApplication().getApp(), data.getApplication().isSinavi_, 
	            		"Fichier Excel g�n�r� avec succ�s.").activate();

	          } catch (final RowsExceededException _err) {
	            FuLog.error(_err);
	          } catch (final WriteException _err) {
	            FuLog.error(_err);
	          } catch (final IOException _err) {
	            FuLog.error(_err);
	          }

	        }// fin du if si le composant est bon
	}
	
	
	public void buildTable(final Sinavi3DataSimulation data) {
		
		TableModelDureeManeuvre modelTable = new TableModelDureeManeuvre(data,dataToModify);
		
		tableau =new BuTable(modelTable);
		tableau.setDefaultEditor(Object.class,new Sinavi3CellEditorDureeParcours());
	/*
		tableau.setDefaultRenderer(Object.class, new DefaultTableCellRenderer() {
			
			public Component getTableCellRendererComponent(JTable table, Object value,
					boolean isSelected, boolean hasFocus, int row, int column) {
				
				String val = null ;
				if(column != 0 && value != null && value instanceof String) {
					val = value.toString();
					val = val.replace(".",":").replace(",",":");
				}
				return  super.getTableCellRendererComponent(table, val!=null?val:value, isSelected, hasFocus, row, column);
				
			}
		});
		*/
		getContentPane().removeAll();
		getContentPane().add(new JScrollPane(tableau), BorderLayout.CENTER);
		getContentPane().add(tableau.getTableHeader(), BorderLayout.NORTH);
		tableau.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		ColumnAutoSizer.sizeColumnsToFit(tableau);
		getContentPane().revalidate();
		this.revalidate();
	}
	
	
	
	
	
	
	
	

}
