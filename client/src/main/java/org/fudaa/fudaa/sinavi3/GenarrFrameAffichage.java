package org.fudaa.fudaa.sinavi3;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

import javax.swing.DefaultCellEditor;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableColumn;

import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.table.CtuluTableExcelWriter;
import org.fudaa.fudaa.ressource.FudaaResource;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuDialogMessage;
import com.memoire.bu.BuLabel;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuTable;
import com.memoire.fu.FuLog;

public class GenarrFrameAffichage extends Sinavi3InternalFrame{


	Sinavi3DataSimulation donnees_;

	BuTable tableau;


	GenarrModeleTable modeleGenarr_;

	JScrollPane panneauDeroulant_=null;
	boolean typeTrie=true;

	BuPanel commandePanel_=new BuPanel(new FlowLayout(FlowLayout.CENTER));
	private final BuButton sauver_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_enregistrer"), "Enregistrer");
	private final BuButton trier_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_ranger"), "Trier");
	private final BuButton impression_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_generer"), "Exporter");
	private final BuButton valider_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_quitter"), "Quitter");


	public GenarrFrameAffichage(Sinavi3DataSimulation _d){
		super("", true, true, true, true);
		donnees_=_d;


		//-- creation du modele du jtable --//
		modeleGenarr_=new GenarrModeleTable(donnees_);

		setTitle("Liste des bateaux g�n�r�s");
		setSize(700, 500);

		//-- mise a jour du tableau --//
		maj();



		//-- Panel des commandes --//
		//BuPanel c1=new BuPanel(new FlowLayout(FlowLayout.CENTER));
		//BuPanel c2=new BuPanel(new FlowLayout(FlowLayout.CENTER));
		//c1.add(new BuLabel("Cliquez pour trier le tableau:"));
		commandePanel_.add(valider_);
		commandePanel_.add(trier_);
		commandePanel_.add(impression_);
		//c2.add(new BuLabel("Cliquez pour enregistrer les modifications:"));
		commandePanel_.add(sauver_);
		
		//commandePanel_.add(c2);
		//commandePanel_.add(c1);
		
		
		trier_.addActionListener(new ActionListener(){

			public void actionPerformed(ActionEvent e) {

				//-- trier les donn�es du tableau --//
				if(typeTrie){
					donnees_.genarr_.trier();
					typeTrie=false;
				}
				else
				{
					typeTrie=true;
					donnees_.genarr_.trierNavires();
				}
				//-- mise a jour du contenu du tableau --//
				//maj();
				modeleGenarr_.miseAjour();
			}

		});
		
		sauver_.addActionListener(new ActionListener() {
			public void actionPerformed(final ActionEvent e) {
			
		donnees_.genarr_.ecritureFichierGenarr(donnees_.projet_.getFichier());
			}
		});
		valider_.addActionListener(new ActionListener() {
			public void actionPerformed(final ActionEvent e) {
				dispose();
			}
		});
		this.impression_.addActionListener(new ActionListener() {
			public void actionPerformed(final ActionEvent e) {
				tableau.editCellAt(0, 0);
				// generation sous forme d'un fichier excel:
				File fichier;
				// definition d un file chooser
				final JFileChooser fc = new JFileChooser();
				final int returnVal = fc.showOpenDialog(GenarrFrameAffichage.this);

				if (returnVal == JFileChooser.APPROVE_OPTION) {
					fichier = fc.getSelectedFile();
					final File f = CtuluLibFile.appendExtensionIfNeeded(fichier, "xls");

					// on r�cupere l abstrct model du tableau contenant les donn�es

					/**
					 * creation d un abstract model impl�mentant l'interface CtuluTableModelInterface
					 */
					final Sinavi3ModeleExcel modele = modeleGenarr_;

					/**
					 * on essaie d 'ecrire en format excel
					 */
					final CtuluTableExcelWriter ecrivain = new CtuluTableExcelWriter(modele, f);

					try {
						ecrivain.write(null);
						new BuDialogMessage(donnees_.application_.getApp(), donnees_.application_.isSinavi_, 
								"Fichier Excel g�n�r� avec succ�s.").activate();

					} catch (final RowsExceededException _err) {
						FuLog.error(_err);
					} catch (final WriteException _err) {
						FuLog.error(_err);
					} catch (final IOException _err) {
						FuLog.error(_err);
					}

				}// fin du if si le composant est bon
			}

		});
		trier_.setToolTipText("Tie les donn�es du tableau selon la cat�gorie ou le num�ro de bateau");
		sauver_.setToolTipText("Enregistre les modifications apport�es au fichier");
		this.impression_.setToolTipText("Exporte le tableau au format xls");
		this.valider_.setToolTipText("Ferme la sous-fen�tre");

		JTableHeader header = tableau.getTableHeader();
		header.setReorderingAllowed(true);


	}

	public void maj(){

		if(panneauDeroulant_!=null)
			this.remove(panneauDeroulant_);

		/*
		Object[][] data = new Object[donnees_.genarr_.taille()][5];
		for(int i=0;i<donnees_.genarr_.taille();i++)
		{
			data[i][0]=""+donnees_.genarr_.retourner(i).navire;
			data[i][1]=""+donnees_.categoriesNavires_.retournerNavire(donnees_.genarr_.retourner(i).categorie).nom;
			data[i][2]=""+donnees_.genarr_.retourner(i).jour;
			data[i][3]=""+donnees_.genarr_.retourner(i).heure;
			data[i][4]=""+donnees_.genarr_.retourner(i).minute;
		}
		 */
		String[] titreColonnes = { "Bateau", "Cat�gorie", "Jour","Heure","Minute" };



		tableau=new BuTable(modeleGenarr_);//data,titreColonnes);
		//-- editeur par d�faut du tableau --// 
		tableau.setDefaultEditor(Object.class,new Sinavi3CellEditor());
		tableau.getTableHeader().setReorderingAllowed(false);
		
		
		
		JComboBox listeNavires=new JComboBox();
		for(int i=0;i<donnees_.listeBateaux_.listeNavires_.size();i++)
			listeNavires.addItem(donnees_.listeBateaux_.retournerNavire(i).nom);
		/**
	     * Creation pour les colonnes 2 et 3 d'objets de type jcombobox
	     */
	    final TableColumn typeCol = tableau.getColumnModel().getColumn(1);
	    typeCol.setCellEditor(new DefaultCellEditor(listeNavires));
	    
		
		
		
		
		
		
		panneauDeroulant_=new JScrollPane(tableau);

		this.setLayout(new BorderLayout());

		this.add(panneauDeroulant_,BorderLayout.CENTER);
		this.add(commandePanel_,BorderLayout.SOUTH);

		//-- remise a jour des composants en cascade --//
		this.validate();
		this.repaint();
	}

}
