package org.fudaa.fudaa.sinavi3;

import java.util.ArrayList;
import java.util.List;

/**
 * Classe de base de dfinition des Navires sous forme d'une liste de de la classe Navire. Les differentes methodes
 * associes concernent - l'ajout d'un Navire - la suppression d un navire via le nom, le numero - la modification d'un
 * navire
 * 
 * @author Adrien Hadoux
 */

public class Sinavi3Navires {

  /**
   * definition d'un tableau de Navires.
   */
  List<Sinavi3Bateau> listeNavires_ = new ArrayList<Sinavi3Bateau>();

//  int nbCategories;

  public Sinavi3Navires() {

  }

  public void ajout(final Sinavi3Bateau _n) {

    listeNavires_.add(_n);
    //-- Notification aux vues pour se mettre � jour --//
    Sinavi3DataSimulation.setProperty("navire");
  }

  /**
   * Methode de suppression d'un Navire.
   * 
   * @param n entier correspondant l'indice du navire a detruire
   */
  public void suppression(final int n) {
    listeNavires_.remove(n);
    //-- Notification aux vues pour se mettre � jour --//
    Sinavi3DataSimulation.setProperty("navire");
  }

  /**
   * Methode de modification d'un Navire une position donne
   * 
   * @param n indice du Navire a modifier dans le tableau de Navires
   * @param nav Navire modifi remplacer par un autre navire
   */
  public void modification(final int n, final Sinavi3Bateau nav) {
    this.listeNavires_.set(n, nav);
    //-- Notification aux vues pour se mettre � jour --//
    Sinavi3DataSimulation.setProperty("navire");
  }

  /**
   * @return le nombre de catgories de Navires differents
   */
  public int NombreNavires() {
    return this.listeNavires_.size();

  }

  /**
   * Methode qui retourne la i eme catgorie de Navire tres puissant car c'est cette methode qui sera a la base de la
   * modification d'un Navire: il suffit de faire appel a la fonction et de stock le Navire retourn dans une variable
   * temp, de modifier cette variable temp, ce qui aura alors pour effet de modifier le contenu du tableau de Navires!
   * 
   * @param i indice de la catgorie de Navire du tableau de Navire a retourner
   * @return un objet de type Navire qui pourra etre modifi et renvoy
   */

  public Sinavi3Bateau retournerNavire(final int i) {
    if (i < this.listeNavires_.size()) {
      return (Sinavi3Bateau) this.listeNavires_.get(i);
    } else {
      return null;
    }
  }

  /**
   * Methode qui retourne l'indice de la cat�gorie � partir du nom
   * @param nom
   * @return
   */
  public int retournerIndiceNavire(String nom){
	  for(int i=0;i<listeNavires_.size();i++)
		  if(retournerNavire(i).nom.equals(nom))
			  return i;
	  
	  return -1;
	  
  }
  
  
 

  boolean existeDoublon(final String _nomComposant, final int k) {
    for (int i = 0; i < this.listeNavires_.size(); i++) {
      if (i != k) {
        if (this.retournerNavire(i).nom.equals(_nomComposant)) {
          return true;
        }
      }

    }

    // arriv� a ce stade; on a pas touv� de doublons, le nom n'existe donc pas!
    return false;

  }
  
  /** retourne le nombre de trajet total**/
  int nombresTrajetsTotal(){
	  int total=0;
	  for(int i=0;i<listeNavires_.size();i++)
	  {
		  Sinavi3Bateau bat=retournerNavire(i);
		    total+=bat.listeTrajet_.size();
		  
	  }
	  
	  return total;
  }
  
  /** Methode de verification d'existence d'au moins un trajet pour l'ensemble des navires. **/
  int existeTrajetPourChaqueNavires(){
	  for (int i=0;i<listeNavires_.size();i++)
		  if(!retournerNavire(i).listeTrajet_.existeTrajet())
			  return i;
	  return -1;
	  
  }

public List<Sinavi3Bateau> getListeNavires_() {
	return listeNavires_;
}

public void setListeNavires_(List listeNavires_) {
	this.listeNavires_ = listeNavires_;
}
  
  

}
