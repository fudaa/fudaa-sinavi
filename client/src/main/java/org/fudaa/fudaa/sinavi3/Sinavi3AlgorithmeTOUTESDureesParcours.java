package org.fudaa.fudaa.sinavi3;

import org.fudaa.dodico.corba.sinavi3.SParametresResultatsDureParcoursCategories;
import org.fudaa.dodico.corba.sinavi3.SParametresResultatsDureesParcoursTrajet;

public class Sinavi3AlgorithmeTOUTESDureesParcours {

  /**
   * Methode qui calcule toutes les dur�es de parcours en evaluant toutes les combinaisons de trajet
   * 
   * @param _d: donnes
   */
  public static void calcul(final Sinavi3DataSimulation _d, Sinavi3ProgressFrame _fenetreProgression) {
    // calcul de toutes les dur�es de parcours
	  if(_fenetreProgression != null)
	  _fenetreProgression.miseAjourBarreProgression(75, "Calcul des dur�es de parcours");

    // 0: determiner le place totale a allouer:
    _d.params_.ResultatsCompletsSimulation.TOUTEDureesParoucrs = new SParametresResultatsDureesParcoursTrajet[
	( _d.listeBief_.listeBiefs_.size() + _d.listeEcluse_.listeEcluses_.size() ) * ( _d.listeBief_.listeBiefs_.size() +  _d.listeEcluse_.listeEcluses_.size() + 1 ) ];

    int compteur = 0;

    // 1: entre les chenaux
    for (int i = 0; i < _d.listeBief_.listeBiefs_.size(); i++) {
      for (int j = i; j < _d.listeBief_.listeBiefs_.size(); j++) { //j=i+1 remplac� par j=i par fargeix
        // calcul dans le sens entrant:
        Sinavi3AlgorithmeDureesParcours.calcul(_d, 0, i, 0, j, 0);
        _d.params_.ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur] = new SParametresResultatsDureesParcoursTrajet();
        _d.params_.ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].typeElement1 = 0;
        _d.params_.ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].element1 = i;
        _d.params_.ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].typeElement2 = 0;
        _d.params_.ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].element2 = j;
        _d.params_.ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].sens = 0;
        // allocation memoire du tableau de dur�es de parcours
        _d.params_.ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].DureeParcoursCategorie = new SParametresResultatsDureParcoursCategories[_d.params_.ResultatsCompletsSimulation.DureeParcoursCategorie.length];
        for (int k = 0; k < _d.params_.ResultatsCompletsSimulation.DureeParcoursCategorie.length; k++) {
          // alocation memoire
          _d.params_.ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].DureeParcoursCategorie[k] = new SParametresResultatsDureParcoursCategories();
          _d.params_.ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].DureeParcoursCategorie[k].nombreNavires = _d.params_.ResultatsCompletsSimulation.DureeParcoursCategorie[k].nombreNavires;
          _d.params_.ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].DureeParcoursCategorie[k].dureeMaximumParcours = _d.params_.ResultatsCompletsSimulation.DureeParcoursCategorie[k].dureeMaximumParcours;
          _d.params_.ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].DureeParcoursCategorie[k].dureeMinimumParcours = _d.params_.ResultatsCompletsSimulation.DureeParcoursCategorie[k].dureeMinimumParcours;
          _d.params_.ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].DureeParcoursCategorie[k].dureeParcoursTotale = _d.params_.ResultatsCompletsSimulation.DureeParcoursCategorie[k].dureeParcoursTotale;
          _d.params_.ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].DureeParcoursCategorie[k].distribution = _d.params_.ResultatsCompletsSimulation.DureeParcoursCategorie[k].distribution;
          _d.params_.ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].DureeParcoursCategorie[k].valeurIntervalleDistribution = _d.params_.ResultatsCompletsSimulation.DureeParcoursCategorie[k].valeurIntervalleDistribution;
        }
        compteur++;
        // calcul dans le sens sortant:
        Sinavi3AlgorithmeDureesParcours.calcul(_d, 0, i, 0, j, 1);
        _d.params_.ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur] = new SParametresResultatsDureesParcoursTrajet();
        _d.params_.ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].typeElement1 = 0;
        _d.params_.ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].element1 = i;
        _d.params_.ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].typeElement2 = 0;
        _d.params_.ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].element2 = j;
        _d.params_.ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].sens = 1;
        // allocation memoire du tableau de dur�es de parcours
        _d.params_.ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].DureeParcoursCategorie = new SParametresResultatsDureParcoursCategories[_d.params_.ResultatsCompletsSimulation.DureeParcoursCategorie.length];
        for (int k = 0; k < _d.params_.ResultatsCompletsSimulation.DureeParcoursCategorie.length; k++) {
          // alocation memoire
          _d.params_.ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].DureeParcoursCategorie[k] = new SParametresResultatsDureParcoursCategories();
          _d.params_.ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].DureeParcoursCategorie[k].nombreNavires = _d.params_.ResultatsCompletsSimulation.DureeParcoursCategorie[k].nombreNavires;
          _d.params_.ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].DureeParcoursCategorie[k].dureeMaximumParcours = _d.params_.ResultatsCompletsSimulation.DureeParcoursCategorie[k].dureeMaximumParcours;
          _d.params_.ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].DureeParcoursCategorie[k].dureeMinimumParcours = _d.params_.ResultatsCompletsSimulation.DureeParcoursCategorie[k].dureeMinimumParcours;
          _d.params_.ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].DureeParcoursCategorie[k].dureeParcoursTotale = _d.params_.ResultatsCompletsSimulation.DureeParcoursCategorie[k].dureeParcoursTotale;
          _d.params_.ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].DureeParcoursCategorie[k].distribution = _d.params_.ResultatsCompletsSimulation.DureeParcoursCategorie[k].distribution;
          _d.params_.ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].DureeParcoursCategorie[k].valeurIntervalleDistribution = _d.params_.ResultatsCompletsSimulation.DureeParcoursCategorie[k].valeurIntervalleDistribution;
        }
        compteur++;

      }
    }
    if(_fenetreProgression != null)
    _fenetreProgression.miseAjourBarreProgression(79, "Calcul des dur�es de parcours");

   
    // 2: chenaux et ecluses
    for (int i = 0; i < _d.listeBief_.listeBiefs_.size(); i++) {
      for (int j = 0; j < _d.listeEcluse_.listeEcluses_.size(); j++) {
        // calcul dans le sens entrant:
        Sinavi3AlgorithmeDureesParcours.calcul(_d, 0, i, 1, j, 0);
        _d.params_.ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur] = new SParametresResultatsDureesParcoursTrajet();
        _d.params_.ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].typeElement1 = 0;
        _d.params_.ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].element1 = i;
        _d.params_.ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].typeElement2 = 1;
        _d.params_.ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].element2 = j;
        _d.params_.ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].sens = 0;
        // allocation memoire du tableau de dur�es de parcours
        _d.params_.ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].DureeParcoursCategorie = new SParametresResultatsDureParcoursCategories[_d.params_.ResultatsCompletsSimulation.DureeParcoursCategorie.length];
        for (int k = 0; k < _d.params_.ResultatsCompletsSimulation.DureeParcoursCategorie.length; k++) {
          // alocation memoire
          _d.params_.ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].DureeParcoursCategorie[k] = new SParametresResultatsDureParcoursCategories();
          _d.params_.ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].DureeParcoursCategorie[k].nombreNavires = _d.params_.ResultatsCompletsSimulation.DureeParcoursCategorie[k].nombreNavires;
          _d.params_.ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].DureeParcoursCategorie[k].dureeMaximumParcours = _d.params_.ResultatsCompletsSimulation.DureeParcoursCategorie[k].dureeMaximumParcours;
          _d.params_.ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].DureeParcoursCategorie[k].dureeMinimumParcours = _d.params_.ResultatsCompletsSimulation.DureeParcoursCategorie[k].dureeMinimumParcours;
          _d.params_.ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].DureeParcoursCategorie[k].dureeParcoursTotale = _d.params_.ResultatsCompletsSimulation.DureeParcoursCategorie[k].dureeParcoursTotale;
          _d.params_.ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].DureeParcoursCategorie[k].distribution = _d.params_.ResultatsCompletsSimulation.DureeParcoursCategorie[k].distribution;
          _d.params_.ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].DureeParcoursCategorie[k].valeurIntervalleDistribution = _d.params_.ResultatsCompletsSimulation.DureeParcoursCategorie[k].valeurIntervalleDistribution;
        }
        compteur++;
        // calcul dans le sens sortant:
        Sinavi3AlgorithmeDureesParcours.calcul(_d, 0, i, 1, j, 1);
        _d.params_.ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur] = new SParametresResultatsDureesParcoursTrajet();
        _d.params_.ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].typeElement1 = 0;
        _d.params_.ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].element1 = i;
        _d.params_.ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].typeElement2 = 1;
        _d.params_.ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].element2 = j;
        _d.params_.ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].sens = 1;
        // allocation memoire du tableau de dur�es de parcours
        _d.params_.ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].DureeParcoursCategorie = new SParametresResultatsDureParcoursCategories[_d.params_.ResultatsCompletsSimulation.DureeParcoursCategorie.length];
        for (int k = 0; k < _d.params_.ResultatsCompletsSimulation.DureeParcoursCategorie.length; k++) {
          // alocation memoire
          _d.params_.ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].DureeParcoursCategorie[k] = new SParametresResultatsDureParcoursCategories();
          _d.params_.ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].DureeParcoursCategorie[k].nombreNavires = _d.params_.ResultatsCompletsSimulation.DureeParcoursCategorie[k].nombreNavires;
          _d.params_.ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].DureeParcoursCategorie[k].dureeMaximumParcours = _d.params_.ResultatsCompletsSimulation.DureeParcoursCategorie[k].dureeMaximumParcours;
          _d.params_.ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].DureeParcoursCategorie[k].dureeMinimumParcours = _d.params_.ResultatsCompletsSimulation.DureeParcoursCategorie[k].dureeMinimumParcours;
          _d.params_.ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].DureeParcoursCategorie[k].dureeParcoursTotale = _d.params_.ResultatsCompletsSimulation.DureeParcoursCategorie[k].dureeParcoursTotale;
          _d.params_.ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].DureeParcoursCategorie[k].distribution = _d.params_.ResultatsCompletsSimulation.DureeParcoursCategorie[k].distribution;
          _d.params_.ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].DureeParcoursCategorie[k].valeurIntervalleDistribution = _d.params_.ResultatsCompletsSimulation.DureeParcoursCategorie[k].valeurIntervalleDistribution;
        }
        compteur++;

      }
    }
 
    if(_fenetreProgression != null)   
        _fenetreProgression.miseAjourBarreProgression(82, "Calcul des dur�es de parcours");


    // 3: ecluses et ecluses
    for (int i = 0; i < _d.listeEcluse_.listeEcluses_.size(); i++) {
      for (int j = i; j < _d.listeEcluse_.listeEcluses_.size(); j++) {
        // calcul dans le sens entrant:
        Sinavi3AlgorithmeDureesParcours.calcul(_d, 1, i, 1, j, 0);
        _d.params_.ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur] = new SParametresResultatsDureesParcoursTrajet();
        _d.params_.ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].typeElement1 = 1;
        _d.params_.ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].element1 = i;
        _d.params_.ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].typeElement2 = 1;
        _d.params_.ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].element2 = j;
        _d.params_.ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].sens = 0;
        // allocation memoire du tableau de dur�es de parcours
        _d.params_.ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].DureeParcoursCategorie = new SParametresResultatsDureParcoursCategories[_d.params_.ResultatsCompletsSimulation.DureeParcoursCategorie.length];
        for (int k = 0; k < _d.params_.ResultatsCompletsSimulation.DureeParcoursCategorie.length; k++) {
          // alocation memoire
          _d.params_.ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].DureeParcoursCategorie[k] = new SParametresResultatsDureParcoursCategories();
          _d.params_.ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].DureeParcoursCategorie[k].nombreNavires = _d.params_.ResultatsCompletsSimulation.DureeParcoursCategorie[k].nombreNavires;
          _d.params_.ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].DureeParcoursCategorie[k].dureeMaximumParcours = _d.params_.ResultatsCompletsSimulation.DureeParcoursCategorie[k].dureeMaximumParcours;
          _d.params_.ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].DureeParcoursCategorie[k].dureeMinimumParcours = _d.params_.ResultatsCompletsSimulation.DureeParcoursCategorie[k].dureeMinimumParcours;
          _d.params_.ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].DureeParcoursCategorie[k].dureeParcoursTotale = _d.params_.ResultatsCompletsSimulation.DureeParcoursCategorie[k].dureeParcoursTotale;
          _d.params_.ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].DureeParcoursCategorie[k].distribution = _d.params_.ResultatsCompletsSimulation.DureeParcoursCategorie[k].distribution;
          _d.params_.ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].DureeParcoursCategorie[k].valeurIntervalleDistribution = _d.params_.ResultatsCompletsSimulation.DureeParcoursCategorie[k].valeurIntervalleDistribution;
        }
        compteur++;
        // calcul dans le sens sortant:
        Sinavi3AlgorithmeDureesParcours.calcul(_d, 1, i, 1, j, 1);
        _d.params_.ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur] = new SParametresResultatsDureesParcoursTrajet();
        _d.params_.ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].typeElement1 = 1;
        _d.params_.ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].element1 = i;
        _d.params_.ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].typeElement2 = 1;
        _d.params_.ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].element2 = j;
        _d.params_.ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].sens = 1;
        // allocation memoire du tableau de dur�es de parcours
        _d.params_.ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].DureeParcoursCategorie = new SParametresResultatsDureParcoursCategories[_d.params_.ResultatsCompletsSimulation.DureeParcoursCategorie.length];
        for (int k = 0; k < _d.params_.ResultatsCompletsSimulation.DureeParcoursCategorie.length; k++) {
          // alocation memoire
          _d.params_.ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].DureeParcoursCategorie[k] = new SParametresResultatsDureParcoursCategories();
          _d.params_.ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].DureeParcoursCategorie[k].nombreNavires = _d.params_.ResultatsCompletsSimulation.DureeParcoursCategorie[k].nombreNavires;
          _d.params_.ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].DureeParcoursCategorie[k].dureeMaximumParcours = _d.params_.ResultatsCompletsSimulation.DureeParcoursCategorie[k].dureeMaximumParcours;
          _d.params_.ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].DureeParcoursCategorie[k].dureeMinimumParcours = _d.params_.ResultatsCompletsSimulation.DureeParcoursCategorie[k].dureeMinimumParcours;
          _d.params_.ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].DureeParcoursCategorie[k].dureeParcoursTotale = _d.params_.ResultatsCompletsSimulation.DureeParcoursCategorie[k].dureeParcoursTotale;
          _d.params_.ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].DureeParcoursCategorie[k].distribution = _d.params_.ResultatsCompletsSimulation.DureeParcoursCategorie[k].distribution;
          _d.params_.ResultatsCompletsSimulation.TOUTEDureesParoucrs[compteur].DureeParcoursCategorie[k].valeurIntervalleDistribution = _d.params_.ResultatsCompletsSimulation.DureeParcoursCategorie[k].valeurIntervalleDistribution;
        }
        compteur++;

      }
    }
  

  }
}
