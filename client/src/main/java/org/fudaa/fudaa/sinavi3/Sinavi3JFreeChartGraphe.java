package org.fudaa.fudaa.sinavi3;

import java.awt.BasicStroke;
import java.awt.Color;
import java.text.NumberFormat;

import org.fudaa.dodico.corba.sinavi3.SParametresCoupleDistribution;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.Marker;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.ValueMarker;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYItemRenderer;
import org.jfree.chart.renderer.xy.XYStepAreaRenderer;
import org.jfree.chart.title.LegendTitle;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.jfree.ui.RectangleAnchor;
import org.jfree.ui.RectangleEdge;
import org.jfree.ui.TextAnchor;

/**
 * Cr�ation de m�thodes statiques tr�s utiles qui construisent automatiquement des panels contenants des objets
 * graphiques JFreeChart (camemberts, graphique,...) � partir d'une liste de valeurs et d'une liste de libell�s
 * correspondant. Les m�thodes retournent un panel contenant l'objet graphique. Il ne reste plus qu'� int�grer le panel
 * r�sultat � un panel, une frame, internalframe....
 * 
 *@version $Version$
 * @author Mederic FARGEIX
 */

final public class Sinavi3JFreeChartGraphe {

  private Sinavi3JFreeChartGraphe() {}

  /**
   * M�thode statique permettant de g�n�rer un graphe de distribution cumul�e de dur�es.
   * 
   * @param _titre titre du graphe
   * @param _values matrice de couples (duree,nombre)
   * @param _categories categories de bateaux
   * @param _totaux
   * @return
   */
  public static JFreeChart genererGrapheDistributionCumuleeDurees(String _titre, SParametresCoupleDistribution[][] _values,
      String[] _categories, int[] _totaux, boolean _echelleHeures, boolean _percent) {

    XYSeriesCollection data = new XYSeriesCollection();
    XYSeries[] series = new XYSeries[_categories.length];

    String axe1 = "Dur�e";
    String axe2;
    if (_percent) axe2 = "Proportion de bateaux (%)";
    else axe2 = "Nombre de bateaux";
    int nbCumul;

    if (_echelleHeures) axe1 += " (heures)";
    else axe1 += " (minutes)";

    for (int i = 0; i < _categories.length; i++) {
      nbCumul = 0;
      series[i] = new XYSeries(_categories[i]);
      series[i].add(0, 0); // Passage par le point de coordonn�es (0,0)
      data.addSeries(series[i]);
      for (int k = 0; k < _values[i].length - 1; k++) {
        if (_totaux[i] > 0) {
          nbCumul += _values[i][k].nombre;
          if (!_percent) {
            if (_echelleHeures) series[i].add(((_values[i][k].duree + _values[i][k + 1].duree) / 2) / 60, FonctionsSimu
                .diviserSimu(nbCumul));
            else series[i].add((_values[i][k].duree + _values[i][k + 1].duree) / 2, FonctionsSimu.diviserSimu(nbCumul));
          } else {
            if (_echelleHeures) series[i].add(((_values[i][k].duree + _values[i][k + 1].duree) / 2) / 60, 100 * nbCumul
                / _totaux[i]);
            else series[i].add((_values[i][k].duree + _values[i][k + 1].duree) / 2, 100 * nbCumul / _totaux[i]);
          }
        }
      }
    }

    JFreeChart chart = ChartFactory.createXYLineChart(_titre, axe1, axe2, data, PlotOrientation.VERTICAL, true, true,
        true);

    // L�gende � droite du graphe
    LegendTitle legend = chart.getLegend();
    legend.setPosition(RectangleEdge.RIGHT);

    // -- modification du composant graphique --//
    return chart;
  }

  /**
   * M�thode statique permettant de g�n�rer un graphe de distribution cumul�e de dur�es.
   * 
   * @param _titre titre du graphe
   * @param _intervalle intervalle d'abscisse entre deux valeurs
   * @param _values matrice de couples (duree,nombre)
   * @param _series s�ries � afficher
   * @param _totaux
   * @return
   */
  public static JFreeChart genererGrapheDistributionCumuleeDurees(String _titre, int _intervalle, short[][] _values,
      String[] _series, int[] _totaux, boolean _echelleHeures, boolean _percent) {

    XYSeriesCollection data = new XYSeriesCollection();
    XYSeries[] series = new XYSeries[_series.length];

    String axe1 = "Dur�e";
    String axe2;
    if (_percent) axe2 = "Proportion de bateaux (%)";
    else axe2 = "Nombre de bateaux";
    int nbCumul;

    if (_echelleHeures) axe1 += " (heures)";
    else axe1 += " (minutes)";

    for (int i = 0; i < _series.length; i++) {
      nbCumul = 0;
      series[i] = new XYSeries(_series[i]);
      series[i].add(0, 0); // Passage par le point de coordonn�es (0,0)
      data.addSeries(series[i]);
      for (int k = 0; k < _values[i].length - 1; k++) {
        if (_totaux[i] > 0) {
          nbCumul += _values[i][k];
          if (!_percent) {
            if (_echelleHeures) series[i].add(((k + .5) * _intervalle) / 60, FonctionsSimu.diviserSimu(nbCumul));
            else series[i].add((k + .5) * _intervalle, FonctionsSimu.diviserSimu(nbCumul));
          } else {
            if (_echelleHeures) series[i].add(((k + .5) * _intervalle) / 60, 100 * nbCumul / _totaux[i]);
            else series[i].add((k + .5) * _intervalle, 100 * nbCumul / _totaux[i]);
          }
        }
      }
    }

    JFreeChart chart = ChartFactory.createXYLineChart(_titre, axe1, axe2, data, PlotOrientation.VERTICAL, true, true,
        true);

    // L�gende � droite du graphe
    LegendTitle legend = chart.getLegend();
    legend.setPosition(RectangleEdge.RIGHT);

    // -- modification du composant graphique --//
    return chart;
  }

  /**
   * M�thode statique permettant de g�n�rer un histogramme de distribution de dur�es.
   */
  public static JFreeChart genererHistoDistributionDurees(String _titre, SParametresCoupleDistribution[][] _values,
      String[] _categories, int[] _totaux, boolean _echelleHeures, boolean _percent) {

    XYSeriesCollection data = new XYSeriesCollection();
    XYSeries[] series = new XYSeries[_categories.length];

    String axe1 = "Dur�e";
    String axe2;
    if (_percent) axe2 = "Proportion de bateaux (%)";
    else axe2 = "Nombre de bateaux";

    if (_echelleHeures) axe1 += " (heures)";
    else axe1 += " (minutes)";

    for (int i = 0; i < _categories.length; i++) {
      series[i] = new XYSeries(_categories[i]);
      data.addSeries(series[i]);
      for (int k = 0; k < _values[i].length; k++) {
        if (_totaux[i] > 0) {
          if (!_percent) {
            if (_echelleHeures) series[i]
                .add(_values[i][k].duree / 60, FonctionsSimu.diviserSimu(_values[i][k].nombre));
            else series[i].add(_values[i][k].duree, FonctionsSimu.diviserSimu(_values[i][k].nombre));
          } else {
            if (_echelleHeures) series[i].add(_values[i][k].duree / 60, 100 * _values[i][k].nombre / _totaux[i]);
            else series[i].add(_values[i][k].duree, 100 * _values[i][k].nombre / _totaux[i]);
          }
        }
      }
    }

    JFreeChart chart = ChartFactory.createXYStepAreaChart(_titre, axe1, axe2, data, PlotOrientation.VERTICAL, true,
        true, true);
    XYPlot plot = chart.getXYPlot();
    XYItemRenderer renderer = new XYStepAreaRenderer();
    plot.setRenderer(0, renderer);

    // L�gende � droite du graphe
    LegendTitle legend = chart.getLegend();
    legend.setPosition(RectangleEdge.RIGHT);

    return chart;
  }

  /**
   * M�thode statique permettant de g�n�rer un histogramme de distribution de dur�es.
   */
  public static JFreeChart genererHistoDistributionDurees(String _titre, int _intervalle, short[][] _values, String[] _categories, int[] _totaux, boolean _echelleHeures, boolean _percent){
		
	  XYSeriesCollection data = new XYSeriesCollection();
	  XYSeries[] series = new XYSeries[_categories.length];
	
	  String axe1 = "Dur�e";
	  String axe2;
	  if (_percent) axe2 = "Proportion de bateaux (%)"; else axe2 = "Nombre de bateaux";
	
	  if (_echelleHeures) axe1 += " (heures)"; else axe1 += " (minutes)";
	
	  for (int i = 0; i < _categories.length; i++)
	  {
		  series[i] = new XYSeries(_categories[i]);
		  data.addSeries(series[i]);
		  for(int k = 0; k < _values[i].length; k++)
		  {
			  if (_totaux[i] > 0)
			  {
				  if (!_percent)
				  {
					  if (_echelleHeures) series[i].add(k*_intervalle/60., FonctionsSimu.diviserSimu(_values[i][k])); else series[i].add(k*_intervalle, FonctionsSimu.diviserSimu(_values[i][k]));
				  }
				  else
				  {
					  if (_echelleHeures) series[i].add(k*_intervalle/60., 100*_values[i][k]/_totaux[i]); else series[i].add(k*_intervalle, 100*_values[i][k]/_totaux[i]);
				  }
			  }
		  }
	  }
	
	  JFreeChart chart = ChartFactory.createXYStepAreaChart(_titre, axe1, axe2, data, PlotOrientation.VERTICAL, true, true, true);
	  XYPlot plot = chart.getXYPlot();
	  XYItemRenderer renderer = new XYStepAreaRenderer();
	  plot.setRenderer(0, renderer);
		
	  //L�gende � droite du graphe		
	  LegendTitle legend = chart.getLegend();
	  legend.setPosition(RectangleEdge.RIGHT);
	
	  return chart;
	}
  
  /**
   * M�thode statique permettant de g�n�rer un histogramme de distribution de valeurs enti�res, selon un intervalle donn�, les barres �tant centr�es sur les valeurs
   */
  public static JFreeChart genererHistoDistributionCumuleeCentreSurValeurs(String _titre, String _titreX, String _titreY, int _intervalle, int[][] _values,
      String[] _series, int[] _totaux, boolean _percent) {

    XYSeriesCollection data = new XYSeriesCollection();
    XYSeries[] series = new XYSeries[_series.length];

    String axe1 = _titreX;
    String axe2 = _titreY;

    for (int i = 0; i < _series.length; i++) {
      series[i] = new XYSeries(_series[i]);
      data.addSeries(series[i]);
      for (int k = 0; k < _values[i].length; k++) {
        if (_totaux[i] > 0) {
          if (!_percent) series[i].add((k-.5) * _intervalle, _values[i][k]); //-0,5 pour centrer les barres sur la valeur
          else series[i].add((k-.5) * _intervalle, Float.valueOf(_values[i][k]) / _totaux[i]);
        }
      }
    }

    JFreeChart chart = ChartFactory.createXYStepAreaChart(_titre, axe1, axe2, data, PlotOrientation.VERTICAL, true, true, true);
    XYPlot plot = chart.getXYPlot();
    XYItemRenderer renderer = new XYStepAreaRenderer();
    plot.setRenderer(0, renderer);
    
    // set the range axis to display integers only...
    final NumberAxis xAxis = (NumberAxis) plot.getDomainAxis();
    xAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
    xAxis.setLowerBound(-0.5);
    
    if(_percent) {
    	final NumberAxis yAxis = (NumberAxis) plot.getRangeAxis();
    	yAxis.setNumberFormatOverride(NumberFormat.getPercentInstance());
    	yAxis.setUpperBound(1); // On force une graduation jusqu'� 100%
    }

    // L�gende � droite du graphe
    LegendTitle legend = chart.getLegend();
    legend.setPosition(RectangleEdge.RIGHT);

    return chart;
  }

  /**
   * M�thode statique d'affichage d'un seuil vertical.
   * 
   * @param _chart un graphe
   * @param _seuil la valeur du seuil en minutes
   * @param _echelleHeures booleen traduisant l'affichage des durees en hh.mm
   * @return
   */
  public static void afficherSeuil(ChartPanel _chart, int _seuil, boolean _echelleHeures) {

    JFreeChart chart = _chart.getChart();
    XYPlot plot = chart.getXYPlot();
    Marker marker;
    if (_echelleHeures) marker = new ValueMarker(_seuil / 60.);
    else marker = new ValueMarker(_seuil);
    marker.setLabel("seuil");
    marker.setPaint(Color.black);
    marker.setLabelAnchor(RectangleAnchor.TOP_LEFT);
    marker.setLabelTextAnchor(TextAnchor.TOP_RIGHT);
    marker.setStroke(new BasicStroke(3.0F));
    plot.addDomainMarker(marker);

    _chart.setChart(chart);
  }
  
  /**
   * M�thode statique d'affichage d'un seuil horizontal.
   * 
   * @param _chart un graphe
   * @param _seuil la valeur du seuil en minutes
   * @param _echelleHeures booleen traduisant l'affichage des durees en hh.mm
   * @return
   */
  public static void afficherSeuilHorizontal(ChartPanel _chart, String _nom, int _seuil, boolean _echelleHeures) {

    JFreeChart chart = _chart.getChart();
    XYPlot plot = chart.getXYPlot();
    Marker marker;
    if (_echelleHeures) marker = new ValueMarker(_seuil / 60.);
    else marker = new ValueMarker(_seuil);
    marker.setLabel(_nom);
    marker.setPaint(Color.black);
    marker.setLabelAnchor(RectangleAnchor.TOP_LEFT);
    marker.setLabelTextAnchor(TextAnchor.TOP_RIGHT);
    marker.setStroke(new BasicStroke(3.0F));
    plot.addRangeMarker(marker);

    _chart.setChart(chart);
  }

}
