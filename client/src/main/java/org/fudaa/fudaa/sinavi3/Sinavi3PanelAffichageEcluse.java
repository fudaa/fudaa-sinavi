package org.fudaa.fudaa.sinavi3;

/**
 * 
 */
import java.awt.BorderLayout;
import java.awt.Component;
import java.util.EventObject;

import javax.swing.BorderFactory;
import javax.swing.DefaultCellEditor;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListCellRenderer;
import javax.swing.ListSelectionModel;
import javax.swing.border.Border;
import javax.swing.event.CellEditorListener;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableColumn;
import javax.swing.table.TableModel;

import org.apache.commons.lang.StringUtils;

import com.memoire.bu.BuDialogError;
import com.memoire.bu.BuTable;

// import javax.swing.event.*;
/**
 * Panel d'affichage des donn�es des ecluses sous forme d un tableau: avec la possibilit� de modifier ou de
 * supprimer une donn�e: NOTE TRES PRATIQUE SUR LE FONCTIONNEMENT DES BuTable IL FAUT AFFICHER LE HEADER SEPAREMMENT
 * SI ON UTILISE PAS DE JSCROLLPANE DIRECTEMENT ON PEUT POUR CELA UTILISER LE LAYOUT BORDERLAYOUT ET AFFICHER LE
 * TABLEHEADER EN DEBUT DE FICHIER ET AFFICHER LE TABLEAU AU CENTER
 * 
 * @author Adrien Hadoux
 */
public class Sinavi3PanelAffichageEcluse extends JPanel {

  String[] titreColonnes = { "Nom", "Longueur", "Largeur", "Profondeur", "Hauteur de chute", "Bassin�e montante (min)", "Bassin�e avalante (min)", "Manoeuvre entrante (min)","Manoeuvre sortante (min)","Seconde Manoeuvre entrante (min)","Seconde Manoeuvre sortante (min)",
      "Loi d'indisponibilit�", "Fr�quence d'indisponibilit�", "Dur�e d'indisponibilit�", "Coef bassin d'�pargne", "Cr�neau 1", "Cr�neau 2", "Cr�neau 3" };

  /**
   * Tableau de type BuTable qui contiendra les donn�es des quais
   */

  Object[][] data = {};

  TableModel modele_;
  BuTable tableau_ = new BuTable(data, titreColonnes);

  /**
   * Panel ascenceur
   */
  JScrollPane ascenceur;

  /**
   * Bordure du tableau
   */

  Border borduretab = BorderFactory.createLoweredBevelBorder();

private Sinavi3DataSimulation donnees_;

  /**
   * constructeur du panel d'affichage des quais
   * 
   * @param d donn�es de la simulation
   */
  Sinavi3PanelAffichageEcluse(final Sinavi3DataSimulation d) {
	  
    setLayout(new BorderLayout());
    this.maj(d);

  }
  
  public Object[] constructLine(final int line) {
		return constructLine(donnees_.listeEcluse_.retournerEcluse(line));
	}
	
	public Object[] constructLine(final Sinavi3Ecluse ecluse) {
		 int cpt=0;
		 Object[]  data = new Object[titreColonnes.length];
	     data[cpt++] = ecluse.nom_;
	     data[cpt++] = "" + (float) ecluse.longueur_;
	     data[cpt++] = "" + (float) ecluse.largeur_;
	     data[cpt++] = "" + (float) ecluse.profondeur_;
	     data[cpt++] = "" + (float) ecluse.hauteurchute;
	     data[cpt++] = "" + ecluse.tempsFausseBassinneeMontant_;
	     data[cpt++] = "" + ecluse.tempsFausseBassinneeAvalant_;
	     data[cpt++] = "" + ecluse.dureeManoeuvreEntrant_;
	     data[cpt++] = " " + ecluse.dureeManoeuvreSortant_;
	     data[cpt++] = "" + ecluse.dureeManoeuvreEntrant2_;
	     data[cpt++] = " " + ecluse.dureeManoeuvreSortant2_;

	      if (ecluse.typeLoi_ == 0) {
	       data[cpt++] = "Loi d'Erlang: " + Integer.toString(ecluse.loiFrequence_);
	      } else if (ecluse.typeLoi_ == 1) {
	       data[cpt++] = "Loi deterministe: ";
	      } else if (ecluse.typeLoi_ == 2) {
	       data[cpt++] = "Loi journaliere: ";
	      }

	      if (ecluse.typeLoi_ == 0) {
	       data[cpt++] = "" + (float) ecluse.frequenceMoyenne_;
	      } else {
	       data[cpt++] = "";
	      }
	     data[cpt++] = "" + (float) ecluse.dureeIndispo_;
	     data[cpt++] = "" + (float) ecluse.coefficientBassinEpargne_;
	      //ndata[i][cpt++] = Integer.toString(ecluse.loiIndispo_);

	     data[cpt++] = " " + Sinavi3Horaire.format( ecluse.h_.semaineCreneau1HeureDep) + " � " + Sinavi3Horaire.format( ecluse.h_.semaineCreneau1HeureArrivee);
	     data[cpt++] = " " + Sinavi3Horaire.format( ecluse.h_.semaineCreneau2HeureDep) + " � " + Sinavi3Horaire.format( ecluse.h_.semaineCreneau2HeureArrivee);
	     data[cpt++] = " " + Sinavi3Horaire.format( ecluse.h_.semaineCreneau3HeureDep) + " � " + Sinavi3Horaire.format( ecluse.h_.semaineCreneau3HeureArrivee);

	     return data;
		
	}

  /**
   * Methode d'ajout d'une cellule
   */
  void maj(final Sinavi3DataSimulation d) {
	donnees_ = d;
    
	modele_ = new TableModel() {

		public int getRowCount() {
			return d.listeEcluse_.getListeEcluses().size();
		}

		public int getColumnCount() {
			return titreColonnes.length;
		}

		public String getColumnName(int columnIndex) {
			return titreColonnes[columnIndex];
		}

		public Class<?> getColumnClass(int columnIndex) {

			return String.class;
		}

		public boolean isCellEditable(int rowIndex, int columnIndex) {
			return true;
		}

		public Object getValueAt(int rowIndex, int columnIndex) {			
			return constructLine(rowIndex)[columnIndex];
		}

		

		public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
			if(StringUtils.isBlank(aValue.toString())) {
				new BuDialogError(donnees_.application_.getApp(), donnees_.application_.isSinavi_, "Saisie manquante")
				.activate();
				return;
			}
			final Sinavi3Ecluse ecluse = d.listeEcluse_.retournerEcluse(rowIndex);
			try{ 
			switch(columnIndex) {
			case 0: {
				if(d.listeEcluse_.existeDoublon(aValue.toString(), rowIndex)) {
					new BuDialogError(donnees_.application_.getApp(), donnees_.application_.isSinavi_, "Nom deja pris.")
					.activate();
					
				}else
					ecluse.setNom_(aValue.toString());
			}break;
			case 1:ecluse.setLongueur_(Double.parseDouble(aValue.toString()));break;
			case 2:ecluse.setLargeur_(Double.parseDouble(aValue.toString()));break;
			case 3:ecluse.setProfondeur_(Double.parseDouble(aValue.toString()));break;
			case 4:ecluse.setHauteurchute(Double.parseDouble(aValue.toString()));break;
			case 5:ecluse.setTempsFausseBassinneeMontant_(Integer.parseInt(aValue.toString()));break;
			case 6:ecluse.setTempsFausseBassinneeAvalant_(Integer.parseInt(aValue.toString()));break;
			case 7:ecluse.setDureeManoeuvreEntrant_(Integer.parseInt(aValue.toString()));break;
			case 8:ecluse.setDureeManoeuvreSortant_(Integer.parseInt(aValue.toString()));break;
			case 9:ecluse.setDureeManoeuvreEntrant2_(Integer.parseInt(aValue.toString()));break;
			case 10:ecluse.setDureeManoeuvreSortant2_(Integer.parseInt(aValue.toString()));break;
			
			case 11:ecluse.setTypeLoi_(Integer.parseInt(aValue.toString()));break;
			case 12:ecluse.setFrequenceMoyenne_(Double.parseDouble(aValue.toString()));break;
			case 13:ecluse.setDureeIndispo_(Double.parseDouble(aValue.toString()));break;
			case 14:ecluse.setCoefficientBassinEpargne_(Double.parseDouble(aValue.toString()));break;
			}
			} catch(NumberFormatException e) {
				new BuDialogError(d.application_.getApp(), d.application_.isSinavi_, "La valeur doit �tre un nombre.")
				.activate();
			}
		}
		
		

		public void addTableModelListener(TableModelListener l) {
			// TODO Auto-generated method stub

		}

		public void removeTableModelListener(TableModelListener l) {
			// TODO Auto-generated method stub

		}

	};

	this.tableau_ = new BuTable(modele_);
	tableau_.setDefaultEditor(Object.class,new Sinavi3CellEditor());
	JComboBox<String> combo = new JComboBox<String>(new String[]{"0","1"});
	tableau_.getColumnModel().getColumn(11).setCellEditor(new DefaultCellEditor(combo));
	//tableau_.getColumnModel().getColumn(8).setCellEditor(new DefaultCellEditor(new JComboBox<String>(new String[]{"0","1","2","3","4","5","6","7","8","9","10"})));
	final DefaultListCellRenderer defaultRenderer = new DefaultListCellRenderer();
	combo.setRenderer(new ListCellRenderer<String>() {

		public Component getListCellRendererComponent(
				JList<? extends String> list, String value, int index,
				boolean isSelected, boolean cellHasFocus) {
			 JLabel renderer = (JLabel) defaultRenderer.getListCellRendererComponent(list, value, index,
				        isSelected, cellHasFocus);
			 renderer.setText(value.equals("0")?"Erlang":"Deterministe");
				
			return renderer;
		}
	});
	TableCellEditor editorCreneau = new TableCellEditor() {
		
		public boolean stopCellEditing() {
		return true;
		}
		
		public boolean shouldSelectCell(EventObject anEvent) {
			return true;
		}
		
		public void removeCellEditorListener(CellEditorListener l) {
		}
		
		public boolean isCellEditable(EventObject anEvent) {
			return true;
		}
		
		public Object getCellEditorValue() {
				return null;
		}
		
		public void cancelCellEditing() {
		}
		
		public void addCellEditorListener(CellEditorListener l) {
		}
		
		public Component getTableCellEditorComponent(JTable table, Object value,
				boolean isSelected, int row, int column) {					
			Sinavi3Ecluse ecluse = d.listeEcluse_.retournerEcluse(row);
			donnees_.application_.addInternalFrame(new Sinavi3FrameSaisieHorairesResume(ecluse.h_));
				
			return null;
		}
	}; 
	tableau_.getColumnModel().getColumn(15).setCellEditor(editorCreneau);
	tableau_.getColumnModel().getColumn(16).setCellEditor(editorCreneau);
	tableau_.getColumnModel().getColumn(17).setCellEditor(editorCreneau);

	
    tableau_.setBorder(this.borduretab);
    tableau_.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    org.fudaa.fudaa.sinavi.factory.ColumnAutoSizer.sizeColumnsToFit(tableau_);
    tableau_.revalidate();
    this.removeAll();
    this.add(new JScrollPane(tableau_), BorderLayout.CENTER);
    this.add(/* ascenceur */tableau_.getTableHeader(), BorderLayout.NORTH);
    tableau_.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
    
    this.revalidate();
    this.updateUI();
  }

}
