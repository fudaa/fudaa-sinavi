package org.fudaa.fudaa.sinavi3;

import java.awt.Component;

import javax.swing.AbstractCellEditor;
import javax.swing.JTable;
import javax.swing.table.TableCellEditor;

public class Sinavi3CellEditor extends AbstractCellEditor
implements TableCellEditor{
	
	
	Object contenu_;
	Sinavi3TextField saisie=new Sinavi3TextField(10);
	
	public Sinavi3CellEditor() {
		
	}

	/**Methode qui retourne le composant graphique utilis� pour r�aliser les saisies. **/
	public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
		//saisie.setText(""+(String)value);
		//contenu_=value;

		//par d�faut on met dans le champs text le contenu pr�c�dent
		if((value instanceof Integer) || (value instanceof Float) || (value instanceof Double))
		saisie.setText(""+value);
		else 
			saisie.setText((String)value);

		return saisie;
	}

	public Object getCellEditorValue() {
		// TODO Auto-generated method stub
		
		//-- transformation de la virgule en point
		String contenu=saisie.getText();
		if(!contenu.equals("")){
			int indice=contenu.lastIndexOf(",");
			if(indice!=-1){
				//-- remplacement de la virgule par un point --//
				String section1=contenu.substring(0, contenu.lastIndexOf(","));
				String section2=contenu.substring(contenu.lastIndexOf(",")+1,contenu.length());
				saisie.setText(section1+"."+section2);
				
			}
		
		
		}
		
		
		
		
		return saisie.getText();
	}

}
