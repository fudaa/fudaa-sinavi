/**
 *@creation 16 oct. 06
 *@modification $Dates$
 *@license  GNU General Public Licence 2
 *@copyright   (c)1998-2006 CETMEF 2 bd Gambetta F -60231 Compiegne
 *@mail   devel@fudaa.fr 
 */
package org.fudaa.fudaa.sinavi3;

import java.util.ArrayList;

/**
 * Classe e gestion des matrics booll�ennes des regles de navigations poru chacun des cheneaux. dans cette classe sont
 * pr�sent�s les diverses m�thode: - d'ajout de valeur (dans le cas d'un ajout de cat�gorie de navires) - de suppression
 * de ligne et colonne (dans el cas d'une suppression d'une cat�gorie de navires) - de modification de valeur (dans le
 * cas d'une saisie de regles de navications car les donn�es seront deja allou�es)
 * 
 * @version $Version$
 * @author hadoux
 */
public class Sinavi3StrutureReglesNavigation {

  // attributs:

  /**
   * Vecteur de vecteur bool��ens. contient les donn�es des regles de navigations.
   */

  ArrayList vecteurDeVecteur_ = new ArrayList();

  /**
   * Constructeur
   * 
   * @param numNavires: le nombre de navires deja cr��s; permet d initailiser la matrice, ie le vecteur de vecteur
   */
  Sinavi3StrutureReglesNavigation(final int numNavires) {
    // for(int i=0;i<numNavires;i++)
    // this.ajoutNavire();
    initialisationCreationChenal(numNavires);
  }

  /**
   * Methode d 'initialisation de la matrice: a n'appeler que dans le cas de la cr�ation d un chenal.
   * 
   * @param numNavires
   */
  void initialisationCreationChenal(final int numNavires) {
    // Etape 1: on ajoute un nouveau vecteur pour chaque navire deja cr��s:
    for (int i = 0; i < numNavires; i++) {
      vecteurDeVecteur_.add(new ArrayList());

    }

    // etape 2: on ajoute a chacun de ces navires un booleen initialis� a true
    for (int i = 0; i < this.vecteurDeVecteur_.size(); i++) {
      // recuperation du vecteur ligne
      final ArrayList vecteurI = (ArrayList) this.vecteurDeVecteur_.get(i);

      for (int j = 0; j < numNavires; j++) {
        vecteurI.add(new Boolean(true));
      }

    }

  }

  /**
   * Methode qui retourne l'�l�ment (i,j) de la matrice.
   * 
   * @param i indice ligne
   * @param j indice colonne
   * @return un objet de type Booleen qui contient la valeur pour la regle de navigation correspondante
   */
  Boolean retournerAij(final int i, final int j) {

    final ArrayList vecteurI = (ArrayList) this.vecteurDeVecteur_.get(i);
    return ((Boolean) vecteurI.get(j));

  }

  /**
   * m�thode d'ajout. Lorsque l'on ajoute une cat�gorie de navires, on doit ajouter un vecteur dans le vecteur de
   * vecteur et: pour chaque vecteur du vecteur de vecteur on ajoute un booleen initialis� a true:
   */

  void ajoutNavire() {
    // etape 1: on ajoute un vecteur au vecteur de vecteurs:
    final ArrayList nouvelleLigne = new ArrayList();

    // on rempli ce nouveau vecteur des nb-1 navires
    for (int i = 0; i < this.vecteurDeVecteur_.size(); i++) {
      nouvelleLigne.add(new Boolean(true));
    }

    vecteurDeVecteur_.add(nouvelleLigne);

    // etape 2: pour chaque vecteur du vecteur de vecteur, on ajoute un booleen initialis� a true
    for (int i = 0; i < this.vecteurDeVecteur_.size(); i++) {
      // a) on recupere le vecteur i
      final ArrayList vecteur = (ArrayList) this.vecteurDeVecteur_.get(i);
      // b)on ajoute le booleen a ce vecteur i:
      vecteur.add(new Boolean(true));

    }

  }

  /**
   * Methode de suppression d'un navire:
   * 
   * @param numNavire. indice du navire a supprimer.
   */
  void suppressionNavire(final int numNavire) {
    this.vecteurDeVecteur_.remove(numNavire);

    for (int i = 0; i < this.vecteurDeVecteur_.size(); i++) {
      final ArrayList V = (ArrayList) this.vecteurDeVecteur_.get(i);
      V.remove(numNavire);

    }

  }

  /**
   * Methode de modification des donn�es lors de la saisie des regles de navigations les parametres d entr�es sont un
   * couple (navire 1,navire 2).
   * 
   * @param reponse. booleen qui correspond a la saisie de l utilisateur
   * @param navire1. indice du navire 1
   * @param navire2. indice du navire 2
   */
  void modification(final boolean reponse, final int navire1, final int navire2) {
    // 1)on recupere le vecteur i du vecteur de vecteur O(1)
    final ArrayList vectNav1 = (ArrayList) this.vecteurDeVecteur_.get(navire1);

    // 2)on modifie l'element j du vecteur i par la valeur reponse
    final Boolean val = new Boolean(reponse);

    vectNav1.set(navire2, val);

    // 3) on recupere le vecteur j du vecteur de vecteur O(1)
    final ArrayList vectNav2 = (ArrayList) this.vecteurDeVecteur_.get(navire2);

    // 4)on modifie l'element i du vecteur j par la valeure reponse
    vectNav2.set(navire1, val);

  }

  /**
   * Methode de duplication d un veceteur de vecteur a partir d un autre: permet d accelerer la saisie des donn�es et de
   * faciliter l'utilisateur
   * 
   * @param VV veceteur de vecteur que l'on va recopier pour ce vecteur
   */

  void duplication(final ArrayList VV) {
    this.vecteurDeVecteur_ = new ArrayList();

    for (int i = 0; i < VV.size(); i++) {
      // creation d un nouveau vecteur ligne que l on remplira au fur e ta mesure
      final ArrayList nouveauVecteur = new ArrayList();
      // recuperation du vecteur ligne du vecetur de vecteur VV
      final ArrayList vecteurLigne = (ArrayList) VV.get(i);
      for (int k = 0; k < vecteurLigne.size(); k++) {
        final Boolean donnee = (Boolean) vecteurLigne.get(k);
        final Boolean val = new Boolean(donnee.booleanValue());

        nouveauVecteur.add(val);
      }
      // on ajoute le vecteur rempli au vecteur de vecteur
      this.vecteurDeVecteur_.add(nouveauVecteur);

    }

  }

  void affichage() {
    System.out.println("dimensions de la matrice: " + this.vecteurDeVecteur_.size());
    for (int i = 0; i < this.vecteurDeVecteur_.size(); i++) {
      System.out.println("");
      final ArrayList vecteurI = (ArrayList) this.vecteurDeVecteur_.get(i);
      for (int j = 0; j < vecteurI.size(); j++) {
        if (((Boolean) vecteurI.get(j)).booleanValue()) {
          System.out.print(" Y ");
        } else {
          System.out.print(" N ");
        }
      }
    }

  }

}
