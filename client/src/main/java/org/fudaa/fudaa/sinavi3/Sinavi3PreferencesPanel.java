/*
 * @file         SiporPreferencesPanel.java
 * @creation     1999-10-01
 * @modification $Date: 2007-11-23 11:27:51 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.sinavi3;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.border.AbstractBorder;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;

import com.memoire.bu.BuCheckBox;
import com.memoire.bu.BuAbstractPreferencesPanel;
import com.memoire.bu.BuGridLayout;

/**
 * Panneau de preferences pour Sinavi. Il n'y a qu'une seule pr�f�rence, pour enregistrer les r�sultats ou pas.
 * 
 * @version $Revision: 1.1 $ $Date: 2007-11-23 11:27:51 $ by $Author: hadouxad $
 * @author Nicolas Chevalier
 */
public class Sinavi3PreferencesPanel extends BuAbstractPreferencesPanel implements ActionListener {
  Sinavi3Preferences options_ = Sinavi3Preferences.SINAVI;
  // Composant du paneau
  //JCheckBox cbEnregistrerResultats = new JCheckBox("Enregistrer les r�sultats");
  BuCheckBox cbEnregistrerResultats = new BuCheckBox("Enregistrer les r�sultats");
  
  public Sinavi3PreferencesPanel() {
    super();
    final BuGridLayout lo = new BuGridLayout();
    lo.setColumns(1);
    setLayout(lo);
    final AbstractBorder cadre = new CompoundBorder(new TitledBorder("R�sultats"), new EmptyBorder(5, 5, 5, 5));
    setBorder(cadre);
    cbEnregistrerResultats.addActionListener(this);
    add(cbEnregistrerResultats);
    updatePanel();
  }

  public String getTitle() {
    return "Sinavi";
  }

  /** Le bouton "cancel" est actif. */
  public boolean isPreferencesCancelable() {
    return true;
  }

  /** Quand on clique sur le bouton cancel : relit le fichier et met � jour les composants. */
  public void cancelPreferences() {
    options_.readIniFile();
    updatePanel();
    setDirty(false);
  }

  /** Le bouton "apply" est actif. */
  public boolean isPreferencesApplyable() {
    return false;
  }

  /** click sur "apply" : preferences_ de sipor est mis � jour. */
  public void applyPreferences() {
    remplitOptions();
  }

  /** Le bouton "valider" est actif. */
  public boolean isPreferencesValidable() {
    return true;
  }

  /** Quand on clique sur le bouton "valider" : �criture du fichier et maj de preferences_ dans sinavi. */
  public void validatePreferences() {
    remplitOptions();
    options_.writeIniFile();
    setDirty(false);
  }

  /** Lit la valeur dans le panneau et l'affecte aux pr�f�rences en m�moire. */
  private void remplitOptions() {
    options_.putStringProperty("enregistrerResultats", String.valueOf(cbEnregistrerResultats.isSelected()));
  }

  /** Met � jour la case � cocher du panneau. */
  private void updatePanel() {
    cbEnregistrerResultats.setSelected(options_.getBooleanProperty("enregistrerResultats"));
  }

public void actionPerformed(final ActionEvent _e) {
    setDirty(true);
  }
}
