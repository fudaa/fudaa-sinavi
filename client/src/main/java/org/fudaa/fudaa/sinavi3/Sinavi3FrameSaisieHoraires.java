package org.fudaa.fudaa.sinavi3;

/**
 * 
 */

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;

import javax.swing.BorderFactory;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.Border;

import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.fudaa.ressource.FudaaResource;

import com.memoire.bu.BuButton;

/**
 * Fenetre de saisie des crneaux d'horaires standart
 * 
 * @author Adrien Hadoux
 */
public class Sinavi3FrameSaisieHoraires extends Sinavi3InternalFrame {

  // attributs

  /**
   * Panel global contenant toutes les donnes de la frame
   */
  JPanel global_;

  /**
   * JText du premier horaire a saisir
   */
  Sinavi3TextFieldDureePM  creneau1debut_ = new Sinavi3TextFieldDureePM (3);
  Sinavi3TextFieldDureePM  creneau1fin_ = new Sinavi3TextFieldDureePM (3);
  Sinavi3TextFieldDureePM  creneau2debut_ = new Sinavi3TextFieldDureePM (3);
  Sinavi3TextFieldDureePM  creneau2fin_ = new Sinavi3TextFieldDureePM (3);

  /**
   * Bouton de validation des horaires lance les tests decontroles de cohrence des donnes
   */
  final BuButton validation_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_oui"), "valider");

  // bordures
  Border raisedBevel_ = BorderFactory.createRaisedBevelBorder();
  Border loweredBevel_ = BorderFactory.createLoweredBevelBorder();
  Border compound_ = BorderFactory.createCompoundBorder(raisedBevel_, loweredBevel_);
  Border bordnormal_ = BorderFactory.createEtchedBorder();

  /**
   * Objet horaire qui sera rempli suite a la saisie de l utilisateur
   */
  Sinavi3Horaire horaire_;

  /**
   * Constructeur de la fenetre de saisie des horaires
   * 
   * @param h horaire qui va etre rempli par l'utilisateur
   */

  Sinavi3FrameSaisieHoraires(final Sinavi3Horaire h) {

    super("", true, true, true, true);
    horaire_ = h;
    setTitle("Saisie d'un horaire par rapport � la pleine mer");
    setSize(415, 180);
    setBorder(BorderFactory.createCompoundBorder(BorderFactory.createRaisedBevelBorder(), BorderFactory.createLoweredBevelBorder()));
    
    /**
     * position relative a la position parent
     */
    // setLocationRelativeTo(this.getParent());

    /**
     * *************************************************** controles sur les composants
     * **************************************************
     */

    // listener du bouton de validation:
    validation_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        
        // lancement de la fonction de validation des dones saisies:
        creationHoraire();
      }
    });

    this.creneau1debut_
        .setToolTipText("Saisissez le nombre correspondant  l'horaire d'ouverture. Par exemple, si vous voulez rentrer 8h46, tapez 8.46");
    this.creneau2debut_
        .setToolTipText("Saisissez le nombre correspondant  l'horaire d'ouverture. Par exemple, si vous voulez rentrer 8h46, tapez 8.46");
    this.creneau1fin_
        .setToolTipText("Saisissez le nombre correspondant  l'horaire de fermeture. Par exemple, si vous voulez rentrer 8h46, tapez 8.46");
    this.creneau2fin_
        .setToolTipText("Saisissez le nombre correspondant  l'horaire de fermeture. Par exemple, si vous voulez rentrer 8h46, tapez 8.46");

    if (horaire_.semaineCreneau1HeureArrivee != -1 && horaire_.semaineCreneau2HeureArrivee != -1
        && horaire_.semaineCreneau1HeureDep != -1 && horaire_.semaineCreneau2HeureDep != -1) {
      // valeur recuperee de la structure horaire
      this.creneau1debut_.setText("" + (float) horaire_.semaineCreneau1HeureDep);
      this.creneau1fin_.setText("" + (float) horaire_.semaineCreneau1HeureArrivee);
      this.creneau2debut_.setText("" + (float) horaire_.semaineCreneau2HeureDep);
      this.creneau2fin_.setText("" + (float) horaire_.semaineCreneau2HeureArrivee);
    } else {

      // valeur par defaut:
      this.creneau1debut_.setText(CtuluLibString.ZERO);
      this.creneau1fin_.setText(CtuluLibString.ZERO);
      this.creneau2debut_.setText(CtuluLibString.ZERO);
      this.creneau2fin_.setText(CtuluLibString.ZERO);
    }

    this.creneau1debut_.addFocusListener(new FocusAdapter()

    {
      public void focusGained(final FocusEvent e) {
        creneau1debut_.selectAll();
      }

      public void focusLost(final FocusEvent e) {
        System.out.print("test validite de Longueur Min");
        // test de validit si on a rentr quelque chose
        if (!creneau1debut_.getText().equals("")) {
          try {
            final float i = Float.parseFloat(creneau1debut_.getText());

            

          } catch (final NumberFormatException nfe) {
            JOptionPane.showMessageDialog(null, "Erreur!! Ce nombre n'existe pas!!!!", "Avertissement",
                JOptionPane.ERROR_MESSAGE);
            creneau1debut_.setText("");
          }
        }
      }
    });

    this.creneau1fin_.addFocusListener(new FocusAdapter() {
      public void focusGained(final FocusEvent e) {
        creneau1fin_.selectAll();
      }

      public void focusLost(final FocusEvent e) {
        System.out.print("test validite de Longueur Min");
        // test de validit si on a rentr quelque chose
        if (!creneau1fin_.getText().equals("")) {
          try {
            final float i = Float.parseFloat(creneau1fin_.getText());

            
            /*
             * else { //horaire fin inferieur a horaire de depart: if(i< Float.parseFloat(creneau1debut_.getText())) {
             * JOptionPane.showMessageDialog(null,"Erreur!! Cet horaire de fin est infererieur a l'horaire de
             * depart!!!!","Avertissement",JOptionPane.ERROR_MESSAGE); creneau1fin_.setText(""); } }
             */

          } catch (final NumberFormatException nfe) {
            JOptionPane.showMessageDialog(null, "Erreur!! Ce nombre n'existe pas!!!!", "Avertissement",
                JOptionPane.ERROR_MESSAGE);
            creneau1fin_.setText("");
          }
        }
      }
    });

    this.creneau2debut_.addFocusListener(new FocusAdapter() {
      public void focusGained(final FocusEvent e) {
        creneau2debut_.selectAll();
      }

      public void focusLost(final FocusEvent e) {
        // test de validit si on a rentr quelque chose
        if (!creneau2debut_.getText().equals("")) {
          try {
            final float i = Float.parseFloat(creneau2debut_.getText());

            
            /*
             * else//cas des intervalles qui se chevauchent if(i>Float.parseFloat(creneau1debut_.getText()) && i<Float.parseFloat(creneau1fin_.getText())) {
             * JOptionPane.showMessageDialog(null,"Erreur!! Cet horaire est inclus dans le premier creneau!!!! Les
             * creneaux sont des intervalles de temps distincts...","Avertissement",JOptionPane.ERROR_MESSAGE);
             * creneau2debut_.setText(""); }
             */

          } catch (final NumberFormatException nfe) {
            JOptionPane.showMessageDialog(null, "Erreur!! Ce nombre n'existe pas!!!!", "Avertissement",
                JOptionPane.ERROR_MESSAGE);
            creneau2debut_.setText("");
          }
        }
      }
    });

    this.creneau2fin_.addFocusListener(new FocusAdapter() {
      public void focusGained(final FocusEvent e) {
        creneau2fin_.selectAll();
      }

      public void focusLost(final FocusEvent e) {
        // test de validit si on a rentr quelque chose
        if (!creneau2fin_.getText().equals("")) {
          try {
            final float i = Float.parseFloat(creneau2fin_.getText());

            
            

          } catch (final NumberFormatException nfe) {
            JOptionPane.showMessageDialog(null, "Erreur!! Ce nombre n'existe pas!!!!", "Avertissement",
                JOptionPane.ERROR_MESSAGE);
            creneau2fin_.setText("");
          }
        }
      }
    });

    /**
     * ********************************************** Organisation des donnes dans la frame:
     * **********************************************
     */
    global_ = new JPanel();
    global_.setLayout(new GridLayout(3, 1));

    // panel de saisie du premier creneau horaire
    final JPanel ch1 = new JPanel();

    ch1.add(new JLabel(" Creneau 1: Horaire: "));
    ch1.add(this.creneau1debut_);
    ch1.add(new JLabel("�"));
    ch1.add(this.creneau1fin_);
    ch1.add(new JLabel("HEURES.MINUTES"));
    ch1.setBorder(bordnormal_);
    global_.add(ch1);

    // panel de saisie du deuxieme horaire
    final JPanel ch2 = new JPanel();
    ch2.add(new JLabel(" Creneau 2: Horaire: "));
    ch2.add(this.creneau2debut_);
    ch2.add(new JLabel("�"));
    ch2.add(this.creneau2fin_);
    ch2.add(new JLabel("HEURES.MINUTES"));
    ch2.setBorder(bordnormal_);

    global_.add(ch2);

    // panel qui contient le bouton de validation:
    final JPanel ch3 = new JPanel();
    ch3.add(new JLabel("Cliquez ici pour valider: "));
    ch3.add(this.validation_);
    ch3.setBorder(bordnormal_);
    global_.add(ch3);
    global_.setBorder(compound_);
    getContentPane().add(global_);

    // affichage de la fenetre
    setVisible(true);

  }

  /**
   * Methode booleene de validation des donnes saisies
   */
  boolean controle_creationHoraire() {

    if (this.creneau1debut_.getText().equals("")) {
      JOptionPane.showMessageDialog(null, "Erreur!! Creneau 1: heure de depart manquant!!!!", "Avertissement",
          JOptionPane.ERROR_MESSAGE);
      return false;
    }
    if (this.creneau1fin_.getText().equals("")) {
      JOptionPane.showMessageDialog(null, "Erreur!! Creneau 1: heure de fin manquant!!!!", "Avertissement",
          JOptionPane.ERROR_MESSAGE);
      return false;
    }

    if (this.creneau2debut_.getText().equals("")) {
      JOptionPane.showMessageDialog(null, "Erreur!! Creneau 2: heure de depart manquant!!!!", "Avertissement",
          JOptionPane.ERROR_MESSAGE);
      return false;
    }
    if (this.creneau2fin_.getText().equals("")) {
      JOptionPane.showMessageDialog(null, "Erreur!! Creneau 2: heure de fin manquant!!!!", "Avertissement",
          JOptionPane.ERROR_MESSAGE);
      return false;
    }

    final float a1 = Float.parseFloat(creneau1debut_.getText());
    final float a2 = Float.parseFloat(creneau1fin_.getText());
    final float b1 = Float.parseFloat(creneau2debut_.getText());
    final float b2 = Float.parseFloat(creneau2fin_.getText());

    if (a1 > a2) {
      JOptionPane.showMessageDialog(null,
          "Horaire du creneau 1 de fin est infererieur a l'horaire de depart.", "Avertissement",
          JOptionPane.ERROR_MESSAGE);
      return false;
    }
    /*else  if(a1==a2 && b1==b2)
    {
    	JOptionPane.showMessageDialog(null,
    	          "Toutes les valeurs sont identiques. Ce ne sont pas des cr�neaux.", "Avertissement",
    	          JOptionPane.ERROR_MESSAGE);
    	      return false;
    }*/
    else if (b1 > b2) {
      JOptionPane.showMessageDialog(null,
          "Horaire du creneau 2 de fin est infererieur a l'horaire de depart.", "Avertissement",
          JOptionPane.ERROR_MESSAGE);
      return false;
    } else if (b1 > a1 && b1 < a2 && (b1 != 0 || b2 != 0)) {
      JOptionPane
          .showMessageDialog(
              null,
              "Le creneau 2 est inclus dans le premier creneau. \n Les creneaux sont des intervalles de temps distincts.",
              "Avertissement", JOptionPane.ERROR_MESSAGE);
      return false;

    } else if (b2 > a1 && b2 < a2 && (b1 != 0 || b2 != 0)) {
      JOptionPane
          .showMessageDialog(
              null,
              "Le creneau 2 est inclus dans le premier creneau. Les creneaux sont des intervalles de temps distincts.",
              "Avertissement", JOptionPane.ERROR_MESSAGE);
      return false;
    } else if (b1 < a1 && b2 > a2 && (b1 != 0 || b2 != 0)) {
      JOptionPane
          .showMessageDialog(
              null,
              "Le creneau 2 est inclus dans le premier creneau. Les creneaux sont des intervalles de temps distincts.",
              "Avertissement", JOptionPane.ERROR_MESSAGE);
      return false;
    }
    else if(a1>b2 && b2 !=0){
    	JOptionPane
        .showMessageDialog(
            null,
            "Le creneau 1 doit �tre inf�rieur au cr�neau 2.",
            "Avertissement", JOptionPane.ERROR_MESSAGE);
    return false;
    }
    // tous les tests ont t ngatifs, les donnes sont donc correctes.
    return true;
  }

  /**
   * Methode de controle de creation des horaires
   */

  void creationHoraire() {

    if (controle_creationHoraire()) {
      System.out.println("yes: tous les controles ont t vrifi, les donnes sont cohrentes!!");

      // creation d'un nouvel objet horaire:
      horaire_.semaineCreneau1HeureDep = Float.parseFloat(this.creneau1debut_.getText());
      horaire_.semaineCreneau1HeureArrivee = Float.parseFloat(this.creneau1fin_.getText());
      horaire_.semaineCreneau2HeureDep = Float.parseFloat(this.creneau2debut_.getText());
      horaire_.semaineCreneau2HeureArrivee = Float.parseFloat(this.creneau2fin_.getText());
           
      
      // verification des donnes saisies:
      horaire_.affichage();

      // destruction de la frame:
      this.dispose();

    }

  }

}
