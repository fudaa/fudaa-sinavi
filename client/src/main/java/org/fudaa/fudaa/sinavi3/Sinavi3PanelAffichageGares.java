package org.fudaa.fudaa.sinavi3;

/**
 * 
 */

import java.awt.BorderLayout;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.border.Border;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;

import org.apache.commons.lang.StringUtils;

import com.memoire.bu.BuDialogError;
import com.memoire.bu.BuTable;

/**
 * Panel d'affichage des donn�es des gares sous forme d un tableau BuTable: avec la possibilit� de modifier ou de
 * supprimer une donn�e: NOTE TRES PRATIQUE SUR LE FONCTIONNEMENT DES BuTable IL FAUT AFFICHER LE HEADER SEPAREMMENT
 * SI ON UTILISE PAS DE JSCROLLPANE DIRECTEMENT ON PEUT POUR CELA UTILISER LE LAYOUT BORDERLAYOUT ET AFFICHER LE
 * TABLEHEADER EN DEBUT DE FICHIER ET AFFICHER LE TABLEAU AU CENTER
 * 
 * @author Adrien Hadoux
 */
public class Sinavi3PanelAffichageGares extends JPanel {

  String[] titreColonnes = { "Nom" };

  /**
   * Tableau de type BuTable qui contiendra les donn�es des bassins
   */

  BuTable tableau_;

  /**
   * Bordure du tableau
   */

  Border borduretab = BorderFactory.createLoweredBevelBorder();

  /**
   * constructeur du panel d'affichage des bassins
   * 
   * @param d donn�es de la simulation
   */
  public Sinavi3PanelAffichageGares(final Sinavi3DataSimulation d) {

    setLayout(new BorderLayout());
    this.maj(d);

  }

  /**
   * Methode d'ajout d'une cellule
   */
  void maj(final Sinavi3DataSimulation d) {

     
	  TableModel modele = new TableModel() {

		public int getRowCount() {
			return d.listeGare_.listeGares_.size();
		}

		public int getColumnCount() {
			return titreColonnes.length;
		}

		public String getColumnName(int columnIndex) {
			return titreColonnes[columnIndex];
		}

		public Class<?> getColumnClass(int columnIndex) {

			return String.class;
		}

		public boolean isCellEditable(int rowIndex, int columnIndex) {
			return true;
		}

		public Object getValueAt(int rowIndex, int columnIndex) {
			
			return d.listeGare_.retournerGare(rowIndex);
		}

		

		public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
			if(StringUtils.isBlank(aValue.toString())) {
				new BuDialogError(d.application_.getApp(), d.application_.isSinavi_, "Saisie manquante")
				.activate();
				return;
			}
			
				if(d.listeGare_.existeDoublon(aValue.toString(), rowIndex)) {
					new BuDialogError(d.application_.getApp(), d.application_.isSinavi_, "Nom deja pris.")
					.activate();
					
				}else
					d.listeGare_.modification(rowIndex, aValue.toString());
			
		}

		public void addTableModelListener(TableModelListener l) {
			// TODO Auto-generated method stub

		}

		public void removeTableModelListener(TableModelListener l) {
			// TODO Auto-generated method stub

		}

	};

	this.tableau_ = new BuTable(modele);
    
    

    tableau_.setBorder(this.borduretab);
    tableau_.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    tableau_.revalidate();
    this.removeAll();
    this.add(/* ascenceur */tableau_.getTableHeader(), BorderLayout.PAGE_START);
    this.add(tableau_, BorderLayout.CENTER);

    this.revalidate();
    this.updateUI();
  }

}
