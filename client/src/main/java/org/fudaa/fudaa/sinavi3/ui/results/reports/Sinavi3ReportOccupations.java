/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.sinavi3.ui.results.reports;

import java.util.ArrayList;
import java.util.List;
import jxl.format.Colour;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import org.fudaa.fudaa.commun.projet.FudaaProjet;
import org.fudaa.fudaa.sinavi3.Sinavi3DataSimulation;
import org.fudaa.fudaa.sinavi3.Sinavi3ResultatsAttenteTrajet;
import org.fudaa.fudaa.sinavi3.Sinavi3TraduitHoraires;

/**
 *
 * @author Adrien Hadoux
 */
public class Sinavi3ReportOccupations extends Sinavi3SheetFactoryHelper{
    
    
    
    

    public static void produceSheetAttentesTrajetsValues(Sinavi3DataSimulation data, WritableSheet sheet, int indiceLigne, 
            int ecluse ) throws WriteException {
        Object[][] results = null;
        //--sens avalant --//
         results = Sinavi3ResultatsAttenteTrajet.computeData(data, ecluse, 1, ecluse, 1, 0);
         Sinavi3ReportAttemtesTrajets.ValueAttente v = new Sinavi3ReportAttemtesTrajets.ValueAttente();
         //-- afficher la flotte --//
        v.nbBateauxTotalSens =0;
        for(int i=0;i<results.length;i++) {
            if(results[i][1] != null)
            v.nbBateauxTotalSens = v.nbBateauxTotalSens +getIntValue(results[i],1);
        } 
        setValueAt(indiceLigne, 5, sheet, v.nbBateauxTotalSens);
                        
        Object[] dataTotal = results[results.length-1];        
        
       //-- afficher valeurs attente occup --//
       
        v.nbNavires = getIntValue(dataTotal,14);
        v.totalAttente = dataTotal[12].toString();
        v.totalAttenteValue = Sinavi3TraduitHoraires.traduitReverseMinutes(v.totalAttente);
        v.moyenne = v.nbNavires != 0?v.totalAttenteValue /    (float)v.nbNavires:0;     
        v.moyenneFlotte = v.nbBateauxTotalSens != 0?v.totalAttenteValue /    (float)v.nbBateauxTotalSens:0;     
        v.moyennePercentFlotte = v.nbBateauxTotalSens != 0?(v.nbNavires /(float)v.nbBateauxTotalSens)*100.0f:0;     
        setValueAt(indiceLigne, 3, sheet, v.nbBateauxTotalSens);
        setValueDateAt(indiceLigne, 4, sheet, v.totalAttente);
        setValueAt(indiceLigne, 5, sheet, v.nbNavires);
        setValueDateAt(indiceLigne, 6, sheet, Sinavi3TraduitHoraires.traduitMinutesEnHeuresMinutes3(v.moyenne));
        setValueDateAt(indiceLigne, 7, sheet, Sinavi3TraduitHoraires.traduitMinutesEnHeuresMinutes3(v.moyenneFlotte));
        setValueAt(indiceLigne,8, sheet, v.moyennePercentFlotte);
        
        
        //montant
         results = Sinavi3ResultatsAttenteTrajet.computeData(data, ecluse, 1, ecluse, 1, 1);
         dataTotal = results[results.length-1];       
         Sinavi3ReportAttemtesTrajets.ValueAttente v2 = new Sinavi3ReportAttemtesTrajets.ValueAttente();
       v2.nbBateauxTotalSens = v.nbBateauxTotalSens;  
       v2.nbNavires = getIntValue(dataTotal,14);
       v2.totalAttente = dataTotal[12].toString();
       v2.totalAttenteValue = Sinavi3TraduitHoraires.traduitReverseMinutes(v2.totalAttente);
       v2.moyenne =v2.nbNavires != 0?v2.totalAttenteValue /    (float)v2.nbNavires:0;     
       v2.moyenneFlotte =v2.nbBateauxTotalSens != 0?v.totalAttenteValue /    (float)v2.nbBateauxTotalSens:0;     
       v2.moyennePercentFlotte =v2.nbBateauxTotalSens != 0?(v2.nbNavires /(float)v2.nbBateauxTotalSens)*100.0f:0;     
        setValueAt(indiceLigne, 10, sheet,v2.nbBateauxTotalSens);
        setValueDateAt(indiceLigne, 11, sheet,v2.totalAttente);
        setValueAt(indiceLigne, 12, sheet,v2.nbNavires);
        setValueDateAt(indiceLigne, 13, sheet, Sinavi3TraduitHoraires.traduitMinutesEnHeuresMinutes3(v2.moyenne));
        setValueDateAt(indiceLigne, 14, sheet, Sinavi3TraduitHoraires.traduitMinutesEnHeuresMinutes3(v2.moyenneFlotte));
        setValueAt(indiceLigne,15, sheet,v2.moyennePercentFlotte);
        
        
        //-- calcul moyen
        Sinavi3ReportAttemtesTrajets.ValueAttente   vmoy = Sinavi3ReportAttemtesTrajets.compute2sens(v, v2);
       
        setValueDateAt(indiceLigne, 17, sheet, Sinavi3TraduitHoraires.traduitMinutesEnHeuresMinutes3(vmoy.moyenne));
        setValueDateAt(indiceLigne, 18, sheet, Sinavi3TraduitHoraires.traduitMinutesEnHeuresMinutes3(vmoy.moyenneFlotte));
      

    }
    
    
   
    


    public static void produceSheetAttentesTrajetsHeader(WritableSheet sheet, boolean multiSimulation, Sinavi3DataSimulation data) throws WriteException {
        // taille de la colonne
        sheet.setColumnView(0, 30);
        sheet.setColumnView(1, 20);
        sheet.setColumnView(2, 5);
        sheet.setColumnView(9, 5);
        sheet.setColumnView(16, 5);
         sheet.setRowView(6, 38*20);
         sheet.setRowView(1, 38*20);
        setValueTitleAt(0, 0, sheet, "Attente occupation", Colour.WHITE);
        if (!multiSimulation) {
            setValueTitleAt(1, 0, sheet, " R�sultats d'un seul sc�nario de simulation", Colour.WHITE);
            setValueTitleAt(5, 0, sheet, "Sc�nario", Colour.TURQOISE2);
            fusionneCells(5, 0, 2, false, sheet);
        } else {
            setValueTitleAt(1, 0, sheet, "R�sultats de plusieurs sc�narios de simulation", Colour.WHITE);
            setValueTitleAt(5, 0, sheet, "Sc�nario", Colour.TURQOISE2);
            
        }

        setValueTitleAt(5, 1, sheet, "Ecluse", Colour.LIGHT_GREEN);
        fusionneCells(5, 1, 2, false, sheet);
       
        
        setValueTitleAt(5, 3, sheet, "Avalant", Colour.LIGHT_ORANGE);
        fusionneCells(5, 3, 6, true, sheet);
        setValueTitleAt(6, 3, sheet, "Flotte");
        setValueTitleAt(6, 4, sheet, "Total(h:min)");
        setValueTitleAt(6, 5, sheet, "Nb Bat");
        setValueTitleAt(6, 6, sheet, "Moy. / Nb Bat");
        setValueTitleAt(6, 7, sheet, "Moy. / Flotte");
        setValueTitleAt(6, 8, sheet, "% Nb bat / flotte");
        
        setValueTitleAt(5, 10, sheet, "Montant", Colour.LIGHT_ORANGE);
        fusionneCells(5, 10, 6, true, sheet);
        setValueTitleAt(6, 10, sheet, "Flotte");
        setValueTitleAt(6, 11, sheet, "Total(h:min)");
        setValueTitleAt(6, 12, sheet, "Nb Bat");
        setValueTitleAt(6, 13, sheet, "Moy. / Nb Bat");
        setValueTitleAt(6, 14, sheet, "Moy. / Flotte");
        setValueTitleAt(6, 15, sheet, "% Nb bat / flotte");
        
         setValueTitleAt(5, 17, sheet, "Moyenne / 2 sens", Colour.LIGHT_ORANGE);
        fusionneCells(5, 17, 2, true, sheet);
        setValueTitleAt(6, 17, sheet, "Moy. / Nb Bat");
        setValueTitleAt(6, 18, sheet, "Moy. / Flotte");
       
    }

    public static WritableSheet produceSheetOccup(final WritableWorkbook classeur, final Sinavi3DataSimulation data, boolean multiSimulation) throws WriteException {
        WritableSheet sheet = classeur.createSheet("Occupations", 0);

        produceSheetAttentesTrajetsHeader(sheet, multiSimulation, data);
        List<Sinavi3DataSimulation> listeComparison = new ArrayList<Sinavi3DataSimulation>();
        if (multiSimulation) {
            for (FudaaProjet projet : data.getApplication().getSimulations()) {
                if (data.getProjet_() != projet) {
                    Sinavi3DataSimulation comparison = getSimulationFromFudaaProjet(projet);
                    listeComparison.add(comparison);
                }
            }
        }

        //-- calculs avalant--//
        int indiceLigne = 8;       

        for (int tronconE = 0; tronconE < data.getListeEcluse().getListeEcluses().size(); tronconE++) {
           
                setValueAt(indiceLigne, 1, sheet, data.getListeEcluse().retournerEcluse(tronconE).getName());
               produceSheetAttentesTrajetsValues(data, sheet, indiceLigne, tronconE);
               
                if (multiSimulation) {
                    fusionneCells(indiceLigne, 1, (listeComparison.size()+1)*2 -1, false, sheet);
                     setValueTitleAt(indiceLigne, 0, sheet, "Sc�nario de r�f�rence", Colour.WHITE);
                   indiceLigne+=2;
                    for (Sinavi3DataSimulation comparison : listeComparison) {
                        setValueTitleAt(indiceLigne, 0, sheet, "Sc�nario " + comparison.getProjectName_(), Colour.WHITE);
                        produceSheetAttentesTrajetsValues(comparison, sheet, indiceLigne, tronconE);
                        indiceLigne+=2;
                    }
                }else 
                   {
                indiceLigne+=2;
            }
            
        }
        if (!multiSimulation) {
            setValueTitleAt(7, 0, sheet, "Sc�nario de r�f�rence", Colour.WHITE);
            fusionneCells(7, 0, indiceLigne - 8, false, sheet);
        }
        return sheet;
    }

    
}
