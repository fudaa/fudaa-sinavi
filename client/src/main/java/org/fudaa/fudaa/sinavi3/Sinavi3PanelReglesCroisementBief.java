/**
 *@creation 6 oct. 06
 *@modification $Dates$
 *@license  GNU General Public Licence 2
 *@copyright   (c)1998-2006 CETMEF 2 bd Gambetta F -60231 Compiegne
 *@mail   devel@fudaa.fr 
 */
package org.fudaa.fudaa.sinavi3;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.Observable;
import java.util.Observer;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.Border;

import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.table.CtuluTableExcelWriter;
import org.fudaa.fudaa.ressource.FudaaResource;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuDialogChoice;
import com.memoire.bu.BuDialogError;
import com.memoire.bu.BuDialogMessage;
import com.memoire.bu.BuTable;
import com.memoire.fu.FuLog;

/**
 * @version $Version$
 * @author hadoux
 */
public class Sinavi3PanelReglesCroisementBief extends Sinavi3InternalFrame implements Observer{

  /**
   * JCombo qui permettra de choisir pour chaque tron�on si les navires sont autoris�s a se croiser
   */
  JComboBox ComboChoix_;

  /**
   * ComboBox des cheneaux
   */
  JComboBox comboChoixBief_ = new JComboBox();

  /**
   * Tableau contenant les donn�es du tableau affich� en java (definition attribut car sert pour transformation excel)
   */
  Object[][] ndata;

  /**
   * Descriptif des elements des colonnes
   */
  String[] titreColonnes_;

  /**
   * Tableau de type JTable qui contiendra les donnees des navires
   */

  BuTable tableau_;

  /**
   * Nombre d'objets
   */
  int nbElem_;

  /**
   * Bouton de validation des regles de navigations.
   */

  final BuButton validation_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_oui"), "valider");
  private final BuButton duplication_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_ranger"), "dupliquer");
  private final BuButton impression_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_generer"), "Excel");
  private final BuButton oui_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_ajouter"), "Oui");
  private final BuButton non_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_enlever"), "Non");
  private final BuButton quitter_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_quitter"), "");

  /**
   * Panel qui contiendra le tableau.
   */
  JPanel global_ = new JPanel();

  /**
   * panel de controle
   */
  JPanel controlPanel = new JPanel();
  /**
   * Bordure du tableau.
   */

  Border borduretab_ = BorderFactory.createLoweredBevelBorder();

  /**
   * donn�es de la simulation
   */
  Sinavi3DataSimulation donnees_;

  /**
   * Indice du bief choisi (par defaut 0 => le premier bief.
   */
  int biefChoisi_ = 0;

  /**
   * constructeur du panel d'affichage des bassins.
   * 
   * @param d donn�es de la simulation.
   */
  Sinavi3PanelReglesCroisementBief(final Sinavi3DataSimulation _donnees) {

    super("", true, true, true, true);

    donnees_ = _donnees;
	 donnees_.addObservers(this);
    global_.setLayout(new BorderLayout());

    /**
     * Initialisation du nombre de cheneaux.
     */

    this.remplissage();
    // afichage des elements dans le tableau.
    this.affichage();

    oui_
        .setToolTipText("Transforme la zone selectionnee a la souris en \"oui\". Transforme automatiquement les zones symetriques ");
    non_
        .setToolTipText("Transforme la zone selectionnee a la souris en \"non\". Transforme automatiquement les zones symetriques ");

    /**
     * Creation de la fenetre
     */

    setTitle("R�gles de croisement dans le tron�on " + donnees_.listeBief_.retournerBief(biefChoisi_).nom_);
    setSize(800, 400);
    setBorder(Sinavi3Bordures.compound_);
    getContentPane().setLayout(new BorderLayout());
    this.quitter_.setToolTipText("cliquez sur ce bouton pour fermer la sous fen�tre");

    //final JScrollPane ascenceur = new JScrollPane(global_);

    getContentPane().add(global_, BorderLayout.CENTER);

    // controlPanel.setLayout(new GridLayout(1,8));
    controlPanel.setBorder(this.borduretab_);
    controlPanel.add(quitter_);
    controlPanel.add(oui_);
    controlPanel.add(non_);

    controlPanel.add(new JLabel("Tron�on:"));
    controlPanel.add(this.comboChoixBief_);

    // controlPanel.add(new JLabel("validation des saisies:"));
    controlPanel.add(this.duplication_);
    controlPanel.add(this.impression_);

    controlPanel.add(validation_);

    getContentPane().add(controlPanel, BorderLayout.SOUTH);

    /**
     * Listener du comboChoix de tron�on: lors dde la selection du tron�on, on affiche (rafraichis) le tableau de choix
     * des regles de navigations pour ce tron�on
     */
    this.duplication_.setToolTipText("permet de dupliquer les regles de navigations avec celles d'un autre tron�on");
    this.impression_
        .setToolTipText("Genere un fichier excel du tableau. Attention, ce bouton ne genere que le tableau du tron�on affich�!!");

    quitter_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        dispose();
      }
    });

    this.impression_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        // generation sous forme d'un fichier excel:
        File fichier;
        // definition d un file chooser
        final JFileChooser fc = new JFileChooser();
        final int returnVal = fc.showOpenDialog(Sinavi3PanelReglesCroisementBief.this);

        if (returnVal == JFileChooser.APPROVE_OPTION) {
          fichier = fc.getSelectedFile();
          final File f = CtuluLibFile.appendExtensionIfNeeded(fichier, "xls");

          // on r�cupere l abstrct model du tableau contenant les donn�es

          /**
           * creation d un abstract model impl�mentant l'interface CtuluTableModelInterface
           */

          final Sinavi3ModeleExcel modele = new Sinavi3ModeleExcel();

          // creation du tableau des titres des colonnes en fonction de la fenetre d affichage desecluses
          modele.nomColonnes_ = titreColonnes_;

          /**
           * transformation du model du tableau deja rempli pour le nuoveau model cr�e
           */

          /**
           * recopiage des titres des colonnes
           */
          // initialisation de la taille de data
          modele.data_ = new Object[donnees_.listeBateaux_.listeNavires_.size() + 2][titreColonnes_.length];

          for (int i = 0; i < titreColonnes_.length; i++) {

            // ecriture des nom des colonnes:
            modele.data_[0][i] = titreColonnes_[i];

          }

          /**
           * recopiage des donn�es
           */
          for (int i = 0; i < donnees_.listeBateaux_.listeNavires_.size(); i++) {
            modele.data_[i + 2] = ndata[i];
          }

          modele.setNbRow(donnees_.listeBateaux_.listeNavires_.size() + 2);

          /**
           * on essaie d 'ecrire en format excel
           */
          final CtuluTableExcelWriter ecrivain = new CtuluTableExcelWriter(modele, f);

          try {
            ecrivain.write(null);

          } catch (final RowsExceededException _err) {
            FuLog.error(_err);
          } catch (final WriteException _err) {
            FuLog.error(_err);
          } catch (final IOException _err) {
            FuLog.error(_err);
          }

        }// fin du if si le composant est bon
      }

    });

    this.duplication_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        final String[] values = new String[donnees_.listeBief_.listeBiefs_.size()];

        for (int i = 0; i < donnees_.listeBief_.listeBiefs_.size(); i++) {
          values[i] = donnees_.listeBief_.retournerBief(i).nom_;
        }

        final BuDialogChoice choix = new BuDialogChoice(donnees_.application_.getApp(), Sinavi3Implementation.isSinavi_,
            "Choix du tron�on mod�le", "Le bief "
                + donnees_.listeBief_.retournerBief(biefChoisi_).nom_+" r�cup�re les r�gles du tron�on choisi ci-dessous", values);

        final int reponse = choix.activate();
        if (reponse != -1)

        {
          if (reponse == 0) {

            final int chenal = donnees_.listeBief_.retourneIndice(choix.getValue());
            if (chenal != -1) {
              donnees_.listeBief_.retournerBief(biefChoisi_).reglesCroisement_.duplication(donnees_.listeBief_
                  .retournerBief(chenal).reglesCroisement_.vecteurDeVecteur_);
/*
              new BuDialogMessage(donnees_.application_.getApp(), SiporImplementation.isSipor_,
                  "Le chenal utilise pour la duplication \nest le chenal: "
                      + donnees_.listeChenal_.retournerChenal(chenal).nom_).activate();*/
              // rafraichissement de la duplication
              affichage();
            }
          }

        } else {
          new BuDialogError(donnees_.application_.getApp(), Sinavi3Implementation.isSinavi_,
              "La duplication est impossible! le tron�on choisi n existe pas.").activate();
        }

      }
    });

    this.oui_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        final int[] tableauColonnes = tableau_.getSelectedColumns();
        final int[] tableauLignes = tableau_.getSelectedRows();
        if (tableauColonnes.length != 0 && tableauLignes.length != 0) {

          for (int k = 0; k < tableauLignes.length; k++) {
            for (int l = 0; l < tableauColonnes.length; l++) {
              // l oppos� de (i,j) est (j-1;i+1)
              final int i = tableauLignes[k];
              final int j = tableauColonnes[l];
              if (j != 0)// pour ne pas modifier al colonens des noms de cat�gories de navires
              {
                // ndata[i][j]="oui";
                // ndata[j-1][i+1]="oui";
                donnees_.listeBief_.retournerBief(biefChoisi_).reglesCroisement_.modification(true, i, j - 1);
                donnees_.listeBief_.retournerBief(biefChoisi_).reglesCroisement_.modification(true, j - 1, i);

              }

            }
          }

          // rafraichissement:
          affichage();
        }

      }
    });

    this.non_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        final int[] tableauColonnes = tableau_.getSelectedColumns();
        final int[] tableauLignes = tableau_.getSelectedRows();
        if (tableauColonnes.length != 0 && tableauLignes.length != 0) {

          for (int k = 0; k < tableauLignes.length; k++) {
            for (int l = 0; l < tableauColonnes.length; l++) {
              // l oppos� de (i,j) est (j-1;i+1)
              final int i = tableauLignes[k];
              final int j = tableauColonnes[l];
              if (j != 0)// pour ne pas modifier al colonens des noms de cat�gories de navires
              {
                // ndata[i][j]="non";
                // ndata[j-1][i+1]="non";
                donnees_.listeBief_.retournerBief(biefChoisi_).reglesCroisement_.modification(false, i, j - 1);
                donnees_.listeBief_.retournerBief(biefChoisi_).reglesCroisement_.modification(false, j - 1, i);

              }

            }
          }

          // rafraichissement:
          affichage();
        }
      }
    });

    this.comboChoixBief_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent ev) {
        // 1) enregistrement des modification poru le chanl en cours:
        miseAjourReglesNavigation();

        // 2) recuperation de l indice du tron�on choisi
        // et affectation dans la variable du tron�on choisi
        biefChoisi_ = comboChoixBief_.getSelectedIndex();
        setTitle("R�gles de croisement dans le tron�on "
            + donnees_.listeBief_.retournerBief(biefChoisi_).nom_);

        // 3) affichage(rafraichissement des donn�es)
        affichage();

      }

    });

    // listener du bouton de validation

    this.validation_.addActionListener(new ActionListener() {

      public void actionPerformed(final ActionEvent e) {

        miseAjourReglesNavigation();
        new BuDialogMessage(
            donnees_.application_.getApp(),
            Sinavi3Implementation.isSinavi_,
            "Les r�gles de navigation des tron�ons sont correctement saisies.\nAttention! Un ajout de cat�gorie de bateau implique qu'\nune ligne et une colonne suppl�mentaires\n� saisir apparaitront ici.\n(par defaut la valeur des cases correspondantes\nest \"oui\").")
            .activate();
        dispose();

      }

    });

    // affichage de la frame
    setVisible(true);

  }

  /**
   * Methode de remplissage des JComboBox et des donn�es par d�fauts pour chaque objet.
   */
  void remplissage() {

    // 1)cr�ation du combo
    this.ComboChoix_ = new JComboBox();

    // 3)remplissage du tableau de gare avec toutes les donn�es des gares

    this.ComboChoix_.addItem("oui");
    this.ComboChoix_.addItem("non");

    // 4) remplissage des cheneaux dansla jcomobobox correspondante

    this.comboChoixBief_.removeAllItems();

    // final String[] nomCheneaux = new String[this.donnees_.listebief_.listeChenaux_.size()];
    System.out.println("nb chenaux a remplir: "+this.donnees_.listeBief_.listeBiefs_.size());
    for (int i = 0; i < this.donnees_.listeBief_.listeBiefs_.size(); i++) {
      this.comboChoixBief_.addItem(this.donnees_.listeBief_.retournerBief(i).nom_);
    }
    this.comboChoixBief_.repaint();
    // nomCheneaux[i]=""+this.donnees_.listebief_.retournerbief(i).nom_;

    // creation de la jcombobox avec les donn�es des cheneaux
    // this.ComboChoixbief_=new JComboBox(nomCheneaux);

  }

  /**
   * Methode d affichage des composants du BuTable et du tableau de combo. Cette methode est a impl�menter dans les
   * classes d�riv�es pour chaque composants
   */
  void affichage() {

    final int nbNavires = donnees_.listeBateaux_.listeNavires_.size();

    // nombre de colonnes= nombre de navire + nom du navire
    ndata = new Object[nbNavires][nbNavires + 1];

    titreColonnes_ = new String[nbNavires + 1];

    // initialisation des titres
    titreColonnes_[0] = "Categories: ";
    for (int i = 0; i < nbNavires; i++) {
      titreColonnes_[i + 1] = donnees_.listeBateaux_.retournerNavire(i).nom;
    }

    /**
     * choix du remplissage: par defaut si le nombre de donnees regles de navigation du tron�on selectionn� vaut 0
     */

    System.out.println("***** nb navires: " + nbNavires);
    /** on initialise le tableau avec les donn�es charg�es */
    for (int i = 0; i < nbNavires; i++) {

      // this.donnees_.listebief_.retournerbief(this.biefChoisi_).reglesNavigation_
      // nom du navire
      ndata[i][0] = "  " + donnees_.listeBateaux_.retournerNavire(i).nom;
      for (int j = 0; j < nbNavires; j++) {
        /*
         * if(j>i) ndata[i][j+1]="X"; else {
         */
        // recuperation de la valeur de la case i,j
        if (this.donnees_.listeBief_.retournerBief(this.biefChoisi_).reglesCroisement_.retournerAij(i, j)
            .booleanValue()) {
          ndata[i][j + 1] = "oui";
        } else {
          ndata[i][j + 1] = "non";
        }

        // }
      }// fin du pourparcours des colonnes

    }

    this.tableau_ = new BuTable(ndata, this.titreColonnes_) {
      public boolean isCellEditable(final int row, final int col) {
        return false;
      }
    };
    // tableau_.setSelectionMode(selectionMode);

    tableau_.setRowSelectionAllowed(true);
    tableau_.setColumnSelectionAllowed(true);
    tableau_.setCellSelectionEnabled(true);

    // tableau_.setSelectionModel(newModel)
    tableau_.setBorder(this.borduretab_);
    org.fudaa.fudaa.sinavi.factory.ColumnAutoSizer.sizeColumnsToFit(tableau_);
    tableau_.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
    tableau_.revalidate();

    this.global_.removeAll();

    // this.removeAll();
    this.global_.add(new JScrollPane(tableau_), BorderLayout.CENTER);
    this.global_.add(/* ascenceur */tableau_.getTableHeader(), BorderLayout.NORTH);
    

    this.global_.revalidate();
    this.global_.updateUI();

    /*
     * JPanel controlePanel=new JPanel(); global_.add(controlePanel,BorderLayout.SOUTH); controlePanel.add(validation_);
     */

    /**
     * Creation pour les colonnes 2 a nbElem+1 d'objets de type jcombobox de choix "oui ou non"
     */
    // 1) definition d un tableau de modele de colonnes
    // TableColumn[] choix_oui_non_Column=new TableColumn[nbNavires+1];
    // 2) transformation de chacnu de ces types en JcomboBox de choix booleens:
    /*
     * for(int i=1;i<nbNavires+1;i++) { choix_oui_non_Column[i]=tableau_.getColumnModel().getColumn(i);
     * choix_oui_non_Column[i].setCellEditor(new DefaultCellEditor(this.ComboChoix_)); }
     */

    this.revalidate();
    this.updateUI();

  }

  

  void miseAjourReglesNavigation() {

    final int nbNavires = donnees_.listeBateaux_.listeNavires_.size();

    /**
     * premiere etape on v�rifie la coh�rence des donn�es saisies:
     */

    System.out.println("Yes les donnees sont validees : jugee coherentes");

    /**
     * Initialisation du tableau de regles de navigations du bief concern�
     */
    // this.donnees_.listebief_.retournerbief(this.biefChoisi_).nbDonneesReglesNavigation=nbElem_;
    // allocation de memoire de la matrice de donn�es
    // this.donnees_.listebief_.retournerbief(this.biefChoisi_).reglesNavigation_=new boolean[nbElem_][nbElem_];
    /**
     * MODIFICAITON DES Regles de Navigations POUR le bief choisi:
     */
    for (int i = 0; i < nbNavires; i++) {
      // affichage de chacune des donn�es
      System.out.println("");

      for (int j = 0; j <= i; j++) {
        // recuperer une donn�e
        final String nomchoix = (String) this.tableau_.getModel().getValueAt(i, j + 1);
        System.out.print("tableau[" + i + "][" + j + "]= " + nomchoix);

        /**
         * ajout du choix saisies dans le tableau de regles de navigation du bief choisi
         */
        if (nomchoix.equals("non")) {
          this.donnees_.listeBief_.retournerBief(this.biefChoisi_).reglesCroisement_.modification(false, i, j);
          this.donnees_.listeBief_.retournerBief(this.biefChoisi_).reglesCroisement_.modification(false, j, i);
        } else {
          this.donnees_.listeBief_.retournerBief(this.biefChoisi_).reglesCroisement_.modification(true, i, j);
          this.donnees_.listeBief_.retournerBief(this.biefChoisi_).reglesCroisement_.modification(true, j, i);
        }
        // this.donnees_.listebief_.retournerbief(this.biefChoisi_).reglesNavigation_[i][j]=false;

      }// fin du for parcours de chaque colonnes

    }

  }// fin du if ajout des donn�es

public void update(Observable o, Object arg) {
	// TODO Auto-generated method stub
	if(arg.equals("bief")||arg.equals("navire")){
		
		//affichage();
	}
}



}
