/**
 *@creation 6 oct. 06
 *@modification $Dates$
 *@license  GNU General Public Licence 2
 *@copyright   (c)1998-2006 CETMEF 2 bd Gambetta F -60231 Compiegne
 *@mail   devel@fudaa.fr 
 */
package org.fudaa.fudaa.sinavi3;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.Observable;
import java.util.Observer;

import javax.swing.BorderFactory;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.Border;

import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.table.CtuluTableExcelWriter;
import org.fudaa.fudaa.ressource.FudaaResource;
import org.fudaa.fudaa.sinavi.factory.ColumnAutoSizer;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuDialogMessage;
import com.memoire.bu.BuTable;
import com.memoire.fu.FuLog;

/**
 * @version $Version$
 * @author hadoux
 */
public class Sinavi3PanelVitessesBief extends Sinavi3InternalFrame  {

  /**
   * Tableau contenant les donn�es du tableau affich� en java (definition attribut car sert pour transformation excel)
   */
  Object[][] ndata;

  /**
   * Descriptif des elements des colonnes
   */
  String[] titreColonnes_;

  /**
   * Tableau de type JTable qui contiendra les donnees des navires
   */

  BuTable tableau_;

  /**
   * Modele qui contient la partie metier des donn�es.
   * Utilisee comme mod�le du tableau
   */
  Sinavi3ModeleVitesseBiefAvalant modeleTableauAvalant_;
  Sinavi3ModeleVitesseBiefMontant modeleTableauMontant_;
  JComboBox sens_=new JComboBox(new String[]{"Avalant","Montant"});
  /**
   * Nombre d'objets
   */
  int nbElem_;

  /**
   * Bouton de validation des regles de navigations.
   */

 // final BuButton validation_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_oui"), "valider");
  private final BuButton impression_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_generer"), "Excel");
  private final BuButton valider_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_oui"), "quitter");
  
  
  /**
   * Panel qui contiendra le tableau.
   */
  JPanel global_ = new JPanel();

  /**
   * panel de controle
   */
  JPanel controlPanel = new JPanel();
  /**
   * Bordure du tableau.
   */

  Border borduretab_ = BorderFactory.createLoweredBevelBorder();

  /**
   * donn�es de la simulation
   */
  Sinavi3DataSimulation donnees_;

  /**
   * Indice du chenal choisi (par defaut 0 => le premier chenal.
   */
  int chenalChoisi_ = 0;

  /**
   * constructeur du panel d'affichage des bassins.
   * 
   * @param d donn�es de la simulation.
   */
  Sinavi3PanelVitessesBief(final Sinavi3DataSimulation _donnees) {

    super("", true, true, true, true);

    donnees_ = _donnees;
    
    //-- Creation du modele du tableau; par d�faut le modele avalant --//
    modeleTableauAvalant_=new Sinavi3ModeleVitesseBiefAvalant(donnees_);
    modeleTableauMontant_=new Sinavi3ModeleVitesseBiefMontant(donnees_);
    global_.setLayout(new BorderLayout());

    /**
     * Initialisation du nombre de cheneaux.
     */

    this.affichage();

    /**
     * Creation de la fenetre
     */

    setTitle("Vitesses des bateaux dans les tron�ons Avalants (en KM/H) ");
    setSize(800, 400);
    setBorder(Sinavi3Bordures.compound_);
    getContentPane().setLayout(new BorderLayout());
    this.valider_.setToolTipText("cliquez sur ce bouton pour fermer la sous fen�tre");

   
    getContentPane().add(global_, BorderLayout.CENTER);

    controlPanel.setBorder(this.borduretab_);
    controlPanel.add(valider_);
    controlPanel.add(this.impression_);
    controlPanel.add(new JLabel("Choisissez le sens pour les vitesses:"));
    controlPanel.add(sens_);
    

    getContentPane().add(controlPanel, BorderLayout.SOUTH);

    /**
     * Listener du comboChoix de chenal: lors dde la selection du chenal, on affiche (rafraichis) le tableau de choix
     * des regles de navigations pour ce chenal
     */
    this.impression_
        .setToolTipText("Genere un fichier excel du tableau. Attention, ce bouton ne genere que le tableau du tron�on affich�!!");

    sens_.setToolTipText("Permet de saisir les vitesses selon le sens avalant montant.");
    valider_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        dispose();
      }
    });

    this.impression_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        tableau_.editCellAt(0, 0);
        // generation sous forme d'un fichier excel:
        File fichier;
        // definition d un file chooser
        final JFileChooser fc = new JFileChooser();
        final int returnVal = fc.showOpenDialog(Sinavi3PanelVitessesBief.this);

        if (returnVal == JFileChooser.APPROVE_OPTION) {
          fichier = fc.getSelectedFile();
          final File f = CtuluLibFile.appendExtensionIfNeeded(fichier, "xls");

          // on r�cupere l abstrct model du tableau contenant les donn�es

          /**
           * creation d un abstract model impl�mentant l'interface CtuluTableModelInterface
           */
          final Sinavi3ModeleExcel modele;
          if(sens_.getSelectedIndex()==0)
        	  modele= modeleTableauAvalant_;
          else
        	  modele= modeleTableauMontant_;
          
          /**
           * on essaie d 'ecrire en format excel
           */
          final CtuluTableExcelWriter ecrivain = new CtuluTableExcelWriter(modele, f);

          try {
            ecrivain.write(null);
            new BuDialogMessage(donnees_.application_.getApp(), donnees_.application_.isSinavi_, 
            		"Fichier Excel g�n�r� avec succ�s.").activate();

          } catch (final RowsExceededException _err) {
            FuLog.error(_err);
          } catch (final WriteException _err) {
            FuLog.error(_err);
          } catch (final IOException _err) {
            FuLog.error(_err);
          }

        }// fin du if si le composant est bon
      }

    });

//  -- mise a jour du contenu du tableau via la methode affichage qui affiche le contenu du tableau a partir du non modele montant ou avalant
	this.sens_.addActionListener(new ActionListener(){

		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			if(sens_.getSelectedIndex()==0)
			{
				//cas avalant
				setTitle("Vitesses des bateaux dans les tron�ons Avalants (en KM/H) ");
				tableau_.setModel(modeleTableauAvalant_);
				modeleTableauAvalant_.fireTableStructureChanged();
				ColumnAutoSizer.sizeColumnsToFit(tableau_);
				tableau_.validate();
				   // tableau_.updateUI();
			}
			else
			{
				setTitle("Vitesses des bateaux dans les tron�ons Montants (en KM/H) ");
				tableau_.setModel(modeleTableauMontant_);
				ColumnAutoSizer.sizeColumnsToFit(tableau_);
			}
			
			 
		}
    	
    });
    
    // affichage de la frame
    setVisible(true);

  }

  /**
   * Methode de remplissage des JComboBox et des donn�es par d�fauts pour chaque objet.
   */
  void remplissage() {}

  /**
   * Methode d affichage des composants du BuTable et du tableau de combo Cette methode est a impl�menter dans les
   * classes d�riv�es pour chaque composants
   */
  void affichage() {

    
    // --creation du tableau avec l'obejt modele qui g�re la liaison entre utilisateur et partie m�tier --//
	  tableau_= new BuTable(modeleTableauAvalant_);
	 
    //-- editeur par d�faut du tableau --// 
    tableau_.setDefaultEditor(Object.class,new Sinavi3CellEditor());
  
    this.global_.add(/* ascenceur */tableau_.getTableHeader(), BorderLayout.PAGE_START);
    this.global_.add(new JScrollPane(tableau_), BorderLayout.CENTER);
    ColumnAutoSizer.sizeColumnsToFit(tableau_);
    tableau_.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
    this.global_.revalidate();
    this.global_.updateUI();
    
    this.revalidate();
    this.updateUI();

  }



  

}
