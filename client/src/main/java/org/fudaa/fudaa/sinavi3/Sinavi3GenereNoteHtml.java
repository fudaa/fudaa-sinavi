package org.fudaa.fudaa.sinavi3;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.image.CtuluImageExport;

import com.memoire.bu.BuDialogError;

/**
 * Classe qui permet de g�n�rer un fichier HTML
 * par exemple un rappel des donn�es
 *@version $Version$
 * @author hadoux
 *
 */

public class Sinavi3GenereNoteHtml {

		
	
public static void rappelDonnees(File _fichier, Sinavi3FrameGenerationRappelDonnees _f,Sinavi3DataSimulation _d)	
{
	//chaine contenant le rapport 
	String rapport="";
	
	//indice qui correponda � l'indice de la partie
	int indicePartie=1;
	int chapitre=1;
	
	
	
	//creation d'un titre
	rapport+=creertitre(_f.titre_.getText(),_f.auteur_.getText(),_f.ZoneText_.getText());
	
	
	//creation du sommaire
	rapport+=creerSommaire(_f);
	
	
	
	
	
	rapport+="<br><br><h2>1. Les param�tres</h2><br><br>";
	//insertion des donn�es generales
	if(_f.dg_.isSelected())
	{
		//ecriture du tableau des chenaux:
		rapport+=creerPartie(1,chapitre++,indicePartie++,". Donn�es g�n�rales");
		rapport+=creerDonneesgenerales(_d, _f);
	}
//	insertion des gares
	if(_f.gare_.isSelected())
	{
		//ecriture du tableau des chenaux:
		rapport+=creerPartie(1,chapitre++,indicePartie++,". Gares");
		rapport+=creerTableauGares(_d,_f);
	}

	
	//insertion des chenaux 
	if(_f.chenal_.isSelected())
	{
		//ecriture du tableau des chenaux:
		rapport+=creerPartie(1,chapitre++,indicePartie++,". Tron�ons");
		rapport+=creerTableauChenaux(_d,_f);
	}
	
	
	
	
	if(_f.ecluse_.isSelected())
	{
		//ecriture du tableau des cercles d evitage:
		rapport+=creerPartie(1,chapitre++,indicePartie++,". Ecluses");
		rapport+=creerTableauEcluses(_d,_f);
	}
	
	
	
	
	
	if(_f.nav_.isSelected())
	{
		//ecriture du tableau des cercles d evitage:
		rapport+=creerPartie(1,chapitre++,indicePartie++,". Cat�gories de bateaux");
		rapport+=creerTableauCategorie(_d,_f);
	}
	
	// leeeeeeeeeessssssssssss topologies
	if((_f.topo2_.isSelected() ||_f.topo4_.isSelected() ||_f.topo5_.isSelected()))
	 rapport+="<br><br><h2>2. La topologie</h2><br><br>";
	
	
	chapitre=1;
	
	if(_f.topo2_.isSelected())
	{
		rapport+=creerPartie(2,chapitre++,indicePartie++,". Topologie des tron�ons");
		rapport+=creerTableauTopologieChenaux(_d,_f);
	
	}
	
	if(_f.topo4_.isSelected())
	{
		rapport+=creerPartie(2,chapitre++,indicePartie++,". Topologie des �cluses");
		rapport+=creerTableauTopologieEcluses(_d,_f);
	
	}
	if(_f.topo5_.isSelected())
	{
		rapport+=creerPartie(2,chapitre++,indicePartie++,". Mod�lisation du r�seau");
		/*
		new BuDialogMessage(_d.application_.getApp(),_d.application_.isSipor_,
				   "Veuillez specifier l'image du port � exporter:")
.activate();
*/
	    rapport+=creerModele(_d,_f,_fichier.getAbsolutePath());
	}
	
	// les regles de navigations
	
	
	int partie=3;
	if(_f.topo2_.isSelected() ||_f.topo4_.isSelected() ||_f.topo5_.isSelected())
	partie=3;
	else partie=2;
	
	if((_f.regle1_.isSelected() ||_f.regle2_.isSelected() ||_f.regle3_.isSelected() ))
		rapport+="<br><br><h2>"+partie+". Les r�gles de navigation</h2><br><br>";
		
	chapitre=1;
	
	if(_f.regle1_.isSelected())
	{
		rapport+=creerPartie(partie,chapitre++,indicePartie++,". R�gles de croisement dans les tron�ons");
		rapport+=creerTableauCroisementChenaux(_d,_f);
	
	}
	
	
	if(_f.regle3_.isSelected())
	{
		rapport+=creerPartie(partie,chapitre++,indicePartie++,". Vitesse des bateaux dans les tron�ons (km/h)");
		rapport+=creerTableauDureeChenaux(_d,_f);
		
		rapport+=creerPartie(partie,chapitre++,indicePartie++,"d�lais de remplissage des SAS (h.min)");
	    rapport+=creerTableauRemplissageSAS(_d,_f);
	}
	
	
	
	//ecriture du contenu du rapport g�n�r�
	 
     try {
    	 File file =CtuluLibFile.appendExtensionIfNeeded(_fichier, "html");
       final FileWriter fw = new FileWriter(file);
       fw.write(rapport, 0, rapport.length());
       fw.flush();
       fw.close();
     } catch (final IOException _e1) {
    	 new BuDialogError(_d.application_.getApp(),_d.application_.isSinavi_,
    			 "Impossible d'�crire le fichier sur le disque.")
			.activate();
      
    	 return ;
     }
     
     //afficher el rapport dans l editeur
     _f.editeur.setDocumentText(rapport);
     
}


public static void apercuRapport(Sinavi3FrameGenerationRappelDonnees _f,Sinavi3DataSimulation _d)	
{
	//chaine contenant le rapport 
	String rapport="";
	
	//indice qui correponda � l'indice de la partie
	int indicePartie=1;
	int chapitre=1;
	
	
	//creation d'un titre
	rapport+=creertitre(_f.titre_.getText(),_f.auteur_.getText(),_f.ZoneText_.getText());
	
	
	//creation du sommaire
	rapport+=creerSommaire(_f);
	
	
	
	
	
	rapport+="<br><br><h2>1. Les param�tres</h2><br><br>";
	//insertion des donn�es generales
	if(_f.dg_.isSelected())
	{
		//ecriture du tableau des chenaux:
		rapport+=creerPartie(1,chapitre++,indicePartie++,"Donn�es g�n�rales");
		rapport+=creerDonneesgenerales(_d, _f);
	}
//	insertion des gares
	if(_f.gare_.isSelected())
	{
		//ecriture du tableau des chenaux:
		rapport+=creerPartie(1,chapitre++,indicePartie++,"Gares");
		rapport+=creerTableauGares(_d,_f);
	}

	
	//insertion des chenaux 
	if(_f.chenal_.isSelected())
	{
		//ecriture du tableau des chenaux:
		rapport+=creerPartie(1,chapitre++,indicePartie++,"Tron�ons");
		rapport+=creerTableauChenaux(_d,_f);
	}
	
	
	
	
	if(_f.ecluse_.isSelected())
	{
		//ecriture du tableau des cercles d evitage:
		rapport+=creerPartie(1,chapitre++,indicePartie++,"Ecluses");
		rapport+=creerTableauEcluses(_d,_f);
	}
	
	
	
	
	
	if(_f.nav_.isSelected())
	{
		//ecriture du tableau des cercles d evitage:
		rapport+=creerPartie(1,chapitre++,indicePartie++,"Cat�gories de bateaux");
		rapport+=creerTableauCategorie(_d,_f);
	}
	
	// leeeeeeeeeessssssssssss topologies
	if((_f.topo2_.isSelected()||_f.topo4_.isSelected() ||_f.topo5_.isSelected()))
	 rapport+="<br><br><h2>2. La topologie</h2><br><br>";
	
	
	chapitre=1;
	
	if(_f.topo2_.isSelected())
	{
		rapport+=creerPartie(2,chapitre++,indicePartie++,"Topologie des tron�ons");
		rapport+=creerTableauTopologieChenaux(_d,_f);
	
	}
	
	if(_f.topo4_.isSelected())
	{
		rapport+=creerPartie(2,chapitre++,indicePartie++,"Topologie des �cluses");
		rapport+=creerTableauTopologieEcluses(_d,_f);
	
	}
	if(_f.topo5_.isSelected())
	{
		rapport+=creerPartie(2,chapitre++,indicePartie++,"Mod�lisation du r�seau");
		/*
		new BuDialogMessage(_d.application_.getApp(),_d.application_.isSipor_,
				   "Veuillez specifier l'image du port � exporter:")
.activate();
*/
		_f.fichierImagePort_=_d.projet_.getFichier()+".jpg";
		
		rapport+=creerModele(_d,_f,_f.fichierImagePort_);
	    //rapport+="<br /><br /> Aper�u non disponible en mode aper�u, il faut exporter le rapport pour cr�er le fichier image du sch�ma du port";
	}
	
	// les regles de navigations
	
	
	int partie=3;
	if(_f.topo2_.isSelected()  ||_f.topo4_.isSelected() ||_f.topo5_.isSelected())
	partie=3;
	else partie=2;
	
	if((_f.regle1_.isSelected() ||_f.regle2_.isSelected() ||_f.regle3_.isSelected() ))
		rapport+="<br><br><h2>"+partie+". Les r�gles de navigation</h2><br><br>";
		
	chapitre=1;
	
	if(_f.regle1_.isSelected())
	{
		rapport+=creerPartie(partie,chapitre++,indicePartie++,"R�gles de croisement dans les tron�ons");
		rapport+=creerTableauCroisementChenaux(_d,_f);
	
	}
	if(_f.regle2_.isSelected())
	{
		rapport+=creerPartie(partie,chapitre++,indicePartie++,"R�gles de tr�matage dand les tron�ons");
		rapport+=creerTableauTrematagesChenaux(_d,_f);
	
	}
	
	
	if(_f.regle3_.isSelected())
	{
		rapport+=creerPartie(partie,chapitre++,indicePartie++,"Vitesse des bateaux dans les tron�ons");
		rapport+=creerTableauDureeChenaux(_d,_f);
		rapport+=creerTableauDureeChenauxMontant(_d,_f);
		
		rapport+=creerPartie(partie,chapitre++,indicePartie++,"Dur�e des manoeuvres dans les �cluses");
		rapport+=creerTableauDureeManEcluseEntrant(_d,_f);
		rapport+=creerTableauDureeManEcluseSortant(_d,_f);
		
		 rapport+=creerPartie(partie,chapitre++,indicePartie++,"d�lais de remplissage des SAS (h.min)");
		 rapport+=creerTableauRemplissageSAS(_d,_f);
		
		
	
	}
	
	//-- les trajets des bateaux --//
	rapport+=creerPartie(partie,chapitre++,indicePartie++,"Trajet des bateaux");
	rapport+=creerTableauTrajet(_d,_f);
	
	
     
     //afficher el rapport dans l editeur
     _f.editeur.setDocumentText(rapport);
     
}



public static String creerSommaire(Sinavi3FrameGenerationRappelDonnees _f)
{
	String sommaire="";
	int num=1;
	
	sommaire+="<div align=\"center\">";
	sommaire+="<table border=\"2\" style=\"border-collapse:collapse\" width=\"75%\"><tr><td>";
	sommaire+="<table width=\"100%\" border=0><th rowspan=\"2\" ><H2><font color=\"#000000\">SOMMAIRE</font></H1></th><tr></tr>";

	
	sommaire+="<tr><td ><font color=\"#000000\"><h3>1. Les param�tres</h3></font></td></tr>";
	
	
	if(_f.dg_.isSelected())
	sommaire+="<tr><td bgcolor=\"AACCEE\"><font color=\"#000000\"><a href=\"#index"+(num)+"\">1."+(num++)+"<font color=\"#000000\">. Donn�es g�n�rales</font></a></font></td></tr>";
	
	if(_f.gare_.isSelected())
		sommaire+="<tr><td bgcolor=\"AACCEE\"><font color=\"#000000\"><a href=\"#index"+(num)+"\">1."+(num++)+"<font color=\"#000000\">. Gares</font></a></font></td></tr>";
		
	if(_f.chenal_.isSelected())
		sommaire+="<tr><td bgcolor=\"AACCEE\"><font color=\"#000000\"><a href=\"#index"+(num)+"\">1."+(num++)+"<font color=\"#000000\">. Rron�ons</font></a></font></td></tr>";
		
	if(_f.ecluse_.isSelected())
		sommaire+="<tr><td bgcolor=\"AACCEE\"><font color=\"#000000\"><a href=\"#index"+(num)+"\">1."+(num++)+"<font color=\"#000000\">. Ecluses</font></a></font></td></tr>";
		    
	if(_f.nav_.isSelected())
		sommaire+="<tr><td bgcolor=\"AACCEE\"><font color=\"#000000\"><a href=\"#index"+(num)+"\">1."+(num++)+"<font color=\"#000000\">. Cat�gories de bateaux</font></a></font></td></tr>";
	
	int partie=3;
	if((_f.topo2_.isSelected()  ||_f.topo4_.isSelected() ||_f.topo5_.isSelected()))
	sommaire+="<tr><td><br><font color=\"#000000\"><h3>2. La topologie</h3></font></td></tr>";
	else
		partie=2;
	
	int num2=1;
	if(_f.topo2_.isSelected())
		sommaire+="<tr><td bgcolor=\"AACCEE\"><font color=\"#000000\"><a href=\"#index"+(num++)+"\">2."+(num2++)+"<font color=\"#000000\">. Topologie des tron�ons</font></a></font></td></tr>";
	if(_f.topo4_.isSelected())
		sommaire+="<tr><td bgcolor=\"AACCEE\"><font color=\"#000000\"><a href=\"#index"+(num++)+"\">2."+(num2++)+"<font color=\"#000000\">. Topologie des �cluses</font></a></font></td></tr>";
	if(_f.topo5_.isSelected())
		sommaire+="<tr><td bgcolor=\"AACCEE\"><font color=\"#000000\"><a href=\"#index"+(num++)+"\">2."+(num2++)+"<font color=\"#000000\">. Mod�lisation du r�seau</font></a></font></td></tr>";
	
	
	if((_f.regle1_.isSelected() ||_f.regle2_.isSelected() ||_f.regle3_.isSelected() ))
		sommaire+="<tr><td><br><font color=\"#000000\"><h3>"+partie+". Les r�gles de navigation</h3></font></td></tr>";
	
	
	num2=1;
	if(_f.regle1_.isSelected())
		sommaire+="<tr><td bgcolor=\"AACCEE\"><font color=\"#000000\"><a href=\"#index"+(num++)+"\">"+partie+"."+(num2++)+"<font color=\"#000000\">. Croisement dans les tron�ons</font></a></font></td></tr>";
	if(_f.regle2_.isSelected())
		sommaire+="<tr><td bgcolor=\"AACCEE\"><font color=\"#000000\"><a href=\"#index"+(num++)+"\">"+partie+"."+(num2++)+"<font color=\"#000000\">. Tr�matage dans les tron�ons</font></a></font></td></tr>";
	if(_f.regle3_.isSelected())
	{
		sommaire+="<tr><td bgcolor=\"AACCEE\"><font color=\"#000000\"><a href=\"#index"+(num++)+"\">"+partie+"."+(num2++)+"<font color=\"#000000\">. Vitesse des bateaux dans les tron�ons</font></a></font></td></tr>";
	}
	if(_f.regle4_.isSelected())
	{
		sommaire+="<tr><td bgcolor=\"AACCEE\"><font color=\"#000000\"><a href=\"#index"+(num++)+"\">"+partie+"."+(num2++)+"<font color=\"#000000\">. Dur�e des manoeuvres dans les �cluses</font></a></font></td></tr>";
	}
	
	sommaire+="<tr><td bgcolor=\"AACCEE\"><font color=\"#000000\"><a href=\"#index"+(num++)+"\">"+partie+"."+(num2++)+"<font color=\"#000000\">. Trajet des bateaux</font></a></font></td></tr>";
	
//	<tr><td><font color="#3300BB"><a href="#index2"> 2. Chenaux</a></font></td></tr>
	
	sommaire+="</table></td></tr></table></div><br><br><br>";
	
	return sommaire;
	
}

public static String creertitre(String titre,String auteur,String commentaire)
{
	return
	"<table width=\"100%\" border=0 bgcolor=\"#EEEEEEE\" bordercolor=\"000000\"><tr><td><H1><center><font color=\"#000000\">SINAVI - RAPPEL DE DONNEES</font></center></H1></td></tr></table><br><br>" +
	"<table width=\"100%\" border=0 bgcolor=\"#EEEEEEE\" bordercolor=\"000000\"><tr><td><H1><center><font color=\"#000000\">"+titre+"</font></center></H1></td></tr></table><br><br>" +
			"<H3>Auteur : "+auteur+" </H3><br>" +
					"Commentaires �ventuels : " +commentaire+
					"<br><br><br>";
}



/**
 * Methode qui permet d'�crire un titre sous un format specifique.
 * @param num numero de al partie.
 * @param titre titre de la partie.
 * @return la chaine coreespondante
 */
public static String creerPartie(int partie,int chapitre,int num,String titre)
{
	return
	"<table width=\"90%\" border=1 bgcolor=\"	 	#EEEEEEE\"><tr><td><font color=\"#3300BB\"><b id=\"index"+num+"\">"+partie+"."+chapitre+". "+titre+"</b></font></td></tr></table>";
}


public static String creerModele(Sinavi3DataSimulation _d,Sinavi3FrameGenerationRappelDonnees _f,String fichier)
{
	String chaine="";


	//creation du fichier du modele


	//creation du dessin si il existe en lancant la fenetre cach�e
	_d.application_.activerModelisation();
	_d.application_.gestionModelisation_.setVisible(false);

	File file=new File(fichier);
	CtuluImageExport.exportImageFor(_d.application_, file, _d.application_.gestionModelisation_.panelDessin_);


	//fermeture de la fenetre de dessin
	_d.application_.gestionModelisation_.setVisible(true);
	_d.application_.gestionModelisation_.dispose();


	System.out.println("fichier: "+file.getAbsolutePath());


	//insertion du modele
	chaine+="<br><br><img border=1 src=\""+file.getAbsolutePath()+"\"></img><br><br>";

	
	return chaine;
}

public static String creerDonneesgenerales(final Sinavi3DataSimulation _d,final Sinavi3FrameGenerationRappelDonnees _f)
{
	String chaine="";
	
	chaine+="<p><p ><table border=1>";
	
	//1 constitution des titres
	
	
		//nom
		chaine+="<tr><th bgcolor=\"AACCEE\">Graine d initialisation</th> <td bgcolor=\"	 	#EEEEEEE \">"+_d.params_.donneesGenerales.graine+" </td></tr>";
		chaine+="<tr><th bgcolor=\"AACCEE\">Dur�e de la simulation (jours)</th> <td bgcolor=\"	 	#EEEEEEE \">"+_d.params_.donneesGenerales.nombreJours+" </td></tr>";
		/*
		String jour="";
		switch(_d.params_.donneesGenerales.jourDepart)
		{
		case 1: jour="lundi";break;
		case 2: jour="mardi";break;
		case 3: jour="mercredi";break;
		case 4: jour="jeudi";break;
		case 5: jour="vendredi";break;
		case 6: jour="samedi";break;
		case 7: jour="dimanche";break;
		}
		chaine+="<tr><th bgcolor=\"AACCEE\"> jour de depart de la simulation</th> <td bgcolor=\"	 	#EEEEEEE \">"+jour+" </td></tr>";
		chaine+="<tr><th bgcolor=\"AACCEE\"> pourcentage du pied de pilote</th> <td bgcolor=\"	 	#EEEEEEE \">"+_d.params_.donneesGenerales.piedDePilote+"% </td></tr>";
		chaine+="<tr><th bgcolor=\"AACCEE\"> nombre de jours feries</th> <td bgcolor=\"	 	#EEEEEEE \">"+_d.params_.donneesGenerales.nombreJoursFeries+" </td></tr>";
		*/
			
		
		
		
	
	
	
	chaine+="</table></p><br><br>";
	return chaine;
}


public static String creerTableauGares(Sinavi3DataSimulation _d,Sinavi3FrameGenerationRappelDonnees _f)
{
	String chaine="";
	
	chaine+="<p><p ><table border=1 ><tr>";
	
	//1 constitution des titres
	
	chaine+="<th rowspan=\"1\"  bgcolor=\"	 	#EEEEEEE\">Nom</th>";
	
	
	chaine+="</tr>";
	
	//2 constitution du contenu
	for(int i=0;i<_d.listeGare_.listeGares_.size();i++)
	{
		//nom
		chaine+="<tr><th bgcolor=\"AACCEE\">"+_d.listeGare_.retournerGare(i)+"</th>";
		
			
		
		//fin de la ligne
		chaine+="</tr>";
	}
	
	
	chaine+="</table></p><br><br>";
	return chaine;
}





/**
 * methode d'ecriture des chenaux
 * @param _d
 * @param _f
 * @return
 */
public static String creerTableauChenaux(Sinavi3DataSimulation _d,Sinavi3FrameGenerationRappelDonnees _f)
{
	String chaine="";
	
	chaine+="<p><p ><table border=1 ><tr>";
	
	//1 constitution des titres
	chaine+="<th rowspan=\"2\"  bgcolor=\"	 	#EEEEEEE\">Nom</th>";
	chaine+="<th rowspan=\"2\"  bgcolor=\"	 	#EEEEEEE\">Hauteur d'eau (m)</th>";
	chaine+="<th rowspan=\"2\"  bgcolor=\"	 	#EEEEEEE\">Longueur (m)</th>";
	chaine+="<th rowspan=\"2\"  bgcolor=\"	 	#EEEEEEE\">Largeur (m)</th>";
	chaine+="<th rowspan=\"2\"  bgcolor=\"	 	#EEEEEEE\">Vitesse autorisee (km/h)</th>";
	chaine+="<th colspan=\"3\"  bgcolor=\"	 	#EEEEEEE\">Cr�neaux (h.min)</th>";
	chaine+="</tr><tr bgcolor=\"	 	#EEEEEEE\"><th>Cr�neau 1</th><th>Cr�neau 2</th><th>Cr�neau 3</th>";
	chaine+="</tr>";
	
	//2 constitution du contenu
	for(int i=0;i<_d.listeBief_.listeBiefs_.size();i++)
	{
		//nom
		chaine+="<tr><th bgcolor=\"AACCEE\">"+_d.listeBief_.retournerBief(i).nom_+"</th>";
		//caracteristiques
		chaine+="<td>"+(float)_d.listeBief_.retournerBief(i).profondeur_+"</td> ";
		chaine+="<td>"+(float)_d.listeBief_.retournerBief(i).longueur_+"</td> ";
		chaine+="<td>"+(float)_d.listeBief_.retournerBief(i).largeur_+"</td> ";
		chaine+="<td>"+(float)_d.listeBief_.retournerBief(i).vitesse_+"</td> ";
		chaine+="<td>"+(float)_d.listeBief_.retournerBief(i).h_.semaineCreneau1HeureDep+"-"+(float)_d.listeBief_.retournerBief(i).h_.semaineCreneau1HeureArrivee+"</td> ";
		chaine+="<td>"+(float)_d.listeBief_.retournerBief(i).h_.semaineCreneau2HeureDep+"-"+(float)_d.listeBief_.retournerBief(i).h_.semaineCreneau2HeureArrivee+"</td> ";
		chaine+="<td>"+(float)_d.listeBief_.retournerBief(i).h_.semaineCreneau3HeureDep+"-"+(float)_d.listeBief_.retournerBief(i).h_.semaineCreneau3HeureArrivee+"</td> ";
		//fin de la ligne
		chaine+="</tr>";
	}
	
	
	chaine+="</table></p><br><br>";
	return chaine;
}
	
public static String creerTableauEcluses(Sinavi3DataSimulation _d,Sinavi3FrameGenerationRappelDonnees _f)
{
	String chaine="";
	
	chaine+="<p><p ><table border=1 ><tr>";
	
	//1 constitution des titres
	
	
	chaine+="<th rowspan=\"2\"  bgcolor=\"	 	#EEEEEEE\">Nom</th>";
	chaine+="<th rowspan=\"2\"  bgcolor=\"	 	#EEEEEEE\">Longueur (m)</th>";
	chaine+="<th rowspan=\"2\"  bgcolor=\"	 	#EEEEEEE\">Largeur (m)</th>";
	chaine+="<th rowspan=\"2\"  bgcolor=\"	 	#EEEEEEE\">Profondeur (m)</th>";
	chaine+="<th colspan=\"2\"  bgcolor=\"	 	#EEEEEEE\">Dur�e des bassin�es (min)</th>";
	chaine+="<th colspan=\"2\"  bgcolor=\"	 	#EEEEEEE\">Dur�e des manoeuvres (min)</th>";
	chaine+="<th rowspan=\"2\"  bgcolor=\"	 	#EEEEEEE\">Loi d'indisponibilit�</th>";
	chaine+="<th colspan=\"3\"  bgcolor=\"	 	#EEEEEEE\">Cr�neaux (h.min)</th>";
	chaine+="</tr><tr bgcolor=\"	 	#EEEEEEE\"><th>Montante</th><th>Avalante</th><th>Entree</th><th>Sortie</th><th>Creneau 1</th><th>Creneau 2</th><th>creneau 3</th>";
		
	chaine+="</tr>";
	
	//2 constitution du contenu
	for(int i=0;i<_d.listeEcluse_.listeEcluses_.size();i++)
	{
		//nom
		chaine+="<tr><th bgcolor=\"AACCEE\">"+_d.listeEcluse_.retournerEcluse(i).nom_+"</th>";
		
		//caracteristiques
		chaine+="<td>"+(float)_d.listeEcluse_.retournerEcluse(i).longueur_+"</td>";
		chaine+="<td>"+(float)_d.listeEcluse_.retournerEcluse(i).largeur_+"</td>";
		chaine+="<td>"+(float)_d.listeEcluse_.retournerEcluse(i).profondeur_+"</td>";
		chaine+="<td>"+(float)_d.listeEcluse_.retournerEcluse(i).tempsFausseBassinneeMontant_+"</td>";
		chaine+="<td>"+(float)_d.listeEcluse_.retournerEcluse(i).tempsFausseBassinneeAvalant_+"</td>";
		chaine+="<td>"+(float)_d.listeEcluse_.retournerEcluse(i).dureeManoeuvreEntrant_+" / " + (float)_d.listeEcluse_.retournerEcluse(i).dureeManoeuvreEntrant2_ + "</td>";
		chaine+="<td>"+(float)_d.listeEcluse_.retournerEcluse(i).dureeManoeuvreSortant_+" / " + (float)_d.listeEcluse_.retournerEcluse(i).dureeManoeuvreSortant2_ + "</td>";
		if(_d.listeEcluse_.retournerEcluse(i).typeLoi_==0)
				chaine+="<td>Erlang, d'ordre "+_d.listeEcluse_.retournerEcluse(i).loiIndispo_+"</td>";
		else
			if(_d.listeEcluse_.retournerEcluse(i).typeLoi_==1)
		chaine+="<td>D�terministe</td>";
			else
				chaine+="<td>Journali�re</td>";
		
		chaine+="<td>"+(float)_d.listeEcluse_.retournerEcluse(i).h_.semaineCreneau1HeureDep+"-"+(float)_d.listeEcluse_.retournerEcluse(i).h_.semaineCreneau1HeureArrivee+"</td> ";
		chaine+="<td>"+(float)_d.listeEcluse_.retournerEcluse(i).h_.semaineCreneau2HeureDep+"-"+(float)_d.listeEcluse_.retournerEcluse(i).h_.semaineCreneau2HeureArrivee+"</td> ";
		chaine+="<td>"+(float)_d.listeEcluse_.retournerEcluse(i).h_.semaineCreneau3HeureDep+"-"+(float)_d.listeEcluse_.retournerEcluse(i).h_.semaineCreneau3HeureArrivee+"</td> ";
		
		//fin de la ligne
		chaine+="</tr>";
	}
	
	
	chaine+="</table></p><br><br>";
	return chaine;
}





public static String creerTableauCategorie(Sinavi3DataSimulation _d,Sinavi3FrameGenerationRappelDonnees _f)
{
	String chaine="";
	
	chaine+="<p><p ><table border=1 ><tr>";
	
	//1 constitution des titres
	
	chaine+="<th rowspan=\"2\"  bgcolor=\"	 	#EEEEEEE\">Nom</th>";
	chaine+="<th rowspan=\"2\"  bgcolor=\"	 	#EEEEEEE\">Priorit�</th>";
	chaine+="<th rowspan=\"2\"  bgcolor=\"	 	#EEEEEEE\">Longueur (m)</th>";	
	chaine+="<th rowspan=\"2\"  bgcolor=\"	 	#EEEEEEE\">Largeur (m)</th>";	
	chaine+="<th rowspan=\"2\"  bgcolor=\"	 	#EEEEEEE\">Vitesse (km/H)</th>";
	chaine+="<th rowspan=\"2\"  bgcolor=\"	 	#EEEEEEE\">Tirant d'eau (m)</th>";		
	chaine+="<th rowspan=\"1\" colspan=\"3\"  bgcolor=\"	 	#EEEEEEE\">Cr�neaux (h.min)</th>";	  
	chaine+="</tr>";
	
	
	chaine+="<tr>";
	//sous tableaux de titres
	chaine+="<th rowspan=\"1\"  bgcolor=\"	 	#EEEEEEE\">Cr�neau 1</th>";
	chaine+="<th rowspan=\"1\"  bgcolor=\"	 	#EEEEEEE\">Cr�neau 2</th>";
	chaine+="<th rowspan=\"1\"  bgcolor=\"	 	#EEEEEEE\">Cr�neau 3</th>";
	chaine+="</tr>";
	
	//2 constitution du contenu
	for(int i=0;i<_d.listeBateaux_.listeNavires_.size();i++)
	{
		chaine+="<tr>";
		chaine+="<th bgcolor=\"AACCEE\">"+_d.listeBateaux_.retournerNavire(i).nom+"</th>";	
		if(_d.listeBateaux_.retournerNavire(i).priorite==0)chaine+="<td>normal</td>";
			   else
				   if(_d.listeBateaux_.retournerNavire(i).priorite==1)chaine+="<td>eleve</td>";
				   
				   else chaine+="<td>tres elevee</td>";
		chaine+="<td>"+_d.listeBateaux_.retournerNavire(i).longueurMax+"</th>";
		chaine+="<td>"+_d.listeBateaux_.retournerNavire(i).largeurMax+"</th>";
		chaine+="<td>A:"+(float)_d.listeBateaux_.retournerNavire(i).vitesseDefautAvalant+" - M:"+(float)_d.listeBateaux_.retournerNavire(i).vitesseDefautMontant+"</td>";
		chaine+="<td>E:"+(float)_d.listeBateaux_.retournerNavire(i).tirantEau+" - type: "+_d.listeBateaux_.retournerNavire(i).typeTirant+"</td>";
		chaine+="<td>"+_d.listeBateaux_.retournerNavire(i).creneauxJournaliers_.semaineCreneau1HeureDep+"-"+_d.listeBateaux_.retournerNavire(i).creneauxJournaliers_.semaineCreneau1HeureArrivee+"</td>";
		chaine+="<td>"+_d.listeBateaux_.retournerNavire(i).creneauxJournaliers_.semaineCreneau2HeureDep+"-"+_d.listeBateaux_.retournerNavire(i).creneauxJournaliers_.semaineCreneau2HeureArrivee+"</td>";
		chaine+="<td>"+_d.listeBateaux_.retournerNavire(i).creneauxJournaliers_.semaineCreneau3HeureDep+"-"+_d.listeBateaux_.retournerNavire(i).creneauxJournaliers_.semaineCreneau3HeureArrivee+"</td>";
		//fin de la ligne
		chaine+="</tr>";
	}
	
	
	chaine+="</table></p><br><br>";
	return chaine;
}




public static String creerTableauTopologieChenaux(Sinavi3DataSimulation _d,Sinavi3FrameGenerationRappelDonnees _f)
{
	String chaine="";
	
	chaine+="<p><p ><table border=1 ><tr>";
	
	//1 constitution des titres
	
	chaine+="<th rowspan=\"1\"  bgcolor=\"	 	#EEEEEEE\">Nom</th>";
	chaine+="<th rowspan=\"1\"  bgcolor=\"	 	#EEEEEEE\">Gare de rattachement amont</th>";
	chaine+="<th rowspan=\"1\"  bgcolor=\"	 	#EEEEEEE\">Gare de rattachement aval</th>";
	
	
	chaine+="</tr>";
	
	//2 constitution du contenu
	for(int i=0;i<_d.listeBief_.listeBiefs_.size();i++)
	{
		//nom
		chaine+="<tr><th bgcolor=\"AACCEE\">"+_d.listeBief_.retournerBief(i).nom_+"</th>";
		chaine+="<td>"+_d.listeGare_.retournerGare(_d.listeBief_.retournerBief(i).gareAmont_)+"</th>";
		chaine+="<td>"+_d.listeGare_.retournerGare(_d.listeBief_.retournerBief(i).gareAval_)+"</th>";
		//fin de la ligne
		chaine+="</tr>";
	}
	
	
	chaine+="</table></p><br><br>";
	return chaine;
}




public static String creerTableauTopologieEcluses(Sinavi3DataSimulation _d,Sinavi3FrameGenerationRappelDonnees _f)
{
	String chaine="";
	
	chaine+="<p><p ><table border=1 ><tr>";
	
	//1 constitution des titres
	
	chaine+="<th rowspan=\"1\"  bgcolor=\"	 	#EEEEEEE\">Nom</th>";
	chaine+="<th rowspan=\"1\"  bgcolor=\"	 	#EEEEEEE\">Gare de rattachement amont</th>";
	chaine+="<th rowspan=\"1\"  bgcolor=\"	 	#EEEEEE\">Gare de rattachement aval</th>";
	
	
	chaine+="</tr>";
	
	//2 constitution du contenu
	for(int i=0;i<_d.listeEcluse_.listeEcluses_.size();i++)
	{
		//nom
		chaine+="<tr><th bgcolor=\"AACCEE\">"+_d.listeEcluse_.retournerEcluse(i).nom_+"</th>";
		chaine+="<td>"+_d.listeGare_.retournerGare(_d.listeEcluse_.retournerEcluse(i).gareAmont_)+"</th>";
		chaine+="<td>"+_d.listeGare_.retournerGare(_d.listeEcluse_.retournerEcluse(i).gareAval_)+"</th>";
		//fin de la ligne
		chaine+="</tr>";
	}
	
	
	chaine+="</table></p><br><br>";
	return chaine;
}



public static String creerTableauCroisementChenaux(Sinavi3DataSimulation _d,Sinavi3FrameGenerationRappelDonnees _f)
{
	String chaine="";
	
	chaine+="<br>";
	
	
	
	for(int c=0;c<_d.listeBief_.listeBiefs_.size();c++)
	{
		chaine+="<p><b>Croisement dans le tron�on "+_d.listeBief_.retournerBief(c).nom_+"</b><br>";
	//1 constitution des titres
	chaine+="<table border=1 ><tr>";
	chaine+="<th rowspan=\"1\"  bgcolor=\"	 	#EEEEEE\">Nom</th>";
	
	for(int i=0;i<_d.listeBateaux_.listeNavires_.size();i++)
		chaine+="<th rowspan=\"1\"  bgcolor=\"AACCEE\">"+_d.listeBateaux_.retournerNavire(i).nom+"</th>";
	
	
	chaine+="</tr>";
	
	
	//2 constitution du contenu
	for(int i=0;i<_d.listeBateaux_.listeNavires_.size();i++)
	{
		chaine+="<tr>";
//		nom
		chaine+="<th rowspan=\"1\"  bgcolor=\"AACCEE\">"+_d.listeBateaux_.retournerNavire(i).nom+"</th>";
		
		for(int j=0;j<_d.listeBateaux_.listeNavires_.size();j++)
		{
			if(_d.listeBief_.retournerBief(c).reglesCroisement_.retournerAij(i, j).booleanValue()==true)
			chaine+="<td>oui </td>";
			else
				chaine+="<td>non </td>";
		
		
		
	   }
		chaine+="</tr>";
	
	}
	
	chaine+="</table></p><br><br>";
	
 }
	
	
	return chaine;
}


public static String creerTableauTrematagesChenaux(Sinavi3DataSimulation _d,Sinavi3FrameGenerationRappelDonnees _f)
{
	String chaine="";
	
	chaine+="<br>";
	
	
	
	for(int c=0;c<_d.listeBief_.listeBiefs_.size();c++)
	{
		chaine+="<p><b>Tr�matage dans le tron�on "+_d.listeBief_.retournerBief(c).nom_+"</b><br>";
	//1 constitution des titres
	chaine+="<table border=1 ><tr>";
	chaine+="<th rowspan=\"1\"  bgcolor=\"	 	#EEEEEE\">Nom</th>";
	
	for(int i=0;i<_d.listeBateaux_.listeNavires_.size();i++)
		chaine+="<th rowspan=\"1\"  bgcolor=\"AACCEE\">"+_d.listeBateaux_.retournerNavire(i).nom+"</th>";
	
	
	chaine+="</tr>";
	
	
	//2 constitution du contenu
	for(int i=0;i<_d.listeBateaux_.listeNavires_.size();i++)
	{
		chaine+="<tr>";
//		nom
		chaine+="<th rowspan=\"1\"  bgcolor=\"AACCEE\">"+_d.listeBateaux_.retournerNavire(i).nom+"</th>";
		
		for(int j=0;j<_d.listeBateaux_.listeNavires_.size();j++)
		{
			if(_d.listeBief_.retournerBief(c).reglesTrematage_.retournerAij(i, j).booleanValue()==true)
			chaine+="<td>oui </td>";
			else
				chaine+="<td>non </td>";
		
		
		
	   }
		chaine+="</tr>";
	
	}
	
	chaine+="</table></p><br><br>";
	
 }
	
	
	return chaine;
}


public static String creerTableauDureeChenaux(Sinavi3DataSimulation _d,Sinavi3FrameGenerationRappelDonnees _f)
{
	String chaine="";
	
	chaine+="<br>";
	
	chaine+="<p><b>Vitesse avalante dans les tron�ons (km/h)</b><br>";
		
	chaine+="<table border=1 ><tr>";
	chaine+="<th rowspan=\"1\"  bgcolor=\"	 	#EEEEEE\">Nom</th>";
	
	for(int i=0;i<_d.listeBateaux_.listeNavires_.size();i++)
		chaine+="<th rowspan=\"1\"  bgcolor=\"AACCEE\">"+_d.listeBateaux_.retournerNavire(i).nom+"</th>";
	
	
	chaine+="</tr>";
	
	
	//2 constitution du contenu
	for(int i=0;i<_d.listeBief_.listeBiefs_.size();i++)
	{
		chaine+="<tr>";
//		nom
		chaine+="<th rowspan=\"1\"  bgcolor=\"AACCEE\">"+_d.listeBief_.retournerBief(i).nom_+"</th>";
		
		for(int j=0;j<_d.listeBateaux_.listeNavires_.size();j++)
		{
			chaine+="<td>"+(float)_d.reglesVitesseBiefAvalant_.retournerValeur(i,j) +"</td>";
			
		 }
		chaine+="</tr>";
	
	}
	
	chaine+="</table></p><br>";
	
 
	
	
	return chaine;
}


public static String creerTableauRemplissageSAS(Sinavi3DataSimulation _d,Sinavi3FrameGenerationRappelDonnees _f)
{
  String chaine="";
  
  chaine+="<p>";
  
  
  
    
  chaine+="<table border=1 ><tr>";
  chaine+="<th rowspan=\"1\"  bgcolor=\"    #EEEEEE\">Nom</th>";
  
  for(int i=0;i<_d.getListeBateaux_().getListeNavires_().size();i++)
    chaine+="<th rowspan=\"1\"  bgcolor=\"AACCEE\">"+_d.getListeBateaux_().retournerNavire(i).getNom()+"</th>";
  
  
  chaine+="</tr>";
  
  
  //2 constitution du contenu
  for(int i=0;i<_d.getListeEcluse().getListeEcluses().size();i++)
  {
    chaine+="<tr>";
//    nom
    chaine+="<th rowspan=\"1\"  bgcolor=\"AACCEE\">"+_d.getListeEcluse().retournerEcluse(i).getNom()+"</th>";
    
    for(int j=0;j<_d.getListeBateaux_().getListeNavires_().size();j++)
    {
      chaine+="<td>"+(float)_d.getReglesRemplissageSAS().retournerValeur(i,j) +"</td>";
      
     }
    chaine+="</tr>";
  
  }
  
  chaine+="</table></p><br><br>";
  
 
  
  
  return chaine;
}


public static String creerTableauDureeChenauxMontant(Sinavi3DataSimulation _d,Sinavi3FrameGenerationRappelDonnees _f)
{
	String chaine="";
	
	chaine+="<br>";
	
	chaine+="<p><b>Vitesse montante dans les tron�ons (km/h)</b><br>";
	
		
	chaine+="<table border=1 ><tr>";
	chaine+="<th rowspan=\"1\"  bgcolor=\"	 	#EEEEEE\">Nom</th>";
	
	for(int i=0;i<_d.listeBateaux_.listeNavires_.size();i++)
		chaine+="<th rowspan=\"1\"  bgcolor=\"AACCEE\">"+_d.listeBateaux_.retournerNavire(i).nom+"</th>";
	
	
	chaine+="</tr>";
	
	
	//2 constitution du contenu
	for(int i=0;i<_d.listeBief_.listeBiefs_.size();i++)
	{
		chaine+="<tr>";
//		nom
		chaine+="<th rowspan=\"1\"  bgcolor=\"AACCEE\">"+_d.listeBief_.retournerBief(i).nom_+"</th>";
		
		for(int j=0;j<_d.listeBateaux_.listeNavires_.size();j++)
		{
			chaine+="<td>"+(float)_d.reglesVitesseBiefMontant_.retournerValeur(i,j) +"</td>";
			
		 }
		chaine+="</tr>";
	
	}
	
	chaine+="</table></p><br><br>";
	
 
	
	
	return chaine;
}


public static String creerTableauDureeManEcluseEntrant(Sinavi3DataSimulation _d,Sinavi3FrameGenerationRappelDonnees _f)
{
	String chaine="";
	
	chaine+="<br>";
	
	chaine+="<p><b>Dur�e de manoeuvre entrante dans les �cluses (min)</b><br>";
	
		
	chaine+="<table border=1 ><tr>";
	chaine+="<th rowspan=\"1\"  bgcolor=\"	 	#EEEEEE\">Nom</th>";
	
	for(int i=0;i<_d.listeBateaux_.listeNavires_.size();i++)
		chaine+="<th rowspan=\"1\"  bgcolor=\"AACCEE\">"+_d.listeBateaux_.retournerNavire(i).nom+"</th>";
	
	
	chaine+="</tr>";
	
	
	//2 constitution du contenu
	for(int i=0;i<_d.listeEcluse_.listeEcluses_.size();i++)
	{
		chaine+="<tr>";
//		nom
		chaine+="<th rowspan=\"1\"  bgcolor=\"AACCEE\">"+_d.listeEcluse_.retournerEcluse(i).nom_+"</th>";
		
		for(int j=0;j<_d.listeBateaux_.listeNavires_.size();j++)
		{
			
			chaine+="<td>"+(float)_d.getDureeManeuvresEcluses().getDureeParcours(i,j,_d).dureeEntrant1 +
					" / " + (float)_d.getDureeManeuvresEcluses().getDureeParcours(i,j,_d).dureeEntrant2 + "</td>";
		 }
		chaine+="</tr>";
	
	}
	
	chaine+="</table></p><br>";
	
 
	
	
	return chaine;
}

public static String creerTableauDureeManEcluseSortant(Sinavi3DataSimulation _d,Sinavi3FrameGenerationRappelDonnees _f)
{
	String chaine="";
	
	chaine+="<br>";
	
	chaine+="<p><b>Dur�e de manoeuvre sortante dans les �cluses (min)</b><br>";
	
		
	chaine+="<table border=1 ><tr>";
	chaine+="<th rowspan=\"1\"  bgcolor=\"	 	#EEEEEE\">Nom</th>";
	
	for(int i=0;i<_d.listeBateaux_.listeNavires_.size();i++)
		chaine+="<th rowspan=\"1\"  bgcolor=\"AACCEE\">"+_d.listeBateaux_.retournerNavire(i).nom+"</th>";
	
	
	chaine+="</tr>";
	
	
	//2 constitution du contenu
	for(int i=0;i<_d.listeEcluse_.listeEcluses_.size();i++)
	{
		chaine+="<tr>";
//		nom
		chaine+="<th rowspan=\"1\"  bgcolor=\"AACCEE\">"+_d.listeEcluse_.retournerEcluse(i).nom_+"</th>";
		
		for(int j=0;j<_d.listeBateaux_.listeNavires_.size();j++)
		{
			chaine+="<td>"+(float)_d.getDureeManeuvresEcluses().getDureeParcours(i,j,_d).dureeSortant1 +
					" / " + (float)_d.getDureeManeuvresEcluses().getDureeParcours(i,j,_d).dureeSortant2 + "</td>";
			
		 }
		chaine+="</tr>";
	
	}
	
	chaine+="</table></p><br><br>";
	
 
	
	
	return chaine;
}

public static String creerTableauTrajet(Sinavi3DataSimulation _d,Sinavi3FrameGenerationRappelDonnees _f)
{
	String chaine="";
	
	chaine+="<p>";
	
	
	
		
	chaine+="<table border=1 ><tr>";
	chaine+="<th rowspan=\"1\"  bgcolor=\"AACCEE\">Bateau</th>";
	chaine+="<th rowspan=\"1\"  bgcolor=\"AACCEE\">Gare depart</th>";
	chaine+="<th rowspan=\"1\"  bgcolor=\"AACCEE\">Gare arrivee</th>";
	chaine+="<th rowspan=\"1\"  bgcolor=\"AACCEE\">Sens</th>";
	chaine+="<th rowspan=\"1\"  bgcolor=\"AACCEE\">Type de loi</th>";
	
	
	chaine+="</tr>";
	
	
	//2 constitution du contenu
	for(int i=0;i<_d.listeBateaux_.listeNavires_.size();i++)
		for(int k=0;k<_d.listeBateaux_.retournerNavire(i).listeTrajet_.size();k++)
			{
				Sinavi3Trajet trajet= _d.listeBateaux_.retournerNavire(i).listeTrajet_.retournerTrajet(k);
			
				chaine+="<tr>";
				//nom
				chaine+="<th bgcolor=\"AACCEE\">"+_d.listeBateaux_.retournerNavire(i).nom+"</th>";
				chaine+="<td>"+_d.listeGare_.retournerGare(trajet.gareDepart_)  +"</td>";
				chaine+="<td>"+_d.listeGare_.retournerGare(trajet.gareArrivee_)  +"</td>";
				if(trajet.sens_==0)
				chaine+="<td> Avalant</td>";
				else
					chaine+="<td> Montant</td>";
				if(trajet.typeLoiGenerationNavires_==0)
					chaine+="<td> Erlang</td>";
				else
					if(trajet.typeLoiGenerationNavires_==1)
						chaine+="<td> Deterministe</td>";
					else
						chaine+="<td> Journaliere</td>";
				
				chaine+="</tr>";
			
			}
	
	chaine+="</table></p><br><br>";
	
 
	
	
	return chaine;
}



}



