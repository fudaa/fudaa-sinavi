package org.fudaa.fudaa.sinavi3;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.fudaa.ebli.network.simulationNetwork.SimulationNetworkEditor;
import org.fudaa.fudaa.ressource.FudaaResource;
import org.fudaa.fudaa.sinavi3.structures.DefaultStructure;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuDialogError;
import com.memoire.bu.BuDialogMessage;
import com.memoire.fu.FuLog;

/**
 * Panel de saisie des donnees du chenal
 * 
 * @author Adrien Hadoux
 */
public class Sinavi3PanelSaisieBief extends JPanel {

  static int nbouvertures = 0;
  // attributs
  
  // loi de proba indispo******
  Sinavi3FrameSaisieLoiJournaliere fenetreLoiJournaliere_ = null;
  Sinavi3FrameSaisieLoiDeterministe fenetreLoideter_ = null;
  Sinavi3TextFieldDuree dureeIndispo_ = new Sinavi3TextFieldDuree(3);
  Sinavi3TextFieldInteger frequenceMoyenne_ = new Sinavi3TextFieldInteger(3);
  Sinavi3TextFieldInteger frequenceMoyenne2_ = new Sinavi3TextFieldInteger(3);
  String[] tabloi_ = { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10" };
  JComboBox loiProbaDuree_ = new JComboBox(tabloi_);
  JComboBox loiProbaFrequence_ = new JComboBox(tabloi_);
  String[] choixLoi_ = { "Erlang", "Deterministe"};
  JComboBox choixLoiFrequence = new JComboBox(choixLoi_);
  /** variable contenant le tableau des couples pour la loi deterministe */
  ArrayList loiDeterministe_ = new ArrayList();

  // ******fin loi duree indispo
  
  /**
   * Identificateur
   */
  Sinavi3TextField Nom = new Sinavi3TextField(10);

  Sinavi3TextFieldFloat longueur = new Sinavi3TextFieldFloat(3);
  Sinavi3TextFieldFloat largeur = new Sinavi3TextFieldFloat(3);
  
  Sinavi3TextFieldFloat profondeur = new Sinavi3TextFieldFloat(3);
  Sinavi3TextFieldFloat vitesseMax = new Sinavi3TextFieldFloat(3);

  String[] choix = { "non", "oui" };
 // JComboBox soumisMaree = new JComboBox(choix);

  final BuButton creneau_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_continuer"), "cr�neau");

  final BuButton validation_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_oui"), "valider");

  /**
   * Bordures
   */
 
  /**
   * Horaire specifique au chenal qui sera saisi lors de la creation de la frame de saisie d'horaire et sera compl�t�e
   * dans la methode de validation des donn�es via le bouton de validation
   */
  Sinavi3Horaire horaire_ = new Sinavi3Horaire();

  /**
   * Mode modification si mode est a true alors le mode de saisie est un mode de modification: par defaut est mis sur
   * false
   */
  boolean UPDATE = false;

  /**
   * indice du chenal a modifier dans le cas du mode modification:
   */
  int BIEF_A_MODIFIER_;

  /**
   * Parametres de saisies de la simulation
   */
  Sinavi3DataSimulation donnees_;

  /**
   * Fenetre principal de gestion des cheneaux
   */
  Sinavi3VisualiserBief MENUCHENAL_;

  /**
   * Constructeur de la frame
   */
  public Sinavi3PanelSaisieBief(final Sinavi3DataSimulation d, final Sinavi3VisualiserBief vc) {

    // recuperation des parametres de la simulation:
    donnees_ = d;
    MENUCHENAL_ = vc;
    nbouvertures++;

    /**
     * les differents controles
     */
    this.Nom.setToolTipText("Veuillez entrer le nom du chenal ici: ");
    this.Nom.setText(DefaultStructure.getDefaultName(new Sinavi3Bief(),donnees_.listeBief_.listeBiefs_.size()));
    //this.soumisMaree.setToolTipText("choisissez si vous souhaitez que le chenal soit soumis ou non � la mar�e");

    this.longueur.setToolTipText("Saisissez la longueur en Metres du tron�on.");

    this.largeur.setToolTipText("Saisissez la largeur en Metres du tron�on.");

    this.profondeur.setToolTipText("Saisissez la profondeur en Metres du tron�on.");

    this.vitesseMax.setToolTipText("Saisissez la vitesse maximum autoris�e en Kilometres par Heures du tron�on.");

    /**
     * Listener des boutons
     */
    this.creneau_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        System.out.print("creation d'un horaire");
        donnees_.application_.addInternalFrame(new Sinavi3FrameSaisieHorairesResume(horaire_));
      }
    });

    this.validation_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        System.out.print("validation des donn�es");
        creation_chenal();
      }
    });
    
    this.choixLoiFrequence.addActionListener(new ActionListener() {

        public void actionPerformed(final ActionEvent e) {
          /**
           * ETAPE 1: determiner le type de loi selectionn�
           */
          final int choixLoi = choixLoiFrequence.getSelectedIndex();

          if (choixLoi == 0) {
            // Cas 0: loi d erlang
            frequenceMoyenne_.setEnabled(true);
            frequenceMoyenne2_.setEnabled(true);
            loiProbaFrequence_.setEnabled(true);
            loiProbaDuree_.setEnabled(true);

          } else if (choixLoi == 1) {
            // cas 1: loi deterministe
            frequenceMoyenne_.setEnabled(false);
            frequenceMoyenne2_.setEnabled(false);
            loiProbaFrequence_.setEnabled(false);
            loiProbaDuree_.setEnabled(false);
            // donnees_.application_.addInternalFrame(new
            // SiporFrameSaisieLoiDeterministe(donnees_,loiDeterministe_,dureeIndispo_));

            if (fenetreLoideter_ == null) {
              FuLog.debug("interface nulle");

              fenetreLoideter_ = new Sinavi3FrameSaisieLoiDeterministe(donnees_, loiDeterministe_, dureeIndispo_);

              // System.out.println("55555");
              fenetreLoideter_.setVisible(true);

              donnees_.application_.addInternalFrame(fenetreLoideter_);
            } else {
              FuLog.debug("interface ferm�e");
              if (fenetreLoideter_.isClosed()) {

                fenetreLoideter_ = new Sinavi3FrameSaisieLoiDeterministe(donnees_, loiDeterministe_, dureeIndispo_);

                donnees_.application_.addInternalFrame(fenetreLoideter_);

              } else {
                FuLog.debug("interface cas de figur restant autre que null et fermeture");

                fenetreLoideter_ = new Sinavi3FrameSaisieLoiDeterministe(donnees_, loiDeterministe_, dureeIndispo_);

                donnees_.application_.activateInternalFrame(fenetreLoideter_);
                donnees_.application_.addInternalFrame(fenetreLoideter_);

              }
            }

          } else if (choixLoi == 2) {
            // cas 2: loi journaliere
            frequenceMoyenne_.setEnabled(false);
            loiProbaFrequence_.setEnabled(false);
            frequenceMoyenne2_.setEnabled(false);
            loiProbaDuree_.setEnabled(false);
            // lancement de la frame de saisie des creneaux de la loi journaliere
            // donnees_.application_.addInternalFrame(new
            // SiporFrameSaisieLoiJournaliere(donnees_,loiDeterministe_,dureeIndispo_));

            if (fenetreLoiJournaliere_ == null) {
              FuLog.debug("interface nulle");

              fenetreLoiJournaliere_ = new Sinavi3FrameSaisieLoiJournaliere(donnees_, loiDeterministe_, dureeIndispo_);

              // System.out.println("55555");
              fenetreLoiJournaliere_.setVisible(true);
              donnees_.application_.addInternalFrame(fenetreLoiJournaliere_);
            } else {
              FuLog.debug("interface ferm�e");
              if (fenetreLoiJournaliere_.isClosed()) {

                fenetreLoiJournaliere_ = new Sinavi3FrameSaisieLoiJournaliere(donnees_, loiDeterministe_, dureeIndispo_);

                donnees_.application_.addInternalFrame(fenetreLoiJournaliere_);

              } else {
                FuLog.debug("interface cas de figur restant autre que null et fermeture");

                fenetreLoiJournaliere_ = new Sinavi3FrameSaisieLoiJournaliere(donnees_, loiDeterministe_, dureeIndispo_);

                donnees_.application_.activateInternalFrame(fenetreLoiJournaliere_);
                donnees_.application_.addInternalFrame(fenetreLoiJournaliere_);

              }
            }

          }

        }

      });

    /**
     * ******************************************************************************************** Affichage des
     * �l�ments:
     */
    
   // this.setLayout(new BorderLayout());
    JPanel contenu=new JPanel(new BorderLayout());
    this.setBorder(Sinavi3Bordures.bief);
    this.add(contenu);
    
    
    Box total = Box.createVerticalBox();
    //this.setBorder(SiporBordures.compound_);
    contenu.add(total,BorderLayout.CENTER);
    
    //general
    final JPanel sc1 = new JPanel();
    sc1.setBorder(Sinavi3Bordures.createTitledBorder("General"));//BorderFactory.createTitledBorder(SiporBordures.compound_, "General"));
    sc1.add(new JLabel("Nom du tron�on: "));
    sc1.add(this.Nom);
    contenu.add(sc1,BorderLayout.NORTH);


    //dimensions
    JPanel dimension=new JPanel(new GridLayout(2,2));
    dimension.setBorder(Sinavi3Bordures.createTitledBorder("Dimensions"));//BorderFactory.createTitledBorder(SiporBordures.compound_, "Dimensions"));
    total.add(dimension);
    
    final JPanel sc21 = new JPanel();
    sc21.setBorder(Sinavi3Bordures.bordnormal_);
    sc21.add(new JLabel("Hauteur d'eau: "));
    sc21.add(this.profondeur);
    sc21.add(new JLabel("Metres "));
    dimension.add(sc21);
    
    JPanel sc22=new JPanel();
    sc22.add(new JLabel("Vitesse autoris�e: "));
    sc22.setBorder(Sinavi3Bordures.bordnormal_);
    sc22.add(this.vitesseMax);
    sc22.add(new JLabel("KM/H "));
    dimension.add(sc22);

    JPanel sc23=new JPanel();
    sc23.add(new JLabel("Longueur: "));
    sc23.setBorder(Sinavi3Bordures.bordnormal_);
    sc23.add(this.longueur);
    sc23.add(new JLabel("Metres"));
    dimension.add(sc23);

    JPanel sc24=new JPanel();
    sc24.add(new JLabel("Largeur: "));
    sc24.setBorder(Sinavi3Bordures.bordnormal_);
    sc24.add(this.largeur);
    sc24.add(new JLabel("Metres "));
    dimension.add(sc24);

    

    
    // indispo+duree+creneau
    JPanel indispoCreneau=new JPanel(new GridLayout(1,2));
    total.add(indispoCreneau);
    
    //loi indisponibilit�s
   Box loiIndispo= Box.createVerticalBox();
   loiIndispo.setBorder(Sinavi3Bordures.createTitledBorder("Loi indisponibilite"));//BorderFactory.createTitledBorder(SiporBordures.compound_,"Loi indisponibilite"));
   indispoCreneau.add(loiIndispo);
    
   
    final JPanel p31 = new JPanel();
    p31.add(new JLabel("Type de loi: "));
    p31.add(choixLoiFrequence);
    p31.setBorder(Sinavi3Bordures.bordnormal_);
    loiIndispo.add(p31);

    final JPanel p32 = new JPanel();
    p32.add(new JLabel("Ecart moyen:"));
    p32.add(this.frequenceMoyenne_);
    p32.add(new JLabel("Jours"));
    p32.add(this.frequenceMoyenne2_);
    p32.add(new JLabel("Heures"));
    p32.setBorder(Sinavi3Bordures.bordnormal_);
    loiIndispo.add(p32);

    final JPanel p33 = new JPanel();
    p33.add(new JLabel("Ordre loi d'Erlang frequence: "));
    p33.add(this.loiProbaFrequence_);
    p33.setBorder(Sinavi3Bordures.bordnormal_);
    loiIndispo.add(p33);

    
    //durees indispo + creneaux
    Box dureeIndispoCrenaux=Box.createVerticalBox();
    indispoCreneau.add(dureeIndispoCrenaux);
    
    //durees indispo
    Box dureeIndispo=Box.createVerticalBox();
    dureeIndispo.setBorder(Sinavi3Bordures.createTitledBorder("Dur�e d'indisponibilit�"));//BorderFactory.createTitledBorder(SiporBordures.compound_,"Duree indisponibilite"));
    dureeIndispoCrenaux.add(dureeIndispo);
    
    
    final JPanel p21 = new JPanel();
    p21.add(new JLabel("Dur�e moyenne:"));
    p21.add(this.dureeIndispo_);
    p21.add(new JLabel("h.min"));
    p21.setBorder(Sinavi3Bordures.bordnormal_);
    dureeIndispo.add(p21);

    final JPanel p23 = new JPanel();
    p23.add(new JLabel("Ordre loi d'Erlang de la dur�e: "));
    p23.add(this.loiProbaDuree_);
    p23.setBorder(Sinavi3Bordures.bordnormal_);
    dureeIndispo.add(p23);
    
    
    //creneaux
    JPanel p24=new JPanel();
    p24.setBorder(Sinavi3Bordures.createTitledBorder("Cr�neaux horaires d'acc�s"));//BorderFactory.createTitledBorder(SiporBordures.compound_,"Creneaux"));
    //p24.add(new JLabel("Creneaux d'ouverture: "));
    p24.add(this.creneau_);
    dureeIndispoCrenaux.add(p24);
    
    
    
    //validation
    final JPanel sc5 = new JPanel();
    sc5.add(this.validation_);
    sc5.setBorder(Sinavi3Bordures.compound_);
    contenu.add(sc5,BorderLayout.SOUTH);

    
    // affichage:
    setVisible(true);
  }

  /**
   * Methode de verification des coherences des donn�es
   * 
   * @return true si toutes les donn�es sont coh�rentes
   */
 public  boolean controle_creationChenal() {

    if (this.Nom.getText().equals("")) {
      new BuDialogError(donnees_.application_.getApp(), Sinavi3Implementation.isSinavi_, "Nom manquant.")
          .activate();
      return false;
    } else if (this.UPDATE
        && this.donnees_.listeBief_.existeDoublon(this.Nom.getText(), this.BIEF_A_MODIFIER_)) {
      new BuDialogError(donnees_.application_.getApp(), Sinavi3Implementation.isSinavi_, "Nom deja utilise.")
          .activate();
      return false;
    } else if (!this.UPDATE && this.donnees_.listeBief_.existeDoublon(this.Nom.getText(), -1)) {
      new BuDialogError(donnees_.application_.getApp(), Sinavi3Implementation.isSinavi_, "Nom deja utilise.")
          .activate();
      return false;
    }

    /*
     * if(this.longueur.getText().equals("")) { new
     * BuDialogError(donnees_.application_.getApp(),donnees_.application_.isSipor_, "Erreur!! longueur manquant!!!!")
     * .activate(); return false; }
     */
    if (this.profondeur.getText().equals("")) {
      new BuDialogError(donnees_.application_.getApp(), Sinavi3Implementation.isSinavi_,
          "Hauteur d'eau manquante.").activate();
      return false;
    }
    if (this.longueur.getText().equals("")) {
        new BuDialogError(donnees_.application_.getApp(), Sinavi3Implementation.isSinavi_,
            "Longueur manquante.").activate();
        return false;
      }
    if (this.largeur.getText().equals("")) {
        new BuDialogError(donnees_.application_.getApp(), Sinavi3Implementation.isSinavi_,
            "Largeur manquante.").activate();
        return false;
      }
    if (this.vitesseMax.getText().equals("")) {
        new BuDialogError(donnees_.application_.getApp(), Sinavi3Implementation.isSinavi_,
            "Vitesse manquante.").activate();
        return false;
      }
    /*
     * if(this.vitesseMax.getText().equals("")) { new
     * BuDialogError(donnees_.application_.getApp(),donnees_.application_.isSipor_, "Erreur!! Vitesse maxi
     * manquante!!!!") .activate();return false; }
     */

    // controle de la saisie des horaires:
    if (this.horaire_.semaineCreneau1HeureArrivee == -1 || this.horaire_.semaineCreneau1HeureDep == -1
        || this.horaire_.semaineCreneau2HeureArrivee == -1 || this.horaire_.semaineCreneau2HeureDep == -1) {
      new BuDialogError(donnees_.application_.getApp(), Sinavi3Implementation.isSinavi_,
          "Creneau non saisies correctement.").activate();
      return false;

    }
    
    if (this.dureeIndispo_.getText().equals("")) {
        new BuDialogError(donnees_.application_.getApp(), donnees_.application_.isSinavi_,
            "Duree d'indisponibilite manquante.").activate();
        return false;

      }

    
    /**
     * Cas loi de frequence= loi d erlang: il faut mettre une valeur moyenne
     */
    if (choixLoiFrequence.getSelectedIndex() == 0) {
      if (this.frequenceMoyenne_.getText().equals("")&& this.frequenceMoyenne2_.getText().equals("")) {
        new BuDialogError(donnees_.application_.getApp(), donnees_.application_.isSinavi_,
            "Frequence moyenne manquant.").activate();
        return false;

      }
    }
    

    // arriv� a ce stade de la methode tous les tests ont �chou�s donc c est du tout bon!
    return true;
  }

  /**
   * 
   */
  void creation_chenal() {
    if (controle_creationChenal()) {
      
      // test de la bonne saisie des horaires:
      horaire_.affichage();

      // saisie des donn�es du chenal avec FUDAA
      // FUDDDDDDDDDDDDDDDDDDAAAAAAAAAAAAAAAAAAAAA
      // SAISIR LES DONNEES

      // le nombre de cat�gories de navires afin d'avoir les regles de navigations ad�quates!
       Sinavi3Bief bief = null;
      
      if (!this.UPDATE) {
    	  bief = new Sinavi3Bief(this.donnees_.listeBateaux_.listeNavires_.size());
      } else 
    	  bief = donnees_.getListeBief_().retournerBief(this.BIEF_A_MODIFIER_);
      
      
      bief.nom_ = this.Nom.getText();
      // q.longueur_=Float.parseFloat(this.longueur.getText());
      
      /*if (((String) this.soumisMaree.getSelectedItem()).equals("oui")) {
        q.soumisMaree_ = true;
      } else {
        q.soumisMaree_ = false;
      }*/
      bief.profondeur_ = Double.parseDouble(this.profondeur.getText());
      bief.longueur_ = Double.parseDouble(this.longueur.getText());
      bief.largeur_ = Double.parseDouble(this.largeur.getText());
      bief.vitesse_ = Double.parseDouble(this.vitesseMax.getText());
      
//    loi indisponibilit�s
      bief.dureeIndispo_ = Float.parseFloat(this.dureeIndispo_.getText());
      if (choixLoiFrequence.getSelectedIndex() == 0) {
        bief.typeLoi_ = 0;
        float moy=0;
        if(!this.frequenceMoyenne_.getText().equals(""))
        	moy+=Float.parseFloat(this.frequenceMoyenne_.getText())*24;
        if(!this.frequenceMoyenne2_.getText().equals(""))
        	moy+=Float.parseFloat(this.frequenceMoyenne2_.getText());
        
        bief.frequenceMoyenne_ = moy;
        bief.loiFrequence_ = Integer.parseInt((String) this.loiProbaFrequence_.getSelectedItem());
      } else if (choixLoiFrequence.getSelectedIndex() == 1) {

        bief.typeLoi_ = 1;
        for (int i = 0; i < this.loiDeterministe_.size(); i++) {
          final CoupleLoiDeterministe c = new CoupleLoiDeterministe((CoupleLoiDeterministe) this.loiDeterministe_
              .get(i));
          bief.loiDeterministe_.add(c);

        }

      } else if (choixLoiFrequence.getSelectedIndex() == 2) {
        // cas loi journaliere
        bief.typeLoi_ = 2;
        for (int i = 0; i < this.loiDeterministe_.size(); i++) {
          final CoupleLoiDeterministe c = new CoupleLoiDeterministe((CoupleLoiDeterministe) this.loiDeterministe_
              .get(i));
          bief.loiDeterministe_.add(c);

        }

      }
      bief.loiIndispo_ = Integer.parseInt((String) this.loiProbaDuree_.getSelectedItem());

      
      
      // q.vitesse_=Double.parseDouble(this.vitesseMax.getText());
      bief.h_.recopie(horaire_);

      // affichage des regles de navigation;
      bief.reglesCroisement_.affichage();

      // FUDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDAAAAAAAAAAAAAAAA VAAAAAAARRRRRRRRRRRIAAAAAAABLES

      /**
       * ************************************** Ajout ou modification selon le mode
       */

      if (!UPDATE) {
        donnees_.listeBief_.ajout(bief);
        new BuDialogMessage(donnees_.application_.getApp(), Sinavi3Implementation.isSinavi_, "Le tron�on "
            + this.Nom.getText() + " a �t� ajout� avec succ�s.").activate();

        /**
         * Regles durees de parcours ajout d une ligne
         */
        donnees_.reglesVitesseBiefAvalant_.ajoutLigneAvalant(donnees_.listeBateaux_.listeNavires_.size(),donnees_);
        donnees_.reglesVitesseBiefMontant_.ajoutLigneMontant(donnees_.listeBateaux_.listeNavires_.size(),donnees_);

        //-- add element in network --//
        donnees_.getApplication().getNetworkEditor().addNewNetworkElement(
        		SimulationNetworkEditor.DEFAULT_VALUE_BIEF,
        		bief);
        
      } else {
        // recuperation des infos des gares
        bief.reglesCroisement_ = donnees_.listeBief_.retournerBief(BIEF_A_MODIFIER_).reglesCroisement_;
        bief.reglesTrematage_= donnees_.listeBief_.retournerBief(BIEF_A_MODIFIER_).reglesTrematage_;
        bief.gareAmont_ = donnees_.listeBief_.retournerBief(BIEF_A_MODIFIER_).gareAmont_;
        bief.gareAval_ = donnees_.listeBief_.retournerBief(BIEF_A_MODIFIER_).gareAval_;

        donnees_.listeBief_.modification(BIEF_A_MODIFIER_, bief);
        
        //-- modification si il y a lieu des vitesses --//
        donnees_.reglesVitesseBiefAvalant_.modifierValeurAvalant(BIEF_A_MODIFIER_, donnees_.listeBateaux_.listeNavires_.size(), donnees_);
        donnees_.reglesVitesseBiefMontant_.modifierValeurMontant(BIEF_A_MODIFIER_, donnees_.listeBateaux_.listeNavires_.size(), donnees_);
        
        new BuDialogMessage(donnees_.application_.getApp(), Sinavi3Implementation.isSinavi_, "Le bief "
            + this.Nom.getText() + " a �t� modifi� avec succ�s.").activate();
      }

      /**
       * Fin ajout ****************************
       */
  		
      //--On baisse le niveau de s�curit� pour forcer le test de coh�rence globale --//
      donnees_.baisserNiveauSecurite();

      // JOptionPane.showMessageDialog(null,"tron�on correctement ajout�!");
      // 2)sauvegarde des donnees
      donnees_.enregistrer();

      // affichage du tableau modifi� magique!!
      this.MENUCHENAL_.pile_.first(this.MENUCHENAL_.principalPanel_);

      // REMISE A ZERO DES COMPOSANTS...
      initialiser();

    }

  }

  /**
   * ******************************* METHODE QUI INDIQUE QUE LA FENETRE DE SAISIE D UN QUAI DEVIENT UNE FENETRE DE
   * MODIFICATION D UN chenal
   * 
   * @param numChenal indice du chenal dans la liste des cheneaux a modifier
   */

  void MODE_MODIFICATION(final int numChenal) {

    // 1) passage en mode modification
    this.UPDATE = true;

    // 2) recuperation de l indice du chenal
    this.BIEF_A_MODIFIER_ = numChenal;

    // 3) recuperation de la structure de chenal

    final Sinavi3Bief q = this.donnees_.listeBief_.retournerBief(this.BIEF_A_MODIFIER_);

    // 4)remplissage des donnn�es relatives au quai:
    this.Nom.setText(q.nom_);
    this.longueur.setText("" + (float) q.longueur_);
    this.largeur.setText("" + (float) q.largeur_);
    this.vitesseMax.setText("" + (float) q.vitesse_);
    this.profondeur.setText("" + (float) q.profondeur_);
    this.validation_.setText("modifier");
    /*
    if (q.soumisMaree_ == true) {
      this.soumisMaree.setSelectedIndex(1);
    } else {
      this.soumisMaree.setSelectedIndex(0);
    }
     */
    this.horaire_.recopie(q.h_);
    
    
    this.dureeIndispo_.setText("" + (float) q.dureeIndispo_);
    this.loiProbaDuree_.setSelectedIndex(q.loiIndispo_ - 1);
    this.frequenceMoyenne_.setText("");
    this.frequenceMoyenne2_.setText("");
    this.loiProbaFrequence_.setSelectedIndex(0);
    if (q.typeLoi_ == 0) {

      this.frequenceMoyenne_.setText("" + ((int) q.frequenceMoyenne_)/24);
      this.frequenceMoyenne2_.setText("" + ((int) q.frequenceMoyenne_)%24);
      this.loiProbaFrequence_.setSelectedIndex(q.loiFrequence_ - 1);
      this.choixLoiFrequence.setSelectedIndex(0);

    } else if (q.typeLoi_ == 1) {

      for (int i = 0; i < q.loiDeterministe_.size(); i++) {
        final CoupleLoiDeterministe c = new CoupleLoiDeterministe((CoupleLoiDeterministe) q.loiDeterministe_.get(i));
        if (i >= this.loiDeterministe_.size()) {
          this.loiDeterministe_.add(c);
        } else {
          this.loiDeterministe_.set(i, c);
        }
      }
      this.choixLoiFrequence.setSelectedIndex(1);
    } else if (q.typeLoi_ == 2) {
      // cas loi journaliere

      for (int i = 0; i < q.loiDeterministe_.size(); i++) {
        final CoupleLoiDeterministe c = new CoupleLoiDeterministe((CoupleLoiDeterministe) q.loiDeterministe_.get(i));
        if (i >= this.loiDeterministe_.size()) {
          this.loiDeterministe_.add(c);
        } else {
          this.loiDeterministe_.set(i, c);
        }
      }
      this.choixLoiFrequence.setSelectedIndex(2);

    }

    if (this.fenetreLoideter_ != null) {
      fenetreLoideter_.setVisible(false);
    }

    if (this.fenetreLoiJournaliere_ != null) {
      fenetreLoiJournaliere_.setVisible(false);
    }
    
    
  }

  /**
   * methode d'initialisation des champs de la frame
   */
  void initialiser() {
	  fenetreLoideter_ = null;
    this.horaire_ = new Sinavi3Horaire();
    this.Nom.setText(DefaultStructure.getDefaultName(new Sinavi3Bief(),donnees_.listeBief_.listeBiefs_.size()));
    this.longueur.setText("");
    this.largeur.setText("");
    this.profondeur.setText("");
    this.vitesseMax.setText("");
    this.validation_.setText("valider");
    // rafraichissement du tableau
    this.MENUCHENAL_.affichagePanel_.maj(donnees_);

    this.Nom.requestFocus();
    this.Nom.selectAll();

    
    this.frequenceMoyenne_.setText("");
    this.frequenceMoyenne2_.setText("");
    this.dureeIndispo_.setText("");
    this.loiProbaDuree_.setSelectedIndex(0);
    this.loiProbaFrequence_.setSelectedIndex(0);
    this.choixLoiFrequence.setSelectedIndex(0);

    this.loiDeterministe_ = new ArrayList();
  }

}
