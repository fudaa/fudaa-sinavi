package org.fudaa.fudaa.sinavi3;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.ArrayList;

import javax.swing.table.AbstractTableModel;

import com.memoire.bu.BuDialogError;

public class LoiDeterministeTableModel extends AbstractTableModel{

	
	
	 /**
	   * Donn�es de la loi deterministe
	   */
	  ArrayList loiDeterministe_ = new ArrayList();

	  
	  CoupleLoiDeterministe nouveauCouple=null;

	private Sinavi3DataSimulation donnees_;
	  
	  
	public LoiDeterministeTableModel(ArrayList _loiDeterministe, Sinavi3DataSimulation _donnee){
		loiDeterministe_=_loiDeterministe;
		donnees_=_donnee;
	}
	
	
	public int getColumnCount() {
		// TODO Auto-generated method stub
		return 2;
	}

	public int getRowCount() {
		// TODO Auto-generated method stub
		return loiDeterministe_.size()+1;
	}

	public String formatHeure(double value) {
		String data  = "" + value;
		if(data.contains(","))
			data = data.replace(",", ":");
		if(data.contains("."))
			data = data.replace(".", ":");
		
		return data;
		
	}
	public double unformatHeure(String data) throws ParseException {
		if(data.contains(":"))
			data = data.replace(":", ".");
		if(data.contains(","))
			data = data.replace(",", ".");
		double res = Double.parseDouble(data);
		return res;
		
	}
	
	public Object getValueAt(int rowIndex, int columnIndex) {
		
		if(rowIndex==getRowCount()-1)
			{
			if(nouveauCouple!=null)
			{
				if(columnIndex==0 && nouveauCouple.jour_!=-1)
					return ""+nouveauCouple.jour_;
					else
						if(columnIndex==1 && nouveauCouple.temps_!=-1)
							return formatHeure(nouveauCouple.temps_);
						
						
				
			}
				return "";
			}
		
		if(columnIndex==0 )
			return ""+((CoupleLoiDeterministe) this.loiDeterministe_.get(rowIndex)).jour_;
		
		return formatHeure( ((CoupleLoiDeterministe) this.loiDeterministe_.get(rowIndex)).temps_);
		
		
	}

	
	public String getColumnName(int column) {
		if(column==0)
			return "Jour";
		return "Horaire";
	}

	
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		// TODO Auto-generated method stub
		return true;
	}

	
	public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
		
		
		
		
		if(aValue=="") {loiDeterministe_.remove(this.loiDeterministe_.get(rowIndex)); this.fireTableStructureChanged(); return;}
		double temps=-1;
		int jour=-1;
		
		try {
			
			if(columnIndex==0)
				jour=Integer.parseInt((String)aValue);
			else
			{ 
				//temps=Double.parseDouble((String)aValue);
				temps = unformatHeure(aValue.toString());
			}
		
		
		} catch (NumberFormatException e) {
			new BuDialogError(donnees_.application_.getApp(), Sinavi3Implementation.isSinavi_,
                    "La saisie n'est pas correcte.").activate();
            return;
		} catch (ParseException e) {
			new BuDialogError(donnees_.application_.getApp(), Sinavi3Implementation.isSinavi_,
                    "La saisie n'est pas correcte.").activate();
            return;
		}
		
		
		if(columnIndex==1){
						//-- TEST HORAIRE --//
				
			if(temps>24 || temps<0){
				new BuDialogError(donnees_.application_.getApp(), Sinavi3Implementation.isSinavi_,
				"L'horaire doit �tre compris entre 0 et 24 heures.").activate();
				return;
			}
			
			//-- TEST FORMATTAGE HORAIRE --//
			String contenu=(String)aValue;

			if(contenu.lastIndexOf(".")!=-1){
				String unite=contenu.substring(contenu.lastIndexOf(".")+1, contenu.length());
				if(unite.length()>2){
					new BuDialogError(donnees_.application_.getApp(), Sinavi3Implementation.isSinavi_,
					"Il doit y avoir deux chiffres au maximum apr�s la virgule.").activate();

					return;
				}


				float valUnite=Float.parseFloat(unite);
				if(valUnite>=60){
					new BuDialogError(donnees_.application_.getApp(), Sinavi3Implementation.isSinavi_,
					"Les unites doivent �tre inf�rieures � 60 minutes.").activate();
					return;
				}
			}
		}// fin test formattage horaire
		else
		{
			
			//-- TEST JOUR --//
			if(jour>donnees_.params_.donneesGenerales.nombreJours)
			{
				new BuDialogError(donnees_.application_.getApp(), Sinavi3Implementation.isSinavi_,
				"Le jour doit �tre inf�rieur au nombre \nde jours apr�s la simulation.").activate();
				return;
			}
			else
				if(jour<=0){
					new BuDialogError(donnees_.application_.getApp(), Sinavi3Implementation.isSinavi_,
					"Le jour doit �tre positif.").activate();
					return;
				}
			
		}
		
		
		
		
		//-- MODE AJOUT --//
		if(rowIndex==loiDeterministe_.size())
		{
			//-- ajout de l'�l�ment --//
			if(nouveauCouple==null){
				nouveauCouple=new CoupleLoiDeterministe(-1,-1);
			}
			
			
			//-- ajout de la valeur selon la colonne --//
			if(columnIndex==0)
				nouveauCouple.jour_=jour;
			else
				nouveauCouple.temps_=temps;
			
			//-- test si le nouveau couple est complet, on l'ajoute � la liste des couples --//
			if(nouveauCouple.jour_!=-1 && nouveauCouple.temps_!=-1){
				loiDeterministe_.add(new CoupleLoiDeterministe(nouveauCouple.jour_, nouveauCouple.temps_));
				//reinitialisation du couple
				nouveauCouple=null;
				//la structure du butable doit changer: il faut le faire recalculer car il a une ligne de plus
				this.fireTableStructureChanged();
			}
			
		}
		else
		{
			//-- MODE MODIFICATION --//
			CoupleLoiDeterministe coupleAmodifier= (CoupleLoiDeterministe) this.loiDeterministe_.get(rowIndex);
		      
			if(columnIndex==0)
				coupleAmodifier.jour_=jour;
			else
				coupleAmodifier.temps_=temps;
				
			//mise a joru du contenu de la cellule
			this.fireTableCellUpdated(rowIndex, columnIndex);
		}
		
		
	}
	
	

}
