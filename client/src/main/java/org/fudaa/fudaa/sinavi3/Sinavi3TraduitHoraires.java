package org.fudaa.fudaa.sinavi3;

public class Sinavi3TraduitHoraires {

  public static float traduitMinutesEnHeuresMinutes(final double val) {
    final int valeur = (int) val;
    float horaire = 0;
    int heure = 0;
    int minute = 0;
    int reste = 0;

    heure = valeur / 60;
    reste = valeur % 60;

    minute = reste/* /60 */;

    // reste= reste%60;

    // if(reste>30)minute++;

    horaire = (float) (heure + minute / 100.0);

    return horaire;
  }
  
  public static float traduitHeuresMinutesEnHeures(final double val) {
	    final float valeur = (float) val;
	    float horaire = 0;
	    int heure = 0;
	    int minute = 0;

	    heure = (int) valeur;
	    minute = (int) ((valeur-heure)*100);

	    if(minute<60) horaire = (float) (heure + minute/60.);

	    return horaire;
	  }
  
  public static int traduitHeuresMinutesEnMinutes(final double val) {
	    final float valeur = (float) val;
	    int horaire = 0;
	    int heure = 0;
	    int minute = 0;

	    heure = (int) valeur;
	    minute = (int) ((valeur-heure)*100);

	    if(minute<60) horaire = heure*60 + minute;

	    return horaire;
	  }

  public static String traduitMinutesEnHeuresMinutes2OLD(final double val) {
    final int valeur = (int) val;
    int heure = 0;
    int minute = 0;
    int reste = 0;

    heure = valeur / 60;
    reste = valeur % 60;

    minute = reste/* /60 */;

    // reste= reste%60;

    // if(reste>30)minute++;
    String resultat;
if(minute>=10)
    resultat = "" + heure + "H " + minute + "Min";
else
	resultat = "" + heure + "H 0" + minute + "Min";


    return resultat;
  }



public static String traduitMinutesEnHeuresMinutes2(double val) {
    val=val*60.0;
	final int valeur = (int) val;
    int heure = 0;
    int minute = 0;
    int secondes=0;
    int reste = 0;

    heure = valeur / 3600;
    reste = valeur % 3600;

    minute = reste/60;
   
    secondes=reste%60;
    // reste= reste%60;

    // if(reste>30)minute++;
    String resultat;
if(minute>=10)
    resultat = "" + heure + "H " + minute + "Min";
else
	resultat = "" + heure + "H 0" + minute + "Min";
if(secondes!=0)
if(secondes>=10)
	resultat+=" "+secondes+"Sec";
else
	resultat+=" 0"+secondes+"Sec";

    return resultat;
  }



public static float traduitReverseMinutes(String value) {
    if(value == null)
        return 0;
    
    if(value.contains(":")) {
        String[] split = value.split(":");
        float minutes = Float.parseFloat(split[1]);
        float heures = Float.parseFloat(split[0]);
        return minutes + (heures *60.0f);
        
    }else 
        return Float.parseFloat(value);
}
public static String traduitMinutesEnHeuresMinutes3(double val) {
    return traduitMinutesEnHeuresMinutes3((float)val);
}

public static String traduitMinutesEnHeuresMinutes3(float val) {
    final int valeur = Math.round(val);
    int heure = 0;
    int minute = 0;

    heure = valeur / 60;
    minute = valeur % 60;

    String resultat;
    if(minute>=10)
    	resultat = "" + heure + ":" + minute;
    else
    	resultat = "" + heure + ":0" + minute;

    return resultat;
  }

public static String[] traduitMinutesEnJoursHeuresMinutes(float val) {
    final int valeur = Math.round(val);
    int heure = 0;
    int minute = 0;
    int jour;

    jour = (valeur /60) /24;
    heure = (valeur / 60) % 24;
    minute = valeur % 60;

    String[] resultat = new String[2];
    resultat[0] = String.valueOf(jour);
    if (heure >= 10) resultat[1] = String.valueOf(heure);
    else resultat[1] = "0" + heure;
    if(minute>=10) resultat[1] += ":" + minute;
    else resultat[1] += ":0" + minute;

    return resultat;
  }

}
