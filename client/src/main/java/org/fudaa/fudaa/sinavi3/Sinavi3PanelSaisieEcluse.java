package org.fudaa.fudaa.sinavi3;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

import org.fudaa.ebli.network.simulationNetwork.SimulationNetworkEditor;
import org.fudaa.fudaa.ressource.FudaaResource;
import org.fudaa.fudaa.sinavi3.structures.DefaultStructure;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuDialogError;
import com.memoire.bu.BuDialogMessage;
import com.memoire.bu.BuPanel;
import com.memoire.fu.FuLog;

/**
 * Panel de saisie des principales ecluses:
 * 
 * @author Adrien Hadoux
 */
public class Sinavi3PanelSaisieEcluse extends JPanel {

  // loi de proba indispo******
  Sinavi3FrameSaisieLoiJournaliere fenetreLoiJournaliere_ = null;
  Sinavi3FrameSaisieLoiDeterministe fenetreLoideter_ = null;
  Sinavi3TextFieldDuree dureeIndispo_ = new Sinavi3TextFieldDuree(3);
  Sinavi3TextFieldCoeff coeffBassinEpargne_ = new Sinavi3TextFieldCoeff(3);
  
  Sinavi3TextFieldInteger frequenceMoyenne_ = new Sinavi3TextFieldInteger(3);
  Sinavi3TextFieldInteger frequenceMoyenne2_ = new Sinavi3TextFieldInteger(3);
  String[] tabloi_ = { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10" };
  JComboBox loiProbaDuree_ = new JComboBox(tabloi_);
  JComboBox loiProbaFrequence_ = new JComboBox(tabloi_);
  String[] choixLoi_ = { "Erlang", "Deterministe" };
  JComboBox choixLoiFrequence = new JComboBox(choixLoi_);
  /** variable contenant le tableau des couples pour la loi deterministe */
  ArrayList loiDeterministe_ = new ArrayList();

  // ******fin loi duree indispo

  Sinavi3TextField cNom_ = new Sinavi3TextField(10);
  Sinavi3TextFieldFloat cLongueur_ = new Sinavi3TextFieldFloat(3);
  Sinavi3TextFieldFloat cHauteurChute = new Sinavi3TextFieldFloat(3);
  Sinavi3TextFieldFloat cLargeur_ = new Sinavi3TextFieldFloat(3);
  Sinavi3TextFieldInteger cdureeFBMontant_ = new Sinavi3TextFieldInteger(3);
  Sinavi3TextFieldInteger cdureeFBAvalant_ = new Sinavi3TextFieldInteger(3);
  Sinavi3TextFieldInteger dureeManoeuvreEntrant = new Sinavi3TextFieldInteger(3);
  Sinavi3TextField ccreneauetaleavtPMfin_ = new Sinavi3TextField(3);
  Sinavi3TextFieldDuree ccreneauetaleapresPMdeb_ = new Sinavi3TextFieldDuree(3);
  Sinavi3TextFieldInteger dureeManoeuvreSortant = new Sinavi3TextFieldInteger(3);
  Sinavi3TextFieldFloat profondeur_ = new Sinavi3TextFieldFloat(3);

  Sinavi3TextFieldInteger dureeManoeuvreEntrant2 = new Sinavi3TextFieldInteger(3);
  Sinavi3TextFieldInteger dureeManoeuvreSortant2 = new Sinavi3TextFieldInteger(3);
  
  
  // bouttons de validation des donn�es

  final BuButton creneau_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_continuer"), "Cr�neaux");

  final BuButton validation_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_oui"), "Valider");

  
  /**
   * Horaire qui sera saisie par l'interface de saisie des horaires
   */
  Sinavi3Horaire horaire_ = new Sinavi3Horaire();

  /**
   * Variable statique qui compte le nombre de fois que ce type d'objet a �t� ouvert
   */

  static int nbouverture = 0;

  /**
   * Donnees de la simulation:
   */
  Sinavi3DataSimulation donnees_;

  /**
   * Panel de gestion des ecluses fenetre qui a tous les droits tres utile pour les modifciation , les mise a jour et
   * les supressions d'�cluses
   */
  Sinavi3VisualiserEcluses MENUECLUSE_;

  /**
   * Mode dee la fenetre de sasie des donn�es si MODE=true alors le panek est en mode modification sinon c est en mode
   * ajout Par d�faut c est en mode ajout donc booleen initialis� a false.
   */
  boolean UPDATE = false;

  /**
   * Indice de l ecluse a modifier dans le cas ou l on est en mode modification
   */
  int ECLUSE_A_MODIFIER_;
  
  /**
   * Constructeur du panel de saisie des ecluses
   * 
   * @param d donnees de la simulation
   * @param ve panel qui permet de tout faire
   */

  public Sinavi3PanelSaisieEcluse(final Sinavi3DataSimulation d, final Sinavi3VisualiserEcluses ve) {

    // parametre de trop mis de cot�:
    // this.cdureepassageEcluseEtale_.setEnabled(false);

    donnees_ = d;
    MENUECLUSE_ = ve;
    /**
     * Les controles des composants:
     */
    this.cNom_.setToolTipText("Saisissez le nom de l'ecluse ici: ");
    this.cNom_.setText(DefaultStructure.getDefaultName(new Sinavi3Ecluse(),donnees_.listeEcluse_.listeEcluses_.size()));
    loiProbaDuree_
        .setToolTipText("Loi de probabilite d Erlang,choisissez un chiffre entre 1 a10 avec 10 equivalent � la loi reguliere");
    loiProbaFrequence_
        .setToolTipText("Loi de probabilite d Erlang,choisissez un chiffre entre 1 a10 avec 10 equivalent � la loi reguliere");

    this.cLongueur_.setToolTipText("saisissez la longueur en metre de l'ecluse");
    this.cHauteurChute.setToolTipText("saisissez la hauteur de chute en metre de l'ecluse");
    this.cLargeur_.setToolTipText("saisissez la largeur en metre de l'ecluse");
    this.cdureeFBMontant_.setToolTipText("saisisez la dur�e de la basinn�e dans le sens montant ");
    this.cdureeFBAvalant_.setToolTipText("saisisez la dur�e de la bassinn�e dans le sens avalant ");
    creneau_.setToolTipText("saisissez les horaires en cliquant sur ce bouton");
    this.dureeManoeuvreEntrant.setToolTipText("saisissez la dur�e par d�faut des manoeuvres dans le sens entrant");
    this.dureeManoeuvreSortant.setToolTipText("saisissez la dur�e par d�faut des manoeuvres dans le sens sortant");
    this.dureeManoeuvreEntrant2.setToolTipText("saisissez la seconde dur�e  des manoeuvres dans le sens entrant");
    this.dureeManoeuvreSortant2.setToolTipText("saisissez la seconde dur�e  des manoeuvres dans le sens sortant");
    
    this.profondeur_.setToolTipText("saisissez la profondeur de l'�cluse");

    

    

    

    

   

   
   

    

   

    /**
     * Listener des boutons
     */
    this.creneau_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        
        donnees_.application_.addInternalFrame(new Sinavi3FrameSaisieHorairesResume(horaire_));
      }
    });

    this.validation_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        
        creation_Ecluse();
      }
    });

    this.choixLoiFrequence.addActionListener(new ActionListener() {

      public void actionPerformed(final ActionEvent e) {
        /**
         * ETAPE 1: determiner le type de loi selectionn�
         */
        final int choixLoi = choixLoiFrequence.getSelectedIndex();

        if (choixLoi == 0) {
          // Cas 0: loi d erlang
          frequenceMoyenne_.setEnabled(true);
          frequenceMoyenne2_.setEnabled(true);
          loiProbaFrequence_.setEnabled(true);
          loiProbaDuree_.setEnabled(true);

        } else if (choixLoi == 1) {
          // cas 1: loi deterministe
          frequenceMoyenne_.setEnabled(false);
          frequenceMoyenne2_.setEnabled(false);
          loiProbaFrequence_.setEnabled(false);
          loiProbaDuree_.setEnabled(false);

          if (fenetreLoideter_ == null) {
            FuLog.debug("interface nulle");

            fenetreLoideter_ = new Sinavi3FrameSaisieLoiDeterministe(donnees_, loiDeterministe_, dureeIndispo_);

            // System.out.println("55555");
            fenetreLoideter_.setVisible(true);

            donnees_.application_.addInternalFrame(fenetreLoideter_);
          } else {
            FuLog.debug("interface ferm�e");
            if (fenetreLoideter_.isClosed()) {

              fenetreLoideter_ = new Sinavi3FrameSaisieLoiDeterministe(donnees_, loiDeterministe_, dureeIndispo_);

              donnees_.application_.addInternalFrame(fenetreLoideter_);

            } else {
              FuLog.debug("interface cas de figur restant autre que null et fermeture");

              fenetreLoideter_ = new Sinavi3FrameSaisieLoiDeterministe(donnees_, loiDeterministe_, dureeIndispo_);

              donnees_.application_.activateInternalFrame(fenetreLoideter_);
              donnees_.application_.addInternalFrame(fenetreLoideter_);

            }
          }

        } else if (choixLoi == 2) {
          // cas 2: loi journaliere
          frequenceMoyenne_.setEnabled(false);
          frequenceMoyenne2_.setEnabled(false);
          loiProbaFrequence_.setEnabled(false);
          loiProbaDuree_.setEnabled(false);

          // lancement de la frame de saisie des creneaux de la loi journaliere
          // donnees_.application_.addInternalFrame(new
          // SiporFrameSaisieLoiJournaliere(donnees_,loiDeterministe_,dureeIndispo_));

          if (fenetreLoiJournaliere_ == null) {
            FuLog.debug("interface nulle");

            fenetreLoiJournaliere_ = new Sinavi3FrameSaisieLoiJournaliere(donnees_, loiDeterministe_, dureeIndispo_);

            // System.out.println("55555");
            fenetreLoiJournaliere_.setVisible(true);
            donnees_.application_.addInternalFrame(fenetreLoiJournaliere_);
          } else {
            FuLog.debug("interface ferm�e");
            if (fenetreLoiJournaliere_.isClosed()) {

              fenetreLoiJournaliere_ = new Sinavi3FrameSaisieLoiJournaliere(donnees_, loiDeterministe_, dureeIndispo_);

              donnees_.application_.addInternalFrame(fenetreLoiJournaliere_);

            } else {
              FuLog.debug("interface cas de figur restant autre que null et fermeture");

              fenetreLoiJournaliere_ = new Sinavi3FrameSaisieLoiJournaliere(donnees_, loiDeterministe_, dureeIndispo_);

              donnees_.application_.activateInternalFrame(fenetreLoiJournaliere_);
              donnees_.application_.addInternalFrame(fenetreLoiJournaliere_);

            }
          }

        }

      }

    });

    /**
     * Affichage des composants
     */
   // this.setLayout(new BorderLayout());
    JPanel contenu=new JPanel(new BorderLayout());
    this.setBorder(Sinavi3Bordures.ecluse);
    this.add(contenu);
    
    //panel total
    final Box total = Box.createVerticalBox();
    contenu.add(total,BorderLayout.CENTER);
    
    //general
    final JPanel general=new JPanel(new GridLayout(1,2));
    general.setBorder(BorderFactory.createTitledBorder(Sinavi3Bordures.compound_,"G�n�ral"));
    total.add(general);
   
    final JPanel se1 = new JPanel(new FlowLayout(FlowLayout.LEFT));
    se1.add(new JLabel("Nom de l'�cluse:"));
    se1.add(this.cNom_);
    se1.setBorder(Sinavi3Bordures.bordnormal_);
    general.add(se1);
    
    final JPanel se12 = new JPanel(new FlowLayout(FlowLayout.LEFT));
    se12.add(new JLabel("Hauteur de chute:"));
    se12.add(cHauteurChute);
    se12.add(new JLabel("m�tres"));
    se12.setBorder(Sinavi3Bordures.bordnormal_);
    general.add(se12);
    
    
    //dimensions
    final JPanel dimensions=new JPanel(new GridLayout(1,3));
    dimensions.setBorder(BorderFactory.createTitledBorder(Sinavi3Bordures.compound_,"Dimensions"));
    total.add(dimensions);
    
    final JPanel se2 = new JPanel();
    se2.add(new JLabel("Longueur:"));
    se2.add(this.cLongueur_);
    se2.add(new JLabel("m�tres"));
    se2.setBorder(Sinavi3Bordures.bordnormal_);
    //se2.setBorder(BorderFactory.createTitledBorder(Sinavi3Bordures.bordnormal_,"longueur",0,0,null,Sinavi3Implementation.bleuClairSinavi));
    dimensions.add(se2);
    
    final JPanel se22 = new JPanel();
    se22.add(new JLabel("Largeur:"));
    se22.add(this.cLargeur_);
    se22.add(new JLabel("m�tres"));
    se22.setBorder(Sinavi3Bordures.bordnormal_);
    //se22.setBorder(BorderFactory.createTitledBorder(Sinavi3Bordures.bordnormal_,"largeur",0,0,null,Sinavi3Implementation.bleuClairSinavi));
    dimensions.add(se22);

    final JPanel se23 = new JPanel();
    se23.add(new JLabel("Profondeur:"));
    se23.add(this.profondeur_);
    se23.add(new JLabel("m�tres"));
    se23.setBorder(Sinavi3Bordures.bordnormal_);
    dimensions.add(se23);
    
    //durees
    Box etaleEcluse=Box.createVerticalBox();
    //etaleEcluse.setBorder(BorderFactory.createTitledBorder(Sinavi3Bordures.compound_,"Durees"));
    total.add(etaleEcluse);
    
    //etaleEcluse.add(se23);

    JPanel se331=new JPanel(new GridLayout(1,2));
    se331.setBorder(BorderFactory.createTitledBorder(Sinavi3Bordures.compound_,"Dur�e des bassin�es"));
    etaleEcluse.add(se331);
    
    final JPanel se3 = new JPanel();
   
    se3.add(new JLabel("Bassin�e montante:"));
    se3.add(this.cdureeFBMontant_);
    se3.add(new JLabel("min"));
    se3.setBorder(Sinavi3Bordures.bordnormal_);
    se331.add(se3);
    final JPanel se31 = new JPanel();
    se31.add(new JLabel("Bassin�e avalante:"));
    se31.add(this.cdureeFBAvalant_);
    se31.add(new JLabel("min"));
    se31.setBorder(Sinavi3Bordures.bordnormal_);
    se331.add(se31);

    JPanel  panelCoeffBassin=new JPanel();
    panelCoeffBassin.setBorder(BorderFactory.createTitledBorder(Sinavi3Bordures.compound_,"Coefficient de bassin d'�pargne"));
    etaleEcluse.add(panelCoeffBassin);
    panelCoeffBassin.add(new JLabel("Coefficient (entre 0 et 1):"));
    JPanel panelCoeffGrid = new JPanel();
    panelCoeffGrid.add(coeffBassinEpargne_);
    panelCoeffBassin.add(panelCoeffGrid);
    final JPanel se4 = new JPanel(new GridLayout(2,2));
    se4.setBorder(BorderFactory.createTitledBorder(Sinavi3Bordures.compound_,"Dur�e des manoeuvres"));
    BuPanel se41=new BuPanel(new FlowLayout(FlowLayout.RIGHT));
    se41.setBorder(Sinavi3Bordures.bordnormal_);
    se41.add(new JLabel("Manoeuvre entrante:"));
    se41.add(this.dureeManoeuvreEntrant);
    se41.add(new JLabel("min"));
    BuPanel se42=new BuPanel(new FlowLayout(FlowLayout.RIGHT));
    se42.setBorder(Sinavi3Bordures.bordnormal_);
    se42.add(new JLabel("Manoeuvre sortante:"));
    se42.add(this.dureeManoeuvreSortant);
    se42.add(new JLabel("min"));

    BuPanel se43=new BuPanel(new FlowLayout(FlowLayout.RIGHT));
    se43.setBorder(Sinavi3Bordures.bordnormal_);
    se43.add(new JLabel("Seconde Manoeuvre entrante:"));
    se43.add(this.dureeManoeuvreEntrant2);
    se43.add(new JLabel("min"));
    BuPanel se44=new BuPanel(new FlowLayout(FlowLayout.RIGHT));
    se44.setBorder(Sinavi3Bordures.bordnormal_);
    se44.add(new JLabel("Seconde Manoeuvre sortante:"));
    se44.add(this.dureeManoeuvreSortant2);
    se44.add(new JLabel("min"));
    
    
    se4.add(se41);
    se4.add(se42);
    se4.add(se43);
    se4.add(se44);
    etaleEcluse.add(se4);

    
    //indispo+duree+creneau
    JPanel indispoCreneau=new JPanel(new GridLayout(1,2));
    total.add(indispoCreneau);
    
    //loi indisponibilit�s
   Box loiIndispo= Box.createVerticalBox();
   loiIndispo.setBorder(BorderFactory.createTitledBorder(Sinavi3Bordures.compound_,"Loi d'indisponibilit�"));
   indispoCreneau.add(loiIndispo);
    
   
    final JPanel p31 = new JPanel();
    p31.add(new JLabel("Type de loi:"));
    p31.add(choixLoiFrequence);
    p31.setBorder(Sinavi3Bordures.bordnormal_);
    loiIndispo.add(p31);

    final JPanel p32 = new JPanel();
    p32.add(new JLabel("Ecart moyen:"));
    p32.add(this.frequenceMoyenne_);
    p32.add(new JLabel("jours"));
    p32.add(this.frequenceMoyenne2_);
    p32.add(new JLabel("heures"));
    p32.setBorder(Sinavi3Bordures.bordnormal_);
    loiIndispo.add(p32);

    final JPanel p33 = new JPanel();
    p33.add(new JLabel("Ordre loi d'Erlang de la fr�quence:"));
    p33.add(this.loiProbaFrequence_);
    p33.setBorder(Sinavi3Bordures.bordnormal_);
    loiIndispo.add(p33);

    
    //durees indispo + creneaux
    Box dureeIndispoCrenaux=Box.createVerticalBox();
    indispoCreneau.add(dureeIndispoCrenaux);
    
    //durees indispo
    Box dureeIndispo=Box.createVerticalBox();
    dureeIndispo.setBorder(BorderFactory.createTitledBorder(Sinavi3Bordures.compound_,"Dur�e d'indisponibilit�"));
    dureeIndispoCrenaux.add(dureeIndispo);
    
    
    final JPanel p21 = new JPanel();
    p21.add(new JLabel("Dur�e moyenne:"));
    p21.add(this.dureeIndispo_);
    p21.add(new JLabel("h.min"));
    p21.setBorder(Sinavi3Bordures.bordnormal_);
    dureeIndispo.add(p21);

    final JPanel p23 = new JPanel();
    p23.add(new JLabel("Ordre loi d'Erlang de la dur�e:"));
    p23.add(this.loiProbaDuree_);
    p23.setBorder(Sinavi3Bordures.bordnormal_);
    dureeIndispo.add(p23);
    
    
    //creneaux
    JPanel p24=new JPanel();
    p24.setBorder(BorderFactory.createTitledBorder(Sinavi3Bordures.compound_,"Cr�neaux horaires d'acc�s"));
    //p24.add(new JLabel("Creneaux d'ouverture: "));
    p24.add(this.creneau_);
    dureeIndispoCrenaux.add(p24);
    
    
    //validation
    final JPanel se6 = new JPanel();
    se6.setBorder(Sinavi3Bordures.compound_);
    se6.add(this.validation_);
    contenu.add(se6, BorderLayout.SOUTH);
    
    //add(total, BorderLayout.CENTER);
    
    
    
    setVisible(true);
  }

  /**
   * Methode de verification des coherence des donn�es
   * 
   * @return true si toutes les donn�es sont coh�rentes
   */
  boolean controle_creationEcluse() {

    if (this.cNom_.getText().equals("")) {
      new BuDialogError(donnees_.application_.getApp(), donnees_.application_.isSinavi_, "Nom manquant.")
          .activate();
      return false;
    } else if (this.UPDATE
        && donnees_.listeEcluse_.existeDoublon(this.cNom_.getText(), this.ECLUSE_A_MODIFIER_)) {
      new BuDialogError(donnees_.application_.getApp(), donnees_.application_.isSinavi_, "Nom d�j� pris.")
          .activate();
      return false;
    } else if (!this.UPDATE && donnees_.listeEcluse_.existeDoublon(this.cNom_.getText(), -1)) {
      new BuDialogError(donnees_.application_.getApp(), donnees_.application_.isSinavi_, "Nom d�j� pris.")
          .activate();
      return false;
    }

    if (this.cLongueur_.getText().equals("")) {
      new BuDialogError(donnees_.application_.getApp(), donnees_.application_.isSinavi_,
          "Longueur manquante.").activate();
      return false;
    }
    if (this.cLargeur_.getText().equals("")) {
      new BuDialogError(donnees_.application_.getApp(), donnees_.application_.isSinavi_,
          "Largeur manquante.").activate();
      return false;
    }
    if (this.cHauteurChute.getText().equals("")) {
        new BuDialogError(donnees_.application_.getApp(), donnees_.application_.isSinavi_,
            "Hauteur de chute manquante.").activate();
        return false;
      }
    if (this.profondeur_.getText().equals("")) {
      new BuDialogError(donnees_.application_.getApp(), donnees_.application_.isSinavi_,
          "Profondeur de l'�cluse manquante.").activate();
      return false;
    }
    else
    if (this.cdureeFBMontant_.getText().equals("")) {
      new BuDialogError(donnees_.application_.getApp(), donnees_.application_.isSinavi_,
          "Dur�e de fausse bassin�e montante manquante.").activate();
      return false;
    }
    else
    if (this.cdureeFBAvalant_.getText().equals("")) {
      new BuDialogError(donnees_.application_.getApp(), donnees_.application_.isSinavi_,
          "Dur�e de fausse bassin�e avalante manquante.").activate();
      return false;
    }
    
    if (this.dureeManoeuvreEntrant.getText().equals("")) {
      new BuDialogError(donnees_.application_.getApp(), donnees_.application_.isSinavi_,
          "Dur�e de manoeuvre entrante manquante.").activate();
      return false;
    }
    if (this.dureeManoeuvreEntrant2.getText().equals("")) {
        new BuDialogError(donnees_.application_.getApp(), donnees_.application_.isSinavi_,
            "Seconde Dur�e de manoeuvre entrante manquante.").activate();
        return false;
      }
    if (this.dureeManoeuvreSortant2.getText().equals("")) {
        new BuDialogError(donnees_.application_.getApp(), donnees_.application_.isSinavi_,
            "Seconde Dur�e de manoeuvre sortante manquante.").activate();
        return false;
      }
    if (this.dureeManoeuvreSortant.getText().equals("")) {
      new BuDialogError(donnees_.application_.getApp(), donnees_.application_.isSinavi_,
          "Dur�e de manoeuvre sortante manquante.").activate();
      return false;
    }
    
    //-- check value >-1 --//
    int val = Integer.parseInt(this.dureeManoeuvreSortant.getText());
    if(val<0) {
    	 new BuDialogError(donnees_.application_.getApp(), donnees_.application_.isSinavi_,
    	          "Dur�e de manoeuvre sortante n�gative.").activate();
    	      return false;
    }
    val = Integer.parseInt(this.dureeManoeuvreSortant2.getText());
    if(val<0) {
    	 new BuDialogError(donnees_.application_.getApp(), donnees_.application_.isSinavi_,
    	          "Dur�e de manoeuvre sortante 2 n�gative.").activate();
    	      return false;
    }
    val = Integer.parseInt(this.dureeManoeuvreEntrant.getText());
    if(val<0) {
    	 new BuDialogError(donnees_.application_.getApp(), donnees_.application_.isSinavi_,
    	          "Dur�e de manoeuvre entrante n�gative.").activate();
    	      return false;
    }
    val = Integer.parseInt(this.dureeManoeuvreEntrant2.getText());
    if(val<0) {
    	 new BuDialogError(donnees_.application_.getApp(), donnees_.application_.isSinavi_,
    	          "Dur�e de manoeuvre entrante 2 n�gative.").activate();
    	      return false;
    }

    // controle de la saisie des horaires:
    if (this.horaire_.semaineCreneau1HeureArrivee == -1 || this.horaire_.semaineCreneau1HeureDep == -1
        || this.horaire_.semaineCreneau2HeureArrivee == -1 || this.horaire_.semaineCreneau2HeureDep == -1) {
      new BuDialogError(donnees_.application_.getApp(), donnees_.application_.isSinavi_,
          "Cr�neaux non saisis correctement.").activate();
      return false;

    }

    if (this.dureeIndispo_.getText().equals("")) {
      new BuDialogError(donnees_.application_.getApp(), donnees_.application_.isSinavi_,
          "Dur�e d'indisponibilit� manquante.").activate();
      return false;

    }

    /**
     * Cas loi de frequence= loi d erlang: il faut mettre une valeur moyenne
     */
    if (choixLoiFrequence.getSelectedIndex() == 0) {
      if (this.frequenceMoyenne_.getText().equals("") && this.frequenceMoyenne2_.getText().equals("")) {
        new BuDialogError(donnees_.application_.getApp(), donnees_.application_.isSinavi_,
            "Fr�quence moyenne manquante.").activate();
        return false;

      }
    }

    // arriv� a ce stade de la methode tous les tests ont �chou�s donc c est du tout bon!
    return true;
  }

  /**
   * 
   */
  void creation_Ecluse() {
    if (controle_creationEcluse()) {
      System.out.println("Yes, tous les controles ont ete negatif!!!");
      // test de la bonne saisie des horaires:
      horaire_.affichage();

      // saisie des donn�es du chenal avec FUDAA
      // FUDDDDDDDDDDDDDDDDDDAAAAAAAAAAAAAAAAAAAAA
      // SAISIR LES DONNEES

       Sinavi3Ecluse ecluse = null;
      
      if (!this.UPDATE) {
    	  ecluse = new Sinavi3Ecluse();
      } else 
    	  ecluse = donnees_.getListeEcluse().retournerEcluse(this.ECLUSE_A_MODIFIER_);

      ecluse.nom_ = this.cNom_.getText();
      ecluse.largeur_ = Double.parseDouble(this.cLargeur_.getText());
      ecluse.longueur_ = Double.parseDouble(this.cLongueur_.getText());
      ecluse.hauteurchute = Double.parseDouble(this.cHauteurChute.getText());
      //e.tempsFausseBassinneeMontant_ = Double.parseDouble(this.cdureeFBMontant_.getText());
      //e.tempsFausseBassinneeAvalant_ = Double.parseDouble(this.cdureeFBAvalant_.getText());
      ecluse.tempsFausseBassinneeMontant_ = Integer.parseInt(this.cdureeFBMontant_.getText());
      ecluse.tempsFausseBassinneeAvalant_ = Integer.parseInt(this.cdureeFBAvalant_.getText());
      ecluse.profondeur_ = Double.parseDouble(this.profondeur_.getText());
      ecluse.dureeManoeuvreSortant_ = Integer.parseInt(this.dureeManoeuvreSortant.getText());
      ecluse.dureeManoeuvreEntrant_ = Integer.parseInt(this.dureeManoeuvreEntrant.getText());
      ecluse.dureeManoeuvreSortant2_ = Integer.parseInt(this.dureeManoeuvreSortant2.getText());
      ecluse.dureeManoeuvreEntrant2_ = Integer.parseInt(this.dureeManoeuvreEntrant2.getText());

      
      if(this.coeffBassinEpargne_.getText() != null && this.coeffBassinEpargne_.getText().length()>0) {
      	ecluse.coefficientBassinEpargne_ = Double.parseDouble(this.coeffBassinEpargne_.getText()); 
      }
      
      // loi indisponibilit�s
      ecluse.dureeIndispo_ = Float.parseFloat(this.dureeIndispo_.getText());
      if (choixLoiFrequence.getSelectedIndex() == 0) {
        ecluse.typeLoi_ = 0;
        float moy=0;
        if(!this.frequenceMoyenne_.getText().equals(""))
        	moy+=Float.parseFloat(this.frequenceMoyenne_.getText())*24;
        if(!this.frequenceMoyenne2_.getText().equals(""))
        	moy+=Float.parseFloat(this.frequenceMoyenne2_.getText());
        ecluse.frequenceMoyenne_ = moy; 
        ecluse.loiFrequence_ = Integer.parseInt((String) this.loiProbaFrequence_.getSelectedItem());
      } else if (choixLoiFrequence.getSelectedIndex() == 1) {

        ecluse.typeLoi_ = 1;
        for (int i = 0; i < this.loiDeterministe_.size(); i++) {
          final CoupleLoiDeterministe c = new CoupleLoiDeterministe((CoupleLoiDeterministe) this.loiDeterministe_
              .get(i));
          ecluse.loiDeterministe_.add(c);

        }

      } else if (choixLoiFrequence.getSelectedIndex() == 2) {
        // cas loi journaliere
        ecluse.typeLoi_ = 2;
        for (int i = 0; i < this.loiDeterministe_.size(); i++) {
          final CoupleLoiDeterministe c = new CoupleLoiDeterministe((CoupleLoiDeterministe) this.loiDeterministe_
              .get(i));
          ecluse.loiDeterministe_.add(c);

        }

      }
      ecluse.loiIndispo_ = Integer.parseInt((String) this.loiProbaDuree_.getSelectedItem());

      ecluse.h_.recopie(horaire_);
      
      
      

      if (!this.UPDATE) {
        /**
         * Mode ajout
         */
        donnees_.listeEcluse_.ajout(ecluse);
        
        //-- add element in network --//
        donnees_.getApplication().getNetworkEditor().addNewNetworkElement(
        		SimulationNetworkEditor.DEFAULT_VALUE_ECLUSE,
        		ecluse);
        
        
        new BuDialogMessage(donnees_.application_.getApp(), donnees_.application_.isSinavi_, "L'�cluse "
            + this.cNom_.getText() + " a �t� ajout�e avec succ�s.").activate();
        
        
        /**
         * Regles durees de maneuvre ajout d une ligne
         */
        
       donnees_.notifyAddEcluse(ecluse);
       
      } else {
        /**
         * Mode modification
         */
        // recuperation des informatiosn des gares amonts et avals
        ecluse.gareAmont_ = donnees_.listeEcluse_.retournerEcluse(ECLUSE_A_MODIFIER_).gareAmont_;
        ecluse.gareAval_ = donnees_.listeEcluse_.retournerEcluse(ECLUSE_A_MODIFIER_).gareAval_;

        donnees_.listeEcluse_.modification(this.ECLUSE_A_MODIFIER_, ecluse);
        
       
        new BuDialogMessage(donnees_.application_.getApp(), donnees_.application_.isSinavi_, "L'�cluse "
            + this.cNom_.getText() + " a �t� modifi�e avec succ�s.").activate();
      }
  	//--On baisse le niveau de s�curit� pour forcer le test de coh�rence globale --//
      donnees_.baisserNiveauSecurite();

      
      donnees_.getDureeManeuvresEcluses().modifyEcluse(ecluse);
      
      // 2)sauvegarde des donnees
      donnees_.enregistrer();

      // affichage du tableau modifi� magique!!
      this.MENUECLUSE_.pile_.first(this.MENUECLUSE_.principalPanel_);

      // changement du titre
      this.MENUECLUSE_.setTitle("Gestion des Ecluses");

      // REMISE A ZERO DES COMPOSANTS...
      this.initialiser();

    }

  }

  /**
   * Methode qui d�termine le type de l'�cluse: mode modification implique le passage du booleen a true et de passer
   * l'indice du tableau d'ecluse a modifier:
   * 
   * @param numEcluse indice du tableau d ecluse a modifier
   */
  void MODE_MODIFICATION(final int numEcluse) {

    // 1) passage en mode modification
    this.UPDATE = true;

    // 2) recuperation de l indice du quai
    this.ECLUSE_A_MODIFIER_ = numEcluse;

    // 3) recuperation de la structure de quai

    final Sinavi3Ecluse q = this.donnees_.listeEcluse_.retournerEcluse(this.ECLUSE_A_MODIFIER_);

    // 4)remplissage des donnn�es relatives au quai:
    this.cNom_.setText(q.nom_);
    this.cLongueur_.setText("" + (float) q.longueur_);
    this.cLargeur_.setText("" + (float) q.largeur_);
    this.cHauteurChute.setText("" + (float) q.hauteurchute);
    this.cdureeFBMontant_.setText("" + q.tempsFausseBassinneeMontant_);
    this.cdureeFBAvalant_.setText("" + q.tempsFausseBassinneeAvalant_);
    this.dureeManoeuvreSortant.setText("" + q.dureeManoeuvreSortant_);
    this.dureeManoeuvreEntrant.setText("" + q.dureeManoeuvreEntrant_);
    this.dureeManoeuvreSortant2.setText("" + q.dureeManoeuvreSortant2_);
    this.dureeManoeuvreEntrant2.setText("" + q.dureeManoeuvreEntrant2_);
    
    this.coeffBassinEpargne_.setText(""+ q.coefficientBassinEpargne_);
    this.profondeur_.setText("" + (float) q.profondeur_);
    // this.cdureepassageEcluseEtale.setText(""+q.)
    this.horaire_.recopie(q.h_);
    this.validation_.setText("Modifier");

    this.dureeIndispo_.setText("" + (float) q.dureeIndispo_);
    this.loiProbaDuree_.setSelectedIndex(q.loiIndispo_ - 1);
    this.frequenceMoyenne_.setText("");
    this.frequenceMoyenne2_.setText("");
    this.loiProbaFrequence_.setSelectedIndex(0);
    if (q.typeLoi_ == 0) {

      this.frequenceMoyenne_.setText("" + ((int) q.frequenceMoyenne_)/24);
      this.frequenceMoyenne2_.setText("" + ((int) q.frequenceMoyenne_)%24);
      this.loiProbaFrequence_.setSelectedIndex(q.loiFrequence_ - 1);
      this.choixLoiFrequence.setSelectedIndex(0);

    } else if (q.typeLoi_ == 1) {

      for (int i = 0; i < q.loiDeterministe_.size(); i++) {
        final CoupleLoiDeterministe c = new CoupleLoiDeterministe((CoupleLoiDeterministe) q.loiDeterministe_.get(i));
        if (i >= this.loiDeterministe_.size()) {
          this.loiDeterministe_.add(c);
        } else {
          this.loiDeterministe_.set(i, c);
        }
      }
      this.choixLoiFrequence.setSelectedIndex(1);
    } else if (q.typeLoi_ == 2) {
      // cas loi journaliere

      for (int i = 0; i < q.loiDeterministe_.size(); i++) {
        final CoupleLoiDeterministe c = new CoupleLoiDeterministe((CoupleLoiDeterministe) q.loiDeterministe_.get(i));
        if (i >= this.loiDeterministe_.size()) {
          this.loiDeterministe_.add(c);
        } else {
          this.loiDeterministe_.set(i, c);
        }
      }
      this.choixLoiFrequence.setSelectedIndex(2);

    }

    if (this.fenetreLoideter_ != null) {
      fenetreLoideter_.setVisible(false);
    }

    if (this.fenetreLoiJournaliere_ != null) {
      fenetreLoiJournaliere_.setVisible(false);
    }

  }

  /**
   * methode d'initialisation des champs de la frame
   */
  void initialiser() {
	  fenetreLoideter_ = null;
    this.horaire_ = new Sinavi3Horaire();
    this.cNom_.setText(DefaultStructure.getDefaultName(new Sinavi3Ecluse(),donnees_.listeEcluse_.listeEcluses_.size()));
    this.cLongueur_.setText("");
    this.cHauteurChute.setText("");
    this.cLargeur_.setText("");
    this.profondeur_.setText("");
    this.cdureeFBMontant_.setText("");
    this.cdureeFBAvalant_.setText("");
    this.ccreneauetaleapresPMdeb_.setText("");
    this.dureeManoeuvreSortant.setText("");
    this.dureeManoeuvreEntrant.setText("");
    this.dureeManoeuvreSortant2.setText("");
    this.dureeManoeuvreEntrant2.setText("");
    this.ccreneauetaleavtPMfin_.setText("");
    this.validation_.setText("Valider");
    // rafraichissement du tableau
    this.MENUECLUSE_.affichagePanel_.maj(donnees_);
    this.cNom_.requestFocus();
    this.cNom_.selectAll();

    this.frequenceMoyenne_.setText("");
    this.frequenceMoyenne2_.setText("");
    this.dureeIndispo_.setText("");
    this.loiProbaDuree_.setSelectedIndex(0);
    this.loiProbaFrequence_.setSelectedIndex(0);
    this.choixLoiFrequence.setSelectedIndex(0);
    this.loiDeterministe_ = new ArrayList();

    
    this.dureeManoeuvreEntrant.setText("-1");
    this.dureeManoeuvreSortant.setText("-1");
    this.dureeManoeuvreEntrant2.setText("-1");
    this.dureeManoeuvreSortant2.setText("-1");
    
  }

}
