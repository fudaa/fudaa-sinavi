
package org.fudaa.fudaa.sinavi3.ui.results;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.ByteArrayInputStream;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;
import javax.swing.table.TableColumn;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.image.CtuluImageExport;
import org.fudaa.dodico.corba.sinavi3.Sinavi3ResultatConsoEau;
import org.fudaa.ebli.graphe.BGraphe;
import org.fudaa.fudaa.ressource.FudaaResource;
import org.fudaa.fudaa.sinavi3.FonctionsSimu;
import org.fudaa.fudaa.sinavi3.Sinavi3AlgorithmeAttentesGenerales;
import org.fudaa.fudaa.sinavi3.Sinavi3Bordures;
import org.fudaa.fudaa.sinavi3.Sinavi3DataSimulation;
import org.fudaa.fudaa.sinavi3.Sinavi3GestionResultats;
import org.fudaa.fudaa.sinavi3.Sinavi3InternalFrame;
import org.fudaa.fudaa.sinavi3.Sinavi3JFreeChartCamembert;
import org.fudaa.fudaa.sinavi3.Sinavi3TextFieldInteger;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import com.memoire.bu.BuButton;
import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuScrollPane;
import com.memoire.bu.BuTabbedPane;
import com.memoire.bu.BuTable;

/**
 * classe de gestion des resultats de la consommation eau.
 * 
 * @version $Version$
 * @author Adrien Hadoux
 */
public class Sinavi3ResultatsConsommationEau extends Sinavi3InternalFrame {

  /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

/**
   * ensemble des donn�es du tableau sous la forme de data.
   */
  Object[][] data_;
 
  /**
   * Graphe associ�e aux r�sultats de la g�n�ration de bateaux.
   */
  BGraphe graphe_ = new BGraphe();

  /**
   * Camembert des proportions.
   */
  ChartPanel camembert_;
  
  /**
   * Panel de gestion du camembert.
   */
  BuPanel panelCamembert_=new BuPanel(new BorderLayout());
  
  /**
   * panels principaux de la fenetre.
   */
  BuPanel panelPrincipal_ = new BuPanel();
  BuPanel panelPrincipalClassique_ = new BuPanel();
  
  /**
   * Panel de selection des preferences.
   */
 
  BuPanel selectionPanel1_;
 

  
  /**
   * Panel des options: type affichages, colonnes � faire figurer.
   */
  BuPanel optionPanelTableau_ = new BuPanel(new BorderLayout());
   
  String[] typeGraphe_ = { "Lignes", "Barres"};
  JComboBox<String> choixTypeGraphe_ = new JComboBox<String>(typeGraphe_);

  
  BuPanel optionPanelGraph_ = new BuPanel(new BorderLayout());
  
 

  Sinavi3TextFieldInteger valSeuilLitres_ = new Sinavi3TextFieldInteger(3);
 
  JCheckBox valideSeuil_ = new JCheckBox("Seuil:", false);
  int valeurSeuil_;
  boolean seuil_;
  

  /**
   * Tableau r�capitulatif des r�sultats de la simulation.
   */
  BuTable tableau_;
  
  
  String[] titreTableau_ = { "SAS Ecluse", "Consommation eau" };
  
  /**
   * Panels tabbed qui g�rent les 2 onglets, ie les 2 versions d'affichage des r�sultats.
   */
  BuTabbedPane panelPrincipalAffichage_ = new BuTabbedPane();
  BuTabbedPane panelPrincipalAffichageClassique_ = new BuTabbedPane();
  
  /**
   * Panel contenant le bouton quitter
   */
  BuPanel panelQuitter_ = new BuPanel();

  /**
   * Panel contenant la repr�sentation graphique
   */
  BuPanel panelGraphAffichage_ = new BuPanel(new BorderLayout());

    
  /**
   * Panel cniotenant le tableau et les boutns de controles
   */
  BuPanel panelGestionTableau_ = new BuPanel();
  /*
   * panel de gestion du tableau et de ses options
   */
  BuPanel panelTableau_ = new BuPanel();
  
  /**
   * panel d'affichage du tableau
   */
  BuPanel panelTableauAffichage_ = new BuPanel();

  /**
   * panel de gestion des boutons
   */
  BuPanel controlPanel_ = new BuPanel();
  
  
  /**
   * panel de gestion des boutons du camembert
   */
  BuPanel controlPanelCamembert_ = new BuPanel();
  
  /**
   * Panel de gestion des boutons des courbes
   */
  BuPanel controlPanelCourbes_ = new BuPanel();


  /**
   * panel de gestion des repr�sentations graphiques
   */
  BuPanel panelGraph_ = new BuPanel();
  
  /**
   * panel de gestion des repr�sentations graphiques
   */
  BuPanel panelResult_ = new BuPanel();

  /**
   * panel de gestion des courbes
   */
  BuPanel panelCourbe_ = new BuPanel();

 
  /**
   * combolist qui permet de selectionenr les lignes du tableau a etre affich�es:
   */
 
  JComboBox<String> listeEcluses_=new JComboBox<String>();
 
  /**
   * buoton de generation des resultats
   */
  private final BuButton exportationExcel_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_generer"), "Exporter");
  final BuButton exportationGraphe_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_generer"), "Exporter");
 
  private final BuButton quitter_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_quitter"), "Quitter");
  private final BuButton lancerRecherche_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_oui"), "Rechercher");
  private final BuButton rafraichirSeuil_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_rafraichir"), "");
  
  Border raisedBevel_ = BorderFactory.createRaisedBevelBorder();
  Border loweredBevel_ = BorderFactory.createLoweredBevelBorder();
  Border compound_ = BorderFactory.createCompoundBorder(raisedBevel_, loweredBevel_);
  Border bordnormal_ = BorderFactory.createEtchedBorder();
  /**
   * donnees de la simulation.
   */
  Sinavi3DataSimulation donnees_;

  /**
   * Constructeur de la sous fenetre de gestion des resultats.
   */
  public Sinavi3ResultatsConsommationEau(final Sinavi3DataSimulation _donnees) {
    super("Consommation d'eau par SAS �cluse", true, true, true, true);

    // recuperation des donn�es de la simulation
    donnees_ = _donnees;

    // premier calcul par defaut execut�: entre le Tron�on 0 et le Tron�on 0 dans le sens aval
    Sinavi3AlgorithmeAttentesGenerales.calculTrajetDepuisListeTrajet(donnees_, 0, 0, 0, 0, 0);

    setSize(820, 600);
    setBorder(Sinavi3Bordures.compound_);
    this.getContentPane().setLayout(new /* BorderLayout() */GridLayout(1, 1));
    this.panelPrincipal_.setLayout(new BorderLayout());
    this.getContentPane().add(this.panelPrincipal_/* ,BorderLayout.CENTER */);
    this.panelPrincipal_.add(this.panelPrincipalAffichage_, BorderLayout.CENTER);
    panelPrincipalAffichage_.addTab("R�sultats de synth�se ", this.panelPrincipalClassique_);
    
    this.panelPrincipal_.add(panelQuitter_, BorderLayout.SOUTH);
    panelQuitter_.add(quitter_);
    
    this.panelPrincipalClassique_.setLayout(new BorderLayout());
    this.panelPrincipalClassique_.add(this.optionPanelTableau_, BorderLayout.WEST);
    this.panelPrincipalClassique_.add(panelPrincipalAffichageClassique_, BorderLayout.CENTER);
    panelPrincipalAffichageClassique_.addTab("Tableau", FudaaResource.FUDAA.getIcon("crystal_arbre"), panelTableau_);
    panelPrincipalAffichageClassique_.addTab("R�partition par type d'attente", FudaaResource.FUDAA.getIcon("crystal_graphe"), panelCamembert_);
    panelPrincipalAffichageClassique_.addTab("Exploitation graphique", FudaaResource.FUDAA.getIcon("crystal_graphe"), panelGraph_);
    
     
    /*******************************************************************************************************************
     * gestion du panel de selection
  	 */

    
    tableauChoixEcluses_ = new JCheckBox[this.donnees_.getListeEcluse().getListeEcluses().size()];
    for (int i = 0; i < this.donnees_.getListeEcluse().getListeEcluses().size(); i++) {
      this.tableauChoixEcluses_[i] = new JCheckBox(this.donnees_.getListeEcluse().retournerEcluse(i).getNom(), true);
      this.tableauChoixEcluses_[i].addActionListener(this);
    }
    

 
    
    /*******************************************************************************************************************
     * gestion du panel des options pour les r�sultats de synth�se
     ******************************************************************************************************************/

    BuGridLayout lo=new BuGridLayout(1,5,0,true,false,false,false);
    final BuPanel panoption = new BuPanel(lo);
    final BuScrollPane panoptionGestionScroll = new BuScrollPane(panoption);
    this.optionPanelTableau_.add(panoptionGestionScroll);

    
    final Box bVert2 = Box.createVerticalBox();
    bVert2.setBorder(this.bordnormal_);
    for (int i = 0; i < this.tableauChoixEcluses_.length; i++) {
      bVert2.add(this.tableauChoixEcluses_[i]);
    }
    final TitledBorder bordure2 = BorderFactory.createTitledBorder(BorderFactory
        .createEtchedBorder(EtchedBorder.LOWERED), "Ecluses");
    bVert2.setBorder(bordure2);
    panoption.add(bVert2);
    
    /*******************************************************************************************************************
     * gestion du panel des options graphiques
     ******************************************************************************************************************/
    this.choixTypeGraphe_.addActionListener(this);
   
    
    /*******************************************************************************************************************
     * gestion du panel courbes panelTableau_
     ******************************************************************************************************************/
    this.panelTableau_.setLayout(new BorderLayout());
   // this.panelTableau_.add(this.optionPanelTableau_, BorderLayout.WEST);
    this.panelTableau_.add(this.panelGestionTableau_, BorderLayout.CENTER);
    
    /*******************************************************************************************************************
     * gestion du panel tableau panelGestionTableau_
     ******************************************************************************************************************/

    // etape 1: architecture du panel panelGestionTableau_ et du panel ascenseur qui le contiendra
    panelGestionTableau_.setLayout(new BorderLayout());
    final JScrollPane asc = new JScrollPane(this.panelTableauAffichage_);

    // ajout au centre du panel qui contiendra le tableau d affichage
    this.panelGestionTableau_.add(asc, BorderLayout.CENTER);

    // panel qui contient les differents boutons
    this.controlPanel_.add(exportationExcel_);
    this.panelGestionTableau_.add(this.controlPanel_, BorderLayout.SOUTH);
    
    final Border bordureTab = BorderFactory.createEtchedBorder();
    controlPanel_.setBorder(bordureTab);
    
    //panel du camembert
    affichageCamembert( false);
    this.panelCamembert_.add(camembert_,BorderLayout.CENTER);
    
   
    this.panelCamembert_.add(this.controlPanelCamembert_, BorderLayout.SOUTH);
    
    // etape 2: remplissage du comboliste avec les noms des ecluses
    this.listeEcluses_.addItem("Tous");
    for (int i = 0; i < donnees_.getListeEcluse().getListeEcluses().size(); i++) {
      this.listeEcluses_.addItem("" + donnees_.getListeEcluse().retournerEcluse(i).getNom());
      this.listeEcluses_.addItem("" + donnees_.getListeEcluse().retournerEcluse(i).getNom());
    }

    // etape 3: gestion de l affichage du tableau de donn�es
    affichageTableau(-1);

    // etape 4: listener du combolist afin de pouvoir selectionner l'�cluse qui nous interesse
    this.listeEcluses_.addActionListener(new ActionListener() {
        public void actionPerformed(final ActionEvent _e) {
          // evenement du clic sur le bouton
          final int val = listeEcluses_.getSelectedIndex();
          affichageCamembert( true);

        }
      });
    
    // bouton qui permet de generer le contenu du tableau en ficheir excel:
    this.exportationExcel_.setToolTipText("Exporte le contenu du tableau au format xls");
    exportationExcel_.addActionListener(new ActionListener() {
    	public void actionPerformed(final ActionEvent _e) {
      	  Sinavi3GestionResultats.exportTableau(Sinavi3ResultatsConsommationEau.this, titreTableau_, data_);
           //Sinavi3GestionResultats.exportReport(_donnees, false,false,false,false,true,false);
        }
    });

    /*******************************************************************************************************************
     * gestion du panel courbes panelGraph_
     ******************************************************************************************************************/
    this.panelGraph_.setLayout(new BorderLayout());
    this.panelGraph_.add(this.optionPanelGraph_, BorderLayout.WEST);
    this.panelGraph_.add(this.panelGraphAffichage_, BorderLayout.CENTER);
    
    /*******************************************************************************************************************
     * gestion du panel courbes panelCourbe_
     ******************************************************************************************************************/

    final String descriptionGraphe = affichageGraphe();
    this.graphe_.setFluxDonnees(new ByteArrayInputStream(descriptionGraphe.getBytes()));
    this.panelGraphAffichage_.add(this.graphe_, BorderLayout.CENTER);

    // bouton de generation du fichier image
    exportationGraphe_.setToolTipText("Exporte le graphe au format image");
    exportationGraphe_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent _e) {
        CtuluImageExport.exportImageFor(donnees_.getApplication(), graphe_);
      }
    });
    
    rafraichirSeuil_.setEnabled(false);
    rafraichirSeuil_.setToolTipText("Actualise la valeur du seuil graphique");

    // creation du panel des boutons des courbes:
    this.panelGraphAffichage_.add(this.controlPanelCourbes_, BorderLayout.SOUTH);

    BuPanel exportGraphe = new BuPanel();
    exportGraphe.add(exportationGraphe_);
    this.controlPanelCourbes_.add(exportationGraphe_);
    this.controlPanelCourbes_.add(new JLabel("          "));
    this.controlPanelCourbes_.add(valideSeuil_);
    this.controlPanelCourbes_.add(valSeuilLitres_);
    this.controlPanelCourbes_.add(new JLabel("Litres"));
   
    this.controlPanelCourbes_.add(rafraichirSeuil_);
    this.controlPanelCourbes_.add(new JLabel("          "));
    this.controlPanelCourbes_.add(choixTypeGraphe_);
    
    choixTypeGraphe_.setToolTipText("Modifie le style de la repr�sentation graphique");
    
    final Border bordure = BorderFactory.createEtchedBorder();
    controlPanelCourbes_.setBorder(bordure);

    valideSeuil_.addActionListener(this);
    rafraichirSeuil_.addActionListener(this);

    /** listener des boutons quitter */
    this.quitter_.setToolTipText("Ferme la sous-fen�tre");

    final ActionListener actionQuitter = new ActionListener() {
      public void actionPerformed(ActionEvent _e) {
        Sinavi3ResultatsConsommationEau.this.windowClosed();
      }
    };
    
    this.quitter_.addActionListener(actionQuitter);
    
  }

  JCheckBox[] tableauChoixEcluses_;
  
  /**
   * Methode d'affichage du tableau.
   * @param indiceEcluse indice de l'�cluse.
   */
  void affichageTableau(final int indiceEcluse) {
    data_ = new Object[this.donnees_.getListeEcluse().getListeEcluses().size()+3][titreTableau_.length];
    int indiceTbaleau = 0;
    if (indiceEcluse < 0) {

      for (int i = 0; i < this.donnees_.getListeEcluse().getListeEcluses().size(); i++) {
        if (this.tableauChoixEcluses_[i].isSelected()) {
          data_[indiceTbaleau][0] = this.donnees_.getListeEcluse().retournerEcluse(i).getNom();
          data_[indiceTbaleau][1] = 
        		  this.donnees_.getListeResultatsConsommationEau_().get(i).consommationEau;
          indiceTbaleau++;
          }
        }
      int cl=2;
      data_[++indiceTbaleau][0]="TOTAL: "+(indiceTbaleau-1)+" �cluses affich�es";
    } else if (indiceEcluse < this.donnees_.getListeEcluse().getListeEcluses().size()) {
      // on affiche uniquement la ligne selectionn� par le combolist:
      data_ = new Object[1][this.titreTableau_.length];
      data_[0][0] = this.donnees_.getListeEcluse().retournerEcluse(indiceEcluse).getNom();
      data_[0][1] = this.donnees_.getListeResultatsConsommationEau_().get(indiceEcluse).consommationEau;
    }
    // etape 3: creation du tableau a partir des donn�es g�n�r�es ci dessus:
    this.tableau_ = new BuTable(data_, this.titreTableau_) {
      public boolean isCellEditable(final int _row, final int _col) {
        return false;
      }
    };

    // etape 3.5: dimensionnement des colonnes du tableau 
    for(int i=0; i<tableau_.getModel().getColumnCount();i++){
        TableColumn column = tableau_.getColumnModel().getColumn(i);
               column.setPreferredWidth(150); 
        }
    
    // etape 4: ajout sdu tableau cr�� dans l'interface
    org.fudaa.fudaa.sinavi.factory.ColumnAutoSizer.sizeColumnsToFit(tableau_);
    tableau_.revalidate();
    this.panelTableauAffichage_.removeAll();
    this.panelTableauAffichage_.setLayout(new BorderLayout());
    this.panelTableauAffichage_.add(tableau_.getTableHeader(), BorderLayout.PAGE_START);
    this.panelTableauAffichage_.add(this.tableau_, BorderLayout.CENTER);

    // mise a jour de l'affichage
    this.revalidate();
    this.updateUI();

  }
  


  /**
   * Methode qui permet de d�crire le graphe � afficher.
   * 
   * @param _histo: si true, on affiche un histogramme plutot qu'un graphe classique
   * @return chaine: chaine qui contient la des cription de la chaine de caracteres.
   */
  public String affichageGraphe() {	  
		
	boolean histo = false;
	if (this.choixTypeGraphe_.getSelectedIndex() == 1) histo = true;

	// determiner el nombre de cat�gories de navires selectionn�s
	int nbEcluses = 0;
	for (int k = 0; k < this.tableauChoixEcluses_.length; k++) {
	  if (this.tableauChoixEcluses_[k].isSelected()) {
		  nbEcluses++;
	  }
	}

     int indiceEcluses = 0;

    String g = "";

    g += "graphe\n{\n  titre \"Consommation d'eau des SAS des �cluses  \"\n" ;
    g += "  animation non\n";
    g += "  legende " + "oui" + "\n";
    g += " marges\n {\n";
    g += " gauche 100\n"; g += " droite 100\n"; g += " haut 50\n"; g += " bas 30\n }\n";

    g += "  axe\n  {\n"; // abscisses
    g += "    titre \"Ecluse" + "\"\n";
    g += "    orientation " + "horizontal" + "\n";
    g += "    graduations non\n";
    g += "    minimum " + 0 + "\n";
    g += "    maximum " + (nbEcluses + 2)
        + "\n";
    g += "  }\n";

    g += "  axe\n  {\n"; // Ordonn�es
    g += "    titre \"Consommation" + "\"\n";
    g += "    unite \"litres";
    g += "\"\n";
    g += "    orientation " + "vertical" + "\n";
    g += "    graduations oui\n";
    g += "    minimum " + 0 + "\n";
    g += "    maximum " + donnees_.getMaxConsommationEau()+ "\n";
    g += "  }\n";


    // ******************************debut conso eau************************************************
    g += "  courbe\n  {\n";
    g += "    titre \"";
    g += "consommation d'eau";
    g += "\"\n";
    g += "    type ";
    if (histo) g += "histogramme"; else g += "courbe";
    g += "\n";
    g += "    aspect\n {\n";
    g += "contour.largeur 1 \n";
    g += "surface.couleur  	BB0000 \n";
    g += "texte.couleur 000000 \n";
    g += "contour.couleur ";
    if (histo) g += "000000"; else g += "BB0000";
    g += " \n";
    g += "    }\n";
    g += "    valeurs\n    {\n";
    
    indiceEcluses = 0;
    for (int n = 0; n < this.donnees_.getListeEcluse().getListeEcluses().size(); n++) {
      if (this.tableauChoixEcluses_[n].isSelected()) {
        g += (indiceEcluses + 1)
            + " ";
        indiceEcluses++;
        g +=  donnees_.getListeResultatsConsommationEau_().get(n).consommationEau;
        g += "\n etiquette  \n \"" + this.donnees_.getListeEcluse().retournerEcluse(n).getNom() + "\" \n"/* + */
            + "\n";
      }
    }// din du pour remplissage des coordonn�es
    g += "    }\n";
    g += "  }\n";

    // //******************************fin conso eau************************************************

    

    
  
    if (seuil_) {
      /**
       * declaration d'un seuil
       */
      g += " contrainte\n";
      g += "{\n";
      g += "titre \"seuil \"\n";
      g += " type max\n";
      g += " valeur " + valeurSeuil_ + CtuluLibString.LINE_SEP_SIMPLE;
      g += " \n }\n";      
    }
    
  //-- doit on le supprimer?
    g += "    }\n";
    g += "  }\n";
    
    System.out.println("graphe \n\n "+g);

    return g;
  }

  
  
  
  public int getNbSelectedEcluses() {
	  int cpt=0;
	  
	  for(int i=0;i<this.tableauChoixEcluses_.length;i++)
	  if (this.tableauChoixEcluses_[i].isSelected()) {
		  cpt++;
	  }
	  return cpt;
  }
  
  public void affichageCamembert( boolean _refresh){
	  
	  int nbEclusesSelected=getNbSelectedEcluses();
	  
	  int[] tab = new int[nbEclusesSelected];
	  String[] libelles=new String[nbEclusesSelected];
	  int cpt=0;
	  
	  for (int i = 0; i < this.donnees_.getListeEcluse().getListeEcluses().size(); i++) {
	        if (this.tableauChoixEcluses_[i].isSelected()) {
	        	tab[cpt] = 
	        			(int)Math.round( this.donnees_.getListeResultatsConsommationEau_().get(i).consommationEau);
	        	libelles[cpt] = "Consommation eau de l'�cluse " + donnees_.getListeEcluse().retournerEcluse(cpt).getNom();  
	        	cpt++;
	          }
	        }
	  
	
	  String  titre = "Conseommation d'eau des SAS des �cluses";
	  JFreeChart chart;
	  chart = Sinavi3JFreeChartCamembert.genererCamembert(tab, libelles, titre, false);
	  if (!_refresh) this.camembert_ = new ChartPanel(chart);
	  else this.camembert_.setChart(chart);
  }
  
  /**
   * Listener principal des elements de la frame: tres important pour les composant du panel de choix des �l�ments.
   * 
   * @param ev evenements qui apelle cette fonction
   */

  public void actionPerformed(final ActionEvent ev) {
    final Object source = ev.getSource();

    // action commune a tous les �v�nements: redimensionnement de la fenetre
    final Dimension actuelDim = this.getSize();
    final Point pos = this.getLocation();
     if (source == this.choixTypeGraphe_) {
      // mise a jour des courbes
      final String descriptionCourbes = this.affichageGraphe();
      this.graphe_.setFluxDonnees(new ByteArrayInputStream(descriptionCourbes.getBytes()));     
    }    
   
    if (source == this.lancerRecherche_) {
       	// mise a jour des affichages:
    	affichageTableau(this.listeEcluses_.getSelectedIndex() - 1);
    	// mise a jour des courbes
    	final String descriptionCourbes = this.affichageGraphe();
    	this.graphe_.setFluxDonnees(new ByteArrayInputStream(descriptionCourbes.getBytes()));    
    	//-- mise a jour du camembert --//
    	affichageCamembert( true);

    } else if (source == this.valideSeuil_ || source == this.rafraichirSeuil_) {
    	this.rafraichirSeuil_.setEnabled(this.valideSeuil_.isSelected());
    	if (this.valideSeuil_.isSelected() && !this.valSeuilLitres_.getText().equals("") ) {
    		// booleen passe a true
    		this.seuil_ = true;
    		// on recupere al valeure du seuil choisie par l utilisateur
    		valeurSeuil_ = (int) Float.parseFloat(this.valSeuilLitres_.getText());
    		// on redesssinne l histogramme en tenant compte du seuil de l utilisateur
    		final String descriptionGraphe = this.affichageGraphe();
    		this.graphe_.setFluxDonnees(new ByteArrayInputStream(descriptionGraphe.getBytes()));
    	} else {
    		// booleen passe a false
    		this.seuil_ = false;
    		// on redesssinne le graphe sans seuil
    		final String descriptionGraphe = this.affichageGraphe();
    		this.graphe_.setFluxDonnees(new ByteArrayInputStream(descriptionGraphe.getBytes()));
    	}
    }

    // si la source provient d un navire du tableau de checkBox
    boolean trouve = false;
    for (int k = 0; k < this.tableauChoixEcluses_.length && !trouve; k++) {
    	if (source == this.tableauChoixEcluses_[k]) {
    		trouve = true;
    		affichageTableau(-1);
    		final String descriptionCourbes = this.affichageGraphe();
    		this.graphe_.setFluxDonnees(new ByteArrayInputStream(descriptionCourbes.getBytes()));
    		this.affichageCamembert( true);
    	}    	
    }

    // on redimensionne la fenetre comme elle etais avant manipulation des elements graphique
    this.setSize(actuelDim);
    this.setLocation(pos);
  }// fin de actionPerformed

  /**
   * Methode qui s active lorsque l'on quitte l'application.
   */
  protected void windowClosed() {
    System.out.print("Fin de la fenetre de gestion des cercles!!");
    dispose();
  }
  
  
}
