package org.fudaa.fudaa.sinavi3;

/**
 * 
 */
import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JLabel;
import javax.swing.JPanel;

import org.fudaa.ebli.network.simulationNetwork.DefaultNetworkUserObject;
import org.fudaa.ebli.network.simulationNetwork.SimulationNetworkEditor;
import org.fudaa.fudaa.ressource.FudaaResource;
import org.fudaa.fudaa.sinavi3.structures.DefaultStructure;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuDialogError;

/**
 * Panel de saisie des dif�rents Bassins
 * 
 * @author Adrien Hadoux
 */
public class Sinavi3PanelSaisieGare extends JPanel {

  // attributs:
  static int nbouverture = 0;
  /**
   * Jtext du nom a saisir
   */
  Sinavi3TextField nom_ = new Sinavi3TextField(10);

  /**
   * Bouton de validation du gare
   */
  final BuButton validation_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_oui"), "Valider");

  /**
   * donn�es de la simulation
   */
  Sinavi3DataSimulation donnees_;

  
  /**
   * Fenetre principale de gestion des bassins
   */

  Sinavi3VisualiserGares MENUGARES_;

  /**
   * Booleen qui indique si le panel est en mode modification MODE_MODIFICATION_ON=true =>>> mode modif sinon mode
   * saisie classique Par defaut r�gl� sur mode classique de saisie(booleen=false)
   */
  boolean UPDATE = false;

  /**
   * Indice de la gare a modifier:
   */
  int GARE_A_MODIFIER_;

  /**
   * Constructeur du panel de saisie des bassins
   * 
   * @param d donn�es de la simulation
   */
  public Sinavi3PanelSaisieGare(final Sinavi3DataSimulation d, final Sinavi3VisualiserGares vb) {

    // recuperation de la fenetre de commande principale
    MENUGARES_ = vb;
    donnees_ = d;

    nbouverture++;
    this.setLayout(new BorderLayout());

    this.nom_.setText(DefaultStructure.getDefaultName(null,this.donnees_.listeGare_.listeGares_.size()));
    this.nom_.setToolTipText("Nom de la gare");

    validation_.setToolTipText("Valide les donn�es saisies");

    validation_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        System.out.println("validation du noom de la gare:");
        if (nom_.getText().equals("")) {
          new BuDialogError(donnees_.application_.getApp(), Sinavi3Implementation.isSinavi_,
              "La gare n'a pas de nom.").activate();
        } else if (UPDATE && donnees_.listeGare_.existeDoublon(nom_.getText(), GARE_A_MODIFIER_)) {
          new BuDialogError(donnees_.application_.getApp(), Sinavi3Implementation.isSinavi_,
              "Ce nom est d�j� attribu� � une autre gare.").activate();
        } else if (!UPDATE && donnees_.listeGare_.existeDoublon(nom_.getText(), -1)) {
          new BuDialogError(donnees_.application_.getApp(), Sinavi3Implementation.isSinavi_,
              "Ce nom est d�j� attribu� � une autre gare.").activate();
        } else {

          /**
           * MODE SAISIE/
           */
          if (!UPDATE) {// récupération de la donnée du bassin
            // FUDAA

            // 1)ajout de la chaine de caractere du bassin
            donnees_.listeGare_.ajout(nom_.getText());
            
          //-- add element in network --//
            DefaultNetworkUserObject userobject = new DefaultNetworkUserObject(nom_.getText(),true);
            donnees_.getApplication().getNetworkEditor().addNewNetworkElement(
            		SimulationNetworkEditor.DEFAULT_VALUE_GARE,
            		userobject);
            
            // 2)sauvegarde des donnees
            donnees_.enregistrer();
          } else {
            /***********************************************************************************************************
             * Mode MODIFICATION:
             */
            donnees_.listeGare_.modification(GARE_A_MODIFIER_, nom_.getText());
            // JOptionPane.showMessageDialog(null,"Gare "+nom_.getText()+" correctement modifi�e!");

          }
          // 2)mise a jour dans le tableau
          MENUGARES_.affichagePanel_.maj(donnees_);

          // 3)remise a zero des composants

          nom_.setText(DefaultStructure.getDefaultName(null,donnees_.listeGare_.listeGares_.size()));

          // 4)mode modification remis a false par defaut
          UPDATE = false;
          MENUGARES_.mode_.setText("Ajout d'une gare:");
          validation_.setText("Valider");
          MENUGARES_.suppression_.setEnabled(true);
        }

      }

    });

    // architecture des composants
    final JPanel panneau = new JPanel(new GridLayout(2,1));

    panneau.add(new JLabel("Nom de la gare: "));
    panneau.add(this.nom_);
    panneau.setBorder(Sinavi3Bordures.bordnormal_);
    this.add(panneau, BorderLayout.CENTER);

    this.add(validation_, BorderLayout.SOUTH);
    this.setBorder(Sinavi3Bordures.bordnormal_);

    // affichage:
  }

  /**
   * Methode qui transforme le type de la fenetre de saisie en fenetre de modification:
   * 
   * @param numBassin indice du tableau de bassin a modifier
   */

  public void MODE_MODIFICATION(final int numGare) {

    UPDATE = true;
    this.nom_.setText(this.donnees_.listeGare_.retournerGare(numGare));
    GARE_A_MODIFIER_ = numGare;
    this.MENUGARES_.mode_.setText("Modification d'une gare:");
    this.validation_.setText("Valider");
    this.MENUGARES_.suppression_.setEnabled(false);
  }

}
