/*
 * @file         SiporOutilsDonnees.java
 * @creation     1999-10-01
 * @modification $Date: 2007-11-23 11:28:16 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.fudaa.sinavi3;

import org.fudaa.dodico.corba.sipor.SParametresBassin;
import org.fudaa.dodico.corba.sipor.SParametresCategorie;
import org.fudaa.dodico.corba.sipor.SParametresCategories;
import org.fudaa.dodico.corba.sipor.SParametresCreneauMareeDansBassin;
import org.fudaa.dodico.corba.sipor.SParametresCreneauxHorairesDansBassin;
import org.fudaa.dodico.corba.sipor.SParametresDonneesGenerales;
import org.fudaa.dodico.corba.sipor.SParametresEcluseDansBassin;
import org.fudaa.dodico.corba.sipor.SParametresMaree;
import org.fudaa.dodico.corba.sipor.SParametresPerturbateur;
import org.fudaa.dodico.corba.sipor.SParametresQuai;
import org.fudaa.dodico.corba.sipor.SParametresQuais;
import org.fudaa.dodico.corba.sipor.SParametresReglesSecurite;
import org.fudaa.dodico.corba.sipor.SParametresReglesSecuriteBassin;
import org.fudaa.dodico.corba.sipor.SParametresSipor;
import org.fudaa.dodico.corba.sipor.SResultatsIterations;
import org.fudaa.fudaa.commun.projet.FudaaProjet;

/**
 * Un ensemble de m�thodes facilitant l'acc�s aux donn�es.
 * 
 * @version $Revision: 1.1 $ $Date: 2007-11-23 11:28:16 $ by $Author: hadouxad $
 * @author Nicolas Chevalier
 */
public class Sinavi3OutilsDonnees {
  /** Projet dans lequel il faut chercher les donn�es. */
  FudaaProjet projet_;

  // Constructeur
  public Sinavi3OutilsDonnees(final FudaaProjet _projet) {
    projet_ = _projet;
  }

  /**
   * Mutateur de projet.
   */
  public void setProjet(final FudaaProjet _projet) {
    projet_ = _projet;
  }

  /** Cr�� une structure de param�tres par d�faut et l'ajoute dans projet_. */
  public void creationDonnees() {
    // Structure regroupant toutes les donn�es
    final SParametresSipor params = new SParametresSipor();
    // Construction des donn�es g�n�rales
    final SParametresDonneesGenerales dg = new SParametresDonneesGenerales();
    dg.arretProgApresRappelDonnees = true; // 3 bool�ens devenus inutiles
    dg.impressionRappelDonnees = true; // fix�s � true et impossibles
    dg.impressionPassagesIntermediaires = true; // � modifier.
    dg.heureDebutReelSimulation = 44640; // 1 mois d'initialisation
    dg.heureFinSimulation = 570240; // 12 mois de simulation
    params.donneesGenerales = dg;
    // Construction du bassin 1
    final SParametresBassin b1 = new SParametresBassin();
    b1.ecluse = new SParametresEcluseDansBassin(); // Ecluse
    b1.creneauMaree = new SParametresCreneauMareeDansBassin();
    // Cr�neau de mar�e
    b1.creneauxHoraires = new SParametresCreneauxHorairesDansBassin();
    // Cr�neaux horaires
    params.bassin1 = b1;
    // Construction du bassin 2
    final SParametresBassin b2 = new SParametresBassin();
    b2.ecluse = new SParametresEcluseDansBassin(); // Ecluse
    b2.creneauMaree = new SParametresCreneauMareeDansBassin();
    // Cr�neau de mar�e
    b2.creneauxHoraires = new SParametresCreneauxHorairesDansBassin();
    // Cr�neaux horaires
    params.bassin2 = b2;
    // Construction du bassin 3
    final SParametresBassin b3 = new SParametresBassin();
    b3.ecluse = new SParametresEcluseDansBassin(); // Ecluse
    b3.creneauMaree = new SParametresCreneauMareeDansBassin();
    // Cr�neau de mar�e
    b3.creneauxHoraires = new SParametresCreneauxHorairesDansBassin();
    // Cr�neaux horaires
    params.bassin3 = b3;
    // Par d�faut : utilisation du bassin 1
    projet_.addParam(Sinavi3Resource.utilisationBassin1, new Boolean(true));
    projet_.addParam(Sinavi3Resource.utilisationBassin2, new Boolean(false));
    projet_.addParam(Sinavi3Resource.utilisationBassin3, new Boolean(false));
    // Construction de la mar�e.
    final SParametresMaree maree = new SParametresMaree();
    maree.periodeVivesEaux = 21240; // 354 heures par d�faut
    maree.periodeMaree = 746; // 12h26 par d�faut
    params.maree = maree;
    // Quais, cat�gories et perturbateur
    params.quais = new SParametresQuais(0, new SParametresQuai[20]);
    params.categories = new SParametresCategories(0, 0, new SParametresCategorie[30]);
    params.perturbateur = new SParametresPerturbateur(Integer.MAX_VALUE, 0, 1, 0);
    final SParametresReglesSecurite rs = new SParametresReglesSecurite();
    rs.rsPE = new SParametresReglesSecuriteBassin();
    rs.rsPE.entree = new long[8];
    rs.rsPE.sortie = new long[8];
    rs.rsB1 = new SParametresReglesSecuriteBassin();
    rs.rsB1.entree = new long[8];
    rs.rsB1.sortie = new long[8];
    rs.rsB2 = new SParametresReglesSecuriteBassin();
    rs.rsB2.entree = new long[8];
    rs.rsB2.sortie = new long[8];
    rs.rsB3 = new SParametresReglesSecuriteBassin();
    rs.rsB3.entree = new long[8];
    rs.rsB3.sortie = new long[8];
    params.reglesSecurite = rs;
    projet_.addParam(Sinavi3Resource.parametres, params);
  }

  /** Pour l'acc�s � la totalit� des param�tres. */
  public SParametresSipor getParametres() {
    return (SParametresSipor) projet_.getParam(Sinavi3Resource.parametres);
  }

  /** Pour l'acc�s aux donn�es g�n�rales. */
  public SParametresDonneesGenerales getDonneesGenerales() {
    return getParametres().donneesGenerales;
  }

  /** Pour l'acc�s au bassin 1. */
  public SParametresBassin getBassin1() {
    return getParametres().bassin1;
  }

  /** Pour l'acc�s au bassin 2. */
  public SParametresBassin getBassin2() {
    return getParametres().bassin2;
  }

  /** Pour l'acc�s au bassin 3. */
  public SParametresBassin getBassin3() {
    return getParametres().bassin3;
  }

  /** Pour l'acc�s aux quais. */
  public SParametresQuais getQuais() {
    return getParametres().quais;
  }

  /** Pour l'acc�s � la mar�e. */
  public SParametresMaree getMaree() {
    return getParametres().maree;
  }

  /** Pour l'acc�s aux cat�gories. */
  public SParametresCategories getCategories() {
    return getParametres().categories;
  }

  /** Pour l'acc�s au perturbateur. */
  public SParametresPerturbateur getPerturbateur() {
    return getParametres().perturbateur;
  }

  /** Pour l'acc�s aux r�gles de s�curit�. */
  public SParametresReglesSecurite getReglesSecurite() {
    return getParametres().reglesSecurite;
  }

  /** Pour savoir si le bassin 1 est utilis�. */
  public boolean getUtilisationBassin1() {
    return ((Boolean) projet_.getParam(Sinavi3Resource.utilisationBassin1)).booleanValue();
  }

  /** Pour savoir si le bassin 2 est utilis�. */
  public boolean getUtilisationBassin2() {
    return ((Boolean) projet_.getParam(Sinavi3Resource.utilisationBassin2)).booleanValue();
  }

  /** Pour savoir si le bassin 3 est utilis�. */
  public boolean getUtilisationBassin3() {
    return ((Boolean) projet_.getParam(Sinavi3Resource.utilisationBassin3)).booleanValue();
  }

  /**
   * Pour savoir quels sont les bassins utilis�s.
   * 
   * @return tableau de 3 bool�ens : l'�l�ment i indique si le bassin i est utilis�.
   */
  public boolean[] getUtilisationBassins() {
    final boolean[] tab = new boolean[3];
    tab[0] = getUtilisationBassin1();
    tab[1] = getUtilisationBassin2();
    tab[2] = getUtilisationBassin3();
    return tab;
  }

  /** Pour l'acc�s aux r�sultats. */
  public SResultatsIterations getResultats() {
    return (SResultatsIterations) projet_.getResult(Sinavi3Resource.resultats);
  }
}
