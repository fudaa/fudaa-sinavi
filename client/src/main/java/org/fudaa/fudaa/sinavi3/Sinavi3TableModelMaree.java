package org.fudaa.fudaa.sinavi3;

import javax.swing.table.AbstractTableModel;

import com.memoire.bu.BuDialogError;


/**
 * Classe qui d�finit un modele pour le tableau de saisie des donn�es marees
 *@version $Version$
 * @author hadoux
 *
 */

public class Sinavi3TableModelMaree extends AbstractTableModel{

	double[] tableau_;
	String[] titreColonnes_;
	
	
		public Sinavi3TableModelMaree(double[] _tableau){
			tableau_=_tableau;
			
			titreColonnes_=new String[2];
			titreColonnes_[0]="Intervalle";
			titreColonnes_[1]="Nb douzi�mes";
		}
	
		public int getColumnCount() {
			// TODO Auto-generated method stub
			return 2;
		}

		public int getRowCount() {
			// TODO Auto-generated method stub
			return tableau_.length;
		}

		
		/** Methode qui retourne l'objet correspondant  **/
		public Object getValueAt(int rowIndex, int columnIndex) {
			// TODO Auto-generated method stub
			if(columnIndex==0)
				return ""+(rowIndex+1);
			
			return ""+(tableau_[rowIndex]);
		}

		
		
		//-- ajout d'une valeur
		public void setValueAt(Object value, int rowIndex, int columnIndex) {
			
			double valeur;
			try {
				valeur = Double.parseDouble((String)value);
				
				
				//-- Gestion des erreurs --//
			if(valeur < 0) {
	            new BuDialogError(null, Sinavi3Implementation.isSinavi_,
	                    "La valeur ne peut �tre n�gative.").activate();
	                return;
	              }
			else
				if(valeur>12)
				{
		            new BuDialogError(null, Sinavi3Implementation.isSinavi_,
		                    "La valeur doit �tre inf�rieure � 12.").activate();
		                return;
		              }
				
			
			} catch (NumberFormatException e) {
				new BuDialogError(null, Sinavi3Implementation.isSinavi_,
			              "La valeur n'est pas coh�rente: ce n'est pas un reel.").activate();

			          return;
			}
			
			
			
			
			//mise a jour du ArrayList durees de parcours (cot� m�tier)
			tableau_[rowIndex]=valeur;
	        
			//mise a jour du tableau (cot� graphique)
			fireTableCellUpdated(rowIndex, columnIndex);
			
			
		}

		
		public boolean isCellEditable(int rowIndex, int columnIndex) {
			// TODO Auto-generated method stub
			if(columnIndex==0)
				return false;
			else
				return true;
		}
		
		/**Methode qui permet de recuperer els noms des colonnes du tableau **/
		public String getColumnName(int column) {
			
			return titreColonnes_[column];
		}
		
		  
	  }
  

