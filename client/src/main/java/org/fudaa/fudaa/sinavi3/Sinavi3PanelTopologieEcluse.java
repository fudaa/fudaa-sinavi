package org.fudaa.fudaa.sinavi3;

/**
 * 
 */

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.DefaultCellEditor;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JInternalFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.Border;
import javax.swing.table.TableColumn;

import org.fudaa.ebli.network.simulationNetwork.DefaultNetworkUserObject;
import org.fudaa.ebli.network.simulationNetwork.NetworkUserObject;
import org.fudaa.fudaa.ressource.FudaaResource;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuDialogError;
import com.memoire.bu.BuDialogMessage;

/**
 * Panel d'affichage des donn�es topologiques sous forme d un tableau: cette classe permet de saisir les gares amont
 * et aval de chacun des chenaux cr��s.
 * AAAAAAAAAAAAAAAAAAATTTTTTTTTTTTTTTTTTTTTTTTTTEEEEEEEEEEEEEEEEEEEENNNNNNNNNNNNNNNNNNNNTTTTTTTTTTTIIIIIIIIIOOOOOONN ce
 * panel ne peut s utiliser qu a condition qu'il y ait au moins 2 gares pour joindre les 2 bouts!!!
 * 
 * @author Adrien Hadoux
 */
public class Sinavi3PanelTopologieEcluse extends Sinavi3InternalFrame {

  /**
   * JCombo qui permettra de choisir pour chaque chenal les gares amont et gares avales
   */
  JComboBox ComboGare;

  /**
   * Descriptif des elements des colonnes
   */
  String[] titreColonnes = { "Nom objet", "Gare Amont", "Gare Aval" };

  /**
   * Tableau de type JTable qui contiendra les donn�es des bassins
   */

  JTable tableau;

  /**
   * Nombre d'objets
   */
  int nbElem;

  /**
   * Bouton de validation des donn�es topolgiques saisies pour le chenal
   */
  final BuButton validation = new BuButton(FudaaResource.FUDAA.getIcon("crystal_oui"), "valider");

  /**
   * Fenetre qui contiendra le panel
   */
  JPanel global = new JPanel();

  /**
   * Bordure du tableau
   */

  Border borduretab = BorderFactory.createLoweredBevelBorder();

  /**
   * donn�es de la simulation
   */
  Sinavi3DataSimulation d;

  /**
   * constructeur du panel d'affichage des bassins
   * 
   * @param d donn�es de la simulation
   */
  Sinavi3PanelTopologieEcluse(final Sinavi3DataSimulation de) {
    super("", true, true, true, true);
    // recuperation des donn�es de la simulation
    d = de;

    global.setLayout(new BorderLayout());

    /**
     * TEST SI AU MOINS 2 GARES EXISTENT
     */
    if (d.listeGare_.listeGares_.size() < 2) {
      new BuDialogError(d.application_.getApp(), Sinavi3Implementation.isSinavi_,
          "Erreur!! Il n existe pas a umoins 2 gares!!").activate();
    }

    /**
     * Initialisation du nombre de cheneaux
     */

    nbElem = d.listeEcluse_.listeEcluses_.size();

    // remplissage des comboBox en onction des donn�es
    this.remplissage();
    // afichage des elements dans le tableau.
    this.affichage();

    // listener du bouton de validation

    this.validation.addActionListener(new ActionListener() {

      public void actionPerformed(final ActionEvent e) {

        miseAjourTopologie();

      }

    });

    /**
     * Creation de la fenetre
     */

    setTitle("Topologie des ecluses");
    setSize(400, 300);
    setBorder(Sinavi3Bordures.compound_);
    getContentPane().setLayout(new BorderLayout());

    final JScrollPane ascenceur = new JScrollPane(global);

    getContentPane().add(ascenceur, BorderLayout.CENTER);

    final JPanel controlPanel = new JPanel();
    controlPanel.add(validation);
    getContentPane().add(controlPanel, BorderLayout.SOUTH);

    // affichage de la frame
    setVisible(true);

  }

  /**
   * Methode de remplissage des JComboBox et des donn�es par d�fauts pour chaque objet.
   */
  void remplissage() {

    // 1)cr�ation du combo i des gares amont
    this.ComboGare = new JComboBox();

    // 3)remplissage du tableau de gare avec toutes les donn�es des gares
    for (int j = 0; j < d.listeGare_.listeGares_.size(); j++) {
      // ajout successif des differents noms de gare
      this.ComboGare.addItem(d.listeGare_.retournerGare(j));

    }

  }

  /**
   * Methode d affichage des composants du JTable et du tableau de combo Cette methode est a impl�menter dans les
   * classes d�riv�es pour chaque composants
   */
  void affichage() {

    final Object[][] ndata = new Object[nbElem][3];

    for (int i = 0; i < nbElem; i++) {

      ndata[i][0] = d.listeEcluse_.retournerEcluse(i).nom_;

      // dernieres valeur rentr�es pour les gares
      ndata[i][1] = d.listeGare_.retournerGare(d.listeEcluse_.retournerEcluse(i).gareAmont_);// comboBox i des gares
                                                                                              // amont
      ndata[i][2] = d.listeGare_.retournerGare(this.d.listeEcluse_.retournerEcluse(i).gareAval_); // combobox i des
                                                                                                  // gares avals

    }

    this.tableau = new JTable(ndata, this.titreColonnes) {
      public boolean isCellEditable(final int row, final int col) {
        if (col == 0) {
          return false;
        } else {
          return true;
        }
      }
    };

    tableau.setBorder(this.borduretab);

    // tableau.revalidate();
    // this.removeAll();
    this.global.add(/* ascenceur */tableau.getTableHeader(), BorderLayout.PAGE_START);
    this.global.add(tableau, BorderLayout.CENTER);

    final JPanel controlePanel = new JPanel();
    controlePanel.add(validation);

    /**
     * Creation pour les colonnes 2 et 3 d'objets de type jcombobox
     */
    final TableColumn gareAmontColumn = tableau.getColumnModel().getColumn(1);
    final TableColumn gareAvalColumn = tableau.getColumnModel().getColumn(2);
    gareAmontColumn.setCellEditor(new DefaultCellEditor(this.ComboGare));
    gareAvalColumn.setCellEditor(new DefaultCellEditor(this.ComboGare));

    global.add(controlePanel, BorderLayout.SOUTH);
    this.revalidate();
    this.updateUI();

  }

  /**
   * Methode qui permet de verifier la pertinence des donn�es saisies: IE: v�rifie que les gares choisies en amont et
   * avales sont bien diff�rentes pour un m�me chenal
   * 
   * @return true si les donn�es sont coh�rentes sinon retourne false et surtout indique a quel endroit se situe
   *         l'erreur de logique de la saisie
   */
  boolean verificationCoherence() {

    for (int i = 0; i < nbElem; i++) {
      final String nomgareAmont = (String) this.tableau.getModel().getValueAt(i, 1);
      final String nomgareAval = (String) this.tableau.getModel().getValueAt(i, 2);

      // test de verification d'�galit� des gares amonts et avals des differents cheneaux
      if (nomgareAmont.equals(nomgareAval)) {
        // message d erreur:
        new BuDialogError(d.application_.getApp(), Sinavi3Implementation.isSinavi_,
            "Erreur!! Les gares amont et aval de l'�cluse " + this.d.listeEcluse_.retournerEcluse(i).nom_
                + " sont identiques, ce qui est impossible!!").activate();
        // faux ce cas de figure ne peut aps etre possible
        return false;
      }

    }

    // arriv� � ce stade de la m�thode , tous les test de non coh�rence ont �chou�, il suit que
    // les donn�es saisies sont bien coh�rentes et l'on peut les ajouter aux cheneaux.
    return true;
  }

  /**
   * Methode qui permet de mettre a jour les gares saiies en amont et vaales pour chacun des cheneaux: v�rifie dans un
   * premier temps l coh�rence des donn�es:
   */

  void miseAjourTopologie() {

    /**
     * premiere etape on v�rifie la coh�rence des donn�es saisies:
     */
    if (this.verificationCoherence()) {
      System.out.println("Yes les donnees sont validees : jugee coherentes");

      /**
       * MODIFICAITON DES GARES AMONT ET AVALS POUR CHACUN DES CHENEAUX:
       */
      for (int i = 0; i < nbElem; i++) {
        // affichage de chacune des donn�es

        // recuperer une donn�e
        final String nomgareAmont = (String) this.tableau.getModel().getValueAt(i, 1);
        final String nomgareAval = (String) this.tableau.getModel().getValueAt(i, 2);
        System.out.println("Gare[" + i + "][1]= " + nomgareAmont);
        System.out.println("Gare[" + i + "][2]= " + nomgareAval);

        // ajout des gares amont et aval pour l'ecluse i

        /**
         * utilisation de la METHODE POUR RECUPERER LE NUMERO DE LA GARE A PARTIR DU NOM
         */
        this.d.listeEcluse_.retournerEcluse(i).gareAmont_ = d.listeGare_.retrouverGare(nomgareAmont);
        this.d.listeEcluse_.retournerEcluse(i).gareAval_ = d.listeGare_.retrouverGare(nomgareAval);

        
        //-- update network edge --//
        DefaultNetworkUserObject gareAmont = new DefaultNetworkUserObject(nomgareAmont,true);
        DefaultNetworkUserObject gareAval = new DefaultNetworkUserObject(nomgareAval,true);
        NetworkUserObject edge =   this.d.getListeEcluse_().retournerEcluse(i);
        this.d.getApplication().getNetworkEditor().connectElement(gareAmont, gareAval, edge);
        
      }
      new BuDialogMessage(d.application_.getApp(), Sinavi3Implementation.isSinavi_,
          "gares des ecluses sont correctement saisies").activate();
      //--On baisse le niveau de s�curit� pour forcer le test de coh�rence globale --//
      d.baisserNiveauSecurite2();

      dispose();
    }// fin du if ajout des donn�es

  }

}
