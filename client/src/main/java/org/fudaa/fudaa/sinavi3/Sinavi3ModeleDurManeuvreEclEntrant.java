package org.fudaa.fudaa.sinavi3;

import java.util.Observable;
import java.util.Observer;

import jxl.write.Label;
import jxl.write.Number;
import jxl.write.WritableCell;

import org.fudaa.ctulu.CtuluLibString;

import com.memoire.bu.BuDialogError;

@Deprecated
public class Sinavi3ModeleDurManeuvreEclEntrant extends Sinavi3ModeleExcel implements Observer{

	Sinavi3DataSimulation donnees_;
	String[] titreColonnes_;
	
	public Sinavi3ModeleDurManeuvreEclEntrant(Sinavi3DataSimulation _d){
	donnees_=_d;
	donnees_.addObservers(this);
	titreColonnes_ = new String[donnees_.listeBateaux_.listeNavires_.size() + 1];

    // initialisation des titres
    titreColonnes_[0] = "";
    for (int i = 0; i < donnees_.listeBateaux_.listeNavires_.size(); i++) {
      titreColonnes_[i + 1] = donnees_.listeBateaux_.retournerNavire(i).nom;
    }
	
	}
	
	
	public int getColumnCount() {
		// TODO Auto-generated method stub
		return this.donnees_.listeBateaux_.listeNavires_.size()+1;
	}

	public int getRowCount() {
		// TODO Auto-generated method stub
		return this.donnees_.listeEcluse_.listeEcluses_.size();
	}

	
	
	/** retourne le double correspondant au parcours **/
	public Object getValueAt(int i, int j) {
		
		if(j==0)
					return donnees_.listeEcluse_.retournerEcluse(i).nom_;
				else
						//return ""+this.donnees_.durManeuvreEclEntrant_.retournerValeur(i, j-1);
					return ""+this.donnees_.getDureeManeuvresEcluses().getDureeParcours(i,j-1, donnees_).dureeSortant1;
	}

	
	
	/**Recuperation des donnees (saisie par utilisateur) par le tableau et analyse pour validation **/
	public void setValueAt(Object value, int rowIndex, int columnIndex) {
		// TODO Auto-generated method stub
		
		//recuperation des vrais indices du tableau: il faut translater a cause des titres des noms
		
		
		double valeur;
		try {
			valeur = Double.parseDouble((String)value);
			
			
			//-- Gestion des erreurs --//
		if(valeur < 0) {
            new BuDialogError(donnees_.application_.getApp(), Sinavi3Implementation.isSinavi_,
                    "Erreur!!  la valeur entre:\n l'ecluse: " + this.donnees_.listeEcluse_.retournerEcluse(rowIndex).nom_
                        + "\n et \n le bateau: " + this.donnees_.listeBateaux_.retournerNavire(columnIndex-1).nom
                        + "\nest negatif!!!!").activate();
                return;
              }
		
		//-- test si l'utilisateur entre des centi�mes --//
		if(((String)value).lastIndexOf(".")!=-1){
		String min=((String)value).substring(((String)value).lastIndexOf(".")+1);
		int minutes=Integer.parseInt(min);
		if(minutes/100!=0)
		{
			new BuDialogError(donnees_.application_.getApp(), Sinavi3Implementation.isSinavi_,
		              "La valeur entre:\n l'ecluse: " + this.donnees_.listeEcluse_.retournerEcluse(rowIndex).nom_
		                  + "\n et le bateau: " + this.donnees_.listeBateaux_.retournerNavire(columnIndex-1).nom
		                  + " n'est pas coh�rente: \n2 chiffres apr�s la virgule maximum sont attendus.").activate();

		          return;
		}
		
		}
		
		} catch (NumberFormatException e) {
			new BuDialogError(donnees_.application_.getApp(), Sinavi3Implementation.isSinavi_,
		              "La valeur entre:\n l'�cluse: " + this.donnees_.listeEcluse_.retournerEcluse(rowIndex).nom_
		                  + "\n et \n le bateau: " + this.donnees_.listeBateaux_.retournerNavire(columnIndex-1).nom
		                  + "\n n'est pas coh�rente: ce n'est pas un reel!!!").activate();

		          return;
		}
		
		//-- test si la valeur saisie est coh�rente par rapport a la vitesse par defaut --//
		
		
		
		
		//mise a jour du ArrayList durres de parcours (cot� m�tier)
		//this.donnees_.durManeuvreEclEntrant_.modifierValeur(valeur, rowIndex, columnIndex-1);
		this.donnees_.getDureeManeuvresEcluses().getDureeParcours(rowIndex,columnIndex-1, donnees_).dureeEntrant1 = valeur;
		//mise a jour du tableau (cot� graphique)
		fireTableCellUpdated(rowIndex, columnIndex);
		
	}
	
	public boolean isCellEditable(int row, int col) {
        //Note that the data/cell address is constant,
        //no matter where the cell appears onscreen.
        if (col < 1 ) {
            return false;
        } else {
            return true;
        }
    }


	/**Methode qui permet de recuperer els noms des colonnes du tableau **/
	public String getColumnName(int column) {
		
		return titreColonnes_[column];
	}

	
	public int getMaxCol() {
	    // TODO Auto-generated method stub
	    return getColumnCount();
	  }

	  /**
	   * retourne le nombre de ligne
	   */
	  public int getMaxRow() {
	    // TODO Auto-generated method stub
	    return getRowCount();
	  }

	  /**
	   * retoune un tableau pour le format excel Celui-ci sera utilis� avec la fonction write
	   * 
	   * @see org.fudaa.ctulu.table.CtuluTableModelInterface#getExcelWritable(int, int)
	   */
	  public WritableCell getExcelWritable(final int _row, final int _col, int _rowXls, int _colXls) {
	    final int r = _row;
	    final int c = _col;
	    /*
	     * if (column_ != null) c = column_[c]; if (row_ != null) { r = row_[r]; }
	     */
	    final Object o = getValueAt(r,c);
	    if (o == null) {
	      return null;
	    }
	    String s = o.toString();
	    if(CtuluLibString.isEmpty(s)) return null;
	    try {
	      return new Number(_colXls, _rowXls, Double.parseDouble(s));
	    } catch (final NumberFormatException e) {}
	    return new Label(_colXls, _rowXls, s);

	    // return cell;
	  }


	  public void update(Observable o, Object arg) {
			// TODO Auto-generated method stub
			if(arg.equals("ecluse") ||arg.equals("bateau"))
				this.fireTableDataChanged();
			
		}
	

}
