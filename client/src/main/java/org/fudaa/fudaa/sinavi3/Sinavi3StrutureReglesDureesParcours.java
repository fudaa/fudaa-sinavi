package org.fudaa.fudaa.sinavi3;

import java.util.ArrayList;

public class Sinavi3StrutureReglesDureesParcours {

	
	
	
  /**
   * definition du vecteur de vecteur ce vecteur est assimil� � une matrice dont els lignes sont les diff�retns
   * cheneaux(respectivement cercles) saisis et les colonnes sont assimil�es � des navires
   */
  ArrayList matriceDuree = new ArrayList();

  Sinavi3StrutureReglesDureesParcours() {

  }

  
  public  double min(double x, double y){
	  if (x<y) return x;
	  return y;
  }
  
  /**
   * methjode d'ajout d'un ligne
   */
  //-- procedure de creation de la structure --//
  public  void ajoutLigne(final int _numNavires) {

	    final ArrayList vecteurChenal = new ArrayList();

	    for (int k = 0; k < _numNavires; k++) {
	      vecteurChenal.add(new Double(0.0));
	    }

	    this.matriceDuree.add(vecteurChenal);

	  }
  //-- procedure de creation d'un tron�on --//
  public void ajoutLigneMontant(final int _numNavires,final Sinavi3DataSimulation donnees_) {

    final ArrayList vecteurChenal = new ArrayList();

    for (int k = 0; k < _numNavires; k++) {
      vecteurChenal.add(new Double(min(donnees_.listeBief_.retournerBief(donnees_.listeBief_.listeBiefs_.size()-1).vitesse_,donnees_.listeBateaux_.retournerNavire(k).vitesseDefautMontant)));
    }

    this.matriceDuree.add(vecteurChenal);

  }
  
  /**
   * methjode d'ajout d'un ligne
   */
  public void ajoutLigneAvalant(final int _numNavires,final Sinavi3DataSimulation donnees_) {

    final ArrayList vecteurChenal = new ArrayList();

    for (int k = 0; k < _numNavires; k++) {
      vecteurChenal.add(new Double(min(donnees_.listeBief_.retournerBief(donnees_.listeBief_.listeBiefs_.size()-1).vitesse_,donnees_.listeBateaux_.retournerNavire(k).vitesseDefautAvalant)));
    }

    this.matriceDuree.add(vecteurChenal);

  }
  
  public void ajoutLigneManeuvreEntrant(final int _numNavires,final Sinavi3DataSimulation donnees_) {

	    final ArrayList vecteurChenal = new ArrayList();

	    for (int k = 0; k < _numNavires; k++) {
	      vecteurChenal.add(new Double(donnees_.listeEcluse_.retournerEcluse(donnees_.listeEcluse_.listeEcluses_.size()-1).dureeManoeuvreEntrant_));
	    }

	    this.matriceDuree.add(vecteurChenal);
}
  public void ajoutLigneManeuvreSortant(final int _numNavires,final Sinavi3DataSimulation donnees_) {

	    final ArrayList vecteurChenal = new ArrayList();

	    for (int k = 0; k < _numNavires; k++) {
	      vecteurChenal.add(new Double(donnees_.listeEcluse_.retournerEcluse(donnees_.listeEcluse_.listeEcluses_.size()-1).dureeManoeuvreSortant_));
	    }

	    this.matriceDuree.add(vecteurChenal);
}
  
  /**
   * Methode de suppression d une ligne
   * 
   * @param _numChenal
   */
  public void SuprimerLigne(final int _numChenal) {
    this.matriceDuree.remove(_numChenal);
  }

  /**
   * Methode d'ajout d'une colonne ie ajout d un navire
   */
//-- procedure de creation d un navire
  public void ajoutNavireMontant(Sinavi3DataSimulation donnees_) {
    for (int k = 0; k < this.matriceDuree.size(); k++) {
      final ArrayList vecteurLigne = (ArrayList) this.matriceDuree.get(k);
      vecteurLigne.add(new Double(min(donnees_.listeBief_.retournerBief(k).vitesse_,donnees_.listeBateaux_.retournerNavire(donnees_.listeBateaux_.listeNavires_.size()-1).vitesseDefautMontant)));
    }

  }
  public  void ajoutNavireAvalantt(Sinavi3DataSimulation donnees_) {
	    for (int k = 0; k < this.matriceDuree.size(); k++) {
	      final ArrayList vecteurLigne = (ArrayList) this.matriceDuree.get(k);
	      vecteurLigne.add(new Double(min(donnees_.listeBief_.retournerBief(k).vitesse_,donnees_.listeBateaux_.retournerNavire(donnees_.listeBateaux_.listeNavires_.size()-1).vitesseDefautAvalant)));
	    }

	  }
  
  public  void ajoutNavireManeuvreEntrant(Sinavi3DataSimulation donnees_) {
	    for (int k = 0; k < this.matriceDuree.size(); k++) {
	      final ArrayList vecteurLigne = (ArrayList) this.matriceDuree.get(k);
	      vecteurLigne.add(new Double(donnees_.listeEcluse_.retournerEcluse(k).dureeManoeuvreEntrant_));
	    }

	  }
  public  void ajoutNavireManeuvreSortant(Sinavi3DataSimulation donnees_) {
	    for (int k = 0; k < this.matriceDuree.size(); k++) {
	      final ArrayList vecteurLigne = (ArrayList) this.matriceDuree.get(k);
	      vecteurLigne.add(new Double(donnees_.listeEcluse_.retournerEcluse(k).dureeManoeuvreSortant_));
	    }

	  }
  
  /**
   * Methode de suppression d un navire
   * 
   * @param _numNavire
   */
  public  void suppressionNavire(final int _numNavire) {
    for (int k = 0; k < this.matriceDuree.size(); k++) {
      final ArrayList vecteurLigne = (ArrayList) this.matriceDuree.get(k);
      vecteurLigne.remove(_numNavire);
    }

  }

  /**
   * Methode de recuperation de al valeur (i j) de la matrice
   * 
   * @param ligne indice de la ligne
   * @param col indice de la colonne
   * @return la valeur associ�e a la case i j de la matrice
   */
  public double retournerValeur(final int ligne, final int col) {
    double val = 0;
    final ArrayList vecteurLigne = (ArrayList) this.matriceDuree.get(ligne);
    val = ((Double) vecteurLigne.get(col)).doubleValue();

    return val;
  }

  /**
   * Methode d insertion d un objet de type Double dans la matrice
   * 
   * @param val valeure a inserer
   * @param ligne indice de ligne
   * @param col indice de colonne
   */
  public  void modifierValeur(final double val, final int ligne, final int col) {
    final Double nouveau = new Double(val);

    final ArrayList vecteurLigne = (ArrayList) this.matriceDuree.get(ligne);
    vecteurLigne.set(col, nouveau);

  }
  
  public  void modifierValeurAvalant(final int bief,final int _numNavires,final Sinavi3DataSimulation donnees_) {

	    final ArrayList vecteurChenal = new ArrayList();

	    for (int k = 0; k < _numNavires; k++) {
	      modifierValeur(min(donnees_.listeBief_.retournerBief(bief).vitesse_,donnees_.listeBateaux_.retournerNavire(k).vitesseDefautAvalant),bief,k);
	    }
 }

  public  void modifierValeurMontant(final int bief,final int _numNavires,final Sinavi3DataSimulation donnees_) {

	    final ArrayList vecteurChenal = new ArrayList();

	    for (int k = 0; k < _numNavires; k++) {
	      modifierValeur(min(donnees_.listeBief_.retournerBief(bief).vitesse_,donnees_.listeBateaux_.retournerNavire(k).vitesseDefautMontant),bief,k);
	    }
}

  public  void modifierValeurAvalantNavire(final int navire,final int _numBief,final Sinavi3DataSimulation donnees_) {

	    final ArrayList vecteurChenal = new ArrayList();

	    for (int k = 0; k <  _numBief; k++) {
	      modifierValeur(min(donnees_.listeBief_.retournerBief(k).vitesse_,donnees_.listeBateaux_.retournerNavire(navire).vitesseDefautAvalant),k,navire);
	    }
} 
  public  void modifierValeurMontantNavire(final int navire,final int _numBief,final Sinavi3DataSimulation donnees_) {

	    final ArrayList vecteurChenal = new ArrayList();

	    for (int k = 0; k <  _numBief; k++) {
	      modifierValeur(min(donnees_.listeBief_.retournerBief(k).vitesse_,donnees_.listeBateaux_.retournerNavire(navire).vitesseDefautMontant),k,navire);
	    }
}   
  
  
  public  void modifierValeurAvalantNavire(int navire , Sinavi3DataSimulation donnees_) {

	    

	    for (int k = 0; k <  donnees_.listeBief_.listeBiefs_.size(); k++) {
	      modifierValeur(min(donnees_.listeBief_.retournerBief(k).vitesse_,donnees_.listeBateaux_.retournerNavire(navire).vitesseDefautAvalant),k,navire);
	    }
} 
  public  void modifierValeurMontantNavire(int navire,Sinavi3DataSimulation donnees_) {

	   

	    for (int k = 0; k < donnees_.listeBief_.listeBiefs_.size(); k++) {
	      modifierValeur(min(donnees_.listeBief_.retournerBief(k).vitesse_,donnees_.listeBateaux_.retournerNavire(navire).vitesseDefautMontant),k,navire);
	    }
}    
  
  
  public  void modifierManeuvreEntrant(final int ecluse,final int _numNavires,final Sinavi3DataSimulation donnees_) {

	    final ArrayList vecteurChenal = new ArrayList();

	    for (int k = 0; k < _numNavires; k++) {
	    	 modifierValeur(donnees_.listeEcluse_.retournerEcluse(ecluse).dureeManoeuvreEntrant_,ecluse,k);
	    }
	    
}  
  public  void modifierManeuvreSortant(final int ecluse,final int _numNavires,final Sinavi3DataSimulation donnees_) {

	    final ArrayList vecteurChenal = new ArrayList();

	    for (int k = 0; k < _numNavires; k++) {
	    	 modifierValeur(donnees_.listeEcluse_.retournerEcluse(ecluse).dureeManoeuvreSortant_,ecluse,k);
	    }
	    
}  
  
  public  void modifierManeuvreEntrant(final int ecluse,final Sinavi3DataSimulation donnees_) {

	    final ArrayList vecteurChenal = new ArrayList();

	    for (int k = 0; k < donnees_.listeBateaux_.listeNavires_.size(); k++) {
	    	 modifierValeur(donnees_.listeEcluse_.retournerEcluse(ecluse).dureeManoeuvreEntrant_,ecluse,k);
	    }
	    
}  
  public void modifierManeuvreSortant(final int ecluse,final Sinavi3DataSimulation donnees_) {

	    final ArrayList vecteurChenal = new ArrayList();

	    for (int k = 0; k < donnees_.listeBateaux_.listeNavires_.size(); k++) {
	    	 modifierValeur(donnees_.listeEcluse_.retournerEcluse(ecluse).dureeManoeuvreSortant_,ecluse,k);
	    }
	    
}    
  
  

}
