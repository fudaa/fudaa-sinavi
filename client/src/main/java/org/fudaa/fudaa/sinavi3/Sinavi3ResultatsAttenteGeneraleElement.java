/**
 *@creation 14 nov. 06
 *@modification $Dates$
 *@license  GNU General Public Licence 2
 *@copyright   (c)1998-2006 CETMEF 2 bd Gambetta F -60231 Compiegne
 *@mail   devel@fudaa.fr 
 */
package org.fudaa.fudaa.sinavi3;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.AncestorEvent;
import javax.swing.table.TableColumn;

import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;

import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.image.CtuluImageExport;
import org.fudaa.ctulu.table.CtuluTableExcelWriter;
import org.fudaa.ebli.graphe.BGraphe;
import org.fudaa.fudaa.ressource.FudaaResource;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuDialogError;
import com.memoire.bu.BuInternalFrame;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuRadioButton;
import com.memoire.bu.BuTabbedPane;
import com.memoire.bu.BuTable;
import com.memoire.fu.FuLog;

/**
 * classe de gestion des resultats de la generation des bateaux propose 2 onglets: le premier propose un affichage
 * 
 * @version $Version$
 * @author Adrien Hadoux
 */
public class Sinavi3ResultatsAttenteGeneraleElement extends Sinavi3InternalFrame /*implements ActionListener */{

  /**
   * ensemble des donn�es du tableau sous la forme de data
   */
  Object[][] data;

  /**
   * Graphe associ�e aux r�sultats de la g�n�ration de bateaux
   */
  BGraphe graphe_ = new BGraphe();

  /**
   * histogramme associ�e aux r�sultats de la g�n�ration de bateaux
   */
  BGraphe histo_ = new BGraphe();

  /**
   * panel principal de la fenetre
   */
  BuPanel panelPrincipal_ = new BuPanel();

  /**
   * Panel de selection des preferences
   */
  BuPanel selectionPanel_ = new BuPanel();
  BuPanel selectionPanel1;
  /** Comboliste de selection de l element de depart */
  String[] listeElt = { "tron�on", "ecluse"};
  JComboBox ListeTypesDepart_ = new JComboBox(listeElt);
  JComboBox ListeElementDepart_ = new JComboBox();
  /** Comboliste de selection de l element d'arrivee */
  JComboBox ListeTypesArrivee_ = new JComboBox(listeElt);
  JComboBox ListeElementArrivee_ = new JComboBox();
  /** horaire de depart */
  // JTextField horaireDeb_=new JTextField(12);
  // JTextField horaireFin_=new JTextField(12);
  String[] chaine_sens = {"avalant", "montant", "tous"};
  JComboBox Sens_ = new JComboBox(chaine_sens);

  /**
   * Panel des options: type affichages, colonnes � faire figurer:
   */
  BuPanel optionPanel_ = new BuPanel();

  JCheckBox choixNbNavires_ = new JCheckBox("nombre bateaux", true);
  JCheckBox choixTotalAttente_ = new JCheckBox("Attentes totales", true);

  JCheckBox choixMarees_ = new JCheckBox("Attentes mar�es", true);
  JCheckBox choixSecurite_ = new JCheckBox("Attentes de s�curit�", true);
  JCheckBox choixAcces_ = new JCheckBox("Attentes d'acc�s", true);
  JCheckBox choixOccupation_ = new JCheckBox("Attentes d'occupation", true);
  JCheckBox choixPannes_ = new JCheckBox("Attentes d'indisponibilit�", true);
  
  BuPanel optionPanelgraph_ = new BuPanel();
  JRadioButton choixTotalAttente2_ = new JRadioButton("Attentes totales", true);
  JRadioButton choixSecurite2_ = new JRadioButton("Attentes de s�curit�", false);
  JRadioButton choixAcces2_ = new JRadioButton("Attentes d'acc�s", false);
  JRadioButton choixOccupation2_ = new JRadioButton("Attentes d'occupation", false);
  JRadioButton choixPannes2_ = new JRadioButton("Attentes d'indisponibilit�", false);
  
  JCheckBox[] tableauChoixNavires_;
  JCheckBox[] tableauChoixNavires2_;

  JCheckBox choixMax_ = new JCheckBox("Attente max", true);
  JCheckBox choixMoy_ = new JCheckBox("Attente moyenne", true);
  JCheckBox choixMin_ = new JCheckBox("Attente min", true);

  boolean seuil_ = false;
  JTextField valSeuil_ = new JTextField(6);
  JCheckBox valideSeuil_ = new JCheckBox("Afficher", false);
  float valeurSeuil = 0;

  /**
   * Tableau r�capitulatif des r�sultats de la simulation
   */
  BuTable tableau_;

  String titreTableau_[] = { "Cat�gorie", "Nombre de bateaux", "Att. s�curit�: total", "Att. s�curit�: moy./flotte", "Att. s�curit�: nb. bateaux", "Att. s�curit�: moyenne", "Att. acc�s: total", "Att. acc�s: moy./flotte",
	      "Att. acc�s: nb. bateaux", "Att. acc�s: moyenne", "Att. occup.: total", "Att. occup.: moy./flotte", "Att. occup.: nb. bateaux", "Att. occup.: moyenne",
	      "Att. indisp.: total", "Att. indisp.: moy./flotte", "Att. indisp.: nb. bateaux", "Att. indisp.: moyenne", "Attente totale", "Attente moyenne sur flotte",
	      "Nb. bateaux ayant attendu", "Attente moyenne" };
  // 26

  /**
   * Panel tabbed qui g�re les 2 onglets, ie les 2 versions d'affichage des r�sultats:
   */
  BuTabbedPane panelPrincipalAffichage_ = new BuTabbedPane();
  
  /**
   * Panel tabbed qui g�re les 2 onglets de repr�sentations graphiques
   */
  BuTabbedPane panelGraphAffichage_ = new BuTabbedPane();

  /**
   * Panel cniotenant le tableau et les boutns de controles
   */
  BuPanel panelGestionTableau_ = new BuPanel();

  /**
   * panel de gestion du tableau et des diff�rents boutons
   */
  BuPanel panelTableau_ = new BuPanel();

  /**
   * panel de gestion des boutons
   */
  BuPanel controlPanel_ = new BuPanel();

  /**
   * Panel de gestion des boutons des courbes
   */
  BuPanel controlPanelCourbes_ = new BuPanel();

  /**
   * Panel de gestion des boutons des histogrammes
   */
  BuPanel controlPanelHisto_ = new BuPanel();

  /**
   * panel de gestion des repr�sentations graphiques
   */
  BuPanel panelGraph_ = new BuPanel();
  
  /**
   * panel de gestion des courbes
   */
  BuPanel panelCourbe_ = new BuPanel();

  /**
   * panel de gestion des histogrammes
   */
  BuPanel panelHisto_ = new BuPanel();

  /**
   * combolist qui permet de selectionenr les lignes deu tableau a etre affich�es:
   */
  JComboBox ListeNavires_ = new JComboBox();

  /**
   * buoton de generation des resultats
   */
  private final BuButton exportationExcel_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_generer"), "Exporter");

  final BuButton exportationgraphe_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_generer"), "Exporter");

  final BuButton exportationHisto_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_generer"), "Exporter");
  private final BuButton quitter_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_quitter"), "Quitter");
  private final BuButton quitter2_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_quitter"), "Quitter");
  private final BuButton quitter3_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_quitter"), "Quitter");
  private final BuButton lancerRecherche_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_oui"), "Rechercher");
  private final BuButton lancerOptions_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_oui"), "Modifier");
  private final BuButton validerSeuil_ = new BuButton(/* FudaaResource.FUDAA.getIcon("crystal22_oui"), */"Valider seuil");

  Border raisedBevel_ = BorderFactory.createRaisedBevelBorder();
  Border loweredBevel_ = BorderFactory.createLoweredBevelBorder();
  Border compound_ = BorderFactory.createCompoundBorder(raisedBevel_, loweredBevel_);
  Border bordnormal_ = BorderFactory.createEtchedBorder();
  /**
   * donnees de la simulation
   */
  Sinavi3DataSimulation donnees_;

  /**
   * constructeur de la sous fenetre de gestion des resultats:
   */
  Sinavi3ResultatsAttenteGeneraleElement(final Sinavi3DataSimulation _donnees) {
    super("Attentes par �l�ment", true, true, true, true);

    // recuperation des donn�es de la simulation
    donnees_ = _donnees;

    // premier calcul par defaut execut�: entre le chenal 0 et le quai 0 dans l horaire entre 0 et 24
    // SiporCalculDureesParcours.calcul(donnees_,donnees_.listeChenal_.retournerChenal(0).nom_,donnees_.LQuais_.retournerQuais(0).Nom_,0,24);
    // SiporCalculAttentesGenerales.CalculApresSImu(donnees_);

    setSize(820, 600);
    setBorder(Sinavi3Bordures.compound_);
    this.getContentPane().setLayout(new /* BorderLayout() */GridLayout(1, 1));
    this.panelPrincipal_.setLayout(new BorderLayout());
    this.getContentPane().add(this.panelPrincipal_/* ,BorderLayout.CENTER */);
    this.panelPrincipal_.add(this.selectionPanel_, BorderLayout.NORTH);
    //this.panelPrincipal_.add(this.optionPanel_, BorderLayout.WEST);

    this.panelPrincipal_.add(this.panelPrincipalAffichage_, BorderLayout.CENTER);

    // ajout du tableau dans le panel tabbed
    panelPrincipalAffichage_.addTab("R�sultats d�taill�s", FudaaResource.FUDAA.getIcon("crystal_arbre"), panelGestionTableau_);

    // ajout des courbes dans le panel de la sous fenetre
    //panelPrincipalAffichage_.addTab("Graphe", FudaaResource.FUDAA.getIcon("crystal_graphe"), panelCourbe_);
    panelPrincipalAffichage_.addTab("Exploitation graphique", FudaaResource.FUDAA.getIcon("crystal_graphe"), panelGraph_);
    //panelPrincipalAffichage_.addTab("Histogramme", FudaaResource.FUDAA.getIcon("crystal_graphe"), panelHisto_);

    tableauChoixNavires_ = new JCheckBox[this.donnees_.listeBateaux_.listeNavires_.size()];
    for (int i = 0; i < this.donnees_.listeBateaux_.listeNavires_.size(); i++) {
      this.tableauChoixNavires_[i] = new JCheckBox(this.donnees_.listeBateaux_.retournerNavire(i).nom, true);
      this.tableauChoixNavires_[i].addActionListener(this);

    }
    tableauChoixNavires2_ = new JCheckBox[this.donnees_.listeBateaux_.listeNavires_.size()];
    for (int i = 0; i < this.donnees_.listeBateaux_.listeNavires_.size(); i++) {
      this.tableauChoixNavires2_[i] = new JCheckBox(this.donnees_.listeBateaux_.retournerNavire(i).nom, true);
      this.tableauChoixNavires2_[i].addActionListener(this);

    }
    /*******************************************************************************************************************
     * gestion du panel de selection
     ******************************************************************************************************************/
    this.selectionPanel_.setLayout(new GridLayout(1, 1));

    selectionPanel1 = new BuPanel(new FlowLayout(FlowLayout.LEFT));
    selectionPanel1.add(new JLabel("Attentes � cumuler pour l'�l�ment"));
    // selectionPanel1.add(new JLabel("Element de depart:"));
    selectionPanel1.add(this.ListeTypesDepart_);
    selectionPanel1.add(this.ListeElementDepart_);
    // selectionPanel1.add(new JLabel("Element d'arrivee:"));
    // selectionPanel1.add(this.ListeTypesArrivee_);
    // selectionPanel1.add(this.ListeElementArrivee_);
    //selectionPanel1.setBorder(this.bordnormal_);
    this.selectionPanel_.add(selectionPanel1);

    //final BuPanel selectionPanel2 = new BuPanel();
    selectionPanel1.add(new JLabel(" dans le sens"));
    selectionPanel1.add(this.Sens_);
    // selectionPanel2.add(new JLabel("H.Min "));
    // selectionPanel2.add(new JLabel("Horaire de fin: "));
    // selectionPanel2.add(this.horaireFin_);
    // selectionPanel2.add(new JLabel("H.Min "));
    selectionPanel1.add(lancerRecherche_);

    //selectionPanel2.setBorder(this.bordnormal_);
    //this.selectionPanel_.add(selectionPanel2);

    final TitledBorder bordurea = BorderFactory.createTitledBorder(BorderFactory
            .createEtchedBorder(EtchedBorder.LOWERED), "D�finition de la recherche");
        this.selectionPanel_.setBorder(bordurea);
    
    // listener des liste box
    final ActionListener RemplissageElement = new ActionListener() {
      public void actionPerformed(ActionEvent e) {

        int selection = 0;
        if (e.getSource() == ListeTypesDepart_) {
          selection = ListeTypesDepart_.getSelectedIndex();
        } else {
          selection = ListeTypesArrivee_.getSelectedIndex();
        }
        // "gare","quai","ecluse","chenal","cercle","bassin"
        switch (selection) {
        /*
         * case 0: if(e.getSource()==ListeTypesDepart_) { ListeElementDepart_.removeAllItems(); for(int i=0;i<donnees_.listeGare_.listeGares_.size();i++)
         * ListeElementDepart_.addItem(donnees_.listeGare_.retournerGare(i)); ListeElementDepart_.validate(); } else {
         * ListeElementArrivee_.removeAllItems(); for(int i=0;i<donnees_.listeGare_.listeGares_.size();i++)
         * ListeElementArrivee_.addItem(donnees_.listeGare_.retournerGare(i)); ListeElementArrivee_.validate(); } break;
         */
      
        case 1:
          if (e.getSource() == ListeTypesDepart_) {
            ListeElementDepart_.removeAllItems();
            for (int i = 0; i < donnees_.listeEcluse_.listeEcluses_.size(); i++) {
              ListeElementDepart_.addItem(donnees_.listeEcluse_.retournerEcluse(i).nom_);
            }
            ListeElementDepart_.validate();
          } else {
            ListeElementArrivee_.removeAllItems();
            for (int i = 0; i < donnees_.listeEcluse_.listeEcluses_.size(); i++) {
              ListeElementArrivee_.addItem(donnees_.listeEcluse_.retournerEcluse(i).nom_);
            }
            ListeElementArrivee_.validate();
          }
          break;
        case 0:
          if (e.getSource() == ListeTypesDepart_) {
            ListeElementDepart_.removeAllItems();
            for (int i = 0; i < donnees_.listeBief_.listeBiefs_.size(); i++) {
              ListeElementDepart_.addItem(donnees_.listeBief_.retournerBief(i).nom_);
            }
            ListeElementDepart_.validate();
          } else {
            ListeElementArrivee_.removeAllItems();
            for (int i = 0; i < donnees_.listeBief_.listeBiefs_.size(); i++) {
              ListeElementArrivee_.addItem(donnees_.listeBief_.retournerBief(i).nom_);
            }
            ListeElementArrivee_.validate();
          }
          break;
      
        }

        /*
         * ListeElementDepart_.updateUI(); ListeElementDepart_.revalidate(); ListeElementArrivee_.revalidate();
         * selectionPanel1.validate(); updateUI();
         */
      }
    };
    this.ListeTypesDepart_.addActionListener(RemplissageElement);
    this.ListeTypesArrivee_.addActionListener(RemplissageElement);
    this.ListeTypesDepart_.setSelectedIndex(0);
    this.ListeTypesArrivee_.setSelectedIndex(0);
    this.Sens_.setSelectedIndex(0);
    // this.horaireDeb_.setText(""+0.0);
    // this.horaireFin_.setText(""+24.0);
    /*
     * this.horaireDeb_.addFocusListener(new FocusAdapter() { public void focusGained(FocusEvent e) {
     * horaireDeb_.selectAll(); } public void focusLost(FocusEvent e) {System.out.print("test validite de Longueur
     * Min"); //test de validit si on a rentr quelque chose if(!horaireDeb_.getText().equals("")) try{ float
     * i=Float.parseFloat(horaireDeb_.getText()); if(i<0) { new
     * BuDialogError(donnees_.application_.getApp(),donnees_.application_.isSipor_, "Erreur!! Cet horaire est
     * negatif!!!!") .activate(); horaireDeb_.setText("0.0"); } else if(i>=24) { new
     * BuDialogError(donnees_.application_.getApp(),donnees_.application_.isSipor_, "Erreur!! Cet horaire doit etre
     * compris entre 0 et 24!!!!") .activate(); horaireDeb_.setText("0.0"); } }catch(NumberFormatException nfe) { new
     * BuDialogError(donnees_.application_.getApp(),donnees_.application_.isSipor_, "Erreur!! Ce nombre n'existe
     * pas!!!!") .activate(); horaireDeb_.setText("0.0"); }} }); this.horaireFin_.addFocusListener(new FocusAdapter() {
     * public void focusGained(FocusEvent e) { horaireFin_.selectAll(); } public void focusLost(FocusEvent e)
     * {System.out.print("test validite de Longueur Min"); //test de validit si on a rentr quelque chose
     * if(!horaireFin_.getText().equals("0.0")) try{ float i=Float.parseFloat(horaireFin_.getText()); if(i<0) { new
     * BuDialogError(donnees_.application_.getApp(),donnees_.application_.isSipor_, "Erreur!! Cet horaire est
     * negatif!!!!") .activate(); horaireFin_.setText("0.0"); horaireDeb_.setText("0.0"); } else if(i>24) { new
     * BuDialogError(donnees_.application_.getApp(),donnees_.application_.isSipor_, "Erreur!! Cet horaire doit etre
     * compris entre 0 et 24!!!!") .activate(); horaireFin_.setText("0.0"); horaireDeb_.setText("0.0"); } else {
     * //horaire fin inferieur a horaire de depart: if(i< Float.parseFloat(horaireDeb_.getText())) { new
     * BuDialogError(donnees_.application_.getApp(),donnees_.application_.isSipor_, "Erreur!! Cet horaire de fin est
     * infererieur a l'horaire de depart!!!!") .activate(); horaireFin_.setText("0.0"); horaireDeb_.setText("0.0"); } }
     * }catch(NumberFormatException nfe) { new
     * BuDialogError(donnees_.application_.getApp(),donnees_.application_.isSipor_, "Erreur!! Ce nombre n'existe
     * pas!!!!") .activate();horaireFin_.setText("0.0"); horaireDeb_.setText("0.0"); }} });
     */

    this.lancerRecherche_.addActionListener(this);
    /*******************************************************************************************************************
     * gestion du panel des options
     ******************************************************************************************************************/

    // Box panoption=Box.createVerticalBox();
    final Box panoption = Box.createVerticalBox();
    // panoption.setLayout(new GridLayout(2,1));
    //final JScrollPane pcnasc=new JScrollPane(panoption);
    this.optionPanel_.add(panoption);

    final Box bVert = Box.createVerticalBox();
    panoption.add(bVert);
    //final JScrollPane pcnasc1 = new JScrollPane(bVert);
    //panoption.addTab("Affichage", pcnasc1);
    // bVert.add(new JLabel("Options Affichage:"));
    bVert.add(new JLabel(""));
    //bVert.add(this.choixNbNavires_);
    final TitledBorder bordure1 = BorderFactory.createTitledBorder(BorderFactory
        .createEtchedBorder(EtchedBorder.LOWERED), "Types d'attentes");
    bVert.setBorder(bordure1);
    // bVert.setBorder(this.bordnormal_);
    //bVert.add(this.choixMarees_);
    bVert.add(this.choixSecurite_);
    bVert.add(this.choixAcces_);
    bVert.add(this.choixOccupation_);
    bVert.add(this.choixPannes_);
    bVert.add(this.choixTotalAttente_);
    // bVert.add(lancerOptions_);

    final Box bVert2 = Box.createVerticalBox();
    panoption.add(bVert2);
    // bVert2.add(new JLabel("Affichage navires:"));
    bVert2.setBorder(this.bordnormal_);
    for (int i = 0; i < this.tableauChoixNavires_.length; i++) {
      bVert2.add(this.tableauChoixNavires_[i]);
    }
    final TitledBorder bordure2 = BorderFactory.createTitledBorder(BorderFactory
        .createEtchedBorder(EtchedBorder.LOWERED), "Cat�gories");
    bVert2.setBorder(bordure2);
    //final JScrollPane pcnasc = new JScrollPane(bVert2);
    //panoption.addTab("Bateaux", pcnasc);

    final Box bVert3 = Box.createVerticalBox();
    final TitledBorder bordure3 = BorderFactory.createTitledBorder(BorderFactory
        .createEtchedBorder(EtchedBorder.LOWERED), "attente");
    bVert3.setBorder(bordure3);
    bVert3.add(this.choixMin_);
    bVert3.add(this.choixMoy_);
    bVert3.add(this.choixMax_);
    this.choixMin_.addActionListener(this);
    this.choixMoy_.addActionListener(this);
    this.choixMax_.addActionListener(this);
    // panoption.add(bVert3);

    this.optionPanel_.setBorder(this.compound_);

    // listener des checkbox de choix des options d affichage

    this.choixAcces_.addActionListener(this);
    //this.choixMarees_.addActionListener(this);
    this.choixSecurite_.addActionListener(this);
    //this.choixNbNavires_.addActionListener(this);
    this.choixTotalAttente_.addActionListener(this);
    this.choixOccupation_.addActionListener(this);
    this.choixPannes_.addActionListener(this);
    
    // lancerOptions_.addActionListener(this);
    /*******************************************************************************************************************
     * gestion du panel des options graphiques
     ******************************************************************************************************************/

    final Box panoptiongraph = Box.createVerticalBox();
    this.optionPanelgraph_.add(panoptiongraph);

    final Box bVertgraph = Box.createVerticalBox();
    panoptiongraph.add(bVertgraph);
    bVertgraph.add(new JLabel(""));
    
    ButtonGroup bg=new ButtonGroup();
    bg.add(choixSecurite2_);
    bg.add(choixAcces2_);
    bg.add(choixOccupation2_);
    bg.add(choixPannes2_);
    bg.add(this.choixTotalAttente2_);
    bg.add(choixAcces2_);
    
    final TitledBorder borduregraph1 = BorderFactory.createTitledBorder(BorderFactory
        .createEtchedBorder(EtchedBorder.LOWERED), "Types d'attentes");
    bVertgraph.setBorder(borduregraph1);
    bVertgraph.add(this.choixSecurite2_);
    bVertgraph.add(this.choixAcces2_);
    bVertgraph.add(this.choixOccupation2_);
    bVertgraph.add(this.choixPannes2_);
    bVertgraph.add(this.choixTotalAttente2_);

    final Box bVertgraph2 = Box.createVerticalBox();
    panoptiongraph.add(bVertgraph2);
    bVertgraph2.setBorder(this.bordnormal_);
    for (int i = 0; i < this.tableauChoixNavires2_.length; i++) {
      bVertgraph2.add(this.tableauChoixNavires2_[i]);
    }
    final TitledBorder borduregraph2 = BorderFactory.createTitledBorder(BorderFactory
        .createEtchedBorder(EtchedBorder.LOWERED), "Cat�gories");
    bVertgraph2.setBorder(borduregraph2);
    
    this.optionPanelgraph_.setBorder(this.compound_);

    // listener des checkbox de choix des options d affichage

    this.choixAcces2_.addActionListener(this);
    this.choixSecurite2_.addActionListener(this);
    this.choixTotalAttente2_.addActionListener(this);
    this.choixOccupation2_.addActionListener(this);
    this.choixPannes2_.addActionListener(this);
    
    final Box bVert4 = Box.createVerticalBox();
    final TitledBorder bordure4 = BorderFactory.createTitledBorder(BorderFactory
            .createEtchedBorder(EtchedBorder.LOWERED), "Seuil");
    bVert4.setBorder(bordure4);
    bVert4.add(new JLabel("Valeur (h.min):"));
    bVert4.add(valSeuil_);
    bVert4.add(valideSeuil_);
    panoptiongraph.add(bVert4);
    
    /*******************************************************************************************************************
     * gestion du panel tableau panelGestionTableau_
     ******************************************************************************************************************/

    // etape 1: architecture du panel panelGestionTableau_
    panelGestionTableau_.setLayout(new BorderLayout());

    // definition d un panel ascenceur pour stocer le tableau:
    final JScrollPane asc = new JScrollPane(this.panelTableau_);

    // ajout au centre du panel qui contiendra le tableau d affichage
    this.panelGestionTableau_.add(asc, BorderLayout.CENTER);
    this.panelGestionTableau_.add(this.optionPanel_, BorderLayout.WEST);

    // panel qui contient les differents boutons
    this.controlPanel_.add(quitter_);
    // this.controlPanel_.add(new JLabel("Selectionnez la cat�gorie � visualiser: "));
    // this.controlPanel_.add(this.ListeNavires_);
    this.controlPanel_.add(exportationExcel_);
    this.panelGestionTableau_.add(this.controlPanel_, BorderLayout.SOUTH);

    // etape 2: remplissage du comboliste avec les noms des navires
    this.ListeNavires_.addItem("Tous");
    for (int i = 0; i < donnees_.listeBateaux_.listeNavires_.size(); i++) {
      this.ListeNavires_.addItem("" + donnees_.listeBateaux_.retournerNavire(i).nom);
    }

    // etape 3: gestion de l affichage du tableau de donn�es
    // remarque : cette m�thode sera syst�matiquement appel�e afni d'op�rer un changement:
    affichageTableau(-1);

    // etape 4: listener du combolist afin de pouvoir selectionner le navire qui nous interesse
    // a noter que la selection va faire surligner le navire souhait�
    this.ListeNavires_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        // evenement du clic sur le bouton
        final int val = ListeNavires_.getSelectedIndex();
        affichageTableau(val - 1);

      }
    });

    // bouton qui permet de generer le contenu du tableau en ficheir excel:
    this.exportationExcel_
        .setToolTipText("Exporte le contenu du tableau au format xls");
    exportationExcel_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        File fichier;
        final JFileChooser fc = new JFileChooser();
        final int returnVal = fc.showSaveDialog(Sinavi3ResultatsAttenteGeneraleElement.this);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
          fichier = fc.getSelectedFile();
          final File f = CtuluLibFile.appendExtensionIfNeeded(fichier, "xls");
          final Sinavi3ModeleExcel modele = new Sinavi3ModeleExcel();
          modele.nomColonnes_ = titreTableau_;
          modele.data_ = new Object[data.length + 2][titreTableau_.length];
          for (int i = 0; i < titreTableau_.length; i++) {
            modele.data_[0][i] = titreTableau_[i];
          }
          /** recopiage des donn�es */
          for (int i = 0; i < data.length; i++) {
            modele.data_[i + 2] = data[i];
          }
          modele.setNbRow(data.length + 2);
          /** on essaie d 'ecrire en format excel */
          final CtuluTableExcelWriter ecrivain = new CtuluTableExcelWriter(modele, f);
          try {
            ecrivain.write(null);
          } catch (final RowsExceededException _err) {
            FuLog.error(_err);
          } catch (final WriteException _err) {
            FuLog.error(_err);
          } catch (final IOException _err) {
            FuLog.error(_err);
          }
        }// fin du if si le composant est bon
      }// fin de la methode public actionPerformed
    });

    /*******************************************************************************************************************
     * gestion du panel courbes panelGraph_
     ******************************************************************************************************************/
    this.panelGraph_.setLayout(new BorderLayout());
    this.panelGraph_.add(this.optionPanelgraph_, BorderLayout.WEST);
    this.panelGraph_.add(this.panelGraphAffichage_, BorderLayout.CENTER);
    panelGraphAffichage_.addTab("Graphe", FudaaResource.FUDAA.getIcon("crystal_graphe"), panelCourbe_);
    panelGraphAffichage_.addTab("Histogramme", FudaaResource.FUDAA.getIcon("crystal_graphe"), panelHisto_);
    
    /*******************************************************************************************************************
     * gestion du panel courbes panelCourbe_
     ******************************************************************************************************************/
    // etape 1: architecture de la frame
    this.panelCourbe_.setLayout(new BorderLayout());

    // etape 2: creation de la description du graphe
    final String descriptionGraphe = affichageGraphe();

    // etape 3: rattachement du descriptif du graphe au graphe
    this.graphe_.setFluxDonnees(new ByteArrayInputStream(descriptionGraphe.getBytes()));

    // etape 4: affichage du graphe dans le panel associ�
    this.panelCourbe_.add(this.graphe_, BorderLayout.CENTER);
    
    // etape 5: bouton de generation du fichier image
    exportationgraphe_.setToolTipText("Exporte le graphe au format image");
    exportationgraphe_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {

        CtuluImageExport.exportImageFor(donnees_.application_, graphe_);
      }
    });

    // etape 6: creation du panel des boutons des courbes:
    this.controlPanelCourbes_.add(quitter2_);
    this.controlPanelCourbes_.add(exportationgraphe_);
    this.panelCourbe_.add(this.controlPanelCourbes_, BorderLayout.SOUTH);

    /*******************************************************************************************************************
     * gestion du panel histogramme
     ******************************************************************************************************************/
    // etape 1: architecture de la frame
    this.panelHisto_.setLayout(new BorderLayout());

    // etape 2: creation de la description du graphe
    final String descriptionHisto = this.affichageHistogramme();

    // etape 3: rattachement du descriptif du graphe au graphe
    this.histo_.setFluxDonnees(new ByteArrayInputStream(descriptionHisto.getBytes()));

    //final JScrollPane panneauHisto = new JScrollPane(this.histo_);
    // etape 4: affichage du graphe dans le panel associ�
    this.panelHisto_.add(this.histo_ /*panneauHisto*/, BorderLayout.CENTER);
    
    // etape 5: bouton de generation du fichier image
    exportationHisto_.setToolTipText("Exporte l'histogramme au format image");
    exportationHisto_.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {

        CtuluImageExport.exportImageFor(donnees_.application_, histo_);
      }
    });

    this.valSeuil_.addFocusListener(new FocusAdapter() {
      public void focusGained(final FocusEvent e) {
        valSeuil_.selectAll();
      }

      public void focusLost(final FocusEvent e) {
        if (!valSeuil_.getText().equals("")) {
          try {
            final float i = Float.parseFloat(valSeuil_.getText());
            if (i < 0) {
              new BuDialogError(donnees_.application_.getApp(), donnees_.application_.isSinavi_,
                  "Erreur!!  graine de la simulation est negatif \n il faut entrer un entier positif!!!!").activate();
              valSeuil_.setText("");
            }
          } catch (final NumberFormatException nfe) {
            new BuDialogError(donnees_.application_.getApp(), donnees_.application_.isSinavi_,
                "Erreur!!  ce nombre n'existe pas \n il faut entrer un entier naturel!").activate();
            valSeuil_.setText("");
          }
        }
      }
    });
    valideSeuil_.addActionListener(this);

    // etape 6: creation du panel des boutons des courbes:
    this.controlPanelHisto_.add(quitter3_);
    this.controlPanelHisto_.add(exportationHisto_);
    //this.controlPanelHisto_.add(new JLabel(" Seuil (h.min):"));
    //this.controlPanelHisto_.add(valSeuil_);
    //this.controlPanelHisto_.add(valideSeuil_);
    this.panelHisto_.add(this.controlPanelHisto_, BorderLayout.SOUTH);

    /** listener des boutons quitter */
    this.quitter_.setToolTipText("Ferme la sous-fen�tre");
    this.quitter2_.setToolTipText("Ferme la sous-fen�tre");
    this.quitter3_.setToolTipText("Ferme la sous-fen�tre");
    final ActionListener actionQuitter = new ActionListener() {
      public void actionPerformed(ActionEvent e) {
        Sinavi3ResultatsAttenteGeneraleElement.this.windowClosed();
      }
    };
    this.quitter_.addActionListener(actionQuitter);
    this.quitter2_.addActionListener(actionQuitter);
    this.quitter3_.addActionListener(actionQuitter);

    // ajout d'un menuBar
    // petite barre de menu agreable
 /*   final JMenuBar menuBar = new JMenuBar();
    final JMenu menuFile = new JMenu("Fichier");
    final JMenuItem menuFileExit = new JMenuItem("Quitter");
    final JMenu menuOption = new JMenu("Options");
    final JMenu menuInfo = new JMenu("A propos de");

    // menuFile.set.setLabel("Fichier");
    // menuOption.setLabel("Options");
    // menuFileExit.setLabel("Quitter");
    // menuInfo.setLabel("A propos de");

    menuFileExit.addActionListener(new ActionListener() {
      public void actionPerformed(final ActionEvent e) {
        Sinavi3ResultatsAttenteGeneraleElement.this.windowClosed();
      }
    });
    menuFile.add(menuFileExit);
    menuBar.add(menuFile);
    menuBar.add(menuOption);
    menuBar.add(menuInfo);
    setJMenuBar(menuBar);
*/
  }

  /**
   * Methode d'affichage du tableau remarque: cete m�thode sert aussi de rafraichissement du tableau
   * 
   * @param val entier qui indique le num�ro de la cat�gorie de navire � afficher si ce parametre vaut -1 alorso n
   *          affiche la totalit� des navires
   */
  void affichageTableau(final int val) {
    // affichage du tableau

    /**
     * Recherche des donn�es associ�es � l'�l�ment choisi par l utilisateur: on recherche dasn el tableau qui stocke
     * tous les �l�ments, l indice de l �l�ment dont on veut les r�sultats:
     */
    int ELEMENTCHOISI = -1;
    for (int k = 0; k < this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories.length; k++) {
      if (this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[k].typeElement == this.ListeTypesDepart_
          .getSelectedIndex()
          && this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[k].indiceElement == this.ListeElementDepart_
              .getSelectedIndex()) {
        ELEMENTCHOISI = k;
      }
    }
    if (ELEMENTCHOISI == -1) {
      new BuDialogError(donnees_.application_.getApp(), donnees_.application_.isSinavi_,
          "Erreur!! nous ne trouvons pas l'el�ment selectionn�...").activate();
    }

    // operation magique qui permet de determiner ce tableau
    int indiceTbaleau = 0;
    // etape 2: g�n�rer la liste des donn�es � afficher dans le tableau via une matrice de type object
    // ici le nombre de colonnes est de 2 puisqu'il s'agit d'un affichage du nom de la cat�gorie et de son nombre de
    // navires
    data = new Object[this.donnees_.listeBateaux_.listeNavires_.size()+3][26];

    
    int totalSecurite=0;
    int totalAcces=0;
    int totalOccupation=0;
    int totalIndisponibilite=0;
    int totaltotal=0;
    
    
    for (int i = 0; i < this.donnees_.listeBateaux_.listeNavires_.size(); i++) {
      if (this.tableauChoixNavires_[i].isSelected()) {
        data[indiceTbaleau][0] = this.donnees_.listeBateaux_.retournerNavire(i).nom;

        // ecriture des donn�es calcul�es pour les dur�es de parcours
        // si les cases correspondantes ont �t� coch�es:
        int indiceColonne = 1;
        if (this.choixNbNavires_.isSelected()) {
          data[indiceTbaleau][indiceColonne++] = ""
              + FonctionsSimu.diviserSimu(this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[i].nombreNaviresTotal);
        }

/*        if (this.choixMarees_.isSelected()) {
          if (donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[i].nbNaviresAttenteMaree == 0) {
            indiceColonne += 4;
          } else {
            // attente totale
            data[indiceTbaleau][indiceColonne++] = ""
                + Sinavi3TraduitHoraires
                    .traduitMinutesEnHeuresMinutes2((float) FonctionsSimu.diviserSimu(this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[i].attenteMareeTotale));
            // moyenne attentes sur la flotte
            data[indiceTbaleau][indiceColonne++] = ""
                + Sinavi3TraduitHoraires
                    .traduitMinutesEnHeuresMinutes2((float) (this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[i].attenteMareeTotale / this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[i].nombreNaviresTotal));

            // nombre de navires qui attendent
            data[indiceTbaleau][indiceColonne++] = ""
                + (float) FonctionsSimu.diviserSimu(this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[i].nbNaviresAttenteMaree)
                + " ("
                + (float) ((float) this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[i].nbNaviresAttenteMaree
                    / this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[i].nombreNaviresTotal * 100)
                + "%)";
            // moyenne attente sur les navires qui attendent
            data[indiceTbaleau][indiceColonne++] = ""
                + Sinavi3TraduitHoraires
                    .traduitMinutesEnHeuresMinutes2((float) (this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[i].attenteMareeTotale / this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[i].nbNaviresAttenteMaree));

          }
        }*/
        if (this.choixSecurite_.isSelected()) {
          if (donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[i].nbNaviresAttenteSecu == 0) {
            indiceColonne += 4;
          } else {
            // attente totale
            data[indiceTbaleau][indiceColonne++] = ""
                + Sinavi3TraduitHoraires
                    .traduitMinutesEnHeuresMinutes3((float) FonctionsSimu.diviserSimu(this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[i].attenteSecuTotale));
            // moyenne attentes sur la flotte
            data[indiceTbaleau][indiceColonne++] = ""
                + Sinavi3TraduitHoraires
                    .traduitMinutesEnHeuresMinutes3((float) (this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[i].attenteSecuTotale / this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[i].nombreNaviresTotal));

            // nombre de navires qui attendent
            data[indiceTbaleau][indiceColonne++] = ""
                + FonctionsSimu.diviserSimu(this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[i].nbNaviresAttenteSecu)
                + " ("
                + Math.round((float) this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[i].nbNaviresAttenteSecu
                    / this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[i].nombreNaviresTotal * 100)
                + "%)";
            // moyenne attente sur les navires qui attendent
            data[indiceTbaleau][indiceColonne++] = ""
                + Sinavi3TraduitHoraires
                    .traduitMinutesEnHeuresMinutes3((float) (this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[i].attenteSecuTotale / this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[i].nbNaviresAttenteSecu));

          }
        }

        if (this.choixAcces_.isSelected()) {
          if (donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[i].nbNaviresAttenteAcces == 0) {
            indiceColonne += 4;
          } else {
            // attente totale
            data[indiceTbaleau][indiceColonne++] = ""
                + Sinavi3TraduitHoraires
                    .traduitMinutesEnHeuresMinutes3((float)FonctionsSimu.diviserSimu( this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[i].attenteAccesTotale));
            // moyenne attentes sur la flotte
            data[indiceTbaleau][indiceColonne++] = ""
                + Sinavi3TraduitHoraires
                    .traduitMinutesEnHeuresMinutes3((float) (this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[i].attenteAccesTotale / this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[i].nombreNaviresTotal));

            // nombre de navires qui attendent
            data[indiceTbaleau][indiceColonne++] = ""
                + FonctionsSimu.diviserSimu(this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[i].nbNaviresAttenteAcces)
                + " ("
                + Math.round((float) this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[i].nbNaviresAttenteAcces
                    / this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[i].nombreNaviresTotal * 100)
                + "%)";
            // moyenne attente sur les navires qui attendent
            data[indiceTbaleau][indiceColonne++] = ""
                + Sinavi3TraduitHoraires
                    .traduitMinutesEnHeuresMinutes3((float) (this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[i].attenteAccesTotale / this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[i].nbNaviresAttenteAcces));

          }
        }

        if (this.choixOccupation_.isSelected()) {
          if (donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[i].nbNaviresAtenteOccup == 0) {
            indiceColonne += 4;
          } else {
            // attente totale
            data[indiceTbaleau][indiceColonne++] = ""
                + Sinavi3TraduitHoraires
                    .traduitMinutesEnHeuresMinutes3((float)FonctionsSimu.diviserSimu( this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[i].attenteOccupTotale));
            // moyenne attentes sur la flotte
            data[indiceTbaleau][indiceColonne++] = ""
                + Sinavi3TraduitHoraires
                    .traduitMinutesEnHeuresMinutes3((float) (this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[i].attenteOccupTotale / this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[i].nombreNaviresTotal));

            // nombre de navires qui attendent
            data[indiceTbaleau][indiceColonne++] = ""
                + FonctionsSimu.diviserSimu(this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[i].nbNaviresAtenteOccup)
                + " ("
                + Math.round((float) this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[i].nbNaviresAtenteOccup
                    / this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[i].nombreNaviresTotal * 100)
                + "%)";
            // moyenne attente sur les navires qui attendent
            data[indiceTbaleau][indiceColonne++] = ""
                + Sinavi3TraduitHoraires
                    .traduitMinutesEnHeuresMinutes3((float) (this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[i].attenteOccupTotale / this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[i].nbNaviresAtenteOccup));

          }
        }

        if (this.choixPannes_.isSelected()) {
          if (donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[i].nbNaviresAttentePanne == 0) {
            indiceColonne += 4;
          } else {
            // attente totale
            data[indiceTbaleau][indiceColonne++] = ""
                + Sinavi3TraduitHoraires
                    .traduitMinutesEnHeuresMinutes3((float) FonctionsSimu.diviserSimu(this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[i].attentePanneTotale));
            // moyenne attentes sur la flotte
            data[indiceTbaleau][indiceColonne++] = ""
                + Sinavi3TraduitHoraires
                    .traduitMinutesEnHeuresMinutes3((float) (this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[i].attentePanneTotale / this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[i].nombreNaviresTotal));

            // nombre de navires qui attendent
            data[indiceTbaleau][indiceColonne++] = ""
                + FonctionsSimu.diviserSimu(this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[i].nbNaviresAttentePanne)
                + " ("
                + Math.round((float) this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[i].nbNaviresAttentePanne
                    / this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[i].nombreNaviresTotal * 100)
                + "%)";
            // moyenne attente sur les navires qui attendent
            data[indiceTbaleau][indiceColonne++] = ""
                + Sinavi3TraduitHoraires
                    .traduitMinutesEnHeuresMinutes3((float) (this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[i].attentePanneTotale / this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[i].nbNaviresAttentePanne));

          }
        }

        if (this.choixTotalAttente_.isSelected()) {
          // attente totale
          if (donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[i].nbNaviresAttenteTotale == 0) {
            indiceColonne += 4;
          } else {
            data[indiceTbaleau][indiceColonne++] = ""
                + Sinavi3TraduitHoraires
                    .traduitMinutesEnHeuresMinutes3((float) FonctionsSimu.diviserSimu(this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[i].attenteMegaTotale));
            // moyenne attentes sur la flotte
            data[indiceTbaleau][indiceColonne++] = ""
                + Sinavi3TraduitHoraires
                    .traduitMinutesEnHeuresMinutes3((float) (this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[i].attenteMegaTotale / this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[i].nombreNaviresTotal));

            // nombre de navires qui attendent
            data[indiceTbaleau][indiceColonne++] = ""
                + FonctionsSimu.diviserSimu(this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[i].nbNaviresAttenteTotale)
                + " ("
                + Math.round((float) this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[i].nbNaviresAttenteTotale
                    / this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[i].nombreNaviresTotal * 100)
                + "%)";
            // moyenne attente sur les navires qui attendent
            data[indiceTbaleau][indiceColonne++] = ""
                + Sinavi3TraduitHoraires
                    .traduitMinutesEnHeuresMinutes3((float) (this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[i].attenteMegaTotale / this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[i].nbNaviresAttenteTotale));

          }
        }
        totalAcces+=FonctionsSimu.diviserSimu(this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[i].attenteAccesTotale);
        totalSecurite+=FonctionsSimu.diviserSimu(this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[i].attenteSecuTotale);
        totalOccupation+=FonctionsSimu.diviserSimu(this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[i].attenteOccupTotale);
        totalIndisponibilite+=FonctionsSimu.diviserSimu(this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[i].attentePanneTotale);
        totaltotal+=FonctionsSimu.diviserSimu(this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[i].attenteMegaTotale);
        indiceTbaleau++;
      }// fin du if si la cat�gorie a �t� selectionn�e pour etre affich�e
    }
    int cl=2;
    data[++indiceTbaleau][0]="TOTAL: "+(indiceTbaleau-1)+" cat�gories affich�es";
    
    if (this.choixSecurite_.isSelected()) {
        data[indiceTbaleau][cl]="Total s�curite";
        data[indiceTbaleau+1][cl]=Sinavi3TraduitHoraires.traduitMinutesEnHeuresMinutes3(totalSecurite);	
        cl+=4;
        }
    if (this.choixAcces_.isSelected()) {
    data[indiceTbaleau][cl]="Total acc�s";
    data[indiceTbaleau+1][cl]=Sinavi3TraduitHoraires.traduitMinutesEnHeuresMinutes3(totalAcces);
    cl+=4;
    }
   
    if (this.choixOccupation_.isSelected()) {
        data[indiceTbaleau][cl]="Total occupation";
        data[indiceTbaleau+1][cl]=Sinavi3TraduitHoraires.traduitMinutesEnHeuresMinutes3(totalOccupation);	
        cl+=4;
        }
    if (this.choixPannes_.isSelected()) {
        data[indiceTbaleau][cl]="Total pannes";
        data[indiceTbaleau+1][cl]=Sinavi3TraduitHoraires.traduitMinutesEnHeuresMinutes3(totalIndisponibilite);	
        cl+=4;
        }
    if (this.choixTotalAttente_.isSelected()) {
        data[indiceTbaleau][cl]="Total toutes attentes";
        data[indiceTbaleau+1][cl]=Sinavi3TraduitHoraires.traduitMinutesEnHeuresMinutes3(totaltotal);	
        }
    // }
    /*
     * else if(val<this.donnees_.categoriesNavires_.listeNavires_.size()) { //on affiche uniquement la ligne
     * selectionn� par le combolist: data= new Object[1][this.titreTableau_.length];
     * data[0][0]=this.donnees_.categoriesNavires_.retournerNavire(val).nom; //on complete les don�nes par le tableau de
     * resultats: //data[0][0].......... int indiceColonne=1; if(this.choixNbNavires_.isSelected())
     * data[0][indiceColonne++]=""+(float)this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[val].nombreNaviresTotal;
     * if(this.choixMarees_.isSelected()){ //attente totale
     * data[0][indiceColonne++]=""+(float)this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[val].attenteMareeTotale;
     * //moyenne attente sur les navires qui attendent
     * data[0][indiceColonne++]=""+(float)(this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[val].attenteMareeTotale/this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[val].nbNaviresAttenteMaree);
     * //nombre de navires qui attendent
     * data[0][indiceColonne++]=""+(float)this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[val].nbNaviresAttenteMaree +"
     * ("+(float)((float)this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[val].nbNaviresAttenteMaree/this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[val].nombreNaviresTotal*100)
     * +"%)"; //moyenne attentes sur la flotte
     * data[0][indiceColonne++]=""+(float)(this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[val].attenteMareeTotale/this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[val].nombreNaviresTotal); }
     * if(this.choixSecurite_.isSelected()){ //attente totale
     * data[0][indiceColonne++]=""+(float)this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[val].attenteSecuTotale;
     * //moyenne attente sur les navires qui attendent
     * data[0][indiceColonne++]=""+(float)(this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[val].attenteSecuTotale/this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[val].nbNaviresAttenteSecu);
     * //nombre de navires qui attendent
     * data[0][indiceColonne++]=""+(float)this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[val].nbNaviresAttenteSecu +"
     * ("+(float)((float)this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[val].nbNaviresAttenteSecu/this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[val].nombreNaviresTotal*100)
     * +"%)"; //moyenne attentes sur la flotte
     * data[0][indiceColonne++]=""+(float)(this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[val].attenteSecuTotale/this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[val].nombreNaviresTotal); }
     * if(this.choixAcces_.isSelected()){ //attente totale
     * data[0][indiceColonne++]=""+(float)this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[val].attenteAccesTotale;
     * //moyenne attente sur les navires qui attendent
     * data[0][indiceColonne++]=""+(float)(this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[val].attenteAccesTotale/this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[val].nbNaviresAttenteAcces);
     * //nombre de navires qui attendent
     * data[0][indiceColonne++]=""+(float)this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[val].nbNaviresAttenteAcces +"
     * ("+(float)((float)this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[val].nbNaviresAttenteAcces/this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[val].nombreNaviresTotal*100)
     * +"%)"; //moyenne attentes sur la flotte
     * data[0][indiceColonne++]=""+(float)(this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[val].attenteAccesTotale/this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[val].nombreNaviresTotal); }
     * if(this.choixOccupation_.isSelected()){ //attente totale
     * data[0][indiceColonne++]=""+(float)this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[val].attenteOccupTotale;
     * //moyenne attente sur les navires qui attendent
     * data[0][indiceColonne++]=""+(float)(this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[val].attenteOccupTotale/this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[val].nbNaviresAtenteOccup);
     * //nombre de navires qui attendent
     * data[0][indiceColonne++]=""+(float)this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[val].nbNaviresAtenteOccup +"
     * ("+(float)((float)this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[val].nbNaviresAtenteOccup/this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[val].nombreNaviresTotal*100)
     * +"%)"; //moyenne attentes sur la flotte
     * data[0][indiceColonne++]=""+(float)(this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[val].attenteOccupTotale/this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[val].nombreNaviresTotal); }
     * if(this.choixPannes_.isSelected()){ //attente totale
     * data[0][indiceColonne++]=""+(float)this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[val].attentePanneTotale;
     * //moyenne attente sur les navires qui attendent
     * data[0][indiceColonne++]=""+(float)(this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[val].attentePanneTotale/this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[val].nbNaviresAttentePanne);
     * //nombre de navires qui attendent
     * data[0][indiceColonne++]=""+(float)this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[val].nbNaviresAttentePanne +"
     * ("+(float)((float)this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[val].nbNaviresAttentePanne/this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[val].nombreNaviresTotal*100)
     * +"%)"; //moyenne attentes sur la flotte
     * data[0][indiceColonne++]=""+(float)(this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[val].attentePanneTotale/this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[val].nombreNaviresTotal); }
     * if(this.choixTotalAttente_.isSelected()) { // attente totale
     * data[0][indiceColonne++]=""+(float)this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[val].attenteMegaTotale;
     * //moyenne attente sur les navires qui attendent
     * data[0][indiceColonne++]=""+(float)(this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[val].attenteMegaTotale/this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[val].nbNaviresAttenteTotale);
     * //nombre de navires qui attendent
     * data[0][indiceColonne++]=""+(float)this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[val].nbNaviresAttenteTotale +"
     * ("+(float)((float)this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[val].nbNaviresAttenteTotale/this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[val].nombreNaviresTotal*100)
     * +"%)"; //moyenne attentes sur la flotte
     * data[0][indiceColonne++]=""+(float)(this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[val].attentePanneTotale/this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[val].nombreNaviresTotal); } }
     */
    // etape 3: creation du tableau a partir des donn�es g�n�r�es ci dessus:
    this.tableau_ = new BuTable(data, this.titreTableau_) {
      public boolean isCellEditable(final int row, final int col) {
        return false;
      }
    };

    // etape 3.5: dimensionnement des colonnes du tableau 
    for(int i=0; i<tableau_.getModel().getColumnCount();i++){
        TableColumn column = tableau_.getColumnModel().getColumn(i);
               column.setPreferredWidth(150); 
        }

    
    
    // etape 4: ajout sdu tableau cr�� dans l'interface
    org.fudaa.fudaa.sinavi.factory.ColumnAutoSizer.sizeColumnsToFit(tableau_);
    tableau_.revalidate();
    this.panelTableau_.removeAll();
    this.panelTableau_.setLayout(new BorderLayout());
    this.panelTableau_.add(tableau_.getTableHeader(), BorderLayout.PAGE_START);
    this.panelTableau_.add(this.tableau_, BorderLayout.CENTER);
    // this.panelTableau_.add(this.controlPanel_,BorderLayout.SOUTH);

    // mise a jour de l'affichage
    this.revalidate();
    this.updateUI();

  }

  /**
   * Methode qui permet de d�crire le graphe � afficher.
   * 
   * @return chaine: chaine qui contient la des cription de la chaine de caracteres.
   */
  String affichageGraphe() {
	  
	  boolean echelleHeures_=false;
	  if (Sinavi3AlgorithmeAttentesGenerales.determineAttenteMax(donnees_) >= 240) echelleHeures_=true;

    // determiner el nombre de cat�gories de navires selectionn�s
    int nbNavires = 0;
    for (int k = 0; k < this.tableauChoixNavires2_.length; k++) {
      if (this.tableauChoixNavires2_[k].isSelected()) {
        nbNavires++;
      }
    }

    // variable qui permet de determiner a quel indice du navire on est car on en prends pas tous les indices de navires
    int indiceNavire = 0;

    int ELEMENTCHOISI = -1;
    for (int k = 0; k < this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories.length; k++) {
      if (this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[k].typeElement == this.ListeTypesDepart_
          .getSelectedIndex()
          && this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[k].indiceElement == this.ListeElementDepart_
              .getSelectedIndex()) {
        ELEMENTCHOISI = k;
      }
    }
    if (ELEMENTCHOISI == -1) {
      new BuDialogError(donnees_.application_.getApp(), donnees_.application_.isSinavi_,
          "Erreur!! nous ne trouvons pas l'el�ment selectionn�...").activate();
    }

    String g = "";

    g += "graphe\n{\n";
    g += "  titre \"Attentes au sein de l'�l�ment " + (String) this.ListeElementDepart_.getSelectedItem();
    if (this.Sens_.getSelectedIndex() == 0) {
      g += " dans le sens avalant";
    } else if (this.Sens_.getSelectedIndex() == 1) {
      g += " dans le sens montant";
    } else {
      g += " dans les deux sens cumul�s";
    }
    g += " \"\n";

    /*if (this.choixMarees_.isSelected()) {
      g += "  sous-titre \" Attentes de Mar�es \"\n";
    } else*/ if (this.choixSecurite2_.isSelected()) {
      g += "  sous-titre \"Attentes de s�curit�\"\n";
    } else if (this.choixAcces2_.isSelected()) {
      g += "  sous-titre \"Attentes d'acc�s\"\n";
    } else if (this.choixOccupation2_.isSelected()) {
      g += "  sous-titre \"Attentes d'occupation\"\n";
    } else if (this.choixPannes2_.isSelected()) {
      g += "  sous-titre \"Attentes d'indisponibilit�\"\n";
    } else if (this.choixTotalAttente2_.isSelected()) {
      g += "  sous-titre \"Attentes totales\"\n";
    }

    g += "  animation non\n";
    g += "  legende " + "oui" + "\n";
    
    g += " marges\n {\n";
    g += " gauche 100\n"; g += " droite 100\n"; g += " haut 50\n"; g += " bas 30\n }\n";

    g += "  axe\n  {\n"; // abscisses
    g += "    titre \"Cat�gorie" + "\"\n";
    g += "    unite \"num�ro\"\n";
    g += "    orientation " + "horizontal" + "\n";
    g += "    graduations oui\n";
    g += "    minimum " + 0 + "\n";
    g += "    maximum " + (nbNavires + 3)// (this.donnees_.categoriesNavires_.listeNavires_.size()+3)
        + "\n";
    g += "  }\n";

    g += "  axe\n  {\n"; // Ordonn�es
    g += "    titre \"Dur�e" + "\"\n";
    g += "    unite \"";
    if (echelleHeures_==true) g += "heures"; else g += "minutes";
    g += "\"\n";
    g += "    orientation " + "vertical" + "\n";
    g += "    graduations oui\n";
    g += "    minimum " + 0 + "\n";
    g += "    maximum ";
    if (echelleHeures_==true) g+= (Sinavi3AlgorithmeAttentesGenerales.determineAttenteMax(donnees_))/60;
    else g+= (Sinavi3AlgorithmeAttentesGenerales.determineAttenteMax(donnees_));
    /*g+=Sinavi3TraduitHoraires.traduitMinutesEnHeuresMinutes((float) Sinavi3AlgorithmeAttentesGenerales
            .determineAttenteMax(donnees_))*/;
        // ********************ATTTTTTTTTTTTTEEEEEEENNNNNNNNNNTTTTTTTTION A DEFINIR DES QU ON CONNAIS LE TABELAU/ ON
        // DETERMINE LE MAX
    g+= "\n";
    g += "  }\n";
    /*
     * for (int i= 0; i < this.donnees_.categoriesNavires_.listeNavires_.size(); i++) { //A RESERVER POUR GERER
     * PLUSIEURS COURBES
     */

    // ******************************debut histo max************************************************
    if (this.choixMax_.isSelected()) {
      g += "  courbe\n  {\n";
      g += "    titre \"";

     /* if (this.choixMarees_.isSelected()) {
        g += "attentes Mar�es maxi";
      } else*/ if (this.choixSecurite2_.isSelected()) {
        g += "attentes de s�curit� maximales";
      } else if (this.choixAcces2_.isSelected()) {
        g += "attentes d'acc�s maximales";
      } else if (this.choixOccupation2_.isSelected()) {
        g += "attentes d'occupation maximales";
      } else if (this.choixPannes2_.isSelected()) {
        g += "attentes d'indisponibilit� maximales";
      } else if (this.choixTotalAttente2_.isSelected()) {
        g += "attentes totales maximales";
      }
      // g += "dur�es maxi";
      /*
       * + "/" + choixString(choix, i, 1) + "/" + choixString(choix, i, 2)
       */
      g += "\"\n";
      g += "    type " + "courbe" + "\n";
      g += "    aspect\n {\n";
      g += "contour.largeur 1 \n";
      g += "surface.couleur  	BB0000 \n";
      g += "texte.couleur 000000 \n";
      g += "contour.couleur BB0000 \n";
      g += "    }\n";
      g += "    valeurs\n    {\n";
      /*
       * final Vector coordX= (Vector)_courbes.get(i * 2); final Vector coordY= (Vector)_courbes.get(i * 2 + 1); for
       * (int n= 0;(n < coordX.size()) || (n < coordY.size()); n++) {
       */
      indiceNavire = 0;
      for (int n = 0; n < this.donnees_.listeBateaux_.listeNavires_.size(); n++) {
        if (this.tableauChoixNavires2_[n].isSelected()) {
          g += (indiceNavire + 1)// numero de la cat�gorie
              + " ";
          indiceNavire++;
          /*if (this.choixMarees_.isSelected()) {
            g += Sinavi3TraduitHoraires
                .traduitMinutesEnHeuresMinutes((float) this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[n].attenteMareeMaxi);
          } else*/
          if (this.choixSecurite2_.isSelected()) {
        	  if (echelleHeures_) g += ((float) this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[n].attenteSecuMaxi)/60;
        	  else g += ((float) this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[n].attenteSecuMaxi);
          } else if (this.choixAcces2_.isSelected()) {
        	  if (echelleHeures_) g += ((float) this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[n].attenteAccesMaxi)/60;
        	  else g += ((float) this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[n].attenteAccesMaxi);
          } else if (this.choixOccupation2_.isSelected()) {
        	  if (echelleHeures_) g += ((float) this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[n].attenteOccupMaxi)/60;
        	  else g += ((float) this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[n].attenteOccupMaxi);
          } else if (this.choixPannes2_.isSelected()) {
        	  if (echelleHeures_) g += ((float) this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[n].attentePanneMaxi)/60;
        	  else g += ((float) this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[n].attentePanneMaxi);
          } else if (this.choixTotalAttente2_.isSelected()) {
        	  if (echelleHeures_) g += ((float) this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[n].attenteTotaleMaxi)/60;
        	  else g += ((float) this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[n].attenteTotaleMaxi);
          }

          g += "\n etiquette  \n \"" + this.donnees_.listeBateaux_.retournerNavire(n).nom + "\" \n"/* + */
              + "\n";
        }
      }// din du pour remplissage des coordonn�es
      g += "    }\n";
      g += "  }\n";
    }
    // //******************************fin histo max************************************************

    // ******************************debut histo moy************************************************
    if (this.choixMoy_.isSelected()) {
      g += "  courbe\n  {\n";
      g += "    titre \"";

      /*if (this.choixMarees_.isSelected()) {
        g += "attentes Mar�es moyenne";
      } else*/ if (this.choixSecurite2_.isSelected()) {
        g += "attentes de s�curit� moyennes";
      } else if (this.choixAcces2_.isSelected()) {
        g += "attentes d'acc�s moyennes";
      } else if (this.choixOccupation2_.isSelected()) {
        g += "attentes d'occupation moyennes";
      } else if (this.choixPannes2_.isSelected()) {
        g += "attentes d'indisponibilit� moyennes";
      } else if (this.choixTotalAttente2_.isSelected()) {
        g += "attentes totales moyennes";
      }

      /*
       * + "/" + choixString(choix, i, 1) + "/" + choixString(choix, i, 2)
       */
      g += "\"\n";
      g += "    type " + "courbe" + "\n";
      g += "    aspect\n {\n";
      g += "contour.largeur 1 \n";
      g += "surface.couleur BB8800 \n";
      g += "texte.couleur 000000 \n";
      g += "contour.couleur BB8800 \n";
      g += "    }\n";
      g += "    valeurs\n    {\n";
      /*
       * final Vector coordX= (Vector)_courbes.get(i * 2); final Vector coordY= (Vector)_courbes.get(i * 2 + 1); for
       * (int n= 0;(n < coordX.size()) || (n < coordY.size()); n++) {
       */
      indiceNavire = 0;
      for (int n = 0; n < this.donnees_.listeBateaux_.listeNavires_.size(); n++) {
        if (this.tableauChoixNavires2_[n].isSelected()) {
          g += (indiceNavire + 1)// numero de la cat�gorie
              + " ";
          indiceNavire++;
          /*if (this.choixMarees_.isSelected()) {
            g += Sinavi3TraduitHoraires
                .traduitMinutesEnHeuresMinutes((float) (this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[n].attenteMareeTotale / this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[n].nbNaviresAttenteMaree));
          } else*/
          if (this.choixSecurite2_.isSelected()) {
        	  if (this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[n].nbNaviresAttenteSecu == 0) g+=0;
        	  else {
        		  if (echelleHeures_) g += ((float) (this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[n].attenteSecuTotale / this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[n].nbNaviresAttenteSecu))/60;
        		  else g += ((float) (this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[n].attenteSecuTotale / this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[n].nbNaviresAttenteSecu));
        	  }
          } else if (this.choixAcces2_.isSelected()) {
        	  if (this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[n].nbNaviresAttenteAcces == 0) g+=0;
        	  else {
        		  if (echelleHeures_) g += ((float) (this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[n].attenteAccesTotale / this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[n].nbNaviresAttenteAcces))/60;
        		  else g += ((float) (this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[n].attenteAccesTotale / this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[n].nbNaviresAttenteAcces));
        	  }
          } else if (this.choixOccupation2_.isSelected()) {
        	  if (this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[n].nbNaviresAtenteOccup == 0) g+=0;
        	  else {
        		  if (echelleHeures_) g += ((float) (this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[n].attenteOccupTotale / this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[n].nbNaviresAtenteOccup))/60;
        		  else g += ((float) (this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[n].attenteOccupTotale / this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[n].nbNaviresAtenteOccup));
        	  }
          } else if (this.choixPannes2_.isSelected()) {
        	  if (this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[n].nbNaviresAttentePanne == 0) g+=0;
        	  else {
        		  if (echelleHeures_) g += ((float) (this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[n].attentePanneTotale / this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[n].nbNaviresAttentePanne))/60;
        		  else g += ((float) (this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[n].attentePanneTotale / this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[n].nbNaviresAttentePanne));
        	  }
          } else if (this.choixTotalAttente2_.isSelected()) {
        	  if (this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[n].nbNaviresAttenteTotale == 0) g+=0;
        	  else {
        		  if (echelleHeures_) g += ((float) (this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[n].attenteMegaTotale / this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[n].nbNaviresAttenteTotale))/60;
        		  else g += ((float) (this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[n].attenteMegaTotale / this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[n].nbNaviresAttenteTotale));
        	  }
          }

          g += "\n";
        }
      }// din du pour remplissage des coordonn�es
      g += "    }\n";
      g += "  }\n";
    }
    // //******************************fin histo moy************************************************

    // ******************************debut histo min************************************************
    if (this.choixMin_.isSelected()) {

      g += "  courbe\n  {\n";
      g += "    titre \"";
      /*if (this.choixMarees_.isSelected()) {
        g += "attentes Mar�es mini";
      } else*/ if (this.choixSecurite2_.isSelected()) {
        g += "attentes de s�curit� minimales";
      } else if (this.choixAcces2_.isSelected()) {
        g += "attentes d'acc�s minimales";
      } else if (this.choixOccupation2_.isSelected()) {
        g += "attentes d'occupations minimales";
      } else if (this.choixPannes2_.isSelected()) {
        g += "attentes d'indisponibilit� minimales";
      } else if (this.choixTotalAttente2_.isSelected()) {
        g += "attentes totales minimales";
      }

      g += "\"\n";
      g += "    type " + "courbe" + "\n";
      g += "    aspect\n {\n";
      g += "contour.largeur 1 \n";
      g += "surface.couleur BBCC00 \n";
      g += "texte.couleur 000000 \n";
      g += "contour.couleur BBC00 \n";
      g += "    }\n";
      g += "    valeurs\n    {\n";
      /*
       * final Vector coordX= (Vector)_courbes.get(i * 2); final Vector coordY= (Vector)_courbes.get(i * 2 + 1); for
       * (int n= 0;(n < coordX.size()) || (n < coordY.size()); n++) {
       */
      indiceNavire = 0;
      for (int n = 0; n < this.donnees_.listeBateaux_.listeNavires_.size(); n++) {
        if (this.tableauChoixNavires2_[n].isSelected()) {
          g += (indiceNavire + 1)// numero de la cat�gorie
              + " ";
          indiceNavire++;

          /*if (this.choixMarees_.isSelected()) {
            g += Sinavi3TraduitHoraires
                .traduitMinutesEnHeuresMinutes((float) this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[n].attenteMareeMini);
          } else*/
          if (this.choixSecurite2_.isSelected()) {
            if (echelleHeures_) g += ((float) this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[n].attenteSecuMini)/60;
            else g += ((float) this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[n].attenteSecuMini);
          } else if (this.choixAcces2_.isSelected()) {
        	if (echelleHeures_) g += ((float) this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[n].attenteAccesMini)/60;
        	else g += ((float) this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[n].attenteAccesMini);
          } else if (this.choixOccupation2_.isSelected()) {
        	  if (echelleHeures_) g += ((float) this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[n].attenteOccupMini)/60;
        	  else g += ((float) this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[n].attenteOccupMini);
          } else if (this.choixPannes2_.isSelected()) {
        	  if (echelleHeures_) g += ((float) this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[n].attentePanneMini)/60;
        	  else g += ((float) this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[n].attentePanneMini);
          } else if (this.choixTotalAttente2_.isSelected()) {
        	  if (echelleHeures_) g += ((float) this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[n].attenteTotaleMini)/60;
        	  else g += ((float) this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[n].attenteTotaleMini);
          }

          g += "\n";

        }
      }// din du pour remplissage des coordonn�es
      g += "    }\n";
      g += "  }\n";

    }
    // //******************************fin histo min************************************************

    if (seuil_) {
      /**
       * declaration d'un seuil
       */
      g += " contrainte\n";
      g += "{\n";
      // a mettre le seuil
      g += "titre \"seuil \"\n";
      // str+="orientation horizontal \n";
      g += " type max\n";
      if(echelleHeures_==true) g += " valeur " + Sinavi3TraduitHoraires.traduitHeuresMinutesEnHeures(valeurSeuil) + CtuluLibString.LINE_SEP_SIMPLE;// /la valeur ordonn�e du seuil
      else g += " valeur " + Sinavi3TraduitHoraires.traduitHeuresMinutesEnMinutes(valeurSeuil) + CtuluLibString.LINE_SEP_SIMPLE;

      g += " \n }\n";
      // }//fin du for
    }

    return g;
  }

  /**
   * methode qui retoune l histogramme correspondant aux donn�es resultats:
   * 
   * @return
   */
  String affichageHistogramme() {
	  
	  boolean echelleHeures_=false;
	  if (Sinavi3AlgorithmeAttentesGenerales.determineAttenteMax(donnees_) >= 240) echelleHeures_=true;

    // determiner el nombre de cat�gories de navires selectionn�s
    int nbNavires = 0;
    for (int k = 0; k < this.tableauChoixNavires2_.length; k++) {
      if (this.tableauChoixNavires2_[k].isSelected()) {
        nbNavires++;
      }
    }

    // variable qui permet de determiner a quel indice du navire on est car on en prends pas tous les indices de navires
    int indiceNavire = 0;

    int ELEMENTCHOISI = -1;
    for (int k = 0; k < this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories.length; k++) {
      if (this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[k].typeElement == this.ListeTypesDepart_
          .getSelectedIndex()
          && this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[k].indiceElement == this.ListeElementDepart_
              .getSelectedIndex()) {
        ELEMENTCHOISI = k;
      }
    }
    if (ELEMENTCHOISI == -1) {
      new BuDialogError(donnees_.application_.getApp(), donnees_.application_.isSinavi_,
          "Erreur!! nous ne trouvons pas l'el�ment selectionn�...").activate();
    }

    String g = "";

    g += "graphe\n{\n";
    g += "  titre \"Attentes au sein de l'�l�ment " + (String) this.ListeElementDepart_.getSelectedItem();/* +" \"\n"; */
    if (this.Sens_.getSelectedIndex() == 0) {
      g += " dans le sens avalant";
    } else if (this.Sens_.getSelectedIndex() == 1) {
      g += " dans le sens montant";
    } else {
      g += " dans les deux sens cumul�s";
    }
    g += " \"\n";

    /*if (this.choixMarees_.isSelected()) {
      g += "  sous-titre \" Attentes de Mar�es \"\n";
    } else*/ if (this.choixSecurite2_.isSelected()) {
      g += "  sous-titre \"Attentes de s�curit�\"\n";
    } else if (this.choixAcces2_.isSelected()) {
      g += "  sous-titre \"Attentes d'acc�s\"\n";
    } else if (this.choixOccupation2_.isSelected()) {
      g += "  sous-titre \"Attentes d'occupation\"\n";
    } else if (this.choixPannes2_.isSelected()) {
      g += "  sous-titre \"Attentes d'indisponibilit�\"\n";
    } else if (this.choixTotalAttente2_.isSelected()) {
      g += "  sous-titre \"Attentes totales\"\n";
    }

    g += "  animation non\n";
    g += "  legende " + "oui" + "\n";
    
    g += " marges\n {\n";
    g += " gauche 100\n"; g += " droite 100\n"; g += " haut 50\n"; g += " bas 30\n }\n";
    /*
     * g += " marges\n {\n"; g += " gauche " + ((Integer)preference.dlgParamAff.tfMargeG.getValue()).intValue() + "\n";
     * g += " droite " + ((Integer)preference.dlgParamAff.tfMargeD.getValue()).intValue() + "\n"; g += " haut " +
     * ((Integer)preference.dlgParamAff.tfMargeH.getValue()).intValue() + "\n"; g += " bas " +
     * ((Integer)preference.dlgParamAff.tfMargeB.getValue()).intValue() + "\n }\n";
     */

    g += "  axe\n  {\n"; // abscisses
    g += "    titre \"Cat�gorie" + "\"\n";
    g += "    unite \"num�ro\"\n";
    g += "    orientation " + "horizontal" + "\n";
    g += "    graduations oui\n";
    g += "    minimum " + 0 + "\n";
    g += "    maximum " + (nbNavires + 3)// (this.donnees_.categoriesNavires_.listeNavires_.size()+3)
        + "\n";
    g += "  }\n";

    g += "  axe\n  {\n"; // Ordonn�es
    g += "    titre \"Dur�e" + "\"\n";
    g += "    unite \"";
    if (echelleHeures_==true) g += "heures"; else g += "minutes";
    g += "\"\n";
    g += "    orientation " + "vertical" + "\n";
    g += "    graduations oui\n";
    g += "    minimum " + 0 + "\n";
    g += "    maximum ";
    if (echelleHeures_==true) g+=
        (Sinavi3AlgorithmeAttentesGenerales.determineAttenteMax(donnees_))/60;
    else g+= (Sinavi3AlgorithmeAttentesGenerales.determineAttenteMax(donnees_));
    /*g+=Sinavi3TraduitHoraires.traduitMinutesEnHeuresMinutes((float) Sinavi3AlgorithmeAttentesGenerales
            .determineAttenteMax(donnees_))
        // ********************ATTTTTTTTTTTTTEEEEEEENNNNNNNNNNTTTTTTTTION A DEFINIR DES QU ON CONNAIS LE TABELAU/ ON
        // DETERMINE LE MAX
        + "\n";*/
    g += "\n";
    g += "  }\n";
    /*
     * for (int i= 0; i < this.donnees_.categoriesNavires_.listeNavires_.size(); i++) { //A RESERVER POUR GERER
     * PLUSIEURS COURBES
     */

    // ******************************debut histo max************************************************
    if (this.choixMax_.isSelected()) {
      g += "  courbe\n  {\n";
      g += "    titre \"";

      /*if (this.choixMarees_.isSelected()) {
        g += "attentes Mar�es maxi";
      } else*/ if (this.choixSecurite2_.isSelected()) {
        g += "attentes de s�curit� maximales";
      } else if (this.choixAcces2_.isSelected()) {
        g += "attentes d'acc�s maximales";
      } else if (this.choixOccupation2_.isSelected()) {
        g += "attentes d'occupation maximales";
      } else if (this.choixPannes2_.isSelected()) {
        g += "attentes d'indisponibilit� maximales";
      } else if (this.choixTotalAttente2_.isSelected()) {
        g += "attentes totales maximales";
      }
      // g += "dur�es maxi";
      /*
       * + "/" + choixString(choix, i, 1) + "/" + choixString(choix, i, 2)
       */
      g += "\"\n";
      g += "    type " + "histogramme" + "\n";
      g += "    aspect\n {\n";
      g += "contour.largeur 1 \n";
      g += "surface.couleur  	BB0000 \n";
      g += "texte.couleur 000000 \n";
      g += "contour.couleur 000000 \n";
      g += "    }\n";
      g += "    valeurs\n    {\n";
      /*
       * final Vector coordX= (Vector)_courbes.get(i * 2); final Vector coordY= (Vector)_courbes.get(i * 2 + 1); for
       * (int n= 0;(n < coordX.size()) || (n < coordY.size()); n++) {
       */
      indiceNavire = 0;
      for (int n = 0; n < this.donnees_.listeBateaux_.listeNavires_.size(); n++) {
        if (this.tableauChoixNavires2_[n].isSelected()) {
          g += (indiceNavire + 1)// numero de la cat�gorie
              + " ";
          indiceNavire++;
          /*if (this.choixMarees_.isSelected()) {
            g += Sinavi3TraduitHoraires
                .traduitMinutesEnHeuresMinutes((float) this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[n].attenteMareeMaxi);
          } else*/ 
          if (this.choixSecurite2_.isSelected()) {
        	  if (echelleHeures_) g += ((float) this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[n].attenteSecuMaxi)/60;
        	  else g += ((float) this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[n].attenteSecuMaxi);
          } else if (this.choixAcces2_.isSelected()) {
        	  if (echelleHeures_) g += ((float) this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[n].attenteAccesMaxi)/60;
        	  else g += ((float) this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[n].attenteAccesMaxi);
          } else if (this.choixOccupation2_.isSelected()) {
        	  if (echelleHeures_) g += ((float) this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[n].attenteOccupMaxi)/60;
        	  else g += ((float) this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[n].attenteOccupMaxi);
          } else if (this.choixPannes2_.isSelected()) {
        	  if (echelleHeures_) g += ((float) this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[n].attentePanneMaxi)/60;
        	  else g += ((float) this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[n].attentePanneMaxi);
          } else if (this.choixTotalAttente2_.isSelected()) {
        	  if (echelleHeures_) g += ((float) this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[n].attenteTotaleMaxi)/60;
        	  else g += ((float) this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[n].attenteTotaleMaxi);
          }

          g += "\n etiquette  \n \"" + this.donnees_.listeBateaux_.retournerNavire(n).nom + "\" \n"/* + */
              + "\n";
        }// fin du if le navire est selectionn�
      }// din du pour remplissage des coordonn�es
      g += "    }\n";
      g += "  }\n";
    }
    // //******************************fin histo max************************************************

    // ******************************debut histo moy************************************************
    if (this.choixMoy_.isSelected()) {
      g += "  courbe\n  {\n";
      g += "    titre \"";

      /*if (this.choixMarees_.isSelected()) {
        g += "attentes Mar�es moyenne";
      } else*/ if (this.choixSecurite2_.isSelected()) {
        g += "attentes de s�curit� moyennes";
      } else if (this.choixAcces2_.isSelected()) {
        g += "attentes d'acc�s moyennes";
      } else if (this.choixOccupation2_.isSelected()) {
        g += "attentes d'occupation moyennes";
      } else if (this.choixPannes2_.isSelected()) {
        g += "attentes d'indisponibilt� moyennes";
      } else if (this.choixTotalAttente2_.isSelected()) {
        g += "attentes totales moyennes";
      }

      /*
       * + "/" + choixString(choix, i, 1) + "/" + choixString(choix, i, 2)
       */
      g += "\"\n";
      g += "    type " + "histogramme" + "\n";
      g += "    aspect\n {\n";
      g += "contour.largeur 1 \n";
      g += "surface.couleur BB8800 \n";
      g += "texte.couleur 000000 \n";
      g += "contour.couleur 000000 \n";
      g += "    }\n";
      g += "    valeurs\n    {\n";
      /*
       * final Vector coordX= (Vector)_courbes.get(i * 2); final Vector coordY= (Vector)_courbes.get(i * 2 + 1); for
       * (int n= 0;(n < coordX.size()) || (n < coordY.size()); n++) {
       */
      indiceNavire = 0;
      for (int n = 0; n < this.donnees_.listeBateaux_.listeNavires_.size(); n++) {
        if (this.tableauChoixNavires2_[n].isSelected()) {
          g += (indiceNavire + 1)// numero de la cat�gorie
              + " ";
          indiceNavire++;
          /*if (this.choixMarees_.isSelected()) {
            g += Sinavi3TraduitHoraires
                .traduitMinutesEnHeuresMinutes((float) (this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[n].attenteMareeTotale / this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[n].nbNaviresAttenteMaree));
          } else*/ 
          if (this.choixSecurite2_.isSelected()) {
        	  if (this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[n].nbNaviresAttenteSecu == 0) g+=0;
        	  else {
        		  if (echelleHeures_) g += ((float) (this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[n].attenteSecuTotale / this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[n].nbNaviresAttenteSecu))/60;
        		  else g += ((float) (this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[n].attenteSecuTotale / this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[n].nbNaviresAttenteSecu));
        	  }
          } else if (this.choixAcces2_.isSelected()) {
        	  if (this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[n].nbNaviresAttenteAcces == 0) g+=0;
        	  else {
        		  if (echelleHeures_) g += ((float) (this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[n].attenteAccesTotale / this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[n].nbNaviresAttenteAcces))/60;
        		  else g += ((float) (this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[n].attenteAccesTotale / this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[n].nbNaviresAttenteAcces));
        	  }
          } else if (this.choixOccupation2_.isSelected()) {
        	  if (this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[n].nbNaviresAtenteOccup == 0) g+=0;
        	  else {
        		  if (echelleHeures_) g += ((float) (this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[n].attenteOccupTotale / this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[n].nbNaviresAtenteOccup))/60;
        		  else g += ((float) (this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[n].attenteOccupTotale / this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[n].nbNaviresAtenteOccup));
        	  }
          } else if (this.choixPannes2_.isSelected()) {
        	  if (this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[n].nbNaviresAttentePanne == 0) g+=0;
        	  else {
        		  if (echelleHeures_) g += ((float) (this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[n].attentePanneTotale / this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[n].nbNaviresAttentePanne))/60;
        		  else g += ((float) (this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[n].attentePanneTotale / this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[n].nbNaviresAttentePanne));
        	  }
          } else if (this.choixTotalAttente2_.isSelected()) {
        	  if (this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[n].nbNaviresAttenteTotale == 0) g+=0;
        	  else {
        		  if (echelleHeures_) g += ((float) (this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[n].attenteMegaTotale / this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[n].nbNaviresAttenteTotale))/60;
        		  else g += ((float) (this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[n].attenteMegaTotale / this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[n].nbNaviresAttenteTotale));
        	  }
          }

          g += "\n";
        }
      }// din du pour remplissage des coordonn�es
      g += "    }\n";
      g += "  }\n";
    }
    // //******************************fin histo moy************************************************

    // ******************************debut histo min************************************************
    if (this.choixMin_.isSelected()) {
      g += "  courbe\n  {\n";
      g += "    titre \"";
      /*if (this.choixMarees_.isSelected()) {
        g += "attentes Mar�es mini";
      } else*/ if (this.choixSecurite2_.isSelected()) {
        g += "attentes de s�curit� minimales";
      } else if (this.choixAcces2_.isSelected()) {
        g += "attentes d'acc�s minimales";
      } else if (this.choixOccupation2_.isSelected()) {
        g += "attentes d'occupations minimales";
      } else if (this.choixPannes2_.isSelected()) {
        g += "attentes d'indisponibilit� minimales";
      } else if (this.choixTotalAttente2_.isSelected()) {
        g += "attentes totales minimales";
      }

      g += "\"\n";
      g += "    type " + "histogramme" + "\n";
      g += "    aspect\n {\n";
      g += "contour.largeur 1 \n";
      g += "surface.couleur BBCC00 \n";
      g += "texte.couleur 000000 \n";
      g += "contour.couleur 000000 \n";
      g += "    }\n";
      g += "    valeurs\n    {\n";
      /*
       * final Vector coordX= (Vector)_courbes.get(i * 2); final Vector coordY= (Vector)_courbes.get(i * 2 + 1); for
       * (int n= 0;(n < coordX.size()) || (n < coordY.size()); n++) {
       */
      indiceNavire = 0;
      for (int n = 0; n < this.donnees_.listeBateaux_.listeNavires_.size(); n++) {
        if (this.tableauChoixNavires2_[n].isSelected()) {
          g += (indiceNavire + 1)// numero de la cat�gorie
              + " ";
          indiceNavire++;

          /*if (this.choixMarees_.isSelected()) {
            g += Sinavi3TraduitHoraires
                .traduitMinutesEnHeuresMinutes((float) this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[n].attenteMareeMini);
          } else*/ 
          if (this.choixSecurite2_.isSelected()) {
              if (echelleHeures_) g += ((float) this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[n].attenteSecuMini)/60;
              else g += ((float) this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[n].attenteSecuMini);
            } else if (this.choixAcces2_.isSelected()) {
          	if (echelleHeures_) g += ((float) this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[n].attenteAccesMini)/60;
          	else g += ((float) this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[n].attenteAccesMini);
            } else if (this.choixOccupation2_.isSelected()) {
          	  if (echelleHeures_) g += ((float) this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[n].attenteOccupMini)/60;
          	  else g += ((float) this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[n].attenteOccupMini);
            } else if (this.choixPannes2_.isSelected()) {
          	  if (echelleHeures_) g += ((float) this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[n].attentePanneMini)/60;
          	  else g += ((float) this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[n].attentePanneMini);
            } else if (this.choixTotalAttente2_.isSelected()) {
          	  if (echelleHeures_) g += ((float) this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[n].attenteTotaleMini)/60;
          	  else g += ((float) this.donnees_.params_.ResultatsCompletsSimulation.AttentesTousElementsToutesCategories[ELEMENTCHOISI].tableauAttenteCategories[n].attenteTotaleMini);
            }

            g += "\n";

        }
      }// din du pour remplissage des coordonn�es
      g += "    }\n";
      g += "  }\n";
    }
    // //******************************fin histo min************************************************

    if (seuil_) {
      /**
       * declaration d'un seuil
       */
      g += " contrainte\n";
      g += "{\n";
      // a mettre le seuil
      g += "titre \"seuil \"\n";
      // str+="orientation horizontal \n";
      g += " type max\n";
      //g += " valeur " + Sinavi3TraduitHoraires.traduitHeuresMinutesEnHeures(valeurSeuil) + CtuluLibString.LINE_SEP_SIMPLE;// /la valeur ordonn�e du seuil
      if(echelleHeures_==true) g += " valeur " + Sinavi3TraduitHoraires.traduitHeuresMinutesEnHeures(valeurSeuil) + CtuluLibString.LINE_SEP_SIMPLE;// /la valeur ordonn�e du seuil
      else g += " valeur " + Sinavi3TraduitHoraires.traduitHeuresMinutesEnMinutes(valeurSeuil) + CtuluLibString.LINE_SEP_SIMPLE;

      g += " \n }\n";
      // }//fin du for
    }

    return g;
  }

  /**
   * Listener principal des elements de la frame: tres important pour les composant du panel de choix des �l�ments
   * 
   * @param ev evenements qui apelle cette fonction
   */

  public void actionPerformed(final ActionEvent ev) {
    final Object source = ev.getSource();

    // action commune a tous les �v�nements: redimensionnement de la fenetre
    final Dimension actuelDim = this.getSize();
    final Point pos = this.getLocation();
    if (source == this.choixTotalAttente_ || source == this.choixNbNavires_ || source == this.choixAcces_
        /*|| source == this.choixMarees_*/ || source == this.choixSecurite_ || source == this.choixOccupation_
        || source == this.choixPannes_)
     /* if(source==this.lancerOptions_) */{
      // clic sur un checkBox:
      // construction de la colonne des titres

      if (/*!this.choixMarees_.isSelected() &&*/ !this.choixSecurite_.isSelected() && !this.choixAcces_.isSelected()
          && !this.choixOccupation_.isSelected() && !this.choixPannes_.isSelected()
          && !this.choixTotalAttente_.isSelected()) {
        this.choixTotalAttente_.setSelected(true);
      }

      int compteurColonnes = 0;
      if (this.choixAcces_.isSelected()) {
        compteurColonnes += 4;
      }
      /*if (this.choixMarees_.isSelected()) {
        compteurColonnes += 4;
      }*/
      if (this.choixSecurite_.isSelected()) {
        compteurColonnes += 4;
      }
      if (this.choixNbNavires_.isSelected()) {
        compteurColonnes++;
      }
      if (this.choixTotalAttente_.isSelected()) {
        compteurColonnes += 4;
      }
      if (this.choixOccupation_.isSelected()) {
        compteurColonnes += 4;
      }
      if (this.choixPannes_.isSelected()) {
        compteurColonnes += 4;
      }

      // String
      // titreTableau_[]={"Cat�gorie","Nb.bat.flotte","Total.Mar�e","MoyTotal.Mar�e","NbbatAtt.Mar�e","MoyAtt.Mar�e","Total.Secu","MoyTotal.Secu","NbbatAtt.Secu","MoyAtt.Secu","Total.Acces","MoyTotal.Acces","NbbatAtt.Acces","MoyAtt.Acces","Total.Occup","MoyTotal.Occup","NbbatAtt.Occup","MoyAtt.Occup","Total.Pannes","MoyTotal.Pannes","NbbatAtt.Pannes","MoyAtt.Pannes","Total.Global","MoyTotal.Global","NbbatAtt.Global","MoyAtt.Global"};

      this.titreTableau_ = new String[compteurColonnes + 1];
      int indiceColonne = 0;
      this.titreTableau_[indiceColonne++] = "Cat�gorie";
      if (this.choixNbNavires_.isSelected()) {
        this.titreTableau_[indiceColonne++] = "Nombre de bateaux";
      }

      
      if (this.choixSecurite_.isSelected()) {
        this.titreTableau_[indiceColonne++] = "Att. s�curit�: total";
        this.titreTableau_[indiceColonne++] = "Att. s�curit�: moy./flotte";
        this.titreTableau_[indiceColonne++] = "Att. s�curit�: nb. bateaux";
        this.titreTableau_[indiceColonne++] = "Att. s�curit�: moyenne";
      }
      if (this.choixAcces_.isSelected()) {
        this.titreTableau_[indiceColonne++] = "Att. acc�s: total";
        this.titreTableau_[indiceColonne++] = "Att. acc�s: moy./flotte";
        this.titreTableau_[indiceColonne++] = "Att. acc�s: nb. bateaux";
        this.titreTableau_[indiceColonne++] = "Att. acc�s: moyenne";
      }
      if (this.choixOccupation_.isSelected()) {
        this.titreTableau_[indiceColonne++] = "Att. occup.: total";
        this.titreTableau_[indiceColonne++] = "Att. occup.: moy./flotte";
        this.titreTableau_[indiceColonne++] = "Att. occup.: nb. bateaux";
        this.titreTableau_[indiceColonne++] = "Att. occup.: moyenne";
      }
      if (this.choixPannes_.isSelected()) {
        this.titreTableau_[indiceColonne++] = "Att. indisp.: total";
        this.titreTableau_[indiceColonne++] = "Att. indisp.: moy./flotte";
        this.titreTableau_[indiceColonne++] = "Att. indisp.: nb. bateaux";
        this.titreTableau_[indiceColonne++] = "Att. indisp.: moyenne";
      }
      if (this.choixTotalAttente_.isSelected()) {
        this.titreTableau_[indiceColonne++] = "Attente totale";
        this.titreTableau_[indiceColonne++] = "Attente moyenne sur flotte";
        this.titreTableau_[indiceColonne++] = "Nb. bateaux ayant attendu";
        this.titreTableau_[indiceColonne++] = "Attente moyenne";
      }
      // mise a jour via appel a la fonction affichage avec l'onglet selectionn� des cat�gories
      affichageTableau(this.ListeNavires_.getSelectedIndex() - 1);
      // mise a jour de l'histogramme
      final String descriptionHisto = this.affichageHistogramme();
      this.histo_.setFluxDonnees(new ByteArrayInputStream(descriptionHisto.getBytes()));
      // mise a jour des courbes
      final String descriptionCourbes = this.affichageGraphe();
      this.graphe_.setFluxDonnees(new ByteArrayInputStream(descriptionCourbes.getBytes()));

    } else if (source == this.lancerRecherche_) {
      /*
       * //verification des horaires saisis: if(this.horaireDeb_.getText().equals("")){ new
       * BuDialogError(donnees_.application_.getApp(),donnees_.application_.isSipor_, "Erreur!! horaire de depart
       * manquant!!") .activate(); } else if(this.horaireFin_.getText().equals("")){ new
       * BuDialogError(donnees_.application_.getApp(),donnees_.application_.isSipor_, "Erreur!! horaire d'arriv�e
       * manquant!!") .activate(); } else {
       */
      // lancement des calculs pour les dur�es de parcours:
      Sinavi3AlgorithmeAttentesGenerales.calcul(donnees_, this.Sens_.getSelectedIndex());
      // SiporCalculDureesParcours.calcul(donnees_,(String)this.ListeElementDepart_.getSelectedItem(),(String)this.ListeElementArrivee_.getSelectedItem(),Float.parseFloat(this.horaireDeb_.getText()),Float.parseFloat(this.horaireFin_.getText()));
      // mise a jour des affichages:
      affichageTableau(this.ListeNavires_.getSelectedIndex() - 1);
      // mise a jour de l'histogramme
      final String descriptionHisto = this.affichageHistogramme();
      this.histo_.setFluxDonnees(new ByteArrayInputStream(descriptionHisto.getBytes()));
      // mise a jour des courbes
      final String descriptionCourbes = this.affichageGraphe();
      this.graphe_.setFluxDonnees(new ByteArrayInputStream(descriptionCourbes.getBytes()));
    } else if (source == this.choixTotalAttente2_ || source == this.choixAcces2_
        || source == this.choixSecurite2_ || source == this.choixOccupation2_ || source == this.choixPannes2_) {
      // mise a jour de l'histogramme
      final String descriptionHisto = this.affichageHistogramme();
      this.histo_.setFluxDonnees(new ByteArrayInputStream(descriptionHisto.getBytes()));
      // mise a jour des courbes
      final String descriptionCourbes = this.affichageGraphe();
      this.graphe_.setFluxDonnees(new ByteArrayInputStream(descriptionCourbes.getBytes()));

      // }
    } else if (source == this.valideSeuil_ || source == this.choixMax_ || source == this.choixMoy_
        || source == this.choixMin_) {
      if (this.valideSeuil_.isSelected() && !this.valSeuil_.getText().equals("")) {
        // booleen passe a true
        this.seuil_ = true;
        // on recupere al valeure du seuil choisie par l utilisateur
        valeurSeuil = Float.parseFloat(this.valSeuil_.getText());
        // on redesssinne l histogramme en tenant compte du seuil de l utilisateur
        final String descriptionGraphe = this.affichageGraphe();
        final String descriptionHisto = this.affichageHistogramme();
        this.graphe_.setFluxDonnees(new ByteArrayInputStream(descriptionGraphe.getBytes()));
        this.histo_.setFluxDonnees(new ByteArrayInputStream(descriptionHisto.getBytes()));
      } else {
        // booleen passe a false
        this.seuil_ = false;
        // on redesssinne l histogramme en tenant compte du seuil de l utilisateur
        final String descriptionGraphe = this.affichageGraphe();
        final String descriptionHisto = this.affichageHistogramme();
        this.graphe_.setFluxDonnees(new ByteArrayInputStream(descriptionGraphe.getBytes()));
        this.histo_.setFluxDonnees(new ByteArrayInputStream(descriptionHisto.getBytes()));
      }
    }

    // si la source provient d un navire du tableau de checkBox
    boolean trouve = false;
    for (int k = 0; k < this.tableauChoixNavires_.length && !trouve; k++) {
      if (source == this.tableauChoixNavires_[k]) {
        trouve = true;
        affichageTableau(-1);
      }
      else if (source == this.tableauChoixNavires2_[k]) {
    	  trouve = true;
        // mise a jour de l'histogramme
        final String descriptionHisto = this.affichageHistogramme();
        this.histo_.setFluxDonnees(new ByteArrayInputStream(descriptionHisto.getBytes()));
        // mise a jour des courbes
        final String descriptionCourbes = this.affichageGraphe();
        this.graphe_.setFluxDonnees(new ByteArrayInputStream(descriptionCourbes.getBytes()));

      }
    }

    // on redimensionne la fenetre comme elle etais avant manipulation des elements graphique
    this.setSize(actuelDim);
    this.setLocation(pos);
  }// fin de actionPerformed

  /**
   * Methode qui s active lorsque l'on quitte l'application
   */
  protected void windowClosed() {
    // verif.stop(); //stop n est aps sur on le modifie donc par une autre variable

    // desactivation du thread
    // dureeVieThread=false;

    System.out.print("Fin de la fenetre de gestion des cercles!!");
    dispose();
  }
}
