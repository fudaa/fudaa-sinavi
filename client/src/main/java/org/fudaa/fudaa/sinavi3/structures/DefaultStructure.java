package org.fudaa.fudaa.sinavi3.structures;

import org.fudaa.ebli.network.simulationNetwork.NetworkUserObject;
import org.fudaa.fudaa.sinavi3.Sinavi3Bief;
import org.fudaa.fudaa.sinavi3.Sinavi3Ecluse;

/**
 * Structure by default used in Sinavi.
 * @author Adrien Hadoux
 *
 */
public class DefaultStructure {

	public static String DEFAULT_GARE ="G";
	public static String DEFAULT_TRONCON ="TR";
	public static String DEFAULT_ECLUSE ="E";
	
	public static String getDefaultName(final NetworkUserObject object, int nbObjects) {
		String res ="G00";
		if(object == null) {
			res = DEFAULT_GARE;
		} else
		if(object instanceof Sinavi3Ecluse){
			res =DEFAULT_ECLUSE;
		} else if(object instanceof Sinavi3Bief){
			res =DEFAULT_TRONCON;
		} 
		if(nbObjects<=0){
			res+="00";
		}else if(nbObjects<10){
			res+="0"+nbObjects;
		}else {
			res+= nbObjects;
		}
		
		
		return res;
	}
	
	
	
	
}
