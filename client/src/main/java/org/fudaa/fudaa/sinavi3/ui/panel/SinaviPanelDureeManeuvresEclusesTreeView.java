package org.fudaa.fudaa.sinavi3.ui.panel;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.MouseEvent;
import java.util.Enumeration;
import java.util.EventObject;
import java.util.List;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellEditor;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;

import org.fudaa.fudaa.ressource.FudaaResource;
import org.fudaa.fudaa.sinavi3.Sinavi3Bateau;
import org.fudaa.fudaa.sinavi3.Sinavi3Bordures;
import org.fudaa.fudaa.sinavi3.Sinavi3DataSimulation;
import org.fudaa.fudaa.sinavi3.Sinavi3Ecluse;
import org.fudaa.fudaa.sinavi3.Sinavi3Implementation;
import org.fudaa.fudaa.sinavi3.Sinavi3InternalFrame;
import org.fudaa.fudaa.sinavi3.Sinavi3TextField;
import org.fudaa.fudaa.sinavi3.Sinavi3TextFieldFloat;
import org.fudaa.fudaa.sinavi3.structures.DureeManeuvreEcluse.DureeParcours;

import com.memoire.bu.BuButton;
import com.memoire.bu.BuDialogMessage;


/**
 * Panel d'affichage du tree des dur�es de maneuvres.
 * @author Adrien
 *
 */
public class SinaviPanelDureeManeuvresEclusesTreeView extends Sinavi3InternalFrame{


	JTree treeview =new JTree();
	final Sinavi3DataSimulation data ;
	public SinaviPanelDureeManeuvresEclusesTreeView(final Sinavi3DataSimulation data) {
		this.data = data;
		setTitle("Duree de maneuvre des bateaux dans les ecluses en entrant (en Min.sec) ");
		setSize(600, 500);
	    setBorder(Sinavi3Bordures.compound_);
	    getContentPane().setLayout(new BorderLayout());
	    
		buildTree(data);
		
		//-- add button --//
		
		JPanel buttonPanel = new JPanel();
		
		final BuButton valider = new BuButton("Valider",FudaaResource.FUDAA.getIcon("crystal_oui"));
		final BuButton expand = new BuButton( "Etendre l'arbre",FudaaResource.FUDAA.getIcon("crystal_ajouter"));
		
		buttonPanel.add(expand);
		buttonPanel.add(valider);
		this.getContentPane().add(buttonPanel,BorderLayout.SOUTH);
		valider.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				new BuDialogMessage(data.getApplication().getApp(), Sinavi3Implementation.isSinavi_,
			              "Les donn�es ont �t� correctement saisies.").activate();
				
			}
		});
		
		expand.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				TreeNode root = (TreeNode) treeview.getModel().getRoot();
				
				if(treeview.isExpanded(1)) {
					expandAll(treeview,  new TreePath(root), false);
					expand.setIcon(FudaaResource.FUDAA.getIcon("crystal_ajouter"));
				}
				else { 
					expandAll(treeview,  new TreePath(root), true);
					expand.setIcon(FudaaResource.FUDAA.getIcon("crystal_enlever"));
				}
			}
		});
		
		
	}
	
	private void expandAll(JTree tree, TreePath parent, boolean expand) {

		TreeNode node = (TreeNode) parent.getLastPathComponent();

		if (node.getChildCount() >= 0) {

			for (@SuppressWarnings("unchecked")

			Enumeration<TreeNode> e = node.children(); e.hasMoreElements();) {

				TreeNode treeNode = (TreeNode) e.nextElement();

				TreePath path = parent.pathByAddingChild(treeNode);

				expandAll(tree, path, expand);

			}

		}

		// Expansion or collapse must be done bottom-up

		if (expand) {

			tree.expandPath(parent);

		} else {

			tree.collapsePath(parent);

		}

	}

	
	public void buildTree(Sinavi3DataSimulation data) {
		DefaultMutableTreeNode parent = new DefaultMutableTreeNode("Dur�es de manoeuvres des �cluses");
		
		
		for(Sinavi3Ecluse ecluse: data.getListeEcluse().getListeEcluses()) {
			DefaultMutableTreeNode nodeEcluse = new DefaultMutableTreeNode(ecluse);
			parent.add(nodeEcluse);
			List<DureeParcours> durees = data.getDureeManeuvresEcluses().getListeDureesParcours().get(ecluse);
			if(durees != null) {
				for(DureeParcours duree: durees) {
					DefaultMutableTreeNode nodeBateau = new DefaultMutableTreeNode(duree.bateau);
					nodeEcluse.add(nodeBateau);
					nodeBateau.add(new DefaultMutableTreeNode(duree));
				}
		
			}
		}
		treeview =new JTree(parent);
		DureeManeuvreTreeRenderer renderer = new DureeManeuvreTreeRenderer();
		treeview.setCellRenderer(renderer);
		treeview.setEditable(true);
		treeview.setCellEditor(new DureeManeuvreTreeCellEditor(treeview, renderer));
		getContentPane().removeAll();
		getContentPane().add(new JScrollPane(treeview), BorderLayout.CENTER);
		getContentPane().revalidate();
		this.revalidate();
	}
	

	
	public static class DureeManeuvreTreeCellEditor extends DefaultTreeCellEditor {

		JLabel labelEntrant = new JLabel("Maneuvre entrant");
		JLabel labelEntrant2 = new JLabel("Seconde maneuvre entrant");
		JLabel labelSortant = new JLabel("Maneuvre sortant");
		JLabel labelSortant2 = new JLabel("Seconde maneuvre sortant");
		
		
		Sinavi3TextFieldFloat entrant1 = new Sinavi3TextFieldFloat();
		Sinavi3TextFieldFloat entrant2 = new Sinavi3TextFieldFloat();
		Sinavi3TextFieldFloat sortant1 = new Sinavi3TextFieldFloat();
		Sinavi3TextFieldFloat sortant2 = new Sinavi3TextFieldFloat();
		JPanel grid = null;
		
		public JComponent buildFormComponent( DureeParcours duree) {
			
			if(grid == null) {
			
				grid = new JPanel(new GridLayout(2,2));
				
				JPanel flow = new JPanel(new FlowLayout(FlowLayout.RIGHT));
				flow.add(labelEntrant);
				flow.add(entrant1);
				flow.add( new JLabel("Min"));
				grid.add(flow);
				
				flow = new JPanel(new FlowLayout(FlowLayout.RIGHT));
				flow.add(labelEntrant2);
				flow.add(entrant2);
				flow.add( new JLabel("Min"));
				grid.add(flow);				
				
				flow = new JPanel(new FlowLayout(FlowLayout.RIGHT));
				flow.add(labelSortant);
				flow.add(sortant1);
				flow.add( new JLabel("Min"));
				grid.add(flow);
				
				
				
				flow = new JPanel(new FlowLayout(FlowLayout.RIGHT));
				flow.add(labelSortant2);
				flow.add(sortant2);
				flow.add( new JLabel("Min"));
				
				grid.add(flow);	
			}
			if(duree != null) {
				entrant1.setText("" + duree.dureeEntrant1);
				entrant2.setText("" + duree.dureeEntrant2);
				sortant1.setText("" + duree.dureeSortant1);
				sortant2.setText("" + duree.dureeSortant2);
				
				removeListeners();
				entrant1.addFocusListener(getFocusAdapter(duree,entrant1));
				entrant2.addFocusListener(getFocusAdapter(duree,entrant2));
				sortant1.addFocusListener(getFocusAdapter(duree,sortant1));
				sortant2.addFocusListener(getFocusAdapter(duree,sortant2));
			}
			grid.setEnabled(true);
			entrant2.setEnabled(true);
			entrant2.setEditable(true);
			
			entrant1.setEnabled(true);
			entrant1.setEditable(true);
			
			sortant1.setEnabled(true);
			sortant1.setEditable(true);
			
			sortant2.setEnabled(true);
			sortant2.setEditable(true);
			
			return grid;
		}
		
		
		public void removeListeners() {
			for(FocusListener k:entrant1.getFocusListeners())
				if(k != entrant1.getSinaviAdapter())
					entrant1.removeFocusListener(k);
			for(FocusListener k:entrant2.getFocusListeners())
				if(k != entrant2.getSinaviAdapter())
					entrant2.removeFocusListener(k);
			for(FocusListener k:sortant1.getFocusListeners())
				if(k != sortant1.getSinaviAdapter())
					sortant1.removeFocusListener(k);
			for(FocusListener k:sortant2.getFocusListeners())
				if(k != sortant2.getSinaviAdapter())
					sortant2.removeFocusListener(k);
			
		}
		
		public FocusAdapter getFocusAdapter( final DureeParcours duree, final JTextField field) {
			
			FocusAdapter res = new FocusAdapter() {
				Color previousBackground = Color.white;
				public void focusGained(FocusEvent ke) {
					field.selectAll();
					previousBackground = field.getBackground();
					field.setBackground(Sinavi3Implementation.bleuSinavi);
					field.setForeground(Color.white);
					//((JTextField)ke.getSource()).setForeground(Color.gray);
				}
				public void focusLost(FocusEvent ke){
					 System.out.println("Edition de la duree du bateau " + duree.bateau.getNom() + " de source " + ke.getSource());
					 
					 
					 field.setBackground(previousBackground);
					 field.setForeground(Sinavi3Implementation.bleuSinavi);
					 
		            	if(ke.getSource() == entrant1) {
		            		duree.dureeEntrant1 = Double.parseDouble(entrant1.getText());
		            	}
		            	else 
		            		if(ke.getSource() == entrant1) {
			            		duree.dureeEntrant1 = Double.parseDouble(entrant1.getText());
			            	} else 
			            		if(ke.getSource() == entrant2) {
				            		duree.dureeEntrant2 = Double.parseDouble(entrant2.getText());
				            	}
			            		else if(ke.getSource() == sortant1) {
				            		duree.dureeSortant1 = Double.parseDouble(sortant1.getText());
				            	}else if(ke.getSource() == sortant2) {
				            		duree.dureeSortant2 = Double.parseDouble(sortant2.getText());
				            	}
				}
				};
				
			return res;
		}
		/*
		public  KeyAdapter getAdapter( final DureeParcours duree)
	    {
			KeyAdapter res = new KeyAdapter(){
	        public void keyPressed(KeyEvent ke) {
	            if(!(ke.getKeyChar()==27||ke.getKeyChar()==65535)) {
	            	 System.out.println("Edition de la duree du bateau " + duree.bateau.getNom() + " de source " + ke.getSource());
	            	if(ke.getSource() == entrant1) {
	            		duree.dureeEntrant1 = Double.parseDouble(entrant1.getText());
	            	}
	            	else 
	            		if(ke.getSource() == entrant1) {
		            		duree.dureeEntrant1 = Double.parseDouble(entrant1.getText());
		            	} else 
		            		if(ke.getSource() == entrant2) {
			            		duree.dureeEntrant2 = Double.parseDouble(entrant2.getText());
			            	}
		            		else if(ke.getSource() == sortant1) {
			            		duree.dureeSortant1 = Double.parseDouble(sortant1.getText());
			            	}else if(ke.getSource() == sortant2) {
			            		duree.dureeSortant2 = Double.parseDouble(sortant2.getText());
			            	}
	               
	            }
	        }
			};
			
			return res;
	    }
		*/
		
		public DureeManeuvreTreeCellEditor(JTree tree,
				DureeManeuvreTreeRenderer renderer) {
			super(tree, renderer);
			
		}
		
		 public boolean isCellEditable(EventObject event) {
			
			 //TreePath path = ((JTree) event.getSource()).getSelectionPath();
			 if(event instanceof MouseEvent)
	            {
	                TreePath treepath = tree.getPathForLocation(((MouseEvent)event).getX(), 
	                		((MouseEvent)event).getY());
	                DefaultMutableTreeNode node = (DefaultMutableTreeNode) treepath.getLastPathComponent();
	                return node.isLeaf();
	            }   
			 return false;
		 }

		@Override
		public Component getTreeCellEditorComponent(JTree tree, Object value,
				boolean isSelected, boolean expanded, boolean leaf, int row) {
			
			if(leaf) {
				if (value instanceof DefaultMutableTreeNode && ((DefaultMutableTreeNode)value).getUserObject() instanceof DureeParcours) {
					
					
					//-- return composant graphique de saisie --//
					return buildFormComponent((DureeParcours) ((DefaultMutableTreeNode)value).getUserObject() );
				} else	
				return buildFormComponent(null);
			}
			
			return super.getTreeCellEditorComponent(tree, value, isSelected, expanded,
					leaf, row);
		}
		
	}
	
/**
 * Renderer du tree.
 * @author Adrien
 *
 */
	public static class DureeManeuvreTreeRenderer extends DefaultTreeCellRenderer {
		
		
		JLabel labelEntrant = new JLabel("Maneuvre entrant");
		JLabel labelEntrant2 = new JLabel("Seconde maneuvre entrant");
		JLabel labelSortant = new JLabel("Maneuvre sortant");
		JLabel labelSortant2 = new JLabel("Seconde maneuvre sortant");
		
		
		Sinavi3TextFieldFloat entrant1 = new Sinavi3TextFieldFloat();
		Sinavi3TextFieldFloat entrant2 = new Sinavi3TextFieldFloat();
		Sinavi3TextFieldFloat sortant1 = new Sinavi3TextFieldFloat();
		Sinavi3TextFieldFloat sortant2 = new Sinavi3TextFieldFloat();
		JPanel grid = null;
		
		public JComponent buildFormComponent(final DureeParcours duree) {
			
			if(grid == null) {
			
				grid = new JPanel(new GridLayout(2,2));
				
				JPanel flow = new JPanel(new FlowLayout(FlowLayout.RIGHT));
				flow.add(labelEntrant);
				flow.add(entrant1);
				flow.add( new JLabel("Min"));
				grid.add(flow);
				
				flow = new JPanel(new FlowLayout(FlowLayout.RIGHT));
				flow.add(labelEntrant2);
				flow.add(entrant2);
				flow.add( new JLabel("Min"));
				grid.add(flow);				
				
				flow = new JPanel(new FlowLayout(FlowLayout.RIGHT));
				flow.add(labelSortant);
				flow.add(sortant1);
				flow.add( new JLabel("Min"));
				grid.add(flow);
				
				
				
				flow = new JPanel(new FlowLayout(FlowLayout.RIGHT));
				flow.add(labelSortant2);
				flow.add(sortant2);
				flow.add( new JLabel("Min"));
				grid.add(flow);	
			}
			entrant1.setText("" + duree.dureeEntrant1);
			entrant2.setText("" + duree.dureeEntrant2);
			sortant1.setText("" + duree.dureeSortant1);
			sortant2.setText("" + duree.dureeSortant2);
			grid.setEnabled(true);
			entrant2.setEnabled(true);
			entrant2.setEditable(true);
			
			entrant1.setEnabled(true);
			entrant1.setEditable(true);
			
			sortant1.setEnabled(true);
			sortant1.setEditable(true);
			
			sortant2.setEnabled(true);
			sortant2.setEditable(true);
			
			return grid;
		}
		
		public Component getTreeCellRendererComponent(
				JTree tree,
				Object value,
				boolean sel,
				boolean expanded,
				boolean leaf,
				int row,
				boolean hasFocus) {

			super.getTreeCellRendererComponent(
					tree, value, sel,
					expanded, leaf, row,
					hasFocus);
			if (leaf && value instanceof DefaultMutableTreeNode && ((DefaultMutableTreeNode)value).getUserObject() instanceof DureeParcours) {
			
				
				//-- return composant graphique de saisie --//
				return buildFormComponent((DureeParcours) ((DefaultMutableTreeNode)value).getUserObject() );
				
			} else {
				
				if(value instanceof  DefaultMutableTreeNode && ((DefaultMutableTreeNode)value).getUserObject() instanceof Sinavi3Ecluse) {
					this.setText(((Sinavi3Ecluse)((DefaultMutableTreeNode)value).getUserObject()).getName());
				} else 
					if(value instanceof  DefaultMutableTreeNode && ((DefaultMutableTreeNode)value).getUserObject() instanceof Sinavi3Bateau) {
						this.setText(((Sinavi3Bateau)((DefaultMutableTreeNode)value).getUserObject()).getNom());
					}
			} 

			return this;
		}
	}

	

}
