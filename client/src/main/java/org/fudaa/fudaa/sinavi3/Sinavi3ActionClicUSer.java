package org.fudaa.fudaa.sinavi3;

import java.util.ArrayList;

import org.fudaa.dodico.corba.sinavi3.SParametresGrapheTopologies;



/**
 * Classe qui contient l'ensemble des parametres pour une iteration du panel de dessin
 *@version $Version$
 * @author hadoux
 *
 */
public class Sinavi3ActionClicUSer {

	SParametresGrapheTopologies graphe;
	
	int compteur;
	
	ArrayList tableauGare_ ;

	
	int nbTotalGares_=0;
	/**
	 * constructeur de la classe.
	 * @param graphe
	 * @param compteur
	 * @param tableauGare_
	 */
	public Sinavi3ActionClicUSer(SParametresGrapheTopologies graphe, int compteur, ArrayList tableauGare_,int nbTotalGares) {
		
		this.graphe = graphe;
		this.compteur = compteur;
		this.tableauGare_ = tableauGare_;
		this.nbTotalGares_=nbTotalGares;
	}
	
	 
	
}
