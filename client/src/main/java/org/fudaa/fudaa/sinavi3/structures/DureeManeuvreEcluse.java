package org.fudaa.fudaa.sinavi3.structures;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.fudaa.fudaa.sinavi3.Sinavi3Bateau;
import org.fudaa.fudaa.sinavi3.Sinavi3DataSimulation;
import org.fudaa.fudaa.sinavi3.Sinavi3Ecluse;

/**
 * L'ensemble des dur�es de parcours pour ecluse.
 * @author Adrien
 *
 */
public class DureeManeuvreEcluse {

	public Map<Sinavi3Ecluse, List<DureeParcours>> listeDureesParcours = new HashMap<Sinavi3Ecluse, List<DureeParcours>>();
	
	/**
	 * Add a new ecluse.
	 * @param ecluse
	 * @param bateau
	 * @param dureeEntrant1
	 * @param dureeEntrant2
	 * @param dureeSortant1
	 * @param dureeSortant2
	 */
	public void addEcluse(Sinavi3Ecluse ecluse, Sinavi3DataSimulation data ) {
		List<DureeParcours> durees = new ArrayList<DureeManeuvreEcluse.DureeParcours>();
		for(Sinavi3Bateau bateau: data.getListeBateaux_().getListeNavires_()) {
			durees.add(new DureeParcours(ecluse.getDureeManoeuvreEntrant_(),  ecluse.getDureeManoeuvreEntrant2_(),
					ecluse.getDureeManoeuvreSortant_(),  ecluse.getDureeManoeuvreSortant2_(), bateau));
		}
		listeDureesParcours.put(ecluse, durees);
		
	}
	
	public void modifyEcluse(Sinavi3Ecluse ecluse) {
		List<DureeParcours> res = listeDureesParcours.get(ecluse);
		if(res != null) {
			for(DureeParcours dp: res) {
				dp.dureeEntrant1 = ecluse.getDureeManoeuvreEntrant_();
				dp.dureeEntrant2 = ecluse.getDureeManoeuvreEntrant2_();
				dp.dureeSortant1 = ecluse.getDureeManoeuvreSortant_();
				dp.dureeSortant2 = ecluse.getDureeManoeuvreSortant2_();
			}
		}
	}
	
	
	/**
	 * add a new ship.
	 * @param bateau
	 */
	public void addBateau(Sinavi3Bateau bateau ) {
		for(Sinavi3Ecluse ecluse: listeDureesParcours.keySet()) {
			List<DureeParcours> durees = listeDureesParcours.get(ecluse);
			durees.add(new DureeParcours(ecluse.getDureeManoeuvreEntrant_(),  ecluse.getDureeManoeuvreEntrant2_(), 
					ecluse.getDureeManoeuvreSortant_(),  ecluse.getDureeManoeuvreSortant2_(), bateau));
		}
		
	}
	
	public void deleteBateau(Sinavi3Bateau bateau) {
		for(Sinavi3Ecluse ecluse: listeDureesParcours.keySet()) {
			List<DureeParcours> durees = listeDureesParcours.get(ecluse);
			List<DureeParcours> dureesToDelete = new ArrayList<DureeManeuvreEcluse.DureeParcours>();
			for(DureeParcours d: durees) {
				if(d.bateau == bateau) {
					dureesToDelete.add(d);
				}
			}
			durees.removeAll(dureesToDelete);
		}
	}
	
	public void deleteEcluse(Sinavi3Ecluse ecluse) {
		listeDureesParcours.remove(ecluse);
		
	}
	
	public DureeParcours getDureeParcours(int iecluse, int ibateau, Sinavi3DataSimulation d) {	
		Sinavi3Bateau bateau = d.getListeBateaux_().retournerNavire(ibateau);
		Sinavi3Ecluse ecluse = d.getListeEcluse().retournerEcluse(iecluse);
		return getDureeParcours(ecluse, bateau);
	}
	
	
	public DureeParcours getDureeParcours(Sinavi3Ecluse ecluse, Sinavi3Bateau bateau) {		
		if(listeDureesParcours.containsKey(ecluse)) {
			List<DureeParcours> durees = listeDureesParcours.get(ecluse);
			for(DureeParcours res: durees) {
				if(res.bateau == bateau)
					return res;
			}
		}
		
		return null;
	}
	
	
	public void initDataFromCorba(Sinavi3DataSimulation data,  double matriceDurManeuvreEclEntrant[][],
			double matriceDurManeuvreEclSortant[][], double matriceDurManeuvreEclEntrant2[][],
			double matriceDurManeuvreEclSortant2[][]) {
		int cptEcluses=0;
		for(Sinavi3Ecluse ecluse: data.getListeEcluse_().getListeEcluses()) {
			addEcluse(ecluse, data);
			//-- modify with the value inside the matrix --//
			for(int i=0;i<data.getListeBateaux_().NombreNavires(); i++) {
				DureeParcours duree = getDureeParcours(ecluse, data.getListeBateaux_().retournerNavire(i));
				if(duree != null) {
					duree.dureeEntrant1 = matriceDurManeuvreEclEntrant[cptEcluses][i];
					duree.dureeEntrant2 = matriceDurManeuvreEclEntrant2[cptEcluses][i];
					duree.dureeSortant1 = matriceDurManeuvreEclSortant[cptEcluses][i];
					duree.dureeSortant2 = matriceDurManeuvreEclSortant2[cptEcluses][i];
					
					
				}
			}
			
			cptEcluses++;
		}
	}
	
	

	/**
	 * Classe repr�sentant une dur�e de parcours.
	 * @author Adrien
	 *
	 */
	public static class DureeParcours {
		
		public DureeParcours(double dureeEntrant1, double dureeEntrant2,
				double dureeSortant1, double dureeSortant2, Sinavi3Bateau bateau) {
			super();
			this.dureeEntrant1 = dureeEntrant1;
			this.dureeEntrant2 = dureeEntrant2;
			this.dureeSortant1 = dureeSortant1;
			this.dureeSortant2 = dureeSortant2;			
			this.bateau = bateau;
		}
		public double dureeEntrant1, dureeEntrant2, dureeSortant1, dureeSortant2;
		
		public Sinavi3Bateau bateau;
		
		
		
		
	}






	public Map<Sinavi3Ecluse, List<DureeParcours>> getListeDureesParcours() {
		return listeDureesParcours;
	}

	public void setListeDureesParcours(
			Map<Sinavi3Ecluse, List<DureeParcours>> listeDureesParcours) {
		this.listeDureesParcours = listeDureesParcours;
	}


	
	
}
