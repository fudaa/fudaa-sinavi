/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fudaa.fudaa.sinavi3.ui.results.reports;

import java.util.ArrayList;
import java.util.List;
import jxl.format.Colour;
import jxl.write.Formula;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import org.fudaa.fudaa.commun.projet.FudaaProjet;
import org.fudaa.fudaa.sinavi3.Sinavi3DataSimulation;
import org.fudaa.fudaa.sinavi3.Sinavi3ResultatsDureesParcours;
import org.fudaa.fudaa.sinavi3.Sinavi3TraduitHoraires;
import static org.fudaa.fudaa.sinavi3.ui.results.reports.Sinavi3SheetFactoryHelper.integerFormat;
import static org.fudaa.fudaa.sinavi3.ui.results.reports.Sinavi3SheetFactoryHelper.setValueDateAt;

/**
 *
 * @author Adrien Hadoux
 */
public class Sinavi3ReportDureeParcours extends Sinavi3SheetFactoryHelper{
    

    public static void produceSheetDureeParcourValues(Sinavi3DataSimulation data, WritableSheet sheet, int indiceLigne, int tronconE, int tronconS, int typeE, int typeS) throws WriteException {
        Object[] results = null;
        //--sens avalant --//
        results = Sinavi3ResultatsDureesParcours.computeDataAverage(data, tronconE, typeE, tronconS, typeS, 0);
        int flotte1 = getIntValue(results, 1);
        double dureeMin = Sinavi3TraduitHoraires.traduitReverseMinutes(results[2].toString());   
        double dureeMoy = Sinavi3TraduitHoraires.traduitReverseMinutes(results[3].toString());   
        double dureeMax = Sinavi3TraduitHoraires.traduitReverseMinutes(results[4].toString());   
        
        setValueDateAt(indiceLigne, 5, sheet, results[1].toString());
        setValueDateAt(indiceLigne, 6, sheet, results[2].toString());
        setValueDateAt(indiceLigne, 7, sheet, results[3].toString());
        setValueDateAt(indiceLigne, 8, sheet, results[4].toString());

        
         
        
        //-- sens montant --//
        results = Sinavi3ResultatsDureesParcours.computeDataAverage(data, tronconE, typeE, tronconS, typeS, 1);
        int flotte2 = getIntValue(results, 1);
        double dureeMin2 = Sinavi3TraduitHoraires.traduitReverseMinutes(results[2].toString());   
        double dureeMoy2 = Sinavi3TraduitHoraires.traduitReverseMinutes(results[3].toString());   
        double dureeMax2 = Sinavi3TraduitHoraires.traduitReverseMinutes(results[4].toString());   
        setValueDateAt(indiceLigne, 10, sheet, results[1].toString());
        setValueDateAt(indiceLigne, 11, sheet, results[2].toString());
        setValueDateAt(indiceLigne, 12, sheet, results[3].toString());
        setValueDateAt(indiceLigne, 13, sheet, results[4].toString());

        
        // moyenne 
       
        
        setValueAt(indiceLigne, 15, sheet, (flotte2+flotte1)/2);
        setValueDateAt(indiceLigne, 16, sheet, Sinavi3TraduitHoraires.traduitMinutesEnHeuresMinutes3((dureeMin2+dureeMin)/2.0));
        setValueDateAt(indiceLigne, 17, sheet, Sinavi3TraduitHoraires.traduitMinutesEnHeuresMinutes3((dureeMoy+dureeMoy2)/2.0));
        setValueDateAt(indiceLigne, 18, sheet, Sinavi3TraduitHoraires.traduitMinutesEnHeuresMinutes3((dureeMax2+dureeMax)/2.0));
        
        //--  formula excel --//
        /*
        Formula f = new Formula(15, indiceLigne, "MOYENNE(F" + (indiceLigne + 1) + ",K" + (indiceLigne + 1) + ")");
        f.setCellFormat(integerFormat);
        sheet.addCell(f);
        f = new Formula(16, indiceLigne, "MIN(G" + (indiceLigne + 1) + ",L" + (indiceLigne + 1) + ")");
        f.setCellFormat(dateFormat);
        sheet.addCell(f);
        // sheet.addCell( new Formula(17, indiceLigne, "MOYENNE(H"+(indiceLigne+1)+",M"+(indiceLigne+1)+")"));

        f = new Formula(17, indiceLigne, "MOYENNE(H" + (indiceLigne + 1) + ",M" + (indiceLigne + 1) + ")");
        f.setCellFormat(dateFormat);
        sheet.addCell(f);

        f = new Formula(18, indiceLigne, "MAX(I" + (indiceLigne + 1) + ",N" + (indiceLigne + 1) + ")");
        f.setCellFormat(dateFormat);
        sheet.addCell(f);
*/
    }

    public static void produceSheetDureeParcoursHeader(WritableSheet sheet, boolean multiSimulation, Sinavi3DataSimulation data) throws WriteException {
        // taille de la colonne
        sheet.setColumnView(0, 30);
        sheet.setColumnView(1, 5);
        
        sheet.setColumnView(4, 5);
        sheet.setColumnView(2, 20);
        sheet.setColumnView(3, 20);
        sheet.setRowView(6, 32*20);
        sheet.setRowView(1, 38*20);
        setValueTitleAt(0, 0, sheet, "Dur�es de parcours", Colour.WHITE);
        if (!multiSimulation) {
            setValueTitleAt(1, 0, sheet, " R�sultats d'un seul sc�nario de simulation", Colour.WHITE);
            setValueTitleAt(5, 0, sheet, "Sc�nario", Colour.TURQOISE2);
            fusionneCells(5, 0, 2, false, sheet);
        } else {
            setValueTitleAt(1, 0, sheet, "R�sultats de plusieurs sc�narios de simulation", Colour.WHITE);
            setValueTitleAt(5, 0, sheet, "Sc�nario", Colour.TURQOISE2);
            int cpt = 7;
            for (FudaaProjet projet : data.getApplication().getSimulations()) {
                if(data.getProjet_() != projet)
                    setValueTitleAt(cpt++, 0, sheet, "Sc�nario " + projet.getInformations().titre, Colour.TURQOISE2);

            }

        }

        setValueTitleAt(5, 2, sheet, "Trajet", Colour.LIGHT_GREEN);
        fusionneCells(5, 2, 2, true, sheet);
        setValueTitleAt(6, 2, sheet, "Entr�e �l�ment 1", Colour.GREEN);
        setValueTitleAt(6, 3, sheet, "Sortie �l�ment 2", Colour.GREEN);

        setValueTitleAt(5, 5, sheet, "Avalants", Colour.LIGHT_ORANGE);
        fusionneCells(5, 5, 4, true, sheet);
        setValueTitleAt(6, 5, sheet, "Flotte");
        setValueTitleAt(6, 6, sheet, "Dur�e Mini");
        setValueTitleAt(6, 7, sheet, "Dur�e Moy");
        setValueTitleAt(6, 8, sheet, "Dur�e Maxi");

        setValueTitleAt(5, 10, sheet, "Montants", Colour.LIGHT_ORANGE);
        fusionneCells(5, 10, 4, true, sheet);
        setValueTitleAt(6, 10, sheet, "Flotte");
        setValueTitleAt(6, 11, sheet, "Dur�e Mini");
        setValueTitleAt(6, 12, sheet, "Dur�e Moy");
        setValueTitleAt(6, 13, sheet, "Dur�e Maxi");

        setValueTitleAt(5, 15, sheet, "Moyenne / 2 sens", Colour.LIGHT_ORANGE);
        fusionneCells(5, 15, 4, true, sheet);
        setValueTitleAt(6, 15, sheet, "Flotte");
        setValueTitleAt(6, 16, sheet, "Dur�e Mini");
        setValueTitleAt(6, 17, sheet, "Dur�e Moy");
        setValueTitleAt(6, 18, sheet, "Dur�e Maxi");
    }

    public static WritableSheet produceSheetDureeParcours(final WritableWorkbook classeur, 
            final Sinavi3DataSimulation data, boolean multiSimulation, List<Sinavi3SheetProducer.TrajetReport> listeTrajets) throws WriteException {
        WritableSheet sheet = classeur.createSheet("Dur�es de parcours", 0);

        produceSheetDureeParcoursHeader(sheet, multiSimulation, data);
        List<Sinavi3DataSimulation> listeComparison = new ArrayList<Sinavi3DataSimulation>();
        if (multiSimulation) {
            for (FudaaProjet projet : data.getApplication().getSimulations()) {
                if (data.getProjet_() != projet) {
                    Sinavi3DataSimulation comparison = getSimulationFromFudaaProjet(projet);
                    listeComparison.add(comparison);
                }
            }
        }

        //-- calculs avalant--//
        int indiceLigne = 8;
        if(listeTrajets != null) {
            for (Sinavi3SheetProducer.TrajetReport trajet: listeTrajets) {
                setValueAt(indiceLigne, 2, sheet, trajet.etypeElement==0?data.getListeBief_().retournerBief(trajet.eelement).getName(): data.getListeEcluse().retournerEcluse(trajet.eelement).getName());
                setValueAt(indiceLigne, 3, sheet, trajet.stypeElement==0?data.getListeBief_().retournerBief(trajet.selement).getName(): data.getListeEcluse().retournerEcluse(trajet.selement).getName());
                
                produceSheetDureeParcourValues(data, sheet, indiceLigne, trajet.eelement,trajet.selement,trajet.etypeElement, trajet.stypeElement);
                
                if (multiSimulation) {
                    fusionneCells(indiceLigne, 2, (listeComparison.size()+1)*2 -1, false, sheet);
                    fusionneCells(indiceLigne, 3, (listeComparison.size()+1)*2 -1, false, sheet);
                    setValueTitleAt(indiceLigne, 0, sheet, "Sc�nario de r�f�rence", Colour.WHITE);
                    indiceLigne+=2;
                    for (Sinavi3DataSimulation comparison : listeComparison) {
                        setValueTitleAt(indiceLigne, 0, sheet, "Sc�nario " + comparison.getProjectName_(), Colour.WHITE);
                        produceSheetDureeParcourValues(comparison, sheet, indiceLigne, trajet.eelement, trajet.selement,trajet.etypeElement, trajet.stypeElement);
                        indiceLigne+=2;
                    }
                }else 
                    indiceLigne+=2;
              }
            
        }else {
        for (int tronconE = 0; tronconE < data.getListeBief_().getListeBiefs_().size(); tronconE++) {
            for (int tronconS = 0; tronconS < data.getListeBief_().getListeBiefs_().size(); tronconS++) {
                setValueAt(indiceLigne, 2, sheet, data.getListeBief_().retournerBief(tronconE).getName());
                setValueAt(indiceLigne, 3, sheet, data.getListeBief_().retournerBief(tronconS).getName());

                produceSheetDureeParcourValues(data, sheet, indiceLigne, tronconE, tronconS, 0, 0);
                
                if (multiSimulation) {
                    fusionneCells(indiceLigne, 2, (listeComparison.size()+1)*2 -1, false, sheet);
                    fusionneCells(indiceLigne, 3, (listeComparison.size()+1)*2 -1, false, sheet);
                    setValueTitleAt(indiceLigne, 0, sheet, "Sc�nario de r�f�rence", Colour.WHITE);
                    indiceLigne+=2;
                    for (Sinavi3DataSimulation comparison : listeComparison) {
                        setValueTitleAt(indiceLigne, 0, sheet, "Sc�nario " + comparison.getProjectName_(), Colour.WHITE);
                        produceSheetDureeParcourValues(comparison, sheet, indiceLigne, tronconE, tronconS, 0, 0);
                        indiceLigne+=2;
                    }
                }else 
                    indiceLigne+=2;

            }
            for (int tronconS = 0; tronconS < data.getListeEcluse().getListeEcluses().size(); tronconS++) {
                setValueAt(indiceLigne, 2, sheet, data.getListeBief_().retournerBief(tronconE).getName());
                setValueAt(indiceLigne, 3, sheet, data.getListeBief_().retournerBief(tronconS).getName());

                produceSheetDureeParcourValues(data, sheet, indiceLigne, tronconE, tronconS, 0, 1);
              
                if (multiSimulation) {
                    fusionneCells(indiceLigne, 2,(listeComparison.size()+1)*2 -1, false, sheet);
                    fusionneCells(indiceLigne, 3, (listeComparison.size()+1)*2 -1, false, sheet);
                    setValueTitleAt(indiceLigne, 0, sheet, "Sc�nario de r�f�rence", Colour.WHITE);
                    indiceLigne+=2;
                    for (Sinavi3DataSimulation comparison : listeComparison) {
                        setValueTitleAt(indiceLigne, 0, sheet, "Sc�nario " + comparison.getProjectName_(), Colour.WHITE);
                        produceSheetDureeParcourValues(comparison, sheet, indiceLigne, tronconE, tronconS, 0, 0);
                        indiceLigne+=2;
                    }
                }else 
                    indiceLigne+=2;
            }
        }

        for (int tronconE = 0; tronconE < data.getListeEcluse().getListeEcluses().size(); tronconE++) {
            for (int tronconS = 0; tronconS < data.getListeBief_().getListeBiefs_().size(); tronconS++) {
                setValueAt(indiceLigne, 2, sheet, data.getListeBief_().retournerBief(tronconE).getName());
                setValueAt(indiceLigne, 3, sheet, data.getListeBief_().retournerBief(tronconS).getName());

                produceSheetDureeParcourValues(data, sheet, indiceLigne, tronconE, tronconS, 1, 0);
            

                if (multiSimulation) {
                    fusionneCells(indiceLigne, 2, (listeComparison.size()+1)*2 -1, false, sheet);
                    fusionneCells(indiceLigne, 3, (listeComparison.size()+1)*2 -1, false, sheet);
                    setValueTitleAt(indiceLigne, 0, sheet, "Sc�nario de r�f�rence", Colour.WHITE);
                    indiceLigne+=2;
                    for (Sinavi3DataSimulation comparison : listeComparison) {
                        setValueTitleAt(indiceLigne, 0, sheet, "Sc�nario " + comparison.getProjectName_(), Colour.WHITE);
                        produceSheetDureeParcourValues(comparison, sheet, indiceLigne, tronconE, tronconS, 0, 0);
                        indiceLigne+=2;
                    }
                }else 
                    indiceLigne+=2;
            }
            for (int tronconS = 0; tronconS < data.getListeEcluse().getListeEcluses().size(); tronconS++) {
                setValueAt(indiceLigne, 2, sheet, data.getListeBief_().retournerBief(tronconE).getName());
                setValueAt(indiceLigne, 3, sheet, data.getListeBief_().retournerBief(tronconS).getName());

                produceSheetDureeParcourValues(data, sheet, indiceLigne, tronconE, tronconS, 1, 1);
               
                if (multiSimulation) {
                    fusionneCells(indiceLigne, 2,(listeComparison.size()+1)*2 -1, false, sheet);
                    fusionneCells(indiceLigne, 3, (listeComparison.size()+1)*2 -1, false, sheet);
                    setValueTitleAt(indiceLigne, 0, sheet, "Sc�nario de r�f�rence", Colour.WHITE);
                    indiceLigne+=2;
                    for (Sinavi3DataSimulation comparison : listeComparison) {
                        setValueTitleAt(indiceLigne, 0, sheet, "Sc�nario " + comparison.getProjectName_(), Colour.WHITE);
                        produceSheetDureeParcourValues(comparison, sheet, indiceLigne, tronconE, tronconS, 0, 0);
                        indiceLigne+=2;
                    }
                }else 
                    indiceLigne+=2;
            }
        }
        
        }
        if (!multiSimulation) {
            setValueTitleAt(7, 0, sheet, "Sc�nario de r�f�rence", Colour.WHITE);
            fusionneCells(7, 0, indiceLigne - 8, false, sheet);
        }
        return sheet;
    }

    
}
