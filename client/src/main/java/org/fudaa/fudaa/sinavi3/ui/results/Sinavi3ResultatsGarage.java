
package org.fudaa.fudaa.sinavi3.ui.results;

import org.fudaa.fudaa.sinavi3.ui.results.reports.Sinavi3SheetProducer;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;
import javax.swing.table.TableColumn;

import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.image.CtuluImageExport;
import org.fudaa.dodico.corba.sinavi3.Sinavi3ResultatConsoEau;
import org.fudaa.dodico.corba.sinavi3.Sinavi3ResultatGarage;
import org.fudaa.ebli.graphe.BGraphe;
import org.fudaa.fudaa.ressource.FudaaResource;
import org.fudaa.fudaa.sinavi3.FonctionsSimu;
import org.fudaa.fudaa.sinavi3.Sinavi3AlgorithmeAttentesGenerales;
import org.fudaa.fudaa.sinavi3.Sinavi3Bateau;
import org.fudaa.fudaa.sinavi3.Sinavi3Bordures;
import org.fudaa.fudaa.sinavi3.Sinavi3DataSimulation;
import org.fudaa.fudaa.sinavi3.Sinavi3GestionResultats;
import org.fudaa.fudaa.sinavi3.Sinavi3InternalFrame;
import org.fudaa.fudaa.sinavi3.Sinavi3JFreeChartCamembert;
import org.fudaa.fudaa.sinavi3.Sinavi3TextFieldInteger;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;

import com.lowagie.text.Font;
import com.memoire.bu.BuButton;
import com.memoire.bu.BuGridLayout;
import com.memoire.bu.BuPanel;
import com.memoire.bu.BuScrollPane;
import com.memoire.bu.BuTabbedPane;
import com.memoire.bu.BuTable;
import com.mxgraph.io.graphml.mxGraphMlKey.keyForValues;

/**
 * classe de gestion des resultats ds garages aupr�s des ecluses.
 * 
 * @version $Version$
 * @author Adrien Hadoux
 */
public class Sinavi3ResultatsGarage extends Sinavi3InternalFrame {

  /**
	 * 
	 */
	private static final long serialVersionUID = 1L;


 
 
  
  /**
   * panels principaux de la fenetre.
   */
  BuPanel panelPrincipal_ = new BuPanel();
  BuPanel panelPrincipalClassique_ = new BuPanel();
  
  /**
   * Panel de selection des preferences.
   */
 
  BuPanel selectionPanel1_;
 

  
  /**
   * Panel des options: type affichages, colonnes � faire figurer.
   */
  BuPanel optionPanelTableau_ = new BuPanel(new BorderLayout());
   
 
  
  
  
  String[] titreTableau_ = null;
  
  BuTabbedPane panelPrincipalAffichage_ = new BuTabbedPane();
  
  /**
   * Panel contenant le bouton quitter
   */
  BuPanel panelQuitter_ = new BuPanel();

  
    
  /**
   * Panel cniotenant le tableau et les boutns de controles
   */
  BuPanel panelGestionTableau_ = new BuPanel();
  /*
   * panel de gestion du tableau et de ses options
   */
  BuPanel panelTableau_ = new BuPanel();
  
  /**
   * panel d'affichage du tableau
   */
  BuPanel panelTableauAffichage_ = new BuPanel();

  /**
   * panel de gestion des boutons
   */
  BuPanel controlPanel_ = new BuPanel();
  
 

 
  /**
   * combolist qui permet de selectionenr les lignes du tableau a etre affich�es:
   */
 
  JComboBox<String> listeEcluses_=new JComboBox<String>();
 
  /**
   * buoton de generation des resultats
   */
  private final BuButton exportationExcel_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_generer"), "Exporter");
  final BuButton exportationGraphe_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_generer"), "Exporter");
 
  private final BuButton quitter_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_quitter"), "Quitter");
  private final BuButton lancerRecherche_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_oui"), "Rechercher");
  private final BuButton rafraichirSeuil_ = new BuButton(FudaaResource.FUDAA.getIcon("crystal_rafraichir"), "");
  
  Border raisedBevel_ = BorderFactory.createRaisedBevelBorder();
  Border loweredBevel_ = BorderFactory.createLoweredBevelBorder();
  Border compound_ = BorderFactory.createCompoundBorder(raisedBevel_, loweredBevel_);
  Border bordnormal_ = BorderFactory.createEtchedBorder();
  /**
   * donnees de la simulation.
   */
  Sinavi3DataSimulation donnees_;

  /**
   * Constructeur de la sous fenetre de gestion des resultats.
   */
  public Sinavi3ResultatsGarage(final Sinavi3DataSimulation _donnees) {
    super("Lin�eaire garage �cluses", true, true, true, true);
    // recuperation des donn�es de la simulation
    donnees_ = _donnees;
    
    titreTableau_ = new String[4 + donnees_.getListeBateaux_().NombreNavires()];
    int cpt = 0;
//    titreTableau_[cpt++] = "Ecluse";
//    titreTableau_[cpt++] = "Gare";
    titreTableau_[cpt++] = "Nb bateaux en attente";
    for(Sinavi3Bateau bat: donnees_.getListeBateaux_().getListeNavires_()){
    	 titreTableau_[cpt++] = bat.getNom();
    }
    titreTableau_[cpt++] = "Lin�eaire total (m)";
    titreTableau_[cpt++] = "Fr�qiuence de retour";
    titreTableau_[cpt++] = "Fr�quence cumul�e";
    
    	
    setSize(860, 600);
    setBorder(Sinavi3Bordures.compound_);
    this.getContentPane().setLayout(new /* BorderLayout() */GridLayout(1, 1));
    this.panelPrincipal_.setLayout(new BorderLayout());
    this.getContentPane().add(this.panelPrincipal_/* ,BorderLayout.CENTER */);
    this.panelPrincipal_.add(this.panelPrincipalAffichage_, BorderLayout.CENTER);
    panelPrincipalAffichage_.addTab("R�sultats de synth�se ", this.panelPrincipalClassique_);
    
    this.panelPrincipal_.add(panelQuitter_, BorderLayout.SOUTH);
    panelQuitter_.add(quitter_);
    
    this.panelPrincipalClassique_.setLayout(new BorderLayout());
    this.panelPrincipalClassique_.add(this.optionPanelTableau_, BorderLayout.WEST);
    this.panelPrincipalClassique_.add(panelTableau_, BorderLayout.CENTER);
      
    /*******************************************************************************************************************
     * gestion du panel de selection
  	 */

    
    tableauChoixEcluses_ = new JCheckBox[this.donnees_.getListeEcluse().getListeEcluses().size()];
    for (int i = 0; i < this.donnees_.getListeEcluse().getListeEcluses().size(); i++) {
      this.tableauChoixEcluses_[i] = new JCheckBox(this.donnees_.getListeEcluse().retournerEcluse(i).getNom(), true);
      this.tableauChoixEcluses_[i].addActionListener(this);
    }
    

 
    
    /*******************************************************************************************************************
     * gestion du panel des options pour les r�sultats de synth�se
     ******************************************************************************************************************/

    BuGridLayout lo=new BuGridLayout(1,5,0,true,false,false,false);
    final BuPanel panoption = new BuPanel(lo);
    final BuScrollPane panoptionGestionScroll = new BuScrollPane(panoption);
    this.optionPanelTableau_.add(panoptionGestionScroll);

    
    final Box bVert2 = Box.createVerticalBox();
    bVert2.setBorder(this.bordnormal_);
    for (int i = 0; i < this.tableauChoixEcluses_.length; i++) {
      bVert2.add(this.tableauChoixEcluses_[i]);
    }
    final TitledBorder bordure2 = BorderFactory.createTitledBorder(BorderFactory
        .createEtchedBorder(EtchedBorder.LOWERED), "Ecluses");
    bVert2.setBorder(bordure2);
    panoption.add(bVert2);
     
    
     this.panelTableau_.setLayout(new BorderLayout());
   // this.panelTableau_.add(this.optionPanelTableau_, BorderLayout.WEST);
    this.panelTableau_.add(this.panelGestionTableau_, BorderLayout.CENTER);
    
    /*******************************************************************************************************************
     * gestion du panel tableau panelGestionTableau_
     ******************************************************************************************************************/

    // etape 1: architecture du panel panelGestionTableau_ et du panel ascenseur qui le contiendra
    panelGestionTableau_.setLayout(new BorderLayout());
    final JScrollPane asc = new JScrollPane(this.panelTableauAffichage_);

    // ajout au centre du panel qui contiendra le tableau d affichage
    this.panelGestionTableau_.add(asc, BorderLayout.CENTER);

    // panel qui contient les differents boutons
    this.controlPanel_.add(exportationExcel_);
    this.panelGestionTableau_.add(this.controlPanel_, BorderLayout.SOUTH);
    
    final Border bordureTab = BorderFactory.createEtchedBorder();
    controlPanel_.setBorder(bordureTab);
    
  
    
    // etape 2: remplissage du comboliste avec les noms des ecluses
    this.listeEcluses_.addItem("Tous");
    for (int i = 0; i < donnees_.getListeEcluse().getListeEcluses().size(); i++) {
      this.listeEcluses_.addItem("" + donnees_.getListeEcluse().retournerEcluse(i).getNom());
      this.listeEcluses_.addItem("" + donnees_.getListeEcluse().retournerEcluse(i).getNom());
    }

    // etape 3: gestion de l affichage du tableau de donn�es
    affichageTableau(-1);

  
    
    // bouton qui permet de generer le contenu du tableau en ficheir excel:
    this.exportationExcel_.setToolTipText("Exporte le contenu du tableau au format xls");
    exportationExcel_.addActionListener(new ActionListener() {
    	public void actionPerformed(final ActionEvent _e) {
    		File fichier;
            final JFileChooser fc = new JFileChooser();
            final int returnVal = fc.showSaveDialog(Sinavi3ResultatsGarage.this);
            if (returnVal == JFileChooser.APPROVE_OPTION) {
            	fichier = fc.getSelectedFile();
            	final File f = CtuluLibFile.appendExtensionIfNeeded(fichier, "xls");
            	Map<KeyGarage, Object[][]> mapData = computeStructTableau(donnees_.getListeGarages());
            	List<KeyGarage> keys = sortKeys(mapData.keySet());
            	Sinavi3SheetProducer.produceGarageEcluse(f, donnees_, titreTableau_, mapData, keys);
            }
      	 // Sinavi3GestionResultats.exportTableau(Sinavi3ResultatsGarage.this, titreTableau_, data_);
        }
    });

    
   
   
    
    /** listener des boutons quitter */
    this.quitter_.setToolTipText("Ferme la sous-fen�tre");

    final ActionListener actionQuitter = new ActionListener() {
      public void actionPerformed(ActionEvent _e) {
        Sinavi3ResultatsGarage.this.windowClosed();
      }
    };
    
    this.quitter_.addActionListener(actionQuitter);
    
  }

  JCheckBox[] tableauChoixEcluses_;
  public String format(double value) {
	  String result = "";
	  DecimalFormat format = new DecimalFormat("##.##");
	  result = format.format(value);
	  return result;
  }
  
  public String formatPercentage(double value) {
	 
	  return format(value *100.0);
  }
  
  
  public static class KeyGarage {
	  public int ecluse;
	  public int gare;
  }
  
  
  public Map<KeyGarage, Object[][]> computeStructTableau(final List<Sinavi3ResultatGarage> listeGarages) {
	  Map<KeyGarage, Object[][]> result = new HashMap<Sinavi3ResultatsGarage.KeyGarage, Object[][]>();
	  
	  
	  int previousGare =-1;
	  int previousEcluse = -1;
	  List<Object[]> structLignes = null;
	  
	  for(Sinavi3ResultatGarage garage: this.donnees_.getListeGarages()) {
	    	
	    		
	    		if(garage.ecluse != previousEcluse || garage.gare != previousGare){
	    			if(structLignes != null) {
	    				KeyGarage keyGarage = new KeyGarage();
	    				keyGarage.ecluse = previousEcluse;
	    				keyGarage.gare = previousGare;
	    				Object[][] innerTab = new Object[structLignes.size()][titreTableau_.length];
	    				int cpt = 0;
	    				for(Object[] lineTab:structLignes ) {
	    					innerTab[cpt++] = lineTab;
	    				}
	    				result.put(keyGarage, innerTab);
	    			}
	    			structLignes = new ArrayList<Object[]>();
	    			previousEcluse = garage.ecluse;
	    			previousGare =  garage.gare;
	    		}
	    		Object[] lineTab = new Object[titreTableau_.length];
	    		
	    	//lineTab[0] = this.donnees_.getListeEcluse().retournerEcluse(garage.ecluse).getNom();
	    	//lineTab[1] = this.donnees_.getListeGare_().retournerGare(garage.gare);
	    	lineTab[0] =garage.nbBateauxAttente;
	    	if( garage.listBateaux != null &&  garage.listBateaux.length == this.donnees_.getListeBateaux_().NombreNavires())
		    	for(int i=0;i< this.donnees_.getListeBateaux_().NombreNavires();i++) {
		    		lineTab[1+i] = garage.listBateaux[i].nbBateaux;
		    	}
	    	lineTab[1+this.donnees_.getListeBateaux_().NombreNavires()] = format(garage.lineaireTotal);
	    	lineTab[2+this.donnees_.getListeBateaux_().NombreNavires()] = formatPercentage(garage.frequenceRetour) + "%";
	    	lineTab[3+this.donnees_.getListeBateaux_().NombreNavires()] = formatPercentage(garage.frequenceCumulee) +"%";
	    	
	    	structLignes.add(lineTab);
	    		
	    	}
	    
	  
	  return result;
  }
  
  private List<KeyGarage> sortKeys(Set<KeyGarage> set){
	  List<KeyGarage> result = new ArrayList<Sinavi3ResultatsGarage.KeyGarage>();
	  for(KeyGarage key:set){
		  result.add(key);
		  
	  }
	  Collections.sort(result,new Comparator<KeyGarage>() {

		@Override
		public int compare(KeyGarage o1, KeyGarage o2) {
			if(o1.ecluse == o2.ecluse && o1.gare == o2.gare)
				return 0;
			if(o1.ecluse == o2.ecluse)
				return o1.gare<o2.gare?-1:1;
			return o1.ecluse<o2.ecluse?-1:1;
		}
	});
	  
	  return result;
  }
  
  /**
   * Methode d'affichage du tableau.
   * @param indiceEcluse indice de l'�cluse.
   */
  public void affichageTableau(final int indiceEcluse) {
	  
	  Map<KeyGarage, Object[][]> structure = computeStructTableau(this.donnees_.getListeGarages());
	  List<KeyGarage> keys = sortKeys(structure.keySet());
	    
	  
	 this.panelTableauAffichage_.removeAll();
	 this.panelTableauAffichage_.setLayout(new BorderLayout());
	 Box boxTab = Box.createVerticalBox();
	 
	 
	 
	// Sinavi3SheetProducer.produceGarageEcluse("F:\\test.xls", this.donnees_, this.titreTableau_, structure,keys);
	  
    for(KeyGarage key: keys) {
    	if(tableauChoixEcluses_[key.ecluse].isSelected()){
    	 Object[][] data = structure.get(key);
    	 BuTable table= new BuTable(data, this.titreTableau_) {
    	      public boolean isCellEditable(final int _row, final int _col) {
    	        return false;
    	      }
    	    };
    	    for(int i=0; i<table.getModel().getColumnCount();i++){
    	        TableColumn column = table.getColumnModel().getColumn(i);
    	               column.setPreferredWidth(150); 
    	    } 
    	    org.fudaa.fudaa.sinavi.factory.ColumnAutoSizer.sizeColumnsToFit(table);
    	    table.revalidate();
    	    
    	    JPanel panelAffichage = new JPanel();
    	    panelAffichage.setLayout(new BorderLayout());
    	   // panelAffichage.add(table.getTableHeader(), BorderLayout.PAGE_START);
    	    JPanel panelTableau = new JPanel(new BorderLayout());
    	    panelTableau.add(table, BorderLayout.CENTER);
    	    panelTableau.add(table.getTableHeader(), BorderLayout.PAGE_START);
    	    panelAffichage.add(panelTableau, BorderLayout.CENTER);
    	    String title = ""+this.donnees_.getListeEcluse().retournerEcluse(key.ecluse).getName() + " " + this.donnees_.getListeGare_().retournerGare(key.gare);
    	    String titleHtml = "<html><body> <bold>"+this.donnees_.getListeEcluse().retournerEcluse(key.ecluse).getName() + 
    	    		"</bold><br /> <bold>" + this.donnees_.getListeGare_().retournerGare(key.gare)+"</bold></body></html>";
    	    JLabel label = new JLabel(titleHtml);
    	    label.setFont(new java.awt.Font("Serif", Font.BOLD, 16));
    	    label.setMinimumSize(new Dimension(70, 0));
    	    label.setPreferredSize(new Dimension(70, 0));
     	    panelAffichage.add(label,BorderLayout.WEST);
    	    panelAffichage.setBorder(Sinavi3Bordures.createTitledBorderLight(title));
    	    
    	    boxTab.add(panelAffichage);
    	}
    }
    this.panelTableauAffichage_.add(boxTab);
    

    // mise a jour de l'affichage
    this.revalidate();
    this.updateUI();

  }
  


  
  
  
  
  public int getNbSelectedEcluses() {
	  int cpt=0;
	  
	  for(int i=0;i<this.tableauChoixEcluses_.length;i++)
	  if (this.tableauChoixEcluses_[i].isSelected()) {
		  cpt++;
	  }
	  return cpt;
  }
  
  
  
  /**
   * Listener principal des elements de la frame: tres important pour les composant du panel de choix des �l�ments.
   * 
   * @param ev evenements qui apelle cette fonction
   */

  public void actionPerformed(final ActionEvent ev) {
    final Object source = ev.getSource();

    // action commune a tous les �v�nements: redimensionnement de la fenetre
    final Dimension actuelDim = this.getSize();
    final Point pos = this.getLocation();
   
   
    if (source == this.lancerRecherche_) {
       	// mise a jour des affichages:
    	affichageTableau(this.listeEcluses_.getSelectedIndex() - 1);
    	
    }

    // si la source provient d un navire du tableau de checkBox
    boolean trouve = false;
    for (int k = 0; k < this.tableauChoixEcluses_.length && !trouve; k++) {
    	if (source == this.tableauChoixEcluses_[k]) {
    		trouve = true;
    		affichageTableau(-1);
    		
    	}    	
    }

    // on redimensionne la fenetre comme elle etais avant manipulation des elements graphique
    this.setSize(actuelDim);
    this.setLocation(pos);
  }// fin de actionPerformed

  /**
   * Methode qui s active lorsque l'on quitte l'application.
   */
  protected void windowClosed() {
    System.out.print("Fin de la fenetre de gestion des cercles!!");
    dispose();
  }
  
  
}
