package org.fudaa.fudaa.sinavi3;

import java.util.ArrayList;

import javax.swing.JComboBox;


/**
 * Definit la structure utilis�e poru un trajet
 *@version $Version$
 * @author hadoux
 *
 */
public class Sinavi3Trajet {
	
	//-- gare de depart du trajet: correspond � l'indice dans la liste des gares --//
	public int gareDepart_=0;
	
	//-- gared 'arriv�e du trajet --//
	public 	int gareArrivee_=1;
	
	/**Sens: 0= avalant, 1=montant**/
	public int sens_= 0;
	  
	//-- loi de generation: 0=erlang 1=journaliere 2=deterministe --// 
	public  int typeLoiGenerationNavires_=0;  
	 
	 //-- indice de la loi d'erlang --//
	public int loiErlangGenerationNavire=1; 
	public double loiErlangEcartType=0;
	public double erlangDebut=0;
	public double erlangFin=24;
	//-- si loi Erlang alors 0 si ecart type =1 --//
	public int typeErlangLoi=0;
	
	//-- nb de bateaux attendus pour ce trajet
	public int nbBateauxattendus=0;
	
	
	
	/**Declaration du tableau de loi deterministe et journaliere: */
	public   ArrayList loiDeterministeOUjournaliere_ = new ArrayList();

	
	
	
	
	
	public Sinavi3Trajet(){
	
		
	}
	
	//-- methode d'acces au couple loi deterministe --//
	
	public int nbCouples(){
		return loiDeterministeOUjournaliere_.size();
	}
	
	public ArrayList copieLoiDeterministe(){
		
		ArrayList nouveau=new ArrayList();
		
		for(int tot=0;tot<loiDeterministeOUjournaliere_.size();tot++)
        {
        	       	
        		final CoupleLoiDeterministe c = new CoupleLoiDeterministe(
        				(CoupleLoiDeterministe) loiDeterministeOUjournaliere_.get(tot));
        		nouveau.add(c);

          }
        	
        
        

		
		
		return nouveau;
		
	}

	public int getGareDepart_() {
		return gareDepart_;
	}

	public void setGareDepart_(int gareDepart_) {
		this.gareDepart_ = gareDepart_;
	}

	public int getGareArrivee_() {
		return gareArrivee_;
	}

	public void setGareArrivee_(int gareArrivee_) {
		this.gareArrivee_ = gareArrivee_;
	}

	public int getSens_() {
		return sens_;
	}

	public void setSens_(int sens_) {
		this.sens_ = sens_;
	}

	public int getTypeLoiGenerationNavires_() {
		return typeLoiGenerationNavires_;
	}

	public void setTypeLoiGenerationNavires_(int typeLoiGenerationNavires_) {
		this.typeLoiGenerationNavires_ = typeLoiGenerationNavires_;
	}

	public int getLoiErlangGenerationNavire() {
		return loiErlangGenerationNavire;
	}

	public void setLoiErlangGenerationNavire(int loiErlangGenerationNavire) {
		this.loiErlangGenerationNavire = loiErlangGenerationNavire;
	}

	public double getLoiErlangEcartType() {
		return loiErlangEcartType;
	}

	public void setLoiErlangEcartType(double loiErlangEcartType) {
		this.loiErlangEcartType = loiErlangEcartType;
	}

	public double getErlangDebut() {
		return erlangDebut;
	}

	public void setErlangDebut(double erlangDebut) {
		this.erlangDebut = erlangDebut;
	}

	public double getErlangFin() {
		return erlangFin;
	}

	public void setErlangFin(double erlangFin) {
		this.erlangFin = erlangFin;
	}

	public int getNbBateauxattendus() {
		return nbBateauxattendus;
	}

	public void setNbBateauxattendus(int nbBateauxattendus) {
		this.nbBateauxattendus = nbBateauxattendus;
	}

	public ArrayList getLoiDeterministeOUjournaliere_() {
		return loiDeterministeOUjournaliere_;
	}

	public void setLoiDeterministeOUjournaliere_(
			ArrayList loiDeterministeOUjournaliere_) {
		this.loiDeterministeOUjournaliere_ = loiDeterministeOUjournaliere_;
	}

	public int getTypeErlangLoi() {
		return typeErlangLoi;
	}

	public void setTypeErlangLoi(int typeErlangLoi) {
		this.typeErlangLoi = typeErlangLoi;
	}

	
	
	
	

}
