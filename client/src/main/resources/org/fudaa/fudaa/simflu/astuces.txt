Lorsqu'une gare est � 0, elle est ind�finie.
Pour cr�er une �cluse identique � une autre, affichez les �cluses et cliquez sur modifier. Vous n'avez plus qu'� changer le nom et � ajouter l'�cluse identique � la pr�c�dente.
Pour cr�er un bief identique � un autre, affichez les biefs et cliquez sur modifier. Vous n'avez plus qu'� changer le nom et � ajouter le bief identique au pr�c�dent.
Une fois que le r�seau est assembl�e, utilisez la palette afin de modifier les donn�es rapidement.
Pour selectionner plusieurs type de bateaux dans les listes pour les graphiques, enfoncez la touche Ctrl et cliquez � l'aide du bouton gauche de la souris pour selectionner ou d�selectionner les types.
N'h�sitez pas � consulter le contr�le dans l'ajout des donn�es afin de ne pas avoir de probl�mes.






