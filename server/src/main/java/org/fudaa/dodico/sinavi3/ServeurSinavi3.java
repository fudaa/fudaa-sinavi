/*
 * @file         ServeurSipor.java
 * @creation     2000-02-16
 * @modification $Date: 2007-11-23 11:31:39 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.sinavi3;
import java.util.Date;

import org.fudaa.dodico.objet.UsineLib;
import org.fudaa.dodico.objet.CDodico;
/**
 * Une classe serveur pour Sipor.
 *
 * @version      $Revision: 1.1 $ $Date: 2007-11-23 11:31:39 $ by $Author: hadouxad $
 * @author       Guillaume Desnoix 
 */
public final class ServeurSinavi3 {
  
  private ServeurSinavi3()
  {
  }
  public static void main(final String[] _args) {
    final String nom=
      (_args.length > 0
        ? _args[0]
        : CDodico.generateName("::sipor::ICalculSipor"));
    //Cas particulier : il s'agit de creer un serveur de calcul dans une jvm donne
    //Cete M�thode n'est pas a imiter. If faut utiliser Boony pour creer des objet corba.
    CDodico.rebind(nom, UsineLib.createService(DCalculSinavi3.class));
    System.out.println("Sipor server running... ");
    System.out.println("Name: " + nom);
    System.out.println("Date: " + new Date());
  }
}
