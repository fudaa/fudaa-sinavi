/**
 * @file         DParametresSinavi3.java
 * @creation     1999-09-17
 * @modification $Date: 2008-01-25 14:02:01 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.sinavi3;

import java.io.FileWriter;
import java.io.IOException;

import org.fudaa.ctulu.CtuluLibString;

import org.fudaa.dodico.calcul.DParametres;

import org.fudaa.dodico.corba.sinavi3.*;
import org.fudaa.dodico.fortran.FortranWriter;


/**
 * Les parametres de Sinavi3.
 * 
 * @version $Revision: 1.9 $ $Date: 2008-01-25 14:02:01 $ by $Author: hadouxad $
 * @author Nicolas Chevalier , Bertrand Audinet
 */
public class DParametresSinavi3 extends DParametres implements IParametresSinavi3, IParametresSinavi3Operations {
 
  
  /**
   * Donn�es Sinavi3 2006.
   */
  private SParametresSinavi32 parametresSinavi_;
  
  
  
  
  public DParametresSinavi3() {
    super();
  }

  public final Object clone() throws CloneNotSupportedException {
    return new DParametresSinavi3();
  }

  public String toString() {
    final String s = "DParametresSinavi3";
    return s;
  }

  

  
  
  
  
  /**
   * M�thode utile qui transforme un booleen en "o" ou "n". true => "o" ; false => "n"
   * 
   * @param arg
   * @return "o" : "n"
   */
  public static String enO_N(final boolean arg) {
    final String x = (arg ? "o" : "n");
    return x;
  }

  /**
   * M�thode de transformation des minutes en heures.minutes au format string. 60 => "1.00" ; 61 => "1.01" ; 90 =>
   * "1.30";
   * 
   * @param _minutes
   */
  public static String enHM(final long _minutes) {
    final Long h = new Long(_minutes / 60);
    final Long m = new Long(_minutes % 60);
    final String m_ret = m.longValue() < 10 ? "0" + m.toString() : m.toString();
    return h.toString() + CtuluLibString.DOT + m_ret;
  }

  
  
  
  
 
 
  
  
  
  /**
   * Methode d'ecriture des donn�es des cat�gories de navires:
   * @param c
   * @param f
   * @throws IOException
   */
  
  public static void ecritDonneesCategoriesNavires(final SParametresNavires2 c, String nomFichier) throws IOException {
	   
	  //creation du fichier des cat�gories des navires:
	  final FortranWriter f = new FortranWriter(new FileWriter(nomFichier + ".categ"));
	    
	  
	 
	  
	  
	  // format du fichier : 40 caract�res de donn�es + 1 s�parateur + 100 caract�res de commentaire
	    final int[] fmt = new int[] { 40, 1, 100 };
	    // ecriture du nombre de cat�gories
	    f.stringField(0, new Integer(c.nombreNavires).toString());
	    f.stringField(2, "Nombre de cat�gories de bateaux");
	    f.writeFields(fmt);
	    // ecriture du pied de pilote commum � toutes les cat�gories
	   // f.stringField(0, new Integer(c.piedPilote).toString());
	    //f.stringField(2, "Pied de pilote");
	    //f.writeFields(fmt);
	    // ecriture de la structure d'une cat�gorie
	    for (int i = 0; i < c.nombreNavires; i++) {
	      // 		nom de la cat�gorie
	      f.stringField(0, c.listeNavires[i].nom);
	      f.stringField(2, "Nom de la cat�gorie" + (i + 1));
	      f.writeFields(fmt);
          
	      // 			Niveau de priorite
	      f.stringField(0, new Integer(c.listeNavires[i].priorite).toString());
	      f.stringField(2, "Niveau de priorite de la categorie" + (i + 1));
	      f.writeFields(fmt);
         
          //	    Longueur de la cat�gorie en modules
	      f.stringField(0, new Float(c.listeNavires[i].longueurMax).toString());
	      f.stringField(2, "Longueur en metres de la categorie" + (i + 1));
	      f.writeFields(fmt);
	      //	    largeur de la cat�gorie en modules
	      f.stringField(0, new Float(c.listeNavires[i].largeurMax).toString());
	      f.stringField(2, "largeur en metres de la categorie" + (i + 1));
	      f.writeFields(fmt);
	      // loi de variation des longueurs
	      //f.stringField(0, new Integer(c.listeNavires[i].ordreLoiVariationLongueur).toString());
	      //f.stringField(2, "Loi d'Erlang de variation des longueurs de la categorie" + (i + 1));
	      //f.writeFields(fmt);
          //	     largeur de la cat�gorie en modules
	      f.stringField(0, new Float(c.listeNavires[i].vitesseAvalant).toString());
	      f.stringField(2, "Vitesse avalant en KM/H de la categorie" + (i + 1));
	      f.writeFields(fmt);
	      //	     Longueur de la cat�gorie en modules
	      f.stringField(0, new Float(c.listeNavires[i].vitesseMontant).toString());
	      f.stringField(2, "Vitesse montant en KM/H de la categorie" + (i + 1));
	      f.writeFields(fmt);
         
          //		   loi de variation des largeurs
	      //f.stringField(0, new Integer(c.listeNavires[i].ordreLoiVariationLargeur).toString());
	      //f.stringField(2, "Loi d'Erlang de variation des largeurs de la categorie" + (i + 1));
	      //f.writeFields(fmt);
	      // Tirant d'eau en entr�e en m.cm
	      f.stringField(0, new Double(c.listeNavires[i].tirantEauEntree).toString());
	      f.stringField(2, "Tirant d'eau" + (i + 1));
	      f.writeFields(fmt);
	      // Tirant d'eau en sortie en m.cm
	      f.stringField(0, ""+c.listeNavires[i].typeTirant);
	      f.stringField(2, "Etat de chargement de la categorie" + (i + 1));
	      f.writeFields(fmt);
	      
	     
	      
	      //creneaux journaliers
	      ecritHoraireBateauSemaine(fmt, f, c, i);
	        
	      f.stringField(0, new Double(c.listeNavires[i].dureeAttenteMaxAdmissible).toString());
          f.stringField(2, "Duree attente maximale admissible de la categorie " + (i + 1));
          f.writeFields(fmt);
	      
	      }
	    
	    //fermeture du fichier de donn�es des cat�gories
	    f.flush();
	    f.close();
	    
  }
  
  
  
  public static void ecritHoraireBateauSemaine(final int[] fmt,final  FortranWriter f, final SParametresNavires2 bateau, final int indice) throws IOException {
	  f.stringField(0,new Float(bateau.listeNavires[indice].h.lundiCreneau1HeureDep).toString());
      f.stringField(2, "creneau 1 depart lundi horaire travail suivi de la categorie" + (indice + 1));
      f.writeFields(fmt);
      f.stringField(0,new Float(bateau.listeNavires[indice].h.lundiCreneau1HeureArrivee).toString());
      f.stringField(2, "creneau 1 arrivee lundi horaire travail suivi de la categorie" + (indice + 1));
      f.writeFields(fmt);
      f.stringField(0,new Float(bateau.listeNavires[indice].h.lundiCreneau2HeureDep).toString());
      f.stringField(2, "creneau 2 depart lundi horaire travail suivi de la categorie" + (indice + 1));
      f.writeFields(fmt);
      f.stringField(0,new Float(bateau.listeNavires[indice].h.lundiCreneau2HeureArrivee).toString());
      f.stringField(2, "creneau 2 arrivee lundi horaire travail suivi de la categorie" + (indice + 1));
      f.writeFields(fmt);
      f.stringField(0,new Float(bateau.listeNavires[indice].h.lundiCreneau3HeureDep).toString());
      f.stringField(2, "creneau 3 depart lundi horaire travail suivi de la categorie" + (indice + 1));
      f.writeFields(fmt);
      f.stringField(0,new Float(bateau.listeNavires[indice].h.lundiCreneau3HeureArrivee).toString());
      f.stringField(2, "creneau 3 arrivee lundi horaire travail suivi de la categorie" + (indice + 1));
      f.writeFields(fmt);
      f.stringField(0,new Float(bateau.listeNavires[indice].h.mardiCreneau1HeureDep).toString());
      f.stringField(2, "creneau 1 depart mardi horaire travail suivi de la categorie numero " + (indice + 1));
      f.writeFields(fmt);
      f.stringField(0,new Float(bateau.listeNavires[indice].h.mardiCreneau1HeureArrivee).toString());
      f.stringField(2, "creneau 1 arrivee mardi horaire travail suivi de la categorie" + (indice + 1));
      f.writeFields(fmt);
      f.stringField(0,new Float(bateau.listeNavires[indice].h.mardiCreneau2HeureDep).toString());
      f.stringField(2, "creneau 2 depart mardi horaire travail suivi de la categorie" + (indice + 1));
      f.writeFields(fmt);
      f.stringField(0,new Float(bateau.listeNavires[indice].h.mardiCreneau2HeureArrivee).toString());
      f.stringField(2, "creneau 2 arrivee mardi horaire travail suivi de la categorie" + (indice + 1));
      f.writeFields(fmt);
      f.stringField(0,new Float(bateau.listeNavires[indice].h.mardiCreneau3HeureDep).toString());
      f.stringField(2, "creneau 3 depart mardi horaire travail suivi de la categorie" + (indice + 1));
      f.writeFields(fmt);
      f.stringField(0,new Float(bateau.listeNavires[indice].h.mardiCreneau3HeureArrivee).toString());
      f.stringField(2, "creneau 3 arrivee mardi horaire travail suivi de la categorie" + (indice + 1));
      f.writeFields(fmt);
      f.stringField(0,new Float(bateau.listeNavires[indice].h.mercrediCreneau1HeureDep).toString());
      f.stringField(2, "creneau 1 depart mercredi horaire travail suivi de la categorie" + (indice + 1));
      f.writeFields(fmt);
      f.stringField(0,new Float(bateau.listeNavires[indice].h.mercrediCreneau1HeureArrivee).toString());
      f.stringField(2, "creneau 1 arrivee mercredi horaire travail suivi de la categorie" + (indice + 1));
      f.writeFields(fmt);
      f.stringField(0,new Float(bateau.listeNavires[indice].h.mercrediCreneau2HeureDep).toString());
      f.stringField(2, "creneau 2 depart mercredi horaire travail suivi de la categorie" + (indice + 1));
      f.writeFields(fmt);
      f.stringField(0,new Float(bateau.listeNavires[indice].h.mercrediCreneau2HeureArrivee).toString());
      f.stringField(2, "creneau 2 arrivee mercredi horaire travail suivi de la categorie" + (indice + 1));
      f.writeFields(fmt);
      f.stringField(0,new Float(bateau.listeNavires[indice].h.mercrediCreneau3HeureDep).toString());
      f.stringField(2, "creneau 3 depart mercredi horaire travail suivi de la categorie" + (indice + 1));
      f.writeFields(fmt);
      f.stringField(0,new Float(bateau.listeNavires[indice].h.mercrediCreneau3HeureArrivee).toString());
      f.stringField(2, "creneau 3 arrivee mercredi horaire travail suivi de la categorie" + (indice + 1));
      f.writeFields(fmt);
      f.stringField(0,new Float(bateau.listeNavires[indice].h.jeudiCreneau1HeureDep).toString());
      f.stringField(2, "creneau 1 depart jeudi horaire travail suivi de la categorie" + (indice + 1));
      f.writeFields(fmt);
      f.stringField(0,new Float(bateau.listeNavires[indice].h.jeudiCreneau1HeureArrivee).toString());
      f.stringField(2, "creneau 1 arrivee jeudi horaire travail suivi de la categorie" + (indice + 1));
      f.writeFields(fmt);
      f.stringField(0,new Float(bateau.listeNavires[indice].h.jeudiCreneau2HeureDep).toString());
      f.stringField(2, "creneau 2 depart jeudi horaire travail suivi de la categorie" + (indice + 1));
      f.writeFields(fmt);
      f.stringField(0,new Float(bateau.listeNavires[indice].h.jeudiCreneau2HeureArrivee).toString());
      f.stringField(2, "creneau 2 arrivee jeudi horaire travail suivi de la categorie" + (indice + 1));
      f.writeFields(fmt);
      f.stringField(0,new Float(bateau.listeNavires[indice].h.jeudiCreneau3HeureDep).toString());
      f.stringField(2, "creneau 3 depart jeudi horaire travail suivi de la categorie" + (indice + 1));
      f.writeFields(fmt);
      f.stringField(0,new Float(bateau.listeNavires[indice].h.jeudiCreneau3HeureArrivee).toString());
      f.stringField(2, "creneau 3 arrivee jeudi horaire travail suivi de la categorie" + (indice + 1));
      f.writeFields(fmt);
      f.stringField(0,new Float(bateau.listeNavires[indice].h.vendrediCreneau1HeureDep).toString());
      f.stringField(2, "creneau 1 depart vendredi horaire travail suivi de la categorie" + (indice + 1));
      f.writeFields(fmt);
      f.stringField(0,new Float(bateau.listeNavires[indice].h.vendrediCreneau1HeureArrivee).toString());
      f.stringField(2, "creneau 1 arrivee vendredi horaire travail suivi de la categorie" + (indice + 1));
      f.writeFields(fmt);
      f.stringField(0,new Float(bateau.listeNavires[indice].h.vendrediCreneau2HeureDep).toString());
      f.stringField(2, "creneau 2 depart vendredi horaire travail suivi de la categorie" + (indice + 1));
      f.writeFields(fmt);
      f.stringField(0,new Float(bateau.listeNavires[indice].h.vendrediCreneau2HeureArrivee).toString());
      f.stringField(2, "creneau 2 arrivee vendredi horaire travail suivi de la categorie" + (indice + 1));
      f.writeFields(fmt);
      f.stringField(0,new Float(bateau.listeNavires[indice].h.vendrediCreneau3HeureDep).toString());
      f.stringField(2, "creneau 3 depart vendredi horaire travail suivi de la categorie" + (indice + 1));
      f.writeFields(fmt);
      f.stringField(0,new Float(bateau.listeNavires[indice].h.vendrediCreneau3HeureArrivee).toString());
      f.stringField(2, "creneau 3 arrivee vendredi horaire travail suivi de la categorie" + (indice + 1));
      f.writeFields(fmt);
      f.stringField(0,new Float(bateau.listeNavires[indice].h.samediCreneau1HeureDep).toString());
      f.stringField(2, "creneau 1 depart samedi horaire travail suivi de la categorie" + (indice + 1));
      f.writeFields(fmt);
      f.stringField(0,new Float(bateau.listeNavires[indice].h.samediCreneau1HeureArrivee).toString());
      f.stringField(2, "creneau 1 arrivee samedi horaire travail suivi de la categorie" + (indice + 1));
      f.writeFields(fmt);
      f.stringField(0,new Float(bateau.listeNavires[indice].h.samediCreneau2HeureDep).toString());
      f.stringField(2, "creneau 2 depart samedi horaire travail suivi de la categorie" + (indice + 1));
      f.writeFields(fmt);
      f.stringField(0,new Float(bateau.listeNavires[indice].h.samediCreneau2HeureArrivee).toString());
      f.stringField(2, "creneau 2 arrivee samedi horaire travail suivi de la categorie" + (indice + 1));
      f.writeFields(fmt);
      f.stringField(0,new Float(bateau.listeNavires[indice].h.samediCreneau3HeureDep).toString());
      f.stringField(2, "creneau 3 depart samedi horaire travail suivi de la categorie" + (indice + 1));
      f.writeFields(fmt);
      f.stringField(0,new Float(bateau.listeNavires[indice].h.samediCreneau3HeureArrivee).toString());
      f.stringField(2, "creneau 3 arrivee samedi horaire travail suivi de la categorie" + (indice + 1));
      f.writeFields(fmt);
      f.stringField(0,new Float(bateau.listeNavires[indice].h.dimancheCreneau1HeureDep).toString());
      f.stringField(2, "creneau 1 depart dimanche horaire travail suivi de la categorie" + (indice + 1));
      f.writeFields(fmt);
      f.stringField(0,new Float(bateau.listeNavires[indice].h.dimancheCreneau1HeureArrivee).toString());
      f.stringField(2, "creneau 1 arrivee dimanche horaire travail suivi de la categorie" + (indice + 1));
      f.writeFields(fmt);
      f.stringField(0,new Float(bateau.listeNavires[indice].h.dimancheCreneau2HeureDep).toString());
      f.stringField(2, "creneau 2 depart dimanche horaire travail suivi de la categorie" + (indice + 1));
      f.writeFields(fmt);
      f.stringField(0,new Float(bateau.listeNavires[indice].h.dimancheCreneau2HeureArrivee).toString());
      f.stringField(2, "creneau 2 arrivee dimanche horaire travail suivi de la categorie" + (indice + 1));
      f.writeFields(fmt);
      f.stringField(0,new Float(bateau.listeNavires[indice].h.dimancheCreneau3HeureDep).toString());
      f.stringField(2, "creneau 3 depart dimanche horaire travail suivi de la categorie" + (indice + 1));
      f.writeFields(fmt);
      f.stringField(0,new Float(bateau.listeNavires[indice].h.dimancheCreneau3HeureArrivee).toString());
      f.stringField(2, "creneau 3 arrivee dimanche horaire travail suivi de la categorie" + (indice + 1));
      f.writeFields(fmt);
      /*
      f.stringField(0,new Float(bateau.listeNavires[indice].h.ferieCreneau1HeureDep).toString());
      f.stringField(2, "creneau 1 depart ferie horaire travail suivi de la categorie" + (indice + 1));
      f.writeFields(fmt);
      f.stringField(0,new Float(bateau.listeNavires[indice].h.ferieCreneau1HeureArrivee).toString());
      f.stringField(2, "creneau 1 arrivee ferie horaire travail suivi de la categorie" + (indice + 1));
      f.writeFields(fmt);
      f.stringField(0,new Float(bateau.listeNavires[indice].h.ferieCreneau2HeureDep).toString());
      f.stringField(2, "creneau 2 depart ferie horaire travail suivi de la categorie" + (indice + 1));
      f.writeFields(fmt);
      f.stringField(0,new Float(bateau.listeNavires[indice].h.ferieCreneau2HeureArrivee).toString());
      f.stringField(2, "creneau 2 arrivee ferie horaire travail suivi de la categorie" + (indice + 1));
      f.writeFields(fmt);
      f.stringField(0,new Float(bateau.listeNavires[indice].h.ferieCreneau3HeureDep).toString());
      f.stringField(2, "creneau 3 depart ferie horaire travail suivi de la categorie" + (indice + 1));
      f.writeFields(fmt);
      f.stringField(0,new Float(bateau.listeNavires[indice].h.ferieCreneau3HeureArrivee).toString());
      f.stringField(2, "creneau 3 arrivee ferie horaire travail suivi de la categorie" + (indice + 1));
      f.writeFields(fmt);
      */
  }
  
  
  
  /**
   * Methode d'ecriture des donn�es des donn�es g�n�rales:
   * @param c
   * @param f
   * @throws IOException
   */
  
  public static void ecritDonneesGenerales(final SParametresDonneesGenerales2 dg, String nomFichier) throws IOException {
	   
	  //creation du fichier des cat�gories des navires:
	  final FortranWriter f = new FortranWriter(new FileWriter(nomFichier + ".general"));

//	 format du fichier : 40 caract�res de donn�es + 1 s�parateur + 100 caract�res de commentaire
	    final int[] fmt = new int[] { 40, 1, 100 };
	    // ecriture du nombre de jours de la simulation
	    f.stringField(0, new Integer(dg.nombreJours).toString());
	    f.stringField(2, "Nombre de jours de la simulation");
	    f.writeFields(fmt);
//	  ecriture de la graine de la simulation
	    f.stringField(0, new Integer(dg.graine).toString());
	    f.stringField(2, "graine d'initialisation de la simulation");
	    f.writeFields(fmt);
	    
/*	    
//	  ecriture du nombre du jour de depart de la simulation
	    if(dg.jourDepart==1)f.stringField(0, "lundi");
	    else
		    if(dg.jourDepart==2)f.stringField(0, "mardi");
		    else
			    if(dg.jourDepart==3)f.stringField(0, "mercredi");
			    else
				    if(dg.jourDepart==4)f.stringField(0, "jeudi");
				    else
					    if(dg.jourDepart==5)f.stringField(0, "vendredi");
					    else
						    if(dg.jourDepart==6)f.stringField(0, "samedi");
						    else
							    f.stringField(0, "dimanche");
	    f.stringField(2, "jour de depart de la simulation");
	    f.writeFields(fmt);
	    //pied de pilote
	    f.stringField(0, new Integer(dg.piedDePilote).toString());
	    f.stringField(2, "pourcentage du pied de pilote de la simulation");
	    f.writeFields(fmt);
	    
	    f.stringField(0, new Integer(dg.nombreJoursFeries).toString());
	    f.stringField(2, "Nombre de jours feries de la simulation");
	    f.writeFields(fmt);
	    for(int i=0;i<dg.nombreJoursFeries;i++)
	    {
	    	f.stringField(0, new Integer(dg.tableauJoursFeries[i]).toString());
		    f.stringField(2, "jour ferie num "+i+" de la simulation");
		    f.writeFields(fmt);
		    	
	    }
	*/    
	  //fermeture du fichier de donn�es g�n�rales
	    f.flush();
	    f.close();
	  }
  
  
  
  public static void ecritDonneesEcluses(final SParametresEcluses2 ec, String nomFichier) throws IOException {
	  //creation du fichier des cat�gories des navires:
	  final FortranWriter f = new FortranWriter(new FileWriter(nomFichier + ".ecl"));
//	 format du fichier : 40 caract�res de donn�es + 1 s�parateur + 100 caract�res de commentaire
	    final int[] fmt = new int[] { 40, 1, 100 };
	 
	    
	    f.stringField(0, new Integer(ec.nbEcluses).toString());
	    f.stringField(2, "Nombre d'ecluses de la simulation");
	    f.writeFields(fmt);
  
	    for(int i=0;i<ec.nbEcluses;i++)
	    {
	    	f.stringField(0,ec.listeEcluses[i].nom);
		    f.stringField(2, "nom de l'ecluse "+(i+1));
		    f.writeFields(fmt);
		    f.stringField(0, new Float(ec.listeEcluses[i].longueur).toString());
	        f.stringField(2, "longueur de l'ecluse " + (i + 1));
	        f.writeFields(fmt);
	        f.stringField(0, new Float(ec.listeEcluses[i].largeur).toString());
	        f.stringField(2, "largeur de l'ecluse " + (i + 1));
	        f.writeFields(fmt);
	        f.stringField(0, new Float(ec.listeEcluses[i].profondeur).toString());
	        f.stringField(2, "profondeur de l'ecluse " + (i + 1));
	        f.writeFields(fmt);
	        f.stringField(0, new Float(ec.listeEcluses[i].hauteurChute).toString());
	        f.stringField(2, "hauteur de chute de l'ecluse " + (i + 1));
	        f.writeFields(fmt);
	        
	        f.stringField(0, new Integer((int) ec.listeEcluses[i].tempsFausseBassineeAvalant).toString());
	        f.stringField(2, "Bassin�e avalante de l'ecluse " + (i + 1));
	        f.writeFields(fmt);
	        f.stringField(0, new Integer((int) ec.listeEcluses[i].tempsFausseBassineeMontant).toString());
	        f.stringField(2, "Bassin�e montante de l'ecluse " + (i + 1));
	        f.writeFields(fmt);
	        f.stringField(0, new Integer((int) ec.listeEcluses[i].dureeDefManoeuvreEntrant).toString());
	        f.stringField(2, "Dur�e par d�faut des manoeuvres entrant de l'ecluse " + (i + 1));
	        f.writeFields(fmt);
	        f.stringField(0, new Integer((int) ec.listeEcluses[i].dureeDefManoeuvreSortant).toString());
	        f.stringField(2, "Dur�e par d�faut des manoeuvres sortant de l'ecluse " + (i + 1));
	        f.writeFields(fmt);
	        f.stringField(0, new Integer((int) ec.listeEcluses[i].dureeDefManoeuvreEntrant2).toString());
	        f.stringField(2, "Seconde dur�e des manoeuvres entrant de l'ecluse " + (i + 1));
	        f.writeFields(fmt);
	        f.stringField(0, new Integer((int) ec.listeEcluses[i].dureeDefManoeuvreSortant2).toString());
	        f.stringField(2, "Seconde dur�e des manoeuvres sortant de l'ecluse " + (i + 1));
	        f.writeFields(fmt);
	        f.stringField(0, new Integer(ec.listeEcluses[i].gareAmont+1).toString());
		    f.stringField(2, "gare amont de l'ecluse " + (i + 1));
		    f.writeFields(fmt);
		    f.stringField(0, new Integer(ec.listeEcluses[i].gareAval+1).toString());
		    f.stringField(2, "gare aval de l'ecluse " + (i + 1));
		    f.writeFields(fmt);
	        
		    if(ec.listeEcluses[i].typeLoi==0)
		    {
		    	f.stringField(0, "E");
		    	f.stringField(2, "loi des indisponibilites de l'ecluse"+(i+1));
		    	f.writeFields(fmt);
		    	f.stringField(0, new Integer((int) ec.listeEcluses[i].frequenceMoyenne).toString());
		    	f.stringField(2, "ecart moyen de l'ecluse "+(i+1));
		    	f.writeFields(fmt);
		    	f.stringField(0, new Integer(ec.listeEcluses[i].loiFrequence).toString());
		    	f.stringField(2, "loi d'erlang de la frequence de l'ecluse "+(i+1));
		    	f.writeFields(fmt);
                
		    }//fin loi erlang
		    else
		    	if(ec.listeEcluses[i].typeLoi==1)
		    	{
		    	  //loi deterministe:
		    		f.stringField(0, "D");
			    	f.stringField(2, "loi des indisponibilites de l'ecluse"+(i+1));
			    	f.writeFields(fmt);
			    	f.stringField(0, new Integer(ec.listeEcluses[i].nombreCouplesDeterministes).toString());
			    	f.stringField(2, "nombre de couples deterministes de l'ecluse"+(i+1));
			    	f.writeFields(fmt);
			    	for(int k=0;k<ec.listeEcluses[i].nombreCouplesDeterministes;k++)
			    	{
			    		f.stringField(0, new Integer(ec.listeEcluses[i].TableauloiDeterministe[k].jour).toString());
				    	f.stringField(2, "jour de la loi deterministe "+k+" de l'ecluse"+(i+1));
				    	f.writeFields(fmt);
				    	f.stringField(0, new Integer((int)ec.listeEcluses[i].TableauloiDeterministe[k].horaire).toString());
				    	f.stringField(2, "horaire de la loi deterministe "+k+" de l'ecluse"+(i+1));
				    	f.writeFields(fmt);
				    	/*
				    	f.stringField(0, new Integer(ec.listeEcluses[i].TableauloiDeterministe[k].jour).toString());
				    	f.stringField(2, "jour "+k+" de la loi deterministe de l'ecluse"+(i+1));
				    	f.writeFields(fmt);
			    	    */
			    	}
		    	}//fin loi deterministe
		    	else
		    	{
//		    		loi journaliere:
		    		f.stringField(0, "J");
			    	f.stringField(2, "loi des indisponibilites de l'ecluse"+(i+1));
			    	f.writeFields(fmt);
			    	f.stringField(0, new Integer(ec.listeEcluses[i].nombreHorairesJournaliers).toString());
			    	f.stringField(2, "nombre d'horaires journaliers de l'ecluse"+(i+1));
			    	f.writeFields(fmt);
			    	for(int k=0;k<ec.listeEcluses[i].nombreHorairesJournaliers;k++)
			    	{
			    		f.stringField(0, new Float(ec.listeEcluses[i].TableauloiJournaliere[k].horaire).toString());
				    	f.stringField(2, "horaire "+k+" de la loi journaliere de l'ecluse"+(i+1));
				    	f.writeFields(fmt);
				    	
			    	}
		    	}//fin loi journaliere
		    f.stringField(0, new Float(ec.listeEcluses[i].dureeIndispo).toString());
	    	f.stringField(2, "dur�e moyenne d'indisponibilit� de l'ecluse"+(i+1));
	    	f.writeFields(fmt);
	    	f.stringField(0, new Integer(ec.listeEcluses[i].loiIndispo).toString());
	    	f.stringField(2, "loi erlang de la dur�e indispo de l'ecluse"+(i+1));
	    	f.writeFields(fmt);
		    
	        //crenaux horaires de l ecluse
	        f.stringField(0, new Float(ec.listeEcluses[i].h.semaineCreneau1HeureDep).toString());
	        f.stringField(2, "creneau 1 debut de l'ecluse" + (i + 1));
	        f.writeFields(fmt);
	        f.stringField(0, new Float(ec.listeEcluses[i].h.semaineCreneau1HeureArrivee).toString());
	        f.stringField(2, "creneau 1 arrivee de l'ecluse" + (i + 1));
	        f.writeFields(fmt);
	        f.stringField(0, new Float(ec.listeEcluses[i].h.semaineCreneau2HeureDep).toString());
	        f.stringField(2, "creneau 2 debut de l'ecluse" + (i + 1));
	        f.writeFields(fmt);
	        f.stringField(0, new Float(ec.listeEcluses[i].h.semaineCreneau2HeureArrivee).toString());
	        f.stringField(2, "creneau 2 arrivee de l'ecluse" + (i + 1));
	        f.writeFields(fmt);
	        f.stringField(0, new Float(ec.listeEcluses[i].h.semaineCreneau3HeureDep).toString());
	        f.stringField(2, "creneau 3 debut de l'ecluse" + (i + 1));
	        f.writeFields(fmt);
	        f.stringField(0, new Float(ec.listeEcluses[i].h.semaineCreneau3HeureArrivee).toString());
	        f.stringField(2, "creneau 3 arrivee de l'ecluse" + (i + 1));
	        f.writeFields(fmt);
	        
	        f.stringField(0, new Integer((int) ec.listeEcluses[i].coefficientBassinEpargne).toString());
	        f.stringField(2, "Coefficient de bassin d'�pargne de l'ecluse " + (i + 1));
	        f.writeFields(fmt);
	        
		    
	    }
	    
	    //fermeture du fichier de donn�es des ecluses
	    f.flush();
	    f.close();
	  }
  
  
  public static void ecritDonneesGares(final SParametresGares g, String nomFichier) throws IOException {
	  //creation du fichier des cat�gories des navires:
	  final FortranWriter f = new FortranWriter(new FileWriter(nomFichier + ".gare"));
//format du fichier : 40 caract�res de donn�es + 1 s�parateur + 100 caract�res de commentaire
	    final int[] fmt = new int[] { 40, 1, 100 };
	 
	    f.stringField(0, new Integer(g.nbGares).toString());
	    f.stringField(2, "Nombre de gares de la simulation");
	    f.writeFields(fmt);
  
	    for(int i=0;i<g.nbGares;i++)
	    {
	    	f.stringField(0, g.listeGares[i].nom);
		    f.stringField(2, "nom de la gare"+(i+1));
		    f.writeFields(fmt);
		    
		    
	  	  	
	    }
//fermeture du fichier de donn�es des gares
	    f.flush();
	    f.close();
  }
  
  
   
 
	 
  public static void ecritDonneesBief(final SParametresCheneaux2 c, String nomFichier) throws IOException {
	  //creation du fichier des cat�gories des navires:
	  final FortranWriter f = new FortranWriter(new FileWriter(nomFichier + ".chenal"));
//format du fichier : 40 caract�res de donn�es + 1 s�parateur + 100 caract�res de commentaire
	    final int[] fmt = new int[] { 40, 1, 100 };

	    f.stringField(0, new Integer(c.nbCheneaux).toString());
	    f.stringField(2, "nombres de chenaux de la simulation");
	    f.writeFields(fmt);
  
	    for(int i=0;i<c.nbCheneaux;i++)
	    {
	        f.stringField(0, c.listeCheneaux[i].nom);
		    f.stringField(2, "nom du chenal "+(i+1));
		    f.writeFields(fmt);
		    f.stringField(0, new Float(c.listeCheneaux[i].profondeur).toString());
		    f.stringField(2, "profondeur du chenal "+(i+1));
		    f.writeFields(fmt);
		    
		    f.stringField(0, new Float(c.listeCheneaux[i].longueur).toString());
		    f.stringField(2, "longueur du chenal "+(i+1));
		    f.writeFields(fmt);
		    
		    f.stringField(0, new Float(c.listeCheneaux[i].largeur).toString());
		    f.stringField(2, "largeur du chenal "+(i+1));
		    f.writeFields(fmt);
		    
		    f.stringField(0, new Float(c.listeCheneaux[i].vitesse).toString());
		    f.stringField(2, "vitesse du chenal "+(i+1));
		    f.writeFields(fmt);
		    
		    f.stringField(0, new Integer(c.listeCheneaux[i].gareAmont+1).toString());
		    f.stringField(2, "gare amont du chenal"+(i+1));
		    f.writeFields(fmt);
		    f.stringField(0, new Integer(c.listeCheneaux[i].gareAval+1).toString());
		    f.stringField(2, "gare aval du chenal"+(i+1));
		    f.writeFields(fmt);
		    
		    if(c.listeCheneaux[i].typeLoi==0)
		    {
		    	f.stringField(0, "E");
		    	f.stringField(2, "loi des indisponibilites du bief "+(i+1));
		    	f.writeFields(fmt);
		    	f.stringField(0, new Integer((int) c.listeCheneaux[i].frequenceMoyenne).toString());
		    	f.stringField(2, "ecart moyen du bief "+(i+1));
		    	f.writeFields(fmt);
		    	f.stringField(0, new Integer(c.listeCheneaux[i].loiFrequence).toString());
		    	f.stringField(2, "loi d'erlang de la frequence du bief "+(i+1));
		    	f.writeFields(fmt);
                
		    }//fin loi erlang
		    else
		    	if(c.listeCheneaux[i].typeLoi==1)
		    	{
		    	  //loi deterministe:
		    		f.stringField(0, "D");
			    	f.stringField(2, "loi des indisponibilites du bief"+(i+1));
			    	f.writeFields(fmt);
			    	f.stringField(0, new Integer(c.listeCheneaux[i].nombreCouplesDeterministes).toString());
			    	f.stringField(2, "nombre de couples deterministes du chenal"+(i+1));
			    	f.writeFields(fmt);
			    	for(int k=0;k<c.listeCheneaux[i].nombreCouplesDeterministes;k++)
			    	{
			    		f.stringField(0, new Integer(c.listeCheneaux[i].TableauloiDeterministe[k].jour).toString());
				    	f.stringField(2, "jour de la loi deterministe "+k+" du bief"+(i+1));
				    	f.writeFields(fmt);
				    	f.stringField(0, new Integer((int)c.listeCheneaux[i].TableauloiDeterministe[k].horaire).toString());
				    	f.stringField(2, "horaire de la loi deterministe "+k+" du bief"+(i+1));
				    	f.writeFields(fmt);
				    	/*
				    	f.stringField(0, new Integer(ec.listeEcluses[i].TableauloiDeterministe[k].jour).toString());
				    	f.stringField(2, "jour "+k+" de la loi deterministe du chenal"+(i+1));
				    	f.writeFields(fmt);
			    	    */
			    	}
		    	}//fin loi deterministe
		    	else
		    	{
		    		//		    		loi journaliere:
		    		f.stringField(0, "J");
			    	f.stringField(2, "loi des indisponibilites du chenal"+(i+1));
			    	f.writeFields(fmt);
			    	f.stringField(0, new Integer(c.listeCheneaux[i].nombreHorairesJournaliers).toString());
			    	f.stringField(2, "nombre d'horaires journaliers du chenal"+(i+1));
			    	f.writeFields(fmt);
			    	for(int k=0;k<c.listeCheneaux[i].nombreHorairesJournaliers;k++)
			    	{
			    		f.stringField(0, new Float(c.listeCheneaux[i].TableauloiJournaliere[k].horaire).toString());
				    	f.stringField(2, "horaire "+k+" de la loi journaliere du chenal"+(i+1));
				    	f.writeFields(fmt);
				    	
			    	}
		    	}//fin loi journaliere
		    f.stringField(0, new Float(c.listeCheneaux[i].dureeIndispo).toString());
	    	f.stringField(2, "dur�e moyenne d'indisponibilit� du chenal"+(i+1));
	    	f.writeFields(fmt);
	    	f.stringField(0, new Integer(c.listeCheneaux[i].loiIndispo).toString());
	    	f.stringField(2, "loi erlang de la dur�e indispo du chenal"+(i+1));
	    	f.writeFields(fmt);
		    
		    //-- creneaux du chenal --//
		    f.stringField(0, new Float(c.listeCheneaux[i].h.semaineCreneau1HeureDep).toString());
		    f.stringField(2, "creneau 1 depart du chenal "+(i+1));
		    f.writeFields(fmt);
		    f.stringField(0, new Float(c.listeCheneaux[i].h.semaineCreneau1HeureArrivee).toString());
		    f.stringField(2, "creneau 1 arrivee du chenal "+(i+1));
		    f.writeFields(fmt);
		    f.stringField(0, new Float(c.listeCheneaux[i].h.semaineCreneau2HeureDep).toString());
		    f.stringField(2, "creneau 2 depart du chenal "+(i+1));
		    f.writeFields(fmt);
		    f.stringField(0, new Float(c.listeCheneaux[i].h.semaineCreneau2HeureArrivee).toString());
		    f.stringField(2, "creneau 2 arrivee du chenal "+(i+1));
		    f.writeFields(fmt);
		    f.stringField(0, new Float(c.listeCheneaux[i].h.semaineCreneau3HeureDep).toString());
		    f.stringField(2, "creneau 3 depart du chenal "+(i+1));
		    f.writeFields(fmt);
		    f.stringField(0, new Float(c.listeCheneaux[i].h.semaineCreneau3HeureArrivee).toString());
		    f.stringField(2, "creneau 3 arrivee du chenal "+(i+1));
		    f.writeFields(fmt);
		    
	    }
	    
	    f.flush();
	    f.close();
  }
	 
  
  
  
  public static void ecritDonneesVitessesAvalantBief(final SParametresSinavi32 s, String nomFichier) throws IOException {
	  //creation du fichier des cat�gories des navires:
	  final FortranWriter f = new FortranWriter(new FileWriter(nomFichier + ".vitbiefav"));
//format du fichier : 40 caract�res de donn�es + 1 s�parateur + 100 caract�res de commentaire
	    final int[] fmt = new int[] { 40, 1, 100 };

	  //  f.stringField(0, new Integer(s.nbLignesDureeParcoursChenaux).toString());
	   // f.stringField(2, "nombres de lignes(chenaux) de la matrice duree de parcours chenaux");
	   // f.writeFields(fmt);
	   // f.stringField(0, new Integer(s.nbColonnesDureeParcoursChenaux).toString());
	   // f.stringField(2, "nombres de colonnes(cat�gories) de la matrice duree de parcours chenaux");
	   // f.writeFields(fmt);
	    
	    for(int i=0;i<s.cheneaux.nbCheneaux;i++)
	    	for(int j=0;j<s.navires.nombreNavires;j++)
	    	{

	    	    f.stringField(0, new Float(s.matriceDureeParcoursChenaux[i][j]).toString());
	    	    f.stringField(2, "categorie "+(j+1)+" dans bief "+(i+1)+"avalant");
	    	    f.writeFields(fmt);
	    		
	    		
	    	}
	    
	    f.flush();
	    f.close();
  }
	
  public static void ecritDonneesVitessesMontantBief(final SParametresSinavi32 s, String nomFichier) throws IOException {
	  //creation du fichier des cat�gories des navires:
	  final FortranWriter f = new FortranWriter(new FileWriter(nomFichier + ".vitbiefmo"));
//format du fichier : 40 caract�res de donn�es + 1 s�parateur + 100 caract�res de commentaire
	    final int[] fmt = new int[] { 40, 1, 100 };

	  //  f.stringField(0, new Integer(s.nbLignesDureeParcoursChenaux).toString());
	   // f.stringField(2, "nombres de lignes(chenaux) de la matrice duree de parcours chenaux");
	   // f.writeFields(fmt);
	   // f.stringField(0, new Integer(s.nbColonnesDureeParcoursChenaux).toString());
	   // f.stringField(2, "nombres de colonnes(cat�gories) de la matrice duree de parcours chenaux");
	   // f.writeFields(fmt);
	    
	    for(int i=0;i<s.cheneaux.nbCheneaux;i++)
	    	for(int j=0;j<s.navires.nombreNavires;j++)
	    	{

	    	    f.stringField(0, new Float(s.matriceDureeParcoursChenauxMontant[i][j]).toString());
	    	    f.stringField(2, "categorie "+(j+1)+" dans bief "+(i+1)+" montant");
	    	    f.writeFields(fmt);
	    		
	    		
	    	}
	    
	    f.flush();
	    f.close();
  } 
 
  public static void ecritDonneesDurManeuvreEntrantEcluse(final SParametresSinavi32 s, String nomFichier) throws IOException {
	  //creation du fichier des cat�gories des navires:
	  final FortranWriter f = new FortranWriter(new FileWriter(nomFichier + ".durManEntr"));
//format du fichier : 40 caract�res de donn�es + 1 s�parateur + 100 caract�res de commentaire
	    final int[] fmt = new int[] { 40, 1, 100 };

	  //  f.stringField(0, new Integer(s.nbLignesDureeParcoursChenaux).toString());
	   // f.stringField(2, "nombres de lignes(chenaux) de la matrice duree de parcours chenaux");
	   // f.writeFields(fmt);
	   // f.stringField(0, new Integer(s.nbColonnesDureeParcoursChenaux).toString());
	   // f.stringField(2, "nombres de colonnes(cat�gories) de la matrice duree de parcours chenaux");
	   // f.writeFields(fmt);
	    
	    for(int i=0;i<s.ecluses.listeEcluses.length;i++)
	    	for(int j=0;j<s.navires.nombreNavires;j++)
	    	{

	    	    f.stringField(0, new Float(s.matriceDurManeuvreEclEntrant[i][j]).toString());
	    	    f.stringField(2, " maneuvre categorie "+(j+1)+" dans ecluse "+(i+1)+" entrant(Min.sec)");
	    	    f.writeFields(fmt);
	    		
	    	    f.stringField(0, new Float(s.matriceDurManeuvreEclEntrant2[i][j]).toString());
	    	    f.stringField(2, " seconde maneuvre categorie "+(j+1)+" dans ecluse "+(i+1)+" entrant(Min.sec)");
	    	    f.writeFields(fmt);
	    		
	    	}
	    
	    f.flush();
	    f.close();
  }
  public static void ecritDonneesDurManeuvreSortantEcluse(final SParametresSinavi32 s, String nomFichier) throws IOException {
	  //creation du fichier des cat�gories des navires:
	  final FortranWriter f = new FortranWriter(new FileWriter(nomFichier + ".durManSort"));
//format du fichier : 40 caract�res de donn�es + 1 s�parateur + 100 caract�res de commentaire
	    final int[] fmt = new int[] { 40, 1, 100 };

	  //  f.stringField(0, new Integer(s.nbLignesDureeParcoursChenaux).toString());
	   // f.stringField(2, "nombres de lignes(chenaux) de la matrice duree de parcours chenaux");
	   // f.writeFields(fmt);
	   // f.stringField(0, new Integer(s.nbColonnesDureeParcoursChenaux).toString());
	   // f.stringField(2, "nombres de colonnes(cat�gories) de la matrice duree de parcours chenaux");
	   // f.writeFields(fmt);
	    
	    for(int i=0;i<s.ecluses.listeEcluses.length;i++)
	    	for(int j=0;j<s.navires.nombreNavires;j++)
	    	{

	    	    f.stringField(0, new Float(s.matriceDurManeuvreEclSortant[i][j]).toString());
	    	    f.stringField(2, " maneuvre categorie "+(j+1)+" dans ecluse "+(i+1)+" sortant(Min.sec)");
	    	    f.writeFields(fmt);
	    	    
	    	    f.stringField(0, new Float(s.matriceDurManeuvreEclSortant2[i][j]).toString());
	    	    f.stringField(2, " seconde maneuvre categorie "+(j+1)+" dans ecluse "+(i+1)+" sortant(Min.sec)");
	    	    f.writeFields(fmt);
	    		
	    		
	    	}
	    
	    f.flush();
	    f.close();
  }
  
  public static void ecritDonneesCroisementsBief(final SParametresCheneaux2 c, String nomFichier) throws IOException {
	  //creation du fichier des cat�gories des navires:
	  final FortranWriter f = new FortranWriter(new FileWriter(nomFichier + ".croische"));
//format du fichier : 40 caract�res de donn�es + 1 s�parateur + 100 caract�res de commentaire
	    final int[] fmt = new int[] { 40, 1, 100 };

	//    f.stringField(0, new Integer(c.nbCheneaux).toString());
	//    f.stringField(2, "nombres de tableaux de croisements de navires dans chenal");
	//    f.writeFields(fmt);
  
	    for(int i=0;i<c.nbCheneaux;i++)
	    {
	    	
	    //	f.stringField(0, new Integer(c.listeCheneaux[i].dimensionMatriceReglesNav).toString());
		//    f.stringField(2, "dimensions de la matrice des regles de navigation du chenal"+(i+1));
		//    f.writeFields(fmt);
              
		        for(int k=0;k<c.listeCheneaux[i].dimensionMatriceCroisement;k++)
		        	for(int l=0;l<c.listeCheneaux[i].dimensionMatriceCroisement;l++)
		        	{
		        		if(c.listeCheneaux[i].matriceCroisement[k][l])
		        		f.stringField(0, "oui");
		        		else
		        			f.stringField(0, "non");
		    		    f.stringField(2, "categorie "+(k+1)+" et categorie "+(l+1)+" dans bief "+(i+1));
		    		    f.writeFields(fmt);
		        	}
		    
  
	    }
	    
	f.flush();
	f.close();
  }
  
  public static void ecritDonneesRemplissageSAS(final SParametresSinavi32 s, String nomFichier) throws IOException {
	    //creation du fichier des cat�gories des navires:
	    final FortranWriter f = new FortranWriter(new FileWriter(nomFichier + ".rempliSAS"));
	//format du fichier : 40 caract�res de donn�es + 1 s�parateur + 100 caract�res de commentaire
	      final int[] fmt = new int[] { 40, 1, 100 };
	      
	      for(int i=0;i<s.ecluses.nbEcluses;i++)
	        for(int j=0;j<s.navires.nombreNavires;j++)
	        {

	            f.stringField(0, new Float(s.matriceRemplissageSAS[i][j]).toString());
	            f.stringField(2, "categorie "+(j+1)+" dans SAS ecluse "+(i+1)+"");
	            f.writeFields(fmt);
	          
	          
	        }
	      
	      f.flush();
	      f.close();
	  }
  
  public static void ecritDonneesTrematagesBief(final SParametresCheneaux2 c, String nomFichier) throws IOException {
	  //creation du fichier des cat�gories des navires:
	  final FortranWriter f = new FortranWriter(new FileWriter(nomFichier + ".tremache"));
//format du fichier : 40 caract�res de donn�es + 1 s�parateur + 100 caract�res de commentaire
	    final int[] fmt = new int[] { 40, 1, 100 };

	//    f.stringField(0, new Integer(c.nbCheneaux).toString());
	//    f.stringField(2, "nombres de tableaux de croisements de navires dans chenal");
	//    f.writeFields(fmt);
  
	    for(int i=0;i<c.nbCheneaux;i++)
	    {
	    	
	    //	f.stringField(0, new Integer(c.listeCheneaux[i].dimensionMatriceReglesNav).toString());
		//    f.stringField(2, "dimensions de la matrice des regles de navigation du chenal"+(i+1));
		//    f.writeFields(fmt);
              
		        for(int k=0;k<c.listeCheneaux[i].dimensionMatriceTrematage;k++)
		        	for(int l=0;l<c.listeCheneaux[i].dimensionMatriceTrematage;l++)
		        	{
		        		if(c.listeCheneaux[i].matriceTrematage[k][l])
		        		f.stringField(0, "oui");
		        		else
		        			f.stringField(0, "non");
		    		    f.stringField(2, "categorie "+(k+1)+" et categorie "+(l+1)+" dans bief "+(i+1));
		    		    f.writeFields(fmt);
		        	}
		    
  
	    }
	    
	f.flush();
	f.close();
  }
  
  
 
  
  
  /**
   * M�thode d'�criture des fichiers de param�tres de Sinavi3.
   * @param nomFichier
   * @param params
   */
  public static void ecritParametresSinavi3(final String nomFichier, final SParametresSinavi32 params_) {

	  try {
		  // cat�gories
		  DParametresSinavi3.ecritDonneesCategoriesNavires(params_.navires, nomFichier);
		  // donn�es generales
		  DParametresSinavi3
		  .ecritDonneesGenerales(params_.donneesGenerales, nomFichier);
		  // ecluses
		  DParametresSinavi3.ecritDonneesEcluses(params_.ecluses, nomFichier);
		 // gares
		  DParametresSinavi3.ecritDonneesGares(params_.gares, nomFichier);
		  // chenal
		  DParametresSinavi3.ecritDonneesBief(params_.cheneaux, nomFichier);

		  // durees de parcours des chenaux
		  DParametresSinavi3.ecritDonneesVitessesAvalantBief(params_, nomFichier);
		  
		  // dur�es de croisement des chenaux:
		  DParametresSinavi3.ecritDonneesCroisementsBief(params_.cheneaux, nomFichier);
		  
		  //dur�es de croisement des chenaux:
		  DParametresSinavi3.ecritDonneesTrematagesBief(params_.cheneaux, nomFichier);
		  
		// d�lais remplissage SAS ecluse.
		  DParametresSinavi3.ecritDonneesRemplissageSAS(params_, nomFichier);
	      
		  
	  } catch (final Exception ex) {

	  }
  }

public SParametresSinavi32 parametresSinavi3() {
	// TODO Auto-generated method stub
	return parametresSinavi_;
}

public void parametresSinavi3(SParametresSinavi32 newParametresSinavi3) {
	parametresSinavi_=newParametresSinavi3;
	
}
}
