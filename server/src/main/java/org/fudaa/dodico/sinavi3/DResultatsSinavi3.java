/**
 * @file         DResultatsSinavi3.java
 * @creation     1999-09-23
 * @modification $Date: 2008-01-25 14:02:01 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.sinavi3;
import java.io.*;
import java.util.*;

import org.fudaa.dodico.objet.*;
import org.fudaa.dodico.calcul.*;
import org.fudaa.dodico.fortran.FortranReader;
import org.fudaa.dodico.corba.sinavi3.*;
import org.fudaa.dodico.corba.sinavi3.Sinavi3ResultatGarage;

import com.memoire.fu.FuLog;
/**
 * Permet d'acc�der aux r�sultats d'une simulation.
 *
 * @version      $Revision: 1.4 $ $Date: 2008-08-13 11:00:00 $ by $Author: fargeix $
 * @author       Nicolas Chevalier
 */
public class DResultatsSinavi3
  extends DResultats
  implements IResultatsSinavi3,IResultatsSinavi3Operations {
  private String fichier_;
  public DResultatsSinavi3() {
    super();
    fichier_= "";
  }
  public final  Object clone() throws CloneNotSupportedException {
    final DResultatsSinavi3 r= new DResultatsSinavi3();
    r.fichier(fichier_);
    return r;
  }
  public String toString() {
    return "DResultatsSinavi3(" + fichier_ + ")";
  }
  public String fichier() {
    return fichier_;
  }
  /**
   * Mutateur de fihcier. Ajoute "_histo.dat" � _fichier.
   *
   * @param _fichier
   */
  public void fichier(final String _fichier) {
    fichier_= _fichier + /*"_histo.dat"*/".his";
  }
  /**
   * Pour acc�der aux r�sultats.
   *
   * @return r�sultats
   */
  public Sinavi3ResultatListeevenementsSimu litResultatsSinavi3() {
    return litResultatsSinavi3(fichier());
  }
  /**
   * Lit un fichier de r�sultats et renvoie une instance de SResultatsIterations
   * correspondant aux donn�es lues.
   *
   * @param nomFichier
   * @return r�sultats
   */
  public static double arrondire2decimales(double val)
  {
	  val=val*100;
	  val=(int)(val+0.5);
	  val=val/100.0;
	  return val;
  }
  
  public static Sinavi3ResultatListeevenementsSimu litResultatsSinavi3(final String nomFichier) {
    try {
     FuLog.debug("Lecture de " + nomFichier);
    
      Sinavi3ResultatListeevenementsSimu listeResultats= new Sinavi3ResultatListeevenementsSimu();
      
      //vecteur dynamique qui servira a stocker chaque navire en attendant de connaitrez le nombre exact de navires
      ArrayList vecteurListe=new ArrayList();
      
      //structure finale qui contiendra la totalit� des navires
      FortranReader fr= new FortranReader(new FileReader(nomFichier));
     
      try {
          while (true) {
        	  fr.readFields(); 
        	  
        	  if (fr.getNumberOfFields() > 2) {
        		  	//recopiage des parametres dans le champs
        		 
        		  //allocation m�moire pour les donn�es d un anvire
        		  Sinavi3ResultatsSimulation donneesNavire=new Sinavi3ResultatsSimulation();  
        		 FuLog.debug("num cat: "+fr.intField(0)+"\n categorie: "+(fr.intField(1)-1)+"\n longueur: "+fr.stringField(2)+"\n nb elements trajet: "+fr.intField(4));
        		  //recopiage des donn�es du fichier dans la struture donneesNavire
        		  donneesNavire.numero=fr.intField(0);
        		  donneesNavire.categorie=fr.intField(1)-1;
        		  String typeSens=fr.stringField(2);
        		  if(typeSens.equals("A"))
        			  donneesNavire.sens=0;
        		  else
        			  donneesNavire.sens=1;
        		  
        		  donneesNavire.NbElemtnsParcours=fr.intField(3);
        		  //allocation memoire pour le tableau de trajet a partir du nombre d elements:
        		  donneesNavire.tableauTrajet=new Sinavi3ResultatsDonneeTrajet[donneesNavire.NbElemtnsParcours];
        		 FuLog.debug("trajet: ");
        		  //lecture de la liste ddu trajet du navire donn�
        		  //int indiceDonnee=5;
        		      for(int i=0;i<donneesNavire.NbElemtnsParcours;i++)
        		      {
        		    	  
        		    	  
        		    	  //-- Variante suite aux modifications du fichier historique --//
        		    	  
        		    	  //-- lecture de la ligne contenant l'�l�ment parcouru
        		    	  fr.readFields(); 
        		    	  
        		    	  //allocation memoire pour un element du trajet:
        		    	  donneesNavire.tableauTrajet[i]=new Sinavi3ResultatsDonneeTrajet();
        		    	  //donnees associ�es � un �l�ment
        		    	  String typeElement=fr.stringField(0);
        		    	  if(typeElement.equals("T") ||typeElement.equals("t"))
            		    	  donneesNavire.tableauTrajet[i].typeElement=0;
        		    	  else
        		    		  donneesNavire.tableauTrajet[i].typeElement=1;
        		    	  
        		    	  FuLog.debug("\n indice element: "+fr.intField(1));
          		    	  donneesNavire.tableauTrajet[i].indiceElement=fr.intField(1)-1;
        		    	  
          		    	  //--Horaires et attentes sont en secondes, on les traduit donc en minutes (adaptation sinavi) --//
        		    	 FuLog.debug("\n heure entree: "+fr.doubleField(2));
          		    	  donneesNavire.tableauTrajet[i].heureEntree=fr.doubleField(2);///60.0;
        		    	  
          		    	 FuLog.debug("\n heure sortie: "+fr.doubleField(3));
          		    	  donneesNavire.tableauTrajet[i].heureSortie=fr.doubleField(3);///60.0;
          		    	  
          		    	  donneesNavire.tableauTrajet[i].acces=fr.doubleField(4);///60.0;
        		    	  
        		    	 FuLog.debug("\n attente securite: "+fr.doubleField(5));
        		    	  donneesNavire.tableauTrajet[i].secu=fr.doubleField(5);///60.0;
        		    	  
        		    	 FuLog.debug("\n attente occupation: "+fr.doubleField(6));
        		    	  donneesNavire.tableauTrajet[i].occupation=fr.doubleField(6);///60.0;
        		    	  
        		    	 FuLog.debug("\n attente indisponibilit�: "+fr.doubleField(7));
        		    	  donneesNavire.tableauTrajet[i].indispo=fr.doubleField(7);///60.0;
        		    	  
        		    	  
        		    	  
        		      }
        		  
        		  //ajout des don�esnavire dans le vecteur
        		  vecteurListe.add(donneesNavire);
        		  
        	  }
        	  else
        	  {
        		  //on quitte
        		  //on recopie le contenue ainsi que le nombre de navire du vecteur dans al structure finale que lon renvoie ensuite
        		  
        		  //on connais le nombre de navires: il s agit du nombre d elements du vecteur
        		  listeResultats.nombreNavires=vecteurListe.size();
        		  
        		  //allocation memoire du tableau de navires
        		  listeResultats.listeEvenements=new Sinavi3ResultatsSimulation[vecteurListe.size()];
        		  
        		  for(int i=0;i<vecteurListe.size();i++)
        		  {
        			  listeResultats.listeEvenements[i]=(Sinavi3ResultatsSimulation) vecteurListe.get(i); 
        		  }
        		  
        		  
        		  //renvoie de la structure finale:
        		  return listeResultats;
        		  
        	  }
          }
          }
          catch (final EOFException e) {
        	 FuLog.debug("fin de fichier");
//        	on connais le nombre de navires: il s agit du nombre d elements du vecteur
    		  listeResultats.nombreNavires=vecteurListe.size();
    		  
    		  //allocation memoire du tableau de navires
    		  listeResultats.listeEvenements=new Sinavi3ResultatsSimulation[vecteurListe.size()];
    		  
    		  for(int i=0;i<vecteurListe.size();i++)
    		  {
    			  listeResultats.listeEvenements[i]=(Sinavi3ResultatsSimulation) vecteurListe.get(i); 
    		  }
    		  
    		  
    		  //renvoie de la structure finale:
    		  return listeResultats;
          
          }
         
     
    } catch (final Exception ex) {
      CDodico.exception(DResultatsSinavi3.class, ex);
    }
    return null;
  }
  
public static Sinavi3ResultatListeOccupations litOccupationsSinavi3(final String nomFichier, final int nombreEcluses,final List<Sinavi3ResultatGarage> listeGarages) {
	  
	  int numberOfFields;
	  int[][][] indexBassinee = new int[2][2][nombreEcluses]; // int[2:typeEl:G/E][2:sens:A/M][nbEcluses]
	  
	  try {
		  FuLog.debug("Lecture de " + nomFichier);
	       
		  Sinavi3ResultatListeOccupations listeOccupations = new Sinavi3ResultatListeOccupations();
	         
		  //vecteur dynamique qui servira a stocker chaque navire en attendant de connaitrez le nombre exact de navires
		  ArrayList<Sinavi3ResultatOccupation> vecteurOccup = new ArrayList<Sinavi3ResultatOccupation>();
	         
		  //structure finale qui contiendra la totalit� des navires
		  FortranReader fr= new FortranReader(new FileReader(nomFichier));
	        
		  try {
			  
			  while (true) {
				  fr.readFields(); 
				  numberOfFields = fr.getNumberOfFields();
				  
				  if (numberOfFields > 6) {
					  
					  //allocation m�moire
					  Sinavi3ResultatOccupation resultatOccupation = new Sinavi3ResultatOccupation();  
					  
					  FuLog.debug(fr.stringField(0) + " numero " + fr.intField(1)+"\n heure: "+(fr.doubleField(4))+"\n sens: "+fr.stringField(5));
	           		  //recopiage des donn�es du fichier dans la struture donneesNavire
					  String typeElement = fr.stringField(0);
	           		  if(typeElement.equals("G")) resultatOccupation.type = 0;
	           		  else resultatOccupation.type = 1;
					  
					  resultatOccupation.numero = fr.intField(1)-1; //-1 pour faire correspondre num�rotation 1->N du fichier avec num�rotation 0->N-1 de Fudaa-Sinavi
					  
					  resultatOccupation.ecluseConcernee = fr.intField(3)-1; //-1 pour faire correspondre num�rotation 1->N du fichier avec num�rotation 0->N-1 de Fudaa-Sinavi
					  
					  resultatOccupation.heure = fr.doubleField(4);
	           		  
	           		  String typeSens=fr.stringField(5);
	           		  if(typeSens.equals("A")) resultatOccupation.sens = 0;
	           		  else resultatOccupation.sens = 1;
	           		  
	           		  // On num�rote les bassin�es
	           		System.out.println("taille tableau"+indexBassinee.length);
	           		  System.out.println("type="+  resultatOccupation.type + " - sens="+resultatOccupation.sens+" ecluse= "+resultatOccupation.ecluseConcernee);
					  resultatOccupation.refBassinee = (indexBassinee[resultatOccupation.type][resultatOccupation.sens][resultatOccupation.ecluseConcernee]++);
	           		 
					  
					  
					  
					  
	           		  resultatOccupation.nombreBateauxCategorie = new short[numberOfFields-7];
	           		  for (int k = 7; k < numberOfFields; k++) {
	           			  try{
	           				  resultatOccupation.nombreBateauxCategorie[k-7] = (short) fr.intField(k);
	           			  }catch(Exception e) {
	           				System.err.println("Erreur fichier occupation: lecture du nombre de bateaux par cat�gorie n'est pas un entier : " + fr.stringField(k)); 
	           				resultatOccupation.nombreBateauxCategorie[k-7] = 0;
	           			  }
	           		  }
	        		  vecteurOccup.add(resultatOccupation);
	        		  
	        		  
	        		  //-- stockage des donn�es relatives aux fr�quence de garage ecluse --//
					  //-- dans le cas d'une ecluse: taux d'occupation de l'ecluse en % --//
					  //-- dans le cas d'une gare: Longueur de la file d'attente --//
					  double occuenre = -1;
					  if(typeElement.equals("G")){
							  occuenre =  resultatOccupation.heure;
						  Sinavi3ResultatGarage garage = null;
						  for(Sinavi3ResultatGarage og: listeGarages) {
							  if(og.dateEvent == occuenre) {
								  garage =og;
								  garage.frequenceRetour=garage.frequenceRetour+1;
							  }
						  }
						  if(garage == null) {
							  garage = new Sinavi3ResultatGarage();
							  garage.dateEvent = occuenre;
							  garage.frequenceRetour = 1;
							  garage.lineaireTotal = fr.doubleField(6);
							  garage.ecluse = resultatOccupation.ecluseConcernee ;
							  garage.gare = resultatOccupation.numero;
							  garage.listBateaux = new Sinavi3ResultatGarageBat[resultatOccupation.nombreBateauxCategorie.length];
							  int nbTotalBateaux =0;
							  for(int i =0; i<resultatOccupation.nombreBateauxCategorie.length;i++) {
								  garage.listBateaux[i] = new Sinavi3ResultatGarageBat();
								  garage.listBateaux[i].categorie=i;
								  garage.listBateaux[i].nbBateaux = resultatOccupation.nombreBateauxCategorie[i];
								  nbTotalBateaux += garage.listBateaux[i].nbBateaux;
							  }
							  garage.nbBateauxAttente = nbTotalBateaux;
							  listeGarages.add(garage);
						  }
					  }
	           	  
				  } else {
	           		  //on quitte
	           		  //on recopie le contenu ainsi que le nombre de navire du vecteur dans al structure finale que lon renvoie ensuite
	           		  
	           		  //on connais le nombre de navires: il s agit du nombre d elements du vecteur
	           		  listeOccupations.nombreEvenements = vecteurOccup.size();
	           		  
	           		  //allocation memoire du tableau de navires
	           		  listeOccupations.listeOccupations = new Sinavi3ResultatOccupation[vecteurOccup.size()];
	           		  
	           		  for(int i=0;i<vecteurOccup.size();i++)
	           		  {
	           			  listeOccupations.listeOccupations[i] = (Sinavi3ResultatOccupation) vecteurOccup.get(i); 
	           		  }
	           		  
	           		  
	           		  //renvoie de la structure finale:
	           		  return listeOccupations;
				  }
			  }
		  }
		  catch (final EOFException e) {
			  FuLog.debug("fin de fichier");
			  //on connais le nombre de navires: il s agit du nombre d elements du vecteur
			  listeOccupations.nombreEvenements = vecteurOccup.size();
	       		  
			  //allocation memoire du tableau de navires
			  listeOccupations.listeOccupations = new Sinavi3ResultatOccupation[vecteurOccup.size()];
	       		  
			  for(int i=0;i<vecteurOccup.size();i++)
			  {
				  listeOccupations.listeOccupations[i]=(Sinavi3ResultatOccupation) vecteurOccup.get(i); 
			  }
	       		  
			  //renvoie de la structure finale:
			  return listeOccupations;
	             
		  }
	        
	  } catch (final Exception ex) {
		  CDodico.exception(DResultatsSinavi3.class, ex);
	  }
	  return null;
  }

	
  
  public static Sinavi3ResultatListeBassinees litBassineesSinavi3(final String nomFichier, Map<Integer, Integer> mapBassineeEcluses) {
	  
	  int numberOfFields;
	  
	  try {
		  FuLog.debug("Lecture de " + nomFichier);
	       
		  Sinavi3ResultatListeBassinees listeBassinees = new Sinavi3ResultatListeBassinees();
	         
		  //vecteur dynamique qui servira a stocker chaque navire en attendant de connaitrez le nombre exact de navires
		  ArrayList<Sinavi3ResultatBassinee> vecteurBassineee = new ArrayList<Sinavi3ResultatBassinee>();
	         
		  //structure finale qui contiendra la totalit� des navires
		  FortranReader fr= new FortranReader(new FileReader(nomFichier));
	        
		  try {
			  
			  while (true) {
				  fr.readFields(); 
				  numberOfFields = fr.getNumberOfFields();
	           	  
				  if (numberOfFields == 5) {
					  
					  //allocation m�moire
					  Sinavi3ResultatBassinee resultatBassinee = new Sinavi3ResultatBassinee();  

					  //recopiage des donn�es du fichier dans la struture donneesNavire
					  
					  resultatBassinee.numero = fr.intField(1)-1; //-1 pour faire correspondre num�rotation 1->N du fichier avec num�rotation 0->N-1 de Fudaa-Sinavi
					  
					  
					  //-- add bassinee for ecluse --//					  
						  if(mapBassineeEcluses.containsKey(resultatBassinee.numero)) {
							  int nbBassinees = mapBassineeEcluses.get(resultatBassinee.numero) +1;
							  mapBassineeEcluses.put(resultatBassinee.numero, nbBassinees);
						  }else
							  mapBassineeEcluses.put(resultatBassinee.numero, 1);
	           		  
					  
					  
					  String typeBassinee = fr.stringField(2);
	           		  if(typeBassinee.equals("F")) resultatBassinee.fausseBassinee = true;
	           		  else resultatBassinee.fausseBassinee = false;
					  
	           		  resultatBassinee.heure = fr.doubleField(3);
	           		  
	           		  String typeSens=fr.stringField(4);
	           		  if(typeSens.equals("A")) resultatBassinee.sens = 0;
	           		  else resultatBassinee.sens = 1;
	           		  
	           		  //ajout des don�esnavire dans le vecteur
	        		  vecteurBassineee.add(resultatBassinee);
	           	  
				  } else {
	           		  //on quitte
	           		  //on recopie le contenu ainsi que le nombre de navire du vecteur dans al structure finale que lon renvoie ensuite
	           		  
	           		  //on connais le nombre de navires: il s agit du nombre d elements du vecteur
					  listeBassinees.nombreEvenements = vecteurBassineee.size();
	           		  
	           		  //allocation memoire du tableau de navires
					  listeBassinees.listeBassinees = new Sinavi3ResultatBassinee[vecteurBassineee.size()];
	           		  
	           		  for(int i=0;i<vecteurBassineee.size();i++)
	           		  {
	           			  listeBassinees.listeBassinees[i] = (Sinavi3ResultatBassinee) vecteurBassineee.get(i); 
	           		  }
	           		  
	           		  
	           		  //renvoie de la structure finale:
	           		  return listeBassinees;
				  }
			  }
		  }
		  catch (final EOFException e) {
			  FuLog.debug("fin de fichier");
			  //on connais le nombre de navires: il s agit du nombre d elements du vecteur
			  listeBassinees.nombreEvenements = vecteurBassineee.size();
	       		  
			  //allocation memoire du tableau de navires
			  listeBassinees.listeBassinees = new Sinavi3ResultatBassinee[vecteurBassineee.size()];
	       		  
			  for(int i=0;i<vecteurBassineee.size();i++)
			  {
				  listeBassinees.listeBassinees[i]=(Sinavi3ResultatBassinee) vecteurBassineee.get(i); 
			  }
	       		  
			  //renvoie de la structure finale:
			  return listeBassinees;
	             
		  }
	        
	  } catch (final Exception ex) {
		  CDodico.exception(DResultatsSinavi3.class, ex);
	  }
	  return null;
  }
  
 

}
