/**
 * @file         DCalculSinavi3.java
 * @creation     1999-09-17
 * @modification $Date: 2007-11-23 11:31:40 $
 * @license      GNU General Public License 2
 * @copyright    (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail         devel@fudaa.org
 */
package org.fudaa.dodico.sinavi3;
import java.io.File;

import org.fudaa.dodico.calcul.DCalcul;
import org.fudaa.dodico.corba.objet.IConnexion;
import org.fudaa.dodico.corba.sinavi3.ICalculSinavi3;
import org.fudaa.dodico.corba.sinavi3.ICalculSinavi3Helper;
import org.fudaa.dodico.corba.sinavi3.ICalculSinavi3Operations;
import org.fudaa.dodico.corba.sinavi3.IParametresSinavi3;
import org.fudaa.dodico.corba.sinavi3.IParametresSinavi3Helper;
import org.fudaa.dodico.corba.sinavi3.SParametresSinavi32;
//import org.fudaa.dodico.corba.sipor.*;
import org.fudaa.dodico.objet.CDodico;
import org.fudaa.dodico.objet.CExec;
/**
 * @version      $Revision: 1.1 $ $Date: 2007-11-23 11:31:40 $ by $Author: hadouxad $
 * @author       Adrien Hadoux
 */
public class DCalculSinavi3 extends DCalcul implements ICalculSinavi3,ICalculSinavi3Operations {
  public DCalculSinavi3() {
    super();
    setFichiersExtensions(new String[] { ".nom", ".nav", ".bie", ".ouv", ".cnx", ".vites", ".trajet",".croist", ".tremat",".man",".gen",".his"});
    //setFichiersExtensions(new String[] { ".dat", ".out", ".tmp", ".sip", });
  }
  public final  Object clone() throws CloneNotSupportedException{
    return new DCalculSinavi3();
  }
  public String toString() {
    return "DCalculSinavi3()";
  }
  public String description() {
    return "Sinavi, serveur de calcul pour le trafic fluvial"
      + super.description();
  }
  public void calcul(final IConnexion c) {
   
	  /*if (!verifieConnexion(c)) {
      return;
    }*/
	  
    final IParametresSinavi3 params= IParametresSinavi3Helper.narrow(parametres(c));
    if (params == null) {
      CDodico.exceptionAxel(this, new Exception("params non definis (null)"));
    }
   /*
    final IResultatsSipor results= IResultatsSiporHelper.narrow(resultats(c));
    if (results == null) {
      CDodico.exceptionAxel(this, new Exception("results non definis (null)"));
    }*/
    
    
    log(c, "lancement du calcul");
    final String os= System.getProperty("os.name");
    final String path= cheminServeur();
    final String fichier= "sinavi3" + c.numero();
    try {
      
     
      //-- Ecriture des fichiers sipor --//
      final SParametresSinavi32 p2= new SParametresSinavi32();
      
      DParametresSinavi3.ecritParametresSinavi3(path + fichier, p2);
      System.out.println("Appel de l'executable sinavi");
      String[] cmd;
      if (os.startsWith("Windows")) {
        cmd= new String[4];
        cmd[0]= path + "sinavi-win.bat";
        if (path.indexOf(':') != -1) {
          // lettre de l'unite (ex: "C:")
          cmd[1]= path.substring(0, path.indexOf(':') + 1);
          // chemin du serveur
          cmd[2]= path.substring(path.indexOf(':') + 1);
        } else {
          // si pas de lettre dans le chemin
          cmd[1]= "fake_cmd";
          cmd[2]= path;
        }
        cmd[3]= fichier;
        System.out.println(cmd[0] + " " + cmd[1] + " " + cmd[2] + " " + cmd[3]);
      } else {
        cmd= new String[3];
        cmd[0]= path + "sinavi.sh";
        cmd[1]= path;
        cmd[2]= fichier;
        System.out.println(cmd[0] + " " + cmd[1] + " " + cmd[2]);
      }
      final CExec ex= new CExec();
      ex.setCommand(cmd);
      ex.setOutStream(System.out);
      ex.setErrStream(System.err);
      ex.exec();
      System.out.println("Fin du calcul");
      
      log(c, "calcul termin�");
    } catch (final Exception ex) {
      log(c, "erreur du calcul");
      CDodico.exceptionAxel(this, ex);
    } finally {
      new File("fort.10").deleteOnExit();
    }
  }
  
  
  public  void executeSinavi3Genarr(String pathEtude){
	  System.out.println("Appel de l'executable sinavi");
	  final String os= System.getProperty("os.name");
	  final String path= cheminServeur();

	  String[] cmd;

	  try {
		  if (os.startsWith("Windows")) {
			  cmd= new String[4];
			  cmd[0]= path + "sinaviGenarr-win.exe";
			  if (path.indexOf(':') != -1) {
				  // lettre de l'unite (ex: "C:")
				  cmd[1]= path.substring(0, path.indexOf(':') + 1);
				  // chemin du serveur
				  cmd[2]= path.substring(path.indexOf(':') + 1);
			  } else {
				  // si pas de lettre dans le chemin
				  cmd[1]= "fake_cmd";
				  cmd[2]= path;
			  }
			  cmd[3]= pathEtude;
			  System.out.println("**\nLa commande ex�cut�e est: \n "+cmd[0] + " " + cmd[1] + " " + cmd[2] + " " + cmd[3]+"\n**");
		  } else {
			  cmd= new String[3];
			  cmd[0]= path + "sinaviGenarr-linux.x";
			  cmd[1]= path;
			  cmd[2]= pathEtude;
			  System.out.println("**\nLa commande ex�cut�e est: \n "+cmd[0] + " " + cmd[1] + " " + cmd[2]+"\n**");
		  }
		  final CExec ex= new CExec();
		  ex.setCommand(cmd);
		  ex.setOutStream(System.out);
		  ex.setErrStream(System.err);
		  ex.exec();
		  System.out.println("Fin du calcul");


	  } catch (final Exception ex) {

		  CDodico.exceptionAxel(this, ex);
	  } finally {
		  new File("fort.10").deleteOnExit();
	  }


  }
  
  public static void main(String[] args) {
    try {
      ICalculSinavi3 newInstance = (ICalculSinavi3)Class.forName("org.fudaa.dodico.sinavi3.DCalculSinavi3").newInstance();
      System.err.println(newInstance);
    } catch (Exception e) {
      e.printStackTrace();
    }
  
  }

  
}
