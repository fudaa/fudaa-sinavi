/*
 * @creation 12 mars 2006
 * @modification $Date: 2006-09-19 14:45:56 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.sinavi2;

import java.util.Date;

import org.fudaa.dodico.objet.CDodico;
import org.fudaa.dodico.objet.UsineLib;

public final class ServeurSinavi2 {

  private ServeurSinavi2(){
  }

  /**
   * @author stagiaires
   * @version $Id: ServeurSinavi2.java,v 1.7 2006-09-19 14:45:56 deniger Exp $
   */
  public static void main(final String[] _args) {
    final String nom = (_args.length > 0 ? _args[0] : CDodico.generateName("::sinavi2::ICalculSinavi2"));
    // Cas particulier : il s'agit de creer un serveur de calcul dans une jvm donne
    // Cette M�thode n'est pas a imiter. If faut utiliser Boony pour creer des objet corba.
    CDodico.rebind(nom, UsineLib.createService(DCalculSinavi2.class));
    System.out.println("Sinavi2 server running... ");
    System.out.println("Name: " + nom);
    System.out.println("Date: " + new Date());
  }

}