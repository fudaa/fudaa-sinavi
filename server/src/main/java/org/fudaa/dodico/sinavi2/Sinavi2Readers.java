/*
 * @creation 6 avr. 2006
 * @modification $Date: 2006-09-19 14:45:56 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.sinavi2;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.ProgressionInterface;

import org.fudaa.dodico.corba.sinavi2.*;

import org.fudaa.dodico.fortran.FortranReader;

/**
 * @author fred deniger
 * @version $Id: Sinavi2Readers.java,v 1.2 2006-09-19 14:45:56 deniger Exp $
 */
public final class Sinavi2Readers {

  private Sinavi2Readers() {}

  /**
   * Methode utilis� pour v�rifier la l'�criture des fichier.
   */

  public static void lireParametres(final IParametresSinavi2 _param, final File _rep, final String _nomPrj,
      final ProgressionInterface _prog, final CtuluAnalyze _analyze) {
    final String cheminFic = /* _rep+ */_nomPrj;
    _param.parametresBateau(lireFichierBateaux(cheminFic));
    _param.parametresBief(lireFichierBiefs(cheminFic));
    _param.parametresEcluse(lireFichierEcluses(cheminFic));
    _param.parametresVitesse(lireFichierVitesses(cheminFic));
    _param.parametresTrematage(lireFichierTrematages(cheminFic));
    _param.parametresCroisement(lireFichierCroisements(cheminFic));
    _param.parametresManoeuvre(lireFichierManoeuvres(cheminFic));
    _param.parametresTrajet(lireFichierTrajet(cheminFic));
  }

  public static SParametresBateau[] lireFichierBateaux(final String nomFichier) {
    try {
      // SParametresBateau[] bateaux_ = new SParametresBateau[12];
      final String nomFic = nomFichier + ".nav";
      System.out.println("Lecture de " + nomFic);
      final FortranReader fr = new FortranReader(new FileReader(nomFic));

      /** List l=new ArrayList(Arrays.asList(_VSParametresBateaux));pb!!!! */
      final List l = new ArrayList();
      // List l=new ArrayList(Arrays.asList(SParametresBateau[l.size()]));
      // bateaux_);//couille
      boolean eof = false;
      /*
       * for(Iterator it=l.iterator();it.hasNext();it.next()){ }
       */

      /*
       * Vector iterations= new Vector(); Vector iteration= new Vector();
       */
      final int[] fmt = { 40, 101 };
      int i = 0;
      while (!eof) {
        fr.readFields(fmt);
        if (!fr.stringField(0).equals("")) {
          // Copie d'une ligne correspondant � un navire dans une instance de SResultatsBateauGenere
          final SParametresBateau bateau = new SParametresBateau();
          bateau.identification = fr.stringField(0);

          /*
           * System.out.println(fr.stringField(0)); System.out.println(bateau.identification);
           * System.out.println("test");
           */
          fr.readFields(fmt);
          bateau.longueur = fr.doubleField(0);
          fr.readFields(fmt);
          bateau.largeur = fr.doubleField(0);
          fr.readFields(fmt);
          bateau.tirantDeau = fr.doubleField(0);
          fr.readFields(fmt);

          bateau.dureeGeneAdmissible = fr.doubleField(0);
          fr.readFields(fmt);
          bateau.debutNavigation = fr.doubleField(0);
          fr.readFields(fmt);
          bateau.finNavigation = fr.doubleField(0);
          l.add(i, bateau);
          i++;
        } else {
          eof = true;
          final SParametresBateau[] s = (SParametresBateau[]) l.toArray(new SParametresBateau[l.size()]);
          return s;
        }
      }
    } catch (final Exception ex) {
      // CDodico.exception(DResultatsSinaviBen.class, ex);
      ex.printStackTrace();

    }
    return null;
  }

  public static SParametresBief[] lireFichierBiefs(final String nomFichier) {
    try {
      // SParametresBateau[] bateaux_ = new SParametresBateau[12];
      final String nomFic = nomFichier + ".bie";
      System.out.println("Lecture de " + nomFic);
      final FortranReader fr = new FortranReader(new FileReader(nomFic));

      /** List l=new ArrayList(Arrays.asList(_VSParametresBateaux));pb!!!! */
      final List l = new ArrayList();
      // List l=new ArrayList(Arrays.asList(SParametresBateau[l.size()]));
      // bateaux_);//couille
      boolean eof = false;
      /*
       * for(Iterator it=l.iterator();it.hasNext();it.next()){ }
       */

      /*
       * Vector iterations= new Vector(); Vector iteration= new Vector();
       */
      final int[] fmt = { 40, 100 };
      int i = 0;
      while (!eof) {
        fr.readFields(fmt);
        if (!fr.stringField(0).equals("")) {
          // Copie d'une ligne correspondant � un navire dans une instance de SResultatsBateauGenere
          final SParametresBief bief = new SParametresBief();
          bief.identification = fr.stringField(0);

          /*
           * System.out.println(fr.stringField(0)); System.out.println(bief.identification); System.out.println("test");
           */
          fr.readFields(fmt);
          bief.longueur = fr.doubleField(0);
          fr.readFields(fmt);
          bief.largeur = fr.doubleField(0);
          fr.readFields(fmt);
          bief.hauteur = fr.doubleField(0);
          fr.readFields(fmt);
          bief.vitesse = fr.doubleField(0);
          /*
           * fr.readFields(fmt); bief.debutNavigation =fr.doubleField(0); fr.readFields(fmt); bief.finNavigation
           * =fr.doubleField(0);
           */
          l.add(i, bief);
          i++;
        } else {
          eof = true;
          final SParametresBief[] s = (SParametresBief[]) l.toArray(new SParametresBief[l.size()]);
          return s;
        }
      }
    } catch (final Exception ex) {
      // CDodico.exception(DResultatsSinaviBen.class, ex);
      ex.printStackTrace();
    }
    return null;
  }

  public static SParametresEcluse[] lireFichierEcluses(final String nomFichier) {
    try {
      // SParametresBateau[] bateaux_ = new SParametresBateau[12];
      final String nomFic = nomFichier + ".ouv";
      System.out.println("Lecture de " + nomFic);
      final FortranReader fr = new FortranReader(new FileReader(nomFic));

      /** List l=new ArrayList(Arrays.asList(_VSParametresBateaux));pb!!!! */
      final List l = new ArrayList();
      // List l=new ArrayList(Arrays.asList(SParametresBateau[l.size()]));
      // bateaux_);//couille
      boolean eof = false;
      /*
       * for(Iterator it=l.iterator();it.hasNext();it.next()){ }
       */

      /*
       * Vector iterations= new Vector(); Vector iteration= new Vector();
       */
      final int[] fmt = { 40, 100 };
      int i = 0;
      while (!eof) {
        fr.readFields(fmt);
        if (!fr.stringField(0).equals("")) {
          // Copie d'une ligne correspondant � un navire dans une instance de SResultatsBateauGenere
          final SParametresEcluse ecluse = new SParametresEcluse();
          ecluse.identification = fr.stringField(0);
          fr.readFields(fmt);
          ecluse.longueur = fr.doubleField(0);
          fr.readFields(fmt);
          ecluse.largeur = fr.doubleField(0);
          fr.readFields(fmt);
          ecluse.profondeur = fr.doubleField(0);
          fr.readFields(fmt);
          ecluse.dureeBassineeDescendante = fr.doubleField(0);
          fr.readFields(fmt);
          ecluse.dureeBassineeDescendante = fr.doubleField(0);
          fr.readFields(fmt);
          /*
           * bief.debutNavigation =fr.doubleField(0); fr.readFields(fmt); bief.finNavigation =fr.doubleField(0);
           */
          l.add(i, ecluse);
          i++;
        } else {
          eof = true;
          final SParametresEcluse[] s = (SParametresEcluse[]) l.toArray(new SParametresEcluse[l.size()]);
          return s;
        }
      }
    } catch (final Exception ex) {
      // CDodico.exception(DResultatsSinaviBen.class, ex);
      ex.printStackTrace();
    }
    return null;
  }

  public static SParametresVitesses[] lireFichierVitesses(final String nomFichier) {
    try {
      // SParametresBateau[] bateaux_ = new SParametresBateau[12];
      final String nomFic = nomFichier + ".vites";
      System.out.println("Lecture de " + nomFic);
      final FortranReader fr = new FortranReader(new FileReader(nomFic));

      /** List l=new ArrayList(Arrays.asList(_VSParametresBateaux));pb!!!! */
      final List l = new ArrayList();
      // List l=new ArrayList(Arrays.asList(SParametresBateau[l.size()]));
      // bateaux_);//couille
      boolean eof = false;
      /*
       * for(Iterator it=l.iterator();it.hasNext();it.next()){ }
       */

      /*
       * Vector iterations= new Vector(); Vector iteration= new Vector();
       */
      final int[] fmt = { 21, 25, 10, 10 };
      int i = 0;
      while (!eof) {
        fr.readFields(fmt);
        if (!fr.stringField(0).equals("")) {
          // Copie d'une ligne correspondant � un navire dans une instance de SResultatsBateauGenere
          final SParametresVitesses vitesse = new SParametresVitesses();
          vitesse.bateau = fr.stringField(0);
          vitesse.bief = fr.stringField(1);
          vitesse.vitesseDesMontants = fr.doubleField(2);
          vitesse.vitesseDesAvalants = fr.doubleField(3);
          l.add(i, vitesse);
          i++;
        } else {
          eof = true;
          final SParametresVitesses[] s = (SParametresVitesses[]) l.toArray(new SParametresVitesses[l.size()]);
          return s;
        }
      }
    } catch (final Exception ex) {
      // CDodico.exception(DResultatsSinaviBen.class, ex);
      ex.printStackTrace();
    }
    return null;
  }

  public static SParametresTrematages[] lireFichierTrematages(final String nomFichier) {
    try {
      // SParametresBateau[] bateaux_ = new SParametresBateau[12];
      final String nomFic = nomFichier + ".tremat";
      System.out.println("Lecture de " + nomFic);
      final FortranReader fr = new FortranReader(new FileReader(nomFic));

      /** List l=new ArrayList(Arrays.asList(_VSParametresBateaux));pb!!!! */
      final List l = new ArrayList();
      // List l=new ArrayList(Arrays.asList(SParametresBateau[l.size()]));
      // bateaux_);//couille
      boolean eof = false;
      /*
       * for(Iterator it=l.iterator();it.hasNext();it.next()){ }
       */

      /*
       * Vector iterations= new Vector(); Vector iteration= new Vector();
       */
      final int[] fmt = { 20, 20, 20, 1 };
      int i = 0;
      while (!eof) {
        fr.readFields(fmt);
        if (!fr.stringField(0).equals("")) {
          // Copie d'une ligne correspondant � un navire dans une instance de SResultatsBateauGenere
          final SParametresTrematages trematage = new SParametresTrematages();
          trematage.bief = fr.stringField(0);
          trematage.type1 = fr.stringField(1);
          trematage.type2 = fr.stringField(2);
          final String ouiNon = fr.stringField(3);
          if (ouiNon == "O") {
            trematage.ouiNon = true;
          } else {
            trematage.ouiNon = false;
          }
          /*
           * trematage.bateau=fr.stringField(0); trematage.bief=fr.stringField(1);
           * trematage.trematageDesMontants=fr.doubleField(2); trematage.trematageDesAvalants=fr.doubleField(3);
           */
          l.add(i, trematage);
          i++;
        } else {
          eof = true;
          final SParametresTrematages[] s = (SParametresTrematages[]) l.toArray(new SParametresTrematages[l.size()]);
          return s;
        }
      }
    } catch (final Exception ex) {
      // CDodico.exception(DResultatsSinaviBen.class, ex);
      ex.printStackTrace();
    }
    return null;
  }

  public static SParametresCroisements[] lireFichierCroisements(final String nomFichier) {
    try {
      // SParametresBateau[] bateaux_ = new SParametresBateau[12];
      final String nomFic = nomFichier + ".croist";
      System.out.println("Lecture de " + nomFic);
      final FortranReader fr = new FortranReader(new FileReader(nomFic));

      /** List l=new ArrayList(Arrays.asList(_VSParametresBateaux));pb!!!! */
      final List l = new ArrayList();
      // List l=new ArrayList(Arrays.asList(SParametresBateau[l.size()]));
      // bateaux_);//couille
      boolean eof = false;
      /*
       * for(Iterator it=l.iterator();it.hasNext();it.next()){ }
       */

      /*
       * Vector iterations= new Vector(); Vector iteration= new Vector();
       */
      final int[] fmt = { 20, 20, 20, 1 };
      int i = 0;
      while (!eof) {
        fr.readFields(fmt);
        if (!fr.stringField(0).equals("")) {
          // Copie d'une ligne correspondant � un navire dans une instance de SResultatsBateauGenere
          final SParametresCroisements croisement = new SParametresCroisements();
          croisement.bief = fr.stringField(0);
          croisement.type1 = fr.stringField(1);
          croisement.type2 = fr.stringField(2);
          final String ouiNon = fr.stringField(3);
          if (ouiNon == "O") {
            croisement.ouiNon = true;
          } else {
            croisement.ouiNon = false;
          }

          l.add(i, croisement);
          i++;
        } else {
          eof = true;
          final SParametresCroisements[] s = (SParametresCroisements[]) l.toArray(new SParametresCroisements[l.size()]);
          return s;
        }
      }
    } catch (final Exception ex) {
      // CDodico.exception(DResultatsSinaviBen.class, ex);
      ex.printStackTrace();
    }
    return null;
  }

  public static SParametresManoeuvres[] lireFichierManoeuvres(final String nomFichier) {
    try {
      // SParametresBateau[] bateaux_ = new SParametresBateau[12];
      final String nomFic = nomFichier + ".man";
      System.out.println("Lecture de " + nomFic);
      final FortranReader fr = new FortranReader(new FileReader(nomFic));

      /** List l=new ArrayList(Arrays.asList(_VSParametresBateaux));pb!!!! */
      final List l = new ArrayList();
      // List l=new ArrayList(Arrays.asList(SParametresBateau[l.size()]));
      // bateaux_);//couille
      boolean eof = false;
      /*
       * for(Iterator it=l.iterator();it.hasNext();it.next()){ }
       */

      /*
       * Vector iterations= new Vector(); Vector iteration= new Vector();
       */
      final int[] fmt = { 20, 20, 10, 10 };
      int i = 0;
      while (!eof) {
        fr.readFields(fmt);
        if (!fr.stringField(0).equals("")) {
          // Copie d'une ligne correspondant � un navire dans une instance de SResultatsBateauGenere
          final SParametresManoeuvres manoeuvre = new SParametresManoeuvres();
          manoeuvre.ecluse = fr.stringField(0);
          manoeuvre.type = fr.stringField(1);
          manoeuvre.entree = fr.doubleField(2);
          manoeuvre.sortie = fr.doubleField(3);
          l.add(i, manoeuvre);
          i++;
        } else {
          eof = true;
          final SParametresManoeuvres[] s = (SParametresManoeuvres[]) l.toArray(new SParametresManoeuvres[l.size()]);
          return s;
        }
      }
    } catch (final Exception e) {
      // CDodico.exception(DResultatsSinaviBen.class, ex);
      e.printStackTrace();
    }
    return null;
  }

  public static void lireFichierGares(final String _nomFichier, final SParametresBief[] _biefs,
      final SParametresEcluse[] _ecluses) {// Non
    // test�e
    try {
      final String nomFic = _nomFichier + ".cnx";
      System.out.println("Lecture de" + nomFic);
      final FortranReader r = new FortranReader(new FileReader(nomFic));
      boolean eof = false;
      // initialisation
      System.out.println("debut initialisation");
      final int[] fmt = { 40, 100 };
      int numBief = 0;
      int numEcluse = 0;
      // int paramGare;
      final int nbBiefs = _biefs.length;
      final int nbEcluses = _ecluses.length;
      while (!eof) {
        r.readFields(fmt);
        if (!r.stringField(1).equals("")) {
          /* remplissage des biefs */
          if (numBief < nbBiefs) {
            _biefs[numBief].gareEnAmont = r.intField(0);
            r.readFields(fmt);
            _biefs[numBief].gareEnAval = r.intField(0);
            numBief++;
          } else if (numEcluse < nbEcluses) {
            _ecluses[numEcluse].gareEnAmont = r.intField(0);
            r.readFields(fmt);
            _ecluses[numEcluse].gareEnAval = r.intField(0);
            numEcluse++;
          } else {
            eof = true;
          }
        } else {
          eof = true;
          // return v;
        }
      }
    } catch (final IOException e) {
      e.printStackTrace();
    }
    // return null;

  }

  /** methode permettant de lire le fichier trajet. */
  public static SParametresTrajets[] lireFichierTrajet(final String nomFichier) {
    try {
      final String nomFic = nomFichier + ".trajet";
      System.out.println("Lecture de " + nomFic);
      final FortranReader r = new FortranReader(new FileReader(nomFic));
      final List ltrajet = new ArrayList();
      final List lheure = new ArrayList();
      final List lheureJour = new ArrayList();
      boolean eof = false;
      int d = 0;
      int j = 0;
      final int[] fmt = { 40, 101 };
      final int[] fmtE = { 2, 1, 37, 100 };
      final int[] fmtD = { 3, 3, 1, 33, 100 };
      int i = 0;
      while (!eof) {
        r.readFields(fmt);
        if (!r.stringField(0).equals("")) {
          final SParametresTrajets _trajet = new SParametresTrajets();
          _trajet.bateau = r.stringField(0);
          r.readFields(fmt);
          if (r.equals("A")) {
            _trajet.avalantMontant = true;
          } else {
            _trajet.avalantMontant = false;
          }
          r.readFields(fmt);
          _trajet.gareDepart = r.intField(0);
          r.readFields(fmt);
          _trajet.gareArrivee = r.intField(0);
          r.readFields(fmt);
          _trajet.typeDeLoi = r.intField(0);
          // r.readFields(fmt);
          if (_trajet.typeDeLoi == 0) {
            r.readFields(fmtE);
            final SHeure hdeb = new SHeure();
            final SHeure hfin = new SHeure();
            final SParametresLoiE loiE = new SParametresLoiE();
            hdeb.heure = r.intField(0);
            hdeb.minutes = r.intField(2);
            r.readFields(fmtE);
            hfin.heure = r.intField(0);
            hfin.minutes = r.intField(2);
            r.readFields(fmtE);
            // loiE.heureDebutGeneration=hdeb;
            // loiE.heureFinGeneration=hfin;
            // r.readFields(fmtE);
            loiE.ordreDeLaLoi = r.intField(0);
            r.readFields(fmt);
            loiE.nbreBateauxAttendus = r.intField(0);
            loiE.heureDebutGeneration = hdeb;
            loiE.heureFinGeneration = hfin;
            _trajet.loiE = loiE;
            ltrajet.add(i, _trajet);
            i++;
          }

          if (_trajet.typeDeLoi == 1) {
            r.readFields(fmtD);
            final SParametresLoiD loiD = new SParametresLoiD();
            while (!r.stringField(4).equals("| Fin des entr�es pour la loi deterministe")) {
              // System.out.print("r.stringField(0)");
              final SDateHeure dateheure = new SDateHeure();
              final SHeure h = new SHeure();
              dateheure.date = r.intField(0);
              h.heure = r.intField(1);
              h.minutes = r.intField(3);
              dateheure.heure = h;
              lheure.add(d, dateheure);
              d++;
              r.readFields(fmtD);
            }
            final SDateHeure[] dateheures = (SDateHeure[]) lheure.toArray(new SDateHeure[lheure.size()]);
            // System.out.println(dateheures.length);
            loiD.dateHeure = dateheures;
            _trajet.loiD = loiD;
            ltrajet.add(i, _trajet);
            i++;
          }

          if (_trajet.typeDeLoi == 2) {
            r.readFields(fmtE);
            final SParametresLoiJ loiJ = new SParametresLoiJ();
            while (!r.stringField(4).equals("| Fin des entr�es pour la loi journali�re")) {
              final SHeure h = new SHeure();
              h.heure = r.intField(0);
              h.minutes = r.intField(2);
              lheureJour.add(j, h);
              j++;
              r.readFields(fmtE);
            }
            final SHeure[] heures = (SHeure[]) lheureJour.toArray(new SHeure[lheureJour.size()]);
            loiJ.heureJ = heures;
            _trajet.loiJ = loiJ;
            ltrajet.add(i, _trajet);
            i++;
          }
        } else {
          eof = true;
          final SParametresTrajets[] _tr = (SParametresTrajets[]) ltrajet
              .toArray(new SParametresTrajets[ltrajet.size()]);
          return _tr;
        }
      }
    } catch (final IOException e) {
      // CDodico.exception(ex);
      e.printStackTrace();
    }

    return null;
  }

  public static void lireFichierReseauImport(final String _nomFichier, final ArrayList _biefs, final ArrayList _ecluses) {
    try {
      final String nomFic = _nomFichier;
      System.out.println("Lecture de" + nomFic);
      final FortranReader r = new FortranReader(new FileReader(nomFic));
      boolean eof = false;
      // initialisation
      System.out.println("debut initialisation");
      final int[] fmt = { 40, 100 };
      // List listebiefs=new ArrayList();
      // List listeEcluses=new ArrayList();
      int numBief = 0;
      int numEcluse = 0;
      String test;
      String id;
      boolean trouve = false;
      while (!eof) {
        r.readFields(fmt);
        id = r.stringField(0);
        test = r.stringField(1);
        if (!r.stringField(0).equals("")) {
          System.out.println("la valeur de test est :" + test);
          if (test.equalsIgnoreCase("| Fin des biefs")) {
            System.out.println("------>" + "j'ai fini bief");
            trouve = true;
          }
          // else if(r.stringField(1).equals(" | Fin des biefs")==false && trouve==false){
          else if (!trouve) {
            final SParametresBief bief = new SParametresBief();
            bief.identification = r.stringField(0);
            r.readFields(fmt);
            bief.longueur = r.doubleField(0);
            r.readFields(fmt);
            bief.largeur = r.doubleField(0);
            r.readFields(fmt);
            bief.hauteur = r.doubleField(0);
            r.readFields(fmt);
            bief.vitesse = r.doubleField(0);
            r.readFields(fmt);
            bief.gareEnAmont = r.intField(0);
            r.readFields(fmt);
            bief.gareEnAval = r.intField(0);
            // listebiefs.add(numBief,bief);
            _biefs.add(numBief, bief);
            numBief++;
            trouve = false;
          }
          if (trouve) {
            final SParametresEcluse ecluse = new SParametresEcluse();
            if (numEcluse == 0) {
              r.readFields(fmt);
              ecluse.identification = r.stringField(0);
            } else {
              System.out.println("id " + id);
              ecluse.identification = id;
            }
            r.readFields(fmt);
            ecluse.longueur = r.doubleField(0);
            r.readFields(fmt);
            ecluse.largeur = r.doubleField(0);
            r.readFields(fmt);
            ecluse.profondeur = r.doubleField(0);
            r.readFields(fmt);
            ecluse.dureeBassineeMontante = r.doubleField(0);
            r.readFields(fmt);
            ecluse.dureeBassineeDescendante = r.doubleField(0);
            r.readFields(fmt);
            ecluse.hauteurChuteDEau = r.doubleField(0);
            r.readFields(fmt);
            ecluse.dureeManoeuvresEnEntree = r.doubleField(0);
            r.readFields(fmt);
            ecluse.dureeManoeuvresEnSortie = r.doubleField(0);
            r.readFields(fmt);
            ecluse.gareEnAmont = r.intField(0);
            r.readFields(fmt);
            ecluse.gareEnAval = r.intField(0);
            // listeEcluses.add(numEcluse,ecluse);
            _ecluses.add(numEcluse, ecluse);
            numEcluse++;

          }

          /*
           * else eof=true;
           */

        } else {
          eof = true;
          // System.out.println("taillle liste bief "+ listebiefs.size());
          // _biefs=new SParametresBief[listebiefs.size()];
          // _ecluses=new SParametresEcluse[listeEcluses.size()];
          // System.out.println("taille dodico bief "+ _biefs.length);
          // _biefs=(SParametresBief[]) listebiefs.toArray(new SParametresBief[listebiefs.size()]);
          // _ecluses=(SParametresEcluse[]) listeEcluses.toArray(new SParametresEcluse[listeEcluses.size()]);
          System.out.println("taille dodico bief " + _biefs.size());
        }
      }
    } catch (final IOException e) {
      e.printStackTrace();
    }
    // return null;

  }

  public static SParametresBateau[] lireFichierBateauxImport(final String _nomFichier) {
    try {
      final String nomFic = _nomFichier;
      System.out.println("Lecture de " + nomFic);
      final FortranReader fr = new FortranReader(new FileReader(nomFic));

      /** List l=new ArrayList(Arrays.asList(_VSParametresBateaux));pb!!!! */
      final List l = new ArrayList();
      boolean eof = false;
      final int[] fmt = { 40, 101 };
      int i = 0;
      while (!eof) {
        fr.readFields(fmt);
        if (!fr.stringField(0).equals("")) {
          final SParametresBateau bateau = new SParametresBateau();
          bateau.identification = fr.stringField(0);
          fr.readFields(fmt);
          bateau.longueur = fr.doubleField(0);
          fr.readFields(fmt);
          bateau.largeur = fr.doubleField(0);
          fr.readFields(fmt);
          bateau.tirantDeau = fr.doubleField(0);
          fr.readFields(fmt);
          bateau.dureeGeneAdmissible = fr.doubleField(0);
          fr.readFields(fmt);
          bateau.debutNavigation = fr.doubleField(0);
          fr.readFields(fmt);
          bateau.finNavigation = fr.doubleField(0);
          l.add(i, bateau);
          i++;
        } else {
          eof = true;
          final SParametresBateau[] s = (SParametresBateau[]) l.toArray(new SParametresBateau[l.size()]);
          return s;
        }
      }
    } catch (final Exception ex) {
      // CDodico.exception(DResultatsSinaviBen.class, ex);
      ex.printStackTrace();

    }
    return null;
  }

  public static SParametresBateau[] lireFichierBateauxImport2(final String nomFichier) {
    try {
      final String nomFic = nomFichier;
      System.out.println("Lecture de " + nomFic);
      final FortranReader fr = new FortranReader(new FileReader(nomFic));

      /** List l=new ArrayList(Arrays.asList(_VSParametresBateaux));pb!!!! */
      final List l = new ArrayList();
      boolean eof = false;
      final int[] fmt = { 40, 101 };
      int i = 0;
      while (!eof) {
        fr.readFields(fmt);
        if (!fr.stringField(0).equals("")) {
          final SParametresBateau bateau = new SParametresBateau();
          bateau.identification = fr.stringField(0);
          fr.readFields(fmt);
          bateau.longueur = fr.doubleField(0);
          fr.readFields(fmt);
          bateau.largeur = fr.doubleField(0);
          fr.readFields(fmt);
          bateau.tirantDeau = fr.doubleField(0);
          fr.readFields(fmt);

          bateau.dureeGeneAdmissible = fr.doubleField(0);
          fr.readFields(fmt);
          bateau.debutNavigation = fr.doubleField(0);
          fr.readFields(fmt);
          bateau.finNavigation = fr.doubleField(0);
          fr.readFields(fmt);
          bateau.vitesseDescendantParDefaut = fr.doubleField(0);
          fr.readFields(fmt);
          bateau.vitesseMontantParDefaut = fr.doubleField(0);
          l.add(i, bateau);
          i++;
        } else {
          eof = true;
          final SParametresBateau[] s = (SParametresBateau[]) l.toArray(new SParametresBateau[l.size()]);
          return s;
        }
      }
    } catch (final Exception ex) {
      // CDodico.exception(DResultatsSinaviBen.class, ex);
      ex.printStackTrace();

    }
    return null;
  }

  public static SParametresBief[] lireFichierBiefsImport(final String nomFichier) {
    try {
      final String nomFic = nomFichier;
      System.out.println("Lecture de " + nomFic);
      final FortranReader fr = new FortranReader(new FileReader(nomFic));

      final List l = new ArrayList();
      boolean eof = false;
      final int[] fmt = { 40, 100 };
      int i = 0;
      while (!eof) {
        fr.readFields(fmt);
        if (!fr.stringField(0).equals("")) {
          final SParametresBief bief = new SParametresBief();
          bief.identification = fr.stringField(0);
          fr.readFields(fmt);
          bief.longueur = fr.doubleField(0);
          fr.readFields(fmt);
          bief.largeur = fr.doubleField(0);
          fr.readFields(fmt);
          bief.hauteur = fr.doubleField(0);
          fr.readFields(fmt);
          bief.vitesse = fr.doubleField(0);
          l.add(i, bief);
          i++;
        } else {
          eof = true;
          final SParametresBief[] s = (SParametresBief[]) l.toArray(new SParametresBief[l.size()]);
          return s;
        }
      }
    } catch (final Exception ex) {
      // CDodico.exception(DResultatsSinaviBen.class, ex);
      ex.printStackTrace();
    }
    return null;
  }

  public static SParametresEcluse[] lireFichierEclusesImport(final String nomFichier) {
    try {
      final String nomFic = nomFichier;
      System.out.println("Lecture de " + nomFic);
      final FortranReader fr = new FortranReader(new FileReader(nomFic));

      /** List l=new ArrayList(Arrays.asList(_VSParametresBateaux));pb!!!! */
      final List l = new ArrayList();
      boolean eof = false;
      final int[] fmt = { 40, 100 };
      int i = 0;
      while (!eof) {
        fr.readFields(fmt);
        if (!fr.stringField(0).equals("")) {
          final SParametresEcluse ecluse = new SParametresEcluse();
          ecluse.identification = fr.stringField(0);
          fr.readFields(fmt);
          ecluse.longueur = fr.doubleField(0);
          fr.readFields(fmt);
          ecluse.largeur = fr.doubleField(0);
          fr.readFields(fmt);
          ecluse.profondeur = fr.doubleField(0);
          fr.readFields(fmt);
          String s = fr.stringField(0);
          System.out.println("ancien :" + s);
          s = s.replaceAll(". ", ".0");
          System.out.println("apr�s :" + s);
          ecluse.dureeBassineeMontante = Double.parseDouble(s);
          fr.readFields(fmt);
          s = fr.stringField(0);
          s = s.replaceAll(". ", ".0");
          ecluse.dureeBassineeDescendante = Double.parseDouble(s);
          l.add(i, ecluse);
          i++;
        } else {
          eof = true;
          final SParametresEcluse[] s = (SParametresEcluse[]) l.toArray(new SParametresEcluse[l.size()]);
          return s;
        }
      }
    } catch (final Exception ex) {
      // CDodico.exception(DResultatsSinaviBen.class, ex);
      ex.printStackTrace();
    }
    return null;
  }

  public static SParametresEcluse[] lireFichierEclusesImport2(final String _nomFichier) {
    try {
      final String nomFic = _nomFichier;
      System.out.println("Lecture de " + nomFic);
      final FortranReader fr = new FortranReader(new FileReader(nomFic));
      final List l = new ArrayList();
      boolean eof = false;
      final int[] fmt = { 40, 100 };
      int i = 0;
      while (!eof) {
        fr.readFields(fmt);
        if (!fr.stringField(0).equals("")) {
          final SParametresEcluse ecluse = new SParametresEcluse();
          ecluse.identification = fr.stringField(0);
          fr.readFields(fmt);
          ecluse.longueur = fr.doubleField(0);
          fr.readFields(fmt);
          ecluse.largeur = fr.doubleField(0);
          fr.readFields(fmt);
          ecluse.profondeur = fr.doubleField(0);
          fr.readFields(fmt);
          ecluse.dureeBassineeMontante = fr.doubleField(0);
          fr.readFields(fmt);
          ecluse.dureeBassineeDescendante = fr.doubleField(0);
          fr.readFields(fmt);
          ecluse.hauteurChuteDEau = fr.doubleField(0);
          fr.readFields(fmt);
          ecluse.dureeManoeuvresEnEntree = fr.doubleField(0);
          fr.readFields(fmt);
          ecluse.dureeManoeuvresEnSortie = fr.doubleField(0);
          l.add(i, ecluse);
          i++;
        } else {
          eof = true;
          final SParametresEcluse[] s = (SParametresEcluse[]) l.toArray(new SParametresEcluse[l.size()]);
          return s;
        }
      }
    } catch (final Exception ex) {
      // CDodico.exception(DResultatsSinaviBen.class, ex);
      ex.printStackTrace();
    }
    return null;
  }

}
