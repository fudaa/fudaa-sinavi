/*
 * @creation 12 mars 2006
 * @modification $Date: 2006-09-19 14:45:56 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.sinavi2;

import java.io.EOFException;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.memoire.fu.Fu;
import com.memoire.fu.FuLog;

import org.fudaa.ctulu.fileformat.FortranLib;

import org.fudaa.dodico.corba.sinavi2.IResultatsSinavi2;
import org.fudaa.dodico.corba.sinavi2.IResultatsSinavi2Operations;
import org.fudaa.dodico.corba.sinavi2.SResultatConsommationDEau;
import org.fudaa.dodico.corba.sinavi2.SResultatGenerationBateau;
import org.fudaa.dodico.corba.sinavi2.SResultatHistorique;

import org.fudaa.dodico.calcul.DResultats;
import org.fudaa.dodico.fortran.FortranReader;

/**
 * Cette classe a pour but de lire les fichiers r�sultats. - gen pour la g�nration - his pour l'historique - cons pour
 * la consommation : non encore d�velopp� par Alain Pourplanche
 *
 * @author stagiaires
 * @version $Id: DResultatsSinavi2.java,v 1.13 2006-09-19 14:45:56 deniger Exp $
 */

public class DResultatsSinavi2 extends DResultats implements IResultatsSinavi2, IResultatsSinavi2Operations {
  SResultatGenerationBateau[] resultatGenerationBateaux_;
  SResultatHistorique[] resultatHistorique_;
  SResultatConsommationDEau[] resultatConsommationDEau_;

  public SResultatGenerationBateau[] parametresResultatGenerationBateau() {
    return resultatGenerationBateaux_;
  }

  public void parametresResultatGenerationBateau(final SResultatGenerationBateau[] _newParametresResultatGenerationBateau) {
    resultatGenerationBateaux_ = _newParametresResultatGenerationBateau;

  }

  public SResultatHistorique[] parametresHistorique() {

    return resultatHistorique_;
  }

  public void parametresHistorique(final SResultatHistorique[] _newParametresHistorique) {
    resultatHistorique_ = _newParametresHistorique;

  }

  public static SResultatGenerationBateau[] lireFichierGeneration(final String _nomFichier) {
    final List l = new ArrayList();
    final String nomFic = _nomFichier + ".gen";// /"/home/maneuvrier/fudaa/fudaa/serveurs/sinavi2/test10.gen";
    FuLog.debug("Lecture de " + nomFic);
    final File f = new File(nomFic);
    if (!f.exists()) {
      FuLog.error("Le fichier " + f.getAbsolutePath() + " est introuvable");
      return null;
    }
    try {
      final FortranReader fr = new FortranReader(new FileReader(f));
      boolean eof = false;
      int i = 0;
      while (!eof) {
        fr.readFields();
        if (fr.getNumberOfFields() > 2) {
          // Copie d'une ligne correspondant � un navire dans une instance de SResultatsBateauGenere
          final SResultatGenerationBateau gen = new SResultatGenerationBateau();

          gen.jour = fr.intField(0);
          // System.out.println("++++++"+ gen.jour);
          gen.heure = fr.intField(1);
          gen.minute = fr.intField(2);
          gen.seconde = fr.intField(3);
          final String s = fr.stringField(4);
          if (s.equals("A")) {
            gen.avalantMontant = true;
          } else {
            gen.avalantMontant = false;
          }
          gen.numeroTypeBateau = fr.intField(5);
          gen.numeroGareDepart = fr.intField(6);
          gen.numeroGareSortie = fr.intField(7);
          l.add(i, gen);
          i++;
        } else {
          eof = true;
          final SResultatGenerationBateau[] s = (SResultatGenerationBateau[]) l.toArray(new SResultatGenerationBateau[l
              .size()]);
          return s;
        }
      }
    } catch (final EOFException e) {} catch (final IOException e) {
      e.printStackTrace();
    }

    return (SResultatGenerationBateau[]) l.toArray(new SResultatGenerationBateau[l.size()]);
  }

  public static SResultatHistorique[] lireFichierHistorique(final String _nomFichier) {
    final List l = new ArrayList();
    final String nomFic = _nomFichier + ".his";
    FortranReader fr = null;
    try {
      FuLog.debug("Lecture de " + nomFic);
      final File f = new File(nomFic);
      if (!f.exists()) {
        FuLog.error("Le fichier " + f.getAbsolutePath() + " est introuvable");
        return null;
      }
      fr = new FortranReader(new FileReader(f));
      // int[] fmt={ 4,3,3,3,3,4,5,5 };
      // int[] fmt2={100};
      try {
        fr.readFields();
        while (fr.getNumberOfFields() > 2) {
          final SResultatHistorique his = new SResultatHistorique();
          his.numeroBateau = fr.intField(0);
          // FuLog.debug("++++++"+his.numeroBateau);
          his.numeroTypeBateau = fr.intField(1);
          // FuLog.debug("++++++"+his.numeroTypeBateau );
          his.avalantMontant = Sinavi2Helper.isAval(fr.stringField(2));
          // FuLog.debug("++++++"+his.avalantMontant);
          his.elementAFranchir = fr.stringField(3);
          // FuLog.debug("++++++"+his.elementAFranchir);
          his.numeroElement = fr.intField(4);
          // FuLog.debug("++++++"+his.numeroElement);
          his.heureEntree = fr.doubleField(5);
          // FuLog.debug("++++++"+his.heureEntree );
          his.heureSortie = fr.doubleField(6);
          // FuLog.debug("++++++"+his.heureSortie );
          his.attente = fr.doubleField(7);
          // FuLog.debug("++++++"+his.attente);
          l.add(his);
          fr.readFields();
        }
      } catch (final EOFException _evt) {
        if (Fu.DEBUG && FuLog.isDebug()) {
          FuLog.debug("DSI: fin du fichier " + nomFic);
        }

      }
    } catch (final Exception e) {
      FuLog.error("DSI: lecture du fichier " + nomFic, e);
    } finally {
      if (fr != null) {
        FortranLib.close(fr);
      }
    }

    return (SResultatHistorique[]) l.toArray(new SResultatHistorique[l.size()]);
  }

  public SResultatConsommationDEau[] parametresConsommationDEau() {

    return resultatConsommationDEau_;
  }

  public void parametresConsommationDEau(final SResultatConsommationDEau[] _newParametresConsommationDEau) {
    resultatConsommationDEau_ = _newParametresConsommationDEau;

  }

  public static SResultatConsommationDEau[] lireFichierConsommationDEau(final String _nomFichier) {
    final List l = new ArrayList();

    try {
      final String nomFic = _nomFichier + ".cons"/* "/home/ka/fudaa/fudaa_devel/dodico/dodico_serveurs/sinavi2/test1.his" */;
      System.out.println("Lecture de " + nomFic);
      final FortranReader fr = new FortranReader(new FileReader(nomFic));
      boolean eof = false;
      int i = 0;
      final int[] fmt = { 40, 101 };
      while (!eof) {
        // fr.readFields();
        fr.readFields(fmt);
        // System.out.println("nbligne"+fr.getNumberOfFields());
        if (fr.getNumberOfFields() > 1) {
          final SResultatConsommationDEau cons = new SResultatConsommationDEau();
          cons.numeroEcluse = fr.intField(0);
          fr.readFields(fmt);
          final String s = fr.stringField(0);
          if (s.equals("A")) {
            cons.sens = true;
          } else {
            cons.sens = false;
          }
          fr.readFields(fmt);
          cons.dateHeureOuverture = fr.doubleField(0);
          fr.readFields(fmt);
          cons.dateHeureFermeture = fr.doubleField(0);
          fr.readFields(fmt);
          cons.nombreBateaux = fr.intField(0);
          fr.readFields(fmt);
          cons.typeBateau = fr.intField(0);
          l.add(i, cons);
          i++;
        } else {
          eof = true;
          final SResultatConsommationDEau[] s = (SResultatConsommationDEau[]) l.toArray(new SResultatConsommationDEau[l
              .size()]);
          return s;
        }
      }

    } catch (final Exception e) {

      e.printStackTrace();
      final SResultatConsommationDEau[] s = (SResultatConsommationDEau[]) l.toArray(new SResultatConsommationDEau[l.size()]);
      return s;
    }

    return null;
  }

}
