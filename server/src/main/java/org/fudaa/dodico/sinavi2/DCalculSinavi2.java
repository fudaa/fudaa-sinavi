/*
 * @creation 12 mars 2006
 * @modification $Date: 2006-09-19 14:45:56 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.sinavi2;

import java.io.File;
import java.io.FileReader;
import java.io.LineNumberReader;

import com.memoire.fu.Fu;
import com.memoire.fu.FuLib;
import com.memoire.fu.FuLog;

import org.fudaa.ctulu.CtuluLibFile;
import org.fudaa.ctulu.CtuluLibString;

import org.fudaa.dodico.corba.objet.IConnexion;
import org.fudaa.dodico.corba.sinavi2.ICalculSinavi2;
import org.fudaa.dodico.corba.sinavi2.ICalculSinavi2Operations;
import org.fudaa.dodico.corba.sinavi2.IParametresSinavi2;
import org.fudaa.dodico.corba.sinavi2.IParametresSinavi2Helper;
import org.fudaa.dodico.corba.sinavi2.IResultatsSinavi2;
import org.fudaa.dodico.corba.sinavi2.IResultatsSinavi2Helper;

import org.fudaa.dodico.calcul.DCalcul;
import org.fudaa.dodico.objet.CDodico;
import org.fudaa.dodico.objet.CExec;

/**
 * @author stagiaires
 * @version $Id: DCalculSinavi2.java,v 1.17 2006-09-19 14:45:56 deniger Exp $
 */
public class DCalculSinavi2 extends DCalcul implements ICalculSinavi2, ICalculSinavi2Operations {
  public DCalculSinavi2() {
    super();
    setFichiersExtensions(new String[] { ".nom", ".nav", ".bie", ".ouv", ".cnx", ".vites", ".trajet", ".croist",
        ".tremat", ".man", ".gen", ".his" });
  }

  public String description() {
    return "sinavi2, serveur de calcul de sivani " + super.description();
  }

  public void calcul(final IConnexion _c) {
    final String[] exts = extensions_;
    final String path = cheminServeur();
    /*
     * File f=TestIO.getFile(TestDParametresSinavi.class,"sinavi2.test"); CtuluAnalyze an=new CtuluAnalyze();
     */
    // verifie la connexion et les interfaces des parametres et des resultats.
    if (!verifieConnexion(_c)) {
      return;
    }
    // System.out.println("++++++++++++++"
    // +IParametresSinavi2Helper.narrow(parametres(_c)).parametresBateau()[0].identification);
    final IParametresSinavi2 params = IParametresSinavi2Helper.narrow(parametres(_c));

    if (params == null) {
      CDodico.exceptionAxel(this, new Exception("params non definis (null)"));
    }
    final IResultatsSinavi2 results = IResultatsSinavi2Helper.narrow(resultats(_c));
    if (results == null) {
      CDodico.exceptionAxel(this, new Exception("results non definis (null)"));
    }

    String nomPrj = params.nomPrj();
    System.out.println("nom prj " + nomPrj);
    log(_c, "lancement du calcul");
    // les parametres neccessaires au lancement du code.

    try {

      final String os = System.getProperty("os.name");

      final String fichier = path + "sinavi2" + _c.numero();
      System.out.println("----------- " + path);
      //int indice = 0;
      /*
       * on recherche l'indice du dernier slash pour r�cup�rer le nom du projet car sinavi lit les fichiers de nom,
       * nomProjet + extension
       */
      nomPrj = CtuluLibFile.getSansExtension(nomPrj);
      nomPrj = FuLib.clean(nomPrj);
      if (Fu.DEBUG && FuLog.isDebug()) {
        FuLog.debug("DSI: path=" + path);
        FuLog.debug("DSI: projet=" + nomPrj);
        FuLog.debug("DSI: Appel pour =" + fichier);
      }
      // effacement des fichiers
      if (exts != null) {
        for (int i = exts.length - 1; i >= 0; i--) {
          final File f = new File(path + nomPrj + exts[i]);
          if (f.exists()) {
            f.delete();
          }
        }
      }
      Sinavi2Writers.ecritParametres(params, new File(path), nomPrj, null, null);

      final String[] cmd = new String[5];
      /**
       * on va appelerun script pour lancer le code de calcul : Celui ci se trouve dans le repertoire path voici les
       * arguments 1- nom du projet 2- donn�e pour afficher des lignes du his pour v�rifier le bon fonctionnement du
       * calcul, on affiche une ligne tous les 100 ici. 3- la dur�e de simulation en jours 4- Graine de g�n�ration :
       * utilis� pour le cot� al�atoire
       */

      if (os.startsWith("Windows")) {
        cmd[0] = new File(path, "launcher.bat").getAbsolutePath();

      } else {
        cmd[0] = new File(path, "launcher.sh").getAbsolutePath();
      }
      cmd[1] = nomPrj;
      cmd[2] = "100";
      cmd[3] = Integer.toString(params.parametresGeneration().dureeSimulation);
      cmd[4] = Integer.toString(params.parametresGeneration().graine);
      if (Fu.DEBUG && FuLog.isDebug()) {
        FuLog.debug("DSI: " + CtuluLibString.arrayToString(cmd, CtuluLibString.ESPACE));
      }
      // lancement de l'executable du calcul
      final CExec ex = new CExec();
      ex.setExecDirectory(new File(path));
      ex.setCommand(cmd);
      ex.exec();
      System.out.println("Fin du calcul");
      System.out.println("R�sultats de " + fichier + ".out");
      if (new File(fichier + ".out").exists()) {
        final LineNumberReader fr = new LineNumberReader(new FileReader(fichier + ".out"));
        String s = fr.readLine();
        while (s != null) {
          if (s.length() > 0) {
            System.out.println("out: " + s);
          }
          s = fr.readLine();
        }
      }
      // lecture des fichiers resultats.
      log(_c, "enregistrement des param�tres");
      results.parametresResultatGenerationBateau(DResultatsSinavi2.lireFichierGeneration(path + nomPrj));
      results.parametresHistorique(DResultatsSinavi2.lireFichierHistorique(path + nomPrj));
      /**
       * A d�comentariser lorsque le code de calcul �voluera
       * results.parametresConsommationDEau(DResultatsSinavi2.lireFichierConsommationDEau(path+nomPrj_.substring(indice,nomPrj_.length()-4)));
       */

      log(_c, "calcul termin�");
      // effacement des fichiers
      /*
       * try{ if (os.startsWith("Windows")) { cmd= new String[2]; cmd[0]=path + "sinavi_clean.bat";
       * cmd[1]=nomPrj_.substring(indice,nomPrj_.length()-4); } else { cmd= new String[2]; cmd[0]=path +
       * "sinavi_clean.sh"; cmd[1]=nomPrj_.substring(indice,nomPrj_.length()-4); } //lancement de l'executable du calcul
       * CExec ex2= new CExec(); ex2.setExecDirectory(new File(path)); // ex.setCommand(cmd1);
       * ex2.setOutStream(System.out); ex2.setErrStream(System.err); System.out.println("Effacement des fichiers");
       * ex2.setCommand(cmd); ex2.exec(); } catch(Exception ex2){ System.out.println("Erreur d'effacement de fichiers"); }
       */
    } catch (final Exception ex) {
      log(_c, "erreur du calcul");
      CDodico.exceptionAxel(this, ex);
    }
    finally{
      if (exts != null) {
        for (int i = exts.length - 1; i >= 0; i--) {
          final File f = new File(path + nomPrj + exts[i]);
          if (f.exists()) {
            f.deleteOnExit();
          }
        }
      }
    }

  }

}
