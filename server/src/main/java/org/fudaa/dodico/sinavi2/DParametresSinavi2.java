/*
 * @creation 12 mars 2006
 * @modification $Date: 2006-09-19 14:45:56 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */

package org.fudaa.dodico.sinavi2;

import org.fudaa.dodico.corba.sinavi2.*;

import org.fudaa.dodico.calcul.DParametres;

/**
 * @author maneuvrier KA Cette classe a pour but de sauvegarder les param�tre pour les envoyer au calcul.
 * @version $Id: DParametresSinavi2.java,v 1.16 2006-09-19 14:45:56 deniger Exp $
 */
public class DParametresSinavi2 extends DParametres implements IParametresSinavi2, IParametresSinavi2Operations {
  SParametresBateau[] bateaux_;
  SParametresBief[] biefs_;
  SParametresEcluse[] ecluses_;
  SParametresVitesses[] vitesses_;
  SParametresTrajets[] trajets_;
  SParametresCroisements[] croisements_;
  SParametresTrematages[] trematages_;
  SParametresManoeuvres[] manoeuvres_;
  SParametresGeneration generation_;
  SResultatGenerationBateau[] resultatGenerationBateaux_; // � enlever pas utile je pense
  public static int nbMaxDeGares = 80; // nomre de gare max autoris�
  String nomPrj_;// nom du Projet

  public SParametresBateau[] parametresBateau() {
    return bateaux_;
  }

  public void parametresBateau(final SParametresBateau[] _newParametresBateau) {
    bateaux_ = _newParametresBateau;
  }

  public SParametresBief[] parametresBief() {

    return biefs_;
  }

  public void parametresBief(final SParametresBief[] _newParametresBief) {
    biefs_ = _newParametresBief;
  }

  public SParametresEcluse[] parametresEcluse() {

    return ecluses_;
  }

  public void parametresEcluse(final SParametresEcluse[] _newParametresEcluse) {
    ecluses_ = _newParametresEcluse;

  }

  public SParametresVitesses[] parametresVitesse() {

    return vitesses_;
  }

  public void parametresVitesse(final SParametresVitesses[] _newParametresVitesse) {
    vitesses_ = _newParametresVitesse;

  }

  public SParametresTrajets[] parametresTrajet() {
    return trajets_;
  }

  public void parametresTrajet(final SParametresTrajets[] _newParametresTrajet) {
    trajets_ = _newParametresTrajet;
  }

  public SParametresCroisements[] parametresCroisement() {
    return croisements_;
  }

  public void parametresCroisement(final SParametresCroisements[] _newParametresCroisement) {
    croisements_ = _newParametresCroisement;

  }

  public SParametresTrematages[] parametresTrematage() {

    return trematages_;
  }

  public void parametresTrematage(final SParametresTrematages[] _newParametresTrematage) {

    trematages_ = _newParametresTrematage;
  }

  public SParametresManoeuvres[] parametresManoeuvre() {

    return manoeuvres_;
  }

  public void parametresManoeuvre(final SParametresManoeuvres[] _newParametresManoeuvre) {
    manoeuvres_ = _newParametresManoeuvre;

  }

  public SParametresGeneration parametresGeneration() {

    return generation_;
  }

  public void parametresGeneration(final SParametresGeneration _newParametresGeneration) {
    generation_ = _newParametresGeneration;

  }

  public SResultatGenerationBateau[] parametresResultatGenerationBateau() {

    return resultatGenerationBateaux_;
  }

  public void parametresResultatGenerationBateau(final SResultatGenerationBateau[] _newParametresResultatGenerationBateau) {
    resultatGenerationBateaux_ = _newParametresResultatGenerationBateau;

  }

  public int nombreMaxDeGares() {

    return nbMaxDeGares;
  }

  public void nombreMaxDeGares(final int _newNombreMaxDeGares) {
    nbMaxDeGares = _newNombreMaxDeGares;
  }

  public String nomPrj() {
    return nomPrj_;
  }

  public void nomPrj(final String _newNomPrj) {
    nomPrj_ = _newNomPrj;

  }

}

/** on pourrait �crire une fonction qui cast le nom de projet avec un string extension* */

