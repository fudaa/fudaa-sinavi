/*
 * @creation 6 avr. 2006
 * @modification $Date: 2006-09-19 14:45:56 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.sinavi2;

import java.text.NumberFormat;

import com.memoire.fu.FuLog;

import org.fudaa.ctulu.CtuluLib;
import org.fudaa.ctulu.CtuluLibString;

import org.fudaa.dodico.corba.sinavi2.SHeure;
import org.fudaa.dodico.corba.sinavi2.SParametresCroisements;
import org.fudaa.dodico.corba.sinavi2.SParametresManoeuvres;
import org.fudaa.dodico.corba.sinavi2.SParametresTrajets;
import org.fudaa.dodico.corba.sinavi2.SParametresTrematages;
import org.fudaa.dodico.corba.sinavi2.SParametresVitesses;

/**
 * @author fred deniger
 * @version $Id: Sinavi2Helper.java,v 1.6 2006-09-19 14:45:56 deniger Exp $
 */
public final class Sinavi2Helper {
  // par soucis de performance on ne cree pas une isntance a chaque appel
  public static NumberFormat heureMin_;
  public static NumberFormat dot_;

  private Sinavi2Helper() {}

  public static String getOuiNon(final boolean _b) {
    return _b ? "O" : "N";
  }

  public static String getSinaviTypeLoi(final SParametresTrajets _trajet) {
    final int loi = _trajet.typeDeLoi;
    switch (loi) {
    case 0:
      return "E";
    case 1:
      return "D";
    case 2:
      return "J";
    default:
      return "Inconnu";
    }
  }

  /**
   * M�thode permettant de savoir si deux bateaux ont le meme nom, sens, gare de d�part et d'arriv�e et loi.
   *
   * @param _t1
   * @param _t2
   * @return
   */
  public static boolean isLoiDeterEgale(final SParametresTrajets _t1, final SParametresTrajets _t2) {
    return (_t1.bateau.equalsIgnoreCase(_t2.bateau) && _t1.avalantMontant == _t2.avalantMontant
        && _t1.gareDepart == _t2.gareDepart && _t1.gareArrivee == _t2.gareArrivee);
  }

  /** M�thode permettant de comparer deux bateaux ayant la meme loi: Erlang. */
  public static boolean isLoiErlangEgale(final SParametresTrajets _t1, final SParametresTrajets _t2) {
    return (_t1.bateau.equalsIgnoreCase(_t2.bateau) && _t1.typeDeLoi == _t2.typeDeLoi
        && _t1.loiE.nbreBateauxAttendus == _t2.loiE.nbreBateauxAttendus && _t1.gareDepart == _t2.gareDepart
        && _t1.gareArrivee == _t2.gareArrivee && _t1.avalantMontant == _t2.avalantMontant
        && _t1.loiE.ordreDeLaLoi == _t2.loiE.ordreDeLaLoi
        && _t1.loiE.heureDebutGeneration.heure == _t2.loiE.heureDebutGeneration.heure && _t1.loiE.heureDebutGeneration.minutes == _t2.loiE.heureDebutGeneration.minutes);

  }

  /** M�thode permettant de comparer deux bateaux ayant la meme loi: Journali�re. */
  public static boolean isLoiJourEgale(final SParametresTrajets _t1, final SParametresTrajets _t2) {
    return (_t1.bateau.equalsIgnoreCase(_t2.bateau) && _t1.typeDeLoi == _t2.typeDeLoi
        && _t1.avalantMontant == _t2.avalantMontant && _t1.gareDepart == _t2.gareDepart && _t1.gareArrivee == _t2.gareArrivee);

  }

  public static String en2Chiffres(final String _nb) {
    if (_nb.length() == 1) {
      return "0" + _nb;
    }
    return _nb;

  }

  // public static String formatHeureMin(String _ch) {
  //
  // return Sinavi2Helper.formatHeureMin(Double.parseDouble(_ch));
  // }

  public static String formatDot(final double _value) {
    return Sinavi2Helper.getDot().format(_value);
  }

  public static String formatHeureMin(final int _heure, final int _min) {
    int mInt = _min;
    int hInt = _heure;
    // l'ordre est important
    // les minutes sont sup�rieurs a 59:
    // on remet a zero et on augmente l'heure d'une unite
    if (mInt > 59) {
      mInt = mInt % 60;
      hInt++;
    }
    // on remet l'heure dans 0,24
    if (hInt > 24 || (hInt == 24 && mInt > 0)) {
      hInt = hInt % 24;
    }
    return en2Chiffres(CtuluLibString.getString(hInt)) + CtuluLibString.DOT + en2Chiffres(CtuluLibString.getString(mInt));
  }

  public static String formatHeureMin(final double _value) {
    final NumberFormat nf = Sinavi2Helper.getHeureMin();
    final String val = nf.format(_value);
    // l'indice du point. Logiquement il est egal a 2
    final int idx = val.indexOf('.');
    final String heure = val.substring(0, idx);
    final String min = val.substring(idx + 1);
    // false si une modif a ete effectu�e
    // la valeur de l'heure: doit appartenir a [0,24]
    int hInt = 0;
    // la valeur des minutes: doit appartenir a [0,59]
    int mInt = 0;
    try {
      hInt = Integer.parseInt(heure);
    } catch (final NumberFormatException _evt) {
      // ne devrait jamais arriv�. Au cas, mise a zero
      // faut-t-il mettre une valeur erron�e du style "??" pour avertir l'utilisateur
      FuLog.error(_evt);
    }

    try {
      mInt = Integer.parseInt(min);
    } catch (final NumberFormatException _evt) {
      // faut-t-il mettre une valeur erron�e du style "??" pour avertir l'utilisateur
      FuLog.error(_evt);
    }
    return formatHeureMin(hInt, mInt);
  }

  protected static NumberFormat getHeureMin() {
    if (heureMin_ == null) {
      heureMin_ = CtuluLib.getDecimalFormat();
      heureMin_.setMaximumFractionDigits(2);
      heureMin_.setMaximumIntegerDigits(2);
      heureMin_.setMinimumFractionDigits(2);
      heureMin_.setMinimumIntegerDigits(2);
    }
    return heureMin_;
  }

  protected static NumberFormat getDot() {
    if (dot_ == null) {
      dot_ = CtuluLib.getDecimalFormat();
      dot_.setMaximumFractionDigits(2);
      dot_.setMinimumFractionDigits(2);
      dot_.setMinimumIntegerDigits(2);
    }
    return dot_;
  }

  public static boolean isErlang(final SParametresTrajets _t) {
    return _t.typeDeLoi == 0;
  }

  public static boolean isDeterministe(final SParametresTrajets _t) {
    return _t.typeDeLoi == 1;
  }

  public static boolean isJournaliere(final SParametresTrajets _t) {
    return _t.typeDeLoi == 2;
  }

  /**
   * @param _h l'heure ou la min
   * @return
   */
  public static String getDeuxChiffres(final int _h) {
    return _h < 10 ? CtuluLibString.ZERO + CtuluLibString.getString(_h) : CtuluLibString.getString(_h);
  }

  /**
   * m�thode permettant de mettre sous forme d'une seule chaine l'heure et la minute.
   */
  public static String format(final int _h, final int _min, final String _sep) {
    return getDeuxChiffres(_h) + _sep + getDeuxChiffres(_min);
  }

  public static String format(final int _h, final int _min) {
    return format(_h, _min, "H");
  }

  /**
   * @param _h les heures
   * @param _min les minutes
   * @return HH.MM
   */
  public static String formatSinavi(final int _h, final int _min) {
    return format(_h, _min, CtuluLibString.DOT);
  }

  public static String formatSinaviDuree(final SHeure _h) {
    return formatSinavi(_h.heure, _h.minutes);
  }

  public static String formatSinaviHeure(final SHeure _h) {
    return formatHeureMin(_h.heure, _h.minutes);
  }

  public static String formatDisplay(final SHeure _h) {
    return format(_h.heure, _h.minutes);
  }

  public static String getTypeLoi(final SParametresTrajets _t) {
    return getTypeLoi(_t.typeDeLoi);
  }

  public static String getTypeLoi(final int _l) {
    if (_l == 0) {
      return "Erlang";
    } else if (_l == 1) {
      return "Deterministe";
    } else if (_l == 2) {
      return "Journali�re";
    }
    return "Inconnue";
  }

  /** M�thode qui a a partir d'une chaine renvoie true si c'est avalant ou false si c'est montant. */
  public static boolean isAval(final String _arg) {
    return !CtuluLibString.isEmpty(_arg) && _arg.toLowerCase().startsWith("a");
  }

  /** M�thode permettant de renvoyer le sens true: A et false: M. */
  public static String getAvMontantLongDesc(final boolean _isAval) {
    return (_isAval ? "Avalant" : "Montant");
  }

  public static String getAvalMontSinaviDesc(final boolean _arg) {
    return (_arg ? "A" : "M");
  }

  public static boolean typeCroisementsEquals(final SParametresCroisements _c1, final SParametresCroisements _c2) {
    return (_c1.bief.equalsIgnoreCase(_c2.bief) && _c1.type1.equalsIgnoreCase(_c2.type1) && _c1.type2
        .equalsIgnoreCase(_c2.type2));
  }

  public static boolean typeManoeuvresEquals(final SParametresManoeuvres _c1, final SParametresManoeuvres _c2) {
    return (_c1.ecluse.equalsIgnoreCase(_c2.ecluse) && _c1.type.equalsIgnoreCase(_c2.type));
  }

  public static boolean typeTrematagesEquals(final SParametresTrematages _c1, final SParametresTrematages _c2) {
    return (_c1.bief.equalsIgnoreCase(_c2.bief) && _c1.type1.equalsIgnoreCase(_c2.type1) && _c1.type2
        .equalsIgnoreCase(_c2.type2));
  }

  public static boolean typeVitessesEquals(final SParametresVitesses _c1, final SParametresVitesses _c2) {
    return (_c1.bief.equalsIgnoreCase(_c2.bief) && _c1.bateau.equalsIgnoreCase(_c2.bateau));
  }

}
