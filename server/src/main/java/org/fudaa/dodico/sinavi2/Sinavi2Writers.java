/*
 * @creation 6 avr. 2006
 * @modification $Date: 2006-09-19 14:45:56 $
 * @license GNU General Public License 2
 * @copyright (c)1998-2001 CETMEF 2 bd Gambetta F-60231 Compiegne
 * @mail devel@fudaa.fr
 */
package org.fudaa.dodico.sinavi2;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import com.memoire.fu.Fu;
import com.memoire.fu.FuLog;

import org.fudaa.ctulu.CtuluAnalyze;
import org.fudaa.ctulu.CtuluLibString;
import org.fudaa.ctulu.ProgressionInterface;
import org.fudaa.ctulu.ProgressionUpdater;
import org.fudaa.ctulu.fileformat.FortranLib;

import org.fudaa.dodico.corba.sinavi2.IParametresSinavi2;
import org.fudaa.dodico.corba.sinavi2.SParametresBateau;
import org.fudaa.dodico.corba.sinavi2.SParametresBief;
import org.fudaa.dodico.corba.sinavi2.SParametresCroisements;
import org.fudaa.dodico.corba.sinavi2.SParametresEcluse;
import org.fudaa.dodico.corba.sinavi2.SParametresManoeuvres;
import org.fudaa.dodico.corba.sinavi2.SParametresTrajets;
import org.fudaa.dodico.corba.sinavi2.SParametresTrematages;
import org.fudaa.dodico.corba.sinavi2.SParametresVitesses;

import org.fudaa.dodico.fortran.FortranWriter;

/**
 * @author fred deniger
 * @version $Id: Sinavi2Writers.java,v 1.7 2006-09-19 14:45:56 deniger Exp $
 */
public final class Sinavi2Writers {

  static final int[] DEFAULT_FMT = { 40, 100 };

  private static String getFinFichier() {
    return "| Ligne de fin de fichier ";
  }

  public static void doUnBateau(final SParametresBateau _bat, final FortranWriter _w, final int _indice, final int[] _fmt) throws IOException {
    doUnBateau(_bat, _w, _indice, _fmt, false);
  }

  public static void doUnBateau(final SParametresBateau _bat, final FortranWriter _w, final int _indice, final int[] _fmt, final boolean _vitesse)
      throws IOException {
    _w.stringField(0, _bat.identification);
    _w.stringField(1, "|   Nom du Type " + (_indice + 1));
    _w.writeFields(_fmt);
    _w.stringField(0, Double.toString(_bat.longueur));
    _w.stringField(1, "|   Longueur");
    _w.writeFields(_fmt);
    _w.stringField(0, Double.toString(_bat.largeur));
    _w.stringField(1, "|   Largeur");
    _w.writeFields(_fmt);
    _w.stringField(0, Double.toString(_bat.tirantDeau));
    _w.stringField(1, "|   Tirant d'eau");
    _w.writeFields(_fmt);
    _w.stringField(0, Sinavi2Helper.formatDot(_bat.dureeGeneAdmissible));
    _w.stringField(1, "|   Dur�e de gene admissible");
    _w.writeFields(_fmt);
    _w.stringField(0, Sinavi2Helper.formatHeureMin(_bat.debutNavigation));
    _w.stringField(1, "|   D�but de Navigation");
    _w.writeFields(_fmt);
    _w.stringField(0, Sinavi2Helper.formatHeureMin(_bat.finNavigation));
    _w.stringField(1, "|   Fin de Navigation");
    _w.writeFields(_fmt);
    if (_vitesse) {
      _w.stringField(0, Sinavi2Helper.formatDot(_bat.vitesseMontantParDefaut));
      _w.stringField(1, "| Vitesse Montant par D�faut");
      _w.writeFields(_fmt);
      _w.stringField(0, Sinavi2Helper.formatDot(_bat.vitesseDescendantParDefaut));
      _w.stringField(1, "| Vitesse Descendant par D�faut");
      _w.writeFields(_fmt);
    }
    _w.flush();
  }

  /**
   * Pour l'exportation de fichier bateau.
   */
  public static void doBateauExport2(final SParametresBateau _bat, final FortranWriter _w, final int _indice, final int[] _fmt)
      throws IOException {
    doUnBateau(_bat, _w, _indice, _fmt, true);

  }

  public static void doBateaux(final SParametresBateau[] _bat, final File _dest, final ProgressionInterface _prog, final CtuluAnalyze _analyze) {
    doBateaux(_bat, _dest, _prog, _analyze, false);

  }

  /**
   * M�thode d'�critures : Elles ont toutes la m�me structure, un fonction d'�criture principale qui en appelle une
   * autre pour �crire les �lements unitairement.
   */
  public static void doBateaux(final SParametresBateau[] _bateaux, final File _dest, final ProgressionInterface _prog,
      final CtuluAnalyze _analyze, final boolean _v2) {
    final WriteAction write = new WriteAction(DEFAULT_FMT, "Ecriture des bateaux") {
      void write(FortranWriter _w, int _idx) throws IOException {
        if (_v2) {
          doBateauExport2(_bateaux[_idx], _w, _idx, super.fmt_);
        } else {
          doUnBateau(_bateaux[_idx], _w, _idx, super.fmt_);
        }
      }
    };
    write(_bateaux.length, _dest, _prog, _analyze, write);
  }

  public static void doBateauxV2(final SParametresBateau[] _bat, final File _dest, final ProgressionInterface _prog, final CtuluAnalyze _analyze) {
    doBateaux(_bat, _dest, _prog, _analyze, true);
  }

  public static void doBief(final SParametresBief _bief, final FortranWriter _w, final int _indice, final int[] _fmt) throws IOException {
    doUnBief(_bief, _w, _indice, _fmt, true);
  }

  public static void doUnBief(final SParametresBief _bief, final FortranWriter _w, final int _indice, final int[] _fmt, boolean _all)
      throws IOException {
    _w.stringField(0, _bief.identification);
    _w.stringField(1, "| Nom du bief " + CtuluLibString.getString(_indice + 1));
    _w.writeFields(_fmt);
    _w.stringField(0, Double.toString(_bief.longueur));
    _w.stringField(1, "| Longueur  (m) ");
    _w.writeFields(_fmt);
    _w.stringField(0, Double.toString(_bief.largeur));
    _w.stringField(1, "| Largeur (m) ");
    _w.writeFields(_fmt);
    _w.stringField(0, Double.toString(_bief.hauteur));
    _w.stringField(1, "| Hauteur d'eau (m)");
    _w.writeFields(_fmt);
    _w.stringField(0, Double.toString(_bief.vitesse));
    _w.stringField(1, "| Vitesse (km/h) ");
    _w.writeFields(_fmt);
    if (!_all) {
      return;
    }
    /** ** indisponibilit�s*** */
    _w.stringField(0, "J");
    _w.stringField(1, getLoiName());
    _w.writeFields(_fmt);
    _w.stringField(0, Sinavi2Helper.formatSinaviHeure(_bief.loiJ.heureDebut));
    _w.stringField(1, "| Heure Debut ");
    _w.writeFields(_fmt);
    _w.stringField(0, Sinavi2Helper.formatSinaviHeure(_bief.loiJ.heureFin));
    _w.stringField(1, "| Heure Fin ");
    _w.writeFields(_fmt);
    _w.stringField(0, "D");
    _w.stringField(1, getLoiName());
    _w.writeFields(_fmt);
    for (int j = 0; j < _bief.loiD.length; j++) {
      _w.stringField(0, _bief.loiD[j].dateDebut + CtuluLibString.EMPTY_STRING);
      _w.stringField(1, "| Date de d�but ");
      _w.writeFields(_fmt);
      _w.stringField(0, Sinavi2Helper.formatSinaviHeure(_bief.loiD[j].heureDebut));
      _w.stringField(1, "| Heure de D�but ");
      _w.writeFields(_fmt);
      _w.stringField(0, _bief.loiD[j].dateFin + CtuluLibString.EMPTY_STRING);
      _w.stringField(1, "| Date de Fin ");
      _w.writeFields(_fmt);
      _w.stringField(0, Sinavi2Helper.formatSinaviHeure(_bief.loiD[j].heureFin));
      _w.stringField(1, "| Heure de Fin ");
      _w.writeFields(_fmt);
    }
    _w.stringField(0, CtuluLibString.EMPTY_STRING);
    _w.stringField(1, "| Fin des entr�es pour la loi D�terministe ");
    _w.writeFields(_fmt);
    _w.stringField(0, "E");
    _w.stringField(1, getLoiName());
    _w.writeFields(_fmt);
    _w.stringField(0, CtuluLibString.getString(_bief.loiE.ordreEcart));
    _w.stringField(1, "| Ordre pour l'�cart ");
    _w.writeFields(_fmt);
    _w.stringField(0, CtuluLibString.getString(_bief.loiE.nbJoursDEcart));
    _w.stringField(1, "| Nb de Jours d'Ecarts ");
    _w.writeFields(_fmt);
    _w.stringField(0, CtuluLibString.getString(_bief.loiE.ordreDuree));
    _w.stringField(1, "| Ordre pour la dur�e ");
    _w.writeFields(_fmt);
    _w.stringField(0, Sinavi2Helper.formatSinaviDuree(_bief.loiE.duree));
    _w.stringField(1, "| Dur�e ");
    _w.writeFields(_fmt);
    /** fin des indisponibilit�s* */
    _w.flush();
  }

  private static String getLoiName() {
    return "| Type de Loi ";
  }

  public static void doUnBiefExport(final SParametresBief _bief, final FortranWriter _w, final int _indice, final int[] _fmt)
      throws IOException {
    doUnBief(_bief, _w, _indice, _fmt, false);

  }

  public static void doBiefs(final SParametresBief[] _biefs, final File _dest, final ProgressionInterface _prog, final CtuluAnalyze _analyze) {
    doBiefs(_biefs, _dest, _prog, _analyze, false);
  }

  public static void doBiefs(final SParametresBief[] _biefs, final File _dest, final ProgressionInterface _prog,
      final CtuluAnalyze _analyze, final boolean _export) {
    final WriteAction write = new WriteAction(DEFAULT_FMT, "Ecriture des biefs") {
      void write(FortranWriter _w, int _idx) throws IOException {
        if (_export) {
          doUnBiefExport(_biefs[_idx], _w, _idx, super.fmt_);
        } else {
          doBief(_biefs[_idx], _w, _idx, super.fmt_);
        }
      }
    };
    write(_biefs.length, _dest, _prog, _analyze, write);
  }

  public static void doBiefsExport(final SParametresBief[] _biefs, final File _dest, final ProgressionInterface _prog,
      final CtuluAnalyze _analyze) {
    doBiefs(_biefs, _dest, _prog, _analyze, true);
  }

  public static void doUnCroisement(final SParametresCroisements _croisement, final FortranWriter _w, final int[] _fmt)
      throws IOException {
    _w.stringField(2, _croisement.type2);
    _w.stringField(0, _croisement.bief);
    _w.stringField(1, _croisement.type1);
    _w.stringField(3, Sinavi2Helper.getOuiNon(_croisement.ouiNon));
    _w.writeFields(_fmt);
    _w.flush();
  }

  public static void doCroisements(final SParametresCroisements[] _croisements, final File _dest, final ProgressionInterface _prog,
      final CtuluAnalyze _analyze) {
    final WriteAction write = new WriteAction(getFmtCroisement(), "Ecriture des croisements", false) {
      void write(FortranWriter _w, int _idx) throws IOException {
        doUnCroisement(_croisements[_idx], _w, super.fmt_);
      }
    };
    write(_croisements.length, _dest, _prog, _analyze, write);

  }

  public static void doUneEcluse(final SParametresEcluse _ecluse, final FortranWriter _w, final int _indice, final int[] _fmt)
      throws IOException {
    doUneEcluse(_ecluse, _w, _indice, _fmt, true);
  }

  public static void doUneEcluse(final SParametresEcluse _ecluse, final FortranWriter _w, final int _indice, final int[] _fmt, boolean _all)
      throws IOException {
    _w.stringField(0, _ecluse.identification);
    _w.stringField(1, "| Nom de l'ecluse " + CtuluLibString.getString(_indice + 1));
    _w.writeFields(_fmt);
    _w.stringField(0, Double.toString(_ecluse.longueur));
    _w.stringField(1, "| Longueur  (m) ");
    _w.writeFields(_fmt);
    _w.stringField(0, Double.toString(_ecluse.largeur));
    _w.stringField(1, "| Largeur (m) ");
    _w.writeFields(_fmt);
    _w.stringField(0, Double.toString(_ecluse.profondeur));
    _w.stringField(1, "| Profondeur (m)");
    _w.writeFields(_fmt);
    _w.stringField(0, Double.toString(_ecluse.hauteurChuteDEau));
    _w.stringField(1, "| Hauteur de Chute d'eau (m)");
    _w.writeFields(_fmt);
    _w.stringField(0, Sinavi2Helper.formatDot(_ecluse.dureeBassineeMontante));
    _w.stringField(1, "| Duree d'une bassinee montante (mm.ss) ");
    _w.writeFields(_fmt);
    _w.stringField(0, Sinavi2Helper.formatDot(_ecluse.dureeBassineeDescendante));
    _w.stringField(1, "| Duree bassinee avalante (mm.ss) ");
    _w.writeFields(_fmt);
    if (!_all) {
      return;
    }
    _w.stringField(0, "J");
    _w.stringField(1, getLoiName());
    _w.writeFields(_fmt);
    _w.stringField(0, Sinavi2Helper.formatSinaviHeure(_ecluse.loiJ.heureDebut));
    _w.stringField(1, "| Heure Debut ");
    _w.writeFields(_fmt);
    _w.stringField(0, Sinavi2Helper.formatSinaviHeure(_ecluse.loiJ.heureFin));
    _w.stringField(1, "| Heure Fin ");
    _w.writeFields(_fmt);
    _w.stringField(0, "D");
    _w.stringField(1, getLoiName());
    _w.writeFields(_fmt);
    for (int j = 0; j < _ecluse.loiD.length; j++) {
      _w.stringField(0, _ecluse.loiD[j].dateDebut + CtuluLibString.EMPTY_STRING);
      _w.stringField(1, "| Date de d�but ");
      _w.writeFields(_fmt);
      _w.stringField(0, Sinavi2Helper.formatSinaviHeure(_ecluse.loiD[j].heureDebut));
      _w.stringField(1, "| Heure de D�but ");
      _w.writeFields(_fmt);
      _w.stringField(0, _ecluse.loiD[j].dateFin + CtuluLibString.EMPTY_STRING);
      _w.stringField(1, "| Date de Fin ");
      _w.writeFields(_fmt);
      _w.stringField(0, Sinavi2Helper.formatSinaviHeure(_ecluse.loiD[j].heureFin));
      _w.stringField(1, "| Heure de Fin ");
      _w.writeFields(_fmt);
    }
    _w.stringField(0, CtuluLibString.EMPTY_STRING);
    _w.stringField(1, "| Fin des entr�es pour la loi D�terministe ");
    _w.writeFields(_fmt);
    _w.stringField(0, "E");
    _w.stringField(1, getLoiName());
    _w.writeFields(_fmt);
    _w.stringField(0, CtuluLibString.getString(_ecluse.loiE.ordreEcart));
    _w.stringField(1, "| Ordre pour l'�cart ");
    _w.writeFields(_fmt);
    _w.stringField(0, CtuluLibString.getString(_ecluse.loiE.nbJoursDEcart));
    _w.stringField(1, "| Nb de Jours d'Ecarts ");
    _w.writeFields(_fmt);
    _w.stringField(0, CtuluLibString.getString(_ecluse.loiE.ordreDuree));
    _w.stringField(1, "| Ordre pour la dur�e ");
    _w.writeFields(_fmt);
    _w.stringField(0, Sinavi2Helper.formatSinaviDuree(_ecluse.loiE.duree));
    _w.stringField(1, "| Dur�e ");
    _w.writeFields(_fmt);
    _w.flush();
  }

  public static void doUneEcluseExport(final SParametresEcluse _ecluse, final FortranWriter _w, final int _indice, final int[] _fmt)
      throws IOException {
    doUneEcluse(_ecluse, _w, _indice, _fmt, false);
  }

  public static void doUneEcluseExportV2(final SParametresEcluse _ecluse, final FortranWriter _w, final int _indice, final int[] _fmt)
      throws IOException {
    _w.stringField(0, _ecluse.identification);
    _w.stringField(1, "| Nom de l'ecluse " + CtuluLibString.getString(_indice + 1));
    _w.writeFields(_fmt);
    _w.stringField(0, Double.toString(_ecluse.longueur));
    _w.stringField(1, "| Longueur (m)");
    _w.writeFields(_fmt);
    _w.stringField(0, Double.toString(_ecluse.largeur));
    _w.stringField(1, "| Largeur (m)");
    _w.writeFields(_fmt);
    _w.stringField(0, Double.toString(_ecluse.profondeur));
    _w.stringField(1, "| Profondeur (m)");
    _w.writeFields(_fmt);
    _w.stringField(0, Sinavi2Helper.formatDot(_ecluse.dureeBassineeMontante));
    _w.stringField(1, "| Duree d'une bassinee montante (mm.ss)");
    _w.writeFields(_fmt);
    _w.stringField(0, Sinavi2Helper.formatDot(_ecluse.dureeBassineeDescendante));
    _w.stringField(1, "| Duree d'une bassinee avalante (mm.ss)");
    _w.writeFields(_fmt);
    _w.stringField(0, Double.toString(_ecluse.hauteurChuteDEau));
    _w.stringField(1, "| Hauteur chute  d'eau (m)");
    _w.writeFields(_fmt);
    _w.stringField(0, Sinavi2Helper.formatDot(_ecluse.dureeManoeuvresEnEntree));
    _w.stringField(1, "| Duree manoeuvre en entr�e (mm.ss)");
    _w.writeFields(_fmt);
    _w.stringField(0, Sinavi2Helper.formatDot(_ecluse.dureeManoeuvresEnSortie));
    _w.stringField(1, "| Duree manoeuvre en sortie (mm.ss)");
    _w.writeFields(_fmt);
    _w.flush();
  }

  public static void doEcluses(final SParametresEcluse[] _ecluses, final File _dest, final ProgressionInterface _prog,
      final CtuluAnalyze _analyze) {
    doEcluses(_ecluses, _dest, _prog, _analyze, false);
  }

  public static void write(final int _nb, final File _dest, final ProgressionInterface _prog, final CtuluAnalyze _analyze,
      final WriteAction _write) {
    FortranWriter w = null;
    try {
      w = new FortranWriter(new FileWriter(_dest));
      final ProgressionUpdater prog = new ProgressionUpdater(_prog);
      prog.setValue(4, _nb);
      prog.majProgessionStateOnly(_write.desc_);
      for (int i = 0; i < _nb; i++) {
        _write.write(w, i);
        prog.majAvancement();
      }
      if (_write.writeEnd_) {
        w.stringField(0, CtuluLibString.EMPTY_STRING);
        w.stringField(1, getFinFichier());
        w.writeFields(_write.fmt_);
      }

    } catch (final IOException e) {
      FuLog.error(e);
    } finally {
      FortranLib.close(w);
    }

  }

  public static void doEcluses(final SParametresEcluse[] _ecluses, final File _dest, final ProgressionInterface _prog,
      final CtuluAnalyze _analyze, final boolean _export) {
    final WriteAction act = new WriteAction(DEFAULT_FMT, "Ecriture des �cluses") {
      void write(FortranWriter _w, int _idx) throws IOException {
        if (_export) {
          doUneEcluseExport(_ecluses[_idx], _w, _idx, super.fmt_);
        } else {
          doUneEcluse(_ecluses[_idx], _w, _idx, super.fmt_);
        }
      };
    };
    write(_ecluses.length, _dest, _prog, _analyze, act);

  }

  /**
   * m�thode pour �crire les fichiers de param�tres lus par sinavi _param .<br>
   * La structure Iparam�tres contenant DParametres _rep : path du serveur _prog : barre de progression, on ne s'en est
   * pas trop pr�coccup� _analyse : pareil
   */

  public static void ecritParametres(final IParametresSinavi2 _param, final File _rep, final String _nomPrj,
      final ProgressionInterface _prog, final CtuluAnalyze _analyze) {
    // ecritParametresBateau(....)
    final SParametresBateau[] bateaux = _param.parametresBateau();
    final SParametresBief[] bief = _param.parametresBief();
    final SParametresEcluse[] ecluse = _param.parametresEcluse();
    final SParametresVitesses[] vitesse = _param.parametresVitesse();
    final SParametresTrematages[] trematage = _param.parametresTrematage();
    final SParametresCroisements[] croisement = _param.parametresCroisement();
    final SParametresManoeuvres[] manoeuvre = _param.parametresManoeuvre();
    final SParametresTrajets[] trajet = _param.parametresTrajet();
    File f = new File(_rep, _nomPrj + ".nav");
    if (Fu.DEBUG && FuLog.isDebug()) {
      FuLog.debug("DSI: chemin fic " + f.getAbsolutePath());
    }
    doBateaux(bateaux, f, _prog, _analyze);
    f = new File(_rep, _nomPrj + ".bie");
    doBiefs(bief, f, _prog, _analyze);
    f = new File(_rep, _nomPrj + ".ouv");
    doEcluses(ecluse, f, _prog, _analyze);
    f = new File(_rep, _nomPrj + ".cnx");
    doGares(_param.parametresEcluse(), _param.parametresBief(), f, _prog, _analyze);
    f = new File(_rep, _nomPrj + ".vites");
    doVitesses(vitesse, f, _prog, _analyze);
    f = new File(_rep, _nomPrj + ".tremat");
    doTrematages(trematage, f, _prog, _analyze);
    f = new File(_rep, _nomPrj + ".croist");
    doCroisements(croisement, f, _prog, _analyze);
    f = new File(_rep, _nomPrj + ".man");
    doManoeuvres(manoeuvre, f, _prog, _analyze);
    f = new File(_rep, _nomPrj + ".trajet");
    doTrajets(trajet, f, _prog, _analyze);

  }

  static abstract class WriteAction {
    public WriteAction(final int[] _fmt, final String _desc) {
      this(_fmt, _desc, true);
    }

    public WriteAction(final int[] _fmt, final String _desc, final boolean _writeEnd) {
      super();
      desc_ = _desc;
      writeEnd_ = _writeEnd;
      fmt_ = _fmt;
    }

    abstract void write(FortranWriter _w, int _idx) throws IOException;
    final String desc_;
    final boolean writeEnd_;
    final int[] fmt_;
  }

  public static void doEclusesExport(final SParametresEcluse[] _ecluse, final File _dest, final ProgressionInterface _prog,
      final CtuluAnalyze _analyze) {
    doEcluses(_ecluse, _dest, _prog, _analyze, true);
  }

  public static void doEclusesExportV2(final SParametresEcluse[] _ecluses, final File _dest, final ProgressionInterface _prog,
      final CtuluAnalyze _analyze) {
    final WriteAction act = new WriteAction(DEFAULT_FMT, "Ecriture des �cluses") {
      void write(FortranWriter _w, int _idx) throws IOException {
        doUneEcluseExportV2(_ecluses[_idx], _w, _idx, super.fmt_);
      };
    };
    write(_ecluses.length, _dest, _prog, _analyze, act);
  }

  /**
   * fichier connex cnx modif de la version fatimatou.
   */
  public static void doGares(final SParametresEcluse[] _ecluse, final SParametresBief[] _bief, final File _dest,
      final ProgressionInterface _prog, final CtuluAnalyze _analyze) {
    FortranWriter w = null;
    int numecluse;
    int numbief;
    int i, j;
    final int[] fmt = DEFAULT_FMT;
    try {
      w = new FortranWriter(new FileWriter(_dest));
      final ProgressionUpdater prog = new ProgressionUpdater();
      prog.setValue(5, _bief.length, 0, 50);
      prog.majProgessionStateOnly("Ecriture gares: biefs");
      for (i = 0; i < _bief.length; i++) {
        numbief = i + 1;
        w.stringField(0, CtuluLibString.getString(_bief[i].gareEnAmont));
        w.stringField(1, " | Gare precedant le bief  " + numbief);
        w.writeFields(fmt);
        w.stringField(0, CtuluLibString.getString(_bief[i].gareEnAval));
        w.stringField(1, " | Gare suivant le bief " + numbief);
        w.writeFields(fmt);
        prog.majAvancement();
      }
      prog.setValue(5, _ecluse.length, 50, 50);
      prog.majProgessionStateOnly("Ecriture gares: ecluses");
      for (j = 0; j < _ecluse.length; j++) {
        numecluse = j + 1;
        w.stringField(0, CtuluLibString.getString(_ecluse[j].gareEnAmont));
        w.stringField(1, " | Gare precedant l'ecluse  " + numecluse);
        w.writeFields(fmt);
        w.stringField(0, CtuluLibString.getString(_ecluse[j].gareEnAval));
        w.stringField(1, " | Gare suivant l'ecluse " + numecluse);
        w.writeFields(fmt);
        prog.majAvancement();

      }
      w.stringField(0, CtuluLibString.EMPTY_STRING);
      w.stringField(1, CtuluLibString.EMPTY_STRING);
      w.writeFields(fmt);
      w.flush();
      w.close();
    } catch (final IOException e) {
      FuLog.error(e);
    } finally {
      FortranLib.close(w);
    }
  }

  public static void doManoeuvres(final SParametresManoeuvres[] _manoeuvres, final File _dest, final ProgressionInterface _prog,
      final CtuluAnalyze _analyze) {
    final WriteAction write = new WriteAction(new int[] { 20, 20, 10, 10 }, "Ecritures manoeuvre", false) {
      void write(FortranWriter _w, int _idx) throws IOException {
        doUneManoeuvre(_manoeuvres[_idx], _w, super.fmt_);
      }
    };
    write(_manoeuvres.length, _dest, _prog, _analyze, write);
  }

  public static void doReseauExport(final SParametresEcluse[] _ecluse, final SParametresBief[] _bief, final File _dest,
      final ProgressionInterface _prog, final CtuluAnalyze _analyze) {
    FortranWriter w = null;
    int numecluse;
    int numbief;
    int i, j;
    final int[] fmt = DEFAULT_FMT;
    try {
      w = new FortranWriter(new FileWriter(_dest));
      final ProgressionUpdater prog = new ProgressionUpdater();
      prog.setValue(5, _bief.length, 0, 50);
      prog.majProgessionStateOnly("Ecriture reseau: biefs");
      for (i = 0; i < _bief.length; i++) {
        numbief = i + 1;
        w.stringField(0, _bief[i].identification);
        w.stringField(1, " | identification du bief " + numbief);
        w.writeFields(fmt);
        w.stringField(0, Double.toString(_bief[i].longueur));
        w.stringField(1, " | Longueur  ");
        w.writeFields(fmt);
        w.stringField(0, Double.toString(_bief[i].largeur));
        w.stringField(1, " | Largeur  ");
        w.writeFields(fmt);
        w.stringField(0, Double.toString(_bief[i].hauteur));
        w.stringField(1, " | Hauteur d'eau  ");
        w.writeFields(fmt);
        w.stringField(0, Double.toString(_bief[i].vitesse));
        w.stringField(1, " | Vitesse");
        w.writeFields(fmt);
        w.stringField(0, CtuluLibString.getString(_bief[i].gareEnAmont));
        w.stringField(1, " | Gare precedant le bief  " + numbief);
        w.writeFields(fmt);
        w.stringField(0, CtuluLibString.getString(_bief[i].gareEnAval));
        w.stringField(1, " | Gare suivant le bief " + numbief);
        w.writeFields(fmt);
        prog.majAvancement();
      }
      w.stringField(0, "****");
      w.stringField(1, " | Fin des biefs");
      w.writeFields(fmt);
      prog.setValue(5, _ecluse.length, 50, 50);
      prog.majProgessionStateOnly("Ecriture reseau: ecluses");
      for (j = 0; j < _ecluse.length; j++) {
        numecluse = j + 1;
        w.stringField(0, _ecluse[j].identification);
        w.stringField(1, " | Identification de l'�cluse " + numecluse);
        w.writeFields(fmt);
        w.stringField(0, Double.toString(_ecluse[j].longueur));
        w.stringField(1, " | Longueur");
        w.writeFields(fmt);
        w.stringField(0, Double.toString(_ecluse[j].largeur));
        w.stringField(1, " | Largeur");
        w.writeFields(fmt);
        w.stringField(0, Double.toString(_ecluse[j].profondeur));
        w.stringField(1, " | Profondeur");
        w.writeFields(fmt);
        w.stringField(0, Double.toString(_ecluse[j].hauteurChuteDEau));
        w.stringField(1, " | Hauteur de chute d'eau");
        w.writeFields(fmt);
        w.stringField(0, Double.toString(_ecluse[j].dureeBassineeMontante));
        w.stringField(1, " | Dur�e de bassin�e montante");
        w.writeFields(fmt);
        w.stringField(0, Double.toString(_ecluse[j].dureeBassineeDescendante));
        w.stringField(1, " | Dur�e de bassin�e avalante");
        w.writeFields(fmt);
        w.stringField(0, Double.toString(_ecluse[j].dureeManoeuvresEnEntree));
        w.stringField(1, " | Dur�e de manoeuvre en entr�e");
        w.writeFields(fmt);
        w.stringField(0, Double.toString(_ecluse[j].dureeManoeuvresEnSortie));
        w.stringField(1, " | Dur�e de manoeuvre en sortie");
        w.writeFields(fmt);
        w.stringField(0, CtuluLibString.getString(_ecluse[j].gareEnAmont));
        w.stringField(1, " | Gare precedant l'ecluse  " + numecluse);
        w.writeFields(fmt);
        w.stringField(0, CtuluLibString.getString(_ecluse[j].gareEnAval));
        w.stringField(1, " | Gare suivant l'ecluse " + numecluse);
        w.writeFields(fmt);

      }
      w.stringField(0, CtuluLibString.EMPTY_STRING);
      w.stringField(1, CtuluLibString.EMPTY_STRING);
      w.writeFields(fmt);
      w.flush();
      w.close();
    } catch (final IOException e) {
      FuLog.error(e);
    } finally {
      FortranLib.close(w);
    }
  }

  public static void doTrajets(final SParametresTrajets[] _traj, final File _dest, final ProgressionInterface _prog,
      final CtuluAnalyze _analyze) {
    final WriteAction write = new WriteAction(DEFAULT_FMT, "Ecritures des trajets", false) {

      void write(FortranWriter _w, int _idx) throws IOException {
        doUnTrajet(_traj[_idx], _w, _idx, super.fmt_);
      }

    };
    write(_traj.length, _dest, _prog, _analyze, write);
  }

  public static void doTrematages(final SParametresTrematages[] _trematages, final File _dest, final ProgressionInterface _prog,
      final CtuluAnalyze _analyze) {
    final WriteAction write = new WriteAction(new int[] { 20, 20, 20, 1 }, "Ecritures des trematages", false) {
      void write(FortranWriter _w, int _idx) throws IOException {
        doUnTrematage(_trematages[_idx], _w, super.fmt_);
      }

    };
    write(_trematages.length, _dest, _prog, _analyze, write);
  }

  public static void doUneVitesse(final SParametresVitesses _vitesse, final FortranWriter _w, final int[] _fmt) throws IOException {
    _w.stringField(0, _vitesse.bateau);
    _w.stringField(1, _vitesse.bief);
    _w.stringField(2, Double.toString(_vitesse.vitesseDesMontants));
    _w.stringField(3, Double.toString(_vitesse.vitesseDesAvalants));
    _w.writeFields(_fmt);
    _w.flush();

  }

  public static void doUneManoeuvre(final SParametresManoeuvres _manoeuvre, final FortranWriter _w, final int[] _fmt) throws IOException {
    _w.stringField(0, _manoeuvre.ecluse);
    _w.stringField(1, _manoeuvre.type);
    _w.stringField(2, Sinavi2Helper.formatDot(_manoeuvre.entree));
    _w.stringField(3, Sinavi2Helper.formatDot(_manoeuvre.sortie));
    _w.writeFields(_fmt);
    _w.flush();
  }

  public static void doUnTrajet(final SParametresTrajets _trajet, final FortranWriter _w, final int _indice, final int[] _fmt)
      throws IOException {

    _w.stringField(0, _trajet.bateau);
    _w.stringField(1, "| Type de bateau " + CtuluLibString.getString(_indice + 1));
    _w.writeFields(_fmt);
    _w.stringField(0, Sinavi2Helper.getAvalMontSinaviDesc(_trajet.avalantMontant));
    _w.stringField(1, "| Sens de Navigation");
    _w.writeFields(_fmt);
    _w.stringField(0, CtuluLibString.getString(_trajet.gareDepart));
    _w.stringField(1, "| Gare de d�part");
    _w.writeFields(_fmt);
    _w.stringField(0, CtuluLibString.getString(_trajet.gareArrivee));
    _w.stringField(1, "| Gare d'Arriv�e");
    _w.writeFields(_fmt);
    _w.stringField(0, Sinavi2Helper.getSinaviTypeLoi(_trajet));
    // _w.stringField(0,CtuluLibString.getString(_trajet.typeDeLoi));
    _w.stringField(1, "| Type de loi");
    _w.writeFields(_fmt);
    if (Sinavi2Helper.isErlang(_trajet)) {
      _w.stringField(0, Sinavi2Helper.formatSinaviHeure(_trajet.loiE.heureDebutGeneration));
      _w.stringField(1, "| Heure D�but de G�n�ration");
      _w.writeFields(_fmt);
      _w.stringField(0, Sinavi2Helper.formatSinaviHeure(_trajet.loiE.heureFinGeneration));
      _w.stringField(1, "| Heure de fin de G�n�ration");
      _w.writeFields(_fmt);
      _w.stringField(0, CtuluLibString.getString(_trajet.loiE.ordreDeLaLoi));
      _w.stringField(1, "| Ordre de la loi");
      _w.writeFields(_fmt);
      _w.stringField(0, CtuluLibString.getString(_trajet.loiE.nbreBateauxAttendus));
      _w.stringField(1, "| Nombre de bateaux attendus");
      _w.writeFields(_fmt);
    }
    if (Sinavi2Helper.isDeterministe(_trajet)) {
      for (int j = 0; j < _trajet.loiD.dateHeure.length; j++) {
        _w.stringField(0, CtuluLibString.getString(_trajet.loiD.dateHeure[j].date) + CtuluLibString.ESPACE
            + Sinavi2Helper.formatSinaviHeure(_trajet.loiD.dateHeure[j].heure));
        _w.stringField(1, "| Heure de G�n�ration");
        _w.writeFields(_fmt);
      }
      _w.stringField(1, "| Fin des entr�es pour la loi deterministe");
      _w.writeFields(_fmt);
    }
    if (Sinavi2Helper.isJournaliere(_trajet)) {
      for (int j = 0; j < _trajet.loiJ.heureJ.length; j++) {
        _w.stringField(0, Sinavi2Helper.formatSinaviHeure(_trajet.loiJ.heureJ[j]));
        _w.stringField(1, "| Heure de G�n�ration");
        _w.writeFields(_fmt);
      }
      _w.stringField(1, "| Fin des entr�es pour la loi journali�re");
      _w.writeFields(_fmt);
    }
    _w.flush();

  }

  public static void doUnTrematage(final SParametresTrematages _trematage, final FortranWriter _w, final int[] _fmt) throws IOException {
    _w.stringField(0, _trematage.bief);
    _w.stringField(1, _trematage.type1);
    _w.stringField(2, _trematage.type2);
    _w.stringField(3, Sinavi2Helper.getOuiNon(_trematage.ouiNon));
    _w.writeFields(_fmt);
    _w.flush();
  }

  public static void doVitesses(final SParametresVitesses[] _vitesses, final File _dest, final ProgressionInterface _prog,
      final CtuluAnalyze _analyze) {
    final WriteAction write = new WriteAction(getFmtVitesse(), "Ecriture des vitesses", false) {
      void write(FortranWriter _w, int _idx) throws IOException {
        doUneVitesse(_vitesses[_idx], _w, super.fmt_);
      }
    };
    write(_vitesses.length, _dest, _prog, _analyze, write);
  }

  private static int[] getFmtVitesse() {
    return new int[] { 21, 25, 10, 10 };
  }

  public static int[] getFmtCroisement() {
    return new int[] { 20, 20, 20, 1 };
  }

  private Sinavi2Writers() {}

}
