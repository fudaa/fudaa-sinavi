package org.fudaa.dodico.sinavi2;

import junit.framework.TestCase;

import org.fudaa.dodico.corba.sinavi2.SParametresBateau;

public class TestJParametresSinavi extends TestCase {

  public void testFormat() {
    double d = 12.34;
    assertEquals("12.34", Sinavi2Helper.formatHeureMin(d));
    d = 12.61;
    assertEquals("13.01", Sinavi2Helper.formatHeureMin(d));
    d = 25.61;
    assertEquals("02.01", Sinavi2Helper.formatHeureMin(d));
    d = 24.30;
    assertEquals("00.30", Sinavi2Helper.formatHeureMin(d));
    d = 24.00;
    assertEquals("24.00", Sinavi2Helper.formatHeureMin(d));
    
    d = 23.60;
    assertEquals("24.00", Sinavi2Helper.formatHeureMin(23,60));
    assertEquals("24.00", Sinavi2Helper.formatHeureMin(d));
  }

  public void testEcriture() {
    //final File f = TestIO.getFile(TestJParametresSinavi.class, "sinavi.test");
    //final CtuluAnalyze an = new CtuluAnalyze();
    SParametresBateau batInital = new SParametresBateau();
    batInital.largeur = 0;

    // ecrire bateau dans fichier
    // DParametresSinavi.ecritParametres(null,f,"test",null,an);
    // lire fichier dans struct
    SParametresBateau batLu = new SParametresBateau();
    assertEquals(batInital.largeur, batLu.largeur, 1E-10);

  }

}
